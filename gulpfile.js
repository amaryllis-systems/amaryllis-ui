/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// import global modules
let gulp = require('gulp');
// import tasks
const   taskClean = require('./tasks/build-clean'),
        taskFonts = require('./tasks/build-fonts'),
        taskImages = require('./tasks/build-images'),
        taskAudio = require('./tasks/build-audio'),
        taskCoreScripts = require('./tasks/build-scripts'),
        taskThemeScripts = require('./tasks/build-themes-scripts');
const taskIncrement = require('./tasks/build-increment');

const taskCssCore = require('./tasks/build-css'),
        taskCompileThemes = require('./tasks/compile-themes');

const taskCompressThemes = require('./tasks/compress-themes');

const taskPackCore = require('./tasks/build-pack-core');

const taskMakeThemeConfig = require('./tasks/make-theme-config');
const taskBuildHelp = require('./tasks/build-help');
const tasksBump = require('./tasks/bump');

const taskJsDoc = require('./tasks/build-jsdoc');

/**
 * Gulp Tasks
 * 
 * | Task Name              | Description                                               |
 * |------------------------|-----------------------------------------------------------|
 * | build:clean            | Cleaning css directories                                  |
 * | build:fonts            | building font directory from src to dist                  |
 * | build:images           | building images and icons directory from src to dist      |
 * | build:audio            | building audio directory from src to dist                 |
 * | build:scripts:core     | building system script directory from src to dist         |
 * | build:scripts:modules  | building modules script directories from src to dist      |
 * | build:scripts:themes   | building themes script directories from src to dist       |
 * | build:scripts          | building all script directory from src to dist            |
 * | build:increment        | increments the build number.                              |
 * | build:css              | Compile and Copy all                                      |
 * | build:css:core         | Copy Core only                                            |
 * | build:css:themes       | Compile themes only                                       |
 * | build:css:modules      | build:css modules only                                    |
 * | compress:themes        | compress theme styles only                                |
 * | build                  | Build full, including themes, modules, compression        |
 */

gulp.task('build:clean', taskClean);

// fonts, audio, images
gulp.task('build:fonts', taskFonts);
gulp.task('build:images', taskImages);
gulp.task('build:audio', taskAudio);

// JavaScript files
gulp.task('build:scripts:core', taskCoreScripts);
gulp.task('build:scripts:themes', taskThemeScripts);
gulp.task('build:scripts', gulp.series('build:scripts:core', 'build:scripts:themes', (done) => {
    done();
}));

gulp.task('build:increment', taskIncrement);

gulp.task('build:css:core', taskCssCore);
gulp.task('build:css:themes', taskCompileThemes);
gulp.task('build:css', gulp.series('build:css:core', 'build:css:themes', (done) => {
    done();
}));

//run gulp compress in terminal to minify css for production
gulp.task('compress:themes', taskCompressThemes);

gulp.task('make:theme-config', taskMakeThemeConfig);

gulp.task('build:themes', gulp.series('build:css:themes', 'build:scripts:themes', 'compress:themes', (done) => {
    done();
}));

gulp.task('build:pack:core', taskPackCore);

// run jsdoc
gulp.task('build:jsdoc', taskJsDoc);


// Build the full stuff
gulp.task('build', gulp.series(
    'build:clean', 
    'build:increment',
    'build:fonts', 
    'build:audio', 
    'build:images',
    'build:scripts:core',
    'build:css:core',
    'build:pack:core', function (done) {
    done();
}));

// Update Major Version:
//
// gulp bump --next major
// gulp bump --version 1.3.0
gulp.task('bump', tasksBump.bump);

// Update Major Version:
gulp.task('bump:major', tasksBump.major);

// Update Minor Version:
gulp.task('bump:minor', tasksBump.minor);

// Update Patch Version:
gulp.task('bump:patch', tasksBump.patch);

// Update Version:
// 
// gulp bump --version 1.3.0
gulp.task('bump:version', tasksBump.version);

gulp.task('build:help', taskBuildHelp);

//run 'gulp' in terminal to get everything going
gulp.task('default', gulp.series('build', (done) => {
    done();
}));
