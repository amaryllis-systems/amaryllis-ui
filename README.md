# Amaryllis UI Framework

__NOTE__: Amaryllis UI is a simple UI Framework designed for Amaryllis-CMS.
While it might be possible to use the UI `sass` components anywhere,
the most parts of our `javascript` library is very strict and will 
only work in Amaryllis-CMS themes out of the box.

## Install with npm

To use the custom ui build tool, it is mandatory to have npm installed!
If you don't want to make adjustments, this step is superfluous.

``` bash
npm i -g gulp
npm i -g gulp-cli
npm i
```

## Build with gulp

First, apply your own steps. You can then run the build process to get your own styles.

``` bash
gulp build
```

You can find the new files in `./dist` folder.

## read the docs

A (mostly) complete documentation of the UI is available
[here](https://www.amaryllis-ui.eu/?src=Bitbucket "Amaryllis UI documentation").
