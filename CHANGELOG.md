# Changelog

## 3.5.0

### SASS

* `components/lists/_list` got a new option `&.selectable` to formats list items as possible choices/ choosen items.
* `components/form/field/file/_v5` supports now component and office colors
* add css var support at least to root
* improved scrollbar
* improved filetree and filellist components
* improved media manager
* removed some included styles that can be loaded per js module
* SASS Doc improvements
* Fixed issue with multiple open modal overlays
* fixed an issue with position of the informations/window/v1 component
* fixed an issue with opening/closing effect of the informations/window/v1 component
* Collcection has a new option `.horizontal`
* Form-Groups allow horizontal alignment of certain childs

### JavaScript

* refactored profile board script to `apps/profile/board`
* added new examle script for profile comments in `apps/profile/comments/profile-comments.js`
* new profile avatar form module in `apps/profile/avatar/avatar-form.js`
* new file upload button handling for `v5` and `v6` variants in `apps/ui/elements/file-upload-button.js`
* new ui script for avatar dashboard in profile `apps/profile/avatar/avatar`.
* refactoreded `tools/color-converter` to ES6 in `tools/color/color-converter`
  * added new method to color-converter `namedToHex` to convert named css-color to hex value
* Added new ccs-name-color storage in `tools/color/color.json`
* `tools/string/string-tool` gots new method(s)
  * `pad` to pad a string from left/right
  * `parseHTML` to parse HTML String to element
* new color class to deal with colors in `tools/color/color`
* new JSON data file in `tools/color/color.json` with HTML named colors => hex color pairs
* converted `tools/clipboard` to `ES6` class
* created a new class for range building in `tools/object/range`
* created a new class for extending objects in `tools/object/extend`
* created a new class for array helpers in `tools/object/array-helper`
* deprecation of `tools/utils` - to be removed in `4.0`
* CssLoader has now the root colors from css stylesheet stored
* Update Code Mirror to 5.48.0
* update tinymce to 5.0.9
* update moment.js to 2.24.0
* update moment-timezone to 0.5.25-2019a
* refactored `ui/media/audio/*` to `apps/ui/media/audio/*`
* added new media manager for community edition, currently lightweight version from professional version
* refactored markdown-editor to es6 syntax

## 3.4.4

### SASS

* bugfix for `nav v1` color variants combined with arrow nav
* small improvemnts of the `nav v1`
* bugfix `boxed collection v1` with image/avatar inside and wrong padding
* Profile inbox refactoring and improvements
* SASS opacity classes improvements in `sass/amaryllis/_partials/utilities/visibility`
* FontAwesome Update to Version `5.9`
* comments v1 bugfix with any other element type than anchor as action (enables span now)

### JavaScript

* Profile inbox refactoring and improvements
* `view/admin/configs/configs-field` has been refacored to `apps/admin/configs/configs-field`
* `events/event` has been refactored to es6 module
* Created a new String Tool in `tools/string/string-tool` to replace the `core/string` component in future releases and provide more additional string methods.
* Created a new Comments Module `apps/components/comments/comments` to manage comment voting and comment forms
* Fixed several errors in friendship views

## Version 3.4.3

### SASS

* bugfix in `step-list v1` with double-digit numbers
* quotes styles supporting now v1 to v6 (`quotes v1` instead of `quotes-1`) to have a continous integration of the variant declaration. However, quotes-1 still works up to any of the upcoming minor release updates.
* bugfix in `pager v1` with disabled state on list items
* bugfix for checkbox variants and unicode contents in pseudoclasses for microsoft browsers (ie and edge, except edge chromium).

### JavaScript

* adding new profile view suport for spam reports and front end spam report managing in `apps/profile/spamreport` and `apps/profile/admin/spamreport`
* `ui/overlay/modal` refactored to `apps/ui/informations/modal`

## Version 3.4.2

### SASS

* Modal header Bugfix für Modals in Formular
* New View Stylesheet `sass/view/sitemap.scss`
* The Nav-Component `navs/navs/v1` supports now a simple dropdown sub-menu
* Bugfixes for default SASS variables of nav component `navs/navs/v1`
* We added new SASS configuation-variables to `navs/navs/v1` to build the component- and office-colors only if required
* Bug-Fixes in list component `components/lists/list`

### JavaScript

* `ui/components/panel` refactored to `apps/ui/components/panel`

## Version 3.4.1

### SASS

* Bugfix form-group in statlist v1
* table supports now alignment classes on each table column, not only on table
* table component supports now font size palette and has improved compact mode
* dual-list component has now its own styles and does not longer rely on column styles
* sidebar v2 supports now `nav-separator`
* items v1 supports now different font-sizes `small`, `medium` and `large`

### JavaScript

* Theme Admin refactoring
* Mimetypes Bug fix
* Translations Admin refactoring
* `core/logger` has a new method `logError` to prevent more typos. Its Basically the same as `logWarning`
* `ui/components/duallist` refctored and improved to `apps/ui/components/dual-list`
* new Template Helper `log` to debug variables. `{{log var1 var2}}`

## Version 3.4.0

### SASS

* Bugfix File-Tree v1 Margin/Padding
* `components/dashboard/statinfo/_v1.scss` unterstützt jetzt auch switch
* `informations/window/v1` unterstützt jetzt eine zusätziche `window-details` Sidebar
* `media/filesystem/file-list/v1` verbessert
* `media/filesystem/file-tree/v1` verbessert
* `navs/tabs/v2` ergänzt mit JavaScript unabhängigen Tabs
* emoji/smiles support verbesserungen und neue emojis anstelle der alten
* File Input v6 ergänzt
* form-group Option `.wide` mit Breite `auto` ergänzt (`.form-group.wide.auto`)
* fields Unterstützen jetzt auch "lesbare" Zahlenformate, nicht nur numerische. (`fields-2 fields` => `two fields`)

### JavaScript

* `apps/admin/common-export` ergänzt als allgemeinen APF Admin Export für Table Handler
* `apps/apf/approval/approval` ergänzt für globales Approval Handling, inklusive custom Template `templates/html/apf/approval/approval-popup.hbs`
* Codemirror und Tinymce Update
* neues Tool `tools/class/class-tool` zur Auflistung von Methoden in ES6 Klassen
* Diverse Refactorings von `ui/` nach `apps/` 
* neues Smiles/Emoji Handling unter `apps/front/emoji` inklusive templates unter `templates/html/front/emoji`
* neuer Template Helper `toJson` um Original-Objekte als JSON anzuhängen
* emoji Admin refactoring mit neuem Modul `apps/admin/data/emoji/emoji-admin`
* mimetypes Admin refactoring mit neuem Modul `apps/admin/data/mimetypes/mimetypes-admin`

## Version 3.3.0

### SASS

* Overlay v1 und v2 Verbesserungen
* Update Bar v1 Fix mit fehlender Versions-Angabe
* Update Bar v1 nutzt jetzt ein mixin für die Erstellung von Farb Varianten
* Update Bar v1 unterstützt jetzt office colors
* FontAwesome Update auf 5.7.0
* list-alphabetical verbessert und HTML-Tag Bundling entfernt.
* Bugfix Popover classnames
* Verbesserungen Popover
* Calendar in Office Color Palette ergänzt
* Bugfix in .statinfo.v1 und UI Verbesserungen
* .tag.v1 und .tag.v3 bugfix mit Komponenten Farb-Varianten behoben

### JavaScript

* Overlay Module Refactoring
* Neuer Escaper `tools/string/escaper` und Methoden aus `tools/utils` entfernt.
* Neues UniqueId Modul `tools/string/unique-id` als zukünftiger Ersatz in `tools/utils`
* Profile Tasks App refactoring
* Clients Module Scripts added to Core
* Neue Handlebars Template Funktion `toLowerCase` um Strings in Kleinbuchstaben zu konvertieren.
* `ui/components/listsorter` refactored nach `apps/ui/list-sorter`
* `view/admin/module/modules` refactored nach `apps/admin/module/modules` mit vielen neuen Funktionen
* Das Init Script unterstützt jetzt Translator Dictionaries in Modulen und Themen von Haus aus
* `view/admin/version/module-loader` refactored nach `apps/admin/module/module-loader`
* `ui/widgets/update-bars` refactored nach `apps/ui/informations/update-bars`
* Template für Update Bars erstellt
* UpdateBars in allen Core Modulen ersetzt
* `ui/components/inputsearch` Fehler bei Token List/Token zufügen behoben
* `ui/components/popover` refactored nach `apps/ui/informations/popover`
* neues Tool `tools/image-loaded` zur Validierung ob ein Bild Element geladen wurde
* WindowBox Refactoring auf ES6 (`apps/ui/informations/window`)
* Searching App started (`apps/searching/search`)
* Autocomplete Refactoring zu ES6 (`apps/searching/autocomplete`)
* `tools/string/highlight` ergänzt für Text-Suche mit Highlight Funktion

## Version 3.2.0

### SASS

* FontAwesome Update auf 5.6.3
* Profile Notes Verbesserungen
  * Layout Verbesserungen
* Profile Task Verbesserungen
  * Profil-Task Farb-Unterstützung
  * Layout Verbesserungen

### JavaScript

* Profile Task Verbesserungen und in Modul verschoben
  * Task Color Select
  * Task Handlebars Templates
* Profile Notes Verbesserungen und in modul verschoben
  * Profile Notes basieren jetzt großteils auf Templates
  * Aktionen verbessert
  * zusätzliche Einzel-Ansicht ergänzt
  * Markdown-Editor Support ergänzt (siehe Amaryllis-CMS Changelog)
* FullScreen Widget Refactoring auf ES6 (`ui/widgets/full-screen`)


## Version 3.2.1

### SASS

* Neue [Image-Hover Effekte](https://www.amaryllis-ui.eu/ui-framework/media/images/image-hover/ "mehr erfahren")
    * animate-pivot mit den sub-effekten .in.top-left, .in.top-right, .in.bottom-left, .in.bottom-right, .out.top-left, .out.top-right, .out.bottom-left, .out.bottom-right
    * animate-throw ergänzt um die sub-effekte .out.left, .out.right, .out.up, .out.down
    * animate-image mit den sub-effekten .zoom-out, .zoom-center, .rotate-left, .rotate-right
    * animate-parallax mit den sub-effekten .up, .down, .left, .right
* cookie-bar Verbesserungen und inverted sowie sizing palette ergänzt
* button group v2 bugfix mit button margin
* sizing palette für collection v1 
* allgemeine Verbesserungen
* Handlebars Helpers gestartet
* neue separate styles für JS Module 
    * `ui/navs/offcanvas`
    * `ui/navs/accordion`
* Profile Friendship Refactoring
* FontAwesome Update auf v5.5.0
* Profile Task List Verbesserungen und support für Task-Farben, Komponenten- und Office-Farben eingeführt

### JavaScript

* Cookie-Bar erneuert, mit Handlebar Template und in `apps/cookie-bar` verschoben
* Diverse Module in ES6 konvertiert
    * `ui/navs/offcanvas` to ES6
    * `ui/navs/accordion` to ES6
    * `ui/tools/up-top` to ES6
* cleanup von outdated modules
* eine Reihe an kleinen JS Tools wurde unter `tools` ergänzt
    * `tools/number/bytes`
    * `tools/number/is-number`
    * `tools/string/camelcase`
    * `tools/string/chop`
    * `tools/string/is-selector`
    * `tools/string/sprintf`


## Version 3.2.0

### SASS

* Button Verbesserungen
* `.button-group` und `.buttons` jetzt mit Option `.stackable` für Mobil-Geräte
* Neue Funktion implementiert um Strings in numerische Werte zu konvertieren
* Basis-Font-Größen in sizing-Palette jetzt als numerische Werte
* Gallery v2 hat jetzt JavaScript Optionen zur besseren Kontrolle
* Inputsearch v1 Verbesserungen
* Modal Overlay Verbesserungen, Layout-Optimierungen und ergänzen zusätzlicher Größen
* [Image](https://www.amaryllis-ui.eu/ui-framework/media/images/image/ "mehr erfahren") mit verschiedenen Größen-Angaben und alignments ergänzt
* [Items List v1](https://www.amaryllis-ui.eu/ui-framework/components/lists/items-v1/ "mehr erfahren") ergänzt
* Gallery v1 option-2 kann die Thumbnail Caption jetzt auch oberhalb anzeigen lassen
* Breadcrumb v3 wurde um die Optionen 2-3 ergänzt
* FontAwesome Update auf 5.4.1
  
### JavaScript

* `ui/components/inputsearch`
    * Komponente nutzt jetzt Handlebars Template für Tokens
    * Optionen für Inoutsearch ergänzt und Verbesserungen eingepflegt
* `ui/tools/collapse`
    * neue Optionen für Icon Rotation/unterschiedliche Icons für hidden/visible 
    * Callbacks für enable/disable eingeführt
* `ui/app/notifier`
    * Handlebars Template für Web-Notifications eingeführt

## Version 3.1.0

* Semantische Anpassungen aller Varianten sowie der gesamten Struktur. Varianten werden
  jetzt nahezu überall eingesetzt und können flexibel eingebunden oder ausgelassen werden.
* [Timeline v2](https://www.amaryllis-ui.eu/ui-framework/profile/timeline/timeline-v2/ "Hier findest du ein Beispiel zum Einbinden der Timeline v2") ergänzt
* [Content Block v5](https://www.amaryllis-ui.eu/ui-framework/components/blocks/content-block/content-block-v5/ "Hier findest du ein Beispiel zum Einbinden des Content Blocks v5") ergänzt
* info-box v1 ergänzt
* Font Awesome Update auf 5.3.1
* Neue verbesserte [list Komponente](https://www.amaryllis-ui.eu/ui-framework/components/lists/list/ "Erfahre mehr über die List-Komponente") für unterschiedliche List-Typen
* Neue Navigation "Nav v1"
* definition List refactoring
* verbesserte Funding Bar
* steplist v2 ergänzt
* vereinfachte breadcrumb v3 ergänzt zur Einbindung in den direkten Hintergrund ohne weiteres Markup
* Pager v2 ergänzt
