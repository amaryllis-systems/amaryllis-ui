/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * options for sass-compiler
 */
const sassoptions = {
    linefeed: 'lf',
    sourceMap: false,
    outputStyle: 'nested',
    noCache: true
};

/**
 * options for auto-prefixer
 */
const prefixOptions = {
    /*browsers: [
        'last 3 versions',
        'ie >= 8',
        'ios >= 7',
        'android >= 4.4',
        'bb >= 10'
    ],*/
    cascade: true,
    remove: true
};

/**
 * options for clean-css
 */
const cleanCSSOptions = {
    inline: ['all'],
    level: 2,
    rebase: false
};

const JSONtoSASS = {
    sass: './src/sass/assets/_framework.scss',
    prefix: 'sass',
    suffix: '',
    separator: '-'
};


const minifyOptions = {
    "mangle": false,
    "compress": {
        arrows: false,
        booleans: false,
        if_return: false,
        inline: false,
        unused: false,
        switches: false,
        dead_code: false ,
        collapse_vars: false,
        comparisons: false,
        computed_props: false,
        conditionals: false,
        expression: true,
        join_vars: false,
        keep_classnames: true,
        keep_fnames: true,
        hoist_props: false,
        loops: false,
        negate_iife: false,
        properties: false,
        reduce_funcs: false,
        reduce_vars: false,
        sequences: false,
        typeofs: false,
        warnings: true,
        ecma: 5
    },
    "output": {
        "comments": "/licence|license|copyright/i",
        "quote_style": 3
    },
    parse: {
        bare_returns: true
    },
    //ecma: 6,
    warnings: true
};

const babelOptions = {
    presets: ['@babel/preset-env']
};


/**
 * Config Options for several globale node modules used in gulp tasks.
 * 
 * - sass: Holding sass compile configs
 * - prefix: Holding auto-prefix options.
 * - cleanCSS: Holding Clean-CSS Options
 * - jsonToSass: JSON To SASS Configuration
 */
module.exports = {
    sass: sassoptions,
    prefix: prefixOptions,
    cleanCSS: cleanCSSOptions,
    jsonToSass: JSONtoSASS,
    minify: minifyOptions,
    babel: babelOptions
};
