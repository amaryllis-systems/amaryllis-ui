/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 
// Dist Folder Name
const dist = "./dist";

// media folder name
const media = "/media/";

// paths
const paths = {
    parts: {
        media: media,
        dist: dist
    },
    core: {
        input: {
            sass: "src/sass/amaryllis/**/*.scss",
            scripts: "src/scripts/**/*.min.js",
			fonts: "src/fonts/**/*",
			images: "src/images/**/*",
			icons: "src/icons/**/*",
			audio: "src/audio/**/*"
        },
        output: {
            sass: dist + media + "css",
            scripts: dist + media + "scripts",
			fonts: dist + media + "fonts",
			images: dist + media + "images",
			icons: dist + media + "icons",
			audio: dist + media + "audio"
        }
    },
    themes: {
        input: {
			base: "src/themes",
            sass: "src/themes/%s/sass/**/*.scss",
            scripts: "src/themes/%s/scripts/**/*.js",
            hbs: "src/themes/%s/scripts/**/*.hbs",
            json: "src/themes/%s/scripts/**/*.json",
            fonts: "src/themes/%s/fonts",
            css: dist + "/themes/%s" + media + "css/"
        },
        output: {
            sass: dist + "/themes/%s" + media + "css",
            scripts: dist + "/themes/%s" + media + "scripts",
            fonts: dist + "/themes/%s" + media + "fonts",
            css: dist + "/themes/%s" + media + "css"
        }
    },
    modules: {
        input: {
			base: "src/modules",
            sass: "src/modules/%s/sass/**/*.scss",
            scripts: "src/modules/%s/scripts/**/*.js",
            hbs: "src/modules/%s/scripts/**/*.hbs",
            json: "src/modules/%s/scripts/**/*.json",
            fonts: "src/modules/%s/fonts",
            css: dist + "/modules/%s" + media + "css/**/"
        },
        output: {
            sass: dist + "/modules/%s" + media + "css",
            scripts: dist + "/modules/%s" + media + "scripts",
            fonts: dist + "/modules/%s" + media + "fonts",
            css: dist + "/modules/%s" + media + "css"
        }
	},
	setup: {
		templates: {
			themeconfig: "setup/templates/themes.config.json",
			sassversion: 'sass-version.json'
		},
		output: {
			themeconfig: "setup/conf/themes.config.json"
		}
	}
};

module.exports = paths;
