/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

let fs = require('fs'),
    path = require('path');

const getFolders = (dir) => {
    return fs.readdirSync(dir)
        .filter(function(file){
            return fs.statSync(path.join(dir, file)).isDirectory();
        });
};


const readPackageJson = () => {
    return readJSON('./package.json');
};

const readJSON = (src) => {
    return JSON.parse(fs.readFileSync(src, 'utf8'));
};
 
const readdirRecursSync = (dir, filelist) => {
    filelist = filelist || [];
  
    let files = fs.readdirSync(dir), filepath, file;
  
    files.forEach((filename) => {
      filepath = path.join(dir, filename);
  
      if (fs.statSync(filepath).isDirectory()) {
        filelist = readdirRecursSync(filepath, filelist);
      } else {
        file = {
            name: filename,
            path: filepath
        };
        filelist.push(file);
      }
    });
  
    return filelist;
};

const readFilesRecursSync = (dir, filelist) => {
    filelist = filelist || [];
  
    let files = fs.readdirSync(dir), filepath, file;
  
    files.forEach((filename) => {
      filepath = path.join(dir, filename);
  
      if (fs.statSync(filepath).isDirectory()) {
        filelist = readFilesRecursSync(filepath, filelist);
      } else {
        file = {
            name: filename,
            path: filepath
        };
        filelist.push(file);
      }
    });
  
    return filelist;
};


const files = {
    'readFilesRecursSync': readFilesRecursSync,
    'readdirRecursSync': readdirRecursSync,
    'getFolders': getFolders,
    'readPackageJson': readPackageJson,
    'readJSON': readJSON
};

module.exports = files;
