/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @copyright 2013 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright 2014 - 2018 Amaryllis Systems GmbH http://www.amws.eu/
 * @copyright 2017 - 2018 Amaryllis CMS Project http://www.amaryllis-cms.de/
 * @license https://www.amaryllis-cms.de/lizenzen/
 * @license Apache Licence 2.0 https://www.apache.org/licenses/LICENSE-2.0
 *
 * Dual-Licensed with Amaryllis CMS EULA. You may obtain a copy of the
 * EULA at
 *
 *     https://www.amaryllis-cms.de/lizenzen/eula/
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

define(['module', 'text', 'tools/utils'], function (module, textPlugin, Utils) {

	var buildText = {};
	var defaults = {
			comments: 'strip',
			whitespaceBetweenTags: ''
		},
		hasLocation = typeof location !== 'undefined' && location.href,
		defaultProtocol = hasLocation && location.protocol && location.protocol.replace(/\:/, ''),
		defaultHostName = hasLocation && location.hostname,
		defaultPort = hasLocation && (location.port || undefined),
		masterConfig = (module.config && module.config()) || {},
		xmlRegExp = /^\s*<\?xml(\s)+version=[\'\"](\d)*.(\d)*[\'\"](\s)*\?>/im,
		bodyRegExp = /<body[^>]*>\s*([\s\S]+)\s*<\/body>/im;

	return {

		load: function (name, req, onLoad, config) {
			var self = this,
				file = textPlugin.parseName(name),
				conf = Utils.extend({}, defaults, config.config.html || {});
			if (config && config.isBuild && !config.inlineText) {
				onLoad();
				return;
			}

			masterConfig.isBuild = config && config.isBuild;

			var parsed = textPlugin.parseName(name),
				nonStripName = parsed.moduleName +
				(parsed.ext ? '.' + parsed.ext : ''),
				url = req.toUrl(nonStripName),
				useXhr = (masterConfig.useXhr) ||
				textPlugin.useXhr;
			textPlugin.get(url, function (html) {
				if (parsed.strip) {
					//html = textPlugin.strip(html);
				}
				for (var option in conf) {
					if (option in self.transform) {
						html = self.transform[option](conf[option], html);
					}
				}

				if (config.isBuild) {
					buildText[name] = html //textPlugin.jsEscape(html);
				}
				
				var re = /<html( |>)((.|[\r\n])*)<\/html(.*)>|<head( |>)((.|[\r\n])*)<\/head(.*)>/i;
				var isDocument = re.test(html), el;
				if(!isDocument) {
					var doctype = document.implementation.createDocumentType( 'html', '', '');
					var dom = document.implementation.createDocument('', 'html', doctype);
					var head = dom.createElement('head');
					var charset = dom.createElement('meta');
					charset.charset = 'UTF-8';
					head.appendChild(charset);
					el = dom.createElement('body');
					dom.documentElement.appendChild(head);
					dom.documentElement.appendChild(el);
					el.innerHTML = html;
				} else {
					el = document.createElement('div');
					el.innerHTML = html;
				}

				onLoad(el.innerHTML);
			}, onLoad.error);
		},


		write: function (pluginName, moduleName, write) {
			if (buildText.hasOwnProperty(moduleName)) {
				var name = "'" + pluginName + "!" + moduleName + "'",
					text = "function () {return '" + buildText[moduleName] + "';}";

				write.asModule(pluginName + "!" + moduleName,
					"define(function () { return '" +
					buildText[moduleName] +
					"';});\n");
			}
		},


		transform: {

			comments: function (action, html) {
				if (action === 'strip') {
					return html.replace(/<!--(.|[\n\r])*?-->/gm, '');
				} else {
					return html;
				}
			},


			whitespaceBetweenTags: function (action, html) {
				var pattern = />[\n\r\s]+</gm;

				if (action === 'strip') {
					return html.replace(pattern, '><');
				} else if (action === 'collapse') {
					return html.replace(pattern, '> <');
				} else {
					return html;
				}
			},


			whitespaceBetweenTagsAndText: function (action, html) {
				var afterTagPattern = />[\n\r\s]+/gm,
					beforeTagPattern = /[\n\r\s]+</gm;

				if (action === 'strip') {
					return html.replace(afterTagPattern, '>').replace(beforeTagPattern, '<');
				} else if (action === 'collapse') {
					return html.replace(afterTagPattern, '> ').replace(beforeTagPattern, ' <');
				} else {
					return html;
				}
			},


			whitespaceWithinTags: function (action, html) {
				if (action === 'collapse') {
					var tagPattern = /<([^>"']*?|"[^"]*?"|'[^']*?')+>/g,
						attrPattern = /([^\0\n\r\s"'>\/=]+)(?:\s*(=)\s*([^\n\r\s"'=><`]+|"[^"]*"|'[^']*'))?/gi,
						lastIndex = 0,
						result = '',
						match,
						tag;

					while ((match = tagPattern.exec(html)) !== null) {

						// Copy text between the beginning of this match and the end of the last one
						result += html.substring(lastIndex, match.index);
						tag = match[0];

						if (/^<[^\/]/.test(tag)) { // It's a start tag
							var attrs = tag.match(attrPattern),
								start = attrs.shift(),
								end = /\/>$/.test(tag) ? '/>' : '>';

							result += start + attrs.map(function (attr) {
								return attr.replace(attrPattern, ' $1$2$3');
							}).join('') + end;
						} else { // It's an end tag
							result += tag.replace(/[\n\r\s]+/g, '');
						}

						lastIndex = tagPattern.lastIndex;
					}

					return result + html.substring(lastIndex);
				} else {
					return html;
				}
			}

		}

	};

});