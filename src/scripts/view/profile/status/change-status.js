/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */

define("view/profile/status/change-status", [
    'tools/utils',
    'core/selectors',
    'events/event'
], function(Utils, Selectors, AcmsEvent) {
    
    "use strict";
    
    var STATUSFORM_DATA = "acms.view.profile.status.change-status",
        NS = "." + STATUSFORM_DATA,
        EVENT_CHANGE = "change" + NS,
        EVENT_CHANGED = "changed" + NS
    ;
    
    var StatusForm = function(element, options) {
        this.element =
        this.options = null;
        this.initialize(element, options);
    };
    
    StatusForm.DEFAULT_OPTIONS = {
        remote: null,
        update: '[data-role="profile-status"]'
    }
    
    StatusForm.MODULE = "Profil Status Form";
    
    StatusForm.VERSION = "1.5.0";
    
    StatusForm.needInit = true;
    
    StatusForm.init = function(element, options) {
        var sf = new StatusForm(element, options);
        sf.element.acmsData(STATUSFORM_DATA, sf);
        return sf;
    }
    
    StatusForm.prototype = {
        constructor: StatusForm,
        
        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this.remote = this.options.remote || this.element.getAttribute('action');
            this.listen();
        },
        
        getDefaultOptions: function() {
            return StatusForm.DEFAULT_OPTIONS;
        },
        
        buildOptions: function(options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },
        
        listen: function() {
            var self = this, o = this.options;
            var btn = self.element.querySelector('button[type=submit]');
            AcmsEvent.add(btn, 'click', function(e) {
                e.preventDefault();
                var val = self.element.querySelector('input[type=text]').value;
                require(['http/request', 'notifier'], function(Request) {
                    var url = self.remote,
                        data = new FormData(),
                        done = function(response) {
                            if(response.status === 'success') {
                                var eles = Selectors.qa(o.update), j;
                                for(j = 0; j < eles.length; j++) {
                                    eles[j].innerHTML = response.userstatus;
                                }
                            }
                        }, fail = function(jqXHR, textStatus) {
                            Acms.Logger.writeLog(textStatus)
                        };
                    data.append('op', 'change');
                    data.append('status', val);
                    Request.post(url, data, done, fail);
                });
            })
        }
    };
    
    return StatusForm;
});