/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */


define('view/profile/password/change-password', [
    'tools/utils',
    'core/selectors',
    'events/event',
    'core/acms'
], function(Utils, Selectors, AcmsEvent, A) {
    
    "use strict";
    
    var PasswordChange = function(element, options) {
        this.element    = 
        this.button     =
        this.remote     =
        this.options    = null;
        this.initialize(element, options);
    };
    
    PasswordChange.MODULE = "Password-Change Formular";
    
    PasswordChange.VERSION = "1.5.0";
    
    PasswordChange.NS = "acms.view.profile.password-change";

    PasswordChange.DEFAULT_OPTIONS = {
        onChange: function(self) {}
    };

    PasswordChange.needInit = true;

    PasswordChange.init = function(element, options) {
        var pc = new PasswordChange(element, options);
        pc.element.acmsData(PasswordChange.NS, pc);
        
        return pc;
    };

    PasswordChange.prototype = {
        constructor: PasswordChange,
        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },
        
        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {PasswordChange.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return PasswordChange.DEFAULT_OPTIONS;
        },
        /**
         * Bildet die Optionen
         *
         * <p>
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         * </p>
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function (options) {
            return Utils.extend(
                    {},
                    this.getDefaultOptions(),
                    options,
                    this.element.acmsData()
                    );
        },
        
        /**
         * Aufsetzen der Event Listener
         * 
         * 
         */
        listen: function () {
            var self = this;
            AcmsEvent.add(self.button, 'click', self._onClick);
        },
        
        _onClick: function(e) {
            var self = this,
                target = e.target,
                form = Selectors.closest(target, 'form'),
                fData = new FormData(form);
            e.preventDefault();
            fData.append('op', 'change');
            require(['http/request', 'notifier'], function(Request) {
                Request.post(self.remote, fData, self._done, self._fail, self._always);
            });
            return false;
        },
        
        _done: function (response) {
            var self = this, o = self.options;
            A.Notifications.createFromResponse(response);
            var input = Selectors.q("input[name='acms_redirect']");
            var acms_redirect = input && input.value,
                    redirect = (!acms_redirect) ? A.Urls.baseUrl : acms_redirect;
            if(typeof o.onChange === 'function') {
                o.onChange(self);
            }
            if(redirect && redirect.isUrl()) {
                window.location.href = redirect;
            }
        },
        _fail: function (jqXHR, textStatus) {
            A.Logger.writeLog(textStatus);
        },
        _always: function (textStatus) {
            A.Logger.writeLog(textStatus);
        },
        
        _prepare: function() {
            var self = this, 
                o = self.options,
                element = self.element;
            self.remote = o.remote || element.getAttribute('action');
            self.button = Selectors.q('button[type=submit]', element);
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        }
    };

    return PasswordChange;
});
