/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */


define("view/profile/helpers/update-theme-settings", [
    'tools/utils',
    'events/event'
], function(Utils, AcmsEvent) {

    var Updater = {
        needInit: true,
        init: function(element, options) {
            this.$element = element;
            this.options = options;
        },

        listen:function() {
            
        }
    };

    return Updater;
});
