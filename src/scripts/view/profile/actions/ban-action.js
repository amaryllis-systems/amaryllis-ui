/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */

define('view/profile/actions/ban-action', [
    'tools/utils',
    'core/window',
    'events/event'
], function(Utils, Win, AcmsEvent) {
    
    "use strict";
    
    var MODULE_DATA = "acms.view.profile.ban-action";
    
    var Module = function(element, options) {
        this.element = 
        this.options = null;
        this.initialize(element, options);
    }
    
    Module.MODULE = "Sperr-Aktion";
    
    Module.VERSION = "1.5.0";
    
    Module.needInit = true;
    
    Module.DEFAULT_OPTIONS = {
        remote: null,
        reload: false,
        op: 'ban' // ban oder unban
    };
    
    Module.init = function(element, options) {
        var Mod = new Module(element, options);
        Mod.element.acmsData(MODULE_DATA, Mod);
    }
    
    Module.prototype = {
        constructor: Module,
        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this.listen();
        },
        
        getDefaultOptions: function() {
            return Module.DEFAULT_OPTIONS;
        },
        
        buildOptions: function(options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },
        
        listen: function() {
            var self = this, o = this.options, element = self.element;
            AcmsEvent.add(element, 'click', function(e) {
                e.preventDefault();
                var fData = new FormData();
                fData.append('op', o.op);
                fData.append('id', o.id);
                require(['http/request', 'notifier'], function(Request) {
                    var done = function(response) {
                        
                        if(response.status === "success") {
                            if(o.reload) {
                                Win.refresh(500);
                            }
                        }
                        Acms.Notifications.createFromResponse(response);
                    }, fail = function(jqXHR, textStatus) {
                        Acms.Logger.writeLog(textStatus)
                    }, always = function(jqXHR, textStatus, tt) {
                        console.log(jqXHR)
                        console.log(textStatus)
                        console.log(tt)
                    };
                    
                    Request.post(o.remote, fData, done, fail, always);
                });
                return false;
            });
            
        }
    };
    
    return Module;
});