/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */


define('view/profile/registration/registration-form', [
    'tools/utils',
    'core/selectors',
    'core/classes',
    'events/event',
    'core/acms'
], function(Utils, Selectors, Classes, AcmsEvent, A) {

    "use strict";
    
    /**
     * Constructor
     * 
     * @param {HTMLElement} element     Das Element
     * @param {Object}      options     Die eigenen Optionen
     * 
     * @returns {RegistrationForm}
     */
    var RegistrationForm = function(element, options) {
        this.element            =
        this.options            =
        this.button             =
        this.geburtstag         =
        this.birthday           =
        this.nutzungsbedingungen =
        this.terms              =
        this.fields             = null;
        this.initialize(element, options);
    };
    /**
     * Modul Name
     *
     * @var {String}
     */
    RegistrationForm.MODULE = "Registrierungs Formular";
    
    /**
     * Modul Version
     *
     * @var {String}
     */
    RegistrationForm.VERSION = "1.5.0";
    
    /**
     * Modul Namespace
     *
     * @var {String}
     */
    RegistrationForm.NS = "acms.view.profile.registration.registration-form";

    /**
     * Einbindung des Module Loaders
     *
     * @var {Boolean}
     */
    RegistrationForm.needInit = true;
    
    /**
     * Modul Initialisierung
     * 
     * @param {HTMLElement} element     Das Element
     * @param {Object}      options     Die eigenen Optionen
     * 
     * @returns {RegistrationForm}
     */
    RegistrationForm.init = function(element, options) {
        var f = new RegistrationForm(element, options);
        f.element.acmsData(RegistrationForm.NS, f);
        
        return f;
    };

    RegistrationForm.prototype = {
        
        constructor: RegistrationForm,
        
        /**
         * Interner Constructor
         *
         * @param {Object} element
         * @param {Object} options
         *
         * 
         */
        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this._precheck();
            this.listen();
        },
        
        
        /**
         * Gibt die Standard-Optionen zurück
         * 
         * @returns {RegistrationForm.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function() {
            return RegistrationForm.DEFAULT_OPTIONS;
        },
        
        /**
         * Bildet die Optionen
         *
         * <p>
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         * </p>
         *
         * @param {Object}      options     Eigene Optionen
         * 
         * @returns {RegistrationForm.DEFAULT_OPTIONS|Object}
         */
        buildOptions: function(options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },
        
        /**
         * Aufsetzen der Event Listener
         * 
         * 
         */
        listen: function() {
            var self = this, button = self.button;
            AcmsEvent.add(button, 'click', self._onSubmit);
        },
        
        /**
         * On-Click-Event Handler
         * 
         * @param {Event} e  Das Event
         * @returns {Boolean}
         */
        _onSubmit: function(e) {
            e.preventDefault();
            var self = this,
                fields = this.fields, field, value, group,
                target = e.target,
                form = Selectors.closest(target, 'form'),
                canProcess = true;
            fields.forEach(function(v, i) {
                field = fields[i];
                value = (Selectors.matches(field, 'checkbox')) ? field.checked : field.value;
                if(!value && (field.required || field.getAttribute('required'))) {
                    canProcess = false;
                }
                group = Selectors.closest(field, '.form-group');
                if(group && Classes.hasClass(group, 'invalid')) {
                    canProcess = false;
                }
                
            });
            if(false === canProcess) {
                A.Notifications.create({
                    'title': 'Fehler',
                    'content': 'Bitte füllen Sie alle Felder aus',
                    'type': A.Notifications.TYPE_DANGER
                });
                
                return false;
            }
            self._process(form);
            return false;
        },
        
        /**
         * Drchführen der Remote-Registrierung
         * 
         * @param {Element} form    Das Formular
         * 
         * 
         */
        _process: function(form) {
            var self = this,
                o = self.options,
                fData = new FormData(form),
                url = o.remote || A.Urls.baseUrl + '/registrieren/';
            require(['http/request', 'notifier'], function(Request) {
                Request.post(url, fData, self._done, self._fail);
            });
        },

        /**
         * Ausführung bei erfolgter Abfrage
         * 
         * <p>
         *  Wird ausgeführt, wenn eine Antwort vom Server kommt
         * </p>
         * 
         * @param {Object}      response    Die Antwort vom Server
         * 
         * 
         */
        _done: function(response) {
            A.Notifications.createFromResponse(response);
            if(response.status === 'success') {
                setTimeout(function() {
                    window.location.href = A.Urls.baseUrl;
                });
            }
        },
        
        /**
         * Ausführung bei fehlgeschlagener Abfrage
         * 
         * <p>
         *  Wird ausgeführt, wenn keine Antwort vom Server kommt bzw die 
         *  Anfrage fehlschlägt
         * </p>
         * 
         * @param {Object}      jqXHR    Das XHR Abfrage Object
         * 
         * 
         */
        _fail: function (jqXHR, textStatus) {
            A.Logger.writeLog(textStatus);
        },
        
        _precheck: function() {
            var self = this;
            if(self.birthday !== null) {
                require(['view/profile/registration/field/birthday'], function(Birthday) {
                    self.geburtstag = Birthday.init(self.birthday);
                });
            }
            if(self.terms !== null) {
                require(['view/profile/registration/field/terms'], function(terms) {
                    self.nutzungsbedingungen = terms.init(self.terms);
                });
            }
        },

        _prepare: function() {
            var self = this,
                o = self.options,
                element = self.element, fields;
            fields = Selectors.qa('input,select,textarea', element);
            self.fields = fields;
            self.remote = o.remote || element.getAttribute('action');
            self.button = Selectors.q('button[type=submit]', element);
            self.birthday = Selectors.q('input[name=birthday]', element);
            self.terms = Selectors.q('input[name="terms[]"]', element);
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        }
    };

    return RegistrationForm;

});
