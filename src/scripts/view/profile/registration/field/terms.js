/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */


define('view/profile/registration/field/terms', [
    'tools/utils',
    'core/selectors',
    'core/classes',
    'events/event'
], function(Utils, Selectors, Classes, AcmsEvent) {

    var Terms = function(element, options) {
        this.$element =
        this.element =
        this.options = null;
        this.initialize(element, options);
    };

    Terms.MODULE = "Nutzungsbedingungen";

    Terms.VERSION = "1.5.0";

    Terms.needInit = true;

    Terms.init = function(element, options) {
        var t = new Terms(element, options);

        return t;
    }

    Terms.prototype = {
        constructor: Terms,

        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },
        
        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {Terms.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return Terms.DEFAULT_OPTIONS;
        },
        /**
         * Bildet die Optionen
         *
         * <p>
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         * </p>
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function (options) {
            return Utils.extend(
                    {},
                    this.getDefaultOptions(),
                    options,
                    this.element.acmsData()
                    );
        },

        listen: function() {
            var self = this, checkboxes = self.checkboxes, j;
            for(j = 0; j < checkboxes.length; j++) {
                AcmsEvent.add(checkboxes[j], 'change', self._onChange);
            }
        },
        
        _onChange: function(e) {
            var self = this,
                target = e.target, j, checkboxes = self.checkboxes, k, chbx;
            k = 0;
            for(j = 0; j < checkboxes.length; j++) {
                chbx = checkboxes[j];
                if(chbx.checked) {
                    k++;
                }
            }
            if(k === j) {
                Classes.addClass(self.element, 'valid');
                Classes.removeClass(self.element, 'invalid');
            } else {
                Classes.addClass(self.element, 'invalid');
                Classes.removeClass(self.element, 'valid');
            }
        },
        
        _prepare: function() {
            var self = this, 
                o = self.options,
                element = self.element;
            self.checkboxes = Selectors.qa('input[type=checkbox]', element);
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        }
        
    }

    return Terms;
    
});