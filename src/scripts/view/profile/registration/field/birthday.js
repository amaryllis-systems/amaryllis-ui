/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */


/**
 * Birthday Validierung
 * 
 * Validiert den Geburtstag bei der Registrierung.
 * 
 * @param {Object} Utils        Das Util Modul
 * @param {Object} Selectors    Das Selectors Modul
 * @param {Object} Classes      Das Classes Modul
 * @param {Object} AcmsEvent    Das AcmsEvent Modul
 * @param {Object} Doc          Das Document Modul
 * @param {Object} A            Das Globale Acms Object
 * 
 * @returns {Birthday}
 */
define('view/profile/registration/field/birthday', [
    'tools/utils',
    'core/selectors',
    'core/classes',
    'events/event',
    'core/document',
    'core/acms'
], function(Utils, Selectors, Classes, AcmsEvent, Doc, A) {

    "use strict";

    /**
     * Constructor
     * 
     * @param {HTMLElement} element     Das Formular-Feld "Geburtstag"
     * @param {Object}      options     Eigene Optionen
     * 
     * @returns {Birthday}
     */
    var Birthday = function(element, options) {
        this.$element =
        this.element =
        this.alert =
        this.options = null;
        this.remote = A.Urls.baseUrl + '/c/validation/action/';
        this.initialize(element, options);
    };
    
    /**
     * Module Name
     * 
     * @var {String}
     */
    Birthday.MODULE = "Validierung Geburtstag";

    /**
     * Module Version
     * 
     * @var {String}
     */
    Birthday.VERSION = "1.5.0";
    
    /**
     * Module Namespace
     * 
     * @var {String}
     */
    Birthday.NS = "acms.view.profile.registration.field.birthday";
    
    /**
     * Einbindung des Module Loaders
     * 
     * @var {Boolean}
     */
    Birthday.needInit = true;
    
    /**
     * Standard-Optionen
     * 
     * @var {String}
     */
    Birthday.DEFAULT_OPTIONS = {
        'remote': A.Urls.baseUrl + '/c/validation/action/'
    };
    
    /**
     * Module Initialisierung
     * 
     * @param {HTMLElement} element     Das Formular-Feld "Geburtstag"
     * @param {Object}      options     Eigene Optionen
     * 
     * @returns {Birthday}
     */
    Birthday.init = function(element, options) {
        var u = new Birthday(element, options);
        u.element.acmsData(Birthday.NS, u);
        
        return u;
    };

    Birthday.prototype = {
        constructor: Birthday,
        
        /**
         * Interner Constructor
         * 
         * @param {HTMLElement} element Das Formular-Feld
         * @param {Object}      options Eigene Optionen
         * 
         */
        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },
        
        /**
         * Gibt die Standard-Optionen zurück
         * 
         * @returns {Birthday.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function() {
            return Birthday.DEFAULT_OPTIONS;
        },
        
        /**
         * Bildet die Optionen
         *
         * <p>
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         * </p>
         *
         * @param {Object}      options     Eigene Optionen
         * 
         * @returns {Birthday.DEFAULT_OPTIONS|Object}
         */
        buildOptions: function(options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },
        
        /**
         * Aufsetzen der Event-Listener
         * 
         * 
         */
        listen: function() {
            var self = this,
                element = this.element;
            AcmsEvent.add(element, 'blur', self._check);
        },

        /**
         * On-Blur-Event Handler
         * 
         * @param {Event} e  Das Event
         * @returns {Boolean}
         */
        _check: function(e) {
            var self = this,
                target = e.target,
                val = target.value,
                fData = new FormData();
            fData.append('value', val);
            fData.append('rule', 'birthday');
            require(['http/request', 'notifier'], function(Request) {
                Request.post(self.remote, fData, self._done, self._fail);
            });
        },
        
        /**
         * Ausführung bei erfolgter Abfrage
         * 
         * @param {Object}      response    Die Antwort vom Server
         * 
         * 
         */
        _done: function(response) {
            var self = this,
                element = this.element,
                grp = Selectors.closest(element, '.form-group');
            if(self.alert) {
                self._removeAlert();
            }
            if(response.status === 'success') {
                Classes.removeClass(grp, 'invalid');
                Classes.addClass(grp, 'valid');
            } else {
                Classes.addClass(grp, 'invalid');
                Classes.removeClass(grp, 'valid');
                require(['ui/components/alert', 'tools/http'], function(Alert, AcmsHttp) {
                    AcmsHttp.loadCSS(A.Urls.baseUrl + '/' + Alert.css);
                    var div = Doc.createElement('div'), 
                        p = Doc.createElement('p'),
                        msg = response.message,
                        id = Utils.generateUniqeId();
                    p.innerHTML = msg;
                    div.id = id;
                    div.setAttribute('id', id);
                    div.appendChild(p);
                    grp.appendChild(div);
                    Classes.addClass(div, 'alert,danger');
                    self.alert = id;
                });
            }
        },
        
        /**
         * Entfernt vorhergehende Alert-Nachrichten
         * 
         * 
         */
        _removeAlert: function() {
            Doc.getElementById(this.alert)
                    .parentNode
                    .removeChild(Doc.getElementById(this.alert));
            this.alert = false;
        },
        
        /**
         * Ausführung bei fehlgeschlagener Abfrage
         * 
         * <p>
         *  Wird ausgeführt, wenn keine Antwort vom Server kommt bzw die 
         *  Anfrage fehlschlägt
         * </p>
         * 
         * @param {Object}      jqXHR    Das XHR Abfrage Object
         * 
         * 
         */
        _fail: function(textStatus) {
            A.Logger.writeLog(textStatus);
        },
        
        /**
         * Vorbereitung des Moduls
         * 
         * 
         */
        _prepare: function() {
            var self = this, 
                o = self.options,
                element = self.element;
            self.remote = o.remote;
            self.button = Selectors.q('button[type=submit]', element);
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        }
    };

    return Birthday;

});
