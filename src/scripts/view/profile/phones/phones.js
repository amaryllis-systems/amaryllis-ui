/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */



define('view/profile/phones/phones',[
    'tools/utils',
    'core/selectors',
    'core/classes',
    'events/event',
    'core/acms'
], function(Utils, Selectors, Classes, AcmsEvent, A) {

    "use strict";
    
    /**
     * Constructor
     * 
     * @param {Element} element     Das HTML Element
     * @param {Object}  options     Eigene Optionen
     * 
     * @returns {Phones}
     */
    var Phones = function(element, options) {
        this.element    =
        this.options    =
        this.remote     =
        this.toggles    = null;

        this.initialize(element, options);
    };
    
    /**
     * Modul Name
     *
     * @var {String}
     */
    Phones.MODULE = "Profile Phones-Action";

    /**
     * Modul Version
     *
     * @var {String}
     */
    Phones.VERSION = "1.5.0";
    
    /**
     * Modul Namespace
     *
     * @var {String}
     */
    Phones.NS = "acms.view.profile.phones.phones";
    
    /**
     * Standard-Optionen
     * 
     * @var {Object}
     */
    Phones.DEFAULT_OPTIONS = {
        remote: null,
        owner: null,
        action: null
    };
    
    /**
     * Einbinden des Modul Loaders
     * 
     * @var {Boolean}
     */
    Phones.needInit = true;
    
    /**
     * Modul Initialisierung
     * 
     * @param {Element} element     Das HTML Element
     * @param {Object}  options     Eigene Optionen
     * 
     * @returns {Phones}
     */
    Phones.init = function (element, options) {
        var l = new Phones(element, options);
        l.element.acmsData(Phones.NS, l);

        return l;
    };

    Phones.prototype = {
        constructor: Phones,
        /**
         * Interner Constructor
         *
         * @param {Element} element
         * @param {Object} options
         *
         * 
         */
        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.submitting = false;
            this.listen();

        },
        
        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {Phones.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return Phones.DEFAULT_OPTIONS;
        },
        /**
         * Bildet die Optionen
         *
         * <p>
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         * </p>
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function (options) {
            return Utils.extend(
                    {},
                    this.getDefaultOptions(),
                    options,
                    this.element.acmsData()
                    );
        },
        
        /**
         * Aufsetzen der Event Listener
         * 
         * 
         */
        listen: function () {
            var self = this,
                toggles = self.toggles, j;
            for(j = 0; j < toggles.length; j++) {
                AcmsEvent.add(toggles[j], 'click', self._onClick);
            }
            AcmsEvent.add(self.button, 'click', self._onSubmit);
        },
        
        _onSubmit: function(e) {
            var self = this,
                o = self.options,
                row = Selectors.closest(e.target, '.row'), select, phone, fData;
            if(self.submitting) {
                return;
            }
            self.submitting = true;
            select = Selectors.q('select', row);
            phone = Selectors.q('input[type="tel"]', row);
            fData = new FormData();
            fData.append('phone', phone.value);
            fData.append('type', select.value);
            fData.append('owner', o.owner);
            fData.append('field', phone.name);
            fData.append('op', 'neu');
            require(['http/request', 'notifier'], function(Request) {
                Request.post(o.remote, fData, self._onSubmitDone)
            });
        },
        
        _onSubmitDone: function(response) {
            this.submitting = false;
            var self = this,
                o = self.options,
                select, phone, fData;
            select = Selectors.q('select', self.element);
            phone = Selectors.q('input[type="tel"]', self.element);
            if(response.status === 'success') {
                var tag = document.createElement('div');
                var container = Selectors.q('div[data-role="profile-phones"][data-type="'+select.value+'"]', self.element);
                if(!container) {
                    var option = select.options[select.selectedIndex];
                    var div = document.createElement('div'),
                        inner = document.createElement('div'),
                        h = document.createElement('h3'),
                        container = document.createElement('div');
                    self.container.appendChild(div);
                    Classes.addClass(div, 'column xxsmall-12 large-6 xlarge-6 xxlarge-6 profile-phones');
                    div.appendChild(inner);
                    Classes.addClass(inner, 'push-10');
                    h.innerHTML = option.getAttribute('data-type');
                    inner.appendChild(h);
                    inner.appendChild(container);
                    Classes.addClass(container, 'tag-bar');
                    container.setAttribute('data-role', 'profile-phones');
                    container.setAttribute('data-type', select.value);
                }
                container.appendChild(tag);
                tag.innerHTML = phone.value;
                Classes.addClass(tag, 'tag,profile-phones-phone');
                phone.value = '';
                AcmsEvent.fireChange(phone);
            } else {
                Acms.Notifications.createFromResponse(response);
            }
        },
        

        /**
         * On-Click-Event Handler
         * 
         * @param {Event} e  Das Event
         * @returns {Boolean}
         */
        _onClick: function(e) {
            var self = this,
                o = self.options, url,
                target = e.target,
                fData = new FormData();
            e.preventDefault();
            if(!Selectors.matches(target, '[data-trigger=action]')) {
                target = Selectors.closest(target, '[data-trigger=action]');
            }
            url = (target.acmsData('remote') 
                    && target.acmsData('remote').isUrl()) 
                        ? target.acmsData('remote') 
                        : o.remote;
            fData.append('id', target.acmsData('id') || o.id);
            fData.append('op', target.acmsData('action'));
            require(['http/request', 'notifier'], function(Request) {
                Request.post(url, fData, self._done, self._fail, self._always);
            });
            return false;
        },
        
        /**
         * Ausführung bei erfolgter Abfrage
         * 
         * <p>
         *  Wird ausgeführt, wenn eine Antwort vom Server kommt
         * </p>
         * 
         * @param {Object}      response    Die Antwort vom Server
         * 
         * 
         */
        _done: function (response) {
            var self = this;
            if(response.status === 'success') {
                self.element.remove();
            }
        },
        /**
         * Ausführung bei fehlgeschlagener Abfrage
         * 
         * <p>
         *  Wird ausgeführt, wenn keine Antwort vom Server kommt bzw die 
         *  Anfrage fehlschlägt
         * </p>
         * 
         * @param {Object}      jqXHR    Das XHR Abfrage Object
         * 
         * 
         */
        _fail: function (jqXHR, textStatus) {
            A.Logger.writeLog(textStatus);
        },
        _always: function (textStatus) {
            A.Logger.writeLog(textStatus);
        },
        
        _prepare: function() {
            var self = this, 
                o = self.options,
                element = self.element;
            self.remote = o.remote;
            self.container = Selectors.q('[data-section="phones"]', element);
            self.toggles = Selectors.qa('[data-trigger=action]', self.container);
            self.button = Selectors.q('[data-role="action-button"]', element);
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        }
        
    };

    return Phones;
});
