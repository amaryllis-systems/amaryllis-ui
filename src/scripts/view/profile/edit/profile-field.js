/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */


define("view/profile/edit/profile-field", [
    'tools/utils',
    'core/selectors',
    'events/event'
], function(Utils, Selectors, AcmsEvent) {

    "use strict";

    var PF_DATA = "acms.profile.edit.profile-field",
        NS = "." + PF_DATA,
        EVENT_UPDATING = "updating" + NS,
        EVENT_UPDATED = "updated" + NS;

    var ProfileField = function(element, options) {
        this.element    =
        this.remote     =
        this.options    = null;
        this.initialize(element, options);
    };

    ProfileField.MODULE = "Profil Feld";

    ProfileField.VERSION = "1.5.0";

    ProfileField.DEFAULT_OPTIONS = {
        fieldtype: null,
        remote: ''
    };

    ProfileField.needInit = true;

    ProfileField.init = function(element, options) {
        var f = new ProfileField(element, options);
        f.element.acmsData(PF_DATA, f);

        return f;
    };

    ProfileField.prototype = {
        constructor: ProfileField,

        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            var o = this.options;
            this.remote = o.remote && o.remote.isUrl() ? o.remote : Acms.Urls.profileEditActionUrl;
            this.listen();
        },
        
        /**
         * Gibt die Standard-Optionen zurück
         * 
         * @returns {ProfileField.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function() {
            return ProfileField.DEFAULT_OPTIONS;
        },

        buildOptions: function(options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },

        listen: function() {
            var self = this, o = this.options || this.getDefaultOptions();
            switch(o.fieldtype) {
                case 'core.emails':
                    break;
                case 'core.phones':
                    break;
                case 'core.websites':
                    break;
                default:
                    this._listenDefault();
                    break;
            }
        },
        
        _onChange: function(e) {
            var target = e.target, val, self = this;
            if(target.type === 'checkbox') {
                var parent = Selectors.closest(target, '.checkbox-options'),
                    checkboxes = Selectors.qa('input[type=checkbox]', parent),
                    checked = [];
                for(var k = 0; k < checkboxes.length; k++) {
                    if(checkboxes[k].checked) {
                        checked.push(checkboxes[k].value);
                    }
                }
                val = checked.join(',');
            } else  {
                val = target.value;
            }
            var done = function(response) {
                if(response.status === "error") {
                    Acms.Notifications.createFromResponse(response);
                    return;
                }
                target.acmsData('value', val);
                var ev = AcmsEvent.createCustom(EVENT_UPDATED, {
                    status: response.status,
                    value: val,
                    field: target.name
                });
                AcmsEvent.dispatch(document.body, ev);
            }, 
            fail = function(jqXHR, textStatus) {
                Acms.Logger.logWarning(textStatus);
            }, fData = new FormData();
            fData.append('value', val);
            fData.append('field', target.getAttribute('name').replace('[]', ''));
            require(['http/request', 'notifier'], function(Request) {
                Request.post(self.remote, fData, done, fail);
            });
        },
        
        _listenDefault: function() {
            var self = this, element = this.element, fields = Selectors.qa('input,select,textarea', element), j, val;
            for(j = 0; j < fields.length; j++) {
                AcmsEvent.add(fields[j], 'change', self._onChange.bind(self));
                if(fields[j].type === 'checkbox') {
                    var parent = Selectors.closest(fields[j], '.checkbox-options');
                    var checkboxes = Selectors.qa('input[type=checkbox]', parent);
                    var checked = [];
                    for(var k = 0; k < checkboxes.length; k++) {
                        if(checkboxes[k].checked) {
                            checked.push(checkboxes[k].value);
                        }
                    }
                    val = checked.join(',');
                } else  {
                    val = fields[j].value;
                }
                fields[j].acmsData('value', val);
            }
        }

    }

    return ProfileField;
});