/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */


/**
 * Hilfs-Tool für die "Passwort vergessen" Seite
 * 
 * @param {Object|Utils}        Utils       Utils Modul
 * @param {Object|Selectors}    Selectors   Selectors Modul
 * @param {Object|AcmsEvent}    AcmsEvent   AcmsEvent Modul
 * @param {Object|Acms}         A           Acms Basis
 * 
 * @returns {ForgotPassword}
 */
define("view/profile/forgot-password", [
    'tools/utils',
    "core/selectors",
    "events/event",
    "core/acms"
], function (Utils, Selectors, AcmsEvent, A) {

    "use strict";

    /**
     * Constructor
     * 
     * @param {Element} element     HTMLElement
     * @param {Object} options      Eigene Optionen
     * 
     * @returns {ForgotPassword}
     */
    var ForgotPassword = function (element, options) {
        this.element =
        this.options =
        this.button = null;
        this.initialize(element, options);
    };

    /**
     * Modul Name
     *
     * @var {String}
     */
    ForgotPassword.MODULE = "ForgotPassword-Formular";

    /**
     * Modul Namespace
     *
     * @var {String}
     */
    ForgotPassword.NS = "acms.view.profile.forgot-password";
    
    /**
     * Standard-Optionen
     * 
     * @var {Object}
     */
    ForgotPassword.DEFAULT_OPTIONS = {
        delay: 6000
    };

    /**
     * Einbinden des Modul Loaders
     * 
     * @var {Boolean}
     */
    ForgotPassword.needInit = true;
    
    /**
     * Modul Initialisierung
     * 
     * @param {Element} element     Das HTML Element
     * @param {Object}  options     Eigene Optionen
     * @returns {ForgotPassword}
     */
    ForgotPassword.init = function (element, options) {
        var l = new ForgotPassword(element, options);
        l.element.acmsData(ForgotPassword.NS, l);

        return l;
    };
    
    /**
     * Klassen-Definitionen
     */
    ForgotPassword.prototype = {
        
        /**
         * @constructor
         */
        constructor: ForgotPassword,
        
        /**
         * Interner Constructor
         *
         * @param {Element} element
         * @param {Object} options
         *
         * 
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this.isInModal = Selectors.closest(element, '.modal');
            this._prepare();
            this.listen();
        },
        
        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {ForgotPassword.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return ForgotPassword.DEFAULT_OPTIONS;
        },
        
        /**
         * Bildet die Optionen
         *
         * <p>
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         * </p>
         *
         * @param {Object} options
         *
         * @returns {Object|ForgotPassword.DEFAULT_OPTIONS} Die gebildeten Optionen
         */
        buildOptions: function (options) {
            return Utils.extend(
                    {},
                    this.getDefaultOptions(),
                    options,
                    this.element.acmsData()
                    );
        },
        
        /**
         * Aufsetzen der Event Listener
         * 
         * 
         */
        listen: function () {
            var self = this;
            AcmsEvent.add(self.button, 'click', self._onClick);
        },
        
        /**
         * On-Click-Event Handler
         * 
         * @param {Event} e  Das Event
         * @returns {Boolean}
         */
        _onClick: function(e) {
            var self = this,
                target = e.target,
                form = Selectors.closest(target, 'form'),
                fData = new FormData(form);
            e.preventDefault();
            require(['http/request', 'notifier'], function(Request) {
                Request.post(self.remote, fData, self._done, self._fail, self._always);
            });
            return false;
        },
        
        /**
         * Ausführung bei erfolgter Abfrage
         * 
         * <p>
         *  Wird ausgeführt, wenn eine Antwort vom Server kommt
         * </p>
         * 
         * @param {Object}      response    Die Antwort vom Server
         * 
         * 
         */
        _done: function (response) {
            var self = this, o = self.options;
            A.Notifications.createFromResponse(response);
            var input = Selectors.q("input[name='acms_redirect']");
            var acms_redirect = input && input.value,
                    redirect = (!acms_redirect) ? A.Urls.baseUrl : acms_redirect;
            if(redirect && redirect.isUrl()) {
                window.location.href = redirect;
            }
        },
        /**
         * Ausführung bei fehlgeschlagener Abfrage
         * 
         * <p>
         *  Wird ausgeführt, wenn keine Antwort vom Server kommt bzw die 
         *  Anfrage fehlschlägt
         * </p>
         * 
         * @param {Object}      jqXHR    Das XHR Abfrage Object
         * 
         * 
         */
        _fail: function (jqXHR, textStatus) {
            A.Logger.writeLog(textStatus);
        },
        
        /**
         * Auführung bei jeder Abfrage
         * 
         * @param {String} textStatus Text Status
         * 
         * 
         */
        _always: function (textStatus) {
            A.Logger.writeLog(textStatus);
        },
        
        /**
         * Vorbereiten des Moduls
         * 
         * 
         */
        _prepare: function() {
            var self = this, 
                o = self.options,
                element = self.element;
            self.remote = o.remote || element.getAttribute('action');
            self.button = Selectors.q('button[type=submit]', element);
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        }
    };

    return ForgotPassword;
});


