/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */


define('view/profile/blog/blogform', [
    "tools/utils",
    'core/selectors',
    "view/form/model-form"
], function(Utils, Selectors, ModelForm) {
    
    "use strict";
    
    /**
     * 
     * @param {HTMLElement} element
     * @param {type} options
     * @returns {blogformL#15.BlogForm}
     */
    var BlogForm = function(element, options) {
        this.$element =
        this.scope =
        this.overlay =
        this.action =
        this.options = null;
        this.initialize(element, options);
    };
    
    BlogForm.MODULE = "Profile Blog Form";
    
    BlogForm.VERSION = "1.5.0";
    
    BlogForm.NS = "acms.view.profile.blog.blogform";
    
    BlogForm.DEFAULT_OPTIONS = {
        
    };
    
    BlogForm.needInit = true;
    
    BlogForm.init = function(element, options) {
        var bf = new BlogForm(element, options);
        
        bf.element.acmsData(BlogForm.NS, bf);
        
        return bf;
    };
    
    BlogForm.prototype = {
        constructor: BlogForm,
        
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            var remote = this.options.remote || element.getAttribute('action');
            this.options.remote = remote;
            var formEle = element.tagName.toLowerCase() === 'form' ? element : Selectors.q('form', element);
            this.form = ModelForm.init(formEle, this.options);
        },
        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {BlogForm.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function() {
            return BlogForm.DEFAULT_OPTIONS;
        },

        /**
         * Bildet die Optionen
         *
         * <p>
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         * </p>
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function(options) {
            return Utils.extend(
                    {},
                    this.getDefaultOptions(),
                    options,
                    this.element.acmsData()
                    );
        }
    };

    return BlogForm;
});
