/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */

/**
 * Circle
 *
 * Ein einzelner Kreis an Freunden
 *
 * @returns {Circle}
 */
define("view/profile/circles/circle", [
    'tools/utils',
    'core/selectors',
    'events/event',
    'core/acms'
], function(Utils, Selectors, AcmsEvent, A) {

    "use strict";

    var CIRCLE_DATA = "acms.view.profile.circles.circle",
        NS = "." + CIRCLE_DATA,
        EVENT_DELETED = "deleted" + NS;

    var Circle = function(element, options) {
        this.element   =
        this.id         =
        this.name       =
        this.friends    = null;
        this.initialize(element, options);
    };
    
    /**
     * Modul Name
     * 
     * @var {String}
     */
    Circle.MODULE = "Benutzer-Kreise";
    
    /**
     * Modul Version
     * 
     * @var {String}
     */
    Circle.VERSION = "1.5.0";
    
    /**
     * Modul Namespace
     * 
     * @var {String}
     */
    Circle.NS = "acms.view.profile.circles.circle";
    
    Circle.EVENT_DELETE = "delete." + Circle.NS;
    
    Circle.EVENT_DELETED = "deleted." + Circle.NS;
    
    /**
     * Module Initialisierung
     * 
     * @param {HTMLElement} element
     * @param {Object} options
     * @returns {Circle}
     */
    Circle.init = function(element, options) {
        var c = new Circle(element, options);
        c.element.acmsData(Circle.NS, c);

        return c;
    };
    
    Circle.DEFAULT_OPTIONS = {
        id: 0,
        circle: '',
        friends: 0
    };

    Circle.prototype = {
        constructor: Circle,
        
        /**
         * Interner Constructor
         * 
         * @param {HTMLElement} element
         * @param {Object} options
         * 
         * 
         */
        initialize: function(element, options) {
            var o;
            this.element = element;
            this.options = o = this.buildOptions(options);
            
            this.id = o.id;
            this.name = o.circle;
            this.friends = o.friends;
        },
        
        /**
         * Gibt die Standard-Optionen zurück
         * 
         * @returns {Circle.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return Circle.DEFAULT_OPTIONS;
        },
        
        /**
         * Bilden der Optionen
         * 
         * @param {Object|null} options
         * @returns {Object}
         */
        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },

        getId: function() {
            return this.id;
        },

        getName: function() {
            return this.name;
        },

        getFriendsCount: function() {
            return this.friends;
        },

        incrementFriend: function(minus) {
            if(minus) {
                this.friends --;
            } else {
                this.friends ++;
            }
        },

        deleteCircle: function (c) {

            var self = this,
                element = self.element,
                all = Selectors.closest(element, '[data-role="all-circles"]'),
                e = AcmsEvent.createCustom(Circle.EVENT_DELETE, {circle: self.getId()}),
                del;
            require(['http/request', 'notifier'], function(r) {
                var url = all.data('remoteactionurl'),
                    id = self.getId(),
                    data = {id: id, op: 'delete'},
                    d = function(response) {
                        return self._removed(response)
                    },
                    f = function(jqXHR, textStatus) {
                        A.Logger.logWarning('Fehler-Meldung beim Löschen: ' + textStatus);
                        A.Notifications.createNotification(
                            A.Translator._('Ups! Da ist was schief gelaufen! Bitte versuchen Sie es später erneut!'),
                            A.Translator._('Fehler'),
                            A.Notifications.TYPE_ERROR
                            );
                    }, a = function() {
                        A.Logger.logWarning('Übermittlung zum Löschen des Kreises abgeschlossen');
                    }
                r.post(url, data, d, f, a);
            });
        },
        
        _removed: function(response) {
            var self = this,
                element = self.element,
                o = self.options;
            A.Notifications.createFromResponse(response);
            if(response.status === 'success') {
                var e = AcmsEvent.createCustom(EVENT_DELETED, {circle: self.getId()});
                AcmsEvent.dispatch(element, e);
                if(typeof o.onDelete === 'function') {
                    o.onDelete(element);
                }
                element.data(CIRCLE_DATA, null);
                element.parentNode.removeChild(element);
            }
        },

        listen: function() {
            var self = this, o = self.options, triggers = Selectors.qa('[data-role="remove-circle"]'), j;
            for(j = 0; j < triggers.length; j++) {
                AcmsEvent.add(triggers[j], 'click', function(e) {
                e.preventDefault();
                var circle = Selectors.closest(e.target, '[data-role=circle]');
                
                });
            }
        }
    };

    return Circle;
});
