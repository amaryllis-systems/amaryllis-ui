/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */


define("view/profile/circles/circlebox", [
    'tools/utils',
    'core/selectors',
    'apps/ui/informations/modal',
    'events/event',
    'http/request',
    'view/profile/friendship/template/friend-template',
    'core/acms'
], function(Utils, Selectors, Modal, AcmsEvent, Request, FriendTemplate, A) {
    
    "use strict";

    var CIRCLEBOX_DATA = "acms.view.profile.circles.circlebox";
        
    
    var CircleBox = function(element, options) {
        this.element    =
        this.$element   =
        this.options    =
        this.$box       =
        this._current   = null;
        this.initialize(element, options);
    };
    
    /**
     * Module Name
     * 
     * @var {String}
     */
    CircleBox.MODULE = "Circle Box";

    /**
     * Module Version
     *
     * @var {String}
     */
    CircleBox.VERSION = "1.5.0";
    
    CircleBox.NS = "acms.view.profile.circles.circlebox";
    
    CircleBox.OPEN_ACCORDION = "acms.ui.navs.accordion.open";

    /**
     * Standard Optionen
     *
     * @var {Object}
     */
    CircleBox.DEFAULT_OPTIONS = {
        remote: "",
        circle: '.accordion-section'
    };

    CircleBox.needInit = true;

    CircleBox.init = function(element, options) {
        var cb = new CircleBox(element, options);
        cb.element.acmsData(CircleBox.NS, cb);
        return cb;
    };

    CircleBox.prototype = {
        
        constructor: CircleBox,
        
        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },

        getDefaultOptions: function() {
            return CircleBox.DEFAULT_OPTIONS;
        },

        buildOptions: function(options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },
        
        listen: function() {
            var self = this,
                sections = self.$sections;
            sections.forEach(function(v, i) {
                AcmsEvent.add(v, CircleBox.OPEN_ACCORDION, self._loadBox);
            });
        },
        
        _loadBox: function(event) {
            event.preventDefault();
            var o = this.options, self = this;
            var target = event.target, circle, id;
            if(target.acmsData('loaded') === true) {
                return false;
            }
            target.acmsData('loaded', true);
            target.setAttribute('data-loaded', true);
            circle = target.acmsData('circle');
            id = parseInt(target.acmsData('id'));
            this._current = target;
            var data = {
                op: (id > 0 ? 'loadmemberships' : 'loadnomembership'),
                id: id
            }, done = function(response) {
                if(response.status === 'success') {
                    self._received(response);
                }
            };
            Request.post(o.remote, data, done, self._fail);
            return false;
        },
        
        _received: function(response)
        {
            var members = response.members, self = this, i;
            var list = this._current.querySelector('ul.collection');
            list.innerHTML = '';
            for(i = 0; i < members.length; i++) {
                var member = members[i], div = document.createElement('div');
                var html = FriendTemplate.render(member);
                div.innerHTML = html;
                while (div.firstChild) {
                    list.appendChild(div.firstChild);
                }
            }
            self._refreshLinks(list);
        },
        
        _refreshLinks: function(list) {
            var self = this, current = this._current;
            var links = Selectors.qa('li', list), i;
            for(i = 0; i < links.length; i++) {
                var link = links[i];
                self._registerClick(link);
            }
        },
        
        _registerClick: function(link) {
            var self = this, o = this.options;
            var target = link.querySelector('[data-toggle="circles"]');
            if(target) {
                var userid = target.getAttribute('data-id'),
                    circles = target.getAttribute('data-circles').split(','),
                    ev = AcmsEvent.create(Modal.EVENT_SHOW), cb = function(event) {
                        var modalbox = self.$modalbox, i,
                            chkbxes = Selectors.qa('input[type=checkbox]', modalbox);
                        for(i =0; i < chkbxes.length; i++) {
                            var chb = chkbxes[i], val = chb.value;
                            chb.acmsData('userid', userid);
                            if(circles.indexOf(val) === -1) {
                                chb.checked = false;
                            } else {
                                chb.checked = true;
                            }
                            AcmsEvent.add(chb, 'change', self._onChange);
                        }
                    };
                var mod = Modal.init(target ,{target: '#changecircle-membership'});
                
                AcmsEvent.add(target, Modal.EVENT_SHOW, cb);
            }
        },
        
        _onChange: function(e) {
            var self = this,
                o = self.options,
                checkbx = e.target,
                checked = checkbx.checked,
                userid = checkbx.acmsData('userid'),
                value = parseInt(checkbx.value),
                fData = new FormData();
            fData.append('op', (checked ? 'addmember' : 'removemember'));
            fData.append('circle_id', value);
            fData.append('friend_id', userid);
            var done = function(response) {
                if(response.status !== 'success') {
                    checkbx.checked = !checked;
                    AcmsEvent.fireChange(checkbx);
                } else {
                    var sections = self.$sections;
                    sections.forEach(function(section) {
                        var scircle = parseInt(section.acmsData('id'));
                        if(scircle === value) {
                            section.acmsData('loaded', false);
                            var badge = Selectors.q('.badge', section);
                            if(badge) {
                                var total = parseInt(badge.textContent);
                                if(checked) {
                                    total += 1;
                                } else {
                                    total-=1;
                                }
                                badge.textContent = total;
                            }
                        }
                    });
                }
            };
            Request.post(o.remote, fData, done, self._fail);
        },
        
        _fail: function(xhr, textStatus) {
            Acms.Logger.logWarning(textStatus);
        },
        
        _toggleModal: function() {
            var self = this;
        },
        
        _prepare: function() {
            var self = this,
                o = self.options,
                element = self.element;
            self.$box = this.element.querySelector('.circlebox');
            self.$sections = Selectors.qa(o.circle, element);
            self.$modalbox = document.querySelector('#changecircle-membership');
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        },
    };

    return CircleBox;
});
