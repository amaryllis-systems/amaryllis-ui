/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */

define('view/profile/circles/circle-changer', [
    'tools/utils',
    'core/classes',
    'core/selectors',
    'apps/ui/informations/modal',
    'http/request'
], function(Utils, Classes, Selectors, Modal, Request) {
    
    "use strict";
    
    var CC_DATA = "acms.view.profile.circles.circle-changer";
    
    var CircleChanger = function(element, options) {
        this.element =
        this.options =
        this.box = null;
        this.initialize(element, options);
    };
    
    CircleChanger.MODULE = "Circle Changer";
    
    CircleChanger.VERSION = "1.5.0";
    
    CircleChanger.DEFAULT_OPTIONS = {
        remote: null
    };
    
    CircleChanger.needInit = true;
    
    CircleChanger.init = function(element, options) {
        var cc = new CircleChanger(element, options);
        cc.element.acmsData(CC_DATA, cc);
        return cc;
    };
    
    CircleChanger.css = Modal.css;
    
    CircleChanger.prototype = {
        constructor: CircleChanger,
        
        initialize: function(element, options) {
            this.element = element;
            this.options
        },
        
        getDefaultoptions: function(options) {
            return CircleChanger.DEFAULT_OPTIONS;
        },
        
        buildOptions: function(options) {
            return Utils.extend({}, this.getDefaultoptions(), options, this.element.acmsData());
        }
    }
});
