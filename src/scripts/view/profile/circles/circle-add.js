/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */


define("view/profile/circles/circle-add", [
	"tools/utils",
	"core/selectors",
	"core/window",
	"events/event",
	"core/acms"
], function(Utils, Selectors, Win, AcmsEvent, A) {
    
	"use strict";
    
    
	/**
     * AddCircle
     * 
     * @param {HTMLElement} element
     * @param {Object} options
     * @returns {AddCircle}
     */
	var AddCircle = function(element, options) {
		this.element = 
        this.remote =
        this.isInModal =
        this.dismiss =
        this.options = null;
		this.initialize(element, options);
	};
    
	/**
     * Komponenten Name
     *
     * @var {String}
     */
	AddCircle.MODULE = "Neuer Profil-Kreis";

	/**
     * Komponenten Version
     *
     * @var {String}
     */
	AddCircle.VERSION = "1.5.0";

	/**
     * Standard Optionen
     *
     * @var {Object}
     */
	AddCircle.DEFAULT_OPTIONS = {
		onAdded: function(response) {}, // Wird nach der Remote-Aktion ausgeführt
		remote: null,
		reload: true // neu laden, wenn abgeschlossen?
	};
    
	AddCircle.NS = "acms.view.profile.circles.circle-add";

	/**
     * Initialisierung
     *
     * @var {Boolean}
     */
	AddCircle.needInit = true;

	/**
     * Module Loader Initialisierung
     *
     * @var {Object}
     */
	AddCircle.init = function(element, options) {
		var f = new AddCircle(element, options);
		f.element.acmsData(AddCircle.NS, options);

		return f;
	};
    
	AddCircle.prototype = {
		constructor: AddCircle,

		/**
         * Initializing the AddCircle
         *
         * Method is called during the constructor of the AddCircle.
         *
         * @param {Object} element The Element to initialize the friend on
         * @param {Object} options Custom Options for the search
         *
         * @returns {AddCircle}
         */
		initialize: function(element, options) {
			this.element = element;
			this.options = this.buildOptions(options);
			this._prepare();
			this.listen();
			A.Logger.writeLog("AddCircle initialisiert");
		},

		/**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {AddCircle.DEFAULT_OPTIONS}
         */
		getDefaultOptions: function() {
			return AddCircle.DEFAULT_OPTIONS;
		},

		/**
         * Bildet die Optionen
         *
         * Basierend auf den Standard-Optionen, den übergebenen Optionen des
         * Constructors und den Element Data-Attributen werden die aktuellen
         * Konfigurationen gebildet. Letztenendes gewinnen die Element
         * Data-Attribute.
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
		buildOptions: function(options) {
			return Utils.extend(
				{},
				this.getDefaultOptions(),
				options,
				this.element.acmsData()
			);
		},

		/**
         * Setter/Getter für die Optionen
         *
         * @param {String|NULL} a   Der Optionen-Schlüssel oder NULL
         * @param {Mixed}       b   Undefined oder der Wert für a
         * @returns {AddCircle.options|Boolean|Mixed|AddCircle.prototype}
         */
		option: function (a, b) {
			if (!a) {
				return this.options;
			}
			if (typeof b === "undefined") {
				return (typeof this.options[a] !== "undefined") ? this.options[a] : false;
			}
			this.options[a] = b;
			return this;
		},

        

		listen: function() {
			var self = this,
				element = self.element,
				btn = Selectors.q("button[type=submit]", element);
			AcmsEvent.add(btn, "click", self._onClick);
			self.dismiss.forEach(function(v) {
				AcmsEvent.add(v, "click", self._onDismiss);
			});
		},
        
		_onClick: function(e) {
			var self = this,
				target = e.target,
				form = Selectors.closest(target, "form"),
				fData = new FormData(form);
			e.preventDefault();
			require(["http/request", "notifier"], function(Request) {
				Request.post(self.remote, fData, self._done, self._fail);
			});
		},

		/*eslint no-unused-vars: ["error", { "args": "none" }]*/
		_onDismiss: function(e) {
			this.element.reset();
		},

		_done: function(response) {
			var self = this, o = this.options;
			if(typeof o.onAdded === "function") {
				o.onAdded(response);
			}
			A.Notifications.createFromResponse(response);
			if(response.status === "success") {
				if(self.isInModal) {
					var dismiss = Selectors.q("[data-dismiss=\"modal\"]", self.element);
					if(dismiss) AcmsEvent.fireClick(dismiss);
				}
				if(o.reload) {
					Win.refresh(500);
				}
			}
		},

		_fail: function(textStatus) {
			A.Logger.logWarning(textStatus);
		},
        
		_prepare: function() {
			var self = this,
				element = self.element,
				o = self.options;
			self.remote = o.remote || element.getAttribute("action");
			self.isInModal = Selectors.closest(element, ".modal");
			self.dismiss = Selectors.qa("[data-dismiss=modal]", element);
			for (var fn in self) {
				if (fn.charAt(0) === "_" && typeof self[fn] === "function") {
					self[fn] = self[fn].bind(self);
				}
			}
		}
	};
    
	return AddCircle;
});
