/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */

define('view/profile/preferences/preferences-form', [
    "http/request"
], function(Request) {
    
    "use strict";
    
    var ConfigsForm = function(element, options) {
        this.element =
        this.options = null;
        this.initialize(element, options);
    };
    
    ConfigsForm.MODULE = "Profile Configs Form";
    
    ConfigsForm.DEFAULT_OPTIONS = {};
    
    ConfigsForm.needInit = true;
    
    ConfigsForm.init = function(element, options) {
        var cf = new ConfigsForm(element, options);
        
        return cf;
    };
    
    ConfigsForm.prototype = {
        constructor: ConfigsForm,
        
        initialize: function(element, options) {
            this.element = element;
            this.options = ConfigsForm.DEFAULT_OPTIONS;
            this.element.onsubmit = function() {
                return false;
            };
        }
    };
    
    return ConfigsForm;
    
    
});
