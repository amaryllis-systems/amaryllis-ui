/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */

define('view/profile/preferences/default-fieldset', [
    "tools/utils",
    "core/selectors",
    "core/classes",
    "events/event",
    "http/request",
    "core/acms",
    "notifier"
], function(Utils, Selectors, Classes, AcmsEvent, Request, A) {
    
    "use strict";
    
    var PF_DATA = "acms.view.profile.preferences.default-fieldset",
        NS = "." + PF_DATA,
        EVENT_UPDATED = "updated" + NS;
    
    var ConfigsFieldset = function(element, options) {
        this.element =
        this.options = null;
        this.initialize(element, options);
    };
    
    ConfigsFieldset.MODULE = "Profile Configs Fieldset";
    
    ConfigsFieldset.DEFAULT_OPTIONS = {
        remote: null
    };
    
    ConfigsFieldset.needInit = true;
    
    ConfigsFieldset.init = function(element, options) {
        var cf = new ConfigsFieldset(element, options);
        cf.element.acmsData(PF_DATA, cf);
        return cf;
    };
    
    ConfigsFieldset.prototype = {
        constructor: ConfigsFieldset,
        
        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this.listen();
        },
        
        getDefaultOptions: function() {
            return ConfigsFieldset.DEFAULT_OPTIONS;
        },
        
        buildOptions: function(options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },
        
        listen: function() {
            var self = this;
            var fields = Selectors.qa('input,select,textarea', this.element), i;
            for(i = 0; i < fields.length; i++) {
                var field = fields[i], cb = function(event) {
                    return self._onChange(event);
                };
                AcmsEvent.add(field, 'change', cb);
            }
        },
        
        _onChange: function(event) {
            
            var el = event.target || event.srcElement, 
                self = this,
                o = this.options,
                val, parent = Selectors.closest(el, 'div'),
                container, checkboxesChecked, checkboxes, i;
            if (el.type && el.type === 'checkbox' && parent && Classes.hasClass(parent, 'switch') && Classes.hasClass(parent, 'toggle-variant')) {
                container = Selectors.closest(el, '.form-group');
                checkboxes = Selectors.qa('input[type=checkbox]', container);
                checkboxesChecked = [];
                for (i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].checked || checkboxes[i] === el && !el.checked) {
                        checkboxesChecked.push(checkboxes[i].value);
                    }
                }
                val = checkboxesChecked.join(',');
            } else if (el.type && el.type === 'checkbox' && parent && Classes.hasClass(parent, 'switch')) {
                val = !el.checked ? 'true' : 'false';
            } else if (el.type && el.type === 'checkbox') {
                container = Selectors.closest(el, '.form-group');
                checkboxes = Selectors.qa('input[type=checkbox]', container);
                if(checkboxes.length === 1) {
                    val = !checkboxes[0].checked ? 'true' : 'false';
                } else {
                    checkboxesChecked = [];
                    for (i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].checked) {
                            checkboxesChecked.push(checkboxes[i].value);
                        }
                    }
                    val = checkboxesChecked.join(',');
                }
            } else {
                val = el.value;
            }
            
            var data = {
                value: val,
                field: el.getAttribute('name').replace('[]', ''),
                op: 'update'
            },
            done = function(response) {
                return self._done(response, data);
            },
            fail = function(jqXHR, textStatus) {
                A.Logger.logWarning(textStatus);
            };
            Request.post(o.remote, data, done, fail);
        },
        
        _done: function(response, data) {
            if(response.status === "error") {
                A.Notifications.createNotification(
                    A.Translator._('Fehler'),
                    response.message,
                    A.Notifications.TYPE_DANGER
                );
            }
            var ev = AcmsEvent.createCustom(EVENT_UPDATED, {
                status: response.status,
                value: data.value,
                field: data.field
            });
            AcmsEvent.dispatch(this.element, ev);
        }
        
    };
    
    return ConfigsFieldset;
    
    
});
