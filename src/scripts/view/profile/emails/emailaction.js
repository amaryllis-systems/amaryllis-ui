/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */

define('view/profile/emails/emailaction',[
    'tools/utils',
    'core/selectors',
    'events/event',
    'core/acms'
], function(Utils, Selectors, AcmsEvent, A) {

    "use strict";

    /**
     * 
     * @param {HTMLElement} element
     * @param {Object} options
     * @returns {EmailsAction}
     */
    var EmailsAction = function(element, options) {
        this.element =
        this.options = null;

        this.initialize(element, options);
    }

    EmailsAction.MODULE = "Profile Emails-Action";

    EmailsAction.VERSION = "1.5.0";
    
    EmailsAction.NS = "acms.view.profile.emails.emailaction";

    EmailsAction.DEFAULT_OPTIONS = {
        remote: null,
        id: null,
        action: null
    };

    EmailsAction.prototype = {
        constructor: EmailsAction,

        initialize: function(element, options) {
            this.element = element;
            this.options = Utils.extend(
                                {},
                                EmailsAction.DEFAULT_OPTIONS,
                                options, this.element.acmsData());
            this.listen();
        },

        listen: function() {
            var self = this,
                o = this.options,
                element = self.element,
                btn = Selectors.q('[data-trigger=action]', element),
                remote = btn.acmsData('remote') || o.remote,
                i,
                cb = function(e) {
                    e.preventDefault();
                    require(['http/request'], function(r) {
                        var fData = new FormData();
                        fData.append('id', btn.acmsData('id'));
                        fData.append('op', btn.acmsData('action'));
                        var d = function(response) {
                                if(response.status === 'success') {
                                    element.parentNode.removeChild(element);
                                }
                            }, f = function(jqXHR, textStatus, errorThrown) {
                                A.Logger.writeLog(textStatus);
                            };

                        r.post(remote, fData, d, f);

                    });
                };
                for(i = 0; i < btn.length; i++) {
                    AcmsEvent.add(btn[i], 'click', cb);
                }
        },

        _addAction: function() {
            var self = this, element = self.element;
            var email = Selectors.q('input[type=email]', element).value,
                type = Selectors.q('select', element).selectedIndex,
                self = this,
                o = this.options
            ;
            if(!email.isEmail()) {
                console.log('Invalide E-Mail');
                return false;
            };
            require(['http/request'], function(r) {
                var url = o.remote,
                    fData = new FormData();
                    fData.append('email', email);
                    fData.append('type', type);
                    fData.append('op', 'add');
                    var d = function(response) {
                        if(response.status === 'success') {
                            self.element.parentNode.removeChild(self.element);
                        }
                    }, f = function(jqXHR, textStatus, errorThrown) {
                        A.Logger.writeLog(textStatus);
                    };

                r.post(url, fData, d, f);

            });
        }
    }

    return EmailsAction;
});
