/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */


define("view/profile/emails/emailsadd", [
    'tools/utils',
    'core/selectors',
    'events/event'
], function(Utils, Selectors, AcmsEvent) {
    
    "use strict";
    
    var EmailAdd = function(element, options) {
        this.element =
        this.element =
        this.options = null;
        this.initialize(element, options);
    };
    
    EmailAdd.MODULE = "Neue Profil E-Mail Adresse";
    
    EmailAdd.VERSION = "1.5.0";
    
    EmailAdd.DEFAULT_OPTIONS = {
        
    };
    
    EmailAdd.needInit = true;
    
    EmailAdd.init = function(element, options) {
        
    }
    
    EmailAdd.prototype = {
        
    }
});