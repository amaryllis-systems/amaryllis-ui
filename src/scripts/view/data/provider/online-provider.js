/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('view/data/provider/online-provider', [
    'http/request',
    'events/event',
    'core/document',
    'tools/utils'
], (Request, AcmsEvent, Doc, Utils) => {
    const OP_READ = "read", OP_READFINE = "readfine", MOD_DATA = "acms.view.data.provider.online-provider", NS = `.${MOD_DATA}`, EVENT_READ = `read${NS}`;

    const Module = {
        MODULE: "Online Handler",
        VERSION: "1.5.0",
        response: null,
        read(op) {
            this.response = null;
            const url = Acms.Urls.onlineCheck,
                  self = this,
                  operation = op || OP_READ,
                  data = {
                      op: OP_READ
                  },
                  done = response => {
                      self.response = response;
                      const ev = AcmsEvent.createCustom(EVENT_READ, {'response': response});
                      const bod = Doc.body;
                      AcmsEvent.dispatch(bod, ev);
                      return response;
                  },
                  fail = (jqXHR, textStatus) => {
                          Acms.Logger && Acms.Logger.logWarning(textStatus);
                          self.response = false;
                          return false;
                      },
                  always = (jqXHR, textStatus) => {
                          Acms.Logger && Acms.Logger.writeLog(textStatus)
                      };
            Request.post(url, data, done, fail, always);
        },
        
    };
});
