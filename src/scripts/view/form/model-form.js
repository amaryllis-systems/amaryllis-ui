/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('view/form/model-form', [
    'tools/utils',
    "tools/string/string-tool",
    'core/classes',
    'core/selectors',
    'events/event',
    'core/acms'
], function(Utils, StringTool, Classes, Selectors, AcmsEvent, A) {

    "use strict";

    var MODELFORM_DATA = "acms.view.form.model-form",
        NS = "." + MODELFORM_DATA,
        EVENT_BEFORE_TRIGGER_SAVE = "before-trigger-save" + NS,
        EVENT_AFTER_TRIGGER_SAVE = "after-trigger-save" + NS,
        EVENT_BEFORE_TRIGGER_SAVE_NEW = "before-trigger-save-new" + NS,
        EVENT_AFTER_TRIGGER_SAVE_NEW = "after-trigger-save-new" + NS,
        EVENT_BEFORE_TRIGGER_SAVE_CLOSE = "before-trigger-save-close" + NS,
        EVENT_AFTER_TRIGGER_SAVE_CLOSE = "after-trigger-save-close" + NS,
        EVENT_BEFORE_TRIGGER_ABORT = "before-trigger-abort" + NS,
        EVENT_AFTER_TRIGGER_ABORT = "after-trigger-abort" + NS,
        EVENT_ACMS_BEFORE_SAVE = "acms.before-save-form"
    ;

    var ModelForm = function(element, options) {
        A.Logger.logDeprecatedModule('view/form/model-form', 'apps/form/model-form', '4.0');
        this.element =
        this.options =
        this.isInModal =
        this.scope = null;
        this.initialize(element, options);
    };

    /**
     * Modul Name
     *
     * @var {String}
     */
    ModelForm.MODULE = "Model-Formular";
    
    /**
     * Modul Namespace
     *
     * @var {String}
     */
    ModelForm.NS = "acms.view.form.model-form";
    
    ModelForm.EVENT_SUCCEED = ModelForm.NS + ".succeed";
    
    ModelForm.EVENT_RESET = ModelForm.NS + ".reset";

    ModelForm.SCOPE_SAVE = "save";

    ModelForm.SCOPE_SAVE_NEW = "save-new";

    ModelForm.SCOPE_SAVE_CLOSE = "save-close";

    ModelForm.SCOPE_ABORT = "abort";

    /**
     * Standard Optionen
     *
     * @var {Object}
     */
    ModelForm.DEFAULT_OPTIONS = {
        beforeSave: function(form) {
            return true;
        },

        afterSave: function(form) {

        },
        
        previousUrl: null,
        remote: null,
        modal: '.modal',
        disabled: 'disabled',
        reload: true,
        key: 'id',
        onSuccess: function(form, self, response) {},
        onAbort: function(form, self) {}
    };

    ModelForm.needInit = true;

    ModelForm.init = function(element, options) {
        var mf = new ModelForm(element, options);
        mf.element.acmsData(MODELFORM_DATA, mf);
        return mf;
    };

    ModelForm.prototype = {

        constructor: ModelForm,
        /**
         * Interner Constructor
         *
         * @param {Object} element Element
         * @param {Object} options Options
         */
        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },

        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {ModelForm.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function() {
            return ModelForm.DEFAULT_OPTIONS;
        },

        /**
         * Bildet die Optionen
         *
         * <p>
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         * </p>
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function(options) {
            return Utils.extend(
                    {},
                    this.getDefaultOptions(),
                    options,
                    this.element.acmsData()
                    );
        },

        listen: function() {
            var self = this,
                element = self.element,
                c = 'click',
                o = self.options;
            var save = element.querySelector('[data-trigger="save"]'),
                saveClose = element.querySelector('[data-trigger="save-close"]'),
                saveNew = element.querySelector('[data-trigger="save-new"]'),
                abort = element.querySelector('[data-trigger="abort"]');
            if(save) {
                AcmsEvent.add(save, c, self._triggerSave);
            }
            if(saveClose) {
                AcmsEvent.add(saveClose, c, self._triggerSaveClose);
            }
            if(saveNew) {
                AcmsEvent.add(saveNew, c, self._triggerSaveNew);
            }
            if(abort) {
                AcmsEvent.add(abort, c, self._triggerAbort);
            }
            if(self.isInModal && self.dismiss.length) {
                self.dismiss.forEach(function(v, i) {
                    AcmsEvent.add(v, 'click', self._resetForm);
                });
            }
        },
        
        _resetForm: function(e) {
            this.element.reset();
            this._triggerEvent(ModelForm.EVENT_RESET, 'reset');
        },

        _triggerSave: function(e) {
            var self = this, 
                o = self.options,
                target = e.target;
            if(target.tagName.toUpperCase() !== 'BUTTON') {
                target = Selectors.closest(target, 'button');
            }
            e.preventDefault();
            e.stopPropagation();
            if(Classes.hasClass(target, o.disabled)) {
                return false;
            }
            self.scope = ModelForm.SCOPE_SAVE;
            self._triggerDefaultBeforeSave();
            var evt = self._triggerEvent(EVENT_BEFORE_TRIGGER_SAVE, 'save');
            if(evt.defaultPrevented) {
                return false;
            }
            if(false === self._beforeSave()) {
                return false;
            }
            self._process(e);
        },

        _triggerSaveClose: function(e) {
            var self = this, 
                o = self.options,
                target = e.target;
            if(target.tagName.toUpperCase() !== 'BUTTON') {
                target = Selectors.closest(target, 'button');
            }
            e.preventDefault();
            e.stopPropagation();
            if(Classes.hasClass(target, o.disabled)) {
                return false;
            }
            self.scope = ModelForm.SCOPE_SAVE_CLOSE;
            self._triggerDefaultBeforeSave();
            var evt = self._triggerEvent(EVENT_BEFORE_TRIGGER_SAVE_CLOSE, 'saveClose');
            if(evt.defaultPrevented) {
                return false;
            }
            if(false === self._beforeSave()) {
                return false;
            }
            self._process(e);
        },

        _triggerSaveNew: function(e) {
            var self = this, 
                o = self.options,
                target = e.target;
            if(target.tagName.toUpperCase() !== 'BUTTON') {
                target = Selectors.closest(target, 'button');
            }
            e.preventDefault();
            e.stopPropagation();
            if(Classes.hasClass(target, o.disabled)) {
                return false;
            }
            self.scope = ModelForm.SCOPE_SAVE_NEW;
            self._triggerDefaultBeforeSave();
            var evt = self._triggerEvent(EVENT_BEFORE_TRIGGER_SAVE_NEW, 'saveNew');
            if(evt.defaultPrevented) {
                return false;
            }
            if(false === self._beforeSave()) {
                return false;
            }
            self._process(e);
            
        },

        _triggerAbort: function(e) {
            var self = this, 
                o = self.options,
                element = self.element,
                target = e.target;
            if(target.tagName.toUpperCase() !== 'BUTTON') {
                target = Selectors.closest(target, 'button');
            }
            e.preventDefault();
            e.stopPropagation();
            self.scope = ModelForm.SCOPE_ABORT;
            var evt = self._triggerEvent(EVENT_BEFORE_TRIGGER_ABORT);
            if(evt.defaultPrevented) {
                return false;
            }
            self.element.reset();
            self._triggerEvent(EVENT_AFTER_TRIGGER_ABORT, 'abort');
            if(this.isInModal) {
                var modal = Selectors.closest(element, '.modal'), dismiss;
                if(modal)
                    dismiss = modal.querySelector('[data-dismiss=modal]');
                if(dismiss && dismiss !== target) AcmsEvent.fireClick(dismiss);
                return false;
            }
            if(typeof o.previousUrl === 'string' && StringTool.isUrl(o.previousUrl)) {
                window.location.href = o.previousUrl;
            } else {
                history.go(-1);
            }
        },
        
        _process: function(e) {
            var self = this, form, fData, remote = self.action;
            if(e) {
                form = (e.target.tagName.toUpperCase() === 'FORM')  ?
                            e.target :
                            Selectors.closest(e.target, 'form');
            } else {
                form = self.element;
            }
            fData = new FormData(form);
            require(["http/request", "notifier"], function(Request) {
                Request.post(remote, fData, self._done, self._fail, self._always);
            });
        },

        _done: function(response) {
            var self = this, 
                scope = self.scope,
                element = self.element,
                o = self.options,
                key = self.keyField;
            A.Notifications.createFromResponse(response);
            if(response.status === 'success') {
                self._triggerEvent(ModelForm.EVENT_SUCCEED, 'succeed', response);
                o.onSuccess(self.element, self, response);
                var red = response.redirect;
                switch(scope) {
                    case ModelForm.SCOPE_SAVE:
                        if(response[self.keyField]) {
                            var id = element.querySelector('input[name='+key+']');
                            if(id) {
                                id.value = response[key];
                                id.setAttribute('value', response[key]);
                                AcmsEvent.fireChange(id);
                            }
                            var modelop = element.querySelector('input[name=modelop]');
                            if(modelop) {
                                modelop.value = 'edit';
                                modelop.setAttribute('value', modelop.value);
                                AcmsEvent.fireChange(modelop);
                            }
                            var newmodel = element.querySelector('input[name=is_new_model]');
                            if(newmodel) {
                                newmodel.value = '0';
                                newmodel.setAttribute('value', newmodel.value);
                                AcmsEvent.fireChange(newmodel);
                            }
                            var files = Selectors.qa('input[type="file"]');
                            if(files) {
                                files.forEach(function(file) {
                                    if(file.files) {
                                        var name = file.name;
                                        file.value = '';
                                        file.value = response.model[name];
                                        console.log(file.value);
                                        AcmsEvent.fireChange(file);
                                    }
                                });
                            }
                        }
                        break;
                    case ModelForm.SCOPE_SAVE_CLOSE:
                        if(self.isInModal && scope !== ModelForm.SCOPE_SAVE) {
                            self.element.reset();
                            var modal = Selectors
                                    .closest(element, '.modal'), dismiss;
                            if(modal) {
                                dismiss = modal.querySelector('[data-dismiss=modal]');
                                if(dismiss) AcmsEvent.fireClick(dismiss);
                                if(o.reload == 'true' || o.reload === true) {
                                    window.refresh(500);
                                }
                                return;
                            }
                        }
                        if(red && red.isUrl()) {
                            window.location.href = response.redirect;
                        } else {
                            history.go(-1);
                        }
                        break;
                    case ModelForm.SCOPE_SAVE_NEW:
                        if(self.isInModal && scope !== ModelForm.SCOPE_SAVE) {
                            self.element.reset();
                        } else if(o.reload == 'true' || o.reload === true) {
                            window.refresh(500);
                        } else {
                            self.element.reset();
                        }
                        break;
                }
            }
        },

        _fail: function(jqXHR, textStatus, x) {
            A.Logger.logWarning(textStatus);
            A.Notifications.create({content: textStatus, title: A.Translator._('Fehler!'), type: A.Notifications.TYPE_DANGER});
        },

        _always: function() {
            switch(this.scope) {
                case ModelForm.SCOPE_SAVE:
                    this._triggerEvent(EVENT_AFTER_TRIGGER_SAVE);
                    break;
                case ModelForm.SCOPE_SAVE_CLOSE:
                    this._triggerEvent(EVENT_AFTER_TRIGGER_SAVE_CLOSE);
                    break;
                case ModelForm.SCOPE_NEW:
                    this._triggerEvent(EVENT_AFTER_TRIGGER_SAVE_NEW);
                    break;
            }
            A.Logger.writeLog('Formular erfolgreich übermittelt');
        },

        _prepare: function() {
            var self = this,
                element = self.element,
                o = self.options,
                fields = Selectors.qa('input,select,textarea', element);
            self.isInModal = Selectors.closest(element, o.modal);
            self.action = (o.remote && o.remote.isUrl()) ? o.remote : element.getAttribute('action');
            self.keyField = o.key;
            
            self.saveButton = Selectors.q('[data-trigger="save"]', element);
            self.saveCloseButton = Selectors.q('[data-trigger="save-close"]', element);
            self.saveNewButton = Selectors.q('[data-trigger="save-new"]', element);
            self.abortButton = Selectors.q('[data-trigger="abort"]', element);
            if(this.isInModal) {
                if(self.abortButton) {
                    self.abortButton.setAttribute('onclick', '');
                }
                self.dismiss = Selectors.qa('[data-dismiss=modal]', element);
            }
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        },

        /**
         * BeforeSave Action
         *
         * @returns {Boolean}
         */
        _beforeSave: function() {
            var o = this.options,
                result = true,
                form = this.element;
            if(typeof o.beforeSave === 'function') {
                result = o.beforeSave(form);
            }
            return result === true;
        },

        /**
         * AfterSave Action
         *
         * @returns {Boolean}
         */
        _afterSave: function() {
            var o = this.options,
                result = true,
                form = this.element;
            if(typeof o.afterSave === 'function') {
                result = o.afterSave(form);
            }
            return result === true;
        },

        _triggerDefaultBeforeSave: function() {
            this._triggerEvent(EVENT_ACMS_BEFORE_SAVE);
        },

        _triggerEvent: function(name, shortName, data) {
            var self = this,
                o = self.options,
                element = self.element,
                opts = {
                    relatedTarget: self.element,
                    scope: self.scope,
                    data: data || false
                };
            if(typeof shortName === 'string') {
                var onName = 'on' + shortName.charAt(0).toUpperCase() + shortName.substr(1);
                if(typeof o[onName] === 'function') {
                    o[onName](element, self);
                }
            }
            var e = AcmsEvent.createCustom(name, opts);
            AcmsEvent.dispatch(element, e);
            return e;
        },
        
    };
    
    return ModelForm;

});
