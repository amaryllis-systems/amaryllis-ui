/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license  https://www.amaryllis-system.de/lizenzen/eula/
 */

define('view/admin/acp-sidebar', [
    'tools/utils',
    'core/selectors',
    'core/classes',
    'ui/components/actionlistgroup',
    'events/event',
    'device/match-media',
    'core/acms'
], function (Utils, Selectors, Classes, ActionListGroup, AcmsEvent, MatchMedia, A) {

    "use strict";

    /**
     * Constructor
     * 
     * @param {Element}     element     Das HTML-Element
     * @param {Object}      options     Die Optionen
     * @returns {AcpMenu}
     */
    var AcpMenu = function (element, options) {
        this.element =
                this.options =
                this.toggles = null;
        this.options = AcpMenu.DEFAULT_OPTIONS;

        this.initialize(element, options);
    };

    /**
     * Modul Name
     * 
     * @var {String}
     */
    AcpMenu.MODULE = "ACP Menu";

    /**
     * Modul Namespace
     * 
     * @var {String}
     */
    AcpMenu.NS = "acms.view.admin.acp-sidebar";

    /**
     * Standard-Optionen des Moduls
     * 
     * @var {Object}
     */
    AcpMenu.DEFAULT_OPTIONS = {

    };

    /**
     * Sagt dem Modul-Loader, dass das Modul initialisiert werden muss, wenn es 
     * über diesen geladen wird.
     * 
     * @var {Boolean}
     */
    AcpMenu.needInit = true;

    /**
     * Modul Initialisierung
     * 
     * @param {Element}     element     Das HTML-Element
     * @param {Object}      options     Die Optionen
     * @returns {AcpMenu}
     */
    AcpMenu.init = function (element, options) {
        var v = new AcpMenu(element, options);
        v.element.acmsData(AcpMenu.NS, v);

        return v;
    };

    /**
     * Klassen Definition
     */
    AcpMenu.prototype = {

        constructor: AcpMenu,

        /**
         * Interner Constructor
         * 
         * @param {Element} element     HMTL Element
         * @param {Objct}   options     Eigene Optionen
         * 
         * 
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },

        /**
         * Aufsetzen der Event-Listener
         * 
         */
        listen: function () {
            var self = this,
                    o = self.options,
                    element = self.element, a;
            self.acpitems.forEach(function(li) {
                a = Selectors.q('a', li);
                if(Classes.hasClass(li, 'mobile-toggle-item')) {
                    AcmsEvent.add(li, 'click', self._onClickMobileToggle);
                } else if(li.acmsData('trigger') == 'back') {
                    AcmsEvent.add(li, 'click', self._onClickBack);
                } else if(a && (a.acmsData('role') != 'modal')) {
                    AcmsEvent.add(a, 'click', self._onClickItem);
                }
            });
            AcmsEvent.add(window, 'resize', self._onResize);
        },

        /**
         * Gibt die Standard-Optionen zurück
         * 
         * @returns {AcpMenu.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return AcpMenu.DEFAULT_OPTIONS;
        },

        /**
         * Bilden der Optionen
         * 
         * Bildet die Optionen basierend auf den Standard-Optionen, den 
         * übergebenen Optionen des Constructors und den Element Data-Attributen. 
         * Letztere gewinnen den Kampf, wenn gesetzt.
         * 
         * @param {Object} options    Die eignenen Optionen
         * @returns {Object|AcpMenu.DEFAULT_OPTIONS}
         */
        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },


        _onResize: function(e) {
            var self = this, 
                element = self.element,
                nav = Selectors.q('.fixed-p-mobile-nav', element), cloned;
            if(!nav) {
                return;
            }
            nav.innerHTML = '';
            if(MatchMedia.matches('only screen and (max-width: 960px)')) {
                self.acpitems.forEach(function(li) {
                    if(Classes.hasClass(li, 'desktop-item')) {
                        cloned = li.cloneNode(true);
                        Classes.removeClass(cloned, 'desktop-item');
                        nav.appendChild(cloned);
                    }
                });
            }
        },

        /**
         * Vorbereiten des Moduls
         * 
         * 
         */
        _prepare: function () {
            var self = this,
                    element = self.element,
                    o = self.options, a;
            self.page = Selectors.q('.page');
            self.acpmenu = Selectors.q('.fixed-p-sidebar .sidebar-nav ul', self.page);
            self.acpitems = Selectors.qa('li', self.acpmenu);
            self._onResize();
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        },
        
        
        
        _onClickMobileToggle: function(e) {
            console.log('clicked');
            console.log(e.target);
            e.preventDefault();
            e.stopPropagation();
            var parent = Selectors.closest(e.target, 'li'), 
                self = this,
                nav = Selectors.q('ul', parent);
            Classes.toggleClass(parent, 'active');
            self.isActive = !self.isActive;
        },
        
        _onClickBack: function(e) {
            e.preventDefault();
            e.stopPropagation();
            var parent = Selectors.closest(e.target, 'ul'),
            parentItem = Selectors.closest(parent, 'li'), a = Selectors.q('a', parentItem);
            if(a) {
                AcmsEvent.fireClick(a);
            }
        },
        
        _onClickItem: function(e) {
            e.preventDefault();
            e.stopPropagation();
            var self = this,
                    element = self.element,
                    o = self.options, target, href, li;
            target = e.target;
            if(target.tagName.toLowerCase() !== 'a') {
                target = Selectors.closest(target, 'a');
            }
            if(!target) {
                return false;
            }
            li = Selectors.closest(target, 'li');
            href = target.getAttribute('href').trim();
            if(href !== '#') {
                window.location.href = href;
                return false;
            }
            if(target.acmsData('module') && target.acmsData('module').test('^/modal/')) {
                return false;
            }
            if(Classes.hasClass(li, 'active')) {
                Classes.removeClass(li, 'active');
                if(Selectors.qa('.active', self.acpmenu).length === 0) {
                    Classes.removeClass(self.page, 'menu-open');
                }
                return false;
            }
            var actives = Selectors.qa('.active', self.acpmenu), j, active;
            var activeParent = Selectors.closest(li, '.active');
            for (j = 0; j < actives.length; j++) {
                active = actives[j];
                if(active !== activeParent) {
                    Classes.removeClass(active, 'active');
                }
            }
            
            Classes.addClass(li, 'active');
            Classes.addClass(self.page, 'menu-open');
            return false;
        }

    };

    return AcpMenu;

});

