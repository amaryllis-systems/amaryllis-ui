/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */

define('view/admin/data/routes', [
    'tools/utils',
    'core/selectors',
    'core/classes',
    'apps/ui/informations/modal',
    'events/event',
    'core/acms'
], function (Utils, Selectors, Classes, Modal, AcmsEvent, A) {

    "use strict";

    /**
     * Constructor
     * 
     * @param {Element}     element     Das HTML-Element
     * @param {Object}      options     Die Optionen
     * @returns {Routes}
     */
    var Routes = function (element, options) {
        this.element =
                this.options =
                this.toggles = null;
        this.options = Routes.DEFAULT_OPTIONS;

        this.initialize(element, options);
    };

    /**
     * Modul Name
     * 
     * @var {String}
     */
    Routes.MODULE = "Benutzerdefinierte Routen";

    /**
     * Modul Version
     * 
     * @var {String}
     */
    Routes.VERSION = "1.5.0";

    /**
     * Modul Namespace
     * 
     * @var {String}
     */
    Routes.NS = "acms.view.admin.data.routes";

    /**
     * Standard-Optionen des Moduls
     * 
     * @var {Object}
     */
    Routes.DEFAULT_OPTIONS = {

    };

    /**
     * Sagt dem Modul-Loader, dass das Modul initialisiert werden muss, wenn es 
     * über diesen geladen wird.
     * 
     * @var {Boolean}
     */
    Routes.needInit = true;

    /**
     * Modul Initialisierung
     * 
     * @param {Element}     element     Das HTML-Element
     * @param {Object}      options     Die Optionen
     * @returns {Routes}
     */
    Routes.init = function (element, options) {
        var v = new Routes(element, options);
        v.element.acmsData(Routes.NS, v);

        return v;
    };

    /**
     * Klassen Definition
     */
    Routes.prototype = {

        constructor: Routes,

        /**
         * Interner Constructor
         * 
         * @param {Element} element     HMTL Element
         * @param {Objct}   options     Eigene Optionen
         * 
         * 
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },

        /**
         * Aufsetzen der Event-Listener
         * 
         */
        listen: function () {
            var self = this,
                    o = self.options,
                    edits = self.editModals,
                    deletes = self.deletes;
            var cb = function(ele, target, overlay) {
                self._onClickDelete(ele);
            }, opts = {onShow: cb, onHide: function(ele, targ, mod) {self._onAbortDelete(ele)}};
            var cb2 = function(ele, target, overlay) {
                self._onClickEdit(ele);
            }, opts2 = {onShow: cb2, onHide: function(ele, targ, mod) {self._onAbortEdit(ele)}};
            deletes.forEach(function(btn) {
               Modal.init(btn, opts);
            });
            
            try {
                AcmsEvent.add(self.deleteBtn, 'click', self._onDelete);
            } catch(E) {
                console.log(E)
            }
        },
        
        /**
         * Gibt die Standard-Optionen zurück
         * 
         * @returns {Routes.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return Routes.DEFAULT_OPTIONS;
        },

        /**
         * Bilden der Optionen
         * 
         * Bildet die Optionen basierend auf den Standard-Optionen, den 
         * übergebenen Optionen des Constructors und den Element Data-Attributen. 
         * Letztere gewinnen den Kampf, wenn gesetzt.
         * 
         * @param {Object} options    Die eignenen Optionen
         * @returns {Object|Routes.DEFAULT_OPTIONS}
         */
        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },
        
        _onClickDelete: function(element) {
            var self = this,
                btn = element, tr = Selectors.closest(btn, 'tr');
            if(null === tr) {
                return false;
            }
            var id = tr.acmsData('id');
            var hidden = Selectors.q('input[name="id"]', self.deleteModal);
            hidden.value = id;
            hidden.setAttribute('value', id);
            self.currentDelete = tr;
        },
        
        _onAbortDelete: function(element) {
            var self = this;
            self.deleteForm.reset();
            self.currentDelete = null;
            self.inProcess = false;
        },
        
        _onAbortEdit: function(element) {
            var self = this;
            self.editForm.reset();
            self.inProcess = false;
        },
        
        
        _onDelete: function(e) {
            e.preventDefault();
            if(this.inProcess) {
                return false;
            }
            this.inProcess = true;
            var f = Selectors.closest(e.target, 'form'),
                    self = this, o = self.options,
                    fData = new FormData(f);
            require(['http/request', 'notifier'], function(Request) {
                Request.post(o.remote, fData, self._onDeleteDone);
            });
            
            return false;
        },
        
        _onDeleteDone: function(response) {
            A.Notifications.createFromResponse(response);
            this.inProcess = false;
            if(response.status === 'success') {
                if(this.currentDelete) {
                    this.currentDelete.parentNode.removeChild(this.currentDelete);
                    this.currentDelete = null;
                }
                var dismiss = Selectors.q('[data-dismiss=modal]', this.deleteModal);
                dismiss && AcmsEvent.fireClick(dismiss);
            }
            
        },
        
        
        _onSave: function(e) {
            e.preventDefault();
            if(this.inProcess) {
                return false;
            }
            this.inProcess = true;
            var f = Selectors.closest(e.target, 'form'),
                    self = this, o = self.options,
                    fData = new FormData(f);
            require(['http/request', 'notifier'], function(Request) {
                Request.post(o.remote, fData, self._onSaveDone)
            });
            
            return false;
        },
        
        _onSaveDone: function(response) {
            A.Notifications.createFromResponse(response);
            this.inProcess = false;
            if(response.status === 'success') {
                var dismiss = Selectors.q('[data-dismiss=modal]', this.editModal);
                dismiss && AcmsEvent.fireClick(dismiss);
                window.refresh(500);
            }
        },
        


        _onClickEdit: function(element) {
            var self = this,
                btn = element, tr = Selectors.closest(btn, 'tr');
            if(null === tr) {
                return false;
            }
            var id = tr.acmsData('id'), fields = new Array();
            var hidden = Selectors.q('input[name="id"]', self.editModal),
                constant = Selectors.q('input[name="constant"]', self.editModal),
                translation = Selectors.q('textarea[name="translation"]', self.editModal),
                locale = Selectors.q('select[name="locale"]', self.editModal),
                mkjs = Selectors.q('input[type=checkbox][name="mkjs"]', self.editModal),
                td = Selectors.q('select[name="textdomain"]', self.editModal); // textdomain und locale wären als radio einfacher
            hidden.value = id;
            hidden.setAttribute('value', id);
            fields.push(hidden)
            constant.value = tr.acmsData('constant');
            fields.push(constant)
            translation.value = tr.acmsData('translation');
            fields.push(translation)
            locale.value = tr.acmsData('language');
            fields.push(locale)
            td.value = tr.acmsData('textdomain');
            fields.push(constant);
            var checked = (parseInt(tr.acmsData('mkjs')) === 1);
            mkjs.checked = checked;
            if(checked) {
                mkjs.setAttribute('checked', 'checked');
            } else {
                mkjs.removeAttribute('checked');
            }
            fields.push(mkjs);
            self.currentEdit = tr;
            fields.forEach(function(field) {
                AcmsEvent.fireChange(field);
            });
        },
        
        /**
         * Vorbereiten des Moduls
         * 
         * 
         */
        _prepare: function () {
            var self = this,
                    element = self.element,
                    o = self.options;
            self.table = Selectors.q('table', element);
            self.deletes = Selectors.qa('[data-trigger=delete]', self.table);
            self.editModals = Selectors.qa('.route-edit-modal');
            self.deleteModal = Selectors.q('#routes-delete-modal');
            self.deleteForm = Selectors.q('form', self.deleteModal);
            self.deleteBtn = Selectors.q('button[type=submit]', self.deleteModal);
            self.deleteAbort = Selectors.q('button[type=reset]', self.deleteModal);
            
            self.edits = Selectors.qa('[data-trigger=edit]', self.table);
            
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        }

    };

    return Routes;

});

