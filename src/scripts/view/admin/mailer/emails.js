/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */


define('view/admin/mailer/emails', [
    'tools/utils',
    'core/selectors'
], function (Utils, Selectors) {

    "use strict";


    var EmailView = function (element, options) {
        this.element =
                this.options = null;

        this.initialize(element, options);
    };

    EmailView.MODULE = "View: E-Mail Administration";

    EmailView.VERSION = "1.5.0";

    EmailView.NS = "acms.view.admin.mailer.emails";

    EmailView.css = "media/css/admin/mailer.min.css";

    EmailView.needInit = true;

    EmailView.init = function (element, options) {
        var view = new EmailView(element, options);
        view.element.acmsData(EmailView.NS, view);

        return view;
    };



    EmailView.prototype = {

        constructor: EmailView,

        initialize: function (element, options) {
            this.element = element;
            var btn = Selectors.q('button[type=reset]', element);
            btn.setAttribute('data-dismiss', 'modal');
            btn.setAttribute('onclick', '');
        }
    };

    return EmailView;

});