/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */


define("view/admin/mailer/emailconnection", [
    'events/event',
    'core/selectors'
], function (AcmsEvent, Selectors) {

    var View = {
        MODULE: 'E-Mail Connection Test',
        VERSION: '1.5.0',
        needInit: true,
        init: function (element, options) {
            this.element = element;
            this.listen();
        },

        listen: function () {
            var self = this;
            AcmsEvent.add(this.element, 'click', function (e) {
                e.preventDefault();
                var $form = Selectors.closest(e.target, 'form');
                return self.testSMTPConnection($form);
            });
        },

        testSMTPConnection: function (form, e) {
            
            var testUrl = Acms.Urls.acmsAdminUrl + "/core/mailer/email/aktion/test/";
            require(['http/request', 'notifier'], function (Request) {
                Request.post(testUrl, new FormData(form), function (response) {
                    if (response.status === 'success') {
                        Acms.Notifications.create({title: 'Erfolgreich', content: response.message});
                    } else {
                        Acms.Notifications.create({title: 'Fehler', content: response.message});
                    }
                }, function (jqXHR, textStatus) {
                    Acms.Notifications.create({title: 'Fehler', content: textStatus, type: Acms.Notifications.TYPE_DANGER});
                });
            });
            return false;
        }
    };

    return View;
});
