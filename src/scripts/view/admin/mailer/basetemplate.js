/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */


define('view/admin/mailer/basetemplate', [
    'tools/utils',
    'core/selectors',
    'core/classes',
    'events/event'
], function (Utils, Selectors, Classes, AcmsEvent) {

    "use strict";

    var View = function (element, options) {
        this.element =
        this.toggles = 
        this.triggers =
        this.options = null;
        this.initialize(element, options);
    };

    View.MODULE = "Email Basis Template";

    View.VERSION = "1.5.0";

    View.NS = "acms.view.admin.mailer.basetemplate";

    View.css = "media/css/admin/mailer.min.css";

    View.needInit = true;

    View.init = function (element, options) {
        var v = new View(element, options);
        v.element.acmsData(View.NS, v);

        return v;
    }

    View.prototype = {
        constructor: View,
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },

        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {View.DEFAULT_OPTIONS|Object}
         */
        getDefaultOptions: function () {
            return View.DEFAULT_OPTIONS;
        },

        /**
         * Bildet die Optionen
         *
         * <p>
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         * </p>
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },

        listen: function () {
            var self = this, 
                o = self.options,
                element = self.element;
            AcmsEvent.on(element, 'click', '[data-action="makeDefault"]', self._onClickMakeDefault);
            AcmsEvent.on(element, 'click', '[data-action="delete"]', self._onClickDelete);
        },
        
        /**
         * 
         * @param {Event} e
         */
        _onClickMakeDefault: function(e) {
            var self = this, 
                o = self.options,
                element = self.element, tr, id, input;
            tr = Selectors.closest(e.target, 'tr');
            id = tr.acmsData('id');
            input = Selectors.q('input[name="id"]', self.toggleModal);
            input.value = id;
            input.setAttribute('value', id);
            AcmsEvent.fireChange(input);
        },
        
        _onClickDelete: function(e) {
            var self = this, 
                o = self.options,
                element = self.element, tr, id, input;
            tr = Selectors.closest(e.target, 'tr');
            console.log(tr)
            id = tr.acmsData('id');
            input = Selectors.q('input[name="id"]', self.deleteModal);
            input.value = id;
            input.setAttribute('value', id);
            AcmsEvent.fireChange(input);
        },
        
        /**
         * Vorbereiten des Moduls
         * 
         * 
         */
        _prepare: function() {
            var self = this, 
                o = self.options,
                element = self.element;
            self.remote = o.remote || element.getAttribute('action');
            self.deleteModal = Selectors.q('#emailtemplate-modal-form-delete');
            self.toggleModal = Selectors.q('#emailtemplate-modal-form-toggle');
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        }
    };

    return View

});
