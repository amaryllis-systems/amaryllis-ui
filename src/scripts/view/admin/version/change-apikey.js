/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */


define('view/admin/version/change-apikey', [
    'core/classes',
    'core/selectors',
    'events/event'
], function (Classes, Selectors, AcmsEvent) {

    "use strict";

    var ApikeyForm = function (element) {
        this.element = null;
        this.initialize(element);
    };
    ApikeyForm.MODULE = "Kunden Feedback";

    ApikeyForm.VERSION = "1.5.0";

    ApikeyForm.needInit = true;

    ApikeyForm.init = function (element) {
        var f = new ApikeyForm(element);
        return f;
    };

    ApikeyForm.prototype = {

        constructor: ApikeyForm,

        initialize: function (element) {
            this.element = element;
            this.listen();
        },

        listen: function () {
            var self = this,
                    element = self.element,
                    btn = element.querySelector('button[type=submit]'),
                    dismiss = Selectors.qa('[data-dismiss=modal]', element), j,
                    res = function () {
                        self.element.reset()
                    }, cb = function (e) {
                e.preventDefault();
                self._sendFeedback(e);
                return false;
            };

            AcmsEvent.add(btn, 'click', cb);
            for (j = 0; j < dismiss.length; j++) {
                AcmsEvent.add(dismiss[j], 'click', res);
            }
        },

        _sendFeedback: function (e) {
            var self = this,
                    target = e.target,
                    form = Selectors.closest(target, 'form'),
                    fData = new FormData(form),
                    action = form.getAttribute('action');
            require(['http/request', 'notifier'], function (Request) {

                Request.post(action, fData, self._done.bind(self));
            });
        },

        _done: function (response) {
            Acms.Notifications.createFromResponse(response);
            var self = this, element = self.element;
            if (response.status === 'success') {
                self.apikey = response.api;
                self.api.value = response.api;
            } else {
                self.api.value = response.apikey;
            }
            AcmsEvent.fireChange(self.api);
        },

        _prepare: function () {
            var self = this,
                    element = self.element,
                    api = Selectors.q('#apikey', element);
            self.api = api;
            self.apikey = api.value;
        }
    };

    return ApikeyForm;

});