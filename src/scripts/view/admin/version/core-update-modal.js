/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */

define('view/admin/version/core-update-modal', [
    'tools/utils',
    'core/classes',
    'core/selectors',
    'events/event',
    'apps/ui/informations/modal'
], function (Utils, Classes, Selectors, AcmsEvent, Modal) {

    "use strict";

    var COREUPDATEMODAL_DATA = "acms.view.admin.version.core-update-modal",
            NS = "." + COREUPDATEMODAL_DATA,
            EVENT_CHECKED = "finished" + NS;

    /**
     * Constructor
     * 
     * @param {Element} element The HTML Element requiring the module
     * @param {Object|NULL} options     Custom options, if used inside another Module. in most cases this will be NULL
     * 
     * @returns {CoreUpdateModal}
     */
    var CoreUpdateModal = function (element, options) {
        this.element =
                this.options =
                this.modal =
                this.lastResult =
                this.updateCheck =
                this.checkRequired = null;
        this.initialize(element, options);
    };

    CoreUpdateModal.EVENT_FINISHED = EVENT_CHECKED;

    /**
     * Using this to define default options for a module.
     * The Default Options might be adjusted on page load within some layouts/pages.
     * Put all your Module options here.
     * 
     * @var {Object}
     */
    CoreUpdateModal.DEFAULT_OPTIONS = {
        updatecheck: null,
        updatetarget: null,
        updateprocess: null
    };

    /**
     * This will tell the module loader, that the Module needs to be initialized.
     * 
     * Setting the "needInit" to false will prevent the loader from executing 
     * anything. Most modules should have turned on. If the Module has "extensions" 
     * or uses multiple files, turn it to off or simple don't set it 
     * in those files since they should not be required within the layout.
     * 
     * @var {Boolean}
     */
    CoreUpdateModal.needInit = true;

    /**
     * Initializing the module
     * 
     * @param {Element} element THE HTML Element
     * @param {Object|NULL} options    Custom Options or NULL
     * 
     * @returns {CoreUpdateModal}   Returning the module
     */
    CoreUpdateModal.init = function (element, options) {
        var cu = new CoreUpdateModal(element, options);
        cu.element.acmsData(COREUPDATEMODAL_DATA, cu);

        return cu;
    };

    /**
     * CSS File
     * 
     * If a module requires to have a custom CSS File you can put it here. 
     * Starting the path from the site-root, since they might be in multiple 
     * places.
     * 
     * The CSS-File will only be loaded if the module is loaded using the default
     * Module Loader. If it is in dependencies in the define-func custom css 
     * files requires to be loaded in the requiring module.
     * 
     * @var {String}
     */
    CoreUpdateModal.css = Modal.css;

    /**
     * Module prototype
     */
    CoreUpdateModal.prototype = {
        constructor: CoreUpdateModal,

        /**
         * Internal Constructor - The module is launched - start using your logic
         * 
         * @param {Element} element The HTML Element requiring the module
         * @param {Object|NULL} options     Custom options, if used inside another Module. in most cases this will be NULL
         * 
         * @returns {CoreUpdateModal}
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },

        listen: function () {
            var self = this,
                    o = self.options,
                    element = self.element,
                    cb = function (e) {
                        var target = self.modal.target;
                        if (false === self.checkRequired
                                && null !== self.lastResult
                                && typeof self.lastResult === 'object') {
                            return;
                        }
                        require(['view/admin/version/core-update'], function (CoreUpdateChecker) {
                            var opts = {
                                remote: o.updatecheck,
                                process: o.updateprocess,
                                target: o.updatetarget || '.main',
                                onFinished: function (cu, data) {
                                    self._processUpdateResult(data);
                                    data.lastCheck = new Date().toJSON();
                                    self.checkRequired = false;
                                    self.lastResult = data;
                                    self._updateCookies(data);
                                    return false;
                                }

                            };
                            self.updateCheck = new CoreUpdateChecker(target, opts);
                        });
                    };
            AcmsEvent.add(element, Modal.EVENT_SHOW, cb);
        },

        /**
         * Build the the final Options of the module
         * 
         * @param {Object|NULL} options
         * @returns {Object}
         */
        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },

        /**
         * Simply returns the default options
         * 
         * @returns {CoreUpdateModal.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return CoreUpdateModal.DEFAULT_OPTIONS;
        },

        _processUpdateResult: function (result) {
            var self = this, target = self.modal.target, icns = Selectors.qa('[data-role=icn]', target), notice = Selectors.q('[data-role=notice]', target), j, icn;
            for (j = 0; j < icns.length; j++) {
                icn = icns[j];
                if (Classes.hasClass(icn, 'acms-icon-stack-1x')) {
                    Classes.removeClass(icn, 'text-success,text-warning,acms-icon-exclamation-triangle,acms-icon-check');
                    if (result.hasUpdate) {
                        Classes.addClass(icn, 'text-warning,acms-icon-exclamation-triangle');
                    } else {
                        Classes.addClass(icn, 'text-success,acms-icon-check');
                    }
                } else {
                    Classes.removeClass(icn, 'text-success,text-warning');
                    if (result.hasUpdate) {
                        Classes.addClass(icn, 'text-warning');
                    } else {
                        Classes.addClass(icn, 'text-success');
                    }
                }
            }
            notice.innerHTML = result.message;
            return false;
        },

        _updateCookies: function (result) {
            require(['http/cookie'], function (HttpCookie) {
                HttpCookie.setCookie('lastUpdateCheck', new Date().toJSON(), 1);
                HttpCookie.setCookie('lastUpdateResult', JSON.stringify(result), 1);
            });
        },

        _prepare: function () {
            var self = this, o = self.options, element = self.element;
            self.modal = Modal.init(element, o);
            this.container = Selectors.q(o.target, element);
            require(['http/cookie'], function (HttpCookie) {
                var last = HttpCookie.readCookie('lastUpdateCheck');
                if (last === false) {
                    self.checkRequired = true;
                } else {
                    try {
                        var res = HttpCookie.readCookie('lastUpdateResult'),
                                result = JSON.parse(res);
                        self.lastResult = result;
                        self._processUpdateResult(result);
                        var d = new Date(last);
                        var now = new Date();
                        var ms = now.getTime() - d.getTime();
                        var min = (ms / 1000 / 60) << 0;
                        if (min > 3600) {
                            self.checkRequired = true;
                        } else {
                            self.checkRequired = false;
                        }
                    } catch (E) {
                        self.checkRequired = true;
                    }
                }

            });
        }
    };

    return CoreUpdateModal;

});
