/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */

define('view/admin/version/core-version', [
    'tools/object/extend',
    'tools/string/sprintf',
    'core/classes',
    'core/selectors',
    'events/event'
], function (Extend, sprintf, Classes, Selectors, AcmsEvent) {

    "use strict";

    var COREUPDATE_DATA = "acms.view.admin.version.core-version",
            NS = "." + COREUPDATE_DATA,
            EVENT_CHECKED = "finished" + NS;

    /**
     * Constructor
     * 
     * @param {Element} element The HTML Element requiring the module
     * @param {Object|NULL} options     Custom options, if used inside another Module. in most cases this will be NULL
     * 
     * @returns {CoreUpdate}
     */
    var CoreUpdate = function (element, options) {
        this.element =
                this.options = null;
        this.initialize(element, options);
    };

    CoreUpdate.EVENT_FINISHED = EVENT_CHECKED;

    /**
     * Using this to define default options for a module.
     * The Default Options might be adjusted on page load within some layouts/pages.
     * Put all your Module options here.
     * 
     * @var {Object}
     */
    CoreUpdate.DEFAULT_OPTIONS = {
        remote: null,
        process: null,
        canUpdate: false,
        target: '#core-version-wrapper',
        onFinished: function (self, data) {
            return true;
        }
    };

    /**
     * This will tell the module loader, that the Module needs to be initialized.
     * 
     * Setting the "needInit" to false will prevent the loader from executing 
     * anything. Most modules should have turned on. If the Module has "extensions" 
     * or uses multiple files, turn it to off or simple don't set it 
     * in those files since they should not be required within the layout.
     * 
     * @var {Boolean}
     */
    CoreUpdate.needInit = true;

    /**
     * Initializing the module
     * 
     * @param {Element} element THE HTML Element
     * @param {Object|NULL} options    Custom Options or NULL
     * 
     * @returns {CoreUpdate}   Returning the module
     */
    CoreUpdate.init = function (element, options) {
        var cu = new CoreUpdate(element, options);
        cu.element.acmsData(COREUPDATE_DATA, cu);

        return cu;
    };

    /**
     * CSS File
     * 
     * If a module requires to have a custom CSS File you can put it here. 
     * Starting the path from the site-root, since they might be in multiple 
     * places.
     * 
     * The CSS-File will only be loaded if the module is loaded using the default
     * Module Loader. If it is in dependencies in the define-func custom css 
     * files requires to be loaded in the requiring module.
     * 
     * @var {String}
     */
    CoreUpdate.css = "media/css/widgets/update-bars.min.css";

    /**
     * Module prototype
     */
    CoreUpdate.prototype = {
        constructor: CoreUpdate,

        /**
         * Internal Constructor - The module is launched - start using your logic
         * 
         * @param {Element} element The HTML Element requiring the module
         * @param {Object|NULL} options     Custom options, if used inside another Module. in most cases this will be NULL
         * 
         * @returns {CoreUpdate}
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this._process();
        },

        /**
         * Build the the final Options of the module
         * 
         * @param {Object|NULL} options
         * @returns {Object}
         */
        buildOptions: function (options) {
            return Extend.flat({}, false, this.getDefaultOptions(), options, this.element.acmsData());
        },

        /**
         * Simply returns the default options
         * 
         * @returns {CoreUpdate.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return CoreUpdate.DEFAULT_OPTIONS;
        },

        _process: function () {
            var self = this,
                    o = self.options,
                    fData = new FormData(),
                    done = function (response) {
                        return self._done(response);
                    };
            fData.append('op', 'checkcore')
            require(['http/request', 'notifier'], function (Request) {
                Request.post(o.remote, fData, done);
            });
        },

        _done: function (response) {
            //console.log(response)
            if (response.status !== 'success') {
                return false;
            }
            var self = this, o = self.options;
            var hasUpdate = response.versionAvailable,
                    type = (hasUpdate ? 'warning' : 'success'),
                    appendTo = self.container || self.element,
                    iconClass = hasUpdate ? 'acms-icon,acms-icon-exclamation-triangle' : 'acms-icon,acms-icon-check',
                    message = hasUpdate ? 
                                sprintf('Amaryllis-CMS v%s ist verfügbar!  ', response.newVersion.version) : 
                                'Die aktuelleste Version ist bereits installiert';
            var params = {
                hasUpdate: hasUpdate,
                type: type,
                iconClass: hasUpdate ? 'acms-icon-exclamation-triangle' : 'acms-icon-success',
                color: hasUpdate ? 'text-warning' : 'text-success',
                message: message
            };
            var ev = AcmsEvent.createCustom(EVENT_CHECKED, params);
            AcmsEvent.dispatch(document.body, ev);
            if (typeof o.onFinished === 'function') {
                var res = o.onFinished(self, params);
                if (true !== res) {
                    return;
                }
            }
            if(!hasUpdate) {
                return;
            }
            var vicn = Selectors.q('.acms-icon-check', self.element);
            var vcircle = Selectors.q('.acms-icon-circle', self.element);
            if(vicn) {
                Classes.removeClass(vicn, 'acms-icon-check,text-success');
                Classes.addClass(vicn, 'acms-icon-exclamation-triangle,text-warning');
            }
            if(vcircle) {
                Classes.removeClass(vcircle, 'text-success');
                Classes.addClass(vcircle, 'text-warning');
            }
            var notice = Selectors.q('[data-role="notice"]', self.element);
            if(notice) {
                notice.innerHTML = message;
                var a = document.createElement('a'), self = this;
                a.href = o.process || '#';
                a.role = 'button';
                a.title = 'Jetzt die Aktualisierung durchführen';
                a.innerHTML = 'Jetzt aktualisieren';
                notice.appendChild(a);
                Classes.addClass(a, 'process-update,button,theme-button,small,right');
            }
        },

        _processUpdate: function (e) {
            e.preventDefault();
            var fData = new FormData(), self = this, o = self.options;
            fData.append('op', 'update');
            
            require(['http/request', 'apps/ui/informations/overlay', 'notifier'], function (Request, Overlay) {
                var o = Overlay.init(document.body);
                o.setLoadingContent();
                o.append();
                Request.post(o.process + 'aktion/update/', fData, self._onFinished.bind(self), function(chr, textStatus) {alert(textStatus)});
            });
            return false;
        },
        
        _onFinished: function(response) {
            Acms.Notifications.createFromResponse(response);
            window.refresh(100);
        },

        _prepare: function () {
            var self = this, o = self.options, element = self.element;
            this.container = Selectors.q(o.target, element);

        }
    };

    return CoreUpdate;

});
