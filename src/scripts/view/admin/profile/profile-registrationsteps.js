/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */

define('view/admin/profile/profile-registrationsteps', [
    'tools/utils',
    'core/selectors',
    'core/classes',
    'events/event',
    'libs/sortable/Sortable'
], function (Utils, Selectors, Classes, AcmsEvent, Sortable) {
    
    "use strict";

    var View = function (element, options) {
        this.element =
                this.options =
                this.sortable = null;
        this.initialize(element, options);
    };

    View.needInit = true;

    View.init = function (element, options) {
        var v = new View(element, options);

        return v;
    };

    View.css = "media/css/admin/profile-categories";

    View.DEFAULT_OPTIONS = {
        css: View.css,
        themecss: false
    };

    View.prototype = {
        constructor: View,

        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();

        },

        getDefaultOptions: function () {
            return View.DEFAULT_OPTIONS;
        },

        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },

        _prepare: function () {
            var self = this,
                    o = self.options,
                    cats = document.querySelector('#available-steps'),
                    fieldGroups = Selectors.qa('.profile-fields'),
                    remote = cats.acmsData('remoteurl'),
                    fieldContainer = Selectors.q('#available-profilefields'),
                    done = function (response) {
                        Acms.Notifications.createFromResponse(response);
                    },
                    remote2 = fieldContainer.acmsData('action'),
                    sortableOptions = {
                        group: 'profile-steps',
                        draggable: '.registration-step',
                        dataIdAttr: 'data-id',
                        onEnd: function (e) {
                            var item = e.item,
                                    ol = Selectors.closest(item, 'ol'),
                                    steps = Selectors.qa('.registration-step', ol),
                                    sort = ol.acmsData('acms.ui.app.sortable'),
                                    arr = sort.toArray(), j;
                            var fData = new FormData();
                            fData.append('sort', JSON.stringify(arr));
                            require(['http/request', 'notifier'], function (Request) {
                                Request.post(remote, fData, done);
                            });
                            for (j = 0; j < arr.length; j++) {
                                var nI = arr[j];
                                var step = steps[j], ul = Selectors.q('ul', step), lis = Selectors.qa('li', ul), k;
                                ul.acmsData('step', nI);
                                ul.setAttribute('data-step', nI);
                                for (k = 0; k < lis.length; k++) {
                                    lis[k].acmsData('step', nI);
                                    lis[k].setAttribute('data-step', nI);
                                }
                            }

                        }
                    },
                    sortableOptions2 = {
                        group: 'profile-field-lists',
                        animation: 150,
                        handle: '.secondary-content',
                        draggable: 'li.sortable-field',
                        dataIdAttr: 'data-fieldid',
                        onEnd: function (e, sortable) {
                            var item = e.item,
                                    ul = Selectors.closest(item, 'ul'),
                                    stepId = ul.acmsData('step'),
                                    index = e.newIndex + 1;
                            item.acmsData('step', stepId);
                            item.acmsData('stepweight', index);
                            var fData = new FormData();
                            fData.append('step_id', stepId);
                            fData.append('step_position', index);
                            fData.append('field_id', item.acmsData('fieldid'));
                            require(['http/request', 'notifier'], function (Request) {
                                Request.post(remote2, fData, done);
                            });
                        }
                    }, j;
            this.sortable = Sortable.create(cats, sortableOptions);
            var sortables = [];

            for (j = 0; j < fieldGroups.length; j++) {
                var sort = Sortable.create(fieldGroups[j], sortableOptions2);
                sortables.push(sort);
            }
        }
    };

    return View;

});