/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */

define('view/admin/profile/profile-categories', [
    'tools/utils',
    'core/selectors',
    'core/classes',
    'events/event',
    'libs/sortable/Sortable'
], function (Utils, Selectors, Classes, AcmsEvent, Sortable) {
    
    "use strict";

    var View = function (element, options) {
        this.element =
                this.options =
                this.sortable = null;
        this.initialize(element, options);
    };

    View.needInit = true;

    View.init = function (element, options) {
        var v = new View(element, options);

        return v;
    };

    View.css = "media/css/admin/profile-categories";

    View.DEFAULT_OPTIONS = {
        css: View.css,
        themecss: false
    };

    View.prototype = {
        constructor: View,

        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
        },

        getDefaultOptions: function () {
            return View.DEFAULT_OPTIONS;
        },

        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },
        
        listen: function() {
            var self = this,
                dd = self.dropdowns,
                cb = function(e) {
                    var target = e.target, uri;
                    console.log(e);
                    if(target.tagName.toLowerCase() !== 'a') {
                        target = Selectors.closest(target, 'a');
                    }
                    uri = target.getAttribute('href');
                    
                    window.location.href = uri;
                };
            dd.forEach(function(anch) {
                AcmsEvent.add(anch, 'click', cb);
            });
        },

        _prepare: function () {
            var self = this,
                    o = self.options,
                    element = self.element,
                    cats = document.querySelector('#available-categories'),
                    fieldGroups = Selectors.qa('.profile-fields'),
                    remote = cats.acmsData('remoteurl'),
                    fieldContainer = Selectors.q('#available-profilefields'),
                    done = function (response) {
                        Acms.Notifications.createFromResponse(response);
                    },
                    remote2 = fieldContainer.acmsData('action'),
                    sortableOptions = {
                        group: 'profile-categories',
                        draggable: 'li.sortable-category',
                        dataIdAttr: 'data-id',
                        ignore: 'img,a',
                        onEnd: function (e) {
                            var item = e.item,
                                    ol = Selectors.closest(item, 'ol'),
                                    cats = Selectors.qa('.sortable-category', ol),
                                    sort = ol.acmsData('acms.ui.app.sortable'),
                                    arr = sort.toArray(), j;

                            var fData = new FormData();
                            fData.append('sort', JSON.stringify(arr));
                            require(['http/request', 'notifier'], function (Request) {
                                Request.post(remote, fData, done);
                            });
                            for (j = 0; j < arr.length; j++) {
                                var nI = arr[j];
                                var cat = cats[j], ul = Selectors.q('ul', cat), lis = Selectors.qa('li', ul), k;
                                ul.acmsData('cat', nI);
                                ul.setAttribute('data-cat', nI);
                                for (k = 0; k < lis.length; k++) {
                                    lis[k].acmsData('cat', nI);
                                    lis[k].setAttribute('data-cat', nI);
                                }
                            }

                        }
                    },
                    sortableOptions2 = {
                        group: 'profile-field-lists',
                        animationDelay: 150,
                        handle: false,
                        draggable: '.sortable-field',
                        dataIdAttr: 'data-fieldid',
                        ignore: 'img',
                        sort: true,
                        onEnd: function (e, sortable) {
                            var item = e.item,
                                    ul = Selectors.closest(item, 'ul'),
                                    catId = ul.acmsData('cat'),
                                    index = e.newIndex + 1;
                            console.log(index)
                            item.acmsData('cat', catId);
                            item.acmsData('catweight', index);
                            var fData = new FormData();
                            fData.append('cat_id', catId);
                            fData.append('cat_position', index);
                            fData.append('field_id', item.acmsData('fieldid'));
                            require(['http/request', 'notifier'], function (Request) {
                                Request.post(remote2, fData, done);
                            });
                        }
                    }, j;
            this.sortable = Sortable.create(cats, sortableOptions);
            var sortables = [];
            for (j = 0; j < fieldGroups.length; j++) {
                var sort = Sortable.create(fieldGroups[j], sortableOptions2);
                fieldGroups[j].draggable = true;
                sortables.push(sort);
            }
            this.sortableFieldGroups = sortables;
        },
        
    };

    return View;

});