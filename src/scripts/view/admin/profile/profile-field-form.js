/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define('view/admin/profile/profile-field-form', [
    'tools/utils'
], function (Utils) {

    "use strict";

    var ProfileForm = function (element, options) {
        this.element =
        this.options =
        this.originalUrl = null;

        this.initialize(element, options);
    };

    ProfileForm.MODULE = "Profile Field Form";

    ProfileForm.NS = "acms.view.admin.profile.profile-field-form";

    ProfileForm.DEFAULT_OPTIONS = {
    };

    ProfileForm.needInit = true;

    ProfileForm.init = function (element, options) {
        var f = new ProfileForm(element, options);
        f.element.acmsData(ProfileForm.NS, f);

        return f;
    };

    ProfileForm.prototype = {
        constructor: ProfileForm,

        initialize: function (element, options) {
            this.element = element;
            this.options = options;
            this.originalUrl = element.getAttribute('action');
        },

        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {ProfileForm.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return ProfileForm.DEFAULT_OPTIONS;
        },

        /**
         * Bildet die Optionen
         *
         * <p>
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         * </p>
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function (options) {
            return Utils.extend(
                    {},
                    this.getDefaultOptions(),
                    options,
                    this.element.acmsData()
                    );
        }
    };

    return ProfileForm;

});
