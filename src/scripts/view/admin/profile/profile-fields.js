/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("view/admin/profile/profile-fields", [
    'tools/utils',
    "core/selectors",
    "events/event",
    "core/acms"
], function (Utils, Selectors, AcmsEvent, A) {

    "use strict";

    /**
     * Constructor
     * 
     * @param {Element} element
     * @param {Object} options
     * 
     * @returns {ProfileFields}
     */
    var ProfileFields = function (element, options) {
        this.element =
        this.options =
        this.button = null;
        this.initialize(element, options);
    };

    /**
     * Modul Name
     *
     * @var {String}
     */
    ProfileFields.MODULE = "Profil Feld Administration";

    /**
     * Modul Namespace
     *
     * @var {String}
     */
    ProfileFields.NS = "view/admin/profile/profile-fields";
    
    /**
     * Standard-Optionen
     * 
     * @var {Object}
     */
    ProfileFields.DEFAULT_OPTIONS = {
        delay: 6000
    };

    /**
     * Einbinden des Modul Loaders
     * 
     * @var {Boolean}
     */
    ProfileFields.needInit = true;
    
    /**
     * Modul Initialisierung
     * 
     * @param {Element} element     Das HTML Element
     * @param {Object}  options     Eigene Optionen
     * @returns {ProfileFields}
     */
    ProfileFields.init = function (element, options) {
        var l = new ProfileFields(element, options);
        l.element.acmsData(ProfileFields.NS, l);

        return l;
    };

    ProfileFields.prototype = {
        constructor: ProfileFields,
        /**
         * Interner Constructor
         *
         * @param {Element} element
         * @param {Object} options
         *
         * 
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this.isInModal = Selectors.closest(element, '.modal');
            this._prepare();
            this.listen();
        },
        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {ProfileFields.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return ProfileFields.DEFAULT_OPTIONS;
        },
        /**
         * Bildet die Optionen
         *
         * <p>
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         * </p>
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function (options) {
            return Utils.extend(
                    {},
                    this.getDefaultOptions(),
                    options,
                    this.element.acmsData()
                    );
        },
        
        /**
         * Aufsetzen der Event Listener
         * 
         * 
         */
        listen: function () {
            var self = this, toggles = self.toggles, j;
            for(j = 0; j < toggles.length; j++) {
                AcmsEvent.add(toggles[j], 'click', self._onClickDisabled);
            }
        },
        
        /**
         * On-Click-Event Handler
         * 
         * @param {Event} e  Das Event
         * @returns {Boolean}
         */
        _onClickDisabled: function(e) {
            var self = this,
                target = e.target;
                /*form = Selectors.closest(target, 'form'),
                fData = new FormData(form)*/
            e.preventDefault();
            e.stopPropagation();
            return false;
            /**
            require(['http/request', 'notifier'], function(Request) {
                Request.post(self.remote, fData, self._done, self._fail, self._always);
            });
            return false;
            */
        },
        
        /**
         * Ausführung bei erfolgter Abfrage
         * 
         * <p>
         *  Wird ausgeführt, wenn eine Antwort vom Server kommt
         * </p>
         * 
         * @param {Object}      response    Die Antwort vom Server
         * 
         * 
         */
        _done: function (response) {
            var self = this, o = self.options;
            A.Notifications.createFromResponse(response);
            var input = Selectors.q("input[name='acms_redirect']");
            var acms_redirect = input && input.value,
                    redirect = (!acms_redirect) ? A.Urls.baseUrl : acms_redirect;
            if(redirect && redirect.isUrl()) {
                window.location.href = redirect;
            }
        },
        /**
         * Ausführung bei fehlgeschlagener Abfrage
         * 
         * Wird ausgeführt, wenn keine Antwort vom Server kommt bzw die 
         * Anfrage fehlschlägt
         * 
         * @param {Object}      jqXHR    Das XHR Abfrage Object
         * 
         * 
         */
        _fail: function (jqXHR, textStatus) {
            A.Logger.writeLog(textStatus);
        },
        _always: function (textStatus) {
            A.Logger.writeLog(textStatus);
        },
        
        _prepare: function() {
            var self = this, 
                o = self.options,
                element = self.element;
            self.remote = o.remote || element.getAttribute('action');
            //self.button = Selectors.q('button[type=submit]', element);
            self.toggles = Selectors.qa('.disabled', element);
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        }
    };

    return ProfileFields;
    
});
