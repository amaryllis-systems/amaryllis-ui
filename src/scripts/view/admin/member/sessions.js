/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */

define('view/admin/member/sessions', [
    'tools/utils',
    'core/selectors',
    'core/classes',
    'ui/components/actionlistgroup',
    'events/event',
    'core/acms'
], function (Utils, Selectors, Classes, ActionListGroup, AcmsEvent, A) {

    "use strict";

    /**
     * Constructor
     * 
     * @param {Element}     element     Das HTML-Element
     * @param {Object}      options     Die Optionen
     * @returns {Table}
     */
    var Table = function (element, options) {
        this.element =
                this.options =
                this.toggles = null;
        this.options = Table.DEFAULT_OPTIONS;

        this.initialize(element, options);
    };

    /**
     * Modul Name
     * 
     * @var {String}
     */
    Table.MODULE = "Profil-Sichtbarkeiten";

    /**
     * Modul Version
     * 
     * @var {String}
     */
    Table.VERSION = "1.5.0";

    /**
     * Modul Namespace
     * 
     * @var {String}
     */
    Table.NS = "acms.view.admin.member.sessions";

    /**
     * Standard-Optionen des Moduls
     * 
     * @var {Object}
     */
    Table.DEFAULT_OPTIONS = {

    };

    /**
     * Sagt dem Modul-Loader, dass das Modul initialisiert werden muss, wenn es 
     * über diesen geladen wird.
     * 
     * @var {Boolean}
     */
    Table.needInit = true;

    /**
     * Modul Initialisierung
     * 
     * @param {Element}     element     Das HTML-Element
     * @param {Object}      options     Die Optionen
     * @returns {Table}
     */
    Table.init = function (element, options) {
        var v = new Table(element, options);
        v.element.acmsData(Table.NS, v);

        return v;
    };

    /**
     * Klassen Definition
     */
    Table.prototype = {

        constructor: Table,

        /**
         * Interner Constructor
         * 
         * @param {Element} element     HMTL Element
         * @param {Objct}   options     Eigene Optionen
         * 
         * 
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },

        /**
         * Aufsetzen der Event-Listener
         * 
         */
        listen: function () {
            var self = this,
                    o = self.options,
                    element = self.element;
            self.toggles.forEach(function(toggle) {
                AcmsEvent.add(toggle, 'click', self._onClick.bind(self));
            });
        },

        /**
         * Gibt die Standard-Optionen zurück
         * 
         * @returns {Table.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return Table.DEFAULT_OPTIONS;
        },

        /**
         * Bilden der Optionen
         * 
         * Bildet die Optionen basierend auf den Standard-Optionen, den 
         * übergebenen Optionen des Constructors und den Element Data-Attributen. 
         * Letztere gewinnen den Kampf, wenn gesetzt.
         * 
         * @param {Object} options    Die eignenen Optionen
         * @returns {Object|Table.DEFAULT_OPTIONS}
         */
        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },
        
        _onClick: function(e) {
            var self = this, target = e.target;
            if(target.tagName.toLowerCase() !== 'button') {
                target = Selectors.closest(target, 'button');
            }
            self.lastBtn = target;
            self.last = Selectors.closest(target, 'tr');
            if(target && target.acmsData('remote')) {
                require(['http/request', 'notifier'], function(Request) {
                    Request.post(target.acmsData('remote'), new FormData(), self._done.bind(self));
                });
            }
        },
        
        _done: function(response) {
            Acms.Notifications.createFromResponse(response);
            if(response.status === 'success') {
                if(this.last) {
                    this.last.parentNode.removeChild(this.last);
                }
            }
        },

        /**
         * Vorbereiten des Moduls
         * 
         * 
         */
        _prepare: function () {
            var self = this,
                    element = self.element,
                    o = self.options;
            self.toggles = Selectors.qa('[data-trigger="delete"]');
        }

    };

    return Table;

});

