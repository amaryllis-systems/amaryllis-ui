/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */


define('view/admin/member/roles', [
    'tools/utils',
    'core/selectors',
    'core/classes',
    'ui/components/actionlistgroup',
    'events/event',
    'core/acms'
], function (Utils, Selectors, Classes, ActionListGroup, AcmsEvent, A) {

    "use strict";

    var View = function (element, options) {
        this.element =
                this.options =
                this.container =
                this.actionGroups =
                this.groups = null;
        this.initialize(element, options);
    };

    View.MODULE = 'View: Rollen-Management';

    View.VERSION = '1.5.0';

    View.DEFAULt_OPTIONS = {
        remote: null,
        container: "#roles-view",
        groups: '.action-collection',
        items: '.collection-item',
        ignore: '.collection-item-disabled'
    };

    View.needInit = true;

    View.init = function (element, options) {
        var v = new View(element, options);

        return v;
    };

    View.prototype = {

        constructor: View,

        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },

        listen: function () {
            var self = this,
                    o = self.options,
                    groups = self.groups, j, k, group, actionGroup, items, item,
                    cb = function (e) {
                        return self._onClick(e);
                    };

            for (j = 0; j < groups.length; j++) {
                group = groups[j];
                actionGroup = ActionListGroup.init(group);
                self.actionGroups.push(actionGroup);
                items = Selectors.qa(o.items, group);
                for (k = 0; k < items.length; k++) {
                    item = items[k];
                    item.acmsData('index', k);
                    if (Classes.hasClass(item, o.ignore)) {
                        continue;
                    }
                    AcmsEvent.add(item, 'click', cb);
                }
            }
        },

        getDefaultOptions: function () {
            return View.DEFAULt_OPTIONS;
        },

        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },

        _onClick: function (e) {
            var self = this,
                    o = self.options,
                    item = e.target,
                    fData = new FormData();
            if (!Selectors.matches(item, o.items)) {
                item = Selectors.closest(item, o.items);
            }

            var div = Selectors.closest(item, '.action-collection'),
                    groupID = item.acmsData('groupid'),
                    roleID = div.acmsData('role'),
                    type = div.acmsData('type'),
                    isActive = Classes.hasClass(item, 'active');
            fData.append('op', 'toggle');
            fData.append('group_id', groupID);
            fData.append('role_id', roleID);
            fData.append('type', type);
            fData.append('state', isActive);
            var done = function (response) {
                if ('error' === response.status) {
                    A.Notifications.createFromResponse(response);
                    if (isActive) {
                        Classes.removeClass(item, 'active');
                    } else {
                        Classes.addClass(item, 'active');
                    }
                }
            }, fail = function (jqXHR, textStatus) {
                if (isActive) {
                    Classes.removeClass(item, 'active');
                } else {
                    Classes.addClass(item, 'active');
                }
                A.Logger.logWarning(textStatus);

            };
            require(['http/request', 'notifier'], function (Request) {
                Request.post(o.remote, fData, done, fail);
            });
        },

        _prepare: function () {
            var self = this,
                    element = self.element,
                    o = self.options,
                    container = Selectors.q(o.container, element);
            self.container = Selectors.q(o.container, element);
            self.groups = Selectors.qa(o.groups, container);
            self.actionGroups = [];
        }
    };

    return View;

});
