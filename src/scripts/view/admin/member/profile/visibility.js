/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */

define('view/admin/member/profile/visibility', [
    'tools/utils',
    'core/selectors',
    'core/classes',
    'ui/components/actionlistgroup',
    'events/event',
    'core/acms'
], function(Utils, Selectors, Classes, ActionListGroup, AcmsEvent, A) {
    
    "use strict";
    
    /**
     * Constructor
     * 
     * @param {Element}     element     Das HTML-Element
     * @param {Object}      options     Die Optionen
     * @returns {Vis}
     */
    var Vis = function(element, options) {
        this.element =
        this.options =
        this.groups =
        this.actionGroups =
        
        this.toggles = null;
        this.options = Vis.DEFAULT_OPTIONS;
        
        this.initialize(element, options);
    };
    
    /**
     * Modul Name
     * 
     * @var {String}
     */
    Vis.MODULE = "Profil-Sichtbarkeiten";
    
    /**
     * Modul Version
     * 
     * @var {String}
     */
    Vis.VERSION = "1.5.0";
    
    /**
     * Modul Namespace
     * 
     * @var {String}
     */
    Vis.NS = "acms.view.admin.member.profile.visibility";
    
    Vis.css = "media/css/admin/profile-visibility";
    
    /**
     * Standard-Optionen des Moduls
     * 
     * @var {Object}
     */
    Vis.DEFAULT_OPTIONS = {
        ugroup: null,
        remote: null,
        fieldid: null,
        groups: '.action-collection',
        itemclass: '.collection-item',
        ignoreclass: 'disabled-collection-item',
        iconchecked: 'acms-icon acms-icon-check-square',
        iconunchecked: 'acms-icon acms-icon-square',
        css: Vis.css,
        themecss: false
    };
    
    /**
     * Sagt dem Modul-Loader, dass das Modul initialisiert werden muss, wenn es 
     * über diesen geladen wird.
     * 
     * @var {Boolean}
     */
    Vis.needInit = true;
    
    /**
     * Modul Initialisierung
     * 
     * @param {Element}     element     Das HTML-Element
     * @param {Object}      options     Die Optionen
     * @returns {Vis}
     */
    Vis.init = function(element, options) {
        var v = new Vis(element, options);
        v.element.acmsData(Vis.NS, v);
        
        return v;
    };
    
    /**
     * Klassen Definition
     */
    Vis.prototype = {
        
        constructor: Vis,
        
        /**
         * Interner Constructor
         * 
         * @param {Element} element     HMTL Element
         * @param {Objct}   options     Eigene Optionen
         * 
         * 
         */
        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },
        
        /**
         * Aufsetzen der Event-Listener
         * 
         */
        listen: function() {
            var self = this,
                o = self.options,
                element = self.element,
                groups = self.groups, j, group, actionGroup, items, k, item, agOptions;
            agOptions = {
                    itemclass: o.itemclass,
                    ignoreclass: o.ignoreclass,
                    iconchecked: o.iconchecked,
                    iconunchecked: o.iconunchecked,
                    onChange: function(item, checkbox, checked) {
                        var fData = self._buildFormData(item, checkbox);
                        self._process(fData);
                        return true;
                    }
                };
            for (j = 0; j < groups.length; j++) {
                group = groups[j];
                actionGroup = ActionListGroup.init(group, agOptions);
                self.actionGroups.push(actionGroup);
                items = Selectors.qa(o.itemclass, group);
                for (k = 0; k < items.length; k++) {
                    item = items[k];
                    item.acmsData('index', k);
                }
            }
        },
        
        /**
         * Gibt die Standard-Optionen zurück
         * 
         * @returns {Vis.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function() {
            return Vis.DEFAULT_OPTIONS;
        },
        
        /**
         * Bilden der Optionen
         * 
         * Bildet die Optionen basierend auf den Standard-Optionen, den 
         * übergebenen Optionen des Constructors und den Element Data-Attributen. 
         * Letztere gewinnen den Kampf, wenn gesetzt.
         * 
         * @param {Object} options    Die eignenen Optionen
         * @returns {Object|Vis.DEFAULT_OPTIONS}
         */
        buildOptions: function(options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },
        
        /**
         * Bilden der Formular-Daten
         * 
         * @param {Element} item   Das HTML-Element der ActionGroup
         * @param {Element} checkbox   Das Checkbox-Element der ActionGroup
         * 
         * @returns {FormData}
         */
        _buildFormData: function(item, checkbox) {
            var self = this, o = this.options, fData = new FormData();
            fData.append('op', 'update');
            fData.append('fieldid', o.fieldid);
            fData.append('vistype', parseInt(item.acmsData('vistype')));
            fData.append('user_group', parseInt(item.acmsData('ugroup')));
            fData.append('profile_group', parseInt(item.acmsData('pgroup')) || 0);
            fData.append('state', checkbox.checked);
            
            return fData;
        },
        
        _process: function(fData) {
            var self = this, o = self.options;
            require(['http/request', 'notifier'], function(Request) {
                Request.post(o.remote, fData, self._onDone.bind(self), self._onFail.bind(self));
            });
        },
        
        _onDone: function(response) {
            if(response.status !== 'success') {
                A.Notifications.createFromResponse(response);
            }
        },
        
        
        _onFail: function(xhr, textStatus) {
            A.Logger.writeLog(textStatus);
        },
        
        /**
         * Vorbereiten des Moduls
         * 
         * 
         */
        _prepare: function() {
            var self = this,
                element = self.element,
                o = self.options;
            self.groups = Selectors.qa(o.groups, element);
            self.actionGroups = [];
        }
        
    };
    
    return Vis;
    
});
