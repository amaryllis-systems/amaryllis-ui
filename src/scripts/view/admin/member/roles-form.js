/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */


define("view/admin/member/roles-form", [
    'tools/utils',
    'core/selectors',
    'events/event',
    'core/window',
    'core/acms'
], function (Utils, Selectors, AcmsEvent, Win, A) {

    "use strict";

    var RolesForm = function (element, options) {
        this.element =
                this.options = null;
        this.initialize(element, options);
    };

    RolesForm.MODULE = "Formular Benutzerrollen";

    RolesForm.VERSION = "1.5.0";

    RolesForm.NS = "acms.view.admin.member.roles-form";

    RolesForm.needInit = true;

    RolesForm.DEFAULT_OPTIONS = {
        remote: null
    };

    RolesForm.init = function (element, options) {
        var rf = new RolesForm(element, options);
        rf.element.acmsData(RolesForm.NS, rf);

        return rf;
    }

    RolesForm.prototype = {

        constructor: RolesForm,

        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this.isModal = Selectors.closest(element, '.modal');
            if (this.isModal) {
                var reset = Selectors.q('button[type=reset]', element)
                reset.removeAttribute('onclick');
            }
            this.remote = this.options.remote || this.element.getAttribute('action');
            this.listen();
        },

        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {RolesForm.DEFAULT_OPTIONS|Object}
         */
        getDefaultOptions: function () {
            return RolesForm.DEFAULT_OPTIONS;
        },

        /**
         * Bildet die Optionen
         *
         * <p>
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         * </p>
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },

        listen: function () {
            var self = this, element = self.element, btn = Selectors.q('button[type=submit]', element);
            AcmsEvent.add(btn, 'click', function (e) {
                e.preventDefault();
                var form = Selectors.closest(e.target, 'form');
                var fData = new FormData(form),
                        d = function (response) {
                            return self._done(response);
                        }, f = function (jqXHR, textStatus) {
                    return self._fail(jqXHR, textStatus);
                }
                ;
                require(['http/request', 'notifier'], function (Request) {
                    Request.post(self.remote, fData, d, f);
                });
                return false;
            });
        },

        _done: function (response) {
            var self = this;
            A.Notifications.createFromResponse(response);
            if (response.status === "success") {
                Win.refresh(1500)
            }
        },

        _fail: function (jqXHR, textStatus) {
            A.Logger.logWarning(textStatus);
        }

    };

    return RolesForm;
});