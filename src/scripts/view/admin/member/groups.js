/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */

define('view/admin/member/groups', [
    'tools/utils',
    'core/classes',
    'core/selectors',
    'events/event',
    'templates/templates'
], function (Utils, Classes, Selectors, AcmsEvent) {

    "use strict";

    var MODELFORM_DATA = "acms.view.form.model-form",
            NS = "." + MODELFORM_DATA,
            EVENT_BEFORE_TRIGGER_SAVE = "before-trigger-save" + NS,
            EVENT_AFTER_TRIGGER_SAVE = "after-trigger-save" + NS,
            EVENT_BEFORE_TRIGGER_SAVE_NEW = "before-trigger-save-new" + NS,
            EVENT_AFTER_TRIGGER_SAVE_NEW = "after-trigger-save-new" + NS,
            EVENT_BEFORE_TRIGGER_SAVE_CLOSE = "before-trigger-save-close" + NS,
            EVENT_AFTER_TRIGGER_SAVE_CLOSE = "after-trigger-save-close" + NS,
            EVENT_BEFORE_TRIGGER_ABORT = "before-trigger-abort" + NS,
            EVENT_AFTER_TRIGGER_ABORT = "after-trigger-abort" + NS,
            EVENT_ACMS_BEFORE_SAVE = "acms.before-save-form"
            ;

    var GroupsForm = function (element, options) {
        this.element =
                this.options =
                this.isInModal =
                this.scope = null;
        this.initialize(element, options);
    }

    GroupsForm.MODULE = "Benutzergruppen Formular";

    GroupsForm.VERSION = "1.5.0";

    GroupsForm.SCOPE_SAVE = "save";

    GroupsForm.SCOPE_SAVE_NEW = "save-new";

    GroupsForm.SCOPE_SAVE_CLOSE = "save-close";

    GroupsForm.SCOPE_ABORT = "abort";

    /**
     * Standard Optionen
     *
     * @var {Object}
     */
    GroupsForm.DEFAULT_OPTIONS = {
        beforeSave: function (form) {
            return true;
        },

        afterSave: function (form) {

        },

        previousUrl: null
    };

    GroupsForm.needInit = true;

    GroupsForm.init = function (element, options) {
        var gf = new GroupsForm(element, options);
        gf.element.acmsData(MODELFORM_DATA, gf);
        return gf;
    };

    GroupsForm.prototype = {
        constructor: GroupsForm,
        /**
         * Interner Constructor
         *
         * @param {Object} element Element
         * @param {Object} options Options
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this.isInModal = Selectors.closest(element, '.modal') || false;
            this.action = element.getAttribute('action');
            this.keyField = element.acmsData('key');
            this._prepare();
            this._precheckName();
            this.listen();
        },

        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {GroupsForm.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return GroupsForm.DEFAULT_OPTIONS;
        },

        /**
         * Bildet die Optionen
         *
         * <p>
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         * </p>
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function (options) {
            options = (typeof options === "object") ? options : {};
            return Utils.extend(
                    {},
                    this.getDefaultOptions(),
                    options,
                    this.element.acmsData()
                    );
        },

        listen: function () {
            var self = this,
                    form = self.element,
                    btn1 = Selectors.q('[data-trigger="save"]', form),
                    btn2 = Selectors.q('[data-trigger="save-close"]', form),
                    btn3 = Selectors.q('[data-trigger="save-new"]', form),
                    btn4 = Selectors.q('[data-trigger="abort"]', form);
            AcmsEvent.add(btn1, 'click', function (e) {
                e.preventDefault();
                e.stopPropagation();
                if (Classes.hasClass(e.target, 'disabled')) {
                    return false;
                }
                self._triggerSave(e);
            });
            AcmsEvent.add(btn2, 'click', function (e) {
                e.preventDefault();
                e.stopPropagation();
                if (Classes.hasClass(e.target, 'disabled')) {
                    return false;
                }
                self._triggerSaveClose(e);
            });
            AcmsEvent.add(btn3, 'click', function (e) {
                e.preventDefault();
                e.stopPropagation();
                if (Classes.hasClass(e.target, 'disabled')) {
                    return false;
                }
                self._triggerSaveNew(e);
            });
            AcmsEvent.add(btn4, 'click', function (e) {
                e.preventDefault();
                e.stopPropagation();
                if (Classes.hasClass(e.target, 'disabled')) {
                    return false;
                }
                self._triggerAbort(e);
            });
        },

        _triggerSave: function (e) {
            var self = this,
                    o = self.options,
                    form = e.target;
            if (form.tagName.toUpperCase() !== 'FORM') {
                form = Selectors.closest(form, 'FORM');
            }
            self.scope = GroupsForm.SCOPE_SAVE;
            self._triggerDefaultBeforeSave();
            var e = self._triggerEvent(EVENT_BEFORE_TRIGGER_SAVE);
            if (e.defaultPrevented) {
                return false;
            }
            if (false === self._beforeSave()) {
                return false;
            }

            require(["http/request", "notifier"], function (Request) {
                var data = new FormData(form),
                        d = function (response) {
                            return self._done(response);
                        }, f = function (jqXHR, textStatus) {
                    return self._fail(jqXHR, textStatus);
                }, a = function () {
                    self._triggerEvent(EVENT_AFTER_TRIGGER_SAVE);
                    return self._always();
                };
                Request.post(self.action, data, d, f, a);
                ;

            });
        },

        _triggerSaveClose: function (e) {
            var self = this,
                    form = e.target;
            if (form.tagName.toUpperCase() !== 'FORM') {
                form = Selectors.closest(form, 'FORM');
            }
            self.scope = GroupsForm.SCOPE_SAVE_CLOSE;
            self._triggerDefaultBeforeSave();
            var e = self._triggerEvent(EVENT_BEFORE_TRIGGER_SAVE_CLOSE);
            if (e.defaultPrevented) {
                return false;
            }
            if (false === self._beforeSave()) {
                return false;
            }
            require(["http/request", "notifier"], function (r) {
                var data = new FormData(form),
                        d = function (response) {
                            return self._done(response);
                        }, f = function (jqXHR, textStatus) {
                    return self._fail(jqXHR, textStatus);
                }, a = function () {
                    self._triggerEvent(EVENT_AFTER_TRIGGER_SAVE_CLOSE);
                    return self._always();
                };
                r.post(self.action, data, d, f, a);
                ;

            });
        },

        _triggerSaveNew: function (e) {
            var self = this,
                    form = e.target;
            if (form.tagName.toUpperCase() !== 'FORM') {
                form = Selectors.closest(form, 'FORM');
            }
            self.scope = GroupsForm.SCOPE_SAVE_NEW;
            self._triggerDefaultBeforeSave();
            var e = self._triggerEvent(EVENT_BEFORE_TRIGGER_SAVE_NEW);
            if (e.defaultPrevented) {
                return false;
            }
            if (false === self._beforeSave()) {
                return false;
            }

            require(["http/request", "notifier"], function (Request) {
                var data = new FormData(form),
                        d = function (response) {
                            return self._done(response);
                        }, f = function (jqXHR, textStatus) {
                    return self._fail(jqXHR, textStatus);
                }, a = function () {
                    self._triggerEvent(EVENT_AFTER_TRIGGER_SAVE_NEW);
                    return self._always();
                };
                Request.post(self.action, data, d, f, a);
                ;

            });
        },

        _triggerAbort: function (e) {
            var self = this,
                    o = this.options,
                    form = e.target;
            if (form.tagName.toUpperCase() !== 'FORM') {
                form = Selectors.closest(form, 'FORM');
            }
            ;
            self.scope = GroupsForm.SCOPE_ABORT;
            var e = self._triggerEvent(EVENT_BEFORE_TRIGGER_ABORT);
            if (e.defaultPrevented) {
                return false;
            }
            self.element[0].reset();
            self._triggerEvent(EVENT_BEFORE_TRIGGER_ABORT);
            if (this.isInModal) {
                var modal = Selectors.closest(form, '.modal');
                var dismiss = modal.querySelector('[data-dismiss=modal]');
                AcmsEvent.fireClick(dismiss);
                return false;
            }
            if (typeof o.previousUrl === "string" && o.previousUrl.isUrl()) {
                window.location.href = o.previousUrl;
            } else {
                history.go(-1);
            }
        },

        _done: function (response) {
            var self = this, scope = this.scope, form = self.element;
            if (response.status === 'success') {

                Acms.Notifications.create({content: response.message, title: Acms.Translator._('Erfolgreich gespeichert'), type: Acms.Notifications.TYPE_SUCCESS});
                var red = response.redirect;
                switch (scope) {
                    case GroupsForm.SCOPE_SAVE:
                        if (response[self.keyField]) {
                            var id = form.querySelector('input[name=' + self.keyField + ']');
                            if (id) {
                                id.value = response[self.keyField];
                                AcmsEvent.fireChange(id);
                            }
                            var op = form.querySelector('input[name=modelop]');
                            op.value = 'edit';
                            AcmsEvent.fireChange(op);
                            var op2 = form.querySelector('input[name=is_new_model]');
                            op2.value = '0';
                            AcmsEvent.fireChange(op2);
                        }
                        break;
                    case GroupsForm.SCOPE_SAVE_CLOSE:
                        if (self.isInModal && scope !== GroupsForm.SCOPE_SAVE) {
                            self.element.reset();
                            var modal = Selectors.closest(form, '.modal');
                            var dismiss = modal.querySelector('[data-dismiss=modal]');
                            AcmsEvent.fireClick(dismiss);
                            return;
                        }
                        if (red && typeof red === "string" && red.isUrl()) {
                            window.location.href = response.redirect;
                        } else {
                            history.go(-1);
                        }
                        break;
                    case GroupsForm.SCOPE_SAVE_NEW:
                        if (self.isInModal && scope !== GroupsForm.SCOPE_SAVE) {
                            self.element.reset();
                        } else {
                            window.refresh(1500);
                        }
                        ;
                        break;
                }
            } else {
                Acms.Notifications.create({content: response.message, title: Acms.Translator._('Fehler!'), type: Acms.Notifications.TYPE_DANGER});

            }
        },

        _fail: function (jqXHR, textStatus) {
            Acms.Logger.logWarning(textStatus);
            Acms.Notifications.createNotification(textStatus, Acms.Translator._('Fehler'), Acms.Notifications.TYPE_ERROR);
        },

        _always: function () {
            Acms.Logger.writeLog('Formular erfolgreich übermittelt')
        },

        _prepare: function () {
            var self = this,
                    form = this.element,
                    fields = Selectors.qa('input,select,textarea', form), i;
            for (i = 0; i < fields.length; i++) {
                var f = fields[i], type = f.getAttribute('type');
                if (type && (type.toLowerCase() === 'hidden'
                        || Classes.hasClass(f, 'hidden'))) {
                    continue;
                }
                var required = (f.hasAttribute('required')
                        && f.getAttribute('required') == 'required'
                        || f.getAttribute('required') == true);
                if (required && !f.value) {

                }
            }
            ;
            if (this.isInModal) {
                var abort = form.querySelector('button[type="reset"]');
                if (abort)
                    abort.setAttribute('onclick', '');
            }
        },

        /**
         * BeforeSave Action
         *
         * @returns {Boolean}
         */
        _beforeSave: function () {
            var o = this.options,
                    result = true,
                    form = this.element;
            if (typeof o.beforeSave === 'function') {
                result = o.beforeSave(form);
            }
            return result === true;
        },

        /**
         * AfterSave Action
         *
         * @returns {Boolean}
         */
        _afterSave: function () {
            var o = this.options,
                    result = true,
                    form = this.element;
            if (typeof o.afterSave === 'function') {
                result = o.afterSave(form);
            }
            return result === true;
        },

        _triggerDefaultBeforeSave: function () {
            this._triggerEvent(EVENT_ACMS_BEFORE_SAVE);
        },

        _triggerEvent: function (name) {
            var self = this,
                    o = self.options,
                    opts = {
                        relatedTarget: self.element,
                        scope: this.scope
                    };

            var e = AcmsEvent.createCustom(name, opts);
            AcmsEvent.dispatch(self.element, e);
            return e;
        },

        _precheckName: function () {
            var self = this,
                    form = self.element,
                    nameField = Selectors.q("input[name=name]", form),
                    nameGroup = Selectors.closest(nameField, '.form-group');
            if (nameField) {

                AcmsEvent.add(nameField, 'focusin, focusout', function (e) {
                    var url = Acms.Urls.coreMembergroupValidateName,
                            nameValue = e.target.value;
                    if (!nameValue) {
                        nameGroup.addClass('has-error');
                        var alertBox = document.querySelector("#GroupNameAlert");
                        if (!alertBox) {
                            var div = document.createElement('div');
                            var alertBox = document.createElement('div');
                            div.innerHTML = Acms.Templates.Components.Alert;
                            while (div.firstChild) {
                                alertBox.appendChild(div.firstChild);
                            }
                            nameGroup.appendChild(alertBox);
                        }
                        Classes.addClass(alert, 'alert,alert-warning')
                        alert.setAttribute('id', 'GroupNameAlert');
                        var msg = alert.querySelector('[data-role="alert-message"]');
                        msg.innerHTML = Acms.Translator._('Achtung! Der Gruppen Name kann nicht leer sein!');
                        return;
                    }
                    if (!url) {
                        throw new Error('Invalid URL given');
                    }
                    var fData = new FormData(),
                            done = function (response) {
                                if (response.status === "success") {
                                    Classes.removeClass(nameGroup, 'has-error');
                                    Classes.addClass(nameGroup, 'has-success');
                                    var alert = nameGroup.querySelector('#GroupNameAlert');
                                    if (alert && 'parentNode' in alert) {
                                        alert.parentNode.removeChild(alert);
                                    }
                                } else {
                                    Classes.removeClass(nameGroup, 'has-success');
                                    Classes.addClass(nameGroup, 'has-error');
                                    var alertBox = document.querySelector('#GroupNameAlert');
                                    if (!alertBox) {
                                        var div = document.createElement('div');
                                        var alertBox = document.createElement('div');
                                        div.innerHTML = Acms.Templates.Components.Alert;
                                        while (div.firstChild) {
                                            alertBox.appendChild(div.firstChild);
                                        }
                                        nameGroup.appendChild(alertBox);
                                    }
                                    Classes.addClass(alertBox, 'alert-danger');
                                    Classes.removeClass('alert-warning');
                                    alertBox.querySelector('[data-role="alert-message"]').innerHTML = response.message;

                                }
                            };
                    fData.append('name', nameValue);
                    require(['http/request', 'notifier'], function (Request) {
                        Request.post(url, fData, done);
                    });

                });
            }
        }
    }

    return GroupsForm;

});
