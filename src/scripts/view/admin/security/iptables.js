/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */

define('view/admin/security/iptables', [
    'tools/utils',
    'core/selectors',
    'core/classes',
    'apps/ui/informations/modal',
    'events/event',
    'core/acms'
], function (Utils, Selectors, Classes, Modal, AcmsEvent, A) {

    "use strict";

    /**
     * Constructor
     * 
     * @param {Element}     element     Das HTML-Element
     * @param {Object}      options     Die Optionen
     * @returns {Iptables}
     */
    var Iptables = function (element, options) {
        this.element =
                this.options =
                this.deleteBtn =
                this.deleteModal =
                this.deleteModalDismiss =
                this.deleteModalForm =
                this.deleteModalSubmit = 
                this.deletes =
                this.addBtn =
                this.addModal =
                this.addModalForm = null;
        this.options = Iptables.DEFAULT_OPTIONS;

        this.initialize(element, options);
    };

    /**
     * Modul Name
     * 
     * @var {String}
     */
    Iptables.MODULE = "Firewall";

    /**
     * Modul Version
     * 
     * @var {String}
     */
    Iptables.VERSION = "1.5.0";

    /**
     * Modul Namespace
     * 
     * @var {String}
     */
    Iptables.NS = "acms.view.admin.security.iptables";

    /**
     * Standard-Optionen des Moduls
     * 
     * @var {Object}
     */
    Iptables.DEFAULT_OPTIONS = {
        remote: null,
        deleteTrigger: '[data-trigger="delete"]',
        deleteModal: '#iptables-modal-form-delete',
        newModal: '#iptables-modal-form-add'
    };

    /**
     * Sagt dem Modul-Loader, dass das Modul initialisiert werden muss, wenn es 
     * über diesen geladen wird.
     * 
     * @var {Boolean}
     */
    Iptables.needInit = true;

    /**
     * Modul Initialisierung
     * 
     * @param {Element}     element     Das HTML-Element
     * @param {Object}      options     Die Optionen
     * @returns {Iptables}
     */
    Iptables.init = function (element, options) {
        var v = new Iptables(element, options);
        v.element.acmsData(Iptables.NS, v);

        return v;
    };

    /**
     * Klassen Definition
     */
    Iptables.prototype = {

        constructor: Iptables,

        /**
         * Interner Constructor
         * 
         * @param {Element} element     HMTL Element
         * @param {Objct}   options     Eigene Optionen
         * 
         * 
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },

        /**
         * Aufsetzen der Event-Listener
         * 
         */
        listen: function () {
            var self = this,
                o = self.options,
                deletes = self.deletes;
            var cb = function(ele, target, overlay) {
                self._onClickDelete(ele);
            }, opts = {onShow: cb, onHide: function(ele, targ, mod) {self._onAbortDelete(ele)}, target: o.deleteModal};
            
            deletes.forEach(function(btn) {
               Modal.init(btn, opts);
            });
            
            AcmsEvent.add(self.addBtn, 'click', self._onSave);
            AcmsEvent.add(self.deleteModalSubmit, 'click', self._onDelete);
        },

        /**
         * Gibt die Standard-Optionen zurück
         * 
         * @returns {Iptables.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return Iptables.DEFAULT_OPTIONS;
        },

        /**
         * Bilden der Optionen
         * 
         * Bildet die Optionen basierend auf den Standard-Optionen, den 
         * übergebenen Optionen des Constructors und den Element Data-Attributen. 
         * Letztere gewinnen den Kampf, wenn gesetzt.
         * 
         * @param {Object} options    Die eignenen Optionen
         * @returns {Object|Iptables.DEFAULT_OPTIONS}
         */
        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },

        /**
         * Vorbereiten des Moduls
         * 
         * 
         */
        _prepare: function () {
            var self = this,
                    element = self.element,
                    o = self.options;
            self.deletes = Selectors.qa(o.deleteTrigger, element);
            self.button = Selectors.q('[data-trigger="submit"]', element);
            self.deleteModal = Selectors.q(o.deleteModal);
            self.deleteModalForm = Selectors.q('form', self.deleteModal);
            self.deleteModalSubmit = Selectors.q('[data-trigger="submit"]', self.deleteModal);
            self.deleteModalDismiss = Selectors.qa('[data-dismiss="modal"]', self.deleteModal);
            
            self.addModal = Selectors.q(o.newModal);
            self.addModalForm = Selectors.q('form', self.addModal);
            self.addBtn = Selectors.q('button[type=submit]', self.addModal);
            
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        },
        
        _onClickDelete: function(element) {
            var self = this,
                btn = element, tr = Selectors.closest(btn, 'tr');
            if(null === tr) {
                return false;
            }
            var id = tr.acmsData('id');
            var hidden = Selectors.q('input[name="id"]', self.deleteModal);
            hidden.value = id;
            hidden.setAttribute('value', id);
            AcmsEvent.fireChange(hidden);
            self.currentDelete = tr;
        },
        
        _onAbortDelete: function(element) {
            var self = this;
            self.deleteModalForm.reset();
            self.currentDelete = null;
            self.inProcess = false;
        },

        _onSave: function(e) {
            e.preventDefault();
            if(this.inProcess) {
                return false;
            }
            this.inProcess = true;
            var f = Selectors.closest(e.target, 'form'),
                    self = this, o = self.options,
                    fData = new FormData(f);
            require(['http/request', 'notifier'], function(Request) {
                Request.post(o.remote, fData, self._onSaveDone);
            });
            
            return false;
        },
        
        _onSaveDone: function(response) {
            A.Notifications.createFromResponse(response);
            this.inProcess = false;
            if(response.status === 'success') {
                var dismiss = Selectors.q('[data-dismiss=modal]', this.addModal);
                dismiss && AcmsEvent.fireClick(dismiss);
                window.refresh(300);
            }
        },
        
        _onDelete: function(e) {
            e.preventDefault();
            if(this.inProcess) {
                return false;
            }
            this.inProcess = true;
            var f = Selectors.closest(e.target, 'form'),
                    self = this, o = self.options,
                    fData = new FormData(f);
            require(['http/request', 'notifier'], function(Request) {
                Request.post(o.remote, fData, self._onDeleteDone);
            });
            
            return false;
        },
        
        _onDeleteDone: function(response) {
            A.Notifications.createFromResponse(response);
            this.inProcess = false;
            if(response.status === 'success') {
                if(this.currentDelete) {
                    this.currentDelete.parentNode.removeChild(this.currentDelete);
                    this.currentDelete = null;
                }
                var dismiss = Selectors.q('[data-dismiss=modal]', this.deleteModal);
                dismiss && AcmsEvent.fireClick(dismiss);
            }
            
        },
    };

    return Iptables;

});

