/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */

define('view/admin/preferences/quicksearch', [
    'tools/utils',
    'events/event',
    'ui/app/autocomplete'
], function (Utils, AcmsEvent, AutoComplete) {

    "use strict";

    var Quicksearch = function (element, options) {
        this.element =
                this.options =
                this.autocomplete = null;
        this.initialize(element, options);
    };
    Quicksearch.MODULE = "Schnellsuche der Einstellungen";

    Quicksearch.VERSION = "1.5.0";

    Quicksearch.DEFAULT_OPTIONS = {
        remote: null
    };

    Quicksearch.css = AutoComplete.css;

    Quicksearch.needInit = true;

    Quicksearch.init = function (element, options) {
        var qs = new Quicksearch(element, options);

        return qs;
    };

    Quicksearch.prototype = {

        constructor: Quicksearch,

        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
        },

        getDefaultOptions: function () {
            return Quicksearch.DEFAULT_OPTIONS;
        },

        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },

        _prepare: function () {
            var self = this,
                    element = self.element,
                    o = self.options,
                    autocompleteOptions = {
                        remote: o.remote,
                        onSelect: function (e, value, label, item) {
                            console.log(item.acmsData('autocompleteitem'))
                            if (value && typeof value === 'string' && value.isUrl()) {
                                window.location.href = value;
                            }
                        },
                    };
            self.autocomplete = AutoComplete.init(element, autocompleteOptions);
        }
    };

    return Quicksearch;
});