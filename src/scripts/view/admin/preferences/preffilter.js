/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */


define('view/admin/preferences/preffilter', [
    'tools/utils',
    'core/selectors',
    'core/classes',
    'events/event'
], function (Utils, Selectors, Classes, AcmsEvent) {

    "use strict";

    var PrefFilter = function (element, options) {
        this.element =
                this.options =
                this.target =
                this.filters =
                this.buttons = null;
        this.initialize(element, options);
    };
    PrefFilter.MODULE = "Schnell-Filter der Einstellungen";

    PrefFilter.VERSION = "1.5.0";

    PrefFilter.DEFAULT_OPTIONS = {

    };

    PrefFilter.needInit = true;

    PrefFilter.init = function (element, options) {
        var qs = new PrefFilter(element, options);

        return qs;
    };

    PrefFilter.prototype = {

        constructor: PrefFilter,

        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
        },

        getDefaultOptions: function () {
            return PrefFilter.DEFAULT_OPTIONS;
        },

        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },

        applyFilter: function () {
            var self = this;
            var items = Selectors.qa('li', self.target), j;
            for (j = 0; j < items.length; j++) {
                var li = items[j], type = li.acmsData('typ');
                if (self.filters[type] === false) {
                    Classes.addClass(li, 'hiding');
                } else {
                    Classes.removeClass(li, 'hiding');
                }
            }

        },

        _prepare: function () {
            var self = this,
                    element = self.element,
                    o = self.options,
                    btns = Selectors.qa('.filter-button', element),
                    j,
                    target = Selectors.q('#system-categories');
            self.buttons = btns;
            self.target = target;
            self.filters = {};
            for (j = 0; j < btns.length; j++) {
                AcmsEvent.add(btns[j], 'click', self._onClick.bind(self));
                var curr = btns[j].value;
                if (Classes.hasClass(btns[j], 'active')) {
                    this.filters[curr] = true;
                } else {
                    this.filters[curr] = false;
                }
            }
        },

        _onClick: function (e) {
            var self = this,
                    target = e.target;
            if (target.tagName.toLowerCase() !== 'button') {
                target = Selectors.closest(target, 'button');
            }
            Classes.toggleClass(target, 'active');
            var curr = target.value;
            if (document.activeElement != document.body)
                document.activeElement.blur();
            self.filters[curr] = Classes.hasClass(target, 'active');
            self.applyFilter();
        }
    };

    return PrefFilter;
});