/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */


define('view/admin/components/navigations/menumanagement', [
    'tools/object/extend',
    'tools/string/sprintf',
    'core/selectors',
    'core/classes',
    'events/event',
    'ui/app/sortable'
], function (Extend, sprintf, Selectors, Classes, AcmsEvent, Sortable) {
    
    "use strict";
    
    /**
     * Constructor
     * 
     * @param {Element} element Das HTML Element
     * @param {Object|NULL} options Eigene Optionen
     * @returns {NavigationsMenu}
     */
    var NavigationsMenu = function(element, options) {
        this.element =
        this.options =
        this.buttons = null;
        this.options = NavigationsMenu.DEFAULT_OPTIONS;
        this.initialize(element, options);
    };
    
    
    /**
     * Modul Name
     *
     * @var {String}
     */
    NavigationsMenu.MODULE = "Navigations-Management";

    /**
     * Modul Version
     *
     * @var {String}
     */
    NavigationsMenu.VERSION = "1.5.0";
    
    /**
     * Module NS
     * 
     * @var [String]
     */
    NavigationsMenu.NS = "acms.view.admin.components.navigations.menumanagement";
    
    
    /**
     * Standard-Optionen
     * 
     * @var {Object}
     */
    NavigationsMenu.DEFAULT_OPTIONS = {
        nav: '#menumanagement',
        changeOnAdd: false, // wechselt nach erstellen einer neuen navigation zu dieser.
        selectform: '#menu-select-form form'
    };

    /**
     * Einbinden des Modul Loaders
     * 
     * @var {Boolean}
     */
    NavigationsMenu.needInit = true;
    
    /**
     * Modul Initialisierung
     * 
     * @param {Element} element     Das HTML Element
     * @param {Object}  options     Eigene Optionen
     * @returns {NavigationsMenu}
     */
    NavigationsMenu.init = function (element, options) {
        var l = new NavigationsMenu(element, options);
        l.element.acmsData(NavigationsMenu.NS, l);

        return l;
    };
    
    NavigationsMenu.CODE = '&lt;{menu name="%s" template="acmsfile:Core|pfad/zu/Template.tpl"}&gt;';
    
    /**
     * Module CSS
     * 
     * @var [String]
     */
    NavigationsMenu.css = "media/css/admin/menumanagement.min.css";
    
    NavigationsMenu.prototype = {
        
        constructor: NavigationsMenu,
        
        /**
         * Interner Constructor
         *
         * @param {Element} element
         * @param {Object} options
         *
         * 
         */
        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },
        
        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {NavigationsMenu.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return NavigationsMenu.DEFAULT_OPTIONS;
        },
        
        /**
         * Bildet die Optionen
         *
         * <p>
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         * </p>
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function (options) {
            return Extend.flat(
                    {},
                    false,
                    this.getDefaultOptions(),
                    options,
                    this.element.acmsData()
                    );
        },
        
        /**
         * Aufsetzen der Event Listener
         * 
         * 
         */
        listen: function () {
            var self = this, items = self.submenus;
            self._sortable(self.menu);
            items.forEach(function(item) {
                self._sortable(item);
            });
            self.additemButtons.forEach(function(btn) {
                btn.acmsData('listening', true);
                AcmsEvent.add(btn, 'click', self._onAddItemClick);
            });
            self.deleteitemButtons.forEach(function(btn) {
                btn.acmsData('listening', true);
                AcmsEvent.add(btn, 'click', self._onDeleteItemClick);
            });
            self.edititemButtons.forEach(function(btn) {
                btn.acmsData('listening', true);
                AcmsEvent.add(btn, 'click', self._onEditItemClick);
            });
            
            AcmsEvent.add(self.addItemForm, 'acms.view.form.model-form.succeed', self._onAddItemSucceed);
            AcmsEvent.add(self.deleteItemForm, 'acms.view.form.model-form.succeed', self._onDeleteItemSucceed);
            AcmsEvent.add(self.editItemForm, "acms.before-save-form", self._onEditItemStart);
            AcmsEvent.add(self.editItemForm, 'acms.view.form.model-form.succeed', self._onEditItemSucceed);
            
            AcmsEvent.add(self.deleteMenuForm, 'acms.view.form.model-form.succeed', self._onDeleteMenuSucceed);
            AcmsEvent.add(self.addMenuForm, 'acms.view.form.model-form.succeed', self._onAddMenuSucceed);
            AcmsEvent.add(self.editMenuForm, 'acms.view.form.model-form.succeed', self._onEditMenuSucceed);
        },
        
        _onDeleteMenuSucceed: function(e) {
            window.refresh(100);
        },
        
        _onAddMenuSucceed: function(e) {
            var self = this, detail = e.detail && e.detail.data || {}, menu;
            if(detail && detail.model) {
                var id = parseInt(detail.model.id);
                var title = detail.model.title;
                var form = self.menuSelectForm;
                var select = Selectors.q('select[name="menu"]', form);
                var options = Selectors.qa('option', select);
                var option = document.createElement('option');
                option.setAttribute('value', id);
                option.value = id;
                option.textContent = title;
                select.appendChild(option);
                if(self.options.changeOnAdd) {
                    option.selected = false;
                    option.setAttribute('selected', 'selected');
                    options.forEach(function(opt) {
                        opt.selected = false;
                        opt.removeAttribute('selected');
                    });
                    AcmsEvent.fireChange(select);
                }
            }
        },
        
        _onEditMenuSucceed: function(e) {
            var self = this, detail = e.detail && e.detail.data || {}, menu;
            if(detail && detail.model) {
                var id = parseInt(detail.model.id);
                var title = detail.model.title;
                var name = detail.model.name;
                var cssClass = detail.model.class;
                var cssID = detail.model.ulid;
                var form = self.menuSelectForm;
                var select = Selectors.q('select[name="menu"]', form);
                var options = Selectors.qa('option', select);
                options.forEach(function(option) {
                    var val = parseInt(option.value);
                    if(val === id) {
                        option.textContent = title;
                    }
                });
                self.codeExample.innerHTML = sprintf(NavigationsMenu.CODE, name);
                
                setTimeout(function() {
                    var cssClassInput = Selectors.q('input[name="class"]', self.editMenuForm);
                    var cssIDInput = Selectors.q('input[name="ulid"]', self.editMenuForm);
                    var titleInput = Selectors.q('input[name="title"]', self.editMenuForm);
                    var nameInput = Selectors.q('input[name="name"]', self.editMenuForm);
                    cssClassInput.value = cssClass;
                    cssClassInput.setAttribute('value', cssClass);
                    cssIDInput.value = cssID;
                    cssIDInput.setAttribute('value', cssID);
                    titleInput.value = title;
                    titleInput.setAttribute('value', title);
                    nameInput.value = name;
                    nameInput.setAttribute('value', name);
                    AcmsEvent.fireChange(cssClassInput);
                    AcmsEvent.fireChange(cssIDInput);
                    AcmsEvent.fireChange(titleInput);
                    AcmsEvent.fireChange(nameInput);
                }, 1000);
            }
        },
        
        _sortable: function(element) {
            var self = this, sortableOptions = {
                group: 'navigation',
                draggable: 'li.acms-menu-item',
                dataIdAttr: 'data-id',
                handle: '',
                ignore: '',
                onEnd: self._onSortEnd
            };
            Sortable.init(element, sortableOptions);
        },
        
        _onSortEnd: function(e) {
            var self = this, item = e.item,
                ol = Selectors.closest(item, 'ol'),
                parent = Selectors.closest(item, 'li'),
                cats = Selectors.qa(':scope > li.acms-menu-item', ol),
                sort = ol.acmsData('acms.ui.app.sortable'),
                arr = sort.toArray(), j, list = item;
            if(parent) {
                item.acmsData('parentid', parent.acmsData('id'));
                item.acmsData('root', parent.acmsData('root'));
            } else {
                item.acmsData('parentid', 0);
                item.acmsData('root', item.acmsData('id'));
            }
            do {
                list = Selectors.closest(list, 'ol');
            } while(Selectors.closest(list, 'ol'));
            var arr = [], order = 0;
            var makeItems = function(it, par) {
                order++;
                var parent = Selectors.closest(it, 'li');
                if(parent) {
                    var pid = parent.acmsData('id');
                    var root = parent.acmsData('root');
                } else {
                    var pid = 0;
                    var root = item.acmsData('id');
                }
                item.acmsData('parentid', pid);
                item.acmsData('root', root);
                var id = it.acmsData('id');
                var rid = it.acmsData('root');
                var ol = Selectors.q('ol', it), subs, s, o = order,
                ret = {id: id, parentid: pid, root: rid, order: o};
                if(ol) {
                    s = Selectors.qa(':scope > li', ol);
                    subs = [];
                    s.forEach(function(si) {
                        subs.push(makeItems(si));
                    });
                    if(!subs.length) {
                        subs = false;
                    }
                } else {
                    subs = false;
                }
                ret.subs = subs;
                return ret;
            };
            var listitems = Selectors.qa(':scope > li', list);
            listitems.forEach(function(it) {
                arr.push(makeItems(it));
            });
            
            var fData = new FormData();
            fData.append('sort', JSON.stringify(arr));
            fData.append('op', 'sort');
            fData.append('id', self.options.id);
            require(['http/request', 'notifier'], function (Request) {
                Request.post(self.options.remote, fData, self._onSortDone);
            });
            
        },
        
        _onSortDone: function(response) {
            if(response.status !== 'success') {
                Acms.Notifications.createFromResponse(response);
            }
        },
        
        _onAddItemSucceed: function(e) {
            var self = this, detail = e.detail && e.detail.data || {}, menu;
            if(self.lastItem === false) {
                menu = self.menu;
            } else {
                menu = Selectors.q('.sub-menu', self.lastItem);
                if(!menu) {
                    menu = document.createElement('ol');
                    self.lastItem.appendChild(menu);
                    Classes.addClass(menu, 'sub-menu');
                }
            }
            if(detail.model && detail.model.html) {
                var div = document.createElement('div');
                div.innerHTML = detail.model.html;
                
                while(div.firstChild) {
                    menu.appendChild(div.firstChild);
                }
                var modals = Selectors.qa('[data-toggle="modal"]', menu);
                modals.forEach(function(btn) {
                    if(!btn.acmsData('acms.ui.overlay.modal')) {
                        require(['apps/ui/informations/modal'], function(Modal) {
                            Modal.init(btn);
                        });
                    }
                    if(!btn.acmsData('listening')) {
                        var action = btn.acmsData('action').trim();
                        if(action === 'deleteitem') {
                            AcmsEvent.add(btn, 'click', self._onDeleteItemClick);
                        } else if(action === 'additem') {
                            AcmsEvent.add(btn, 'click', self._onAddItemClick);
                        } else if(action === 'edititem') {
                            AcmsEvent.add(btn, 'click', self._onEditItemClick);
                        }
                        btn.acmsData('listening', true);
                    }
                });
            }
        },
        
        _onAddItemClick: function(e) {
            var self = this,
                o = self.options,
                target = e.target,
                menu = self.menu, li, id, parentid, root, rootVal, menuid;
            if(target.tagName.toLowerCase() !== 'button') {
                target = Selectors.closest(target, 'button');
            }
            menuid = Selectors.q('input[name="menu_id"]', self.addItemModal);
            menuid.value = o.id;
            menuid.setAttribute('value', o.id);
            li = Selectors.closest(target, 'li');
            if(li) {
                self.lastItem = li;
                id = li.acmsData('id');
                parentid = Selectors.q('input[name="parentid"]', self.addItemModal);
                parentid.value = id;
                parentid.setAttribute('value', id);
                root = Selectors.q('input[name="root"]', self.addItemModal);
                rootVal = parseInt(li.acmsData('root')) === 0 ? li.acmsData('id') : li.acmsData('root');
                root.value = rootVal;
                root.setAttribute('value', rootVal);
            } else {
                self.lastItem = false;
                parentid = Selectors.q('input[name="parentid"]', self.addItemModal);
                parentid.value = 0;
                parentid.setAttribute('value', 0);
                root = Selectors.q('input[name="root"]', self.addItemModal);
                root.value = 0;
                root.setAttribute('value', 0);
            }
        },
        
        _onDeleteItemClick: function(e) {
            var self = this,
                o = self.options,
                target = e.target,
                li, id, content;
            if(target.tagName.toLowerCase() !== 'button') {
                target = Selectors.closest(target, 'button');
            }
            
            li = Selectors.closest(target, 'li');
            self.lastItem = li;
            id = li.acmsData('id');
            var title = Selectors.q('[data-role="title"]', li).innerHTML;
            content = Selectors.q('[data-role=content]', self.deleteItemModal);
            content.innerHTML = Acms.Translator._('Sind Sie sicher, dass Sie den Menupunkt %s entfernen möchten?', 'Core', 'de_DE', [title]);
            var input = Selectors.q('input[name="id"]', self.deleteItemModal);
            input.value = id;
            input.setAttribute('value', id);
        },
        
        _onDeleteItemSucceed: function(e) {
            var self = this, detail = e.detail && e.detail.data || {}, menu;
            if(self.lastItem && detail.status === 'success') {
                self.lastItem.parentNode.removeChild(self.lastItem);
            }
        },
        
        _onEditItemClick: function(e) {
            var self = this,
                o = self.options,
                target = e.target,
                menu = self.menu, li, id, parentid, root, menuid;
            if(target.tagName.toLowerCase() !== 'button') {
                target = Selectors.closest(target, 'button');
            }
            menuid = Selectors.q('input[name="menu_id"]', self.editItemModal);
            menuid.value = o.id;
            menuid.setAttribute('value', o.id);
            li = Selectors.closest(target, 'li');
            self.lastItem = li;
            id = li.acmsData('id');
            var pid = li.acmsData('parentid');
            var rid = li.acmsData('root');
            parentid = Selectors.q('input[name="parentid"]', self.editItemModal);
            parentid.value = pid;
            parentid.setAttribute('value', pid);
            root = Selectors.q('input[name="root"]', self.editItemModal);
            root.value = rid;
            root.setAttribute('value', rid);
            var idf = Selectors.q('input[name="id"]', self.editItemModal);
            idf.value = id;
            idf.setAttribute('value', id);
            var titlef = Selectors.q('input[name="title"]', self.editItemModal);
            var title = Selectors.q('[data-role="title"]', li).textContent;
            titlef.value = title;
            titlef.setAttribute('value', title);
            var dscf = Selectors.q('input[name="teaser"]', self.editItemModal);
            var dsc = Selectors.q('[data-role="description"]', li).textContent;
            dscf.value = dsc;
            dscf.setAttribute('value', dsc);
            var urlf = Selectors.q('input[name="url"]', self.editItemModal);
            var url = Selectors.q('[data-role="url"]', li).textContent;
            urlf.value = url;
            urlf.setAttribute('value', url);
            var targetf = Selectors.q('select[name="target"]', self.editItemModal);
            var target = Selectors.q('[data-role="target"]', li).textContent.trim();
            targetf.value = target || 'self';
            targetf.setAttribute('value', target || 'self');
            Selectors.qa('option', targetf).forEach(function(option) {
                if(option.value === target) {
                    option.selected = true;
                } else {
                    option.selected = false;
                }
            });
            Selectors.q('input[name="is_new_model"]', self.editItemModal).value = false;
            Selectors.q('input[name="modelop"]', self.editItemModal).value = 'edit';
            Selectors.q('input[name="language"]', self.editItemModal).value = o.locale;
            var accessf = Selectors.qa('input[name="menuitem_view[]"]', self.editItemModal);
            var access = li.acmsData('access');
            try {
                var arr = JSON.parse(access);
                access = {};
                arr.forEach(function(val) {
                    access[val] = val;
                });
            } catch(E) {
                access = {};
            }
            
            accessf.forEach(function(chb) {
                var value = chb.value;
                
                if(access[value]) {
                    chb.checked = true;
                    chb.setAttribute('checked', 'checked');
                } else {
                    chb.checked = false;
                    chb.removeAttribute('checked');
                }
            });
        },
        
        _onEditItemStart: function(e) {
            var self = this, detail = e.detail && e.detail.data;
            if(detail && detail.relatedTarget) {
                self.formData = new FormData(detail.relatedTarget);
            }
        },
        
        _onEditItemSucceed: function(e) {
            var self = this, detail = e.detail && e.detail.data;
            var li = self.lastItem;
            if(detail && detail.model) {
                var fData = detail.model;
                var title = Selectors.q('[data-role="title"]', li);
                title.textContent = fData.title;
                var desc = Selectors.q('[data-role="description"]', li);
                desc.textContent = fData.teaser;
                var target = Selectors.q('[data-role="target"]', li);
                target.textContent = fData.target;
                var url = Selectors.q('[data-role="url"]', li);
                url.textContent = fData.url;
                li.acmsData('access', fData.jgroups);
            }
        },
        
        _prepare: function() {
            var self = this,
                o = self.options,
                element = self.element;
            self.menu = Selectors.q(o.nav, element);
            self.submenus = Selectors.qa('li .sub-list', element);
            self.additemButtons = Selectors.qa('.button[data-action="additem"]');
            self.addItemModal = Selectors.q('#modalform-add-menuitem');
            self.addItemForm = Selectors.q('form', self.addItemModal);
            
            self.deleteitemButtons = Selectors.qa('.button[data-action="deleteitem"]');
            self.deleteItemModal = Selectors.q('#menu-item-delete');
            self.deleteItemForm = Selectors.q('form', self.deleteItemModal);
            
            self.edititemButtons = Selectors.qa('.button[data-action="edititem"]');
            self.editItemModal = Selectors.q('#modalform-edit-menuitem');
            self.editItemForm = Selectors.q('form', self.editItemModal);
            
            self.deleteMenuModal = Selectors.q('#menu-delete');
            self.deleteMenuForm = Selectors.q('form', self.deleteMenuModal);
            
            self.addMenuModal = Selectors.q('#modalformMenu');
            self.addMenuForm = Selectors.q('form', self.addMenuModal);
            
            self.editMenuModal = Selectors.q('#menu-edit-modal');
            self.editMenuForm = Selectors.q('form', self.editMenuModal);
            
            self.menuSelectForm = Selectors.q(o.selectform);
            self.codeExample = Selectors.q('#menu-code-example code');
            
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        }
        
        
    };

    return NavigationsMenu;

});
