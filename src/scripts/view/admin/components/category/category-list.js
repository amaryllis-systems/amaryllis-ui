/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */

define('view/admin/components/category/category-list', [
    'tools/utils',
    'core/selectors',
    'core/classes',
    'ui/app/sortable',
    'events/event',
    'core/acms'
], function (Utils, Selectors, Classes, Sortable, AcmsEvent, A) {

    "use strict";

    /**
     * Constructor
     * 
     * @param {Element}     element     Das HTML-Element
     * @param {Object}      options     Die Optionen
     * @returns {Vis}
     */
    var Vis = function (element, options) {
        this.element =
                this.options =
                this.toggles = null;
        this.options = Vis.DEFAULT_OPTIONS;

        this.initialize(element, options);
    };

    /**
     * Modul Name
     * 
     * @var {String}
     */
    Vis.MODULE = "Kategorien-Baum";

    /**
     * Modul Version
     * 
     * @var {String}
     */
    Vis.VERSION = "1.5.0";

    /**
     * Modul Namespace
     * 
     * @var {String}
     */
    Vis.NS = "acms.view.admin.components.category.category-list";
    
    Vis.css = 'media/css/admin/categories.css';

    /**
     * Standard-Optionen des Moduls
     * 
     * @var {Object}
     */
    Vis.DEFAULT_OPTIONS = {

    };

    /**
     * Sagt dem Modul-Loader, dass das Modul initialisiert werden muss, wenn es 
     * über diesen geladen wird.
     * 
     * @var {Boolean}
     */
    Vis.needInit = true;

    /**
     * Modul Initialisierung
     * 
     * @param {Element}     element     Das HTML-Element
     * @param {Object}      options     Die Optionen
     * @returns {Vis}
     */
    Vis.init = function (element, options) {
        var v = new Vis(element, options);
        v.element.acmsData(Vis.NS, v);

        return v;
    };

    /**
     * Klassen Definition
     */
    Vis.prototype = {

        constructor: Vis,

        /**
         * Interner Constructor
         * 
         * @param {Element} element     HMTL Element
         * @param {Objct}   options     Eigene Optionen
         * 
         * 
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },

        /**
         * Aufsetzen der Event-Listener
         * 
         */
        listen: function () {
            var self = this,
                    o = self.options,
                    element = self.element;
        },

        /**
         * Gibt die Standard-Optionen zurück
         * 
         * @returns {Vis.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return Vis.DEFAULT_OPTIONS;
        },

        /**
         * Bilden der Optionen
         * 
         * Bildet die Optionen basierend auf den Standard-Optionen, den 
         * übergebenen Optionen des Constructors und den Element Data-Attributen. 
         * Letztere gewinnen den Kampf, wenn gesetzt.
         * 
         * @param {Object} options    Die eignenen Optionen
         * @returns {Object|Vis.DEFAULT_OPTIONS}
         */
        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },

        /**
         * Vorbereiten des Moduls
         * 
         * 
         */
        _prepare: function () {
            var self = this,
                element = self.element,
                o = self.options, j,
                
                fieldGroups = Selectors.qa('.connected-list'),
                remote = o.remote,
                done = function (response) {
                    Acms.Notifications.createFromResponse(response);
                },
                sortableOptions = {
                    group: 'categories',
                    draggable: 'li.sortable-category',
                    dataIdAttr: 'data-id',
                    placeholderClass: 'sortable-category',
                    sort: true,
                    onEnd: function (e) {
                        var item = e.item,
                                ol = Selectors.closest(item, 'ol'),
                                cats = Selectors.qa('.sortable-category', ol),
                                sort = ol.acmsData('acms.ui.app.sortable'),
                                arr = sort.toArray(), j;
                        console.log(e)
                        if(!Selectors.q('ol', item)) {
                            var ol = document.createElement('ol');
                            item.appendChild(ol);
                            Classes.addClass(ol, 'connected-list');
                            Sortable.init(ol, sortableOptions);
                        }
                        var parent = parseInt(ol.acmsData('parent'));
                        console.log(parent)
                        return;
                        var fData = new FormData();
                        fData.append('sort', JSON.stringify(arr));
                        require(['http/request', 'notifier'], function (Request) {
                            Request.post(remote, fData, done);
                        });
                        for (j = 0; j < arr.length; j++) {
                            var nI = arr[j];
                            var cat = cats[j], ul = Selectors.q('ul', cat), lis = Selectors.qa('li', ul), k;
                            ul.acmsData('cat', nI);
                            ul.setAttribute('data-cat', nI);
                            for (k = 0; k < lis.length; k++) {
                                lis[k].acmsData('cat', nI);
                                lis[k].setAttribute('data-cat', nI);
                            }
                        }

                    }
                };
            var sortables = [];

            for (j = 0; j < fieldGroups.length; j++) {
                var sort = Sortable.init(fieldGroups[j], sortableOptions);
                sortables.push(sort);
            }
            this.sortables = sortables;
        }

    };

    return Vis;

});

