/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */

define('view/admin/components/tags/tag-approvals', [
    'tools/utils',
    'core/selectors',
    'core/classes',
    'ui/components/actionlistgroup',
    'events/event',
    'core/acms'
], function (Utils, Selectors, Classes, ActionListGroup, AcmsEvent, A) {

    "use strict";

    /**
     * Constructor
     * 
     * @param {Element}     element     Das HTML-Element
     * @param {Object}      options     Die Optionen
     * @returns {CategoryApprovals}
     */
    var CategoryApprovals = function (element, options) {
        this.element =
                this.options =
                this.toggles = null;
        this.options = CategoryApprovals.DEFAULT_OPTIONS;

        this.initialize(element, options);
    };

    /**
     * Modul Name
     * 
     * @var {String}
     */
    CategoryApprovals.MODULE = "Category-Genehmigungen";

    /**
     * Modul Version
     * 
     * @var {String}
     */
    CategoryApprovals.VERSION = "1.5.0";

    /**
     * Modul Namespace
     * 
     * @var {String}
     */
    CategoryApprovals.NS = "acms.view.admin.components.tags.tag-approvals";

    /**
     * Standard-Optionen des Moduls
     * 
     * @var {Object}
     */
    CategoryApprovals.DEFAULT_OPTIONS = {

    };

    /**
     * Sagt dem Modul-Loader, dass das Modul initialisiert werden muss, wenn es 
     * über diesen geladen wird.
     * 
     * @var {Boolean}
     */
    CategoryApprovals.needInit = true;

    /**
     * Modul Initialisierung
     * 
     * @param {Element}     element     Das HTML-Element
     * @param {Object}      options     Die Optionen
     * @returns {CategoryApprovals}
     */
    CategoryApprovals.init = function (element, options) {
        var v = new CategoryApprovals(element, options);
        v.element.acmsData(CategoryApprovals.NS, v);

        return v;
    };

    /**
     * Klassen Definition
     */
    CategoryApprovals.prototype = {

        constructor: CategoryApprovals,

        /**
         * Interner Constructor
         * 
         * @param {Element} element     HMTL Element
         * @param {Objct}   options     Eigene Optionen
         * 
         * 
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },

        /**
         * Aufsetzen der Event-Listener
         * 
         */
        listen: function () {
            var self = this,
                    o = self.options,
                    buttons = self.buttons;
            buttons.forEach(function(btn) {
                AcmsEvent.add(btn, 'click', self._onClick)
            })
        },

        /**
         * Gibt die Standard-Optionen zurück
         * 
         * @returns {CategoryApprovals.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return CategoryApprovals.DEFAULT_OPTIONS;
        },

        /**
         * Bilden der Optionen
         * 
         * Bildet die Optionen basierend auf den Standard-Optionen, den 
         * übergebenen Optionen des Constructors und den Element Data-Attributen. 
         * Letztere gewinnen den Kampf, wenn gesetzt.
         * 
         * @param {Object} options    Die eignenen Optionen
         * @returns {Object|CategoryApprovals.DEFAULT_OPTIONS}
         */
        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },
        
        _onClick: function(e) {
            var self = this,
                o = self.options,
                btn = e.target,
                form = Selectors.closest(btn, 'form'),
                fData = new FormData(form);
            if(btn.tagName.toLowerCase() !== 'button') {
                btn = Selectors.closest(btn, 'button');
            }
            fData.append('toggle', btn.acmsData('toggle'));
            fData.append('op', 'multiapprove');
            self.lastAction = btn.acmsData('toggle').trim();
            require(['http/request', 'notifier'], function(Request) {
                Request.post(o.remote, fData, self._done);
            });
        },
        
        _done: function(response) {
            var self = this, element = self.element;
            A.Notifications.createFromResponse(response);
            if(response.status === 'success') {
                var boxes = Selectors.qa('input[type=checkbox]'), tr;
                boxes.forEach(function(checkbox) {
                    if(checkbox.checked && self.lastAction === 'approved') {
                        tr = Selectors.closest(checkbox, 'tr');
                        tr.parentNode.removeChild(tr);
                    } else if(checkbox.checked) {
                        checkbox.checked = false;
                        checkbox.removeAttribute('checked');
                        AcmsEvent.fireChange(checkbox);
                    }
                });
                if(self.message) {
                    self.message.value = '';
                    AcmsEvent.fireChange(self.message)
                }
            }
        },

        /**
         * Vorbereiten des Moduls
         * 
         * 
         */
        _prepare: function () {
            var self = this,
                    element = self.element,
                    o = self.options;
            self.table = Selectors.q('#tags-table');
            self.buttons = Selectors.qa('.button-toolbar > button', element);
            self.message = Selectors.q('textarea', element);
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        }

    };

    return CategoryApprovals;

});

