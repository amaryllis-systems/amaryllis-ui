/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */

/**
 * Core Maintenance view
 * 
 * @param {type} Utils
 * @param {type} Selectors
 * @param {type} AcmsEvent
 * 
 * @returns {CacheMaintenance}
 */
define("view/admin/maintenance", [
    'tools/utils',
    'core/selectors',
    'events/event'
], function (Utils, Selectors, AcmsEvent) {

    var CacheMaintenance = function (element, options) {
        this.element =
                this.options =
                this.container =
                this.triggers = null;
        this.initialize(element, options);
    };

    CacheMaintenance.MODULE = "System Wartung";

    CacheMaintenance.VERSION = "1.1.0";

    CacheMaintenance.DEFAULT_OPTIONS = {
        remote: null,
        container: '#maintenance',
        tagname: 'button'
    };

    CacheMaintenance.needInit = true;

    CacheMaintenance.init = function (element, options) {
        var cm = new CacheMaintenance(element, options);

        return cm;
    };

    CacheMaintenance.prototype = {
        constructor: CacheMaintenance,

        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },

        listen: function () {
            var self = this,
                    element = self.element,
                    o = self.options,
                    triggers = self.triggers, j;
            for (j = 0; j < triggers.length; j++) {
                AcmsEvent.add(triggers[j], 'click', self._onClick.bind(self));
            }

        },

        _onClick: function (evt) {
            evt.preventDefault();
            var self = this,
                    element = self.element,
                    o = self.options, target, done, fData;
            target = evt.target;
            if (target.tagName.toLowerCase() !== o.tagname.toLowerCase()) {
                target = Selectors.closest(target, o.tagname);
            }
            done = function (response) {
                Acms.Notifications.createFromResponse(response);
            }
            fData = new FormData();
            fData.append('name', target.acmsData('name'));
            fData.append('type', target.acmsData('type'));
            fData.append('op', 'clear');
            require(['http/request', 'notifier'], function (Request) {
                Request.post(o.remote, fData, done);
            });
        },

        getDefaultOptions: function () {
            return CacheMaintenance.DEFAULT_OPTIONS;
        },

        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },

        _prepare: function () {
            var self = this,
                    o = self.options,
                    element = self.element;
            self.container = document.querySelector(o.container);
            var trigger = self.container.acmsData('listen');
            self.triggers = Selectors.qa('.' + trigger, self.container);
        }
    };

    return CacheMaintenance;

});
