/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('view/admin/feedback', [
    'core/classes',
    'core/selectors',
    'events/event'
], (Classes, Selectors, AcmsEvent) => {
    const FeedbackForm = function (element) {
        this.element = null;
        this.initialize(element);
    };
    FeedbackForm.MODULE = "Kunden Feedback";

    FeedbackForm.needInit = true;

    FeedbackForm.init = element => {
        const f = new FeedbackForm(element);
        return f;
    };

    FeedbackForm.prototype = {

        constructor: FeedbackForm,

        initialize(element) {
            this.element = element;
            this.listen();
        },

        listen() {
            const self = this;
            const element = self.element;
            const btn = element.querySelector('button[type=submit]');
            const dismiss = Selectors.qa('[data-dismiss=modal]', element);
            let j;

            const res = () => {
                self.element.reset()
            };

            const cb = e => {
                e.preventDefault();
                self._sendFeedback(e);
                return false;
            };

            AcmsEvent.add(btn, 'click', cb);
            for (j = 0; j < dismiss.length; j++) {
                AcmsEvent.add(dismiss[j], 'click', res);
            }
        },

        _sendFeedback(e) {
            const self = this, target = e.target, form = Selectors.closest(target, 'form'), fData = new FormData(form), action = form.getAttribute('action');
            require(['http/request', 'notifier'], Request => {
                if (true !== self._validate(form)) {
                    Acms.Notifications.create({
                        'type': Acms.Notifications.TYPE_DANGER,
                        'title': 'Fehler',
                        'message': 'Sie müssen das Formular vollständig ausfüllen'
                    });
                    return false;
                }
                Request.post(action, fData, self._done.bind(self));
            });
        },

        _done(response) {
            Acms.Notifications.createFromResponse(response);
            const self = this, element = self.element;
            if (response.status === 'success') {
                element.reset();
                const dismiss = Selectors.q('[data-dismiss=modal]', element);
                dismiss && AcmsEvent.fireClick(dismiss);
            }
        },

        _validate(form) {
            const element = this.element;
            const name = form.querySelector("input[name=name]").value, email = form.querySelector("input[name=email]").value, msg = form.querySelector("textarea[name=text]").value;
            if (!name || !email.isEmail() || !msg) {
                console.log(`Name: ${name}`);
                console.log(`Msg: ${msg}`);
                console.log(`Email: ${email}`);
                return false;
            }
            return true;
        }
    };

    return FeedbackForm;
});
