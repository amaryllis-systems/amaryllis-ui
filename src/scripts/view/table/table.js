/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define('view/table/table', [
    'tools/object/extend',
    'core/selectors',
    'core/classes',
    'events/event',
    'core/acms'
], (Extend, Selectors, Classes, AcmsEvent, A) => {
    /**
     * Constructor
     * 
     * @param {Element} element Das HTML-Element
     * @param {Object} options  Die Optionen
     * 
     * @returns {Table}
     */
    const Table = function(element, options) {
        this.element = 
        this.options =
        this.checkboxes = 
        this.checked =
        this.triggers = null;
        this.initialize(element, options);
    };

    /**
     * Muss das Modul initialisiert werden?
     * 
     * @var boolean
     */
    Table.needInit = true;

    /**
     * Modul Initialisierung
     * 
     * @param {Element} element Das HTML-Element
     * @param {Object} options  Die Optionen
     * 
     * @returns {Table}
     */
    Table.init = (element, options) => {
        const t = new Table(element, options);
        t.element.acmsData(Table.NS, t);
        
        return t;
    };

    /**
     * Standard-Optionen
     * 
     * <ul>
     *  <li>
     *      <strong>remote</strong> <i>{String}</i><br>
     *      Die Remote-URL für die Trigger. [Standard: NULL]
     *  </li>
     *  <li>
     *      <strong>toggle</strong> <i>{String}</i><br>
     *      Der CSS-Selector für die Toggle-All Buttons. 
     *      [Standard: '.acms-table-toggle-all']
     *  </li>
     *  <li>
     *      <strong>trigger</strong> <i>{String}</i><br>
     *      Der CSS-Selector für die Trigger-Buttons. [Standard: 'button[data-trigger]']
     *  </li>
     *  <li>
     *      <strong>input</strong> <i>{String}</i><br>
     *      [Standard: 'input[type="checkbox"]']
     *  </li>
     *  <li>
     *      <strong>batchdelete</strong> <i>{String}</i><br>
     *      Der OP-Name für das Entfernen mehrere ausgewählter Elemente. [Standard: 'batchdelete']
     *  </li>
     *  <li>
     *      <strong>active</strong> <i>{String}</i><br>
     *      CSS-Klasse für den Toggle-All Button, wenn er aktiviert wurde. [Standard: 'active']
     *  </li>
     *  <li>
     *      <strong>postname</strong> <i>{String}</i><br>
     *      POST-Name für die ausgewählten Elemente bei einer Trigger-Operation. 
     *      Der POST-Name enthält alle ausgewählten Elemente als indizierten Array 
     *      mit dem Wert der Checkbox. [Standard: 'ids']
     *  </li>
     * </ul>
     * 
     * @var {Object}
     */
    Table.DEFAULT_OPTIONS = {
        remote: null,
        toggle: '.acms-table-toggle-all',
        trigger: 'button[data-trigger]',
        input: 'input[type="checkbox"]',
        batchdelete: 'batchdelete',
        active: 'active',
        postname: 'ids'
    };

    /**
     * Table Module Namespace
     * 
     * @var {String}
     */
    Table.NS = "acms.view.table.table";

    /**
     * Klassen Definitionen
     * 
     * @type {Object}
     */
    Table.prototype = {
        
        /**
         * Interner Constructor
         * 
         * @param {Element} element Das HTML-Element
         * @param {Object} options  Die Optionen
         * 
         * @returns {Table}
         */
        initialize(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },
        
        
        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {Table.DEFAULT_OPTIONS}
         */
        getDefaultOptions() {
            return Table.DEFAULT_OPTIONS;
        },
        
        /**
         * Bildet die Optionen
         *
         * <p>
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         * </p>
         *
         * @param {Object} options
         *
         * @returns {Object|Table.DEFAULT_OPTIONS} Die gebildeten Optionen
         */
        buildOptions(options) {
            return Extend.flat(
                    {},
                    false,
                    this.getDefaultOptions(),
                    options,
                    this.element.acmsData()
                    );
        },
        
        /**
         * Aufsetzen der Event Listener
         * 
         * 
         */
        listen() {
            const self = this;
            const o = self.options;
            const toggles = self.toggles;
            const triggers = self.triggers;
            let i;
            let j;
            if(self.checkboxes.length) {
                self.checkboxes.forEach(chb => {
                    AcmsEvent.add(chb, 'change', self._onChange);
                });
            }
            if(toggles.length > 0) {
                for (i = 0; i < toggles.length; i++) {
                    AcmsEvent.add(toggles[i], 'click', self._onToggle);
                }
            }
            if(triggers.length > 0) {
                for (j = 0; j < triggers.length; j++) {
                    AcmsEvent.add(triggers[j], 'click', self._onTrigger);
                }
            }
        },
        
        /**
         * Event Handler für die Input-Elemente der Tabelle (Checkboxen)
         * 
         * Der Handler wird ausgeführt, wenn sich eines der Input-Elemente 
         * ändert und aktiviert/deaktiiert entsprechend die Trigger.
         * 
         * @param {Event} e         Das Event
         * 
         * 
         */
        _onChange(e) {
            const self = this;
            const o = self.options;
            let checked = 0;
            self.checkboxes.forEach(chb => {
                if(chb.checked) {
                    checked++;
                }
            });
            self.triggers.forEach(trigger => {
                if(e.target.checked || checked > 0) {
                    trigger.removeAttribute('disabled');
                    trigger.setAttribute('aria-disabled', 'false');
                    Classes.removeClass(trigger, 'disabled');
                } else {
                    trigger.setAttribute('disabled', 'disabled');
                    trigger.setAttribute('aria-disabled', 'true');
                    Classes.addClass(trigger, 'disabled');
                }
            });
        },
        
        /**
         * Event Handler für den "Toggle-All" Button der Checkboxen
         * 
         * Der Handler wird ausgeführt, wenn der Toggle-All Button der Tabelle  
         * geklickt wird und aktiviert/deaktiviert entsprechend alle Checkboxen
         * 
         * @param {Event} e     Das Event
         * 
         * 
         */
        _onToggle(e) {
            e.preventDefault();
            const self = this;
            const o = self.options;
            let btn = e.target;
            let i;
            let j;
            const toggles = self.toggles;
            if(btn.tagName.toLowerCase() !== 'button') {
                btn = Selectors.closest(btn, 'button');
            }
            const chbx = self.checkboxes;
            let active;
            setTimeout(() => {

                if (Classes.hasClass(btn, o.active)) {
                    active = false;
                } else {
                    active = true;
                }
                for (i = 0; i < toggles.length; i++) {
                    self._toggleButton(active, toggles[i]);
                }
                for (j = 0; j < chbx.length; j++) {
                    chbx[j].checked = active;
                    if(active === true) {
                        chbx[j].setAttribute('checked', 'checked');
                    } else {
                        chbx[j].removeAttribute('checked');
                    }
                    AcmsEvent.fireChange(chbx[j]);
                }
            }, 150);
        },
        
        /**
         * Event Handler für einen Batch-Button
         * 
         * Triggert die OP eines einzelnen Trigger-Buttons für alle Elemente der 
         * Tabelle, die ausgewählt sind.
         * 
         * @param {Event} e     Das Event
         * 
         * 
         */
        _onTrigger(e) {
            const self = this;
            const o = self.options;
            const checked = [];
            const fData = new FormData();
            let target = e.target;
            if(target.tagName.toLowerCase() !== 'button') {
                target = Selectors.closest(target, 'button');
            }
            self.currentop = target.acmsData('trigger').trim();
            self.checked = [];
            self.checkboxes.forEach(chb => {
                if(chb.checked) {
                    checked.push(chb.value);
                    self.checked.push(chb);
                }
            });
            if(!checked.length) {
                self.checked = null;
                return;
            }
            fData.append(o.postname, checked);
            fData.append('op', self.currentop);
            require(['http/request', 'notifier'], Request => {
                Request.post(o.remote, fData, self._onTriggerSucceed, self._fail, self._always);
            });
        },
        
        /**
         * Wird ausgeführt, wenn ein einzelner Trigger abgeschlossen wurde
         * 
         * @param {Object} response     Die JSON Response Daten als Objekt
         * 
         * 
         */
        _onTriggerSucceed(response) {
            A.Notifications.createFromResponse(response);
            if(response.status !== 'success') {
                return;
            }
            const self = this;
            const o = self.options;
            let row;
            let i;
            const checked = self.checked;
            for(i = 0; i < checked.length; i++) {
                checked[i].removeAttribute('checked');
                AcmsEvent.fireChange(checked[i]);
                if(self.currentop !== o.batchdelete) {
                    continue;
                }
                row = Selectors.closest(checked[i], 'tr');
                row && row.parentNode.removeChild(row);
            }
            setTimeout(() => {
                self.checked = null;
            }, 500);
        },
        
        /**
         * Ausführung bei fehlgeschlagener Abfrage
         * 
         * <p>
         *  Wird ausgeführt, wenn keine Antwort vom Server kommt bzw die 
         *  Anfrage fehlschlägt
         * </p>
         * 
         * @param {Object}      jqXHR    Das XHR Abfrage Object
         * 
         * 
         */
        _fail(jqXHR, textStatus) {
            A.Logger.writeLog(textStatus);
        },
        
        /**
         * Auführung bei jeder Abfrage
         * 
         * @param {String} textStatus Text Status
         * 
         * 
         */
        _always(textStatus) {
            A.Logger.writeLog(textStatus);
        },
        
        /**
         * Aktivieren/deaktivieren eines einzelnen toggle-All Buttons
         * 
         * Wird vom Event Handler des Toggle All Buttons ausgeführt und wechselt 
         * den Aktiv-Status eines einzelnen Toggles.
         * 
         * @param {Boolean} active  Der neue Status
         * @param {Element} btn     Das Button-Element des Toggles
         * 
         * 
         */
        _toggleButton(active, btn) {
            const self = this;
            const o = self.options;
            let icon = Selectors.q('.acms-icon', btn);
            Classes.toggleClass(btn, o.active);
            if(active === false && icon !== null) {
                Classes.removeClass(icon, 'acms-icon-check-square');
                Classes.addClass(icon, 'acms-icon-square');
            } else if(icon !== null) {
                Classes.removeClass(icon, 'acms-icon-square');
                Classes.addClass(icon, 'acms-icon-check-square');
            }
        },
        
        
        /**
         * Vorbereiten des Moduls
         * 
         * 
         */
        _prepare() {
            const self = this, element = self.element, o = self.options;
            self.triggers = Selectors.qa(o.trigger, element);
            self.checkboxes = Selectors.qa(o.input, element);
            self.toggles = Selectors.qa(o.toggle, element);
            for (const fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        }
    };

    return Table;
});
