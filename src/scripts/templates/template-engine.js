/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("templates/template-engine", [
    "tools/string/sprintf",
    "handlebars",
    "core/window",
    "events/event",
    "core/acms",
    "templates/helpers/helpers"
], (Sprintf, Handlebars, Win, AcmsEvent, A, Helpers) => {

    Handlebars.registerHelper(Helpers);

    if(typeof Theme !== 'undefined') {
        const hbs = Theme.hasOwnProperty('hbs') ? Theme.hbs : {};
        if('partials' in hbs) {
            Handlebars.registerPartial(hbs.partials);
        }
        if('helpers' in hbs) {
            Handlebars.registerHelper(hbs.partials);
        }
    }

    /**
     * Amaryllis UI uses [handlebars](http://handlebarsjs.com/ "Handlebars") to
     * render HTML Templates in JavaScript. a list of custom helpers 
     * we use is {@link https://jsdoc.amaryllis-ui.eu/module-templates_helpers_helpers.html|here}.
     * 
     * __Register custom helpers and partials__:
     * 
     * Most easiest way is to create a custom `Theme` object in `head` of your Amarylllis-CMS Theme
     * with property `hbs` for Handlebars extensions.
     * 
     * ``` js
     * const Theme = {};
     * Theme.hbs = {
     *  partials: {
     *      name_of_partial: 'template source string',
     *      button: '<button class="button {{cls}}" type="{{type}}">{{label}}</button>'
     *  },
     *  helpers: {
     *     custom1: (arg1, arg2, ctx) => {
     *       return arg1 + arg2;
     *     }
     *  }
     * };
     * ```
     * 
     * If your theme uses as custom init script, you can add event listeners to `document.body`.
     * 
     * ``` js
     * const cb = e => {
     *  const hbs = e.detail.hbs;
     *  const engine = e.detail.engine;
     *  hbs.registerHelper('custom1', (arg1, arg2, ctx) => {
     *      return arg1 + arg2;
     *  });
     * };
     * require(["init.min"], function() {
     *   const config = {
     *       skipDataMain: true,
     *       paths: {
     *           'THEMENAME': '/themes/THEMENAME/media/scripts'
     *       }
     *   };
     *   requirejs.config(config);
     * });
     * ```
     * 
     * If you want to register a helper/partial only in specific context, you can register them before compiling:
     * 
     * ``` js
     * define('THEMENAME/app/my-module', [
     *  "templates/template-engine", 
     *  "THEMENAME/template/helpers", 
     *  "text!THEMENAME/template/partials/mypartial.hbs"
     * ], (TemplateEngine, CustomHelpers, MyPartial) => {
     *  class MyModule {
     *      compileMyTemplate() {
     *          const hbs = TemplateEngine.hbs;
     *          // assuming your custom helpers script exports an object of helper => callable pairs.
     *          hbs.registerHelper(CustomHelpers);
     *          // reguster the required partial
     *          hbs.registerPartial('mypartial', MyPartial);
     *          // process compile the template
     *          const cb = (content) => {
     *              // process after template is rendered and appended
     *              }, 
     *              data = {title: "Modal Title", id: ...},
     *              appendTo = document.body,
     *              src = "THEMENAME/template/custom/custom-tpl.hbs";
     *          TemplateEngine.compileResource(src, data, cb, appendTo, 'beforeend');
     *      }
     *  }
     * });
     * ```
     *
     * @class
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class TemplateEngine {

        /**
         * Module Namespace
         * 
         * @type {String}
         *
         * @readonly
         * @static
         * @memberof TemplateEngine
         */
        static get NS() {
            return "acms.templates.template-engine";
        }

        /**
         * Gets the handlebars itself
         *
         * @readonly
         * @static
         * @memberof TemplateEngine
         */
        static get hbs() {
            return Handlebars;
        }

        /**
         * Compile and render the template
         * 
         * @static
         * 
         * @param {String}              source      Can either be a String-Template or an ID of a Template Wrapper (e.g. script-tag with id).
         * @param {Object}              data        Template Data Object
         * @param {HTMLElement|NULL}    appendTo    Optional Element, to prepend/append the compiled template
         * @param {String}              append      Specifies where to append/prepend the Element.
         *                                          - `beforebegin`: Before the element itself. 
         *                                          - `afterbegin`: Just inside the element, before its first child. 
         *                                          - `beforeend` Just inside the element, after its last child (default). 
         *                                          - `afterend` After the element itself.
         * 
         * @returns {String}    Template Content of final Template
         * 
         * @memberof TemplateEngine
         * 
         * @example
         * const tpl = '{{author/name}} has published "{{item/title}}"'
         * const data = {author: {name: 'Joe'}, item: {title: 'My Blog Post'}};
         * const appendTo = document.querySelector('#my-wrapper');
         * const append = 'beforeend'
         * TemplateEngine.compile(tpl, data, appemdTo, append);
         * // => 'Joe has published "My Blog Post"'
         */
        static compile(source, data, appendTo, append) {
            if (source.indexOf("#") === 0) {
                let ele = document.querySelector(source);
                source = ele.innerHTML;
            }
            data.Acms = A;
            data.AmaryllisCMS = Win.AmaryllisCMS;
            const template = Handlebars.compile(source);
            
            const content = template(data);
            if (appendTo) {
                //let wrapper = document.createElement("div");
                //wrapper.innerHTML = content;
                //while (wrapper.firstChild) {
                //    appendTo.appendChild(wrapper.firstChild);
                //}
                append = append || 'beforeend';
                appendTo.insertAdjacentHTML(append, content);

            }
            return content;
        }

        /**
         * Compile Template from remote resource (handlebars template file)
         * 
         * @param {String}              source      HTML Source (e.g. `templates/html/ui/informations/modal.hbs`)
         * @param {Object}              data        Template Data to assign to the template
         * @param {CallableFunction}    cb          The callback is called when the template rendering is complete
         * @param {HTMLElement|NULL}    appendTo    Optional Element, to prepend/append the compiled template
         * @param {('beforebegin'|'afterbegin'|'beforeend'|'afterend')} [append=beforeend]      
         *                                          Specifies where to append/prepend the Element.
         *                                          - `beforebegin`: Before the element itself. 
         *                                          - `afterbegin`: Just inside the element, before its first child. 
         *                                          - `beforeend` Just inside the element, after its last child (default). 
         *                                          - `afterend` After the element itself.
         * 
         * @static
         * @memberof TemplateEngine
         * 
         * @example 
         * let cb = (content) => {
         *     // process HTML Content
         *  }, 
         *  data = {title: "Modal Title", id: ...},
         *  appendTo = document.body,
         *  src = "templates/html/ui/informations/modal.hbs";
         * TemplateEngine.compileResource(src, data, cb, appendTo, 'beforeend');
         */
        static compileResource(source, data, cb, appendTo, append) {
            let self = this,
                src = Sprintf("text!%s!strip", source);
            require([src], function (tpl) {
                let content = self.compile(tpl, data, appendTo, append);
                if(typeof cb === 'function') {
                    cb(content);
                }
            });
        }

        /**
         * Compile Resource
         * 
         * @param {String[]}            source      HTML Source Array
         * @param {Object}              data        Template Data
         * @param {Callable}            cb          Calllback
         * @param {HTMLElement|NULL}    appendTo    Optional Element, to prepend/append the compiled template
         * @param {String}              append      Specifies where to append/prepend the Element.
         *                                          - `beforebegin`: Before the element itself. 
         *                                          - `afterbegin`: Just inside the element, before its first child. 
         *                                          - `beforeend` Just inside the element, after its last child (default). 
         *                                          - `afterend` After the element itself.
         * 
         * @static
         * @memberof TemplateEngine
         * 
         * @example 
         *      let cb = (contents) {
         *              // process HTML Contents
         *          }, 
         *          data = [{title: "Modal Title", id: ...}, {title: "Button Label", id: ...}],
         *          appendTo = [document.querySelector("body"), document.getElementById("mydiv")],
         *          src = ["path/to/tpl-1.hbs", "path/to/tpl-2.hbs"];
         *      TemplateEngine.compileResource(src, data, cb, appendTo);
         */
        static compileResources(source, data, cb, appendTo, append) {
            let self = this,
                contents = [],
                fn = (index) => {
                    let src = Sprintf("text!%s!strip", source[index]);
                    require([src], (tpl) => {
                        let appending = (typeof appendTo[index] !== "undefined" ? appendTo[index] : false);
                        let content = self.compile(tpl, data[index], appending, append);

                        contents[index] = content;
                        if (contents.length === source.length && typeof cb === 'function') {
                            cb(contents);
                        }
                    });
                };
            for (let j = 0; j < source.length; j++) {
                fn(j);
            }
        }

    }
    
    AcmsEvent.emit(document.body, TemplateEngine.NS, {engine: TemplateEngine, hbs: Handlebars});


    return TemplateEngine;
});
