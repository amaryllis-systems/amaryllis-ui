/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("templates/helpers/helpers", [
    "tools/object/extend",
    "tools/is-empty",
    "tools/date/year",
    "moment",
    "core/acms",
    "core/translator"
], (Extend, isEmpty, Year, moment, A) => {

    /**
     * Template Helpers
     * 
     * Provides some Helper Methods for Handlebars Templates
     * 
     * @exports templates/helpers/helpers
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    const TemplateHelpers = {

        /**
         * Handlebars Helper "assign"
         * 
         * ``` hbs
         * {{assign 'scope' 'auhtor'}}
         * {{@root.scope}} // author
         * ```
         * 
         * @param {String}  varName     Name of the new variable
         * @param {*}       value       Value to assign
         * @param {Object}  options     Handlebars internal object
         */
        assign: (varName, varValue, options) => {
            if (!options.data.root) {
                options.data.root = {};
            }
            options.data.root[varName] = varValue;
        },

        /**
         * Prints the current year
         *
         * @param {String} context Optional format (`YYYY` or `YY`)
         * @param {Object} block
         * 
         * @returns {String}
         */
        Year: (context, block) => {
            if(context && context.hash) {
                block = Extend.flat({}, false, context);
                context = undefined;
            }
            return Year(context);
        },

        /**
         * Moment JS Helper for Handlebars
         * 
         * ``` hbs
         * {{moment article/pdate-web format="dd-MM-YYYY" locale="en"}}
         * ```
         * 
         * @param {String} context  Optional string to create the date from (ISO formatted date)
         * @param {Object} block
         * 
         * @returns {String} formatted Date
         */
        moment: (context, block) => {
            if (context && context.hash) {
                block = Extend.flat({}, false, context);
                context = undefined;
            }
            let date = moment(context);

            if (block.hash.timezone) {
                date.tz(block.hash.timezone);
            }

            let hasFormat = false;

            date.locale(A.Translator.currentLanguage.substr(0, 2));

            for (let i in block.hash) {
                if (i === 'format') {
                    hasFormat = true;
                } else if (date[i]) {
                    date = date[i](block.hash[i]);
                } else {
                    A.Logger.writeLog('moment.js does not support option "' + i + '"');
                }
            }

            if (hasFormat) {
                date = date.format(block.hash.format);
            }
            return date;
        },

        
        /**
         * custom if 
         * 
         * ``` hbs
         * // if(list.length>0 && public){}
         * {{#aif list.length '>' 0 '&&' public '==' true}} 
         *      <p>condition satisfied</p>
         * {{else}}
         *      <p>condition not satisfied</p>
         * {{/aif}}
         * ```
         * 
         * @param {Mixed}   v1  Value 1 to compare
         * @param {String} o1   Operator 1
         * @param {String} v2   Value 2 to compare
         * @param {String} mainOperator Main Operator
         * @param {String} v3   Value 3 to compare
         * @param {String} o2   Operator 2
         * @param {String} v4   Value 4 to compare
         * @param {String} options Handlebars internal options
         * 
         */
        aif: (v1,o1,v2,mainOperator,v3,o2,v4,options) => {
            let operators = {
                 '==': function(a, b){ return (a == b);},
                 '===': function(a, b){ return (a === b);},
                 '!=': function(a, b){ return (a != b);},
                 '!==': function(a, b){ return (a !== b);},
                 '<': function(a, b){ return (a < b);},
                 '<=': function(a, b){ return (a <= b);},
                 '>': function(a, b){ return (a > b);},
                 '>=': function(a, b){ return (a >= b);},
                 '&&': function(a, b){ return (a && b);},
                 '||': function(a, b){ return (a || b);},
            };
            const a1 = operators[o1](v1,v2);
            const a2 = operators[o2](v3,v4);
            const isTrue = operators[mainOperator](a1, a2);

            return isTrue ? options.fn(this) : options.inverse(this);
        },

        /**
         * xif 
         * 
         * Another if-else helper for simple comparison of two values.
         * 
         * ``` hbs
         * {{#xif content/parentid '>' 0}}
         *  'child'
         * {{else}}
         *  'default'
         * {{/xif}}
         * ```
         *
         * @param {*}       v1          Value 1 to compare
         * @param {String}  operator    Operator
         * @param {*}       v2          Value 2 to cpmpare
         * @param {Object}  options
         * 
         */
        xif: (v1, operator, v2, options) => {
            let isTrue;
            switch (operator) {
                case '==':
                    isTrue = (v1 == v2);
                    break;
                case '===':
                    isTrue = (v1 === v2);
                    break;
                case '!=':
                    isTrue = (v1 != v2);
                    break;
                case '!==':
                    isTrue = (v1 !== v2);
                    break;
                case '<':
                    isTrue = (v1 < v2);
                    break;
                case '<=':
                    isTrue = (v1 <= v2);
                    break;
                case '>':
                    isTrue = (v1 > v2);
                    break;
                case '>=':
                    isTrue = (v1 >= v2);
                    break;
                case '&&':
                    isTrue = (v1 && v2);
                    break;
                case '||':
                    isTrue = (v1 || v2);
                    break;
                default:
                    throw new Error('Unknown Operator in xif');
            }
            if(options && typeof options.fn === 'function') {

                return isTrue ? options.fn(this) : options.inverse(this);
            }
            return isTrue;
        },

        /**
         * Handlebars Helper "t"
         * 
         * translates a given id from textdomain and locale
         * 
         * ``` hbs
         * {{t 'Willkommen' 'Core'}} // 'Willkommen'
         * ```
         *
         * @param {String} id
         * @param {String} textdomain
         * @param {String} locale
         * 
         * @returns {String}    translated string or `id`
         */
        t: (id, textdomain, locale, context) => {
            if(false === (typeof id === 'string')) {
                return;
            }
            if (textdomain && textdomain.hash) {
                context = Extend.flat({}, false, textdomain);
                textdomain = "Core";
            }
            if(locale && locale.hash) {
                context = Extend.flat({}, false, locale);
                locale = undefined;
            }
            return A.Translator._(id, textdomain, locale);
        },

        /**
         * Checks if an parameter is empty
         * 
         * ``` hbs
         * {{#isEmpty value}}
         * {{else}}
         * {{/isEmpty}}
         * ```
         *
         * @param {*} value The value to check
         */
        isEmpty: (value, options) => {
            const isTrue = isEmpty(value);
            
            if(options && typeof options.fn === 'function') {

                return isTrue ? options.fn(this) : options.inverse(this);
            }
            return isTrue;
        },

        /**
         * Checks if an parameter is an object
         * 
         * ``` hbs
         * {{#isObject value}}
         * {{else}}
         * {{/isObject}}
         * ```
         *
         * @param {*} value The value to check
         * 
         */
        isObject: (value, options) => {
            const isTrue = (value && typeof value === 'object');
            
            if(options && typeof options.fn === 'function') {

                return isTrue ? options.fn(this) : options.inverse(this);
            }
            return isTrue;
        },

        /**
         * Simple iteration counter
         * 
         * ``` hbs
         * {{#each data as |v k|}}
         *  {{counter @index}}
         * {{/each}}
         * ```
         *
         * @param {Number} index    current index
         * 
         * @returns {Number}        incremented number
         */
        counter: index => {
            return index +1;
        },

        /**
         * Converts an object to json
         * 
         * ``` hbs
         * {{toJson data}}
         * ```
         *
         * @param {Object} value The value to convert
         * 
         * @returns {String}    JSON String
         */
        toJson: (value) => {
            return JSON.stringify(value);
        },

        
        /**
         * Log
         * 
         * ``` hbs
         * {{log article category}}
         * ```
         *
         * @param {...*} args Arguments to debug
         */
        log: (...args) => {
            
            let last = args[args.length - 1];
            if (args.length > 1 && last && typeof last === 'object' && last.hash) {
                args.pop();
            }
            console.log(args);
        },
        
        /**
         * String to lower
         * 
         * ``` hbs
         * {{toLowerCase value}}
         * {{#toLowerCase}}{{strVal}}{{/toLowerCase}}
         * ```
         *
         * @param {String} value
         * @param {Object} options
         * 
         * @returns {String} Lower cased `value`
         */
        toLowerCase: (value, options) => {
            if(options && typeof options.fn === 'function') {
                return options.fn(this).toLowerCase();
            }
            if(typeof value !== 'string' || isEmpty(value)) {
                return '';
            }

            return value.toLowerCase();
        },

        
        /**
         * String to upper
         *
         * ``` hbs
         * {{toUpperCase value}}
         * {{#toUpperCase}}{{strVal}}{{/toUpperCase}}
         * ```
         *
         * @param {String} value
         * @param {Object} options
         * 
         * @returns {String} uppercased `value`
         */
        toUpperCase: (value, options) => {
            if(options && typeof options.fn === 'function') {
                return options.fn(this).toUpperCase();
            }
            if(typeof value !== 'string' || isEmpty(value)) {
                return '';
            }

            return value.toUpperCase();
        },

                
        /**
         * Returns true if `key` is an own, enumerable property
         * of the given `context` object.
         *
         * ```hbs
         * {{hasOwnProp context key}}
         * {{#hasOwnProp context key}}
         *  // is true
         * {{else}}
         *  // not found
         * {{/hasOwnProp}}
         * ```
         *
         * @param {Object} context The context object.
         * @param {String} key      The key to check
         */
        hasOwnProp: (context, key, options) => {
            const hasOwn = Object.hasOwnProperty;
            const isTrue = hasOwn.call(context, key);

            if(options && typeof options.fn === 'function') {

                return isTrue ? options.fn(this) : options.inverse(this);
            }
            return isTrue;
        }
    };

    return TemplateHelpers;
});
