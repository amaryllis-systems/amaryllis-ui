/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @copyright 2013 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright 2014 - 2018 Amaryllis Systems GmbH http://www.amws.eu/
 * @copyright 2017 - 2018 Amaryllis CMS Project http://www.amaryllis-cms.de/
 * @license https://www.amaryllis-cms.de/lizenzen/
 * @license Apache Licence 2.0 https://www.apache.org/licenses/LICENSE-2.0
 *
 * Dual-Licensed with Amaryllis CMS EULA. You may obtain a copy of the
 * EULA at
 *
 *     https://www.amaryllis-cms.de/lizenzen/eula/
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

let oldie = document.querySelector(".old-ie");

let baseUri = (typeof Acms !== "undefined" ? Acms.Urls.baseUrl : '') + '/',
    basePath = baseUri + 'media/scripts/',
    libs = basePath + 'libs/';

let config = {
    baseUrl: baseUri,
    useMinified: true,
    paths: {
        /* Apps */
        apps: basePath + 'apps',
        initial: basePath + 'core/boot',
        notifier: basePath + 'ui/app/notifier',
        core: basePath + 'core',
        device: basePath + 'device',
        editor: basePath + 'editor',
        events: basePath + 'events',
        http: basePath + 'http',
        router: basePath + 'router',
        templates: basePath + 'templates',
        tools: basePath + 'tools',
        ui: basePath + 'ui',
        view: basePath + 'view',
        /* Libs */
        libs: basePath + 'libs',
        handlebars: libs + 'handlebars/handlebars',
        sortable: libs + 'sortable/Sortable',
        editors: libs + 'editors',
        moment: libs + 'moment/moment',
        async: libs + 'requirejs-plugins/async',
        text: libs + 'requirejs-plugins/text',
        html: libs + 'requirejs-plugins/html',
        tinyMCE: libs + 'editors/tinymce/tinymce',
        /** Translator */
        'core-translations': 'translations',
        'package': basePath+'version.json'
    },
    shim: {
        moment: {},
        prism: {
            exports: "Prism"
        },
        tinyMCE: {
            exports: 'tinyMCE',
            init: function () {
                this.tinyMCE.DOM.events.domLoaded = true;
                this.tinyMCE.baseURL = baseUri + "/media/scripts/libs/editors/tinymce/";
                return this.tinyMCE;
            }
        },
        handlebars: {
            exports: "Handlebars"
        }
    },
    waitSeconds: 20
};


if (typeof Acms !== "undefined") {
    if (Acms.Modules && Acms.Modules.length) {
        let modules = Acms.Modules,
            j, module, lower;
        for (j = 0; j < modules.length; j++) {
            module = modules[j];
            lower = module.toLowerCase();
            if (lower === 'core') {
                continue;
            }
            config.paths[lower + '-module'] = 'modules/' + module + '/media/scripts';
            config.paths[lower + '-module-translations'] = 'modules/' + module + '/translations';
        }
    }
    if (typeof Acms.Theme === "object" && Acms.Theme.foldername) {
        let foldername = Acms.Theme.foldername,
            lower = foldername.toLowerCase();
        config.paths[lower + '-theme'] = 'themes/' + foldername + '/media/scripts';
        config.paths[lower + '-theme-translations'] = 'themes/' + foldername + '/translations';
    }
}


requirejs.config(config);

try {
    require(['initial'], function (Boot) {
        
        try {
            let app = new Boot();
            app.run();
        } catch (E) {
            console.log('Error on init in ' + E.fileName + '::' + E.lineNumber + '!'+ "\n" + E.message);

        }
    });
} catch (E) {
    console.log('Error on load in ' + E.fileName + '::' + E.lineNumber + '!'+ "\n" + E.message);
}
