/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('core/element', ["core/data"], (Data) => {

    /**
     * Element Manipulation
     * 
     * @exports core/element
     */
    let el = Element;

    Element.prototype.remove = () => {
        let parent = this.parentElement;
        if (parent) {
            parent.removeChild(this);
        }
    }

    /**
     * Zugriff auf das AcmsData objekt
     * 
     * @param {Mixed} a String, Objekt
     * @param {Mixed} b  NULL, undefined, String
     * @param {Boolean} updateElement TRUE um das HTML Element zu aktualisieren
     * 
     * @returns {Element.prototype}
     */
    Element.prototype.acmsData = function(a, b, updateElement) {
        if (!this._acmsData) {
            this._acmsData = new Data(this);
        }
        if (!a || typeof a === "string" && (typeof(b) === "undefined")) {
            return this._acmsData.get(a, b, updateElement);
        }
        this._acmsData.set(a, b, updateElement);
        return this;
    }

    /**
     * Setzt die Höhe des Elements oder gibt die Höhe des Elements zurück,
     * wenn height nicht angegeben ist
     * 
     * @param {integer} height Die neue Höhe, wenn man eine Höhe setzen will
     * 
     * @returns {Element.clientHeight|Element.prototype}
     */
    Element.prototype.height = function(height) {
        if (height !== undefined) {
            this.clientHeight = height;
            return this;
        }
        return this.clientHeight;
    }

    /**
     * Gibt die Höhe des Elements inklusive Padding zurück
     * 
     * @returns {Element.offsetHeight|Element.prototype}
     */
    Element.prototype.outerHeight = function(height) {
        if (height !== undefined) {
            this.offsetHeight = height;
            return this;
        }
        return this.offsetHeight;
    }

    /**
     * Gibt die Breite des Elements zurück
     * 
     * @returns {Element.clientWidth|Element.prototype}
     */
    Element.prototype.width = function(width) {
        if (width !== undefined) {
            this.clientWidth = width;
            return this;
        }
        return this.clientWidth;
    }

    /**
     * Gibt die Äußere Breite zurück oder setzt die Breite
     * @returns {Element.prototype|width}
     */
    Element.prototype.outerWidth = function(width) {
        if (width !== undefined) {
            this.offsetWidth = width;
            return this;
        }
        return this.offsetWidth;
    }

    Element.prototype.offset = function() {
        // Support: IE<=11+
        // Das aufrufen von getBoundingClientRect eines nicht konnektierten 
        // Elements wirft einen Error aus im Internet Explorer
        let clRects = this.getClientRects(),
            rect, doc, win, docElem;
        if (!clRects.length) {
            return { top: 0, left: 0 };
        }
        rect = this.getBoundingClientRect();
        if (rect.width || rect.height) {
            doc = this.ownerDocument;
            win = this.getWindow();
            docElem = doc.documentElement;
            return {
                top: rect.top + win.pageYOffset - docElem.clientTop,
                left: rect.left + win.pageXOffset - docElem.clientLeft
            };
        }
        return rect;
    }

    Element.prototype.after = function(el) {
        let parent = this.parentNode;
        if (!el instanceof Node) {
            el = el.get(0)
        }
        if (el instanceof Node) {
            this.parentNode.insertBefore(el, this.nextSibling);
        }
    }

    Element.prototype.before = function(el) {
        let parent = this.parentNode;
        if (!el instanceof Node) {
            el = el.get(0)
        }
        if (el instanceof Node) {
            this.parentNode.insertBefore(el, this);
        }
    }

    Element.prototype.outerOffset = function() {
        return {
            left: this.offsetLeft,
            top: this.offsetTop,
            width: this.offsetWidth,
            height: this.offsetHeight,
            parent: this.offsetParent
        }
    }

    Element.prototype.getWindow = function() {
        let doc = this.ownerDocument;
        if (window !== doc.parentWindow) {
            doc.parentWindow.execScript("document._parentWindow = window;", "Javascript");
            let win = doc._parentWindow;
            doc._parentWindow = null;
            return win;
        }
        return doc.parentWindow || doc.defaultView;
    }

    Element.prototype.matches = (Element.prototype.matches ||
        Element.prototype.mozMatchesSelector ||
        Element.prototype.msMatchesSelector ||
        Element.prototype.oMatchesSelector ||
        Element.prototype.webkitMatchesSelector ||
        Element.prototype.webkitMatchesSelector);



    NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
        for (let i = this.length - 1; i >= 0; i--) {
            if (this[i] && this[i].parentElement) {
                this[i].parentElement.removeChild(this[i]);
            }
        }
    }



    return Element;
});
