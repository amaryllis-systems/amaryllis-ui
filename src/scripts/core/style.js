/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('core/style', [], () => {
    
    const fbs = ['-webkit-', '-ms-', '-moz-', '-o-', '-khtml-'];
    const hfb = [
        'transform',
        'transition',
        'borderRadius'
    ];
    

    /**
     * Style Rule Manipulation
     *
     * @class Style
     */
    class Style {

        static get fbs() {
            return fbs;
        }

        static get hfb() {
            return hfb;
        }


        /**
         * Gibt den Wert einer CSS-Eigenschaft zurück
         * 
         * <p>Es wird zuerst versucht der berechnete Style zu ermitteln. Scheitert 
         * dies, wird das Style-Attribut ausgelesen</p>
         * 
         * <pre><code>
         *  let elem = document.querySelector('#test');
         *  Style.getStyle(elem, 'font-size'); // z.B. '20px'
         *  Style.getStyle(elem, 'marginLeft'); // z.B. '20px';
         * </code></pre>
         * 
         * @param {Node} el
         * @param {String} styleProp
         * @param {mixed} def Standard-Wert
         * 
         * @returns {mixed}
         */
        static getStyle(el, styleProp, def) {
            def = def || '0';
            let style = el.style,
                prop;
            if (el.currentStyle) {
                style = el.currentStyle;
            } else if (document.defaultView && document.defaultView.getComputedStyle) {
                style = document.defaultView.getComputedStyle(el, '');
            }
            if (typeof styleProp !== 'string') {
    
                return style;
            }
            prop = Style._camelize(styleProp);
            return (typeof style[prop] !== "undefined") ? style[prop] : def;
        }
    
    
        /**
         * Setzt ein Style Property
         * 
         * <pre><code>
         *  let elem = document.querySelector('#test');
         *  Style.setStyle(elem, 'font-size', '20px');
         *  Style.setStyle(elem, 'marginLeft', '20px');
         * </code></pre>
         * 
         * @param {Node}        el          Das Element
         * @param {String}      styleProp   Die Eigenschaft
         * @param {String}      value       Der Wert
         * 
         * @returns {unresolved}
         */
        static setStyle(el, styleProp, value) {
            let prop = Style._camelize(styleProp),
                j, k;
            for (j = 0; j < Style.hfb.length; j++) {
                if (Style.hfb[j] === prop) {
                    for (k = 0; k < Style.fbs.length; k++) {
                        el.style[Style.fbs[k] + prop] = value;
                    }
                    break;
                }
            }
            el.style[prop] = value;
        }
    
    
        /**
         * Setzt mehrere Styles
         * 
         * @param {Node}    el  Das Node Element
         * @param {Object}  css Ein Objekt an CSS Styles
         * 
         * 
         */
        static setStyles(el, css) {
            let prop;
            for (prop in css) {
                if (css.hasOwnProperty(prop)) {
                    Style.setStyle(el, prop, css[prop]);
                }
            }
        }
    
        /**
         * Camelize
         * 
         * @param {String} str
         * 
         * @returns {String}
         */
        static _camelize(str) {
            return str.replace(/\-(\w)/g, function (str, letter) {
                return letter.toUpperCase();
            });
        }
    
        static _dimension(elm) {
            let elmHeight, elmMargin;
            if (document.all) { // IE
                elmHeight = elm.currentStyle.height;
                elmMargin = parseInt(elm.currentStyle.marginTop, 10) + parseInt(elm.currentStyle.marginBottom, 10) + "px";
            } else { // Mozilla
                elmHeight = document.defaultView.getComputedStyle(elm, '').getPropertyValue('height');
                elmMargin = parseInt(document.defaultView.getComputedStyle(elm, '').getPropertyValue('margin-top')) + parseInt(document.defaultView.getComputedStyle(elm, '').getPropertyValue('margin-bottom')) + "px";
            }
            return (elmHeight + elmMargin);
        }
    
    
    }
    
    return Style;

});
