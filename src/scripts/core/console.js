/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("core/console", [], () => {

    /**
     * Wrapper für die console
     *
     * Außerhalb des IE ist die Console immer vohanden. MS braucht mal wieder
     * Extra-Arbeit.
     * @exports core/console
     */
    let con = console || window.console;

    if (typeof console === "undefined" || !console.log) {
        console = {
            log: function (msg) {
                return;
            },
            trace: function () {
                return;
            }
        };
            
        Object.defineProperty(window, "console", {
            value: console,
            writable: false,
            configurable: false
        });

        let i = 0;

        let showWarningAndThrow = () => {
            if (!i) {
                setTimeout(() => {
                    console.log("%cWarning message", "font: 2em sans-serif; color: yellow; background-color: red;");
                }, 1);
                i = 1;
            }
            throw "Console is disabled";
        }

        let l, n = {
            set: function (o) {
                l = o;
            },
            get: function () {
                showWarningAndThrow();
                return l;
            }
        };
        Object.defineProperty(console, "_commandLineAPI", n);
        Object.defineProperty(console, "__commandLineAPI", n);
    }

    return console;

});
