/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('core/string', [
    'core/acms'
], (A) => {

    /**
     * String Wrapper für zusätzliche String Funktionen
     * 
     * Ergänzt String prototyp Methoden mit zusätzlichen Methoden zur schnellen 
     * Anwendung in Strings.
     * 
     * @exports core/string
     */
    const strng = String;

    const JSON_FILTER = /^\/\*-secure-([\s\S]*)\*\/\s*$/;

    if (typeof String.prototype.startsWith !== 'function') {
        (function (Object, String, RegExp, max, min) {
          Object.defineProperty(this, 'startsWith',
            {
              writable: true,
              enumerable: false,
              configurable: true,
              value: function startsWith (searchString /* [, position] */) {
                let thisArg, position, length;
      
                if (this === null || typeof this === 'undefined') {
                  throw new TypeError('String.prototype.startsWith called on null or undefined');
                }
      
                if (searchString instanceof RegExp) {
                  throw new TypeError('First argument to String.prototype.startsWith must not be a regular expression');
                }
      
                thisArg = String(this);
                searchString = String(searchString);
                length = searchString.length;
                position = arguments.length < 2 ? 0 : min(max(~~arguments[1], 0), length);
      
                if (position + length > thisArg.length) {
                  return false;
                }
      
                return thisArg.lastIndexOf(searchString, position) === position;
              }
            });
        }.call(String.prototype, Object, String, RegExp, Math.max, Math.min));
      }

    /**
     * String Erstellung
     * 
     * 
     * Erstellt wenn möglich aus dem übergebenen eine neue String Instanz. 
     * Wird null übergeben wir ein leerer String zurückgegeben.
     * 
     * 
     * @param {Mixed}   value Der Wert, aus dem ein neuer String gemacht werden 
     *                  soll.
     * @returns {String}    Den neuen String
     */
    String.prototype.makeString = function(value) {
        A.Logger.logDeprecated('`core/str` is deprecated and will be removed in 4.0. please use `tools/string/string-tool` if one of your JS modules rely on methods here.');
        return value === null ? '' : String(value);
    };

    /**
     * Prüft ob es ein JSON String ist
     * 
     * @returns {Boolean}   <strong>TRUE</strong> wenn es sich um einen JSON 
     *                      String handelt
     */
    String.prototype.isJSON = () => {
        A.Logger.logDeprecated('`core/str` is deprecated and will be removed in 4.0. please use `tools/string/string-tool` if one of your JS modules rely on methods here.');
        let str = this;
        if (str.isBlank())
            return false;
        str = str.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@');
        str = str.replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']');
        str = str.replace(/(?:^|:|,)(?:\s*\[)+/g, '');
        return (/^[\],:{}\s]*$/).test(str);
    };

    /**
     * Vorbereiten des Strings
     * 
     * @param {String} filter Optionaler Filter
     * 
     * @returns {String}
     */
    String.prototype.unfilterJSON = (filter) => {
        A.Logger.logDeprecated('`core/str` is deprecated and will be removed in 4.0. please use `tools/string/string-tool` if one of your JS modules rely on methods here.');
        return this.replace(filter || JSON_FILTER, '$1');
    };
    /**
     * Eval JSON
     * 
     * @param  {Boolean} sanitize
     * 
     * @throws SyntaxError  Wenn der String kein valides JSON ist.
     */
    String.prototype.evalJSON = function(sanitize) {
        A.Logger.logDeprecated('`core/str` is deprecated and will be removed in 4.0. please use `tools/string/string-tool` if one of your JS modules rely on methods here.');
        let json = this.unfilterJSON(),
            cx = /[\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff\u0000]/g;
        if (cx.test(json)) {
            json = json.replace(cx, function(a) {
                return '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
            });
        }
        try {
            if (!sanitize || json.isJSON())
                return eval('(' + json + ')');
        } catch (e) {}
        throw new SyntaxError('Badly formed JSON string');
    };

    /**
     * Konvertiert den JSON String zu einem Array/Objekt
     * 
     * @returns {Array|Object}      Das JSON Objekt
     */
    String.prototype.parseJSON = function() {
        A.Logger.logDeprecated('`core/str` "parseJSON" is deprecated and will be removed in 4.0. please use `tools/string/string-tool` if one of your JS modules rely on methods here.');
        let json = this.unfilterJSON();
        return JSON.parse(json);
    };

    /**
     * Prüft auf eine URL
     * 
     * @returns {Boolean}   <strong>TRUE</strong> wenn es sich um eine URL 
     *                      handelt.
     */
    String.prototype.isUrl = function() {
        "use strict";
        A.Logger.logDeprecated('`core/str` "isUrl" is deprecated and will be removed in 4.0. please use `tools/string/string-tool` if one of your JS modules rely on methods here.');
        let regexp = /^(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
        return regexp.test(this);
    };

    /**
     * Prüft auf eine Hex Farbangabe
     * 
     * @returns {Boolean}   <strong>TRUE</strong> wenn es sich um eine Hex 
     *                      Farbangabe handelt.
     */
    String.prototype.isColor = function() {
        "use strict";
        A.Logger.logDeprecated('`core/str` "isColor" is deprecated and will be removed in 4.0. please use `tools/string/string-tool` if one of your JS modules rely on methods here.');
        return /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(this);
    };

    /**
     * Prüft auf eine E-Mail
     * 
     * @returns {Boolean}   <strong>TRUE</strong> wenn es sich um eine E-Mail 
     *                      handelt.
     */
    String.prototype.isEmail = function() {
        "use strict";
        A.Logger.logDeprecated('`core/str` "isEmail" is deprecated and will be removed in 4.0. please use `tools/string/string-tool` if one of your JS modules rely on methods here.');
        return /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i.test(this);
    };

    /**
     * Prüft auf einen leeren String (' ')
     * 
     * @returns {Boolean}   <strong>TRUE</strong> wenn der String leer ist.
     */
    String.prototype.isBlank = function() {
        A.Logger.logDeprecated('`core/str` "isBlank" is deprecated and will be removed in 4.0. please use `tools/string/string-tool` if one of your JS modules rely on methods here.');
        return /^\s*$/.test(this);
    };

    /**
     * Prüft auf einen leeren String ('')
     * 
     * @returns {Boolean}   <strong>TRUE</strong> wenn der String leer ist.
     */
    String.prototype.isEmpty = function() {
        A.Logger.logDeprecated('`core/str` "isEmpty" is deprecated and will be removed in 4.0. please use `tools/string/string-tool` if one of your JS modules rely on methods here.');
        return this === '';
    };

    /**
     * Umwandeln eines Unix Zeitstempels in einen Zeit-String
     * 
     * @returns {String}    Dei Zeitangabe als String
     */
    String.prototype.toHHMMSS = function() {
        A.Logger.logDeprecated('`core/str` "toHHMMSS" is deprecated and will be removed in 4.0. please use `tools/string/string-tool` if one of your JS modules rely on methods here.');
        let sec = parseInt(this, 10),
            hours = Math.floor(sec / 3600),
            minutes = Math.floor((sec - (hours * 3600)) / 60),
            seconds = sec - (hours * 3600) - (minutes * 60);

        if (hours < 10) {
            hours = "0" + hours;
        }
        if (minutes < 10) {
            minutes = "0" + minutes;
        }
        if (seconds < 10) {
            seconds = "0" + seconds;
        }
        return hours + ':' + minutes + ':' + seconds;
    };

    return strng;
});
