/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('core/weak-map', [
    'core/acms'
], (A) => {

    
    const wm = window.WeakMap || window.MozWeakMap || ((() => {

        /**
         * WeakMap Polyfill
         *
         * @class WeakMap
         */
        class WeakMap {
            construct() {
                this.keys = [];
                this.values = [];
            }
    
            get(key) {
                let i, item, j, len, ref;
                ref = this.keys;
                for (i = j = 0, len = ref.length; j < len; i = ++j) {
                    item = ref[i];
                    if (item === key) {
                        return this.values[i];
                    }
                }
            }
    
            set(key, value) {
                let i, item, j, len, ref;
                ref = this.keys;
                for (i = j = 0, len = ref.length; j < len; i = ++j) {
                    item = ref[i];
                    if (item === key) {
                        this.values[i] = value;
                        return;
                    }
                }
                this.keys.push(key);
                return this.values.push(value);
            }
        }
    
        window.WeakMap = WeakMap;
    
        return WeakMap;
    
    })());
    


    return wm;
});
