/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('core/classes', [], () => {

    /**
     * CSS-Klassen Handling
     * 
     * Setzen, entfernen und wechseln von CSS Klassen sowie das prüfen auf eine 
     * vorhandene Klasse
     * 
     * @class
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class Classes {

        /**
         * Fügt eine oder mehrere neue Klassen hinzu
         * 
         * @param {Element} element     DOM Element
         * @param {String}  cls         Die Neue Klasse oder, optional, 
         *                              mehrere Klassen mit einem Leerzeichen 
         *                              oder Komma verbunden
         * 
         * @static 
         * @memberof Classes
         * 
         */
        static addClass(element, cls) {
            cls = cls.replace(/ /g, ",");
            cls = cls.replace(",,", ",");
            if (cls.indexOf(',') !== -1) {
                cls = cls.split(',');
                cls.forEach(function(cl, ind, arr) {
                    Classes._addClass(element, cl);
                });
            } else {
                Classes._addClass(element, cls);
            }
            return this;
        }

        /**
         * Entfernt eine oder mehrere Klassen
         * 
         * @param {HTMLElement} element     DOM Element
         * @param {String}  cls         Die Neue Klasse oder, optional, 
         *                              mehrere Klassen mit einem Leerzeichen 
         *                              oder Komma verbunden
         * 
         * @static 
         * @memberof Classes 
         */
        static removeClass(element, cls) {
            if (!cls || '' === cls) {
                element.className = "";
                return this;
            }
            cls = cls.replace(/ /g, ",");
            cls = cls.replace(",,", ",");
            if (cls.indexOf(',') !== -1) {
                cls = cls.split(',');
                cls.forEach(function(cl, ind, arr) {
                    Classes._removeClass(element, cl);
                });
            } else if (cls) {
                Classes._removeClass(element, cls);
            }
            return this;
        }

        /**
         * Wechselt eine oder mehrere Klassen. Die klasse wird, wenn nicht 
         * gefunden, gesetzt, andernfalls entfernt.
         * 
         * @param {HTMLElement} element     DOM Element
         * @param {String}  cls         Die zu wechselnde Klasse oder, optional, 
         *                              mehrere Klassen mit einem Leerzeichen 
         *                              oder Komma verbunden
         * 
         * @static 
         * @memberof Classes
         */
        static toggleClass(element, cls) {
            cls = cls.replace(/ /g, ",");
            cls = cls.replace(",,", ",");
            if (cls.indexOf(',') !== -1) {
                cls = cls.split(',');
                cls.forEach(function(cl, ind, arr) {
                    Classes._toggleClass(element, cl);
                });
            } else {
                Classes._toggleClass(element, cls);
            }
            return this;
        }

        /**
         * Prüft auf das vorhandensein einer Klasse
         * 
         * @param {HTMLElement} element     DOM Element
         * @param {String}  cls         Die CSS-Klasse
         *                              
         * @returns {Boolean}
         * 
         * @static 
         * @memberof Classes
         */
        static hasClass(element, cls) {
            if ("classList" in element) {
                return element.classList.contains(cls);
            } else {
                return (-1 < element.className.indexOf(cls));
            }
        }

        /**
         * @param  {HTMLElement} element
         * @param  {String} cls
         * 
         * @static 
         * @memberof Classes
         * @private
         */
        static _addClass(element, cls) {
            if (element === undefined) {
                console.log(element);
                console.log(cls);
                throw new Error('Das Element muss vorhanden sein');
            }
            if ("classList" in element) {
                element.classList.add(cls);
            } else {
                var classes = element.className.split(" ");
                classes.push(cls);
                element.className = classes.join(" ");
            }
        }

        /**
         * @param  {HTMLElement} element
         * @param  {String} cls
         * 
         * @static 
         * @memberof Classes
         * @private
         */
        static _removeClass(element, cls) {
            try {
                if (element.classList && cls) {
                    element.classList.remove(cls);
                } else if (cls) {
                    var classes = element.className.split(" ");
                    classes.splice(classes.indexOf(cls), 1);
                    element.className = classes.join(" ");
                } else {
                    element.className = "";
                }
            } catch (E) {
                console.log(E);
                console.trace();
            }
        }

        /**
         * @param  {HTMLElement} element
         * @param  {String} cls
         * 
         * @static 
         * @memberof Classes
         * @private
         */
        static _toggleClass(element, cls) {
            if ("classList" in element) {
                element.classList.toggle(cls);
            } else {
                if (this.hasClass(element, cls)) {
                    this._removeClass(element, cls);
                } else {
                    this._addClass(element, cls);
                }
            }
        }

    }

    Node.prototype.hasClass = (className) => {
        return Classes.hasClass(this, className);
    };

    Node.prototype.addClass = (className) => {
        Classes.addClass(this, className);
        return this;
    };

    Node.prototype.removeClass = (className) => {
        Classes.removeClass(this, className);
        return this;
    };
    Node.prototype.toggleClass = (className) => {
        Classes.toggleClass(this, className);
        return this;
    };

    return Classes;

});
