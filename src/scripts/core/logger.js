/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("core/logger", [
	"tools/object/extend",
	"core/selectors",
	"core/classes",
	"core/console",
	"core/acms",
	"core/document",
	"events/event",
	"text!templates/html/logger-entry.hbs",
	"templates/template-engine"
], (Extend, Selectors, Classes, Con, A, Doc, AcmsEvent, Template, TemplateEngine) => {

	/**
     * Amaryllis UI Logger
     *
     * Implementierung der System Logger Komponente, wenn der Logger aktiv ist. Der
     * Logger stellt keine Alternative zur Konsole dar! Er erweitert die Logging-
     * Funktionalitäten und erlaubt einem, zusätzliche Informationen, Warnungen
     * oder Hinweise direkt in den Logger-Tab zu schreiben.
     *
     * @class Logger
	 * @author qm-b <https://bitbucket.org/qm-b/>
     */
	class Logger {

		/**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof Logger
         */
		static get MODULE() {
			return "Amaryllis-CMS Logger";
		}

		/**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof Logger
         */
		static get NS() {
			return "acms.core.logger";
		}

		/**
         * Amaryllis UI Version
         *
         * @readonly
         * @static
         * @memberof Logger
         */
		static get VERSION() {
			return window.AmaryllisCMS.version;
		}

		/**
         * Event "booted"
         * 
         * Event will be fired on body, if the logger has been initialized and is ready to use.
         *
         * @readonly
         * @static
         * @memberof Logger
         */
		static get EVENT_BOOTED() {
			return Logger.NS + ".booted";
		}

		static get DEFAULT_OPTIONS() {
			return {
				loggerTab: "#acms-logger-javascript",
				icons: {
					success: "acms-icon acms-icon-check text-success",
					warning: "acms-icon acms-icon-exclamation-triangle text-warning",
					info: "acms-icon acms-icon-info-circle text-info",
					error: "acms-icon acms-icon-exclamation-triangle text-danger",
					danger: "acms-icon acms-icon-exclamation-triangle text-danger",
					muted: "acms-icon acms-icon-info-circle text-muted"
				},
				entryTemplate: null
			};
		}

		static init() {
			if (!(A.Logger instanceof Logger)) {
				let element = Doc.getElementById("acms-logger-tabs");
				if (!element) {
					element = Doc.body;
				}
				A.Logger = new Logger(element, {});
				A.Logger.finishLoggerBoot();
			}
			return A.Logger;
		}

		/**
         * Creates an instance of Logger.
         * 
         * @param {HTMLElement} element     The Element holding the Logger tabs
         * @param {Object|null} options
         * @memberof Logger
         */
		constructor(element, options) {
			this.element = element;
			this.options = this.buildOptions(options);
			if (this.element.tagName.toLowerCase() === "body") {
				this.body = element;
				this.element = Doc.querySelector(this.options.loggerTab);
			}
			this.targetDiv = Doc.querySelector("#acms-logger-javascript");
			this.targetLink = Doc.querySelector("a[href=\"#acms-logger-javascript\"]");
			if (!this.targetDiv) {
				this.enabled = false;
			} else {
				this.enabled = true;
			}
			this.coreDebug = A.General.debug && this.element !== null;
			this.consoleDebug = console && console.log && A.User.isUser;
			this.initiated = true;
			if (this.coreDebug) {
				this._doAppendMessage("Welcome to our Amaryllis-UI JS Debugger " + this.getVersion(), "info");
				this._doAppendMessage("Booted JS Service " + Logger.MODULE, "info");
			}
			this.loggerButton = Selectors.q("[data-role=logger-button]");
		}


		/**
         * Get default options
         * @returns {Logger.DEFAULT_OPTIONS|Object}
         */
		getDefaultOptions() {
			return Logger.DEFAULT_OPTIONS;
		}
		/**
         * Gets the Component Version
         * @returns {String|Logger.VERSION}
         */
		getVersion() {
			return Logger.VERSION;
		}
		/**
         * Gets the Component Name
         * @returns {String|Logger.MODULE}
         */
		getName() {
			return Logger.MODULE;
		}
		/**
         * Build Options
         * 
         * @param {Object} options
         */
		buildOptions(options) {
			return Extend.deep({}, true, this.getDefaultOptions(), options, this.element.acmsData());
		}

		/**
         * Writes a Log Message
         * 
         * @param {String} message Message to be logged
         * @param {String} type Message Type can bei either success, danger, warning, info
         */
		writeLog(message, type) {
			let ltype = this._checkType(type, "info");
			this._doAppendMessage(message, ltype);
		}

		/**
         * Log deprecated messages
         *
         * @param {String} Message
         * @param {String} Message Type. Defaults to "warning"
         */
		logDeprecated(message, type) {
			type = this._checkType(type, "warning");
			this._doAppendMessage(message, type);
		}

		/**
		 * Logging a deprecated module
		 * 
		 * @example 
		 * Acms.Logger.logDeprecatedModule('ui/components/component', 'apps/ui/component', '4.0')
		 * // -> 'ui/components/component has been refactored to apps/ui/component and will be removed in 4.0'
		 *
		 * @param {String} deprecated 	Deprecated module
		 * @param {String} replacement 	replaced by module
		 * @param {String} version 		to be removed in
		 * 
		 * @memberof Logger
		 * @since 3.5.0
		 */
		logDeprecatedModule(deprecated, replacement, version) {
			const self = this;
			require(['tools/string/sprintf'], sprintf => {
				let msg = sprintf('<code class="code-inline">%s</code> has been refactored to <code class="code-inline">%s</code> and will be removed in %s', deprecated, replacement, version);
				self._doAppendMessage(msg, "warning");
			});
		}

		/**
         * Log warning messages
         *
         * @param {String} Message
         * @param {String} type Type. Defaults to "danger"
         */
		logWarning(message, type) {
			type = this._checkType(type, "danger");
			this._doAppendMessage(message, type);
		}

		/**
         * Log error messages
         *
         * @param {String} Message
         * @param {String} type Type. Defaults to "danger"
         */
		logError(message, type) {
			type = this._checkType(type, "danger");
			this._doAppendMessage(message, type);
		}

		/**
         * Log warning messages
         *
         * @param {String} Message
         * @param {String} type Type. Defaults to "info"
         */
		logInfo(message, type) {
			type = this._checkType(type, "info");
			this._doAppendMessage(message, type);
		}


		/**
         * Prüft einen übergebenen Typen
         * 
         * @param {String} type         Der übergebene Typ
         * @param {String} defaultsTo   Standard-Typ wenn nicht oder invalide übergeben
         * 
         * @returns {String}    Den Typ oder `defaultsTo`
         * 
         * @memberof Logger
         * 
         * @private
         */
		_checkType(type, defaultsTo) {
			let self = this,
				o = self.options,
				icons = o.icons;
			type = (typeof type !== "undefined" &&
                typeof icons[type] !== "undefined" ?
				type :
				defaultsTo);
			return type;
		}

		/**
         * Appending the Log Message to Amaryllis CMS Logger
         *
         * @param {String} message Message to be appended
         * @param {String} type Message Type
         * 
         * @memberof Logger
         * 
         * @private
         */
		_doAppendMessage(message, type) {
			if (!this.enabled) {
				return;
			}
			let self = this,
				options = (this.options === null) ? this.getDefaultOptions() : this.options,
				target = self.targetDiv,
				source = options.entryTemplate || Template,
				data = {
					entry: {
						message: message,
						type: type,
						icon: options.icons[type]
					}
				};
			self._updateBadge();
            let content = TemplateEngine.compile(source, data, target);
            if(self.loggerButton && (type === 'danger' || type === 'warning')) {
                let badge = Selectors.q('.badge', self.loggerButton);
                let btn = (self.loggerButton.tagName.toLowerCase() === 'button' ? self.loggerButton : Selectors.q('.button', self.loggerButton));
                if(badge) {
                    badge.textContent = (parseInt(badge.textContent) + 1);
                }
                if(btn) {
                    Classes.removeClass(btn, 'success');
                    Classes.addClass(btn, 'danger');
                }
                let errorTab = Selectors.q('[data-tab="errors"]'),
                    errorPane = Selectors.q('[data-pane="errors"]');
                if(errorTab) {
                    let errorBadge = Selectors.q('.badge', errorTab);
                    if(errorBadge) {
                        errorBadge.textContent = (parseInt(errorBadge.textContent) + 1);
                    }
                }
                if(errorPane) {
                    let div = document.createElement('div');
                    div.innerHTML = content;
                    while(div.firstChild) {
                        errorPane.appendChild(div.firstChild);
                    }
                }
            }

            return content;
		}

		_updateBadge() {
			let self = this,
				tabLink = self.targetLink,
				badge = (Selectors.q(".badge", tabLink) || document.createElement("span")),
				spanNumber = Number((badge.textContent || 0));
			if (spanNumber == 0) {
				Classes.addClass(badge, "badge");
				tabLink.appendChild(badge);
			}
			badge.textContent = spanNumber + 1;
		}

		/**
         * Beenden des Core Boot
         * 
         * @memberof Logger
         * 
         * @private
         */
		finishLoggerBoot() {
			let e = AcmsEvent.createCustom(this.EVENT_BOOTED, {
				Logger: this
			});
			AcmsEvent.dispatch(document.body, e);
		}


	}

	Logger.init();

	return Logger;
});
