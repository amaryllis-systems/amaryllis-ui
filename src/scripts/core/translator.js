/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("core/translator", [
    "core/acms",
    "tools/object/extend",
    "tools/string/sprintf",
    "text!translations/de_DE/de_DE.json",
    "core/logger",
    "core/window",
], (A, Extend, sprintf, Dic, Logger, Win) => {

    let initialized = false;

    const dictionaries = {};

    /**
     * Front End Translator Service
     * 
     * Der Translator-Service kann für JavaScript interne Übersetzungen genutzt
     * werden. Z.Beispiel Beim senden von Benachrichtigungen an den Browser oder
     * dem Setzen von Live-Inhalten.
     * 
     * @class
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class Translator {

        /**
         * Current Locale of Amaryllis-CMS
         *
         * @readonly
         * @static
         * @memberof Translator
         */
        static get currentLanguage() {
            if(A.Locale && A.Locale.locale) {
                return A.Locale.locale;
            }

            return 'de_DE';
        }

        /**
         * Fallback Locale of the Core
         *
         * @readonly
         * @static
         * @memberof Translator
         */
        static get fallbackLanguage() {
            return A.Locale.fallbackLocale;
        }

        /**
         * Debug Mode enabled?
         *
         * @readonly
         * @static
         * @memberof Translator
         */
        static get debug() {
            return A.General.debug;
        }

        /**
         * Has initialized?
         *
         * @static
         * @memberof Translator
         */
        static get initialized() {
            return initialized;
        }

        static set initialized(flag) {
            initialized = (flag === true);
        }

        
        /**
         * Dictionaries
         * 
         * @type {Object}
         *
         * @readonly
         * @static
         * @memberof Translator
         */
        static get dictionaries() {
            return dictionaries;
        }

        /**
         * Initialisiert die Komponente einmalig
         *
         * Die Initialisierung erfolgt automatisch und muss nicht gesondert
         * aufgerufen werden.
         *
         * @private 
         */
        static init() {
            if (!(A.Translator instanceof Translator)) {
                Translator.initialized = true;
                let f = "text!translations/" + Translator.currentLanguage + "/" + Translator.currentLanguage + ".json";
                let mods = [];
                mods.push(f);
                if(A.General.inAdmin) {
                    let f2 = "text!translations/" + Translator.currentLanguage + "/" + Translator.currentLanguage + "-admin.json";
                    mods.push(f2);
                }

                require(mods, (dic, dicAdmin) => {
                    let data = JSON.parse(dic);
                    Translator.add(data, 'Core', Translator.currentLanguage);
                    if(A.General.inAdmin) {
                        let dataAdmin = JSON.parse(dicAdmin);
                        Translator.add(dataAdmin, 'Core', Translator.currentLanguage);
                    }
                    if (A.Logger)
                        A.Logger.writeLog("Wörterbuch " + Translator.currentLanguage + " in Core nachgeladen");
                });
            }
        }

        /**
         * Translate a string
         *
         * alias for `Translator.t`
         * 
         * @param {String}      str         Strign to translate
         * @param {String}      textdomain  Textomain of the dictionary
         * @param {String}      locale      Locale. if omitted or `null`, the current locale will be used
         * @param {...String}   args        Aguments
         * 
         * @returns {String}
         */
        static _(str, textdomain, locale, ...args) {
            let self = this;
            locale = locale || self.currentLanguage;
            textdomain = textdomain || "Core";
            let dic = (typeof dictionaries[textdomain] === "object" &&
                    typeof (dictionaries[textdomain][locale]) === "object") ?
                dictionaries[textdomain][locale] :
                false;
            if (false === dic) {
                return self._loadDictionary(str, textdomain, locale, args);
            }
            return self._applyTranslation(str, dic, args);

        }

        /**
         * Translate a string
         * 
         * Perform the translation if the desired one could be found. 
         * If the entry is not found in the dictionary, the original 
         * string is returned.
         *
         * @static
         * 
         * @param {String}      str         Strign to translate
         * @param {String}      textdomain  Textomain of the dictionary
         * @param {String}      locale      Locale. if omitted or `null`, the current locale will be used
         * @param {...String}   args        Aguments
         * 
         * @returns {String}
         * 
         * @memberof Translator
         */
        static t(str, textdomain, locale, ...args) {
            return this._(str, textdomain, locale, ...args);
        }

        static add(catalogue, textdomain, locale) {
            if (typeof catalogue === 'string') {
                try {
                    catalogue = JSON.parse(catalogue);
                } catch (E) {
                    return;
                }
            }

            if (typeof Translator.dictionaries[textdomain] === 'undefined') {
                Translator.dictionaries[textdomain] = {};
            }
            if (typeof Translator.dictionaries[textdomain][locale] === 'undefined') {
                Translator.dictionaries[textdomain][locale] = {};
            }
            Translator.dictionaries[textdomain][locale] = Extend.flat({}, false, Translator.dictionaries[textdomain][locale], catalogue);
        }

        /**
         * Führt eine Übersetzung durch
         *
         * @param {String} str  Der zu übersetzende String
         * @param {Object} dic  Das intern gefundenen oder nachgeladene Wörterbuch
         * @param {Array|null} args  Argumente zum einfügen in den String
         *
         * @returns {String}
         */
        static _applyTranslation(str, dic, ...args) {
            if (dic &&
                typeof dic === 'object' &&
                dic.hasOwnProperty(str)) {
                str = dic[str] || str;
            } else if (!args) {
                if (typeof A.Logger === "object")
                    A.Logger.writeLog('Übersetzung für "' + str + '" nicht gefunden', 'danger');
                return str;
            }
            if (args && args.length) {
                
                return sprintf(str, args);
            }
            return str;
        }

        /**
         * Nachladen eines Wörterbuchs
         * 
         *
         * @param {String}      str            Der String
         * @param {String}      textdomain     Die Textdomain
         * @param {String}      locale         Die Lokale
         * @param {Array|null}  args           Argumente
         * 
         * @returns {Object|Boolean}    Das Wörterbuch oder FALSE wenn es nicht geladen werden konnte.
         * 
         * @private
         */
        static _loadDictionary(str, textdomain, locale, ...args) {
            let dic = (typeof Translator.dictionaries[textdomain] === "object" &&
                    Translator.dictionaries[textdomain][locale]) ?
                dictionaries[textdomain][locale] : false;
            let self = this;
            if (false !== dic && typeof (dic) === "object") {
                return dic;
            }
            try {
                let td = textdomain.toLowerCase();
                require(["text!modules/" + td  + "/translations/" + locale + "/" + locale + ".json"], function (mod) {
                    let data = JSON.parse(mod);
                    self.add(data, textdomain, locale);

                    if (typeof A.Logger === "object" && null !== A.Logger) {
                        A.Logger.logInfo("Wörterbuch " + locale + " in " + textdomain + " nachgeladen");
                    }
                    return self._applyTranslation(str, mod, args);
                });
            } catch (e) {
                if (A.Logger) {
                    A.Logger.logError("Error beim Laden des Wörterbuchs " + locale + " in " + textdomain, "error");
                }
                if (!Translator.dictionaries[textdomain]) {
                    Translator.dictionaries[textdomain] = {};
                }
                Translator.dictionaries[textdomain][locale] = {};
                return str;
            }

        }

    }

    if (!(A.Translator instanceof Translator)) {
        let data = JSON.parse(Dic);
        Translator.add(data, 'Core', 'de_DE');
        Translator.init();
        A.Translator = Translator;
    }

    return Translator;
});
