/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('initial', [
    'core/classes',
    'core/selectors',
    'events/event',
    'core/window',
    'core/document',
    'http/module-loader',
    'core/acms',
    'core/logger',
    'core/translator',
    'core/string',
    'core/element'
], (Classes, Selectors, AcmsEvent, Win, Doc, ModuleLoader, A) => {

    /**
     * Kernel Boot Loader
     * 
     * Bootet das System und die erforderlichen Module.
     *
     * @class
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class Boot {

        /**
         * Module Name
         * 
         * @type {String}
         *
         * @readonly
         * @static
         * @memberof Boot
         */
        static get MODULE() {
            return 'Kernel Boot Loader';
        }

        /**
         * Module Namespace
         * 
         * @type {String}
         *
         * @readonly
         * @static
         * @memberof Boot
         */
        static get NS() {
            return 'acms.core.boot';
        }

        /**
         * Event "booting"
         * 
         * Event will be fired 
         * 
         * @type {String}
         *
         * @readonly
         * @static
         * @memberof Boot
         */
        static get EVENT_BOOTING() {
            return Boot.NS + '.booting';
        }

        /**
         * Event "booted"
         * 
         * @type {String}
         *
         * @readonly
         * @static
         * @memberof Boot
         */
        static get EVENT_BOOTED() {
            return Boot.NS + '.booted';
        }

        /**
         * Init
         */
        static init() {
            return new Boot();
        }

        /**
         * Creates an instance of Boot.
         *
         * @memberof Boot
         */
        constructor() {
            A.Logger.writeLog('Kernel Core Boot');

            let htm = Doc.getElementsByTagName('html')[0];
            Classes.removeClass(htm, 'no-js');
            Classes.removeClass(Doc.body, 'no-js');
        }

        /**
         * Runs the system initialisation
         *
         * @memberof Boot
         */
        run() {
            let ev = AcmsEvent.createCustom(Boot.EVENT_BOOTING, {});
            AcmsEvent.dispatch(Win.document.body, ev);
            if (ev.defaultPrevented) {
                return;
            }
            if (Win.AmaryllisCMS.support.touch) {
                Classes.addClass(Doc.body, 'touch');
            }
            if (!!Win.AmaryllisCMS.device.isMobileDevice) {

            } else {
                Classes.addClass(Doc.body, 'desktop');
            }

            if (A.Redirect && A.Redirect.message !== "") {
                require(['notifier'], () => {
                    A.Notifications.create({
                        content: A.Redirect.message,
                        title: A.Redirect.title,
                        type: A.Redirect.type,
                        delay: 5
                    });
                });
            }
            try {
                ModuleLoader.load();
            } catch (E) {
                if(Acms.Logger) {
                    Acms.Logger.logWarning(E.message);
                    console.log(E.message);
                    console.log(E.trace());
                }
            }
            this.listen();
        }


        /**
         * Toggle Listener
         * 
         * Ein globaler Listener für alle multi-toggler von Checkboxen.
         * 
         * @deprecated Sollte entfernt und in ein separates Modul verschoben werden
         * 
         */
        listen() {
            let toggleAll = Selectors.qa('.acms-toggle-all'),
                i;
            let cb = function (e) {
                e.preventDefault();
                let target = e.target,
                    parent = target.parentNode;
                let div = Selectors.closest(parent, 'div'),
                    chb = Selectors.qa('input[type=checkbox]', div),
                    i;
                for (i = 0; i < chb.length; i++) {
                    AcmsEvent.fireChange(chb[i]);
                    chb[i].checked = !chb[i].checked;
                    chb[i].setAttribute('checked', chb[i].checked);
                    let label = Selectors.closest(chb[i], 'label');
                    if (label) Classes.toggleClass('active');
                }
                return false;
            };
            for (i = 0; i < toggleAll.length; i++) {
                AcmsEvent.add(toggleAll[i], 'click', cb);
            }
            let ev = AcmsEvent.createCustom(Boot.EVENT_BOOTED, {});
            AcmsEvent.dispatch(Win.document.body, ev);
        }

    }

    return Boot;
});
