/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * MutationObserver
 * 
 * Polyfill für einen JS MutationObserver
 * 
 * @param {Object} Win
 * @param {Object} A
 * @returns {Window.WebkitMutationObserver}
 */
define('core/mutation-observer', [
    'core/window',
    'core/acms'
], (Win, A) => {

    "use strict";

    /**
     * Mutation Observer
     * 
     * Polyfill für einen JS MutationObserver
     * 
     * @exports core/mutation-observer
     */
    let mo = Win.MutationObserver ||
        Win.WebkitMutationObserver ||
        Win.MozMutationObserver ||
        ((() => {
            class MutationObserver {

                static get notSupported() {
                    return true;
                }

                constructor() {
                    A.Logger.logWarning('Mutation server not supported');
                }

                observe() {
                }
            }

            return MutationObserver;

        })());

    return mo;

});
