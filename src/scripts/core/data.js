/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("core/data", [], () => {

    /**
     * HTML Data Handler
     * 
     * Das Modul unterstützt bei der Verarbeitung von HTML5 data- Attributen. Es 
     * injiziert sich selbst in Node Instanzen und erweitert die Nodes um die 
     * einfache Zusatzfunktion "acmsData".
     * 
     * Das Modul ist in den Boot Loader integriert und steht prinzipiell zur 
     * Verfügung.
     * 
     * ``` javascript
     * let ele = document.querySelector('#myElement');
     * let custom = ele.acmsData('custom');
     * console.log(custom);
     * ```
     * 
     * @exports core/data
     * @class Data
     */
    class Data {

        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof Data
         */
        static get MODULE() {
            return "HTML Data Handler";
        }

        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof Data
         */
        static get NS() {
            return "acms.core.data";
        }

        /**
         * Erstellt einen Data-String aus einem Object
         * 
         * @param {Object} obj Object mit Attribut-Value Paaren.
         * 
         * @memberof Data
         * @static
         */
        static toDataString(obj) {
            if (null === obj || typeof obj !== 'object') {
                throw new Error('obj muss ein Objekt sein');
            }
            let datastr = '',
                prop;
            for (prop in obj) {
                if (!obj.hasOwnProperty(prop)) {
                    continue;
                }
                let attrName = "data-" + key.replace(/[A-Z]/g, function ($0) {
                    return "-" + $0.toLowerCase();
                });
                datastr += attrName + " ";
            }

            return datastr.trim();
        }

        /**
         * Creates an instance of Data.
         *
         * @param {HTMLElement} element     Das Element
         * 
         * @memberof Data
         */
        constructor(element) {
            this.element = this.dataset = null;
            this.initialize(element);
        }

        /**
         * Interner Constructor
         * 
         * @param {Element} element Das HTML Element
         * 
         * 
         * 
         * @memberof Data 
         */
        initialize(element) {
            this.element = element;
            if (element && 'dataset' in element) {
                this.dataset = element.dataset;
            } else {
                this.dataset = this._compat();
            }
            this._objSet = {};
        }

        /**
         * Setter
         * 
         * Setter für die HTML data-Attribute oder, optional, nur in den 
         * Data-Store.
         * 
         * @param {String}  a               Der Name des Attributes
         * @param {Mixed}   b               Der zugehörige Wert
         * @param {Boolean} updateElement   Soll das Element selbst aktualisiert 
         *                                  werden? Wenn ja, wird das Attrbut
         *                                  im HTML Context gesetzt/aktualisiert.
         *                                  Die Angabe wird ignoriert, wenn der 
         *                                  Wert kein String ist.
         * 
         * @example
         * let element = document.getElementById('#my-id');
         * console.log(element); // <div id="my-id">Div</div>
         * element.acmsData('x', 'y', true);
         * console.log(element); //  <div id="my-id" data-x="y">Div</div>
         * console.log(element.acmsData('x')); // y
         * 
         * @memberof Data 
         * 
         * @returns {Data}
         */
        set(a, b, updateElement) {
            if (typeof a === "object") {
                for (let prop in a) {
                    if (a.hasOwnProperty(prop)) {
                        this._set(a, a[prop], updateElement);
                    }
                }
            } else {
                this._set(a, b, updateElement);
            }

            return this;
        }

        /**
         * Getter für ein Attribut
         * 
         * Gibt ein Attribut zurück, wenn das Element ein bestimmtes data-
         * Attribut <strong>a</strong> besitzt. Ist das Attribut nicht gesetzt, 
         * wird <strong>defaultsTo</strong> zurückgegeben.
         * 
         * @param {String} a            Der Attribut Name
         * @param {Mixed} defaultsTo    Standard-Wert, wenn a nicht gesetzt
         * 
         * @memberof Data 
         * 
         * @returns {Mixed}
         */
        get(a, defaultsTo) {
            if (!a) {
                return this.dataset;
            }
            let camelCaseName = a.replace(/-(.)/g, function ($0, $1) {
                return $1.toUpperCase();
            });
            defaultsTo = typeof defaultsTo === 'undefined' ? null : defaultsTo;
            if (camelCaseName in this.dataset) {
                return this.dataset[camelCaseName];
            } else if (camelCaseName in this._objSet) {
                return this._objSet[camelCaseName];
            }
            return defaultsTo;
        }

        /**
         * Internen Setter
         * 
         * Setter für die HTML data-Attribute oder, optional, nur in den 
         * Data-Store.
         * 
         * @param {String}  a   Der Name des Attributes
         * @param {Mixed}   b   Der zugehörige Wert
         * 
         * @param {Boolean} updateElement   Soll das Element selbst aktualisiert 
         *                                  werden? Wenn ja, wird das Attrbut
         *                                  im HTML Context gesetzt/aktualisiert.
         *                                  Die Angabe wird ignoriert, wenn der 
         *                                  Wert kein String ist.
         *                                  
         * @returns {Data}
         * 
         * @access private
         * @memberof Data 
         */
        _set(a, b, updateElement) {
            let self = this;
            try {
                let camelCaseName = a.replace(/-(.)/g, function ($0, $1) {
                    return $1.toUpperCase();
                });
                if (typeof b === "object") {
                    this._objSet[camelCaseName] = b;
                } else {
                    this.dataset[camelCaseName] = b;
                    if (updateElement === true && typeof b === "string") {
                        let attrName = "data-" + a.replace(/[A-Z]/g, function ($0) {
                            return "-" + $0.toLowerCase();
                        });
                        self.element.setAttribute(attrName, b);
                    }
                }
            } catch (E) {
                console.log(E.message);
                console.trace();
            }
        }

        /**
         * Polyfill für das dataset eines HTMLElement
         * 
         * @param  {Object} data
         * 
         * @access private
         * @memberof Data 
         * @deprecated
         */
        _compatSet(data) {
            let self = this;
            this.element.keys(data).forEach(function (key) {
                let attrName = "data-" + key.replace(/[A-Z]/g, function ($0) {
                    return "-" + $0.toLowerCase();
                });
                self.element.setAttribute(attrName, data[key]);
            });
        }

        /**
         * Polyfill für das dataset eines HTMLElement
         * 
         * @returns {Object} Das data Objekt mit den Attributen
         * 
         * @access private
         * @memberof Data 
         */
        _compat() {
            let data = {},
                el = this.element;
            if (!el) {
                return data;
            }
            [].forEach.call(el.attributes, function (attr) {
                if (/^data-/.test(attr.name)) {
                    let camelCaseName = attr.name.substr(5).replace(/-(.)/g, function ($0, $1) {
                        return $1.toUpperCase();
                    });
                    data[camelCaseName] = attr.value;
                }
            });

            return data;
        }

    }

    return Data;

});
