/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("core/localstorage", ["core/window", "core/document"], (w, d) => {

    /**
     * Wrapper für den local Storage
     * 
     * @exports core/localstorage
     */
    let localStorage = w.localStorage;

    if (!localStorage) {
        Object.defineProperty(w, "localStorage", new(function() {
            let allK = [],
                store = {};
            Object.defineProperty(store, "getItem", {
                value: function(k) {
                    return k ? this[k] : null;
                },
                writable: false,
                configurable: false,
                enumerable: false
            });
            Object.defineProperty(store, "key", {
                value: function(newK) {
                    return allK[newK];
                },
                writable: false,
                configurable: false,
                enumerable: false
            });
            Object.defineProperty(store, "setItem", {
                value: function(k, sValue) {
                    if (!k) {
                        return;
                    }
                    d.cookie = escape(k) + "=" + escape(sValue) + "; expires=Tue, 19 Jan 2038 03:14:07 GMT; path=/";
                },
                writable: false,
                configurable: false,
                enumerable: false
            });
            Object.defineProperty(store, "length", {
                get: function() {
                    return allK.length;
                },
                configurable: false,
                enumerable: false
            });
            Object.defineProperty(store, "removeItem", {
                value: function(k) {
                    if (!k) {
                        return;
                    }
                    d.cookie = escape(k) + "=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/";
                },
                writable: false,
                configurable: false,
                enumerable: false
            });
            this.get = function() {
                let isIndexOf;
                for (let k in store) {
                    isIndexOf = allK.indexOf(k);
                    if (isIndexOf === -1) {
                        store.setItem(k, store[k]);
                    } else {
                        allK.splice(isIndexOf, 1);
                    }
                    delete store[k];
                }
                for (allK; allK.length > 0; allK.splice(0, 1)) {
                    store.removeItem(allK[0]);
                }
                for (let aCouple, iKey, nIdx = 0, aCouples = d.cookie.split(/\s*;\s*/); nIdx < aCouples.length; nIdx++) {
                    aCouple = aCouples[nIdx].split(/\s*=\s*/);
                    if (aCouple.length > 1) {
                        store[iKey = unescape(aCouple[0])] = unescape(aCouple[1]);
                        allK.push(iKey);
                    }
                }
                return store;
            };
            this.configurable = false;
            this.enumerable = true;
        })());
    }

    return w.localStorage;
});
