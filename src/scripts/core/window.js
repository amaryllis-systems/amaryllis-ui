/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("core/window", [
    "device/browser-detect",
    "device/os-detect",
    "core/navigator",
    "core/console",
    "core/acms",
    "text!package"
], (BrowserDetect, OsDetect, Navi, C, A, pckgsrc) => {

    let pckg = JSON.parse(pckgsrc);
    let VERISON = pckg.version;

    /**
     * Window Wrapper
     * 
     * @exports core/window
     * 
     * Das Window Objekt besteht primär aus dem vorgegebenen JavaScript Window
     * Objekt und wird erweitert um die AmaryllisCMS Komponente.
     */
    let win = window;
    let doc = win.document;
    let s = doc.createElement('p').style,
        transEndEventNames = {
            WebkitTransition: 'webkitTransitionEnd',
            MozTransition: 'transitionend',
            OTransition: 'oTransitionEnd otransitionend',
            transition: 'transitionend'
        },
        supportTransitions = () => {
            for (let name in transEndEventNames) {
                if (s[name] !== undefined) {
                    return {
                        end: transEndEventNames[name]
                    };
                }
            }
            return false;
        };

    let deviceAgent = Navi.userAgent,
        lowerDeviceAgent = deviceAgent.toLowerCase(),
        sBrowser,
        cssPointerEvents = (function (el) {
            if (!!Navi.userAgent.match(/Trident.*rv[ :]?11\./)) {
                return false;
            }
            el = document.createElement('x');
            el.style.cssText = 'pointer-events:auto';
            return el.style.pointerEvents === 'auto';
        })();

    win.matchMedia = win.matchMedia || (function (doc, undefined) {

        let res,
            docElem = doc.documentElement,
            refNode = docElem.firstElementChild || docElem.firstChild,
            fakeBody = doc.createElement("body"),
            div = doc.createElement("div");

        div.id = "amaryllis-test-media";
        div.style.cssText = "position:absolute;top:-100em";
        fakeBody.style.background = "none";
        fakeBody.appendChild(div);

        return (q) => {

            div.innerHTML = "&shy;<style media=\"" + q + "\"> #amaryllis-test-media { width: 42px; }</style>";

            docElem.insertBefore(fakeBody, refNode);
            res = div.offsetWidth === 42;
            docElem.removeChild(fakeBody);

            return {
                matches: res,
                media: q
            };

        };

    }(doc));

    win.requestAnimationFrame = (window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function (callback) { window.setTimeout(callback, 1000 / 60); });

    let Detektor = new BrowserDetect();

    let currentBrowser = Detektor.get();

    let isEdge = Detektor.is('edge');
    let isOpera = Detektor.is('opera');
    let isFirefox = Detektor.is('firefox');
    let isSafari = Detektor.is('safari');
    let isChrome = Detektor.is('chrome');
    let isIE = Detektor.is('msie');
    let isBlackberry = Detektor.is('blackberry');
    let isMozilla = Detektor.is('mozilla');

    let isTouchDevice = (('ontouchstart' in win) ||
            (Navi.msMaxTouchPoints > 0)) ||
        (win.DocumentTouch && doc instanceof DocumentTouch) ||
        (deviceAgent.match(/(iphone|ipod|ipad)/) ||
            deviceAgent.match(/(android)/) ||
            deviceAgent.match(/(iemobile)/) ||
            deviceAgent.match(/iphone/i) ||
            deviceAgent.match(/ipad/i) ||
            deviceAgent.match(/ipod/i) ||
            deviceAgent.match(/blackberry/i) ||
            deviceAgent.match(/bada/i));

    win.AmaryllisCMS = {
        version: VERISON,
        supportTransition: supportTransitions(),
        inAdmin: A.General.inAdmin,
        support: {
            touch: isTouchDevice,
            draggable: !!('draggable' in document.createElement('div')),
            isTouchDevice: isTouchDevice,
            transition: supportTransitions(),
            orientation: ("orientation" in win && "onorientationchange" in win),
            cssPointerEvents: cssPointerEvents,
            windows: (typeof Windows !== 'undefined' &&
                typeof Windows.UI !== 'undefined' &&
                typeof Windows.ApplicationModel !== 'undefined')
        },
        debug: A.General.debug && C && C.log,
        device: {
            isAppleMobile: lowerDeviceAgent.match(/(iphone|ipod|ipad)/),
            isIphone: lowerDeviceAgent.match(/iphone/i),
            isIpad: lowerDeviceAgent.match(/ipad/i),
            isIpod: lowerDeviceAgent.match(/ipod/i),
            isAndroid: lowerDeviceAgent.match(/(android)/),
            isIEMobile: lowerDeviceAgent.match(/(iemobile)/),
            isBlackberry: lowerDeviceAgent.match(/blackberry/i),
            isBada: lowerDeviceAgent.match(/bada/i)
        },
        os: {
            name: OsDetect.os,
            version: OsDetect.version
        },
        browser: {
            isCrome: isChrome,
            isSafari: isSafari,
            isOpera: isOpera,
            isFirefox: isFirefox,
            isIE: isIE,
            isMozilla: isMozilla,
            isEdge: isEdge,
            isBlackberry: isBlackberry,
            is: Detektor.is,
            detected: currentBrowser
        },
        browserName: currentBrowser.name,
        browserVendor: currentBrowser.vendor
    };
    win.AmaryllisCMS.device.isMobileDevice = (
        win.AmaryllisCMS.device.isAndroid ||
        win.AmaryllisCMS.device.isAppleMobile ||
        win.AmaryllisCMS.device.isBada ||
        win.AmaryllisCMS.device.isBlackberry ||
        win.AmaryllisCMS.device.isIEMobile ||
        win.AmaryllisCMS.device.isIpad ||
        win.AmaryllisCMS.device.isIphone ||
        win.AmaryllisCMS.device.isIpod);

    
    window.refresh = (time) => {
        time = time || 100;
        setTimeout(() => {
            window.location.reload();
        }, time);
    };

    return win;
});
