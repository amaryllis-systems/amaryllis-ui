/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("core/base", ["tools/class/class-tool", "tools/object/extend"], (ClassTool, Extend) => {

    /**
     * Base Class
     *
     * @class
     * @abstract
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class Base {

        /**
         * Braucht das Modul eine Initialisierung durch den Modul Loader?
         * 
         * @readonly
         * @static
         * @memberof Base
         * 
         * @returns {Boolean}
         */
        static get needInit() {
            if (typeof this._needInit === "undefined") {
                this._needInit = true;
            }

            return this._needInit;
        }

        /**
         * Ändern der Need Init Einstellungen
         * 
         * @param {Boolean} flag    Neuer Wert
         * 
         * @static
         * @memberof Base
         */
        static set needInit(flag) {
            this._needInit = (flag === true);
        }

        /**
         * Basis Constructor
         * 
         * @param {HTMLElement}     element     Das HTML Element
         * @param {Object|NULL}     options     Eigene Optionen oder NULL
         * 
         * @memberof Base
         */
        constructor(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._listeners = {};
        }

        /**
         * Gibt die Standard-Optionen eines Moduls zurück
         *
         * @returns {Object}    Objekt mit den Stanard-Optionen des Moduls
         */
        getDefaultOptions() {
            let self = this;
            if ("DEFAULT_OPTIONS" in self.constructor) {
                return self.constructor.DEFAULT_OPTIONS;
            }
            return {};
        }

        /**
         * Getter/Setter für Optionen
         *
         * Wird der Wert *name* nicht übergeben, werden alle Optionen zurückgegeben.
         * Wird *name* übergeben, aber *value* nicht wird der Wert für *name*
         * zurückgegeben oder *NULL* wenn *name* nicht Existiert. Werden sowohl
         * *name* als auch *value*`übergeben, wird Option *name* auf *value* gesetzt.
         * In diesem Fall wird *this* zurückgegeben.
         *
         * @param {String}  name    Optionaler Name einer Option
         * @param {Mixed}   value   Optionaler Wert einer Option
         *
         * @returns {Mixed}
         */
        option(name, value) { 
            if (typeof name === "undefined") {
                return this.options;
            }
            if(typeof name === 'object') {
                this.options = name;
            }
            if (typeof value === "undefined") {
                if (typeof this.options[name] !== "undefined") {
                    return this.options[name];
                }
                return null;
            }
            this.options[name] = value;

            return this;
        }

        /**
         * Bilden der Optionen
         *
         * Basierend auf den Standard-Optionen, den übergebenen Optionen des
         * Constructors und den Element Data-Attributen werden die aktuellen
         * Konfigurationen gebildet. Letztenendes gewinnen die Element
         * Data-Attribute.
         *
         * @param {Object|NULL} options    Optionale eigene Optionen
         */
        buildOptions(options) {
            let data = {};
            if (typeof this.element !== 'undefined') {
                data = this.element.acmsData();
            }
            return Extend.deep({}, true, this.getDefaultOptions(), options, data);
        }

        /**
         * Add a callback to module
         * 
         * This is currently not supported/used in many modules, but we're working on.
         * See modules for possible support of `evt`
         *
         * @param {String}              evt         Name of Event
         * @param {CallableFunction}    handler     Callback
         * 
         * @returns {Base} this instance
         * 
         * @example 
         * Module.on('done', () => {alert('done!')});
         * 
         * @memberof Base
         */
        on(evt, handler) {
            if(typeof this._listeners[evt] === "undefined") {
                this._listeners[evt] = [];
            }
            this._listeners[evt].push(handler);

            return this;
        }

        /**
         * remove a callback from module
         * 
         * This is currently not supported/used in many modules, but we're working on.
         * See modules for possible support of `evt`
         *
         * @param {String}              evt         Name of Event
         * @param {CallableFunction}    handler     Callback
         * 
         * @returns {Base}  Current istance
         * 
         * @example 
         * Module.off('done', () => {alert('done!')});
         * 
         * @memberof Base
         */
        off(evt, handler) {
            if(typeof this._listeners[evt] === "undefined") {
                return;
            }
            this._listeners.filter(callback => callback !== handler);

            return this;
        }

        /**
         * emit an Event
         *
         * @param {String}  evt     Event Name
         * @param {...*}    args    Arguments
         * 
         * @example 
         * Module.emit('done', response, self);
         * 
         * @memberof Base
         */
        emit(evt, ...args) {
            if(typeof this._listeners[evt] === "undefined") {
                return;
            }
            let i, cb, len = this._listeners[evt].length;
            for(i = 0; i < len; i++) {
                cb = this._listeners[evt][i];
                cb(...args);
            }

        }

        /**
         * @private
         * @memberof Base
         */
        _initialize() {
            let self = this;
            if ("initialize" in self) {
                self.initialize(element, options);
            }
            if ("_prepare" in self) {
                self._prepare();
            }
            self._prepareSelf(this);
            if ("listen" in self) {
                this.listen();
            }
        }

        /**
         * Binden aller "privater" Funktionen an die Klasse
         * 
         * @access private
         */
        _prepareSelf(clsName) {
            let self = this;
            ClassTool.bindMethods(clsName, self);
        }

    }

    return Base;

});
