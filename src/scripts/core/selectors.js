/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */

/**
 * Selectors
 * 
 * @param {Object} doc  Doc Module
 * 
 * @returns {Selectors} Das Selectors Modul
 */
define("core/selectors", [
    "core/document"
], function(doc) {

    /**
     * @type function
     */
    const slice = Array.prototype.slice;

    /**
     * Sibling finden
     * 
     * @param {HTMLElement} current Aktuelles Element
     * @param {String} direction    Richtung in der gesucht werden soll
     * 
     * @returns {HTMLElement}
     */
    const sibling = (current, direction) => {
        while ((current = current[direction]) && current.nodeType !== 1) {}

        return current;
    };

    /**
     * Hilft-Modul für das finden von HTML Elementen
     * 
     * @exports core/selectors
     *
     * @class Selectors
     */
    class Selectors {
        
        /**
         * Hook zu querySelector(selector)
         *
         * @param {String}    selector   Kann jede Form eines Selector
         *                                         sein
         * @param {Object}              scope      Der Scope innerhalb dessen
         *                                         gesucht werden soll. Wenn
         *                                         kein Scope angegeben ist,
         *                                         wird das document Objekt
         *                                         genommen.
         *
         * @throws {SYNTAX_ERR} Wenn der Selector-Parameter invalide ist
         * @returns {Object|Element|NULL} Ein Element-Objekt oder NULL wenn
         *                                kein Ergebnis.
         */
        static q(selector, scope) {
            if (null === scope || typeof scope === "undefined") { scope = doc; }
            return scope.querySelector(selector);
        }

        /**
         * Hook zu querySelectorAll(selector)
         *
         * @param {String}    selector   Kann jede Form eines Selector
         *                                         sein
         * @param {Object}              scope      Der Scope innerhalb dessen
         *                                         gesucht werden soll. Wenn
         *                                         kein Scope angegeben ist,
         *                                         wird das document Objekt
         *                                         genommen.
         *
         * @throws {SYNTAX_ERR} Wenn der Selector-Parameter invalide ist
         * @returns {Array|Element[]|NULL} Ein Element-Array oder NULL wenn
         *                                kein Ergebnis.
         */
        static qa(selector, scope) {
            if (null === scope || typeof scope === "undefined") { scope = doc; }
            return slice.call(scope.querySelectorAll(selector));
        }

        /**
         * wie document.getElementById
         * 
         * @param {String} id ID des gesuchten Elements
         * 
         * @returns {HTMLElement|NULL}  Das Element oder NULL
         */
        static byID(id) {
            return doc.getElementById(id);
        }


        /**
         * Findet Elemente nach Class Name
         * 
         * @param  {String} tagName     Tag Name
         * @param  {HTMLElement} src    Optionales HMTLElement. Wenn nicht angegeben wird document verwendet.
         */
        static byTagName(tagName, src) {
            src = src || doc;
            return src.getElementsByTagName(tagName);
        }

        /**
         * @param  {String} className     Klassen Name
         * @param  {HTMLElement|null} scope Optional ein HTMLElement in dem gesucht werden soll. Wenn nicht angegeben wird document verwendet.
         */
        static byClassName(className, scope) {
            scope = scope || doc;
            return slice.call(scope.getElementsByClassName(className));
        }

        /**
         * Finden eines passenden Eltern-Elementes
         * 
         * @param {HTMLElement} el Das Element
         * @param {String} selector Der Selector
         * 
         * @returns {HTMLElement|null}
         */
        static closest(el, selector) {
            let parent;
            while (el !== null) {
                parent = el.parentElement;
                if (parent !== null &&
                    this.matches(parent, selector)) {
                    return parent;
                }
                el = parent;
            }

            return null;
        }


        /**
         * 
         * @param  {HTMLElement} elem   Das HTML Element
         * @param  {String} selector    Optionaler Selector
         */
        static previous(elem, selector) {
            let matches = sibling(elem, "previousElementSibling"),
                el = matches,
                /*e2 = elem.previousElementSibling,*/
                brk = false;
            if (!el) {
                return false;
            }
            if (typeof selector !== "string" || el.classList.contains(selector)) {
                return el;
            }
            while (el && false === brk) {
                el = sibling(el, "previousElementSibling");
                if (el && el.classList.contains(selector)) {
                    brk = el;
                }
            }
            return brk;
        }

        /**
         * Findet vorangehende Siblings
         * 
         * @param  {HTMLElement} element    Das Element
         * @param  {Callable|String} filter    Optionaler Filter als Selector Angabe oder Callable. Callable muss TRUE oder FALSE zurückgeben.
         */
        static previousAll(element, filter) {
            let self = this,
                result = [];
            while (element = element.previousElementSibling) {
                if (element.nodeType === 3) continue; // text node
                if (!filter) {
                    result.push(element)
                } else if (typeof filter === 'string' && self.matches(element, filter)) {
                    result.push(element)
                } else if (filter && typeof filter !== 'string' && filter(elem)) {
                    result.push(element)
                }
            }
            return result;
        }

        /**
         * Findet alle nächsten Siblings
         * 
         * @param {Node} elem
         * @param {Callable|String} filter  Der Filter kann ein String oder ein 
         *                                 Callback sein. 
         *                                 Wird ein Callback Filter übergeben, 
         *                                 muss dieser TRUE/FALSE zurückgeben; 
         *                                 TRUE für matching.
         *                                 Wird der Filter als String übergeben 
         *                                 so wird die interne matches() funktion
         *                                 aufgerufen
         * 
         * @returns {Array}
         */
        static nextAll(elem, filter) {
            let sibs = [],
                self = this;
            while (elem = elem.nextSibling) {
                if (elem.nodeType === 3) continue; // text node
                if (!filter) {
                    sibs.push(elem)
                } else if (typeof filter === 'string' && self.matches(elem, filter)) {
                    sibs.push(elem)
                } else if (filter && typeof filter !== 'string' && filter(elem)) {
                    sibs.push(elem)
                }
            }
            return sibs;
        }

        /**
         * Findet alle nächsten Siblings
         * 
         * @param {Node} elem
         * @param {Callable} filter  Callback Filter, optional. Muss TRUE/FALSE zurückgeben
         * 
         * @returns {HTMLElement|null}
         */
        static next(elem, filter) {
            let sibs = null;
            while (elem = elem.nextSibling) {
                if (elem.nodeType === 3) continue; // text node
                if (!filter || filter(elem)) {
                    sibs = elem;
                    break;
                } else if (typeof filter === "string" && this.matches(elem, filter)) {
                    sibs = elem;
                    break;
                } else if (filter && typeof filter !== 'string' && filter(elem)) {
                    sibs = elem;
                    break;
                }
            }
            return sibs;
        }

        /**
         * Ist ein Element sicht bar?
         * @return {Boolean}
         */
        static isVisible(elem) {
            let style = window.getComputedStyle(elem);
            return style.width !== 0 &&
                style.height !== 0 &&
                style.opacity !== 0 &&
                style.display !== 'none' &&
                style.visibility !== 'hidden';
        }

        /**
         * Findet alle Eltern-Nodes
         * 
         * @param {Node} element
         * @param {Callable} filter  Callback Filter, optional. 
         *                          Muss TRUE/FALSE zurückgeben, wenn angegeben
         * 
         * @returns {Array}
         */
        static parents(element, filter) {
            let els = [],
                a = element;
            while (a) {
                if (!filter || true === filter(a)) {
                    els.unshift(a);
                }
                a = a.parentNode;
            }
            return els;
        }

        /**
         * prüft das Element auf den Selector
         * 
         * @param {HTMLElement} element     Das Element
         * @param {String}      selector    Der Selector
         * 
         * @returns {Boolean}
         */
        static matches(element, selector) {
            let matchesFn, parent;
            ['matches', 'webkitMatchesSelector', 'mozMatchesSelector', 'msMatchesSelector', 'oMatchesSelector'].some(function(fn) {
                if (typeof doc.body[fn] === 'function') {
                    matchesFn = fn;
                    return true;
                }
                return false;
            });
            return element[matchesFn](selector);
        }

        /**
         * Verschiebt Inhalte eines Elementes so lange, bis es keine mehr hat und entfernt daraufhin das Element.
         * 
         * @param {HTMLElement} element Das Element
         */
        static unwrap(element) {

            let pa = element.parentNode;
            while (element.firstChild) pa.insertBefore(element.firstChild, element);

            pa.removeChild(element);
        }
    }

    return Selectors;

});
