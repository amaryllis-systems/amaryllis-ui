/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */

/**
 * Ajax Uploader
 *
 * <p>
 *  Hilfs-Module um ein Formular-Feld in ein Live-Upload Feld zu transformieren.
 *  <b>ACHTUNG</b>: Dies funktioniert nicht garantiert für alle Upload-Felder!
 *  Diese Funktionalität muss auch aus dem Backend heraus unterstützt werden!
 * </p>
 *
 * @param {Object} Utils
 * @param {Object} AcmsEvent
 * @param {Object} Selectors
 * @param {Object} Request
 * 
 * @returns {Uploader}
 */
define("ui/media/uploader", [
    "tools/utils",
    "events/event",
    'core/selectors',
    'http/request'
], function (Utils, AcmsEvent, Selectors, Request) {

    "use strict";

    /**
     * Constructor
     * 
     * @param {Object} element  Das Upload-Feld
     * @param {Object} options  Die Uploader-Optionen
     *
     * @returns {Uploader}
     */
    var Uploader = function(element, options) {
        this.element            =
        this.$container         =
        this.$errorContainer    =
        this.options            = null;
        this.initialize(element, options);
    };
    
    var UPLOADER_DATA   = "acms.ui.media.uploader",
        NAMESPACE       = "." + UPLOADER_DATA,
        EVENT_UPLOAD  = "upload" + NAMESPACE,
        EVENT_UPLOADED  = "uploaded" + NAMESPACE;

    /**
     * Komponenten Name
     *
     * @var {String}
     */
    Uploader.MODULE = "Uploader";

    /**
     * Standard-Optionen
     *
     * @var {Object}
     */
    Uploader.DEFAULT_OPTIONS = {
        url: undefined,
        deleteurl: undefined,
        accept: undefined,
        data: '', /* additional data as object {key: value, ...} */
        maxfiles: 1, /* maximum files */
        maxfilesize: 2000000, /* maximum allowed filesize */
        hiddenfield: "", /* hidden form-field to submit ele-names */
        delimg: "acms-icon acms-icon-trash-alt", /* delete icon to be displayed behind filename */
        container: '#file-container ul',
        denied: 'Sie können keine $ext Datei auswählen.',
        file: '$file',
        error_container: '#uploader-errors',
        autoupload: false,
        uploadtrigger: '[data-role="file-upload"]',
        deletetrigger: '[data-role="file-remove"]'
    };
    
    Uploader.needInit = true;
    
    Uploader.init = function(element, options) {
        var up = new Uploader(element, options);
        up.element.acmsData(UPLOADER_DATA, up);
        
        return up;
    }

    /**
     * Klassen Definition
     */
    Uploader.prototype = {
        /**
         * Interner Initialisator
         *
         * @param {Object} element  Das Upload-Feld
         * @param {Object} options    Die Upload-Optionen
         *
         * 
         */
        initialize: function(element, options) {
            this.element = element,
            this.options = this.buildOptions(options);
            this.$container = document.querySelector(this.options.container);
            this.$errorContainer = document.querySelector(this.options.error_container);
            this.listen();

        },
        /**
         * Gibt die Standard-Optionen zurück
         * 
         * @returns {Uploader.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function() {
            return Uploader.DEFAULT_OPTIONS;
        },
        /**
         * Bildet die eigenen Optionen
         * 
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function(options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },

        /**
         * Gitbt eine Option zurück
         *
         * @param {String} key  Der Schlüssel-Name der Option
         *
         * @returns {Boolean|Object}
         */
        getOption: function(key) {
            return this.options[key] || false;
        },
        /**
         * Aufsetzen der Event-Listener
         *
         * 
         */
        listen: function() {
            var self = this;
            if(self.options.autoupload) {
                var changeFileUpload = function(e) {
                    self.upload(e);
                };
                AcmsEvent.add(this.element, 'change', changeFileUpload);
            }
            var trigger = document.querySelector(self.options.uploadtrigger);
            if(trigger) {
                var fileUploadTrigger = function(e2) {
                    self.upload(e2);
                };
                AcmsEvent.add(trigger, 'click', fileUploadTrigger);
            }
        },

        /**
         * Führt den Ajax-Upload aus
         *
         * 
         */
        upload: function(event) {
            var self = this,
                element = event.target || this.element,
                file = element.files[0],
                form = Selectors.closest(this.element, 'form'),
                ftype = file.type,
                fname = file.name,
                o = this.options,
                mimeaccept = element.getAttribute("accept"),
                formData = new FormData(form),
                accept;
            
            var listitems = Selectors.qa('li', self.$container);
            if (listitems.length == self.options.maxfiles) {
                console.log("Maximal-Anzahl überschritten")
                return;
            }
            if (o.data !== "") {
                var key, d = o.data;
                for(key in d) {
                    if(d.hasOwnProperty(key)) {
                        formData.append(key, d[key]);
                    }
                }
            }
            if (!file) {
                console.log("Es wurde noch keine Datei ausgewählt!")
                Acms.Logger.writeLog("Es wurde noch keine Datei ausgewählt!");
                return;
            }
            if (o.url === undefined) {
                console.log("Es wurde noch keine Remote URL ausgewählt!")
                Acms.Logger.logWarning("Es wurde keine Remote-URL angegeben!")
                return;
            }
            if (o.accept === undefined && !mimeaccept) {
                accept = "all";
            } else if (self.options.accept === undefined && mimeaccept) {
                accept = mimeaccept;
            } else if (self.options.accept && mimeaccept) {
                accept = self.options.accept + ',' + mimeaccept;
            } else {
                accept = self.options.accept;
            }
            var index = accept === "all" ? 1 : accept.indexOf(ftype);
            if (index < 0) {
                var error_container = self.$errorContainer,
                    li = document.createElement('li'),
                    msg = self.options.denied.replace("$ext", fname.split(".").pop().toLowerCase());
                li.textContent = msg;
                error_container.appendChild(li);
            }
            var e = AcmsEvent.createCustom(EVENT_UPLOAD, {accept: accept, formData: formData, fileType: ftype, fileName: fname});
            AcmsEvent.dispatch(self.element, e);
            var done = function(response) {
                return self._done(response);
            }, fail = function(jqXHR, textStatus) {
                Acms.Logger.logWarning(textStatus);
            };
            Request.postFile(o.url, formData, done, fail);
        },
        
        _done: function(response) {
            Acms.Logger.writeLog(response.message);
            var self = this,
                o = this.options,
                element = this.element,
                container = self.$container,
                li = document.createElement('li'),
                hidden = (o.hiddenfield !== "") 
                            ? document.querySelector(o.hiddenfield) 
                            : false;
            if (response.status === "success") {
                var img = document.createElement('span'),
                    error_container = self.$errorContainer;
                if(error_container) {
                    var attr = {
                        'data-filename': response.filename,
                        "class": self.options.delimg,
                        'data-role': "remove-image",
                    };
                    for(var key in attr) {
                        if(attr.hasOwnProperty(key)) {
                            img.setAttribute(key, attr[key]);
                        }
                    }
                    li.appendedNode(o.file.replace("$file", response.filename));
                    li.appendChild(img);
                    
                    attr = {
                          'data-filename': response.filename,
                          "data-eleid": self.options.hiddenfield.replace("#", ""),
                          "data-container": self.options.container.replace('#', '')
                      };
                    for(var k in attr) {
                        if(attr.hasOwnProperty(k)) {
                            li.setAttribute(k, attr[k]);
                        }
                    }
                }
                element.value = "";
                self.$container.appendChild(li);
                var listitems = Selectors.qa('li', self.$container);
                if (listitems.length == self.options.maxfiles) {
                    element.setAttribute("disabled", "disabled");
                }
                if (hidden) {
                    var oldVal = hidden.value;
                    if (oldVal === "") {
                        hidden.value = response.filename;
                    } else {
                        hidden.value = oldVal + '|' + response.filename;
                    }
                }
                if(self.$errorContainer && self.$errorContainer.innerHtml) {
                    error_container.innerHTML = '';
                }
                var e = AcmsEvent.createCustom(EVENT_UPLOADED, {response: response});
                AcmsEvent.dispatch(element, e);

            } else if (response.status === "error") {
                if(self.$errorContainer) {
                    li.textContent = response.message;
                    self.$errorContainer.appendChild(li);
                    element.value = "";
                }
            }
        }
    };

    return Uploader;

});
