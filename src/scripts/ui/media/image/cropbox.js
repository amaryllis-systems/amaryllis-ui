/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */


define("ui/media/image/cropbox", [
    "tools/utils",
    "core/window",
    "core/style",
    "core/classes",
    "core/selectors",
    "events/event"
], function (Utils, Win, Style, Classes, Selectors, AcmsEvent) {

    'use strict';
    
    
    var location = Win.location,
        // RegExps
        REGEXP_DRAG_TYPES = /^(e|n|w|s|ne|nw|sw|se|all|crop|move|zoom)$/,
        // Classes
        CLASS_MODAL = 'cropbox-modal',
        CLASS_HIDE = 'cropbox-hide',
        CLASS_HIDDEN = 'cropbox-hidden',
        CLASS_INVISIBLE = 'cropbox-invisible',
        CLASS_MOVE = 'cropbox-move',
        CLASS_CROP = 'cropbox-crop',
        CLASS_DISABLED = 'cropbox-disabled',
        CLASS_BG = 'cropbox-bg',
        
        CANVAS = document.createElement('canvas'),
        SUPPORT_CANVAS = CANVAS.getContext || false,
        // Others
        sqrt = Math.sqrt,
        min = Math.min,
        max = Math.max,
        abs = Math.abs,
        sin = Math.sin,
        cos = Math.cos,
        num = parseFloat;

    function isNumber(n) {
        return typeof n === 'number' && !isNaN(n);
    }

    function isUndefined(n) {
        return typeof n === 'undefined';
    }

    function toArray(obj, offset) {
        var args = [];

        if (isNumber(offset)) {
            args.push(offset);
        }

        return args.slice.apply(obj, args);
    }
    function proxy(fn, context) {
        var args = toArray(arguments, 2);

        return function () {
            return fn.apply(context, args.concat(toArray(arguments)));
        };
    }

    function isCrossOriginURL(url) {
        var parts = url.match(/^(https?:)\/\/([^\:\/\?#]+):?(\d*)/i);

        return parts && (parts[1] !== location.protocol || parts[2] !== location.hostname || parts[3] !== location.port);
    }

    function addTimestamp(url) {
        var timestamp = 'timestamp=' + (new Date()).getTime();

        return (url + (url.indexOf('?') === -1 ? '?' : '&') + timestamp);
    }

    function getRotateValue(degree) {
        return degree ? 'rotate(' + degree + 'deg)' : 'none';
    }

    function getRotatedSizes(data, reverse) {
        var deg = abs(data.degree) % 180,
                arc = (deg > 90 ? (180 - deg) : deg) * Math.PI / 180,
                sinArc = sin(arc),
                cosArc = cos(arc),
                width = data.width,
                height = data.height,
                aspectRatio = data.aspectRatio,
                newWidth,
                newHeight;

        if (!reverse) {
            newWidth = width * cosArc + height * sinArc;
            newHeight = width * sinArc + height * cosArc;
        } else {
            newWidth = width / (cosArc + sinArc / aspectRatio);
            newHeight = newWidth / aspectRatio;
        }

        return {
            width: newWidth,
            height: newHeight
        };
    }

    function getSourceCanvas(image, data) {
        var canvas = document.createElement('canvas'),
                context = canvas.getContext('2d'),
                width = data.naturalWidth,
                height = data.naturalHeight,
                rotate = data.rotate,
                rotated = getRotatedSizes({
                    width: width,
                    height: height,
                    degree: rotate
                });

        if (rotate) {
            canvas.width = rotated.width;
            canvas.height = rotated.height;
            context.save();
            context.translate(rotated.width / 2, rotated.height / 2);
            context.rotate(rotate * Math.PI / 180);
            context.drawImage(image, -width / 2, -height / 2, width, height);
            context.restore();
        } else {
            canvas.width = width;
            canvas.height = height;
            context.drawImage(image, 0, 0, width, height);
        }

        return canvas;
    }
    
    /**
     * CropBox Constructor
     * 
     * @param {HTMLElement} element
     * @param {Object} options
     * 
     * @exports ui/media/images/cropbox
     */
    var CropBox = function (element, options) {
        this.element    =
        this.options    = null;
        this.ready = false;
        this.built = false;
        this.rotated = false;
        this.cropped = false;
        this.disabled = false;
        this.replaced = false;
        this.isImg = false;
        this.originalUrl = '';
        this.canvas = null;
        this.cropBox = null;

        this.initialize(element, options);
    };

    /**
     * Modul Name
     * 
     * @var {String}
     */
    CropBox.MODULE = "CropBox";
    
    /**
     * Modul Version
     * 
     * @var {String}
     */
    CropBox.VERSION = "1.5.0";
    
    /**
     * Modul Namespace
     * 
     * @var {String}
     */
    CropBox.NS = "acms.ui.media.image.cropbox";
    
    CropBox.BOX_PREVIEW = CropBox.NS + '.preview';
    
    /**
     * Einbindend des Auto-Modul Loader
     * 
     * @var {Boolean}
     */
    CropBox.needInit = true;

    /**
     * Initialisierung des Moduls
     * 
     * Erstellt eine neue CropBox
     * 
     * @param {HTMLElement} element
     * @param {Object} options
     * 
     * @returns {CropBox}
     */
    CropBox.init = function(element, options) {
        var c = new CropBox(element, options);
        c.element.acmsData(CropBox.NS, c);
        
        return c;
    };
    
    /**
     * Mouse Move Events
     * 
     * @var {Array}
     */
    CropBox.EVENT_MOUSE_MOVE = ['mousemove', 'touchmove', 'pointermove', 'MSPointerMove'];
    
    /**
     * Mouse Down Events
     * 
     * @var {Array}
     */
    CropBox.EVENT_MOUSE_DOWN = ['mousedown', 'touchstart', 'pointerdown', 'MSPointerDown'];
    
    /**
     * Mouse Up Events
     * 
     * @var {Array}
     */
    CropBox.EVENT_MOUSE_UP = ['mouseup', 'touchend', 'touchcancel', 'pointerup', 'pointercancel', 'MSPointerUp', 'MSPointerCancel'];
    
    /**
     * Mouse Wheel Events
     * 
     * @var {Array}
     */
    CropBox.EVENT_WHEEL = ['wheel', 'mousewheel', 'DOMMouseScroll'];
    
    /**
     * Doble Click Event
     * 
     * @var {String}
     */
    CropBox.EVENT_DBLCLICK = 'dblclick';
    
    /**
     * Build Event
     * 
     * Event wird vor dem eigentlichen Build-Vorgang getriggert.
     * 
     * @var {String}
     */
    CropBox.EVENT_BUILD = 'build';
    
    /**
     * Built Event
     * 
     * Event wird nach dem Build-Vorgang getriggert, wenn dieser abgeschlossen 
     * ist.
     * 
     * @var {String}
     */
    CropBox.EVENT_BUILT = 'built';
    
    /**
     * Drag Start Event
     * 
     * @var {String}
     */
    CropBox.EVENT_DRAG_START = 'dragstart';
    
    /**
     * Drag Move Event
     * 
     * @var {String}
     */
    CropBox.EVENT_DRAG_MOVE = 'dragmove';
    
    /**
     * Drag End Event
     * 
     * @var {String}
     */
    CropBox.EVENT_DRAG_END = 'dragend';
    
    /**
     * Event Resize
     * 
     * @var {String}
     */
    CropBox.EVENT_RESIZE = 'resize';
    
    /**
     * Zoom-In Event
     * 
     * @var {String}
     */
    CropBox.EVENT_ZOOMIN = 'zoomin';
    
    /**
     * Zoom-Out Event
     * 
     * @var {String}
     */
    CropBox.EVENT_ZOOMOUT = 'zoomout';
    
    /**
     * Change Event
     * 
     * @var {String}
     */
    CropBox.EVENT_CHANGE = 'change';
    
    
    /**
     * Type Function
     * 
     * @var {String}
     */
    CropBox.TYPE_FUNCTION = 'function';
    
    /**
     * Type Object
     * 
     * @var {String}
     */
    CropBox.TYPE_OBJECT = 'object';
    
    CropBox.DEFAULT_OPTIONS = {
        // Defines the aspect ratio of the crop box
        // Type: Number (16/9 1/1 4/3 2/1)
        aspectRatio: 1,
        // Defines the percentage of automatic cropping area when initializes
        // Type: Number (Must large than 0 and less than 1)
        autoCropArea: 0.8, // 80%

        // Outputs the cropping results.
        // Type: Function
        crop: null,
        // Previous/latest crop data
        // Type: Object
        data: null,
        // Add extra containers for previewing
        // Type: String (selector)
        preview: '',
        // Toggles
        strict: true, // strict mode, the image cannot zoom out less than the container
        responsive: true, // Rebuild when resize the window
        checkImageOrigin: true, // Check if the target image is cross origin

        modal: true, // Show the black modal
        guides: true, // Show the dashed lines for guiding
        center: true, // Show the center indicator for guiding
        highlight: true, // Show the white modal to highlight the crop box
        background: true, // Show the grid background

        autoCrop: true, // Enable to crop the image automatically when initialize
        dragCrop: true, // Enable to create new crop box by dragging over the image
        movable: true, // Enable to move the image
        rotatable: true, // Enable to rotate the image
        zoomable: true, // Enable to zoom the image
        touchDragZoom: true, // Enable to zoom the image by wheeling mouse
        mouseWheelZoom: true, // Enable to zoom the image by dragging touch
        cropBoxMovable: true, // Enable to move the crop box
        cropBoxResizable: false, // Enable to resize the crop box
        doubleClickToggle: true, // Toggle drag mode between "crop" and "move" when double click on the cropbox

        /* Dimensions */
        minCanvasWidth: 0,
        minCanvasHeight: 0,
        minCropBoxWidth: 80,
        minCropBoxHeight: 80,
        minContainerWidth: 200,
        minContainerHeight: 100,
        /* Events */
        onBuild: null, 
        onBuilt: null, 
        onDragstart: null, 
        onDragmove: null, 
        onDragend: null, 
        onZoomin: null, 
        onZoomout: null, 
        onChange: null 
    };

    CropBox.prototype = {
        
        /**
         * Interner Constructor
         *
         * @param {type} element    Das Element
         * @param {type} options    Eigene Optionen
         * 
         * 
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = Utils.extend({}, CropBox.DEFAULT_OPTIONS,options, this.element.acmsData());
            var url;
            if (this.element.tagName.toUpperCase() === 'IMG') {
                this.isImg = true;
                this.originalUrl = this.element.getAttribute('src');

                if (!this.originalUrl) {
                    Acms.Logger.logWarning('Bilder-URL nicht gefunden!');
                    return;
                }

                url = this.element.getAttribute('src');
            } else if (this.element.tagName.toLowerCase() === 'canvas' && SUPPORT_CANVAS) {
                url = this.element.toDataURL();
            }

            this.load(url);
        },
        
        
        load: function (url) {
            var self = this,
                options = self.options,
                element = self.element,
                crossOrigin,
                bustCacheUrl,
                buildEvent,
                $clone;

            if (!url) {
                Acms.Logger.logWarning('Bilder-URL nicht übergeben!');
                return;
            }

            buildEvent = this.triggerEvent(CropBox.EVENT_BUILD);

            if (options.checkImageOrigin && isCrossOriginURL(url)) {
                crossOrigin = ' crossOrigin="anonymous"';

                if (!this.element.crossOrigin) {
                    bustCacheUrl = addTimestamp(url);
                }
            }

            this.$clone = document.createElement('img');
            this.$clone.setAttribute('src', (bustCacheUrl || url));
            
            $clone = this.$clone;
            $clone.onload = function() {
                var image = $clone,
                        naturalWidth = image.naturalWidth || image.width,
                        naturalHeight = image.naturalHeight || image.height;

                self.image = {
                    naturalWidth: naturalWidth,
                    naturalHeight: naturalHeight,
                    aspectRatio: naturalWidth / naturalHeight,
                    rotate: 0
                };

                self.url = url;
                self.ready = true;
                self.build();
            };
            $clone.onerror = function () {
                Acms.Logger.logWarning("Fehler beim bilden der Cropbox")
                $clone.parentNode.removeChild($clone);
            };
            element.parentNode.appendChild($clone);
            Classes.addClass($clone, CLASS_HIDE);
        },
        
        
        build: function () {
            var element = this.element,
                    $clone = this.$clone,
                    options = this.options,
                    $cropbox,
                    $cropBox,
                    $face;

            if (!this.ready) {
                Acms.Logger.logWarning("CropBox Build fehlgeschlagen! Die Cropbox wurde nicht richtig initialisiert!");
                return;
            }
            if (this.built) {
                this.unbuild();
            }
            var cropDiv = document.createElement('div');
            cropDiv.innerHTML = CropBox.TEMPLATE;
            
            this.$container = element.parentNode;
            while(cropDiv.firstChild) {
                this.$container.appendChild(cropDiv.firstChild);
            }
            this.$cropbox = $cropbox = Selectors.q(".cropbox-container", this.$container);
            Classes.addClass(element, CLASS_HIDDEN);
            Classes.removeClass($clone, CLASS_HIDE);
            
            this.$canvas = Selectors.q('.cropbox-canvas', $cropbox)
            this.$canvas.appendChild($clone);
            
            this.$dragBox = Selectors.q('.cropbox-drag-box', $cropbox);
            this.$cropBox = $cropBox = Selectors.q('.cropbox-crop-box', $cropbox);
            
            this.$viewBox = Selectors.q('.cropbox-view-box', $cropbox);
            this.$face = $face = Selectors.q('.cropbox-face', $cropBox);

            this.addListeners();
            this.initPreview();
            options.aspectRatio = num(options.aspectRatio) || NaN;
            if (options.autoCrop) {
                this.cropped = true;
                if (options.modal) {
                    Classes.addClass(this.$dragBox, CLASS_MODAL);
                }
            } else {
                Classes.addClass($cropBox, CLASS_HIDDEN);
            }
            if (!options.guides) {
                var dashed = Selectors.q('.cropbox-dashed');
                Classes.addClass(dashed, CLASS_HIDDEN);
            }
            if (!options.center) {
                var cbCenter = Selectors.q('.cropbox-center', $cropBox);
                Classes.addClass(cbCenter, CLASS_HIDDEN);
            }
            if (options.cropBoxMovable) {
                Classes.addClass($face, CLASS_MOVE)
                $face.acmsData('drag', 'all');
            }
            if (!options.highlight) {
                Classes.addClass($face, CLASS_INVISIBLE);
            }
            if (options.background) {
                Classes.addClass($cropbox, CLASS_BG);
            }
            if (!options.cropBoxResizable) {
                var cbPoints = Selectors.qa('.cropbox-line, .cropbox-point'), cbp;
                for(cbp = 0; cbp < cbPoints.length; cbp++) {
                    Classes.addClass(cbPoints[cbp], CLASS_HIDDEN);
                }
            }
            this.setDragMode(options.dragCrop ? 'crop' : options.movable ? 'move' : 'none');
            this.built = true;
            this.render();
            this.setData(options.data);
            this.triggerEvent(CropBox.EVENT_BUILT);
        },
        
        unbuild: function () {
            if (!this.built) {
                return;
            }

            this.built = false;
            this.initialImage = null;
            this.initialCanvas = null;
            this.initialCropBox = null;
            this.container = null;
            this.canvas = null;
            this.cropBox = null;
            this.removeListeners();

            this.resetPreview();
            this.$preview = null;

            this.$viewBox = null;
            this.$cropBox = null;
            this.$dragBox = null;
            this.$canvas = null;
            this.$container = null;

            this.$cropbox.parentNode.removeChild(this.$cropbox);
            this.$cropbox = null;
        },
        
        /**
         * Rendern der CropBox
         * 
         * 
         */
        render: function () {
            this.initContainer();
            this.initCanvas();
            this.initCropBox();

            this.renderCanvas();

            if (this.cropped) {
                this.renderCropBox();
            }
        },
        
        /**
         * Container Initialisierung
         * 
         * 
         */
        initContainer: function () {
            var self = this,
                element = self.element,
                $container = self.$container,
                $cropbox = self.$cropbox,
                options = self.options;
            Classes.addClass($cropbox, CLASS_HIDDEN);
            Classes.removeClass(element, CLASS_HIDDEN);
            var container = {
                width: max($container.width(), num(options.minContainerWidth) || 200),
                height: max($container.height(), num(options.minContainerHeight) || 100)
            };
            Style.setStyles(this.$cropbox, container);
            this.container = container;
            Classes.addClass(element, CLASS_HIDDEN);
            Classes.removeClass($cropbox, CLASS_HIDDEN);
        },
        
        initCanvas: function () {
            var container = this.container,
                    containerWidth = container.width,
                    containerHeight = container.height,
                    image = this.image,
                    aspectRatio = image.aspectRatio,
                    canvas = {
                        aspectRatio: aspectRatio,
                        width: containerWidth,
                        height: containerHeight
                    };

            if (containerHeight * aspectRatio > containerWidth) {
                canvas.height = containerWidth / aspectRatio;
            } else {
                canvas.width = containerHeight * aspectRatio;
            }

            canvas.oldLeft = canvas.left = (containerWidth - canvas.width) / 2;
            canvas.oldTop = canvas.top = (containerHeight - canvas.height) / 2;

            this.canvas = canvas;
            this.limitCanvas(true, true);
            this.initialImage = Utils.extend({}, image);
            this.initialCanvas = Utils.extend({}, canvas);
        },
        
        
        limitCanvas: function (size, position) {
            var options = this.options,
                    strict = options.strict,
                    container = this.container,
                    containerWidth = container.width,
                    containerHeight = container.height,
                    canvas = this.canvas,
                    aspectRatio = canvas.aspectRatio,
                    cropBox = this.cropBox,
                    cropped = this.cropped && cropBox,
                    initialCanvas = this.initialCanvas || canvas,
                    initialCanvasWidth = initialCanvas.width,
                    initialCanvasHeight = initialCanvas.height,
                    minCanvasWidth,
                    minCanvasHeight;

            if (size) {
                minCanvasWidth = num(options.minCanvasWidth) || 0;
                minCanvasHeight = num(options.minCanvasHeight) || 0;

                if (minCanvasWidth) {
                    if (strict) {
                        minCanvasWidth = max(cropped ? cropBox.width : initialCanvasWidth, minCanvasWidth);
                    }

                    minCanvasHeight = minCanvasWidth / aspectRatio;
                } else if (minCanvasHeight) {
                    if (strict) {
                        minCanvasHeight = max(cropped ? cropBox.height : initialCanvasHeight, minCanvasHeight);
                    }

                    minCanvasWidth = minCanvasHeight * aspectRatio;
                } else if (strict) {
                    if (cropped) {
                        minCanvasWidth = cropBox.width;
                        minCanvasHeight = cropBox.height;

                        if (minCanvasHeight * aspectRatio > minCanvasWidth) {
                            minCanvasWidth = minCanvasHeight * aspectRatio;
                        } else {
                            minCanvasHeight = minCanvasWidth / aspectRatio;
                        }
                    } else {
                        minCanvasWidth = initialCanvasWidth;
                        minCanvasHeight = initialCanvasHeight;
                    }
                }

                Utils.extend(canvas, {
                    minWidth: minCanvasWidth,
                    minHeight: minCanvasHeight,
                    maxWidth: Infinity,
                    maxHeight: Infinity
                });
            }

            if (position) {
                if (strict) {
                    if (cropped) {
                        canvas.minLeft = min(cropBox.left, (cropBox.left + cropBox.width) - canvas.width);
                        canvas.minTop = min(cropBox.top, (cropBox.top + cropBox.height) - canvas.height);
                        canvas.maxLeft = cropBox.left;
                        canvas.maxTop = cropBox.top;
                    } else {
                        canvas.minLeft = min(0, containerWidth - canvas.width);
                        canvas.minTop = min(0, containerHeight - canvas.height);
                        canvas.maxLeft = max(0, containerWidth - canvas.width);
                        canvas.maxTop = max(0, containerHeight - canvas.height);
                    }
                } else {
                    canvas.minLeft = -canvas.width;
                    canvas.minTop = -canvas.height;
                    canvas.maxLeft = containerWidth;
                    canvas.maxTop = containerHeight;
                }
            }
        },
        
        
        renderCanvas: function (changed) {
            var options = this.options,
                    canvas = this.canvas,
                    image = this.image,
                    aspectRatio,
                    rotated;

            if (this.rotated) {
                this.rotated = false;
                rotated = getRotatedSizes({
                    width: image.width,
                    height: image.height,
                    degree: image.rotate
                });

                aspectRatio = rotated.width / rotated.height;

                if (aspectRatio !== canvas.aspectRatio) {
                    canvas.left -= (rotated.width - canvas.width) / 2;
                    canvas.top -= (rotated.height - canvas.height) / 2;
                    canvas.width = rotated.width;
                    canvas.height = rotated.height;
                    canvas.aspectRatio = aspectRatio;
                    this.limitCanvas(true, false);
                }
            }

            if (canvas.width > canvas.maxWidth || canvas.width < canvas.minWidth) {
                canvas.left = canvas.oldLeft;
            }

            if (canvas.height > canvas.maxHeight || canvas.height < canvas.minHeight) {
                canvas.top = canvas.oldTop;
            }

            canvas.width = min(max(canvas.width, canvas.minWidth), canvas.maxWidth);
            canvas.height = min(max(canvas.height, canvas.minHeight), canvas.maxHeight);

            this.limitCanvas(false, true);

            canvas.oldLeft = canvas.left = min(max(canvas.left, canvas.minLeft), canvas.maxLeft);
            canvas.oldTop = canvas.top = min(max(canvas.top, canvas.minTop), canvas.maxTop);

            this.$canvas.css({
                width: canvas.width,
                height: canvas.height,
                left: canvas.left,
                top: canvas.top
            });

            this.renderImage();

            if (this.cropped && options.strict) {
                this.limitCropBox(true, true);
            }

            if (changed) {
                this.output();
            }
        },
        
        
        renderImage: function () {
            var canvas = this.canvas,
                    image = this.image,
                    reversed;

            if (image.rotate) {
                reversed = getRotatedSizes({
                    width: canvas.width,
                    height: canvas.height,
                    degree: image.rotate,
                    aspectRatio: image.aspectRatio
                }, true);
            }

            Utils.extend(image, reversed ? {
                width: reversed.width,
                height: reversed.height,
                left: (canvas.width - reversed.width) / 2,
                top: (canvas.height - reversed.height) / 2
            } : {
                width: canvas.width,
                height: canvas.height,
                left: 0,
                top: 0
            });
            
            Style.setStyles(this.$clone, {
                width: image.width,
                height: image.height,
                marginLeft: image.left,
                marginTop: image.top,
                transform: getRotateValue(image.rotate)
            });
        },
        
        
        initCropBox: function () {
            var options = this.options,
                    canvas = this.canvas,
                    aspectRatio = options.aspectRatio,
                    autoCropArea = num(options.autoCropArea) || 0.8,
                    cropBox = {
                        width: canvas.width,
                        height: canvas.height
                    };

            if (aspectRatio) {
                if (canvas.height * aspectRatio > canvas.width) {
                    cropBox.height = cropBox.width / aspectRatio;
                } else {
                    cropBox.width = cropBox.height * aspectRatio;
                }
            }

            this.cropBox = cropBox;
            this.limitCropBox(true, true);
            cropBox.width = min(max(cropBox.width, cropBox.minWidth), cropBox.maxWidth);
            cropBox.height = min(max(cropBox.height, cropBox.minHeight), cropBox.maxHeight);
            cropBox.width = max(cropBox.minWidth, cropBox.width * autoCropArea);
            cropBox.height = max(cropBox.minHeight, cropBox.height * autoCropArea);
            cropBox.oldLeft = cropBox.left = canvas.left + (canvas.width - cropBox.width) / 2;
            cropBox.oldTop = cropBox.top = canvas.top + (canvas.height - cropBox.height) / 2;

            this.initialCropBox = Utils.extend({}, cropBox);
        },
        
        
        limitCropBox: function (size, position) {
            var options = this.options,
                    strict = options.strict,
                    container = this.container,
                    containerWidth = container.width,
                    containerHeight = container.height,
                    canvas = this.canvas,
                    cropBox = this.cropBox,
                    aspectRatio = options.aspectRatio,
                    minCropBoxWidth,
                    minCropBoxHeight;

            if (size) {
                minCropBoxWidth = num(options.minCropBoxWidth) || 0;
                minCropBoxHeight = num(options.minCropBoxHeight) || 0;

                cropBox.minWidth = min(containerWidth, minCropBoxWidth);
                cropBox.minHeight = min(containerHeight, minCropBoxHeight);
                cropBox.maxWidth = min(containerWidth, strict ? canvas.width : containerWidth);
                cropBox.maxHeight = min(containerHeight, strict ? canvas.height : containerHeight);

                if (aspectRatio) {
                    if (cropBox.maxHeight * aspectRatio > cropBox.maxWidth) {
                        cropBox.minHeight = cropBox.minWidth / aspectRatio;
                        cropBox.maxHeight = cropBox.maxWidth / aspectRatio;
                    } else {
                        cropBox.minWidth = cropBox.minHeight * aspectRatio;
                        cropBox.maxWidth = cropBox.maxHeight * aspectRatio;
                    }
                }
                cropBox.minWidth = min(cropBox.maxWidth, cropBox.minWidth);
                cropBox.minHeight = min(cropBox.maxHeight, cropBox.minHeight);
            }

            if (position) {
                if (strict) {
                    cropBox.minLeft = max(0, canvas.left);
                    cropBox.minTop = max(0, canvas.top);
                    cropBox.maxLeft = min(containerWidth, canvas.left + canvas.width) - cropBox.width;
                    cropBox.maxTop = min(containerHeight, canvas.top + canvas.height) - cropBox.height;
                } else {
                    cropBox.minLeft = 0;
                    cropBox.minTop = 0;
                    cropBox.maxLeft = containerWidth - cropBox.width;
                    cropBox.maxTop = containerHeight - cropBox.height;
                }
            }
        },
        
        
        renderCropBox: function () {
            var options = this.options,
                    container = this.container,
                    containerWidth = container.width,
                    containerHeight = container.height,
                    cropBox = this.cropBox;

            if (cropBox.width > cropBox.maxWidth || cropBox.width < cropBox.minWidth) {
                cropBox.left = cropBox.oldLeft;
            }

            if (cropBox.height > cropBox.maxHeight || cropBox.height < cropBox.minHeight) {
                cropBox.top = cropBox.oldTop;
            }

            cropBox.width = min(max(cropBox.width, cropBox.minWidth), cropBox.maxWidth);
            cropBox.height = min(max(cropBox.height, cropBox.minHeight), cropBox.maxHeight);

            this.limitCropBox(false, true);

            cropBox.oldLeft = cropBox.left = min(max(cropBox.left, cropBox.minLeft), cropBox.maxLeft);
            cropBox.oldTop = cropBox.top = min(max(cropBox.top, cropBox.minTop), cropBox.maxTop);

            if (options.movable && options.cropBoxMovable) {
                this.$face.acmsData('drag', (cropBox.width === containerWidth && cropBox.height === containerHeight) ? 'move' : 'all');
            }
            Style.setStyles(this.$cropBox, {
                width: cropBox.width,
                height: cropBox.height,
                left: cropBox.left,
                top: cropBox.top
            });

            if (this.cropped && options.strict) {
                this.limitCanvas(true, true);
            }

            if (!this.disabled) {
                this.output();
            }
        },
        
        
        output: function () {
            var options = this.options,
                element = this.element;

            this.preview();

            if (options.crop) {
                options.crop.call(element, this.getData());
            }
            
            this.triggerEvent('change');
        },
        
        
        initPreview: function () {
            var self = this,
                o = self.options,
                url = this.url, img, j, preview;
            this.$preview = Selectors.qa(o.preview);
            img = document.createElement('img');
            img.setAttribute('src', url);
            this.$viewBox.appendChild(img);
            for(j = 0; j < this.$preview.length; j++) {
                preview = this.$preview[j];
                preview.acmsData(CropBox.BOX_PREVIEW, {
                    width: preview.outerWidth,
                    height: preview.outerHeight,
                    original: preview.innerHTML
                });
                preview.innerHTML = '<img src="' + url + '" style="display:block;width:100%;min-width:0!important;min-height:0!important;max-width:none!important;max-height:none!important;image-orientation: 0deg!important">';
            }
        },
        
        resetPreview: function () {
            var previews = this.$preview, preview, j;
            for(j = 0; j < previews.length; j++) {
                preview = previews[j];
                preview.innerHTML = preview.acmsData(CropBox.BOX_PREVIEW).original;
                preview.acmsData(CropBox.BOX_PREVIEW, '');
                
            }
        },
        
        
        preview: function () {
            var image = this.image,
                    canvas = this.canvas,
                    cropBox = this.cropBox,
                    width = image.width,
                    height = image.height,
                    left = cropBox.left - canvas.left - image.left,
                    top = cropBox.top - canvas.top - image.top,
                    rotate = image.rotate,
                    previews = this.$preview, j, preview;

            if (!this.cropped || this.disabled) {
                return;
            }
            var img = Selectors.q('img', this.$viewBox);
            Style.setStyles(img, {
                width: width + 'px',
                height: height + 'px',
                marginLeft: -left + 'px',
                marginTop: -top + 'px',
                transform: getRotateValue(rotate)
            });
            for(j = 0; j < previews.length; j++) {
                preview = previews[j];
                var data = preview.acmsData(CropBox.BOX_PREVIEW),
                    ratio = data.width / cropBox.width,
                    newWidth = data.width,
                    newHeight = cropBox.height * ratio;
                if (newHeight > data.height) {
                    ratio = data.height / cropBox.height;
                    newWidth = cropBox.width * ratio;
                    newHeight = data.height;
                }

                preview.outerWidth = newWidth;
                preview.outerHeight = newHeight;
                var img = Selectors.q('img');
                Style.setStyles(img, {
                    width: width * ratio + 'px',
                    height: height * ratio + 'px',
                    marginLeft: -left * ratio + 'px',
                    marginTop: -top * ratio + 'px',
                    transform: getRotateValue(rotate)
                });
            }
        },
        
        addListeners: function () {
            var self = this,
                options = self.options,
                element = self.element,
                $cropbox = self.$cropbox;
            
            if(typeof options.dragstart === CropBox.TYPE_FUNCTION) {
                AcmsEvent.add(element, CropBox.EVENT_DRAG_START, options.dragstart);
            }
            
            if(typeof options.dragmove === CropBox.TYPE_FUNCTION) {
                AcmsEvent.add(element, CropBox.EVENT_DRAG_MOVE, options.dragmove);
            }
            
            if(typeof options.dragend === CropBox.TYPE_FUNCTION) {
                AcmsEvent.add(element, CropBox.EVENT_DRAG_END, options.dragend);
            }
            
            if(typeof options.zoomin === CropBox.TYPE_FUNCTION) {
                AcmsEvent.add(element, CropBox.EVENT_ZOOMIN, options.zoomin);
            }
            
            if(typeof options.zoomout === CropBox.TYPE_FUNCTION) {
                AcmsEvent.add(element, CropBox.EVENT_ZOOMOUT, options.zoomout);
            }
            
            if(typeof options.change === CropBox.TYPE_FUNCTION) {
                AcmsEvent.add(element, CropBox.EVENT_CHANGE, options.change);
            }
            
            AcmsEvent.addMultiple($cropbox, CropBox.EVENT_MOUSE_DOWN, self.dragstart.bind(self));

            if (options.zoomable && options.mouseWheelZoom) {
                AcmsEvent.addMultiple($cropbox, CropBox.EVENT_WHEEL, self.wheel.bind(self));
            }

            if (options.doubleClickToggle) {
                AcmsEvent.add($cropbox, CropBox.EVENT_DBLCLICK, self.dblclick.bind(self));
            }
            AcmsEvent.addMultiple(document, CropBox.EVENT_MOUSE_MOVE, self._dragmove = self.dragmove.bind(self));
            AcmsEvent.addMultiple(document, CropBox.EVENT_MOUSE_UP, self._dragend = self.dragend.bind(self));
            
            if (options.responsive) {
                AcmsEvent.add(window, CropBox.EVENT_RESIZE, self._resize = self.resize.bind(self));
            }
        },
        
        /**
         * Entfernt alle Event Listener
         * 
         * 
         */
        removeListeners: function () {
            var options = this.options,
                    element = this.element,
                    $cropbox = this.$cropbox;
            
            if (typeof options.dragstart === CropBox.TYPE_FUNCTION) {
                AcmsEvent.remove(element, CropBox.EVENT_DRAG_START, options.dragstart);
            }
            
            if (typeof options.dragmove === CropBox.TYPE_FUNCTION) {
                AcmsEvent.remove(element, CropBox.EVENT_DRAG_MOVE, options.dragmove);
            }
            
            if (typeof options.dragend === CropBox.TYPE_FUNCTION) {
                AcmsEvent.remove(element, CropBox.EVENT_DRAG_END, options.dragend);
            }
            
            if (typeof options.zoomin === CropBox.TYPE_FUNCTION) {
                AcmsEvent.remove(element, CropBox.EVENT_ZOOMIN, options.zoomin);
            }

            if (typeof options.zoomout === CropBox.TYPE_FUNCTION) {
                AcmsEvent.remove(element, CropBox.EVENT_ZOOMOUT, options.zoomout);
            }
            
            if (typeof options.change === CropBox.TYPE_FUNCTION) {
                AcmsEvent.remove(element, CropBox.EVENT_CHANGE, options.change);
            }
            
            AcmsEvent.removeMultiple($cropbox, CropBox.EVENT_MOUSE_DOWN, this.dragstart);
            
            if (options.zoomable && options.mouseWheelZoom) {
                AcmsEvent.removeMultiple($cropbox, CropBox.EVENT_WHEEL, this.wheel);
            }

            if (options.doubleClickToggle) {
                AcmsEvent.remove($cropbox, CropBox.EVENT_DBLCLICK, this.dblclick);
            }
            
            AcmsEvent.removeMultiple(document, CropBox.EVENT_MOUSE_MOVE, this._dragmove);
            AcmsEvent.removeMultiple(document, CropBox.EVENT_MOUSE_UP, this._dragend);

            if (options.responsive) {
                AcmsEvent.remove(window, CropBox.EVENT_RESIZE, this._resize);
            }
        },
        
        /** resizing **/
        resize: function () {
            var $container = this.$container,
                    container = this.container,
                    canvasData,
                    cropBoxData,
                    ratio;

            if (this.disabled || !container) {
                return;
            }

            ratio = $container.outerWidth / container.width;

            if (ratio !== 1 || $container.outerHeight !== container.height) {
                canvasData = this.getCanvasData();
                cropBoxData = this.getCropBoxData();
                
                this.render();
                var cvd, prop;
                for(prop in canvasData) {
                    if(canvasData.hasOwnProperty(prop)) {
                        cvd[prop] = canvasData[prop] * ratio;
                    }
                }
                this.setCanvasData(cvd);
                var cbd, prp;
                for(prp in cropBoxData) {
                    if(cropBoxData.hasOwnProperty(prp)) {
                        cbd[prp] = cropBoxData[prp] * ratio;
                    }
                }
                this.setCropBoxData(cbd);
            }
        },
        
        dblclick: function () {
            if (this.disabled) {
                return;
            }
            if (Classes.hasClass(this.$dragBox, CLASS_CROP)) {
                this.setDragMode('move');
            } else {
                this.setDragMode('crop');
            }
        },
        
        wheel: function (event) {
            var e = event.originalEvent,
                    delta = 1;

            if (this.disabled) {
                return;
            }

            event.preventDefault();

            if (e.deltaY) {
                delta = e.deltaY > 0 ? 1 : -1;
            } else if (e.wheelDelta) {
                delta = -e.wheelDelta / 120;
            } else if (e.detail) {
                delta = e.detail > 0 ? 1 : -1;
            }

            this.zoom(-delta * 0.1);
        },
        
        dragstart: function (event) {
            var options = this.options,
                    originalEvent = event.originalEvent,
                    touches = originalEvent && originalEvent.touches,
                    e = event,
                    dragType,
                    dragStartEvent, dragData,
                    touchesLength;

            if (this.disabled) {
                return;
            }

            if (touches) {
                touchesLength = touches.length;

                if (touchesLength > 1) {
                    if (options.zoomable && options.touchDragZoom && touchesLength === 2) {
                        e = touches[1];
                        this.startX2 = e.pageX;
                        this.startY2 = e.pageY;
                        dragType = 'zoom';
                    } else {
                        return;
                    }
                }

                e = touches[0];
            }

            dragType = dragType || e.target.acmsData('drag');

            if (REGEXP_DRAG_TYPES.test(dragType)) {
                event.preventDefault();
                dragData = {
                    originalEvent: originalEvent,
                    dragType: dragType
                };
                dragStartEvent = this.triggerEvent('dragstart', dragData);
                
                if (dragStartEvent.defaultPrevented) {
                    return;
                }

                this.dragType = dragType;
                this.cropping = false;
                this.startX = e.pageX;
                this.startY = e.pageY;

                if (dragType === 'crop') {
                    this.cropping = true;
                    Classes.addClass(this.$dragBox, CLASS_MODAL);
                }
            }
        },
        
        dragmove: function (event) {
            var options = this.options,
                    originalEvent = event.originalEvent,
                    touches = originalEvent && originalEvent.touches,
                    e = event,
                    dragType = this.dragType,
                    dragMoveEvent, dragData,
                    touchesLength;

            if (this.disabled) {
                return;
            }

            if (touches) {
                touchesLength = touches.length;

                if (touchesLength > 1) {
                    if (options.zoomable && options.touchDragZoom && touchesLength === 2) {
                        e = touches[1];
                        this.endX2 = e.pageX;
                        this.endY2 = e.pageY;
                    } else {
                        return;
                    }
                }

                e = touches[0];
            }

            if (dragType) {
                event.preventDefault();
                dragData = {
                    originalEvent: originalEvent,
                    dragType: dragType
                };
                dragMoveEvent = this.triggerEvent('dragmove', dragData);
                
                if (dragMoveEvent.defaultPrevented) {
                    return;
                }
                this.endX = e.pageX;
                this.endY = e.pageY;
                this.change(e.shiftKey);
            }
        },
        
        dragend: function (event) {
            var dragType = this.dragType,
                    dragEndEvent, dragData;

            if (this.disabled) {
                return;
            }

            if (dragType) {
                event.preventDefault();
                dragData = {
                    originalEvent: event.originalEvent,
                    dragType: dragType
                }
                dragEndEvent = this.triggerEvent('dragend', dragData);
                
                if (dragEndEvent.defaultPrevented) {
                    return;
                }
                if (this.cropping) {
                    this.cropping = false;
                    Classes.toggleClass(this.$dragBox, CLASS_MODAL, this.cropped && this.options.modal);
                }

                this.dragType = '';
            }
        },
        
        /** Cropping **/
        crop: function () {
            if (!this.built || this.disabled) {
                return;
            }

            if (!this.cropped) {
                this.cropped = true;
                this.limitCropBox(true, true);

                if (this.options.modal) {
                    Classes.addClass(this.$dragBox, CLASS_MODAL);
                }

                Classes.removeClass(this.$cropBox, CLASS_HIDDEN);
            }

            this.setCropBoxData(this.initialCropBox);
        },
        
        reset: function () {
            if (!this.built || this.disabled) {
                return;
            }

            this.image = Utils.extend({}, this.initialImage);
            this.canvas = Utils.extend({}, this.initialCanvas);
            this.cropBox = Utils.extend({}, this.initialCropBox);

            this.renderCanvas();

            if (this.cropped) {
                this.renderCropBox();
            }
        },
        
        clear: function () {
            if (!this.cropped || this.disabled) {
                return;
            }

            Utils.extend(this.cropBox, {
                left: 0,
                top: 0,
                width: 0,
                height: 0
            });

            this.cropped = false;
            this.renderCropBox();

            this.limitCanvas();
            this.renderCanvas();

            Classes.removeClass(this.$dragBox, CLASS_MODAL);
            Classes.addClass(this.$cropBox, CLASS_HIDDEN);
        },
        
        destroy: function () {
            var element = this.element;

            if (this.ready) {
                if (this.isImg) {
                    element.setAttribute('src', this.originalUrl);
                }

                this.unbuild();
                Classes.removeClass(element, CLASS_HIDDEN);
            } else if (this.$clone) {
                this.$clone.parentNode.removeChild(this.$clone);
            }

            element.acmsData('cropbox', '');
        },
        
        replace: function (url) {
            if (!this.disabled && url) {
                if (this.isImg) {
                    this.element.setAttribute('src', url);
                }
                this.options.data = null;
                this.load(url);
            }
        },
        
        
        enable: function () {
            if (this.built) {
                this.disabled = false;
                Classes.removeClass(this.$cropbox, CLASS_DISABLED);
            }
        },
        
        
        disable: function () {
            if (this.built) {
                this.disabled = true;
                Classes.addClass(this.$cropbox, CLASS_DISABLED);
            }
        },
        
        move: function (offsetX, offsetY) {
            var canvas = this.canvas;

            if (this.built && !this.disabled && this.options.movable && isNumber(offsetX) && isNumber(offsetY)) {
                canvas.left += offsetX;
                canvas.top += offsetY;
                this.renderCanvas(true);
            }
        },
        
        
        zoom: function (delta) {
            var canvas = this.canvas,
                    zoomEvent,
                    width,
                    height;

            delta = num(delta);

            if (delta && this.built && !this.disabled && this.options.zoomable) {
                zoomEvent = delta > 0 ? 'zoomin' : 'zoomout';
                var evt = this.triggerEvent(zoomEvent)

                if (evt.defaultPrevented) {
                    return;
                }

                delta = delta <= -1 ? 1 / (1 - delta) : delta <= 1 ? (1 + delta) : delta;
                width = canvas.width * delta;
                height = canvas.height * delta;
                canvas.left -= (width - canvas.width) / 2;
                canvas.top -= (height - canvas.height) / 2;
                canvas.width = width;
                canvas.height = height;
                this.renderCanvas(true);
                this.setDragMode('move');
            }
        },
        
        
        rotate: function (degree) {
            var image = this.image;

            degree = num(degree);

            if (degree && this.built && !this.disabled && this.options.rotatable) {
                image.rotate = (image.rotate + degree) % 360;
                this.rotated = true;
                this.renderCanvas(true);
            }
        },
        
        
        getData: function (rounded) {
            var cropBox = this.cropBox,
                    canvas = this.canvas,
                    image = this.image,
                    ratio,
                    data, prop;

            if (this.built && this.cropped) {
                data = {
                    x: cropBox.left - canvas.left,
                    y: cropBox.top - canvas.top,
                    width: cropBox.width,
                    height: cropBox.height
                };

                ratio = image.width / image.naturalWidth;
                for(prop in data) {
                    if(data.hasOwnProperty(prop)) {
                        var n = data[prop];
                        n = n / ratio;
                        data[prop] = rounded ? Math.round(n) : n;
                    }
                }
            } else {
                data = {
                    x: 0,
                    y: 0,
                    width: 0,
                    height: 0
                };
            }
            data.rotate = this.ready ? image.rotate : 0;

            return data;
        },
        
        
        setData: function (data) {
            var image = this.image,
                    canvas = this.canvas,
                    cropBoxData = {},
                    ratio;

            if (this.built && !this.disabled) {
                if (isNumber(data.rotate) && data.rotate !== image.rotate && this.options.rotatable) {
                    image.rotate = data.rotate;
                    this.rotated = true;
                    this.renderCanvas(true);
                }

                ratio = image.width / image.naturalWidth;

                if (isNumber(data.x)) {
                    cropBoxData.left = data.x * ratio + canvas.left;
                }

                if (isNumber(data.y)) {
                    cropBoxData.top = data.y * ratio + canvas.top;
                }

                if (isNumber(data.width)) {
                    cropBoxData.width = data.width * ratio;
                }

                if (isNumber(data.height)) {
                    cropBoxData.height = data.height * ratio;
                }

                this.setCropBoxData(cropBoxData);
            }
        },
        
        
        getContainerData: function () {
            return this.built ? this.container : {};
        },
        
        
        getImageData: function () {
            return this.ready ? this.image : {};
        },
        
        
        getCanvasData: function () {
            var canvas = this.canvas,
                    data;

            if (this.built) {
                data = {
                    left: canvas.left,
                    top: canvas.top,
                    width: canvas.width,
                    height: canvas.height
                };
            }

            return data || {};
        },
        
        
        setCanvasData: function (data) {
            var canvas = this.canvas,
                    aspectRatio = canvas.aspectRatio;

            if (this.built && !this.disabled) {
                if (isNumber(data.left)) {
                    canvas.left = data.left;
                }

                if (isNumber(data.top)) {
                    canvas.top = data.top;
                }

                if (isNumber(data.width)) {
                    canvas.width = data.width;
                    canvas.height = data.width / aspectRatio;
                } else if (isNumber(data.height)) {
                    canvas.height = data.height;
                    canvas.width = data.height * aspectRatio;
                }

                this.renderCanvas(true);
            }
        },
        
        
        getCropBoxData: function () {
            var cropBox = this.cropBox,
                    data;

            if (this.built && this.cropped) {
                data = {
                    left: cropBox.left,
                    top: cropBox.top,
                    width: cropBox.width,
                    height: cropBox.height
                };
            }

            return data || {};
        },
        
        
        setCropBoxData: function (data) {
            var cropBox = this.cropBox,
                    aspectRatio = this.options.aspectRatio;

            if (this.built && this.cropped && !this.disabled) {

                if (isNumber(data.left)) {
                    cropBox.left = data.left;
                }

                if (isNumber(data.top)) {
                    cropBox.top = data.top;
                }

                if (isNumber(data.width)) {
                    cropBox.width = data.width;
                }

                if (isNumber(data.height)) {
                    cropBox.height = data.height;
                }

                if (aspectRatio) {
                    if (isNumber(data.width)) {
                        cropBox.height = cropBox.width / aspectRatio;
                    } else if (isNumber(data.height)) {
                        cropBox.width = cropBox.height * aspectRatio;
                    }
                }

                this.renderCropBox();
            }
        },
        
        
        getCroppedCanvas: function (options) {
            var originalWidth,
                    originalHeight,
                    canvasWidth,
                    canvasHeight,
                    scaledWidth,
                    scaledHeight,
                    scaledRatio,
                    aspectRatio,
                    canvas,
                    context,
                    data;

            if (!this.built || !this.cropped || !SUPPORT_CANVAS) {
                return;
            }

            if (null === options || !(typeof options === 'object')) {
                options = {};
            }
            data = this.getData();
            originalWidth = data.width;
            originalHeight = data.height;
            aspectRatio = originalWidth / originalHeight;

            scaledWidth = options.width;
            scaledHeight = options.height;

            if (scaledWidth) {
                scaledHeight = scaledWidth / aspectRatio;
                scaledRatio = scaledWidth / originalWidth;
            } else if (scaledHeight) {
                scaledWidth = scaledHeight * aspectRatio;
                scaledRatio = scaledHeight / originalHeight;
            }


            canvasWidth = scaledWidth || originalWidth;
            canvasHeight = scaledHeight || originalHeight;

            canvas = document.createElement('canvas');
            canvas.width = canvasWidth;
            canvas.height = canvasHeight;
            context = canvas.getContext('2d');

            if (options.fillColor) {
                context.fillStyle = options.fillColor;
                context.fillRect(0, 0, canvasWidth, canvasHeight);
            }

            context.drawImage.apply(context, (function () {
                var source = getSourceCanvas(this.$clone, this.image),
                        sourceWidth = source.width,
                        sourceHeight = source.height,
                        args = [source],
                        srcX = data.x,
                        srcY = data.y,
                        srcWidth,
                        srcHeight,
                        dstX,
                        dstY,
                        dstWidth,
                        dstHeight;

                if (srcX <= -originalWidth || srcX > sourceWidth) {
                    srcX = srcWidth = dstX = dstWidth = 0;
                } else if (srcX <= 0) {
                    dstX = -srcX;
                    srcX = 0;
                    srcWidth = dstWidth = min(sourceWidth, originalWidth + srcX);
                } else if (srcX <= sourceWidth) {
                    dstX = 0;
                    srcWidth = dstWidth = min(originalWidth, sourceWidth - srcX);
                }

                if (srcWidth <= 0 || srcY <= -originalHeight || srcY > sourceHeight) {
                    srcY = srcHeight = dstY = dstHeight = 0;
                } else if (srcY <= 0) {
                    dstY = -srcY;
                    srcY = 0;
                    srcHeight = dstHeight = min(sourceHeight, originalHeight + srcY);
                } else if (srcY <= sourceHeight) {
                    dstY = 0;
                    srcHeight = dstHeight = min(originalHeight, sourceHeight - srcY);
                }

                args.push(srcX, srcY, srcWidth, srcHeight);

                if (scaledRatio) {
                    dstX *= scaledRatio;
                    dstY *= scaledRatio;
                    dstWidth *= scaledRatio;
                    dstHeight *= scaledRatio;
                }
                if (dstWidth > 0 && dstHeight > 0) {
                    args.push(dstX, dstY, dstWidth, dstHeight);
                }

                return args;
            }).call(this));

            return canvas;
        },
        
        setAspectRatio: function (aspectRatio) {
            var options = this.options;

            if (!this.disabled && !isUndefined(aspectRatio)) {
                options.aspectRatio = num(aspectRatio) || NaN;

                if (this.built) {
                    this.initCropBox();

                    if (this.cropped) {
                        this.renderCropBox();
                    }
                }
            }
        },
        
        
        setDragMode: function (mode) {
            var options = this.options,
                    croppable,
                    movable;

            if (this.ready && !this.disabled) {
                croppable = options.dragCrop && mode === 'crop';
                movable = options.movable && mode === 'move';
                mode = (croppable || movable) ? mode : 'none';

                this.$dragBox.acmsData('drag', mode)
                Classes.toggleClass(this.$dragBox, CLASS_CROP, croppable);
                Classes.toggleClass(this.$dragBox, CLASS_MOVE, movable);

                if (!options.cropBoxMovable) {
                    this.$face.acmsData('drag', mode)
                    Classes.toggleClass(this.$face, CLASS_CROP, croppable);
                    Classes.toggleClass(this.$face, CLASS_MOVE, movable);
                }
            }
        },
        
        change: function (shiftKey) {
            var dragType = this.dragType,
                    options = this.options,
                    canvas = this.canvas,
                    container = this.container,
                    cropBox = this.cropBox,
                    width = cropBox.width,
                    height = cropBox.height,
                    left = cropBox.left,
                    top = cropBox.top,
                    right = left + width,
                    bottom = top + height,
                    minLeft = 0,
                    minTop = 0,
                    maxWidth = container.width,
                    maxHeight = container.height,
                    renderable = true,
                    aspectRatio = options.aspectRatio,
                    range = {
                        x: this.endX - this.startX,
                        y: this.endY - this.startY
                    },
            offset;
            if (!aspectRatio && shiftKey) {
                aspectRatio = width && height ? width / height : 1;
            }

            if (options.strict) {
                minLeft = cropBox.minLeft;
                minTop = cropBox.minTop;
                maxWidth = minLeft + min(container.width, canvas.width);
                maxHeight = minTop + min(container.height, canvas.height);
            }

            if (aspectRatio) {
                range.X = range.y * aspectRatio;
                range.Y = range.x / aspectRatio;
            }

            switch (dragType) {
                case 'all':
                    left += range.x;
                    top += range.y;
                    break;
                case 'e':
                    if (range.x >= 0 && (right >= maxWidth || aspectRatio && (top <= minTop || bottom >= maxHeight))) {
                        renderable = false;
                        break;
                    }

                    width += range.x;

                    if (aspectRatio) {
                        height = width / aspectRatio;
                        top -= range.Y / 2;
                    }

                    if (width < 0) {
                        dragType = 'w';
                        width = 0;
                    }

                    break;

                case 'n':
                    if (range.y <= 0 && (top <= minTop || aspectRatio && (left <= minLeft || right >= maxWidth))) {
                        renderable = false;
                        break;
                    }

                    height -= range.y;
                    top += range.y;

                    if (aspectRatio) {
                        width = height * aspectRatio;
                        left += range.X / 2;
                    }

                    if (height < 0) {
                        dragType = 's';
                        height = 0;
                    }

                    break;

                case 'w':
                    if (range.x <= 0 && (left <= minLeft || aspectRatio && (top <= minTop || bottom >= maxHeight))) {
                        renderable = false;
                        break;
                    }

                    width -= range.x;
                    left += range.x;

                    if (aspectRatio) {
                        height = width / aspectRatio;
                        top += range.Y / 2;
                    }

                    if (width < 0) {
                        dragType = 'e';
                        width = 0;
                    }

                    break;

                case 's':
                    if (range.y >= 0 && (bottom >= maxHeight || aspectRatio && (left <= minLeft || right >= maxWidth))) {
                        renderable = false;
                        break;
                    }

                    height += range.y;

                    if (aspectRatio) {
                        width = height * aspectRatio;
                        left -= range.X / 2;
                    }

                    if (height < 0) {
                        dragType = 'n';
                        height = 0;
                    }

                    break;

                case 'ne':
                    if (aspectRatio) {
                        if (range.y <= 0 && (top <= minTop || right >= maxWidth)) {
                            renderable = false;
                            break;
                        }

                        height -= range.y;
                        top += range.y;
                        width = height * aspectRatio;
                    } else {
                        if (range.x >= 0) {
                            if (right < maxWidth) {
                                width += range.x;
                            } else if (range.y <= 0 && top <= minTop) {
                                renderable = false;
                            }
                        } else {
                            width += range.x;
                        }

                        if (range.y <= 0) {
                            if (top > minTop) {
                                height -= range.y;
                                top += range.y;
                            }
                        } else {
                            height -= range.y;
                            top += range.y;
                        }
                    }

                    if (width < 0 && height < 0) {
                        dragType = 'sw';
                        height = 0;
                        width = 0;
                    } else if (width < 0) {
                        dragType = 'nw';
                        width = 0;
                    } else if (height < 0) {
                        dragType = 'se';
                        height = 0;
                    }

                    break;

                case 'nw':
                    if (aspectRatio) {
                        if (range.y <= 0 && (top <= minTop || left <= minLeft)) {
                            renderable = false;
                            break;
                        }

                        height -= range.y;
                        top += range.y;
                        width = height * aspectRatio;
                        left += range.X;
                    } else {
                        if (range.x <= 0) {
                            if (left > minLeft) {
                                width -= range.x;
                                left += range.x;
                            } else if (range.y <= 0 && top <= minTop) {
                                renderable = false;
                            }
                        } else {
                            width -= range.x;
                            left += range.x;
                        }

                        if (range.y <= 0) {
                            if (top > minTop) {
                                height -= range.y;
                                top += range.y;
                            }
                        } else {
                            height -= range.y;
                            top += range.y;
                        }
                    }

                    if (width < 0 && height < 0) {
                        dragType = 'se';
                        height = 0;
                        width = 0;
                    } else if (width < 0) {
                        dragType = 'ne';
                        width = 0;
                    } else if (height < 0) {
                        dragType = 'sw';
                        height = 0;
                    }

                    break;

                case 'sw':
                    if (aspectRatio) {
                        if (range.x <= 0 && (left <= minLeft || bottom >= maxHeight)) {
                            renderable = false;
                            break;
                        }

                        width -= range.x;
                        left += range.x;
                        height = width / aspectRatio;
                    } else {
                        if (range.x <= 0) {
                            if (left > minLeft) {
                                width -= range.x;
                                left += range.x;
                            } else if (range.y >= 0 && bottom >= maxHeight) {
                                renderable = false;
                            }
                        } else {
                            width -= range.x;
                            left += range.x;
                        }

                        if (range.y >= 0) {
                            if (bottom < maxHeight) {
                                height += range.y;
                            }
                        } else {
                            height += range.y;
                        }
                    }

                    if (width < 0 && height < 0) {
                        dragType = 'ne';
                        height = 0;
                        width = 0;
                    } else if (width < 0) {
                        dragType = 'se';
                        width = 0;
                    } else if (height < 0) {
                        dragType = 'nw';
                        height = 0;
                    }

                    break;

                case 'se':
                    if (aspectRatio) {
                        if (range.x >= 0 && (right >= maxWidth || bottom >= maxHeight)) {
                            renderable = false;
                            break;
                        }

                        width += range.x;
                        height = width / aspectRatio;
                    } else {
                        if (range.x >= 0) {
                            if (right < maxWidth) {
                                width += range.x;
                            } else if (range.y >= 0 && bottom >= maxHeight) {
                                renderable = false;
                            }
                        } else {
                            width += range.x;
                        }

                        if (range.y >= 0) {
                            if (bottom < maxHeight) {
                                height += range.y;
                            }
                        } else {
                            height += range.y;
                        }
                    }

                    if (width < 0 && height < 0) {
                        dragType = 'nw';
                        height = 0;
                        width = 0;
                    } else if (width < 0) {
                        dragType = 'sw';
                        width = 0;
                    } else if (height < 0) {
                        dragType = 'ne';
                        height = 0;
                    }

                    break;
                case 'move':
                    canvas.left += range.x;
                    canvas.top += range.y;
                    this.renderCanvas(true);
                    renderable = false;
                    break;
                case 'zoom':
                    this.zoom(function (x1, y1, x2, y2) {
                        var z1 = sqrt(x1 * x1 + y1 * y1),
                                z2 = sqrt(x2 * x2 + y2 * y2);

                        return (z2 - z1) / z1;
                    }(
                            abs(this.startX - this.startX2),
                            abs(this.startY - this.startY2),
                            abs(this.endX - this.endX2),
                            abs(this.endY - this.endY2)
                            ));

                    this.startX2 = this.endX2;
                    this.startY2 = this.endY2;
                    renderable = false;
                    break;
                case 'crop':
                    if (range.x && range.y) {
                        offset = this.$cropbox.offset();
                        left = this.startX - offset.left;
                        top = this.startY - offset.top;
                        width = cropBox.minWidth;
                        height = cropBox.minHeight;

                        if (range.x > 0) {
                            if (range.y > 0) {
                                dragType = 'se';
                            } else {
                                dragType = 'ne';
                                top -= height;
                            }
                        } else {
                            if (range.y > 0) {
                                dragType = 'sw';
                                left -= width;
                            } else {
                                dragType = 'nw';
                                left -= width;
                                top -= height;
                            }
                        }
                        if (!this.cropped) {
                            this.cropped = true;
                            this.$cropBox.removeClass(CLASS_HIDDEN);
                        }
                    }
                    break;
            }

            if (renderable) {
                cropBox.width = width;
                cropBox.height = height;
                cropBox.left = left;
                cropBox.top = top;
                this.dragType = dragType;

                this.renderCropBox();
            }
            this.startX = this.endX;
            this.startY = this.endY;
        },
        
        triggerEvent: function(name, data, element) {
            var self = this,
                ele = element || self.element,
                o = self.options,
                evtName = name + '.' + CropBox.NS,
                onName = 'on' + name.charAt(0).toUpperCase() + name.substr(1);
            var evt = AcmsEvent.createCustom(evtName, data);
            AcmsEvent.dispatch(ele, evt);
            if(typeof o[onName] === function() {
                o[onName](self, element);
            });
            
            return evt;
        }
    };

    CropBox.TEMPLATE = '<div class="cropbox-container">'
                        + '<div class="cropbox-canvas"></div>'
                        + '<div class="cropbox-drag-box"></div>'
                        + '<div class="cropbox-crop-box">'
                            + '<span class="cropbox-view-box"></span>'
                            + '<span class="cropbox-dashed dashed-h"></span>'
                            + '<span class="cropbox-dashed dashed-v"></span>'
                            + '<span class="cropbox-center"></span>'
                            + '<span class="cropbox-face"></span>'
                            + '<span class="cropbox-line line-e" data-drag="e"></span>'
                            + '<span class="cropbox-line line-n" data-drag="n"></span>'
                            + '<span class="cropbox-line line-w" data-drag="w"></span>'
                            + '<span class="cropbox-line line-s" data-drag="s"></span>'
                            + '<span class="cropbox-point point-e" data-drag="e"></span>'
                            + '<span class="cropbox-point point-n" data-drag="n"></span>'
                            + '<span class="cropbox-point point-w" data-drag="w"></span>'
                            + '<span class="cropbox-point point-s" data-drag="s"></span>'
                            + '<span class="cropbox-point point-ne" data-drag="ne"></span>'
                            + '<span class="cropbox-point point-nw" data-drag="nw"></span>'
                            + '<span class="cropbox-point point-sw" data-drag="sw"></span>'
                            + '<span class="cropbox-point point-se" data-drag="se"></span>'
                        + '</div>'
                      + '</div>';

    
    
    
    return CropBox;
});

