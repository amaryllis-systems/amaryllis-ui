/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */

/**
 * Image-Fit
 *
 * Hilfs-Tool um ein Bild Objekt in einen Frame einzupassen (<b>KEIN</b>
 * resizing des Bildes!).
 *
 * @param {Object} c
 * @param {Object} u
 *
 * @returns {ImageFit}
 */
define('ui/media/image/image-fit', [
    'tools/utils',
    'core/selectors',
    'core/classes',
    "tools/string/string-tool",
    'core/style',
    'events/event'
], function(Utils, StringTool, Selectors, Classes, Style, AcmsEvent) {

    "use strict";

    var FIT_DATA = "acms.ui.media.image.image-fit",
        NS = '.'+ FIT_DATA;

    var ImageFit = function(element, options) {
        this.options =
        this.imageContainer =
        this.frame =
        this.source =
        this.imageWidth =
        this.imageHeight =
        this.frameWidth =
        this.frameHeight =
        this.container =
        this.element = null;
        this.initialize(element, options);
    };

    /**
     * Komponenten Name
     *
     * @var {String}
     */
    ImageFit.MODULE = "Image Fit";

    /**
     * Standard-Optionen
     *
     * @var {Object}
     */
    ImageFit.DEFAULT_OPTIONS = {
        shadow: false,
        context: false,
        type: 'default',
        frameColor: 'default',
        format: 'hd',
        maxHeight: false
    };

    ImageFit.needInit = true;

    /**
     * Module-Initialisator
     *
     * @param {Object} element
     * @param {Object} options
     * @returns {ImageFit}
     */
    ImageFit.init = function(element, options) {
        var imf = new ImageFit(element, options);
        imf.element.acmsData(FIT_DATA, imf);

        return imf;
    }

    ImageFit.prototype = {
        constructor: ImageFit,

        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this.imageContainer = Selectors.closest(element, '.image-container');
            this.frame = Selectors.closest(element, '.frame');
            
            this.imageWidth = this.element.offsetWidth;
            this.imageHeight = this.element.offsetHeight;
            this.frameWidth = this.frame.clientWidth;
            this.frameHeight = this.frame.clientHeight;
            
            this.source = element.getAttribute('src');
            this._getFrameSizeFromFormat();
            this._buildContainer();
            this._applyOptions();
            this._element = element;
            
            this.frame.appendChild(this.container);
            element.parentNode.removeChild(element);
            this._fixImageContainer();
            this.listen();
        },

        update: function(options) {
            if(options && typeof options === 'object') {
                this.options = Utils.extend({}, this.options, options);
            }
            this._getFrameSizeFromFormat();
            this._buildContainer();
            this._applyOptions();
            this._fixImageContainer();
        },

        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {ImageFit.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return ImageFit.DEFAULT_OPTIONS;
        },
        /**
         * Bildet die Optionen
         *
         * <p>
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         * </p>
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function (options) {
            var o = Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
            if(o.context === '' || o.context === false || o.context === 'false' || parseInt(o.context) === 0) {
                o.context = false;
            } else if(typeof o.context === 'string' && o.context.toString().indexOf('#') === 0) {
                var context = Selectors.q(o.context);
                o.context = context && context.innerHTML || '';
            }
            var mh = parseInt(o.maxHeight);
            if(o.maxHeight === '' || o.maxHeight === false || o.maxHeight === 'false' || mh === 0) {
                o.maxHeight = false;
            } else if(!isNaN(mh) && mh > 0) {
                o.maxHeight = mh;
            } else {
                o.maxHeight = false;
            }
            return o;
        },

        /**
         * Getter/Setter für die Optionen
         *
         * <p>
         *  Wenn a und b nicht gegeben werden, dann werden alle Optionen
         *  zurückgegeben. Wird a Übergeben, aber b nicht, wird der Wert der
         *  Option [a] zurückgegeben. Wird b mit übergeben, wird der Option a
         *  der Wert b zugewiesen.
         * </p>
         *
         * @param {String|NULL} a
         * @param {mixed}       b
         *
         * @returns {mixed}
         */
        option: function (a, b) {
            if(typeof a === 'object') {
                this.options = a;
                return this;
            } else if(typeof a === "undefined") {
                return this.options;
            } else if (typeof b === "undefined") {
                return this.options[a];
            }
            this.options[a] = b;
            return;
        },

        listen: function() {
            var self = this, o = self.options,
                modal = Selectors.closest(self.element, '.modal');
            
            AcmsEvent.add(window, 'resize', function() {
                self.frameHeight = self.frame.clientHeight;
                self.frameWidth = self.frame.clientWidth;
                self._getFrameSizeFromFormat();
                Style.setStyles(self.container, {
                    'height': self.frameHeight + 'px'
                });
            });
            if(modal !== null) {
                var m = modal.acmsData('acms.ui.overlay.modal');
                if(!m) {
                    return;
                }
                AcmsEvent.add(m.target, 'acms.ui.overlay.modal.shown', function() {
                    AcmsEvent.fireResize()
                });
            }
        },

        _getFrameSizeFromFormat: function() {
            var o = this.options;
            switch (o.format) {
                case 'sd':
                    this.frameHeight = 3 * this.frameWidth / 4;
                    break;
                case 'square':
                case 'circle':
                    this.frameHeight = this.frameWidth;
                    break;
                case 'fill-h':
                case 'fill':
                    this.frameHeight = "100%";
                    Style.setStyle(this.imageContainer, 'height', '100%');
                    break;
                case 'hd':
                default:
                    this.frameHeight = 9 * this.frameWidth / 16;
            }
        },

        _buildContainer: function() {
            var self = this, o = self.options;
            if(! self.container) {
                self.container = document.createElement('div');
            }
            var minHeight = self.frameHeight, height = self.frameHeight;
            if(o.maxHeight) {
                Style.setStyles(self.imageContainer, {
                    'max-height': o.maxHeight + 'px'
                });
                Style.setStyles(self.container, {
                    'max-height': o.maxHeight + 'px'
                });
                
                if(minHeight > o.maxHeight) {
                    minHeight = o.maxHeight;
                    height = o.maxHeight;
                }
            }
            Style.setStyles(self.container, {
                'width': o.format === 'square' ? self.frameHeight + 'px' : '100%',
                'height': height + 'px',
                'min-height': minHeight + 'px',
                'display': 'block',
                'position': 'relative',
                'background-image': 'url('+self.source+')',
                'background-size': 'cover',
                'background-repeat': 'no-repeat',
                'border-radius': o.format === 'circle' ? '50%' : '0'
            });
            
            Style.setStyles(self.imageContainer, {'display': 'block'});
            Classes.addClass(self.imageContainer, 'display-block');
        },

        _applyOptions: function() {
            var self = this, o = self.options,
                color = o.frameColor,
                context = o.context,
                shadow = o.shadow;
            if (color !== 'default') {
                if (StringTool.isColor(color) || StringTool.isUrl(color)) {
                    Style.setStyle(self.frame, 'backgroundColor', color);
                } else {
                    Classes.addClass(self.frame, color);
                }
            }
            if (context !== false) {
                var overlay = document.createElement('div');
                overlay.innerHTML = context;
                self.imageContainer.appendChild(overlay);
                Classes.addClass(overlay, 'context');
            }
            if (shadow !== false) {
                Classes.addClass(self.imageContainer, 'block-shadow');
            }
        },

        _fixImageContainer: function() {
            var self = this, o = self.options;
            switch (o.type) {
                case 'octagon':
                    Classes.addClass(self.imageContainer, 'octagon');
                    Classes.addClass(self.container, 'replacement');
                    break;
                case 'bordered':
                    Classes.addClass(self.imageContainer, 'bordered');
                    break;
                case 'polaroid':
                    Classes.addClass(self.imageContainer, 'polaroid');
                    break;
                case 'hang-up':
                    Classes.addClass(self.imageContainer, 'hang-up');
                    break;
                case 'hang-up animate':
                    Classes.addClass(self.imageContainer, 'hang-up,animate');
                    break;
                case 'hang-up-animate-on-hover':
                    Classes.addClass(self.imageContainer, 'hang-up animate-on-hover');
                    break;
            }
            Classes.addClass(self.imageContainer, 'image-format-' + o.format);
        }
    }

    return ImageFit;
});
