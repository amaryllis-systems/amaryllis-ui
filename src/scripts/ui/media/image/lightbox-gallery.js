/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */


define("ui/media/image/lightbox-gallery", [
    "tools/utils",
    "core/selectors",
    "events/event",
    "ui/media/image/lightbox",
    "core/window"
], function(Utils, Selectors, AcmsEvent, Lightbox, w) {

    "use strict";

    /**
     * Constructor
     *
     * @param {type} element    Das Gallerie-Element
     * @param {type} options    Die eigenen Optionen
     * @returns {Gallery}
     */
    var Gallery = function(element, options) {
        this.element =
        this.images =
        this.current =
        this.length =
        this.lightbox =
        this.options = null;
        this.isOpen = false;
        this.initialize(element, options);
    };
    
    /**
     * Modul Name
     * 
     * @var {String}
     */
    Gallery.MODULE = "Lightbox Galllery";
    
    /**
     * Modul Version
     * 
     * @var {String}
     */
    Gallery.VERSION = "1.5.0";
        
    /**
     * Modul Namespace
     * 
     * @var {String}
     */
    Gallery.NS = "acms.ui.media.image.lightbox-gallery";
    
    /**
     * Linker Pfeil Navigation
     * 
     * @var {Integer}
     */
    Gallery.ARROW_LEFT = 37;
    /**
     * Rechter Pfeil Navigation
     * 
     * @var {Integer}
     */
    Gallery.ARROW_RIGHT = 39;
    /**
     * Escape Navigation
     * 
     * @var {Integer}
     */
    Gallery.ESCAPE = 27;

    /**
     * Standard Optionen
     * 
     * @var {Object}
     */
    Gallery.DEFAULT_OPTIONS = {
        gallery: "gallery",
        show: false,
        leftarrow: '[data-trigger="prev"]',
        rightarrow: '[data-trigger="next"]',
        modalid: '#lightbox-gallery-modal',
        directionalarrows: true
    };

    Gallery.needInit = true;

    Gallery.init = function(element, options) {
        var g = new Gallery(element, options);
        g.element.acmsData(Gallery.NS, g);

        return g;
    };

    Gallery.prototype = {

        constructor: Gallery,

        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },

        /**
         * Gibt die Standard-Optionen zurück
         * 
         * @returns {Gallery.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function() {
            return Gallery.DEFAULT_OPTIONS;
        },

        /**
         * Bildet die Optionen
         *
         * Basierend auf den Standard-Optionen, den übergebenen Optionen des
         * Constructors und den Element Data-Attributen werden die aktuellen
         * Konfigurationen gebildet. Letztenendes gewinnen die Element
         * Data-Attribute.
         *
         * @param {type} options
         *
         * @returns {Object}
         */
        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },

        next: function() {
            this.current ++;
            var self = this, image;
            if(self.current >= self.length){
                self.current = 0;
            }
            image = self.images[self.current];
            self.run(image);
        },
        
        previous: function() {
            this.current--;
            var self = this, image;
            if(self.current < 0){
                self.current = self.length - 1;
            }
            image = self.images[self.current];
            self.run(image);
        },

        first: function() {
            this.current ++;
            var self = this, image;
            if(self.current === self.length){
                self.current = 0;
                image = self.images[0];
            } else{
                image = self.images[self.current];
            }
            self.run(image);
        },

        last: function() {
            var self = this, image;
            self.current = self.length -1;
            image = self.images[self.current];
            
            self.run(image);
        },

        run: function (image) {
            var self = this;
            self.lightbox.updateElement(image);
            if(false === self.isOpen) {
                self.isOpen = true;
                self.lightbox.show();
            }
        },

        listen: function() {
            var self = this, o = self.options, element = self.element;
            AcmsEvent.add(document, 'keyup', self._navigate);
            self.images.forEach(function(img) {
                AcmsEvent.add(img, 'click', self._onClick);
                AcmsEvent.add(img, Lightbox.EVENT_HIDE, self._onClose);
            });
            var cb1 = function(e) {
                console.log(e);
                e.preventDefault();
                self.previous();
                
                return false;
            }, cb2 = function(e) {
                console.log(e);
                e.preventDefault();
                self.next();
                
                return false;
            };
            console.log(self.modal);
            AcmsEvent.on(self.modal, 'click',o.leftarrow, cb1, true);
            AcmsEvent.on(self.modal, 'click', o.rightarrow, cb2, true);
        },
        
        _onClose: function(e) {
            this.isOpen = false;
        },
        
        _onClick: function(e) {
            var self = this, o = self.options, target = e.target;
            if(!Selectors.matches(target, '[data-gallery="' + o.gallery + '"]')) {
                target = Selectors.closest(target, '[data-gallery="' + o.gallery + '"]');
            }
            target && self.run(target);
        },

        _navigate: function (event) {
            event = event || w.event;
            var key = event.which || event.keyCode;
            switch(key) {
                case Gallery.ARROW_LEFT:
                    return this.previous();
                case Gallery.ARROW_RIGHT:
                    return this.next();
                case Gallery.ESCAPE:
                    return this.lightbox.close();
                default:
                    return;
            }
        },
        
        _prepare: function() {
            var self = this, 
                o = self.options,
                element = self.element, i , current = 0, first = true, img;
            self.images = Selectors.qa('[data-gallery="' + o.gallery + '"]', element);
            for(i = 0; i < self.images.length; i++) {
                img = self.images[i];
                img.acmsData('index', i);
                if(true === first) {
                    current = i;
                    first = false;
                }
            }
            self.length = self.images.length;
            if(0 === self.length) {
                throw new Error;
            }
            self.prevBtn = Selectors.q(o.leftarrow);
            self.nextBtn = Selectors.q(o.rightarrow);
            self.current = current;
            self.modal = Selectors.q(o.modalid);
            self.lightbox = Lightbox.init(self.images[self.current], {
                modalid: o.modalid
            });
            if(o.show) {
                self.lightbox.show();
            }
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        }

    };

    return Gallery;

});
