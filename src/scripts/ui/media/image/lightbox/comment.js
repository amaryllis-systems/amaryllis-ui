/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */


define("ui/media/image/lightbox/comment", [
    'tools/utils'
], function(Utils) {

    "use strict";

    var Comment = function(comment) {
    };

    Comment.MODULE = "Kommentar";

    Comment.VERSION = "1.5.0";

    Comment.prototype = {

        constructor: Comment,

        initialize: function(comment) {
            this.comment = comment;
            this.username = comment.user.username;
            this.firstName = comment.user.first_name;
            this.lastName = comment.user.last_name;
            this.avatar = comment.user.avatar;
            this.date = comment.pdate;
            this.message = comment.body;
            this.template = Acms.Templates.Lightbox
        },
        
        setTemplate: function(template) {
            this.template = template;
            
            return this;
        },

        getTemplate: function() {
            return this.template
        },

        render: function() {

        }

    };

});
