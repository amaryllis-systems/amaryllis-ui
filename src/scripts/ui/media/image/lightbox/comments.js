/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */

define("ui/media/image/lightbox/comments", [
    'tools/utils',
    "tools/string/string-tool",
    'core/acms'
], function(Utils, StringTool, A) {

    "use strict";

    var LBC_DATA = "acms.lightbox.comments",
        NS = "." + "." + LBC_DATA,
        EVENT_LOADING = "loading" + NS,
        EVENT_LOADED = "loaded" + NS

    /**
     * Constructor
     *
     * @param {Object} element  Der Kommentar-Container
     * @param {Object} options  Die eigenen Optionen
     *
     * @returns {LBComments}
     */
    var LBComments = function(element, options) {
        this.element =
        this.options =
        this.comments =
        this.length = null;
        this.initialize(element, options);
    };

    /**
     * Komponenten Name
     *
     * @var {String}
     */
    LBComments.MODULE = "Lightbox Comments";

    LBComments.VERSION = "1.5.0";

    LBComments.DEFAULT_OPTIONS = {
        remote: null,
        remoteData: {
            itemid: null,
            item: null,
            module: 0
        },
        container: null,
        offset: 0,
        limit: 30,
        loadMore: true,
        cansubmit: false
    };

    LBComments.needInit = false;

    LBComments.prototype = {

        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
        },

        /**
         * Gibt die Standard-Optionen zurück
         * 
         * @returns {LBComments.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function() {
            return LBComments.DEFAULT_OPTIONS;
        },

        /**
         * Bildet die Optionen
         *
         * Basierend auf den Standard-Optionen, den übergebenen Optionen des
         * Constructors und den Element Data-Attributen werden die aktuellen
         * Konfigurationen gebildet. Letztenendes gewinnen die Element
         * Data-Attribute.
         *
         * @param {type} options
         *
         * @returns {Object}
         */
        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },

        loadComments: function() {
            var self = this, o = self.options, fData = new FormData();
            if(! o.remote || (o.remote && false === StringTool.isUrl(o.remote))) {
                return;
            }
            if(self.loading) {
                return;
            }
            self.loading = true;
            require(['http/request', 'notifier'], function(Request) {
                Request.post(o.remote, fData, self._done, self._fail, self._always);
            });
        },
        
        _always: function() {
            var self = this;
            self.loading = false;
        },

        _done: function(response) {
            if(response.status === "success" && response.comments) {
                var c = response.comments;
                c.forEach(function(comment) {

                });
            }
        },
        
        _fail: function(xhr, textStatus) {
            A.Logger.logWarning(textStatus);
            A.Notifications.createNotification(
                        A.Translator._('Ups! Da ist was schief gelaufen! Bitte versuche Sie es später erneut!'),
                        A.Translator._('Fehler!'),
                        A.Notifications.TYPE_DANGER);
        },
        
        _prepare: function() {
            var self = this;
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        }
    }

    return LBComments;
});
