/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license  https://www.amaryllis-system.de/lizenzen/eula/
 */


/**
 * <h2>Responsive Image Implementation</h2>
 * 
 * <p>
 *  Das Modul bietet eine Unterstützung für unterschiedliche Bildgrößen per 
 *  Screen-Dimension. Die Abfrage erfolgt über Media-Matches und kann entweder 
 *  nur die Quelle eines Bildes ersetzten oder, wenn das Modul auf einem div-
 *  Wrapper initiiert wird, das ganze Bild inklusive title-, alt-tag und CSS-
 *  Klasssen.
 * </p>
 * <p>
 *  Das Modul erwartet nur einen Options-Parameter, der wahlweise über ein Objekt 
 *  oder einen JSON-String übergeben werden kann.<br>
 *  Ein Beispiel für einen Parameter wäre:
 * </p>
 * <pre><code>
 * var opts = {
 *  images: [
 *      {
 *          'media': 'only screen and (min-width: 480px) and (max-width: 767px)',
 *          'src': 'https://www.example.de/example-image/?width=300',
 *          'classes': 'image responsive small-device'
 *      },
 *      {
 *          'media': 'only screen and (min-width: 768px) and (max-width: 1023px)',
 *          'src': 'https://www.example.de/example-image/?width=500',
 *          'classes': 'image responsive medium-device'
 *      },
 *      {
 *          'media': 'only screen and (min-width: 1024px) and (max-width: 1199px)',
 *          'src': 'https://www.example.de/example-image/?width=800',
 *          'classes': 'image responsive large-device'
 *      }
 *  ]
 * };
 * var img = document.querySelector('#example-image');
 * var mo = ResponsiveImage.init(img, opts);
 * </code></pre>
 * <p>
 *  Ein Besipiel aus dem Smarty-Template heraus könnte wie folgt aussehen:
 * </p>
 * <pre><code>
 * <{toJson data=[
 *      0 => [
 *          'media' => 'only screen and (min-width: 480px) and (max-width: 767px)',
 *          'src' => 'https://www.example.de/small-example-image/?width=300',
 *          'classes' => 'image responsive small-device',
 *          'title' => 'Small Device Title',
 *          'alt' => 'Small Device Alt'
 *      ],
 *      1 => [
 *          'media': 'only screen and (min-width: 768px) and (max-width: 1023px)',
 *          'src': 'https://www.example.de/medium-example-image/?width=500',
 *          'classes': 'image responsive medium-device',
 *          'title' => 'Medium Device Title',
 *          'alt' => 'Medium Device Alt'
 *      ],
 *      2 => [
 *          'media': 'only screen and (min-width: 1024px) and (max-width: 1199px)',
 *          'src': 'https://www.example.de/large-example-image/?width=800',
 *          'classes': 'image responsive large-device',
 *          'title' => 'Large Device Title',
 *          'alt' => 'Large Device Alt'
 *      ]
 * ] assign="images"}>
 * <div data-images="<{$images}>" data-module="ui/media/image/responsive-image">
 * </div>
 * </code></pre>
 * <p>
 *  Ein Besipiel aus dem Text heraus (z.B. News-Artikel) über einen Short-Code 
 *  könnte so aussehen:
 * </p>
 * <pre><code>
 *  [image_responsive media="only screen and (min-width: 480px) and (max-width: 767px)|only screen and (min-width: 768px) and (max-width: 1023px)|only screen and (min-width: 1024px) and (max-width: 1199px)"
 *      title="Small Title|Medium Title|Large Title"
 *      alt="Small Alt| Medium Alt| Large Alt"
 *      src="https://examplede/images/my-small-img.png|https://examplede/images/my-large-img.png|https://examplede/images/my-large-img.png"
 *      
 *  ]
 *  </code></pre>
 * 
 * @param {Object} Utils
 * @param {Object} Classes
 * @param {Object} MatchMedia
 * @param {Object} AcmsEvent
 * @param {Object} A
 * @returns {ResponsiveImage}
 */
define('ui/media/image/responsive-image', [
    'tools/utils',
    'core/classes',
    'device/match-media',
    'events/event',
    'core/acms'
], function (Utils, Classes, MatchMedia, AcmsEvent, A) {

    "use strict";

    /**
     * <h3>Constructor</h3>
     * 
     * <p>
     *  Erstellt eine neue Instanz des Moduls. Das Element kann ein DIV-Wrapper 
     *  um eine Bild-Quelle oder ein img-/image-Tag sein.
     * </p>
     * 
     * @param {Element}     element     Das HTML-Element
     * @param {Object}      options     Die Optionen
     * @returns {ResponsiveImage}
     */
    var ResponsiveImage = function (element, options) {
        this.element =
                this.options =
                this.toggles = null;
        this.options = ResponsiveImage.DEFAULT_OPTIONS;

        this.initialize(element, options);
    };

    // -------------------------------------------------------------------------
    // Static Methods
    // -------------------------------------------------------------------------

    /**
     * Modul Name
     * 
     * @var {String}
     */
    ResponsiveImage.MODULE = "Responsive Image";

    /**
     * Modul Version
     * 
     * @var {String}
     */
    ResponsiveImage.VERSION = "1.5.0";

    /**
     * Modul Namespace
     * 
     * @var {String}
     */
    ResponsiveImage.NS = "acms.ui.media.image.responsive-image";

    /**
     * Standard-Optionen des Moduls
     * 
     * @var {Object}
     */
    ResponsiveImage.DEFAULT_OPTIONS = {
        'images': null // Object/JSON-String
    };

    /**
     * Sagt dem Modul-Loader, dass das Modul initialisiert werden muss, wenn es 
     * über diesen geladen wird.
     * 
     * @var {Boolean}
     */
    ResponsiveImage.needInit = true;

    /**
     * Modul Initialisierung
     * 
     * @param {Element}     element     Das HTML-Element
     * @param {Object}      options     Die Optionen
     * @returns {ResponsiveImage}
     */
    ResponsiveImage.init = function (element, options) {
        var v = new ResponsiveImage(element, options);
        v.element.acmsData(ResponsiveImage.NS, v);

        return v;
    };

    /**
     * Klassen Definition
     */
    ResponsiveImage.prototype = {

        constructor: ResponsiveImage,

        /**
         * Interner Constructor
         * 
         * @param {Element} element     HMTL Element
         * @param {Objct}   options     Eigene Optionen
         * 
         * 
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this.updateSource();
            this.listen();
        },

        /**
         * Aufsetzen der Event-Listener
         * 
         */
        listen: function () {
            var self = this;
            AcmsEvent.add(window, 'resize', self.updateSource.bind(self));
        },

        /**
         * Gibt die Standard-Optionen zurück
         * 
         * @returns {ResponsiveImage.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return ResponsiveImage.DEFAULT_OPTIONS;
        },

        /**
         * Bilden der Optionen
         * 
         * Bildet die Optionen basierend auf den Standard-Optionen, den 
         * übergebenen Optionen des Constructors und den Element Data-Attributen. 
         * Letztere gewinnen den Kampf, wenn gesetzt.
         * 
         * @param {Object} options    Die eignenen Optionen
         * @returns {Object|ResponsiveImage.DEFAULT_OPTIONS}
         */
        buildOptions: function (options) {
            var o = Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
            if (typeof o.images === 'string') {
                try {
                    o.images = JSON.parse(o.images);
                } catch (E) {
                    console.log(E);
                    o.images = new Array();
                }
            }

            return o;
        },

        /**
         * Aktualisiert ein Bild
         * 
         * 
         */
        updateSource: function () {
            var self = this,
                    element = self.element,
                    o = self.options, i, images = o.images, img;
            for (i = 0; i < images.length; i++) {
                img = images[i];
                if (MatchMedia.matches(img.media)) {
                    if (element.tagName.toLowerCase() === 'img'
                            || element.tagName.toLowerCase() === 'image') {
                        element.setAttribute('src', img.src);
                        if (typeof img.classes === 'string') {

                            Classes.addClass(element, img.classes);
                        }
                    } else {
                        var im = document.createElement('img');
                        im.src = img.src;
                        if (typeof img.classes === 'string') {
                            im.className = '';
                            Classes.addClass(element, img.classes);
                        }
                        im.setAttribute('title', img.title || '');
                        im.setAttribute('alt', img.alt || '');
                        element.innerHTML = '';
                        element.appendChild(im);
                    }
                    break;
                }
            }
        }

    };

    return ResponsiveImage;

});

