/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */

/**
 * Lightbox Komponente des Systems
 * 
 * @param {Object} Utils
 * @param {Object} Selectors
 * @param {Object} Classes
 * @param {Object} AcmsEvent
 * @param {Object} Style
 * @param {Object} Modal
 * @param {Object} t
 * @param {Object} A
 * 
 * @returns {Lightbox}
 */
define("ui/media/image/lightbox", [
    "tools/utils",
    "core/selectors",
    "core/classes",
    "events/event",
    "core/style",
    "apps/ui/informations/modal",
    "templates/templates",
    "core/acms"
], function (Utils, Selectors, Classes, AcmsEvent, Style, Modal, t, A) {

    'use strict';

    var L_DATA = "acms.ui.media.image.lightbox",
        NS = "." + L_DATA,
        EVENT_SHOW = "show" + NS,
        EVENT_SHOWN = "shown" + NS,
        EVENT_HIDE = "hide" + NS,
        EVENT_HIDDEN = "hidden" + NS,
        EVENT_CONTENT_LOADED = "content-loaded" + NS
    ;

    /**
     * Lightbox Constructor
     *
     * @param {string} element  Das Element wleches die Lightbox repräsentiert
     * @param {Object} options  Eigene Optionen
     *
     * @returns {Lightbox}
     */
    var Lightbox = function (element, options) {
        this.options =
        this.element =
        this.modalID =
        this.modal =
        this.$images =
        this.length =
        this.debug = null
        this.initialize(element, options);
    };
    
    /**
     * Modul Version
     *
     * @var {String}
     */
    Lightbox.VERSION = '1.5.0';

    /**
     * Modul Name
     *
     * @var {String}
     */
    Lightbox.MODULE = 'Lightbox';
    
    /**
     * Modul Namespace
     * 
     * @var {String}
     */
    Lightbox.NS = "acms.media.image.lightbox";
    
    Lightbox.EVENT_SHOW = Lightbox.NS + '.show';
    
    Lightbox.EVENT_SHOWN = Lightbox.NS + '.shown';
    
    Lightbox.EVENT_HIDE = Lightbox.NS + '.hide';
    
    Lightbox.EVENT_HIDDEN = Lightbox.NS + '.hidden';
    
    Lightbox.EVENT_CONTENT_LOADED = Lightbox.NS + '.content-loaded';

    /**
     * Standard-Optionen der Lightbox
     * 
     * @var {Object}
     */
    Lightbox.DEFAULT_OPTIONS = {
        gallery: false,
        title: null,
        footer: null,
        remote: null,
        fullwidth: true,
        showclose: true,
        type: 'image',
        /**
         * Lightbox-Container
         */
        /* das template, wenn kein Container existiert */
        template: A.Templates.Lightbox.container,
        /** Eine optionale Modal id **/
        modalid: null,
        /* ein selector für den Container (z.B. "#acms-lightbox") */
        container: null,
        /* Comment feature */
        /* true, if enable comments */
        hascomment: false,
        /* selector (ID) für den comment container */
        commentcontainer: null,
        /* commentform template */
        commenttemplate: A.Templates.Lightbox.comment,
        commentform: A.Templates.Lightbox.commentForm,
        commenttoggle: "acms-icon acms-icon-comments",
        show: false
    };

    Lightbox.needInit = true;

    /**
     * Module-Initialisator der Light-Box
     *
     * @param {Object} element
     * @param {Object} options
     * @returns {Lightbox}
     */
    Lightbox.init = function (element, options) {
        var lb = new Lightbox(element, options);
        lb.element.acmsData(L_DATA, lb);
        return lb;
    };

    Lightbox.css = "/media/css/overlay/modal.min.css";


    Lightbox.prototype = {
        constructor: Lightbox,
        
        /**
         * Interner Constructor
         * 
         * @param {Object} element  Element
         * @param {Object} options  Optionen
         * 
         * 
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },
        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {Lightbox.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return Lightbox.DEFAULT_OPTIONS;
        },
        /**
         * Bildet die Optionen
         * 
         * <p>
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         * </p>
         * 
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },

        /**
         * Getter/Setter für die Optionen
         *
         * <p>
         *  Wenn a und b nicht gegeben werden, dann werden alle Optionen
         *  zurückgegeben. Wird a Übergeben, aber b nicht, wird der Wert der
         *  Option [a] zurückgegeben. Wird b mit übergeben, wird der Option a
         *  der Wert b zugewiesen.
         * </p>
         *
         * @param {String|NULL} a
         * @param {mixed}       b
         *
         * @returns {mixed}
         */
        option: function (a, b) {
            if(typeof a === "undefined") {
                return this.options;
            }
            if (typeof b === "undefined") {
                return this.options[a]
            }
            this.options[a] = b;
            return;
        },

        listen: function() {
            var self = this,
                element = self.element,
                cb = function(e) {
                    e.preventDefault();
                    self.show();
                };
            AcmsEvent.add(element, 'click', cb);
        },

        show: function() {
            var self = this,
                element = self.element,
                o = self.options, evt;
            evt = AcmsEvent.createCustom(Lightbox.EVENT_SHOW);
            AcmsEvent.dispatch(element, evt);
            
            this.updateTitleAndFooter();
            this.modal.show();
            this.updateContent();
            evt = AcmsEvent.createCustom(Lightbox.EVENT_SHOWN);
            AcmsEvent.dispatch(element, evt);
            
            AcmsEvent.add(element, Modal.EVENT_HIDE, self._onHideModal);
            AcmsEvent.add(element, Modal.EVENT_HIDDEN, self._onHiddenModal);
        },

        /**
         * Aktualisieren eines Elementes
         *
         * Überschreibt das vorige Element mit einem Neuen.
         *
         * @param {Element}  element
         * @param {Object}   options
         * 
         * @returns {Lightbox.prototype}
         */
        updateElement: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this.$modal = this._getModal(true);
            this.updateTitleAndFooter();
            this.updateContent();
            
            return this;
        },

        /**
         * Aktualisieren von Titel und Footer
         *
         * @returns {Lightbox.prototype}
         */
        updateTitleAndFooter: function () {
            var self = this,
                element = self.element,
                o = self.options,
                modal = self.$modal,
                caption, footer, header, title, htitle;
            header = Selectors.q('.header', modal);
            footer = Selectors.q('.footer', modal);
            title = o.title || element.getAttribute("title") || "";
            caption = o.footer || "";
            self.title = title || '';
            if (title || o.showclose) {
                Style.setStyle(header, 'display', '');
                htitle = Selectors.q('.title', header);
                htitle.innerHTML = title || "&nbsp;";
            } else {
                Style.setStyle(header, 'display', 'none');
                htitle = Selectors.q('.title', header);
                htitle.innerHTML = "&nbsp;";
            }
            if (caption) {
                Style.setStyle(footer, 'display', '');
                footer.innerHTML = caption;
            } else {
                Style.setStyle(footer, 'display', 'none');
            }
            return this;
        },

        updateContent: function() {
            var self = this, element = self.element;
            AcmsEvent.add(element, Lightbox.EVENT_CONTENT_LOADED, self._processContent);
            
            self.getContent();
        },

        getContent: function() {
            var self = this,
                o = self.options,
                element = self.element,
                type = o.type || "image",
                opts = {relatedTarget: self.$modal},
                evt, el;
            self.showLoading();
            switch(type) {
                case "image":
                    require(["ui/elements/image"], function(img) {
                        el = new img({
                            src: o.image,
                            alt: '',
                            title: (o.title || element.getAttribute("title") || "")
                        });
                        self.content = el.getImage();
                        opts.content = self.content;
                        evt = AcmsEvent.createCustom(Lightbox.EVENT_CONTENT_LOADED, opts);
                        AcmsEvent.dispatch(element, evt);
                    });
                    break;
                case "audio":
                    require(["ui/elements/audio"], function(aud) {
                        el = new aud({
                            src: o.image
                        });
                        self.content = el;
                        opts.content = self.content;
                        evt = AcmsEvent.createCustom(Lightbox.EVENT_CONTENT_LOADED, opts);
                        AcmsEvent.dispatch(element, evt);
                    });
                    break;
            }
        },

        /**
         * Shows the Loading state on init
         */
        showLoading: function () {
            var self = this,
                modal = self.$modal, div, content, lbc;
            self.loading = true;
            content = Selectors.q('.lightbox-content', modal);
            lbc = Selectors.q('.lightbox-comments', modal);
            if(lbc) Style.setStyles(lbc, {display: 'none', visibility: 'hidden'});
            if(content) {
                div = document.createElement('div');
                div.innerHTML = '<div class="modal-loading">' + A.Translator._('Loading...') + '</div>';
                content.innerHTML = '';
                while(div.firstChild) {
                    content.appendChild(div.firstChild);
                }
            }
            return this;
        },
        /**
         * Checks, if we have an image
         *
         * @return {Boolean}
         */
        isImage: function (str) {
            return str.match(/(^data:image\/.*,)|(\.(jp(e|g|eg)|gif|png|bmp|webp|svg)((\?|#).*)?$)/i);
        },
        /**
         * Checks, if we have an swf file
         *
         * @return {Boolean}
         */
        isSwf: function (str) {
            return str.match(/\.(swf)((\?|#).*)?$/i);
        },
        /**
         * Gets the YouTube ID
         *
         * @return {String|Boolean} YouTube ID or Boolean false
         */
        getYoutubeId: function (str) {
            var match;
            match = str.match(/^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/);
            if (match && match[2].length === 11) {
                return match[2];
            } else {
                return false;
            }
        },
        /**
         * Gets the Vimeo ID
         *
         * @return {String|Boolean} Vimeo ID or Boolean false
         */
        getVimeoId: function (str) {
            if (str.indexOf('vimeo') > 0) {
                return str;
            } else {
                return false;
            }
        },
        /**
         * Gets the Instagram ID
         *
         * @return {String|Boolean} Instagram ID or Boolean false
         */
        getInstagramId: function (str) {
            if (str.indexOf('instagram') > 0) {
                return str;
            } else {
                return false;
            }
        },

        isExternal: function (url) {
            var match;
            match = url.match(/^([^:\/?#]+:)?(?:\/\/([^\/?#]*))?([^?#]+)?(\?[^#]*)?(#.*)?/);
            if (typeof match[1] === "string" && match[1].length > 0 && match[1].toLowerCase() !== location.protocol) {
                return true;
            }
            if (typeof match[2] === "string" && match[2].length > 0 && match[2].replace(new RegExp(":(" + {
                "http:": 80,
                "https:": 443
            }[location.protocol] + ")?$"), "") !== location.host) {
                return true;
            }
            return false;
        },

        _processContent: function() {
            var self = this,
                o = self.options,
                modal = self.$modal,
                content = Selectors.q('.lightbox-content', modal),
                lbc = Selectors.q('.lightbox-comments', modal),
                type = o.type || "image";
                if(lbc && (o.hascomment === false || o.hascomment === 'false')) {
                    Style.setStyles(lbc, {display: 'none', visibility: 'hidden'})
                    lbc.setAttribute('aria-hidden', 'true');
                    Classes.addClass(lbc, 'hidden invisible');
                } else if (lbc) {
                    if(o.commentcontainer) {
                        var comm = Selectors.q(o.commentcontainer);
                        lbc.innerHTML = comm.innerHTML;
                    } else if(o.commentremote) {
                        // @todo
                    }
                }
                switch(type) {
                    case "image":
                    case "audio":
                    case "video":
                        content.innerHTML = '';
                        content.appendChild(self.content);
                        break;
                }
                if(self.loading) {
                    self.loading = false;
                }
        },


        _getModal: function(isUpdate) {
            var self = this,
                o = self.options,
                template;
            isUpdate = isUpdate || false;
            if(true === isUpdate && o.container) {
                return self.$modal;
            }
            if(o.container) {
                return Selectors.q(o.container);
            }
            var template = Selectors.q(self.modalID);
            if(template) {
                return template;
            }
            if(o.template) {
                template = o.template;
            } else {
                template = A.Templates.Lightbox.container;
            }
            var div = document.createElement('div');
            div.innerHTML = template;
            div.firstChild.id = self.modalID.replace('#', '');
            div.firstChild.setAttribute('id', self.modalID.replace('#', ''));
            while(div.firstChild) {
                document.body.appendChild(div.firstChild);
            }
            return Selectors.q(self.modalID);
        },
        
        _onHideModal: function(e) {
            var self = this,
                o = self.options,
                element = self.element, evt, res;
            evt = AcmsEvent.createCustom(Lightbox.EVENT_HIDE);
            AcmsEvent.dispatch(element, evt);
            if(evt.defaultPrevented) {
                e.preventDefault();
            }
            if(typeof o.onHide === 'function') {
                res = o.onHide(element, self);
                if(!res) {
                    e.preventDefault();
                }
            }
        },
        
        _onHiddenModal: function(e) {
            var self = this,
                o = self.options,
                element = self.element, evt;
            evt = AcmsEvent.createCustom(Lightbox.EVENT_HIDDEN);
            AcmsEvent.dispatch(element, evt);
            if(evt.defaultPrevented) {
                e.preventDefault();
            }
            if(typeof o.onHidden === 'function') {
                o.onHidden(element, self);
            }
        },
        
        _prepare: function() {
            var self = this, 
                o = self.options,
                element = self.element;
            self.modalID = this.options.modalid 
                            ? this.options.modalid
                            : '#acms-lightbox-' + Math.floor((Math.random() * 1000) + 1);
            
            self.$modal = self._getModal();
            self.updateTitleAndFooter();
            self.modal = Modal.init(element, {target: self.modalID});
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        }
    };

    return Lightbox;
});
