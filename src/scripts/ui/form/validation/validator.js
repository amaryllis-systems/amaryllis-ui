/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */

define('ui/form/validation/validator', [
    'tools/utils',
    'core/classes',
    'core/selectors',
    'events/event'
], function(Utils, Classes, Selectors, AcmsEvent) {
    
    "use strict";
    
    /**
     * Data Name des Validators
     * 
     * @type String
     */
    var VALIDATOR_DATA = "acms.form.validation.validator";
    /**
     * Event Namespace
     * @type VALIDATOR_DATA|String
     */
    var NS = ".".VALIDATOR_DATA;
    
    /**
     * Basis Data-Name der Rules
     * @type String
     */
    var RULE_DATA = "acms.form.validation.rules.";
    
    /**
     * Constructor
     * 
     * @param {Element} element
     * @param {Object} options
     * @returns {Validator}
     */
    var Validator = function(element, options) {
        this.element    = 
        this.options    =
        this.form       =
        this.fields     =   null;
        this.initialize(element, options);
    };
    
    /**
     * Module Name
     * 
     * @var {String}
     */
    Validator.MODULE = "Formular Validator";
    
    /**
     * Modul Version
     * 
     * @var {String}
     */
    Validator.VERSION = "1.5.0";
    
    /**
     * Standard-Optionen
     * 
     * @var {Object}
     */
    Validator.DEFAULT_OPTIONS = {
        errorContainer: false, // String ID Selector des Error Containers
        useNotifier: false, // Boolean: Soll der Notifier für Errors verwendet werden?
    };
    
    /**
     * Einbinden des Modul Loaders
     * 
     * @var {Boolean}
     */
    Validator.needInit = true;
    
    /**
     * Modul Loader Initialisierung
     * 
     * @param {Element} element
     * @param {Object} options
     * 
     * 
     */
    Validator.init = function(element, options) {
        var v = new Validator(element, options);
        
        return v;
    };
    
    Validator.prototype = {
        constructor: Validator,
        
        /**
         * Interner Constructor
         * 
         * @param {Element} element
         * @param {Object} options
         * 
         * 
         */
        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            if(element.tagName.toUpperCase() === 'FORM') {
                this.form = element;
            } else {
                this.form = element.querySelector('form');
            }
            this._prepare();
            this.listen();
        },
        
        /**
         * Gibt die Standard-Optionen zurück
         * 
         * @returns {Validator.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function() {
            return Validator.DEFAULT_OPTIONS;
        },
        
        buildOptions: function(options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },
        
        validate: function() {
            
        },
        
        listen: function() {
            var self = this,
                fields = this.fields,
                i, field, rule, ruleName;
            var cb = function(e) {
                field = e.target;
                ruleName = field.getAttribute('data-rule');
                rule = field.acmsData(RULE_DATA+ruleName);
                if(null === rule || typeof rule !== "object") {
                    return true;
                }
                rule.isValid();
            };
            
            for(i = 0; i < fields.length; i++) {
                field = fields[i];
                ruleName = field.getAttribute('data-rule');
                rule = field.acmsData(RULE_DATA+ruleName);
                if(null === rule || typeof rule !== "object") {
                    continue;
                }
                
                AcmsEvent.add(field, 'change', cb);
            }
        },
        
        _isInvalid: function(field) {
            
        },
        
        _isValid: function(field) {
            
        },
        
        _typeFromField: function(input) {
            var tagName = input.tagName.toUpperCase();
            var type = tagName === "INPUT"
                            ? input.type : tagName.toLowerCase();
            
            return type;
        },
        
        _prepare: function() {
            var form = this.form,
                self = this,
                o = this.options,
                fields = Selectors.qa('[data-rule]', form),
                i, rule, field;
            var onValidated = function(ele, state) {
                if(state === false) {
                    return self._isInvalid(ele);
                }
                return self._isValid(ele);
            };
            var def = {
                onValidated: onValidated
            };
            if(o.errorContainer) {
                def.errorContainer = o.errorContainer;
            }
            for(i = 0; i < fields.length; i++) {
                field = fields[i];
                rule = field.getAttribute('data-rule');
                try {
                    require(['ui/form/validation/rules/'+rule], function(Rule) {
                        Rule && Rule.init(field, def);
                    });
                } catch(E) {
                    if(Acms.Logger) {
                        Acms.Logger.writeLog("Die Validierung " + rule + " konnte nicht geladen werden");
                    }
                }
            }
            this.fields = fields;
        }
    };
    
    return Validator;
    
});
