/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */


define('ui/form/upload-disclaimer-required', [
    'tools/utils',
    'core/selectors',
    'core/classes',
    'events/event'
], function(Utils, Selectors, Classes, AcmsEvent) {

    "use strict";

    var CHECK_DATA = "acms.upload.disclaimer.check",
        NS = "." + CHECK_DATA,
        EVENT_ENABLED = "enabled" + NS,
        EVENT_ENABLING = "enabling" + NS,
        EVENT_DISABLING = "disabling" + NS,
        EVENT_DISABLED = "disabled" + NS
    ;


    var Check = function(element, options) {
        this.field =
        this.options =
        this.checkbox =
        this.hidden = null;

        this.initialize(element, options);
    };

    Check.MODULE = "Upload Disclaimer Check";

    Check.VERSION = "1.5.0";
    
    Check.NS = "acms.upload.disclaimer.check";

    Check.DEFAULT_OPTIONS = {
        check: ".accept-disclaimer",
        field: "input[type=file]",
        hidden: ".accepted-disclaimer",
        iconChecked: "acms-icon-check-square",
        iconUnchecked: "acms-icon-square",
        trigger: 'click'
    };

    Check.needInit = true;
    
    Check.init = function(element, options) {
        var m = new Check(element, options);
        m.element.acmsData(CHECK_DATA, m);

        return m;
    };

    Check.prototype = {

        constructor: Check,

        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._setup();
            this.listen();
        },

        getDefaultOptions: function() {
            return Check.DEFAULT_OPTIONS;
        },

        buildOptions: function(options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },

        disable: function(event) {
            var self = this,
                o = self.options,
                field = self.field, e;
            e = AcmsEvent.create(EVENT_DISABLING);
            AcmsEvent.dispatch(field, e);
            if(e.defaultPrevented) {
                return;
            }
            self._disableField();
            var clone = field.cloneNode(true);
            field.parentNode.replaceChild(clone, field);
            if(typeof o.onDisable === 'function') {
                o.onDisable(field, self);
            }
            e = AcmsEvent.create(EVENT_DISABLED);
            AcmsEvent.dispatch(field, e);
        },

        enable: function(event) {
            var self = this,
                o = self.options,
                field = self.field, e;
            e = AcmsEvent.create(EVENT_ENABLING);
            AcmsEvent.dispatch(field, e);
            if(e.defaultPrevented) {
                return;
            }
            this._enableField();
            
            if(typeof o.onEnable === 'function') {
                o.onEnable(field, self);
            }

            AcmsEvent.create(EVENT_ENABLED);
            AcmsEvent.dispatch(field, e);
        },

        listen: function() {
            var self = this, o = self.options;
            AcmsEvent.add(self.checkbox, 'click', function(e) {
                var target = e.target,
                    tagName = target.tagName.toLowerCase();
                
                if(tagName === 'input' && target.checked) {
                    self.disable(e);
                } else if(tagName === 'span' && Classes.hasClass(target, o.iconChecked)) {
                    Classes.removeClass(target, o.iconChecked);
                    Classes.addClass(target, o.iconUnchecked);
                    self.disable(e);
                } else if(tagName === 'span' && Classes.hasClass(target, o.iconUnchecked)) {
                    Classes.removeClass(target, o.iconUnchecked);
                    Classes.addClass(target, o.iconChecked);
                    self.enable(e);
                } else {
                    self.enable(e);
                }
            });
        },
        
        _disableField: function() {
            var field = this.field;
            field.disabled = true;
            field.setAttribute('disabled', 'disabled');
            field.setAttribute('aria-disabled', 'true');
        },
        
        _enableField: function() {
            var field = this.field;
            field.disabled = false;
            field.removeAttribute('disabled');
            field.setAttribute('aria-disabled', 'false');
        },
        
        _setup: function() {
            var self = this, o = self.options, element = self.element;
            self.field = Selectors.q(o.field, element);
            self.checkbox = Selectors.q(o.check, element);
            self.hidden = Selectors.q(o.hidden, element);
            if(self.checkbox.checked) {
                if(self.field.getAttribute('disabled') 
                        || self.field.disabled) {
                    self._enableField();
                }
            } else {
                if(!self.field.getAttribute('disabled')) {
                    self._disableField();
                }
            }
        }

    };

    return Check;
});
