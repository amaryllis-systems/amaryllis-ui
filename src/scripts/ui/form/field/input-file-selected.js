/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @copyright 2013 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright 2014 - 2018 Amaryllis Systems GmbH http://www.amws.eu/
 * @copyright 2017 - 2018 Amaryllis CMS Project http://www.amaryllis-cms.de/
 * @license https://www.amaryllis-cms.de/lizenzen/
 * @license Apache Licence 2.0 https://www.apache.org/licenses/LICENSE-2.0
 *
 * Dual-Licensed with Amaryllis CMS EULA. You may obtain a copy of the
 * EULA at
 *
 *     https://www.amaryllis-cms.de/lizenzen/eula/
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

define('ui/form/field/input-file-selected', [], function() {
    
    'use strict';
    
    var FileField = function(element, options) {
        this.element = 
        this.options = null;
        this.initialize(element, options);
    };
    
    FileField.needInit = true;
    
    FileField.init = function(element, options) {
        return new FileField(element, options);
    }
    
    FileField.prototype = {
        constructor: FileField,
        
        initialize: function(element, options) {
            var inputs = element.querySelectorAll( '.inputfile' );
            Array.prototype.forEach.call( inputs, function( input )
            {
                    var label	 = input.nextElementSibling,
                            labelVal = label.innerHTML;

                    input.addEventListener( 'change', function( e )
                    {
                            var fileName = '';
                            if( this.files && this.files.length > 1 )
                                    fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
                            else
                                    fileName = e.target.value.split( '\\' ).pop();

                            if( fileName )
                                    label.querySelector( 'span' ).innerHTML = fileName;
                            else
                                    label.innerHTML = labelVal;
                    });

                    // Firefox bug fix
                    input.addEventListener( 'focus', function(){ input.classList.add( 'has-focus' ); });
                    input.addEventListener( 'blur', function(){ input.classList.remove( 'has-focus' ); });
            });
        }
    };
    
    return FileField;
    
});