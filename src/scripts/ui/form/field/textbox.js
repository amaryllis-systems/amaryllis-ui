/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @copyright 2013 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright 2014 - 2018 Amaryllis Systems GmbH http://www.amws.eu/
 * @copyright 2017 - 2018 Amaryllis CMS Project http://www.amaryllis-cms.de/
 * @license https://www.amaryllis-cms.de/lizenzen/
 * @license Apache Licence 2.0 https://www.apache.org/licenses/LICENSE-2.0
 *
 * Dual-Licensed with Amaryllis CMS EULA. You may obtain a copy of the
 * EULA at
 *
 *     https://www.amaryllis-cms.de/lizenzen/eula/
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

/**
 * Textbox
 *
 * Erstellt eine Standard-Textbox (Input-Element)
 *
 * @param {Object} Utils
 * @param {Object} Classes
 * 
 * @returns {Textbox}
 */
define("ui/form/field/textbox", [
    "tools/utils",
    "core/classes"
], function(Utils, Classes) {

    "use strict";

    var Textbox = {
        
        defaultOptions: {
            value       : "",
            placeholder : "",
            class: "form-input",
            data: {
            },
            id : null
        },

        /**
         * Erstellt eine Textbox
         *
         * @param {Object} options Feld-Optionen
         * @returns {Element}
         */
        create: function(options) {
            if(typeof options !== "object") {
                throw new Error('Optionen sind erforderlich!');
            }
            this.options = Utils.extend({}, this.defaultOptions, options);
            var ele = document.createElement('input'), o = this.options;
            ele.type = 'text';
            ele.setAttribute('type', 'text')
            ele.setAttribute('value', o.value)
            Classes.addClass(ele, o.class);
            if(o.placeholder) {
                ele.setAttribute('placeholder', o.placeholder);
            }
            if(o.id) {
                ele.setAttribute('id', o.id);
                ele.id = o.id;
            }
            if(o.data.length) {
                var k, data = o.data;
                for(k in data) {
                    if (data.hasOwnProperty(k)) {
                        ele.setAttribute('data-'+k, data[k]);
                    }
                }
            }
            return ele;
        }
    };
    
    return Textbox;

});
