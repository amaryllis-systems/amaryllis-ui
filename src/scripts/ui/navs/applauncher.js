/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */

define("ui/navs/applauncher", [
    'tools/utils',
    'core/selectors',
    'core/classes',
    'events/event'
], function (Utils, Selectors, Classes, AcmsEvent) {

    "use strict";

    /**
     * Constructor
     * 
     * @param {Object} element  Das AppLauncher Element
     * @param {Object} options  Die eigenen Optionen
     * 
     * @returns {AppLauncher}
     */
    var AppLauncher = function (element, options) {
        this.element =
        this.options =
        this.toggle =
        this.apps = null;
        this.initialize(element, options);
    };

    /**
     * Komponenten Name
     *
     * @var {String}
     */
    AppLauncher.MODULE = "AppLauncher Navigation";

    /**
     * Komponenten Version
     *
     * @var {String}
     */
    AppLauncher.VERSION = "1.5.0";
    
    /**
     * Namespace
     * 
     * @var {String}
     */
    AppLauncher.NS = "acms.ui.navs.applauncher";

    /**
     * Standard-Optionen
     *
     * @var {Object}
     */
    AppLauncher.DEFAULT_OPTIONS = {
        active: 'active',
        trigger: 'click',
        toggle: '.launcher-button'
    };

    AppLauncher.css = '/media/css/navs/applauncher.min.css';

    AppLauncher.needInit = true;

    /**
     * Externe Initialisierung
     * 
     * @param {Object} element  Das AppLauncher Element
     * @param {Object} options  Die AppLauncher Optionen
     * @returns {AppLauncher}
     */
    AppLauncher.init = function (element, options) {
        var al = new AppLauncher(element, options);
        al.element.acmsData(AppLauncher.NS, al);

        return al;
    };

    AppLauncher.prototype = {
        constructor: AppLauncher,
        /**
         * Interner Constructor
         * 
         * @param {Object} element  Das App-Launcher Element
         * @param {Object} options  Die Optionen des Constructor
         * 
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this.toggle = Selectors.q(this.options.toggle, element);
            this.listen();
        },

        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {AppLauncher.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return AppLauncher.DEFAULT_OPTIONS;
        },
        /**
         * Bildet die Optionen
         *
         * <p>
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         * </p>
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function (options) {
            return Utils.extend(
                    {},
                    this.getDefaultOptions(),
                    options,
                    this.element.acmsData()
                    );
        },

        listen: function() {
            var self = this, o = self.options, cb = function(e) {
                e && e.preventDefault();
                Classes.toggleClass(self.toggle, o.active);
                Classes.toggleClass(self.element, o.active);
            };
            AcmsEvent.add(self.toggle, 'click', cb);
        }

    };

    return AppLauncher;
    
});
