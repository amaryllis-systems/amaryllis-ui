/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */

/**
 * Nav Toggle Hilfstool
 * 
 * Hilfs-Tool für die Navigation 'horizontal-menu-3'
 * 
 * @param {Object} Utils
 * @param {Object} Selectors
 * @param {Object} Classes
 * @param {Object} Media
 * @param {Object} AcmsEvent
 * @param {Object} A
 * @returns {Nav}
 */
define('ui/navs/horizontal-menu', [
    'tools/utils',
    'core/selectors',
    'core/classes',
    'events/event',
    'device/match-media',
    'core/acms'
], function (Utils, Selectors, Classes, AcmsEvent, Media, A) {

    "use strict";

    /**
     * Constructor
     * 
     * @param {Element}     element     Das HTML-Element
     * @param {Object}      options     Die Optionen
     * @returns {Nav}
     */
    var Nav = function (element, options) {
        this.element =
                this.options =
                this.toggle = null;
        this.options = Nav.DEFAULT_OPTIONS;

        this.initialize(element, options);
    };

    /**
     * Modul Name
     * 
     * @var {String}
     */
    Nav.MODULE = "Horizontales Menu";

    /**
     * Modul Namespace
     * 
     * @var {String}
     */
    Nav.NS = "acms.ui.navs.horizontal-menu";

    /**
     * Standard-Optionen des Moduls
     * 
     * @var {Object}
     */
    Nav.DEFAULT_OPTIONS = {
        toggle: '.hamburger',
        toggleClass: 'active',
        dimmer: '.dimmer',
        nav: '.menu',
        submenuClass: 'submenu',
        menuItem: 'li'
    };

    /**
     * Sagt dem Modul-Loader, dass das Modul initialisiert werden muss, wenn es 
     * über diesen geladen wird.
     * 
     * @var {Boolean}
     */
    Nav.needInit = true;

    /**
     * Modul Initialisierung
     * 
     * @param {Element}     element     Das HTML-Element
     * @param {Object}      options     Die Optionen
     * @returns {Nav}
     */
    Nav.init = function (element, options) {
        var v = new Nav(element, options);
        v.element.acmsData(Nav.NS, v);

        return v;
    };

    /**
     * Klassen Definition
     */
    Nav.prototype = {

        constructor: Nav,

        /**
         * Interner Constructor
         * 
         * @param {Element} element     HMTL Element
         * @param {Objct}   options     Eigene Optionen
         * 
         * 
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },

        /**
         * Aufsetzen der Event-Listener
         * 
         */
        listen: function () {
            var self = this,
                    o = self.options,
                    element = self.element, j, icn = self.icons, l = icn.length;
            if(self.di) {
                AcmsEvent.add(self.di, 'click', self.toggleMobile.bind(self));
            }
            if(self.hb) {
                AcmsEvent.add(self.hb, 'click', self.toggleMobile.bind(self));
            }
            for(j = 0; j < l; j++) {
                AcmsEvent.add(icn[j], 'click', self.toggleSubnav.bind(self));
            }
        },

        /**
         * Gibt die Standard-Optionen zurück
         * 
         * @returns {Nav.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return Nav.DEFAULT_OPTIONS;
        },

        /**
         * Bilden der Optionen
         * 
         * Bildet die Optionen basierend auf den Standard-Optionen, den 
         * übergebenen Optionen des Constructors und den Element Data-Attributen. 
         * Letztere gewinnen den Kampf, wenn gesetzt.
         * 
         * @param {Object} options    Die eignenen Optionen
         * @returns {Object|Nav.DEFAULT_OPTIONS}
         */
        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },
        
        /**
         * Toggelt das Menu
         * 
         * @param {Event} e
         * 
         */
        toggleMobile: function(e) {
            var self = this;
            if(self.hb) {
                Classes.toggleClass(self.hb, 'active');
            }
            if(self.di) {
                Classes.toggleClass(self.di, 'active');
            }
            if(self.na) {
                Classes.toggleClass(self.na, 'active');
            }
            self.state = (self.na && Classes.hasClass(self.na));
            Classes.toggleClass(document.body, 'no-scrolling');
            self._update();
        },
        
        toggleSubnav: function(e) {
            var self = this, 
                o = self.options,
                target = e.target,
                parent = Selectors.closest(target, o.menuItem),
                submenu = Selectors.q(o.submenuClass, target);
            Classes.toggleClass(parent, 'showSubmenu');
            if(Classes.hasClass(parent, 'showSubmenu')) {
                submenu.setAttribute('aria-expanded', 'true');
            } else {
                submenu.setAttribute('aria-expanded', 'false');
            }
        },
        
        onMouseEnter: function(e) {
            if(Media.matches('only screen and (min-width: 768px)')) {
                var self = this,
                    o = self.options,
                    item = e.target;
                if(item.tagName.toLowerCase() !== o.menuItem) {
                    item = Selectors.closest(item, o.submenuItem);
                }
                var submenu = Selectors.q(o.submenuClass, item);
                if(submenu) {
                    submenu.setAttribute('aria-expanded', 'true');
                }
            }
        },
        
        onMouseLeave: function(e) {
            if(Media.matches('only screen and (min-width: 768px)')) {
                var self = this,
                    o = self.options,
                    item = e.target;
                if(item.tagName.toLowerCase() !== o.menuItem) {
                    item = Selectors.closest(item, o.submenuItem);
                }
                var submenu = Selectors.q(o.submenuClass, item);
                if(submenu) {
                    submenu.setAttribute('aria-expanded', 'false');
                }
            }
        },
        
        /**
         * aktualisiert die aria-hidden attribute
         * 
         */
        _update: function() {
            var self = this, isActive = self.state;
            if(Media.matches('only screen and (max-width: 767px)')) {
                
                self.na.setAttribute('aria-hidden', (isActive ? 'false' : 'true'));
                self.hb.setAttribute('aria-hidden', (isActive ? 'true' : 'false'));
                self.di.setAttribute('aria-hidden', (isActive ? 'false' : 'true'));
            } else {
                if(self.hb) {
                    self.na.setAttribute('aria-hidden', 'true');
                }
                if(self.na) {
                    self.na.setAttribute('aria-hidden', (isActive ? 'false' : 'true'));
                }
                if(self.di) {
                    self.di.setAttribute('aria-hidden', 'true');
                }
            }
        },

        /**
         * Vorbereiten des Moduls
         * 
         * 
         */
        _prepare: function () {
            var self = this,
                    element = self.element,
                    o = self.options, j;
            self.hb = Selectors.q(o.toggle, element);
            self.di = Selectors.q(o.dimmer, element);
            self.na = Selectors.q(o.nav, element);
            self.state = (self.na && Classes.hasClass(self.na, o.toggleClass));
            if(!self.na.id) {
                self.na.id = Utils.generateUniqeId();
                self.hb.setAttribute('aria-controls', self.na.id);
                self.hb.setAttribute('aria-label', A.Translator._('Navigation öffnen'));
                self.di.setAttribute('aria-controls', self.na.id);
                self.di.setAttribute('aria-label', A.Translator._('Navigation schließen'));
            }
            self.icons = Selectors.qa('li i.icon-arrow', element);
            self.subs = Selectors.qa(o.submenuClass, element);
            for(j = 0; j < self.subs.length; j++) {
                if(!self.subs[j].getAttribute('id')) {
                    self.subs[j].setAttribute('id', Utils.generateUniqeId());
                }
                var parent = Selectors.closest(self.subs[j], o.menuItem);
                var icn = Selectors.q('i.icon-arrow', parent);
                icn.setAttribute('aria-controls', self.subs[j].getAttribute('id'));
                icn.setAttribute('aria-controls', A.Translator._('Unternavigation zeigen'));
            }
            self._update();
        }

    };

    return Nav;

});

