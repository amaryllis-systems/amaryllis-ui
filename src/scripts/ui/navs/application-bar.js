/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */

define('ui/navs/application-bar', [
    'core/core',
    'tools/utils',
    'events/event',
    'core/selectors',
    'core/classes',
    'apps/ui/helpers/fade',
    'apps/ui/helpers/show-hide',
    'libs/domReady'
], function (Core, Utils, AcmsEvent, Selectors, Classes, Fade, ShowHide, domReady) {

    "use strict";

    var ApplicationBar = function (element, options) {
        this.element =
                this.$element =
                this.options =
                this.showOnFlex =
                this.allBarItems =
                this.navsParent =
                this.mobileNav = null;
        this.initialize(element, options);
    };

    ApplicationBar.MODULE = "Application-Bar";

    ApplicationBar.VERSION = "1.5.0";

    ApplicationBar.DEFAULT_OPTIONS = {
        mobileToggle: '.bar-toggle',
        flexstyle: "bar-nav",
        mobileNav: "mobile-nav,hidden,black,white-text",
        mobileMenu: 'mobile-navbar,hidden,black,white-text',
        flexclean: false,
        flextolerance: 3
    };

    ApplicationBar.needInit = true;

    ApplicationBar.init = function (element, options) {
        var bar = new ApplicationBar(element, options);

        return bar;
    };

    ApplicationBar.css = "media/css/navs/application-bar.min.css";

    /**
     * Klassen Definition
     */
    ApplicationBar.prototype = {
        /**
         * Constructor
         */
        constructor: ApplicationBar,
        /**
         * Interner Constructor
         * 
         * @param {Object} element  Das HTML Element
         * @param {Object} options  Optionen
         * 
         * 
         */
        initialize: function (element, options) {
            this.$element = Core.$(element);
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            if (this.navLists.length > 0 && !(Classes.hasClass(element, 'static-item'))) {
                //this._buildBar();
            }
            // console.log('Flex-Menu: ' + this.isFlexMenu);
        },
        /**
         * Gibt die Standard-Optionen zurück
         * 
         * @returns {ApplicationBar.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return ApplicationBar.DEFAULT_OPTIONS;
        },
        /**
         * Bilden der aktuellen Optionen
         * 
         * @param {Object|NULL} options
         * @returns {@this;@call;_doExtend|Object}
         */
        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },
        /**
         * Aufsetzen der Event-Listener
         * 
         * 
         */
        listen: function () {
            var self = this,
                cb = function (event) {
                    event.preventDefault();
                    return self._toggleMobile();
                },
                resize = function (e) {
                    self.resize(e);
                };
            AcmsEvent.add(self.mobileToggle, "click", cb);
            AcmsEvent.add(window, 'resize', resize);
            AcmsEvent.add(window, 'orientationchange', resize);
            AcmsEvent.add(window, 'load', resize);

            var srcs = Selectors.qa('[src]', self.element), i;
            for (i = 0; srcs.length < i; i++) {
                AcmsEvent.add(srcs[i], 'load', resize);
            }
            if (this.applicationLauncher) {
                var cb2 = function (event) {
                    // console.log('clicked')
                    event.preventDefault();
                    event.stopPropagation();
                    self._toggleApplicationLauncher(event);
                    return false;
                };
                AcmsEvent.add(this.applicationLauncher, 'click', cb2);
            }

        },
        
        _calculateBarWidth: function() {
            var navsParentWidth = parseInt(this.navsParent.clientWidth);
            var allChilds = this.element.children, i;
            // console.log(allChilds);
            for(i =0; i < allChilds.length; i++) {
                var child = allChilds[i];
                if(Classes.hasClass(child, 'bar-nav')) {
                    continue;
                }
                if(Classes.hasClass(child, 'mobile-toggle')) {
                    continue;
                }
                if(Classes.hasClass(child, 'mobile-nav')) {
                    continue;
                }
                navsParentWidth -= parseInt(child.outerWidth);
            }
            this.width = navsParentWidth;
        },
        
        _recalculateSpace: function () {
            var self = this;
            var navsParentWidth = 0, childrenWidth = 0, children;
            var freeSpace;
            navsParentWidth = parseInt(this.element.offsetWidth);
            children = Selectors.qa('.bar-item,.bar-nav:not(.mobile-navbar) > li:not(.mobile-nav-item)', this.element);
            // console.log(children);
            var leftFloatingChilds = [];
            var rightFloatingChilds = [];
            var noFloatingChilds = [];
            var floatState, i, len = children.length, style, child;
            for (i = 0; i < len; i++) {
                child = children[i];
                if(! Selectors.isVisible(child)) {
                    continue;
                }
                style = child.style;
                if('cssFloat' in style) {
                    floatState = style['cssFloat'];
                } else if('styleFloat' in style) {
                    floatState = style['styleFloat'];
                } else {
                    floatState = '';
                }
                switch (floatState) {
                    case "left":
                        leftFloatingChilds.push(child);
                        break;
                    case "right":
                        rightFloatingChilds.push(child);
                        break;
                    default:
                        noFloatingChilds.push(child);
                }
            }
            rightFloatingChilds.reverse();
            children = new Array();
            children = leftFloatingChilds.concat(noFloatingChilds, rightFloatingChilds);
            //children = Core.$(children);
            //childrenWidth += parseInt(children[0].style.marginLeft || 0);
            var len = children.length, style, child;
            for (i = 0; i < len; i++) {
                child = children[i];
                if(! Selectors.isVisible(child) || Classes.hasClass(child, 'hidden')) {
                    continue;
                }
                // console.log(child)
                childrenWidth += parseInt(child.clientWidth);
                var nextChild = children[i + 1];
                if (nextChild) {
                    style = child.style;
                    /*
                    childrenWidth += Math.max(
                            parseInt(style.marginRight || 0),
                            parseInt(nextChild.style.marginLeft || 0));*/
                }
                // console.log(childrenWidth);
            }
            var lastChild = children[len - 1];
            if(lastChild) {
            //    childrenWidth += parseInt(lastChild.style.marginRight || 0);
            }
            freeSpace = navsParentWidth - childrenWidth;
            self.freeSpace = freeSpace;
            self.childrenWidth = childrenWidth;
            self.navsParentWidth = navsParentWidth;
            // console.log("freeSpace " + self.freeSpace);
            // console.log("childrenWidth " + self.childrenWidth);
            // console.log("navsParentWidth " + self.navsParentWidth);
            // console.log(navsParentWidth);
            // console.log('freeSpace ' + self.freeSpace)
            return freeSpace;
        },
        moveNavItemBack: function (menu, child) {
            var flexChildren;
            try {
                flexChildren = Selectors.qa('li', menu).filter(function (element, index, array) {
                    return parseInt(element.getAttribute("data-orderorigin")) < parseInt(child.getAttribute("data-orderorigin"));
                });
            } catch(E) {
                // console.log(E);
                // console.log(menu);
                return;
            }
            if (flexChildren.length > 0) {
                menu.appendChild(child);
            } else {
                flexChildren = Selectors.qa('li', menu).filter(function (element, index, array) {
                    return parseInt(element.getAttribute("data-orderorigin")) > parseInt(child.getAttribute("data-orderorigin"));
                });
                if (flexChildren.length > 0) {
                    menu.insertBefore(child, flexChildren[0]);
                } else {
                    menu.appendChild(child);
                }
            }
        },
        /**
         * 
         * @param {String} direction
         * @returns {Boolean}
         */
        moveNavItem: function (direction) {
            var self = this;
            direction = direction || "toMobileNav";
            if (direction === "toMobileNav") {
                var num = self.allBarItems.length;
                while(num-- ) {
                    var nextToHide = self.allBarItems[num];
                    /*
                    if (nextToHide.length === 0) {
                        return false;
                    }*/
                    if (Classes.hasClass(nextToHide, 'mobile-nav-item')) {
                        continue;
                    }
                    
                    var topMenu = Selectors.closest(nextToHide, 'ul'),
                        topMenuIndex = self.showOnFlex.indexOf(topMenu),
                        mobileNavs = Selectors.qa('ul', self.mobileNav),
                        mobileNavBar = mobileNavs[topMenuIndex];
                    //// console.log(topMenuIndex);
                    //// console.log(self.mobileNav);
                    //// console.log(mobileNavBar);
                    self.moveNavItemBack(mobileNavBar, nextToHide);
                    Classes.addClass(nextToHide, "mobile-nav-item");
                    Classes.removeClass(mobileNavBar, "hidden");
                    ShowHide.show(mobileNavBar);
                    var children = Selectors.qa('li', topMenu);
                    if (children.length === 0) {
                        Classes.addClass(topMenu, "hidden");
                    }
                    ShowHide.show(self.mobileToggle);
                }
                
                return nextToHide;
            } else if (direction === "fromMobileNav") {
                var num = self.allBarItems.length;
                while(num-- ) {
                    var nextToShow = self.allBarItems[num];
                    //Core.$(self.allBarItems).filter(".mobile-nav-item").first();
                    if (!Classes.hasClass(nextToShow, 'mobile-nav-item')) {
                        continue;
                    }
                    //var mobileNavBar = Core.$(nextToShow).parent();
                    //var topMenuIndex = Core.$(mobileNavBar).index();
                    //var topMenu = Core.$(self.showOnFlex).eq(topMenuIndex);
                    
                    var mobileNavBar = Selectors.closest(nextToShow, 'ul');
                    var mobileNavs = Selectors.qa('ul', self.mobileNav);
                    var topMenuIndex = mobileNavs.indexOf(mobileNavBar);
                    var topMenu = self.showOnFlex[topMenuIndex];
                    Classes.removeClass(topMenu, 'hidden');
                    //Core.$(topMenu).removeClass("hidden");
                    Classes.removeClass(nextToShow, 'mobile-nav-item');
                    //Core.$(nextToShow).removeClass("mobile-nav-item");
                    self.moveNavItemBack(topMenu, nextToShow);
                    var mobileChildren = Selectors.qa('li', mobileNavBar);
                    if(mobileChildren.length === 0) {
                        Classes.addClass(mobileNavBar, 'hidden');
                    }
                    /*
                    if (Core.$(mobileNavBar).children().length === 0) {
                        Core.$(mobileNavBar).addClass("hidden").hide();
                    }*/
                    var mobileNavBars = Selectors.qa('.mobile-navbar:not(.hidden)');
                    if(mobileNavBars.length === 0) {
                        Classes.addClass(self.mobileNav, 'hidden');
                        ShowHide.hide(self.mobileToggle);
                    }
                    /*
                    if (Core.$(self.mobileNav).children(".mobile-navbar").not(".hidden").length === 0) {
                        Core.$(self.mobileNav).hide().addClass("hidden");
                        Core.$(self.mobileToggle).hide();
                    }
                    if (nextToShow.length === 0) {
                        return false;
                    }*/
                    return nextToShow;
                }
            }
        },
        checkBarItems: function () {
            var self = this,
                o = this.options,
                i;
            var forceEndLoop = false, length = self.allBarItems.length;
            // console.log('Flex Tolerance');
            // console.log(o.flextolerance);
            for (i = 0; i < length; i++) {
                self._recalculateSpace();
                var freeSpace = self.freeSpace;
                if (freeSpace < o.flextolerance || o.flexclean) {
                    if (!(self.moveNavItem("toMobileNav"))) {
                        // console.log('Breaking toMobileNav');
                        break;
                    } else {
                        if (!forceEndLoop) {
                            // console.log('forceEndLoop toMobileNav');
                            continue;
                        }
                    }
                } else {
                    if (!(self.moveNavItem("fromMobileNav"))) {
                        // console.log('Breaking fromMobileNav');
                        break;
                    } else {
                        forceEndLoop = true;
                        // console.log('forceEndLoop fromMobileNav');
                        continue;
                    }
                }
                break;
            }
        },
        resize: function () {
            var self = this;
            if (self.isFlexMenu) {
                self.checkBarItems();
            }
        },
        _buildBar: function () {
            var self = this,
                ele = this.element;
            self._buildFlexMenu();
            if (self.showOnFlex.length <= 0) {
                return;
            }
            self._processFlexMenu();
            self.navsParent = self.navLists[0].parentNode;
            self._buildMobileToggle();
            self._buildMobileNav();
            var clearfix = document.createElement('div');
            Classes.addClass(clearfix, 'clearfix,hidden');
            self.element.appendChild(clearfix);
            self.element.appendChild(self.mobileNav);
            self._calculateBarWidth();
            self.checkBarItems();
            self.listen();
        },
        _buildFlexMenu: function () {
            var self = this, i;
            self.showOnFlex = new Array();
            for (i = 0; i < self.navLists.length; i++) {
                var itm = self.navLists[i];
                if (!(Classes.hasClass(itm, 'static-item'))) {
                    self.showOnFlex.push(itm);
                }
            }
        },
        
        _buildMobileToggle: function() {
            var self = this, element = this.element;
            if (!(self.mobileToggle)) {
                self.mobileToggle = document.createElement('div');
                Classes.addClass(self.mobileToggle, 'bar-toggle,mobile-toggle');
                element.appendChild(self.mobileToggle);
            }
        },
        
        _buildMobileNav: function() {
            var self = this, o = this.options, i;
            self.mobileNav = document.createElement('nav');
            Classes.addClass(self.mobileNav, o.mobileNav + ',flexstyle-' + o.flexstyle);
            for (i = 0; i < self.showOnFlex.length; i++) {
                var list = document.createElement('ul');
                Classes.addClass(list, o.mobileMenu + ',' + o.flexstyle);
                self.mobileNav.appendChild(list);
            };
        },
        
        _processFlexMenu: function() {
            var self = this, flexVisible, menuEntries, i, j;
            self.isFlexMenu = true;
            self.showOnFlex.sort(function (a, b) {
                var aVal = (parseInt(a.acmsData("order")) || self.showOnFlex.indexOf(a) + 1);
                var bVal = (parseInt(b.acmsData("order")) || self.showOnFlex.indexOf(b) + 1);
                return aVal - bVal;
            });
            for (i = 0; i < self.showOnFlex.length; i++) {
                flexVisible = self.showOnFlex[i];
                menuEntries = Selectors.qa('li', flexVisible);
                for (j = 0; j < menuEntries.length; j++) {
                    var that = menuEntries[j], index = menuEntries.indexOf(that);
                    that.setAttribute("data-orderorigin", index);
                    if (!(that.hasAttribute("data-order"))) {
                        that.setAttribute("data-order", index + 1);
                    }
                }
                ;
                menuEntries.sort(function (a, b) {
                    var aVal = parseInt(a.acmsData("order"));
                    var bVal = parseInt(b.acmsData("order"));
                    return aVal - bVal;
                });
                if (flexVisible.hasAttribute("data-direction")) {
                    if (flexVisible.getAttribute('data-direction') == 'reverse')
                        menuEntries.reverse();
                }
                for (j = 0; j < menuEntries.length; j++) {
                    var that = menuEntries[j];
                    if (!(Classes.hasClass(that, 'static-item'))) {
                        self.allBarItems.push(that);
                    }
                }
            };
        },
        
        _toggleApplicationLauncher: function (event) {
            var launcher = (this.applicationLauncher.acmsData('target')
                    ? this.element.querySelector(this.applicationLauncher.acmsData('target'))
                    : this.element.querySelector('.application-bar-launcher'));
            if (launcher) {
                var active = Classes.hasClass(launcher, 'active');

                if (!this.activeLauncher) {
                    ShowHide.show(launcher);
                    this.activeLauncher = true;
                } else {
                    ShowHide.hide(launcher);
                    launcher.toggleClass('active');
                    this.activeLauncher = false;
                }
            }
        },
        _toggleMobile: function () {
            var self = this, o = this.options;
            var mobileToggle = this.element.querySelector(o.mobileToggle);
            if(Classes.hasClass(self.mobileNav,'showing')) {
                Classes.removeClass(self.mobileNav,'showing');
                Classes.addClass(self.mobileNav,'hiding');
            } else {
                Classes.addClass(self.mobileNav,'showing');
                Classes.removeClass(self.mobileNav,'hiding,hidden');
            }
            /*
            if (Core.$(self.mobileNav).is(":hidden")) {
                Core.$(self.mobileNav).show();
                Core.$(self.mobileNav).find(".mobile-navbar")
                        .hide().not(".hidden").slideDown("fast");
            } else {
                Core.$(self.mobileNav).find(".mobile-navbar")
                        .not(".hidden").show().slideUp("fast", function () {
                    Core.$(self.mobileNav).hide();
                });
            }*/
        },
        _prepare: function () {
            var element = this.element, o = this.options;
            this.applicationLauncher = element.querySelector('[data-toggle="application-launcher"]');
            this.mobileToggle = element.querySelector(o.mobileToggle);
            this.allBarItems = new Array();
            this.activeLauncher = false;
            this.lastFlexAction = undefined;
            this.isFlexMenu = false;
            this.navLists = Selectors.qa('.bar-nav', element);
        }
    };

    return ApplicationBar;

});
