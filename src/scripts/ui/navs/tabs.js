/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */

/**
 * Tab Komponente
 *
 * @param {type} Utils
 * @param {type} Classes
 * @param {type} Selectors
 * @param {type} ShowHide
 * @param {type} Fade
 * @param {type} AcmsEvent
 * @param {type} Win
 * @param {type} A
 * @returns {Tabs}
 */
define("ui/navs/tabs", [
    'tools/utils',
    'core/classes',
    'core/selectors',
    'apps/ui/helpers/show-hide',
    'apps/ui/helpers/fade',
    'events/event',
    'core/window',
    'core/acms'
], function (Utils, Classes, Selectors, ShowHide, Fade, AcmsEvent, Win, A) {

    "use strict";

    /**
     * Constructor
     * 
     * Die Tab-Komponente stellt den JavaScript-seitigen Part für Tabs des CSS
     * Cores dar. Die Tabs werden auf Ihrem Eltern-Element (meistens eine
     * `UL` oder `OL` Liste) initiert und gemeinsam als Kollektion verwaltet. 
     * Tabs bestehen im Allgemeinen aus den entsprechenden `LI` Kindern der 
     * Eltern-Liste. Jeder Tab sollte ein Anchor-Element (`a`) beherbergen mit 
     * entweder dem `data-target` Attribut, welches das Ziel des Tabs (den 
     * Tab-Container als eindeutige ID, z.B. `data-target="#mein-tab-content"`) 
     * enthält oder entsprechend das `href`-Attribut gesetzt haben. Das 
     * `data`-Attribut wird hierbei bevorzugt!.
     * 
     * ### Aktivieren von Tabs
     * 
     * Die Standard-CSS Klasse für aktive Tabs ist `active`. Zum Ändern der CSS-
     * Klasse (muss bei der Initialisierung geschehen, nicht im nachhinein!) kann
     * man die Option `active` mit einer alternativen Klasse übergeben.
     * 
     * ``` js
     * var MyTabs = new Tabs(meinEle, {active: "aktiver-tab"})
     * ```
     * 
     * ### Erlauben und verbieten von Tabs
     * 
     * Tabs können erlaubt oder verboten werden, indem Sie die CSS-Klasse
     * "disabled" gesetzt bekommen. Möchte man eine alternative Klasse verwnden,
     * lässt sich die CSS-Klasse durch übergeben der Option "disabled" setzen.
     * 
     * ``` html
     * <li class="tab disabled" aria-disabled="true">
     * <a></a>
     * </li>
     * ```
     * 
     * Zum Ändern der CSS-Klasse (muss bei der Initialisierung geschehen, nicht im
     * nachhinein!):
     * 
     * ``` javascript
     * var MyTabs = new Tabs(meinEle, {disabled: "verboten"})
     * ```
     * 
     * @param {Element}      element Das Element auf dem die Tabs gebildet werden
     * @param {Object|NULL} options Die Optionen
     *
     * @returns {Tabs}
     * 
     * @exports ui/navs/tabs
     */
    var Tabs = function (element, options) {
                this.options =
                this.element =
                this.$tabs =
                this.length = null;
        this.initialize(element, options);
    };

    /**
     * Komponenten Name
     *
     * @var {String}
     */
    Tabs.MODULE = "Tabs";

    Tabs.NS = "acms.ui.navs.tabs";
    
    Tabs.EVENT_SHOW = Tabs.NS + '.show';
    
    Tabs.EVENT_SHOWN = Tabs.NS + '.shown';
    
    Tabs.EVENT_HIDE = Tabs.NS + '.hide';
    
    Tabs.EVENT_HIDDEN = Tabs.NS + '.hidden';
    
    Tabs.EVENT_REFRESH = Tabs.NS + '.refresh';
    
    Tabs.EVENT_REFRESHED = Tabs.NS + '.refreshed';

    /**
     * Standard-Optionen
     * =========================
     *
     * active:          {String}        CSS Klasse für aktive Tabs (geöffnet)
     *                                  Standard: 'active'
     * disabled:        {String}        CSS Klasse für deaktivierte/verbotene 
     *                                  Tabs. Tabs mit dieser Klasse können
     *                                  nicht geklickt/geöffnet werden.
     * trigger:         {String}        Trigger für die Tabs. Der Standard-
     *                                  Trigger ist "click". Kann zum Beispiel
     *                                  auch "click hover" für click und hover
     *                                  oder nur "hover" sein.
     *                                  checked List Group Items
     * tab:             {Selector}      CSS Selector für die tabs. 
     *                                  Standard: '.tab'
     * panes:           {Selector}      CSS-selector für die Tab-Panes.
     *                                  Standard: '.tab-panel'
     * effect:          {String}        Ein Transition Effect. Kann 'fade', 
     *                                  'flip', 'scaleUp', 'slideLeft' oder
     *                                  'scale' sein
     * history:         {Boolean}       Soll die Browser-History aktualisisert
     *                                  werden? Steht der Wert auf TRUE wird die
     *                                  Browser-History mit dem jeweils
     *                                  aktuellen Tab aktualisiert. FALSE
     *                                  schaltet das Feature aus.
     *                                  
     * @var {Object}
     */
    Tabs.DEFAULT_OPTIONS = {
        active: 'active',
        disabled: 'disabled',
        panes: '.tab-panel',
        panesContainer: '.tab-content',
        trigger: 'click',
        tab: '.tab',
        autorotate: false,
        pauseonhover: true,
        effect: 'flipInX',
        outeffect: 'fadeOutUp',
        history: false,
        css: "media/css/ui/tabs",
        themecss: false
    };
    /**
     * Module Loader
     */
    /**  **/
    Tabs.needInit = true;
    /** Module Initialisator **/
    Tabs.init = function (element, options) {
        var t = new Tabs(element, options);
        t.element.acmsData(Tabs.NS, t);

        return t;
    };

    /**
     * Klassen Definitionen
     */
    Tabs.prototype = {
        /**
         * Constructor
         *
         * @version {Tabs}
         */
        constructor: Tabs,
        /**
         * Interner Constructor
         *
         * @param {Object} element
         * @param {Object} options
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            
            this.$paneContainer = element.querySelector(this.options.panesContainer) || Selectors.closest(element, '.tab-container').querySelector(this.options.panesContainer);
            try {
                this._load();
            } catch(E) {
                if(E && E.message) {
                    A.Logger.logWarning(E.message);
                }
            }
            this.listen();
        },
        
        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {Tabs.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return Tabs.DEFAULT_OPTIONS;
        },
        /**
         * Bildet die Optionen
         *
         * Basierend auf den Standard-Optionen, den übergebenen Optionen des
         * Constructors und den Element Data-Attributen werden die aktuellen
         * Konfigurationen gebildet. Letztenendes gewinnen die Element
         * Data-Attribute.
         *
         * @param {type} options
         *
         * @returns {Object}
         */
        buildOptions: function (options) {
            var o = Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
            var effect = o.effect && o.effect.trim(), outeffect = o.outeffect && o.outeffect.trim();
            if(effect === '' || effect === '0' || effect === 'false') {
                o.effect = false;
            }
            if(outeffect === '' || outeffect === '0' || outeffect === 'false') {
                o.outeffect = false;
            }
            if(o.css === '' || o.css === '0' || o.css === 'false') {
                o.css = false;
            }
            return o;
        },
        /**
         * Setter/Getter für die Optionen
         * 
         * @param {String|NULL} a   Der Optionen-Schlüssel oder NULL
         * @param {Mixed}       b   Undefined oder der Wert für a
         * @returns {Tabs.options|Boolean|Mixed|Tabs.prototype}
         */
        option: function (a, b) {
            if (!a) {
                return this.options;
            }
            if (typeof b === "undefined") {
                return this.options[a] !== undefined ? this.options[a] : false;
            }
            this.options[a] = b;
            return this;
        },

        /**
         * Liest alle Tabs neu ein
         *
         * Durch aufrufen des Refreshes werden alle Tabs und Tab-Panes neu
         * eingelesen und die Indizierung neu gebildet.
         *
         * 
         */
        refresh: function() {
            var e = AcmsEvent.createCustom(Tabs.EVENT_REFRESH);
            AcmsEvent.dispatch(this.element, e);
            if(e.defaultPrevented) {
                return;
            }
            this._load();
            var e = AcmsEvent.createCustom(Tabs.EVENT_REFRESHED);
            AcmsEvent.dispatch(this.element, e);
        },
        /**
         * Gibt einen Tab nach index zurück
         *
         * Sucht einen Tab basierend auf seinem Index und gibt den Tab zurück.
         * Wird der Tab nicht gefunden, wird FALSE zurückgegeben.
         *
         * @param {Integer} i   Der Tab-Index des gesuchten Tab
         * @returns {Array|Boolean}
         */
        get: function(i) {
            if(this.$tabs[i]) {
                return this.$tabs[i];
            }
            return false;
        },
        /**
         * Erlaubt einen "verbotenen" Tab
         * 
         * Entfern die "disabled" CSS Klasse aus den Optionen
         * 
         * @param {Number} i
         * @returns {Boolean|Tabs.prototype}
         */
        enable: function (i) {
            var oTab = this.get(i), self = this, o = self.options;
            if(false === oTab) {
                return false;
            }
            oTab['disabled'] = false;
            Classes.removeClass(oTab['tab'], o.disabled);
            oTab['tab'].setAttribute('aria-disabled', false);
            oTab['panel'].setAttribute('aria-disabled', false);
            return this;
        },

        /**
         * Verbietet einen "erlaubten" Tab
         *
         * Setzt die "disabled" CSS Klasse aus den Optionen
         *
         * @param {Number} i
         * @returns {Boolean|Tabs.prototype}
         */
        disable: function (i) {
            var oTab = this.get(i), self = this, o = self.options;
            if(false === oTab) {
                return false;
            }
            oTab['disabled'] = true;
            Classes.addClass(oTab['tab'], o.disabled);
            oTab['tab'].setAttribute('aria-disabled', 'true');
            oTab['panel'].setAttribute('aria-disabled', 'true');

            return this;
        },
        /**
         * Zeigt einen Tab nach Index
         *
         * Aktiviert einen neuen Tab und deaktiviert den akuell sichtbaren Tab
         *
         * @param {Number} i    Der Index
         * @returns {Boolean|undefined}
         */
        show: function (i) {
            var otab = this.get(i),
                self = this, o = this.options;
            if (false === otab) {
                return false;
            }
            var tab = otab['tab'];
            if (Classes.hasClass(tab, o.disabled) || Classes.hasClass(tab, o.active)) {
                return false;
            }
            var ci = this.current, c = this.get(ci);
            this.current = i;
            var
                hideEvent = AcmsEvent.createCustom(Tabs.EVENT_HIDE, {
                    relatedTarget: tab
                }),
                showEvent = AcmsEvent.createCustom(Tabs.EVENT_SHOW, {
                    relatedTarget: c['tab']
                });
            AcmsEvent.dispatch(self.element, hideEvent);
            AcmsEvent.dispatch(self.element, showEvent);
            if (showEvent.defaultPrevented || hideEvent.defaultPrevented) {
                return false;
            }
            self._deactivate(c, function () {
                var e = AcmsEvent.createCustom(Tabs.EVENT_HIDDEN, {relatedTarget: tab});
                AcmsEvent.dispatch(self.element, e);
            });
            self._activate(otab, function () {
                var e = AcmsEvent.createCustom(Tabs.EVENT_SHOWN, {
                    relatedTarget: c
                });
                AcmsEvent.dispatch(self.element, e);
            });
            return false;
        },
        /**
         * Zeigt den nächsten Tab
         *
         * Sucht den nächsten erlaubten Tab nach dem aktuellen und zeigt diesen
         *
         *
         */
        next: function () {
            var c = this.current, next;
            next = this._findNext(c);
            var i = next['id'];
            return this.show(i);
        },
        /**
         * Zeigt den vorherigen Tab
         *
         * Sucht den vorherigen erlaubten Tab vor dem aktuellen und zeigt diesen
         *
         *
         */
        previous: function () {
            var c = this.current, prev;
            prev = this._findPrevious(c);
            var i = prev['id'];
            return this.show(i);
        },
        /**
         * Zeigt den ersten Tab
         *
         * Sucht den ersten erlaubten Tab und zeigt diesen
         *
         *
         */
        first: function () {
            var f;
            f = this._findNext(-1);
            var i = f['id'];
            return this.show(i);
        },
        /**
         * Zeigt den letzten Tab
         *
         * Sucht den letzten erlaubten Tab und zeigt diesen
         *
         *
         */
        last: function () {
            var l, length = this.length +1;
            l = this._findPrevious(length)
            var i = l['id'];
            return this.show(i);
        },

        listen: function() {
            
            var self = this,
                o = self.options, 
                j;
            if(Win.AmaryllisCMS.support.isTouchDevice) {
                require(['events/swipe'], function(SwipeListener) {
                    var opt = {
                        onSwipeLeft: function(opts) {
                            return self.previous();
                        },
                        onSwipeRight: function(opts) {
                            return self.next();
                        },
                        xTolerance: 40
                    }, panes = Selectors.qa(o.panes, self.$paneContainer);
                    for(j = 0; j < panes.length; j++) {
                        SwipeListener.init(panes[j], opt);
                    }
                });
            }
        },

        /**
         * Deaktiviert einen Tab
         *
         * @param {Object} ca
         * @param {Callable} cb
         *
         * 
         */
        _deactivate: function (ca, cb) {
            Classes.removeClass(ca['tab'], this.options.active);
            ca['tab'].setAttribute('aria-expanded', 'false');
            Classes.removeClass(ca['panel'], this.options.active)
            ca['panel'].setAttribute('aria-expanded', 'false');
            cb && cb();
        },
        /**
         * Aktiviert einen Tab
         *
         * @param {Object} ca
         * @param {Callable} cb
         *
         * 
         */
        _activate: function (na, cb) {
            var self = this, o = this.options;
            var $target = na['panel'], effect = o.effect, j;
            for(j = 0; j < self.$tabs.length; j++) {
                if(o.effect) {
                    Classes.removeClass(self.$tabs[j]['tab'], o.effect);
                    Classes.removeClass(self.$tabs[j]['panel'], o.effect);
                }
                if(o.outeffect) {
                    Classes.addClass(self.$tabs[j]['tab'], o.outeffect);
                    Classes.addClass(self.$tabs[j]['panel'], o.outeffect);
                }
                
                Classes.removeClass(self.$tabs[j]['panel'], o.active);
                Classes.removeClass(self.$tabs[j]['tab'], o.active);
                self.$tabs[j]['panel'].setAttribute('aria-expanded', 'false');
                self.$tabs[j]['tab'].setAttribute('aria-expanded', 'false');
                Fade.fadeOut(self.$tabs[j]['panel']);
            }
            na['tab'].setAttribute('aria-expanded', true);
            
            $target.setAttribute('aria-expanded', true);
            Classes.addClass($target, o.active);
            if(o.effect) {
                Classes.addClass($target, o.effect);
                Classes.addClass(na['tab'], o.effect);
            }
            if(o.outeffect) {
                Classes.removeClass(na['tab'], o.outeffect);
                Classes.removeClass($target, o.outeffect);
            }
            
            setTimeout(function() {
                Classes.addClass(na['tab'], o.active);
                Fade.fadeIn($target);
            }, 500);
            
            if(o.history === true || o.history === 'true' || parseInt(o.history) === 1) {
                window.location.hash = na['selector'];
            }
            cb && cb();
        },
        /**
         * Findet den nächsten Tab intern
         *
         * Der aufruf erfolgt intern, bis der nächste Tab gefunden wurde, der
         * nicht deaktiviert ist
         *
         * @param {Number} i    Der Index
         *
         * @returns {Object|Boolean}
         */
        _findNext: function(i) {
            var ni = i +1, self = this;
            if(ni > this.length) {
                ni = 0;
            }
            var n = this.get(ni);
            if(Classes.hasClass(n['tab'], self.option('disabled'))) {
                var n = this._findNext(ni);
            }
            return n;
        },
        /**
         * Findet den vorigen Tab intern
         *
         * Der aufruf erfolgt intern, bis der vorige Tab gefunden wurde, der
         * nicht deaktiviert ist
         *
         * @param {Number} i    Der Index
         *
         * @returns {Object|Boolean}
         */
        _findPrevious: function(i) {
            var pi = i -1;
            if(pi < 0) {
                pi = this.length;
            }
            var p = this.get(pi);
            if(p['tab'].hasClass(this.option('disabled'))) {
                var p = this._findPrevious(pi);
            }
            return p;
        },
        /**
         * Preloader für alle Tabs
         *
         * Lädt alle Tabs inklusive Panels
         *
         * 
         */
        _load: function () {
            var self = this,
                o = self.options,
                tabs = [], hash = window.location.hash.trim(), fromHash,
                tabEles = Selectors.qa(o.tab, self.element), i;
            for(i = 0; i < tabEles.length; i++) {
                var $tab = tabEles[i],
                    $link = $tab.tagName.toLowerCase() === 'a' ? $tab : $tab.querySelector('a'),
                    isDisabled = Classes.hasClass($tab, o.disabled),
                    isActive = Classes.hasClass($tab, o.active), $panel,
                    selector = $link.acmsData('target');
                if (!selector) {
                    selector = $link.getAttribute('href');
                    try {
                        selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '');
                    } catch(E) {
                        return;
                    }
                }
                if(typeof(selector) === "string" && selector.trim() === hash) {
                    fromHash = i;
                }
                $link.acmsData('index', i);
                $panel = document.querySelector(selector);
                if(!Classes.hasClass($panel, 'animated')) {
                    Classes.addClass($panel, 'animated');
                }
                var oTab = {
                    _ignoreHashChange: false,
                    id: i,
                    disabled: isDisabled,
                    tab: $tab,
                    panel: $panel,
                    selector: selector,
                    active: isActive,
                    anchor: $link
                };
                AcmsEvent.add($link, 'click', function(e) {
                    e.preventDefault();
                    var t = e.target;
                    if(t.tagName.toLowerCase() !== 'a') {
                        t = Selectors.closest(t, 'a');
                    }
                    return self.show(t.acmsData('index'));
                });
                tabs[i] = oTab;
                if (isActive === true) {
                    self.current = i;
                }
            }
            
            this.$tabs = tabs;
            this.length = this.$tabs.length -1;
            if(fromHash && fromHash !== self.current) {
                self.show(fromHash)
            }

        }
    };

    return Tabs;
});
