/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */

define("ui/navs/dotnav", [
    "tools/utils"
], function (Utils) {

    "use strict";

    /**
     * Dot Nav Constructor
     * 
     * @param {Object} element
     * @param {Object} options
     * 
     * @exports ui/navs/dotnav
     */
    var DotNav = function (element, options) {
        this.element =
        this.options = null;
        this.initialize(element, options);
    };

    /**
     * Sagt dem Framework, dass eine Initialisierung notwendig ist
     *
     * @var {Boolean}
     */
    DotNav.needInit = true;

    /**
     * DotNav Quick-Initiator
     * 
     * @param {Object} element Element, das die Dot-Nav enthält
     * @param {Object} options Eignene Optionen
     * @returns {DotNav}
     */
    DotNav.init = function (element, options) {
        var dn = new DotNav(element, options);
        
        dn.element.acmsData(DotNav.NS, dn);
        
        return dn;
    };

    /**
     * Komponenten Name
     *
     * @var {String}
     */
    DotNav.MODULE = "Dot Navigation";

    /**
     * Komponenten Version
     *
     * @var {String}
     */
    DotNav.VERSION = "1.5.0";
    
    /**
     * Komponenten Namespace
     *
     * @var {String}
     */
    DotNav.NS = "acms.ui.navs.dotnav";

    /**
     * Die Standard-Optionen der Komponente
     * 
     * @var {Object} Standard-Optionen
     */
    DotNav.DEFAULT_OPTIONS = {
        menuwrapper: "ul,ol",
        callback: function(){}
    };
    
    DotNav.css = "media/css/navs/dotnav.min.css";

    /**
     * Klassen definition
     */
    DotNav.prototype = {
        constructor: DotNav,
        /**
         * Interner Constructor
         *
         * @param {Object} element
         * @param {Object} options
         * 
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this.nav = this.element.querySelector(this.options.menuwrapper);
        },
        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {Object|DotNav.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return DotNav.DEFAULT_OPTIONS;
        },

        /**
         * Bildet die Optionen
         *
         * <p>
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         * </p>
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function(options) {
            return Utils.extend(
                    {},
                    this.getDefaultOptions(),
                    options,
                    this.element.acmsData()
                    );
        },
        
        build: function () {
            var hop = this.element.className.indexOf('dotstyle-hop') !== -1;

            var dots = [].slice.call(this.nav.querySelectorAll('li')), current = 0, self = this;

            dots.forEach(function (dot, idx) {
                dot.addEventListener('click', function (e) {
                    e.preventDefault();
                    if (idx !== current) {
                        dots[ current ].className = '';

                        // special case
                        if (hop && idx < current) {
                            dot.className += ' current-from-right';
                        }

                        setTimeout(function () {
                            dot.className += ' current';
                            current = idx;
                            if (typeof self.options.callback === 'function') {
                                self.options.callback(current);
                            }
                        }, 25);
                    }
                });
            });
        }
    };

    return DotNav;

});
