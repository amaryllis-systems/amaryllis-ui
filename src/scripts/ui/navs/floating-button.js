/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */

/**
 * Floating Buttons
 *
 * Floating Buttons sind eine navigations-Hilfe für Benutzer, die innerhalb des
 * Browsers (meist unten links inder Ecke) auftaucht. Die Buttons können,wenn
 * mit dieser Komponente gehandhat, sub-links enthalten mit entsprechenden
 * Benutzer-Aktionen per Button.
 *
 * @param {Object} Utils
 * @param {Object} Selectors
 * @param {Object} Classes
 * @param {Object} Style
 * @param {Object} AcmsEvent
 * 
 * @returns {FloatingButton}
 */
define('ui/navs/floating-button', [
    'tools/utils',
    'core/selectors',
    'core/classes',
    'core/style',
    'events/event'
], function (Utils, Selectors, Classes, Style, AcmsEvent) {

    "use strict";

    /**
     * FloatingButton Constructor
     *
     * @param {Object} element Das Element, welches die FloatingButton Nav hält
     * @param {Object} options Eigene Optionen
     *
     * @exports ui/navs/floating-button
     */
    var FloatingButton = function (element, options) {
        this.element =
        this.options =
        this.debug = null;
        this.initialize(element, options);
    };
    
    

    FloatingButton.needInit = true;

    FloatingButton.init = function (element, options) {
        var f = new FloatingButton(element, options);
        f.element.acmsData(FloatingButton.NS, f);
        return f;
    }

    /**
     * Komponenten-Name
     *
     * @var {String}
     */
    FloatingButton.MODULE = "Floating Button Menu";

    FloatingButton.NS = 'acms.ui.navs.floating-button';

    /**
     * Komponenten Optionen
     *
     * @var {Object}    Standard-Optionen der Komponente
     */
    FloatingButton.DEFAULT_OPTIONS = {
        wrapper: ".fixed-action-button .button.floating",
        navwrapper: "ul .button.floating",
        btntrigger: '[data-toggle="floating-menu"]',
        btns: ".button.floating"
    };


    FloatingButton.prototype = {
        constructor: FloatingButton,
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this.button = Selectors.q('.button.floating', element)
            this.$menu = Selectors.qa(this.options.navwrapper, element);
            this.listen();
        },
        /**
         * Gibt die Standard-Optionen der Komponente zurück
         * 
         * @returns {Object|FloatingButton.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return FloatingButton.DEFAULT_OPTIONS;
        },
        /**
         * Bildet die Optionen
         *
         * Bildet die eigenen Optionen basierend auf den Standard-Optionen,
         * übergebenen Parametern und den Element data-Attributen. Letztere
         * gewinnen.
         *
         * @param {Object} options Eigene Optionen
         * 
         */
        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },
        listen: function () {
            var self = this, buttons = self.$menu, j, btn = Selectors.q('.button.floating');
            var onOpen = function(e) {
                var target = e.target;
                self.open(target);
            }, onClose = function (e) {
                var target = e.target;
                self.close(target);
            };
            AcmsEvent.add(self.element, 'mouseenter', onOpen);
            AcmsEvent.add(self.element, 'mouseleave', onClose);
        },
        open: function (btn) {
            var self = this,
                     buttons = self.$menu, j;
            
            if (Classes.hasClass(self.element, 'active') === false) {
                self.button && Classes.addClass(self.button, 'active');
                Classes.addClass(self.element, 'active');
                for(j = 0; j < buttons.length; j++) {
                    buttons[j].style['opacity'] = 1;
                }
                return;
            }
        },
        close: function (btn) {
            var self = this,
                    buttons = self.$menu;
            
            Classes.removeClass(self.button, 'active');
            Classes.removeClass(self.element, 'active');
            for(var j = 0; j < buttons.length; j++) {
                Style.setStyle(buttons[j], 'opacity', 0);
            }
        }
    };

    return FloatingButton;
});
