/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */


define('ui/navs/navbar', [
    'tools/utils',
    'core/classes',
    'core/selectors',
    'events/event'
], function(Utils, Classes, Selectors, AcmsEvent) {

    "use strict";

    var NAVBAR_DATA = "acms.ui.navs.navbar";

    var Navbar = function(element, options) {
        this.element    = 
        this.options    = null;
        this.initialize(element, options);
    };

    /**
     * Komponenten Name
     *
     * @var {String}
     */
    Navbar.MODULE = "Model-Formular";

    /**
     * Komponenten Version
     *
     * @var {String}
     */
    Navbar.VERSION = "1.5.0";

    /**
     * Standard Optionen
     *
     * @var {Object}
     */
    Navbar.DEFAULT_OPTIONS = {
        toggle: '[data-toggle=navbar]',
        menu: '.horizontal-menu'
    };

    Navbar.needInit = true;

    Navbar.init = function(element, options) {
        var mf = new Navbar(element, options);
        mf.element.acmsData(NAVBAR_DATA, mf);
        return mf;
    };

    Navbar.prototype = {

        constructor: Navbar,

        /**
         * Interner Constructor
         *
         * @param {Object} element Element
         * @param {Object} options Options
         */
        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this.ww = document.body.clientWidth;
            this.listen();
        },

        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {Navbar.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function() {
            return Navbar.DEFAULT_OPTIONS;
        },

        /**
         * Bildet die Optionen
         *
         * <p>
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         * </p>
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function(options) {
            return Utils.extend(
                    {},
                    this.getDefaultOptions(),
                    options,
                    this.element.acmsData()
                    );
        },

        listen: function() {
            var self = this,
                o = this.options,
                element = self.element,
                toggles = Selectors.qa(o.toggle, element),
                cb = function(ev) {
                    ev.preventDefault();
                    var $this = ev.target, target = $this.acmsData('target');
                    Classes.toggleClass($this, 'active');
                }, i, toggle, dd, j
            for(i = 0; i < toggles.length; i++) {
                toggle = toggles[i],
                dd = Selectors.qa('.dropdown-toggle', toggle);
                for(j = 0; j < dd.length; j++) {
                    AcmsEvent.add(dd[j], 'click', cb);
                }
            }
            if(Classes.hasClass(element, 'application-menu')) {
                var anchors = Selectors.qa('li a', element);
                for(var k = 0; k < anchors.length; k++) {
                    if(anchors[k].nextSibling) {
                        Classes.addClass(anchors[k], 'parent');
                    }
                }
            }
            var menuToggle = element.querySelector('.toggleMenu');
            if(menuToggle) {
                AcmsEvent.add(menuToggle, 'click', function(e) {
                    e.preventDefault();
                    Classes.toggleClass(e.target, 'active');
                    var appMenu = element.querySelector('.application-menu');
                    if(Classes.hasClass(e.target, 'active')) {
                        appMenu.style.display = 'block';
                        appMenu.setAttribute('aria-hidden', 'false');
                        appMenu.setAttribute('aria-expanded', 'false');
                    } else {
                        appMenu.style.display = 'none';
                        appMenu.setAttribute('aria-expanded', 'true');
                    }
                });
            }
            
            if (this.ww <= 1024) {
                self.adjustMenu();
            }
            AcmsEvent.add(window, 'resize,orientationchange', function(e) {
                self.ww = document.body.clientWidth;
                if(self.ww <= 1024) {
                    self.adjustMenu();
                }
            });
            var lis = Selectors.qa('li', element), m, cb = function(e) {
                Classes.toggleClass(e.target, 'hover');
            };
            for(m = 0; m < lis.length; m++) {
                AcmsEvent.add(lis[i], 'mouseenter', cb);
                AcmsEvent.add(lis[i], 'mouseleave', cb);
            }
            
        },
        adjustMenu: function() {
            var self = this,
                o = this.options,
                element = self.element;
            var menuToggle = element.querySelector('.toggleMenu');
            if (this.ww < 1025) {
                menuToggle.style.display = "inline-block";
                if(! Classes.hasClass(menuToggle, "active")) {
                    Classes.removeClass(element, "active");
                } else {
                    Classes.addClass(element, "active");
                }
                AcmsEvent.add(menuToggle, 'click', function(e) {
                    e.preventDefault();
                    Classes.toggleClass(element, 'active');
                });
            } else if (this.ww >= 1025) {
                menuToggle.style.display = "none";
                Classes.addClass(element, 'active');
            }
        }
    };

    return Navbar;
});