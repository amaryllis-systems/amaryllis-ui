/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */

/**
 * TOC
 * 
 * @param {type} c
 * @param {type} w
 * @param {type} d
 * 
 * @returns {Toc}
 */
define("ui/navs/toc", ['core/core', "core/window", "core/document"], function(c, w, d) {

    'use strict';

    var TOC_DATA = "acms.ui.navs.toc",
        NS = "." + TOC_DATA,
        EVENT_SHOW = "show" + NS,
        EVENT_SHOWN = "shown" + NS,
        EVENT_GENERATE = "generate" + NS,
        EVENT_GENERATED = "generated" + NS,
        EVENT_HIDE = "hide" + NS,
        EVENT_HIDDEN = "hidden" + NS
    ;

    /**
     * Constructor
     *
     * TOC fires some events:
     *
     *  "acms.show.toc"         : before showing the nested menu item
     *  "acms.shown.toc"        : after showing the nested menu item
     *  "acms.hide.toc"         : before hiding a nested menu item
     *  "acms.hidden.toc"       : after hiding the nested menu item
     *  "acms.generate.toc"     : before generating the Menu
     *  "acms.generated.toc"    : after generating the menu
     * 
     * @param {Object} element Element to build the TOC for
     * @param {Object} options {@see Toc.DEFAULT_OPTIONS}
     * @returns {Toc}
     */
    var Toc = function(element, options) {
        this.$element   =
        this.options    = null;
        this.items      = [];
        this.initialize(element, options);
    };

    /**
     * Komponenten Name
     *
     * @var {String}
     */
    Toc.MODULE = "Toc Generator";

    /**
     * Komponenten Version
     *
     * @var {String}
     */
    Toc.VERSION = "1.5.0";

    /**
     * Component Default Options
     *
     * container:       {String, Selector}  A Target Container to append the Toc to
     * listtype:        {String}            The List-Type to be used for the TOC.
     *                                      '<ul />' or '<ol />' are supported
     * ***************
     * Selector Setup
     * ***************
     * selectors:       {String}            Selectors to be added to the TOC.
     *                                      Can be the generic h(x)-Tag or a
     *                                      more specific selector. As of default,
     *                                      the TOC will be generated using
     *                                      h2-h6 Tags. To be more strict, you
     *                                      can use selectors like h2.toc-element
     *                                      or similar (depends on your
     *                                      heading-classes)
     * ignoreselectors: {String}            Similar to selectors you can provide
     *                                      a list of selectors to be ignored 
     *                                      for building tocs.. Main difference
     *                                      is, that only ONE selector is
     *                                      supported! Do not add multiple ignore
     *                                      selectors!
     *                                      (e.g. h3.non-toc or .non-toc)
     * ***********************
     * CSS Classes to be used
     * ***********************
     * hidetocclass:    {String}            A class to be added to the toc,
     *                                      if no elements have been found.
     *                                      Default: "hidden".
     * tocclass:        {String}            A TOC Class that will be added,
     *                                      once we found elements,
     * headerclass:     {String}            A Class that will be added to the
     *                                      top level TOC List
     * subheaderclass:  {String}            A Class that will be added to the
     *                                      sub level TOC List
     * itemclass:       {String}            A Class that will be added to the
     *                                      items of the TOC List
     * activeclass      {String}            A Class that will be added to the
     *                                      current active item
     * hoverclass       {String}            A Class that will be added to the
     *                                      current hovered item
     * *************************
     * Effect Settings
     * *************************
     * showeffect:      {String}            A Effect to be used for
     *                                      showing the current item
     * showeffectspeed: {String}            Speed of the show effect. Can be
     *                                      "slow","medium","fast" or any
     *                                      numeric number (milliseconds)
     * hideeffect:      {String}            A Effect to be used for
     *                                      hiding the current item
     * hideeffectspeed: {String}            Speed of the effect. Can be "slow",
     *                                      "medium", "fast", or any numeric
     *                                      number (milliseconds)
     *
     * showandhide:     {Boolean}           Whether or not to hide an element,
     *                                      if another is shown. Default: true
     *
     * 
     * ****************
     *  Scroll Settings
     * ****************
     * smoothScroll     {Boolean}           Determines if a animation
     *                                      should be used to scroll to specific
     *                                      table of contents items on the page
     * scrolltrans      {String, Number}    Duration Time of the Transition
     *                                      duration. Accepts Number
     *                                      (milliseconds) or String: "slow",
     *                                      "medium", or "fast"
     * scrollTo         {Number}            The amount of space between the top
     *                                      of page and the selected table of
     *                                      contents item after the page has
     *                                      been scrolled (in px)
     * showhideonscroll {Boolean}           Determines if table of contents
     *                                      nested items should be shown and
     *                                      hidden while scrolling
     * highlightOnScroll {Boolean}          Determines if table of contents
     *                                      nested items should be highlighted
     *                                      (set to a different color) while
     *                                      scrolling
     * highlightOffset  {Number}            The offset distance in pixels to
     *                                      trigger the next active table of
     *                                      contents item
     * extendPage       {Boolean}           If a user scrolls to the bottom of
     *                                      the page and the page is not tall
     *                                      enough to scroll to the last table
     *                                      of contents item, then the page
     *                                      height is increased
     * extendPageOffset {Number}            How close to the bottom of the page
     *                                      a user must scroll before the page
     *                                      is extended
     * history          {Boolean}           Adds a hash to the page url to
     *                                      maintain history
     * scrollHistory    {Boolean}           Adds a hash to the page url, to
     *                                      maintain history, when scrolling to
     *                                      a TOC item
     * ******************************************
     * Other Settings. Extending the flexibility
     * ******************************************
     * 
     * hashGenerator:   {String, function}  Hash Generator to be used. Can be
     *                                      "pretty", null or a function
     *                                      to generate a hash. Function must
     *                                      return a String!!
     * toctitle:        {String, Boolean}   A custom Title of the Toc. Default:
     *                                      "Toc". Set to FALSE to disable TOC
     *                                      Title
     * highlightDefault {Boolean}           Set's the first TOC item as active
     *                                      if no other TOC item is active.
     *
     *                                      
     * @var {Object}
     */
    Toc.DEFAULT_OPTIONS = {
        container: '#acms-toc',
        listtype: '<ol/>',
        selectors: 'h2,h3,h4,h5,h6',
        ignoreselectors: null,
        /* Classes */
        hidetocclass: 'hidden',
        tocclass: 'acms-toc',
        headerclass: 'acms-toc-header',
        subheaderclass: 'acms-toc-subheader',
        itemclass:  'acms-toc-item',
        activeclass: "active",
        hoverclass: "active",

        showandhide: true,

        hashGenerator: "pretty",
        toctitle: Acms.Translator._("TOC"),
        titleelement: "<div />",
        titleclass: "acms-toc-title",

        showeffect: "slideDown",
        showeffectspeed: "medium",
        hideeffect: "slideUp",
        hideeffectspeed: "medium",

        /** Scrolling **/
        smoothScroll: true,
        scrolltrans: "medium",
        scrollTo: 0,
        showhideonscroll: true,
        highlightOnScroll: true,
        highlightOffset: 40,
        extendPage: true,
        extendPageOffset: 100,
        history: true,
        scrollHistory: false,
        highlightDefault: true
    };

    Toc.css = "media/css/navs/toc.min.css";

    Toc.needInit = true;

    Toc.init = function(element, options) {
        var t = new Toc(element, options);
        c.$(element).data(TOC_DATA, t);

        return t;
    }

    /**
     * Class Body
     */
    Toc.prototype = {
        /**
         * Init Construct
         * @param {Object} element Element to build the TOC for
         * @param {Object} options {@see Toc.DEFAULT_OPTIONS}
         */
        initialize: function(element, options) {
            this.$element = c.$(element);
            this.options = this.buildOptions(options);
            this.supportTransition = w.AmaryllisCMS.supportTransition;
            this.$container = c.$(this.options.container);
            this.focusClass = this.options.activeclass || "active";
            this.hoverClass = this.options.hoverclass || "active";
            this.generateToc();
            this.addCSSClasses();

            this.listen();

        },

        /**
         * Gets the Default Options
         * 
         * @returns {Toc.DEFAULT_OPTIONS|Object}
         */
        getDefaultOptions: function() {
            return Toc.DEFAULT_OPTIONS;
        },
        /**
         * Bildet die Optionen
         *
         * <p>
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         * </p>
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function(options) {
            options = (typeof options === "object") ? options : {};
            return c.$.extend(
                    {},
                    this.getDefaultOptions(),
                    options,
                    this.$element.data()
                    );
        },

        /**
         * Opens the current sub-header
         * 
         * @param {type} elem
         * @param {type} scroll
         * @returns {toc_L25.Toc}
         */
        show: function(elem, scroll) {
            var self = this,
                subheaderClass = "." + this.options.subheaderclass,
                headerClass = "." + this.options.headerclass,
                e = c.$.Event(EVENT_SHOW, {relatedTarget: elem}),
                e2 = c.$.Event(EVENT_SHOWN, {relatedTarget: elem})
                ;
            self.$element.trigger(e);
            if(e.isDefaultPrevented()) {
                return;
            }
            if (!elem.is(":visible")) {
                if(!elem.find(subheaderClass)
                        .length
                        && !elem.parent()
                                .is(headerClass)
                                && !elem.parent()
                                        .is(":visible")) {
                    elem = elem.parents(subheaderClass).add(elem);
                } else if(!elem.children(subheaderClass).length
                            && !elem.parent().is(headerClass)) {
                    elem = elem.closest(subheaderClass);
                }
                switch (self.options.showeffect) {
                    case "show":
                        elem.show(self.options.showeffectspeed);
                        break;
                    case "slideDown":
                        elem.slideDown(self.options.showeffectspeed);
                        break;
                    case "fadeIn":
                        elem.fadeIn(self.options.showeffectspeed);
                        break;
                    case "none":
                    default:
                        elem.show();
                    break;
                }
            }
            if(elem.parent().is(headerClass)) {
                self.hide(c.$(subheaderClass).not(elem));
            } else {
                self.hide(c.$(subheaderClass)
                        .not(elem.closest(headerClass)
                            .find(subheaderClass)
                            .not(elem.siblings())
                        )
                    );
            }
            self.$element.trigger(e2);
            return self;
        },

        /**
         * Set the current Active Element
         * @param {Boolean} pageload
         * @returns {toc_L25.Toc}
         */
        setActiveElement: function(pageload) {

            var self = this,
                hash = w.location.hash.substring(1),
                elem = self.$container.find('li[data-unique="' + hash + '"]'),
                e = c.$.Event('acms.activate.toc', {relatedTarget: elem.first() }),
                e2 = c.$.Event('acms.activated.toc', {relatedTarget: elem.first() })
            ;
            self.$container.trigger(e);
            if(e.isDefaultPrevented()) {
                return;
            }
            if(hash.length) {
                self.$container
                        .find("." + self.focusClass)
                        .removeClass(self.focusClass);
                elem.addClass(self.focusClass);
                if(self.options.showAndHide) {
                    elem.click();
                }
            } else {
                self.$container
                        .find("." + self.focusClass)
                        .removeClass(self.focusClass);
                if(!hash.length 
                        && pageload
                        && self.options.highlightDefault) {
                    self.$container
                            .find("." + self.options.itemclass)
                            .first()
                            .addClass(self.focusClass);
                }
            }
            self.$container.trigger(e2);
            return self;

        },

        /**
         * Closes the current sub-header
         * @param {type} elem
         * @returns {toc_L25.Toc}
         */
        hide: function(elem) {
            var self = this,
                e = c.$.Event(EVENT_HIDE, {relatedTarget: elem}),
                e2 = c.$.Event(EVENT_HIDDEN, {relatedTarget: elem});
            self.$element.trigger(e);
            if(e.isDefaultPrevented()) {
                return;
            }
            switch (self.options.hideeffect) {
                case "hide":
                    elem.hide(self.options.hideeffectspeed);
                    break;
                case "slideUp":
                    elem.slideUp(self.options.hideeffectspeed);
                    break;
                case "fadeOut":
                    elem.fadeOut(self.options.hideeffectspeed);
                    break;
                case "none":
                default:
                    elem.hide();
                    break;
            }
            self.$element.trigger(e2);
            return self;
        },

        /**
         * Determines what elements get shown on scroll and click
         *
         * @param {Object} elem
         * @param {type} scroll
         * @returns {toc_L25.Toc}
         */
        triggerShow: function(elem, scroll) {

            var self = this,
                subheaderClass = "." + this.options.subheaderclass,
                headerClass = "." + this.options.headerclass
            ;
            if(elem.parent().is(headerClass)
                    || elem.next().is(subheaderClass)) {
                self.show(elem.next(subheaderClass), scroll);
            } else if(elem.parent().is(subheaderClass)) {
                self.show(elem.parent(), scroll);
            }
            return self;
        },

        /**
         * Generates the HTML for the dynamic table of contents
         * 
         */
        generateToc: function() {
            var self = this,
                firstElem,
                list,
                ignoreSelector = self.options.ignoreselectors;
            if(this.options.selectors.indexOf(",") !== -1) {
                firstElem = this.$element
                                .find(
                                    this.options.selectors
                                        .replace(/ /g,"")
                                        .substr(0, this.options.selectors.indexOf(",")));
            } else {
                firstElem = this.$element
                                    .find(this.options.selectors.replace(/ /g,""));
            }
            var e = c.$.Event(EVENT_GENERATE, {});
            self.$element.trigger(e);
            if(e.isDefaultPrevented()) {
                return;
            }
            if(!firstElem.length) {
                self.$container.addClass(self.options.hidetocclass);
                return;
            }
            self.$container.addClass(self.options.tocclass);
            var showTitle = self.options.toctitle !== false;
            if(showTitle === true) {
                var titleElement = c.$(self.options.titleelement, {
                    "text": self.options.toctitle,
                    "class": self.options.titleclass
                });
                self.$container.append(titleElement);
            }

            firstElem.each(function(index) {
                var $this = c.$(this);
                if($this.is(ignoreSelector)) {
                    return;
                }
                list = c.$(self.options.listtype, {
                    "id": self.options.headerclass + '-' + index,
                    "class": self.options.headerclass
                })
                .append(self.nestElements($this, index));
                self.$container.append(list);

                $this.nextUntil(this.nodeName.toLowerCase()).each(function() {
                    var $that = c.$(this);
                    if($that.find(self.options.selectors).length === 0) {
                        $that.filter(self.options.selectors).each(function() {
                            if($that.is(ignoreSelector)) {
                                return;
                            }
                            self.appendSubheaders.call(this, self, list);
                        });
                    } else {
                        $that.find(self.options.selectors).each(function() {
                            if(c.$(this).is(ignoreSelector)) {
                                return;
                            }
                            self.appendSubheaders.call(this, self, list);
                        });
                    }
                });
            });
            var e2 = c.$.Event(EVENT_GENERATED, {});
            self.$element.trigger(e2);
        },

        /**
         * Helps create the table of contents list by appending nested list items
         * @param {Object} self
         * @param {Number} index
         * @returns {unresolved}
         */
        nestElements: function(self, index) {
            var arr, item, hashValue;
            arr = c.$.grep(this.items, function (item) {
                return item === self.text();
            });
            if(arr.length) {
                this.items.push(self.text() + index);
            } else {
                this.items.push(self.text());
            }
            hashValue = this.generateHash(arr, self, index);
            item = c.$("<li/>", {
                "class": this.options.itemclass,
                "data-unique": hashValue
            }).append(c.$("<a/>", {
                "text": self.text(),
                "title": self.text()
            }));
            self.before(c.$("<div/>", {
                "name": hashValue,
                "data-unique": hashValue
            }));

            return item;

        },

        /**
         * Generates the hash value that will be used to refer to each item.
         * @param {Array} arr
         * @param {Object} self
         * @param {Number} index
         * @returns {String}
         */
        generateHash: function(arr, self, index) {

            var hashValue = "",
                hashGenerator = this.options.hashGenerator;

            if (hashGenerator === "pretty") {
                hashValue = self.text().toLowerCase().replace(/\s/g, "-");
                while (hashValue.indexOf("--") > -1) {
                    hashValue = hashValue.replace(/--/g, "-");
                }
                while (hashValue.indexOf(":-") > -1) {
                    hashValue = hashValue.replace(/:-/g, "-");
                }
            } else if (typeof hashGenerator === "function") {
                hashValue = hashGenerator(self.text(), self);
            } else {
                hashValue = self.text().replace(/\s/g, "");
            }
            if (arr.length) { hashValue += ""+index; }
            return hashValue;

        },

        /**
         * Helps create the table of contents list by appending subheader elements
         * @param {Toc} self
         * @param {Object} list The List Item
         * 
         */
        appendSubheaders: function(self, list) {
            var $this = c.$(this),
                index = $this.index(self.options.selectors),
                previousHeader = c.$(self.options.selectors).eq(index - 1),
                currentTagName = +$this.prop("tagName").charAt(1),
                previousTagName = +previousHeader.prop("tagName").charAt(1),
                subheaderClass = self.options.subheaderclass,
                itemClass = "." + self.options.itemclass,
                lastSubheader;
            if(currentTagName < previousTagName) {
                self.$container.find( "." + subheaderClass + "[data-tag=" + currentTagName + "]")
                            .last()
                            .append(self.nestElements($this, index));

            } else if(currentTagName === previousTagName) {
                list
                    .find(itemClass)
                    .last()
                    .after(self.nestElements($this, index));
            } else {
                list
                    .find(itemClass)
                    .last()
                    .after(c.$(self.options.listtype, {
                            "class": subheaderClass,
                            "data-tag": currentTagName
                        }))
                    .next("." + subheaderClass)
                    .append(self.nestElements($this, index));
            }
        },

        /**
         * Adds CSS classes to the newly generated table of contents HTML
         *
         * @returns {toc_L25.Toc.prototype}
         */
        addCSSClasses: function() {
            this.$container
                    .find("." + this.options.headerclass + ", ." + this.options.subheaderclass)
                    .addClass("nav nav-list");
            return this;
        },

        /**
         * Adds event handlers to the newly generated table of contents
         * 
         * 
         */
        listen: function() {
            var self = this,
                $self,
                duration;
            this.$container.on("click", "li", function(event) {
                var $this = c.$(this);
                if(self.options.history) {
                    w.location.hash = $this.attr("data-unique");
                }
                self.$container
                        .find("." + self.focusClass)
                        .removeClass(self.focusClass);
                $this.addClass(self.focusClass);
                if(self.options.showandhide) {
                    var elem = c.$('li[data-unique="' + $this.attr("data-unique") + '"]');
                    self.triggerShow(elem);
                }
                self.scrollTo($this);
            });
            this.$container
                    .find("li")
                    .on({
                            // Mouseenter event handler
                            "mouseenter.acms.toc": function() {
                                // Adds a hover CSS class to the current list item
                                c.$(this).addClass(self.hoverClass);
                                // Makes sure the cursor is set to the pointer icon
                                c.$(this).css("cursor", "pointer");
                            },
                            // Mouseleave event handler
                            "mouseleave.acms.toc": function() {
                                // maybe runs in trouble on bs themes
                                c.$(this).removeClass(self.hoverClass);
                            }
                    });

            if (self.options.extendPage
                    || self.options.highlightOnScroll
                    || self.options.scrollHistory
                    || self.options.showhideonscroll)
            {
            // Window scroll event handler
                c.$window.on("scroll.acms.toc", function() {
                    c.$("html, body")
                            .promise()
                            .done(function() {
                                var winScrollTop = c.$window.scrollTop(),
                                    winHeight = c.$window.height(),
                                    docHeight = c.$document.height(),
                                    scrollHeight = c.$body[0].scrollHeight,
                                    elem,
                                    lastElem,
                                    lastElemOffset,
                                    currentElem;
                        if(self.options.extendPage) {
                            if((self.webkit 
                                    && winScrollTop
                                        >= scrollHeight
                                            - winHeight
                                            - self.options.extendPageOffset
                                ) || (!self.webkit
                                        && winHeight
                                            + winScrollTop
                                            > docHeight
                                                - self.options.extendPageOffset)
                                ) {
                                    if(!c.$(".acms-toc-extend").length) {
                                        lastElem = c.$('div[data-unique="' + c.$(self.options.itemclass).last().attr("data-unique") + '"]');
                                        if(!lastElem.length) return;
                                        lastElemOffset = lastElem.offset().top;
                                        self.$container.append(c.$("<div />", {
                                            "class": "acms-toc-extend",
                                            "height": Math.abs(lastElemOffset - winScrollTop) + "px",
                                            "data-unique": "acms-toc-extend"
                                    }));
                                    if(self.extendPageScroll) {
                                        currentElem = self.$container.find('li.active');
                                        self.scrollTo(c.$('div[data-unique="' + currentElem.attr("data-unique") + '"]'));
                                    }
                                }
                            }
                        }
                        
                        setTimeout(function() {
                            var closestAnchorDistance = null,
                                closestAnchorIdx = null,
                                anchors = self.$container
                                                .find("div[data-unique]"),
                                anchorText;
                            anchors.each(function(idx) {
                                var distance = Math.abs((c.$(this).next().length
                                            ? c.$(this).next()
                                            : c.$(this)).offset().top
                                                        - winScrollTop
                                                        - self.options.highlightOffset);
                                if (closestAnchorDistance === null
                                        || distance < closestAnchorDistance) {
                                    closestAnchorDistance = distance;
                                    closestAnchorIdx = idx;
                                } else {
                                    return false;
                                }
                            });
                            anchorText = c.$(anchors[closestAnchorIdx])
                                            .attr("data-unique");
                            elem = c.$('li[data-unique="' + anchorText + '"]');
                            if(self.options.highlightOnScroll
                                    && elem.length) {
                                
                                self.$container
                                        .find("." + self.focusClass)
                                        .removeClass(self.focusClass);
                                elem.addClass(self.focusClass);
                            }
                            if(self.options.scrollHistory) {
                                if(w.location.hash !== "#" + anchorText) {
                                    w.location.replace("#" + anchorText);
                                }
                            }
                            if(self.options.showhideonscroll && self.options.showAndHide) {

                                self.triggerShow(elem, true);
                            }
                        }, 0);
                    });
                });
            }
        },

        /**
         * Scrolls to a specific element
         * 
         * @param {Object} elem
         * @returns {toc_L25.Toc}
         */
        scrollTo: function(elem) {

            var self = this,
                duration = self.options.smoothScroll || 0,
                scrollTo = self.options.scrollTo,
                currentDiv = c.$('div[data-unique="' + elem.attr("data-unique") + '"]');
            if(!currentDiv.length) {
                return self;
            }
            c.$("html, body")
                    .promise()
                    .done(function() {
                        c.$("html, body").animate({
                            // Sets the `scrollTop` to the top offset of the HTML div tag that matches the current list item's `data-unique` tag
                            "scrollTop": currentDiv
                                            .offset()
                                            .top - (c.$.isFunction(scrollTo)
                                                        ? scrollTo.call()
                                                        : scrollTo) + "px"
                        }, {
                            "duration": duration
                        });

            });
            return self;
        }

    };

    return Toc;
});
