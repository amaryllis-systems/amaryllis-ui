/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */


define('ui/navs/dropdown', [
    'tools/utils',
    'core/document',
    'core/selectors',
    'core/classes',
    'events/event'
], function (Utils, Doc, Selectors, Classes, AcmsEvent) {

    "use strict";

    var DD_DATA = "acms.ui.tools.dropdown",
            NS = "." + DD_DATA,
            EVENT_OPEN = "open" + NS,
            EVENT_OPENED = "opened" + NS,
            EVENT_CLOSE = "close" + NS,
            EVENT_CLOSED = "closed" + NS,
            EVENT_ENABLE = "enable" + NS,
            EVENT_ENABLED = "enabled" + NS,
            EVENT_DISABLE = "disable" + NS,
            EVENT_DISABLED = "disabled" + NS
            ;


    /**
     * Constructor
     * 
     * @param {Object} element  Das Dropdown Element
     * @param {Object} options  Die eigenen Optionen
     * @returns {Dropdown}
     */
    var Dropdown = function (element, options) {
        this.element =
        this.options =
        this.toggler =
        this.targetID =
        this.target = null;
        this.initialize(element, options);
    };

    /**
     * Komponenten Name
     *
     * @var {String}
     */
    Dropdown.MODULE = "Dropdown";

    /**
     * Komponenten Version
     *
     * @var {String}
     */
    Dropdown.VERSION = "1.5.0";

    //Dropdown.css = "media/css/navs/dropdown.min.css";

    /**
     * Standard Optionen
     *
     * @var {Object}
     */
    Dropdown.DEFAULT_OPTIONS = {
        target: ".dropdown-content",
        toggle: ".dropdown-toggle",
        active: "active",
        disabled: "disabled",
        effect: 'slide',
        open: 'zoomInDown',
        close: 'zoomOutUp',
        onDrop: function(element, target) {},
        onUp: function(element, target) {}
    };

    Dropdown.needInit = true;

    Dropdown.init = function (element, options) {
        var dd = new Dropdown(element, options);
        dd.element.acmsData(DD_DATA, dd);
        return dd;
    };

    Dropdown.prototype = {
        constructor: Dropdown,
        /**
         * Initialisiert das Dropdown
         *
         * @param {Object} element
         * @param {Object} options
         *
         * 
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            var o = this.options;
            this.target = element.querySelector(o.target);
            if (Classes.hasClass(element, o.toggle)) {
                this.toggler = this.element;
            } else {
                this.toggler = element.querySelector(o.toggle);
            }
            this._prepare();
            this.listen();
        },
        /**
         * Gibt die Standard-Optionen zurück
         * 
         * @returns {Dropdown.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return Dropdown.DEFAULT_OPTIONS;
        },
        /**
         * Bildet die Optionen
         *
         * <p>
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         * </p>
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },
        toggle: function () {
            if (true === this.isOpen()) {
                return this.close();
            } else {
                return this.open();
            }
        },
        open: function () {
            if (true === this._isOpen || true === this.isDisabled()) {
                return;
            }
            var relatedTarget = {relatedTarget: this.target},
                e = AcmsEvent.createCustom(EVENT_OPEN, relatedTarget),
                    self = this,
                    o = this.options;
            AcmsEvent.dispatch(this.element, e);
            AcmsEvent.dispatch(this.target, e);
            if (e.defaultPrevented) {
                return;
            }
            this._isOpen = true;
            Classes.removeClass(this.target, o.close);
            Classes.addClass(this.target, 'animated,'+ o.open);
            Classes.addClass(this.element, o.active);
            Classes.addClass(this.toggler, o.active);
            Classes.addClass(this.target, o.active);
            this.toggler.setAttribute('aria-expanded', 'true');
            this.target.setAttribute('aria-expanded', 'true');
            var focus = AcmsEvent.create('focus');
            AcmsEvent.dispatch(this.target, focus);
            
            var parents = Selectors.qa('.dropdown', this.element), j;
            var closeParent = function(e) {
                if(self.isOpen()) {
                    self.close();
                }
            }
            for(j = 0; j < parents.length; j++) {
                var child = parents[j];
                if(!child.acmsData(DD_DATA)) {
                    continue;
                }
                var dd = child.acmsData(DD_DATA);
                AcmsEvent.add(dd.element, EVENT_CLOSE, closeParent);
            }
            
            if (typeof o.onDrop === 'function') {
                o.onDrop(self.element);
            }
            var e2 = AcmsEvent.createCustom(EVENT_OPENED, relatedTarget);
            AcmsEvent.dispatch(this.element, e2);
            AcmsEvent.dispatch(this.target, e2);
            //Core.$document.on('click', self.checkClick.bind(this));
        },
        close: function () {
            if (false === this.isOpen()) {
                return;
            }
            var relatedTarget = {relatedTarget: this},
                e = AcmsEvent.createCustom(EVENT_CLOSE, relatedTarget),
                target = this.target,
                ele = this.element;
            AcmsEvent.dispatch(ele, e);
            AcmsEvent.dispatch(target, e);
            AcmsEvent.dispatch(this.toggler, e);
            if (e.defaultPrevented) {
                return;
            }
            var self = this,
                o = this.options;
            this._isOpen = false;
            Classes.removeClass(this.target, o.open);
            Classes.addClass(this.target, o.close);
            setTimeout(function() {
                Classes.removeClass(ele, o.active);
                Classes.removeClass(self.toggler, o.active);
                Classes.removeClass(target, o.active);
                self.toggler.setAttribute('aria-expanded', 'false');
                target.setAttribute('aria-expanded', 'false');
            }, 600);
            /*switch (o.effect) {
                case 'fade':
                    target.fadeOut('fast');
                    break;
                case 'slide':
                    target.slideUp('fast');
                    break;
                default:
                    target.hide();
            }*/
            var e2 = AcmsEvent.createCustom(EVENT_CLOSED, relatedTarget);
            AcmsEvent.dispatch(this.element, e2);
            AcmsEvent.dispatch(this.toggler, e2);
            AcmsEvent.dispatch(this.target, e2);
            if (typeof o.onUp === 'function') {
                o.onUp(ele, target);
            }
        },
        enable: function () {
            var relatedTarget = {relatedTarget: this},
            e = AcmsEvent.createCustom(EVENT_ENABLE, relatedTarget);
            AcmsEvent.dispatch(this.element, e);
            AcmsEvent.dispatch(this.target, e);
            AcmsEvent.dispatch(this.toggler, e);
            if (e.defaultPrevented) {
                return;
            }
            Classes.toggleClass(this.toggler, this.options.disabled);
            this.toggler.setAttribute('aria-disabled', 'true');
            var e2 = AcmsEvent.createCustom(EVENT_ENABLED, relatedTarget);
            AcmsEvent.dispatch(this.toggler, e2);
            AcmsEvent.dispatch(this.target, e2);
            AcmsEvent.dispatch(this.element, e2);
        },
        disable: function () {
            var relatedTarget = {relatedTarget: this},
            e = AcmsEvent.createCustom(EVENT_DISABLE, relatedTarget);
            AcmsEvent.dispatch(this.element, e);
            AcmsEvent.dispatch(this.target, e);
            AcmsEvent.dispatch(this.toggler, e);
            if (e.defaultPrevented) {
                return;
            }
            Classes.toggleClass(this.toggler, this.options.disabled);
            this.toggler.setAttribute('aria-disabled', 'true');
            var e2 = AcmsEvent.createCustom(EVENT_DISABLED, relatedTarget);
            AcmsEvent.dispatch(this.toggler, e2);
            AcmsEvent.dispatch(this.target, e2);
            AcmsEvent.dispatch(this.element, e2);
        },
        /**
         * Prüft ob das Dropdown Menu gerade offen ist
         * 
         * @returns {Boolean}
         */
        isOpen: function () {
            return this._isOpen;
        },
        isDisabled: function () {
            Classes.hasClass(this.toggler, this.options.disabled);
        },
        listen: function () {
            var self = this, o = this.options;
            var toggleClick = function(e) {
                e.preventDefault();
                return self.toggle(e);
            };
            AcmsEvent.add(this.toggler, 'click', toggleClick);
            var dropdownOnDocumentClick = function(e) {
                
               return self.checkClick(e);
            }
            AcmsEvent.add(Doc, 'click', dropdownOnDocumentClick);
        },
        
        _prepare: function () {
            if(!this.target) return false;
            if (this.target && this.target.getAttribute('id')) {
                return false;
            }
            var target = this.target,
                id = Utils.generateUniqeId();
            this.target.setAttribute('id', id);
            this.targetID = id;
        },
        checkClick: function (e) {
            e = e || window.event;
            if (!e) {
                return true;
            }
            if (!this.isOpen()) {
                return true;
            }
            var o = this.options,
                target = e.target,
                self = this,
                filter = function(el) {
                    if(typeof el.getAttribute !== 'undefined' && el.getAttribute('id') == self.targetID) {
                        return true;
                    }
                    if(el == self.toggler) {
                        return true;
                    }
                };
            var parent = Selectors.parents(target, filter);
            if (parent.length > 0) {
                return true;
            }
            if(Classes.hasClass(this.element, 'keep-open')) {
                return true;
            }
            this.close();
        }
        
    };

    return Dropdown;

});
