/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */



/**
 * Application Launcher Modul
 * 
 * Das Application Launcher Menu ist eine einfache Nav-Bar mit einem Icon-Menu 
 * rechts und einem App-Menu (im Beispiel ein Profil-Menu) links.
 * 
 * @param {Object} Utils    The Utils Modul for simple extending the 
 *                         custom options
 * @returns {ApplicationLauncher} 
 */
define('ui/navs/application-launcher', [
    'tools/utils',
    'core/selectors',
    'core/classes',
    'events/event'
], function (Utils, Selectors, Classes, AcmsEvent) {

    "use strict";

    /**
     * Constructor
     * 
     * @param {Element} element         Das HTML Element des Application Launcher
     * @param {Object|NULL} options     Optionen
     * 
     * @returns {ApplicationLauncher}
     */
    var ApplicationLauncher = function (element, options) {
        this.element =
        this.appMenu =
        this.appWrapper =
        this.menuToggle =
        this.options = null;
        this.initialize(element, options);
    };
    
    ApplicationLauncher.MODULE = "Application Launcher";
    
    ApplicationLauncher.VERSION = "1.5.0";
    
    ApplicationLauncher.NS = "acms.ui.navs.application-launcher";

    /**
     * Standard-Optionen
     * 
     * @var {Object}
     */
    ApplicationLauncher.DEFAULT_OPTIONS = {
        opt1: '',
        opt2: 0,
        onShow: function(menu, wrapper, toggle, self) {}
    };

    /**
     * Initialisierung des Modul Launcher
     * 
     * @var {Boolean}
     */
    ApplicationLauncher.needInit = true;

    /**
     * Modul-Initialisierung
     * 
     * @param {HTMLElement} element     Das HTML Element
     * @param {Object|NULL} options     Eigene Optionen
     * 
     * @returns {ApplicationLauncher}   Die Instanz des Launchers
     */
    ApplicationLauncher.init = function (element, options) {
        var al = new ApplicationLauncher(element, options);
        al.element.acmsData(ApplicationLauncher.NS, al);
        
        return al;
    };
    
    /**
     * CSS Datei des Moduls
     * 
     * @var {String}
     */
    ApplicationLauncher.css = "media/css/navs/application-launcher.min.css";

    /**
     * Module prototype
     */
    ApplicationLauncher.prototype = {
        constructor: ApplicationLauncher,

        /**
         * Interner Constructor
         * 
         * @param {Element} element The HTML Element requiring the module
         * @param {Object|NULL} options     Custom options, if used inside another Module. in most cases this will be NULL
         * 
         * @returns {ApplicationLauncher}
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },
        
        listen: function() {
            var self = this, 
                element = self.element,
                o = this.options;
            if(!self.menuToggle) {
                return;
            }
            AcmsEvent.add(self.menuToggle, 'click', self._onToggleClick.bind(self));
        },

        /**
         * Bilden der Standard-Optionen
         * 
         * @param {Object|NULL} options
         * @returns {Object}
         */
        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },

        /**
         * Gibt die Standard-Optionen zurück
         * 
         * @returns {ApplicationLauncher.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return ApplicationLauncher.DEFAULT_OPTIONS;
        },
        
        _onToggleClick: function(e) {
            var self = this,
                o = self.options;
            Classes.toggleClass(self.menuToggle, 'active');
            if(Classes.hasClass(self.menuToggle, 'active')) {
                self.menuToggle.setAttribute('aria-expanded', 'true');
                self.appWrapper.setAttribute('aria-hidden', 'false');
            } else {
                self.menuToggle.setAttribute('aria-expanded', 'false');
                self.appWrapper.setAttribute('aria-hidden', 'true');
            }
        },
        
        _prepare: function() {
            var self = this,
                element = self.element,
                o = self.options;
            self.appWrapper = Selectors.q('.application-wrapper', element);
            self.appMenu = Selectors.q('.application-menu', element);
            self.menuToggle = Selectors.q('.application-toggle', element);
            
        }
    };

    return ApplicationLauncher;
    
});
