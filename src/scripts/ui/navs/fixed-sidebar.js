/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */

/**
 * <h2>Fixed Sidebar</h2>
 * 
 * <p>
 *  JavaScript Seitige Unterstützung für die Fixed Sidebar zum wechseln des 
 *  open-State und anpassen der aria-Attribute.
 * </p>
 * 
 * @param {Object} Utils
 * @param {Object} Classes
 * @param {Object} Selectors
 * @param {Object} AcmsEvent
 * 
 * @returns {FixedSidebar}
 */
define('ui/navs/fixed-sidebar', [
    'tools/utils',
    'core/classes',
    'core/selectors',
    'events/event'
], function (Utils, Classes, Selectors, AcmsEvent) {

    'use strict';

    /**
     * <h3>Constructor</h3>
     * 
     * <p>
     *  Erstellt eine neue Sidebar instanz auf dem übergebenen Element.
     * </p>
     * 
     * @param {Object} element      Das Sidebar Element
     * @param {Object} options      Optionale eigene Optionen
     * 
     * @returns {FixedSidebar}
     */
    var FixedSidebar = function (element, options) {
        this.element =
                this.options =
                this.button =
                this.state =
                this.container = null;
        this.initialize(element, options);
    };

    /**
     * Modul Name
     *
     * @var {String}
     */
    FixedSidebar.MODULE = "Fixed Sidebar";

    /**
     * Modul Version
     *
     * @var {String}
     */
    FixedSidebar.VERSION = "1.5.0";
    
    /**
     * Modul Namespace
     *
     * @var {String}
     */
    FixedSidebar.NS = "acms.ui.navs.fixed-sidebar";

    /**
     * Standard Optionen
     * 
     * @var {Object}
     */
    FixedSidebar.DEFAULT_OPTIONS = {
        active: 'active',
        beforeOpen: function () {
            return true;
        },
        onOpen: function () {
            return true;
        },
        beforeClose: function () {
            return true;
        },
        onClose: function () {
            return true;
        },
        iconActive: 'acms-icon-chevron-left',
        iconInactive: 'acms-icon-bars',
        sidebarMenu: '.sidebar-menu',
        toggle: '.parent'
    };
    
    /**
     * Einbinden des Modul Loaders
     * 
     * @var {Boolean}
     */
    FixedSidebar.needInit = true;

    /**
     * <h3>Modul Initialisierung</h3>
     * 
     * @param {Element} element    Das Sidebar Element
     * @param {Object}  options    Optionen
     * 
     * @returns {FixedSidebar}
     */
    FixedSidebar.init = function (element, options) {
        var fs = new FixedSidebar(element, options);

        return fs;
    };

    /**
     * Individuelles Stylesheet
     * 
     * @var {string}
     */
    FixedSidebar.css = "media/css/navs/fixed-sidebar.min.css";

    /**
     * Klassen-Definitionen
     */
    FixedSidebar.prototype = {
        constructor: FixedSidebar,

        /**
         * <h3>Interner Constructor</h3>
         *
         * @param {Element} element     Das Sidebar Element
         * @param {Object}  options     Die Optionen
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },
        
        /**
         * <h3>Gibt die Standard-Optionen zurück</h3>
         * 
         * @returns {FixedSidebar.DEFAULT_OPTIONS}
         */
        getDefaultoptions: function () {
            return FixedSidebar.DEFAULT_OPTIONS;
        },

        /**
         * Bildet die Optionen
         *
         * <p>
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         * </p>
         *
         * @param {Object} options
         *
         * @returns {FixedSidebar.DEFAULT_OPTIONS|Object}
         */
        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultoptions(), options, this.element.acmsData());
        },

        /**
         * <h3>Aufsetzen der Event Listener</h3>
         * 
         * <p>
         *  Setzt den Event Listener für den Sidebar-Toggle auf
         * </p>
         */
        listen: function () {
            var self = this, button = self.button, cb = function (e) {
                e.preventDefault();
                self.toggle();
                return false;
            };
            AcmsEvent.add(button, 'click', cb);
        },

        /**
         * <h3>Wechselt den Status</h3>
         * 
         * <p>
         *  Öffnet oder schließt die Sidebar je nach Status
         * </p>
         */
        toggle: function () {
            var fn = this.state === false ? 'open' : 'close';

            return this[fn]();
        },
        
        /**
         * <h3>Öffnen der Sidebar</h3>
         * 
         * @param {Event} e  Optionales Event
         * 
         * @returns {undefined|Boolean}
         */
        open: function (e) {
            var self = this,
                    button = self.button,
                    element = self.element,
                    o = self.options;
            if (self.state === true) {
                return;
            }
            self.state = true;
            if (typeof o.beforeOpen === 'function') {
                if (!o.beforeOpen()) {
                    return false;
                }
                ;
            }
            element.setAttribute('aria-hidden', 'false');
            Classes.addClass(element, o.active);
            var i = Selectors.q('.acms-icon', button);
            Classes.removeClass(i, o.iconInactive);
            Classes.addClass(i, o.iconActive);
            Classes.addClass(button, 'active');
            if (typeof o.onOpen === 'function') {
                o.onOpen();
            }
        },
        
        /**
         * <h3>Schließen der Sidebar</h3>
         * 
         * @param {Event} e  Optionales Event
         * 
         * @returns {undefined|Boolean}
         */
        close: function (e) {
            var self = this,
                    button = self.button,
                    element = self.element,
                    o = self.options;
            if (self.state === false) {
                return;
            }
            self.state = false;
            if (typeof o.beforeClose === 'function') {
                if (!o.beforeClose()) {
                    return false;
                }
            }
            element.setAttribute('aria-hidden', 'true');
            Classes.removeClass(element, o.active);
            var i = Selectors.q('.acms-icon', button);
            Classes.removeClass(i, o.iconActive);
            Classes.addClass(i, o.iconInactive);
            Classes.removeClass(button, 'active');
            if (typeof o.onClose === 'function') {
                o.onClose();
            }
        },
        
        /**
         * <h3>Vorbereiten der Sidebar</h3>
         */
        _prepare: function () {
            var self = this,
                    element = this.element,
                    o = self.options;

            this.state = Classes.hasClass(element, o.active);
            this.button = Selectors.q('.sidebar-menu-toggle');
            this.sidebarMenu = Selectors.q(o.sidebarMenu);
            if (this.sidebarMenu) {
                var items = Selectors.qa('.parent', this.sidebarMenu),
                        childs = Selectors.qa('.child', this.sidebarMenu), j, i,
                        cb = function (event) {

                            var target = event.target;
                            if (target.tagName.toLowerCase() !== 'li') {
                                target = Selectors.closest(target, 'li');
                            }
                            if (!Classes.hasClass(target, 'parent')) {
                                return;
                            }
                            event.preventDefault();
                            var state = target.acmsData('state'),
                                    index = parseInt(target.acmsData('index')),
                                    open = false, k;
                            if (state == 'true') {
                                Classes.removeClass(target, 'active');
                                target.setAttribute('aria-hidden', 'true');
                                target.acmsData('state', false);
                            } else {
                                open = true;
                                Classes.addClass(target, 'active');
                                target.setAttribute('aria-hidden', 'false');
                                target.acmsData('state', true);
                            }
                            if (open === true) {
                                for (k = 0; k < items.length; k++) {
                                    if (k === index) {
                                        continue;
                                    }
                                    var item = items[k];
                                    if (item.acmsData('state') == 'true') {
                                        Classes.removeClass(item, 'active');
                                        item.setAttribute('aria-hidden', 'true');
                                        item.acmsData('state', false);
                                        var chld = Selectors.qa('.child', item);
                                        for (var l = 0; l < chld.length; l++) {
                                            Classes.removeClass(chld[l], 'active');
                                            chld[l].setAttribute('aria-hidden', 'true');
                                            chld[l].acmsData('state', false);
                                        }
                                    }
                                }
                            }
                            return false;
                        },
                        cb2 = function (evt) {
                            var ziel = evt.target;
                            if (ziel.tagName.toLowerCase() !== 'li') {
                                ziel = Selectors.closest(ziel, 'li');
                            }
                            if (!Classes.hasClass(ziel, 'child')) {
                                return;
                            }
                            evt.preventDefault();
                            var status = ziel.acmsData('status'),
                                    ind = parseInt(ziel.acmsData('index')),
                                    open = false, m;
                            if (status == 'true') {
                                Classes.removeClass(ziel, 'active');
                                ziel.setAttribute('aria-hidden', 'true');
                                ziel.acmsData('status', false);
                            } else {
                                open = true;
                                Classes.addClass(ziel, 'active');
                                ziel.setAttribute('aria-hidden', 'false');
                                ziel.acmsData('status', true);
                            }
                            if (open === true) {
                                for (m = 0; m < childs.length; m++) {
                                    if (m === ind) {
                                        continue;
                                    }
                                    var itm = childs[m];
                                    if (itm.acmsData('status') == 'true') {
                                        Classes.removeClass(itm, 'active');
                                        itm.setAttribute('aria-hidden', 'true');
                                        itm.acmsData('status', false);
                                    }
                                }
                            }
                            return false;
                        };
                for (j = 0; j < items.length; j++) {
                    items[j].acmsData('state', Classes.hasClass(items[j], 'active'));
                    items[j].acmsData('index', j);
                    AcmsEvent.add(items[j], 'click', cb);
                }
                for (i = 0; i < childs.length; i++) {
                    childs[i].acmsData('status', Classes.hasClass(childs[i], 'active'));
                    childs[i].acmsData('index', i);
                    AcmsEvent.add(childs[i], 'click', cb2);
                }
            }
        }

    };

    return FixedSidebar;

});
