/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */

/**
 * Action List Group
 *
 * @param {Object} Utils        Utils Modul
 * @param {Object} Selectors    Selectors Modul
 * @param {Object} Classes      Classes Modul
 * @param {Object} AcmsEvent    AcmsEvent Modul
 * 
 * @returns {ActionListGroup}
 */
define("ui/components/actionlistgroup", [
    "tools/utils",
    "core/selectors",
    "core/classes",
    "events/event"
], function (Utils, Selectors, Classes, AcmsEvent) {

    "use strict";

    /**
     * Action List Group
     *
     * <p>
     *  Hilfs-Tool zum bilden von Action List Groups. The Plugin wurde designt um
     *  mit den List-Collections zu arbeiten, kann sich aber durch die Optionen
     *  auch mit anderen CSS-Listen arrangieren.
     * </p>
     *
     *
     * @param {Object} element The Element to build the Action List on
     * @param {Object} options The Options passed to the Element.
     *                         Note: Options can all be injected using the
     *                         data-attributes of the Element
     *
     * @returns {ActionListGroup}
     */
    var ActionListGroup = function (element, options) {
        this.options =
                this.element =
                this.settings =
                this.items =
                this.debug = null;
        this.initialized = false;
        this.options = ActionListGroup.DEFAULT_OPTIONS;
        this.initialize(element, options);
    };

    /**
     * Komponenten Version
     *
     * @var {String}
     */
    ActionListGroup.VERSION = '1.5.0';

    /**
     * Komponenten Name
     * @var {String}
     */
    ActionListGroup.MODULE = 'Action List-Group';

    /**
     * Namespace
     * 
     * @var {String}
     */
    ActionListGroup.NS = "acms.ui.components.actionlistgroup";

    /**
     * Event "changed"
     * 
     * @var {String}
     */
    ActionListGroup.EVENT_CHANGED = ActionListGroup.NS + '.changed';

    /**
     * Component Default Options
     * =========================
     *
     * itemclass:       {Selector}      CSS-Klasse der List-Items
     * ignoreclass:     {String}        Klassen-Name der zu ignorierenden 
     *                                  List-Items
     *                                  
     * iconchecked:     {String}        CSS-Klasse des ausgewählten Items
     *                                  
     * iconunchecked:   {String}        CSS-Klasse des nicht ausgewählten Items
     *
     * onChange:        {Callable}      Callback-Funktion wenn ein item seinen 
     *                                  Status ändert. Muss Boolean zurückgeben.
     */
    ActionListGroup.DEFAULT_OPTIONS = {
        itemclass: '.collection-item',
        ignoreclass: 'disabled-collection-item',
        iconchecked: 'acms-icon acms-icon-check-square',
        iconunchecked: 'acms-icon acms-icon-square',

        onChange: function (item, checkbox, state) {
            return true;
        }
    };

    ActionListGroup.needInit = true;

    /**
     * Action List Group
     * s
     * @param {Object} element
     * @param {Object} options
     *
     * @returns {ActionListGroup}
     */
    ActionListGroup.init = function (element, options) {
        var ac = new ActionListGroup(element, options);
        ac.element.acmsData(ActionListGroup.NS, ac);

        return ac;
    };

    ActionListGroup.prototype = {

        widgets: [],

        /**
         * Constructor
         * 
         * @param {HTMLElement} element List-Group Element
         * @param {Object} options Optionen (siehe ActionListGroup.DEFAULT_OPTIONS
         *                         für mögliche Optionen)
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },

        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {ActionListGroup.DEFAULT_OPTIONS|Object}
         */
        getDefaultOptions: function () {
            return ActionListGroup.DEFAULT_OPTIONS;
        },
        
        /**
         * Bildet die Optionen
         *
         * Basierend auf den Standard-Optionen, den übergebenen Optionen des
         * Constructors und den Element Data-Attributen werden die aktuellen
         * Konfigurationen gebildet. Letztenendes gewinnen die Element
         * Data-Attribute.
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function (options) {

            return Utils.extend(
                    {},
                    this.getDefaultOptions(),
                    options,
                    this.element.acmsData());
        },

        /**
         * Aufsetzen der Event Listener
         * 
         * 
         */
        listen: function () {
            var self = this,
                    o = self.options,
                    element = self.element, items, item, j, checkbox, color, style, settings;
            items = Selectors.qa(o.itemclass, element);
            for (j = 0; j < items.length; j++) {
                item = items[j];
                if (Classes.hasClass(item, o.ignoreclass)) {
                    Classes.addClass(item, 'cursor-not-allowed');
                    continue;
                }
                checkbox = document.createElement('input');
                checkbox.type = 'checkbox';
                color = (item.acmsData('color') ? item.acmsData('color') : "primary");
                style = (item.acmsData('style') === "button" ? "button " : "collection-item-");
                Classes.addClass(item, 'cursor-pointer');
                item.appendChild(checkbox);
                if (item.acmsData('checked') == true || Classes.hasClass(item, 'active')) {
                    checkbox.checked = true;
                    item.acmsData('checked', true);
                }
                var data = item.acmsData(), k;
                for (k in data) {
                    if (data.hasOwnProperty(k)) {
                        checkbox.acmsData(k, data[k]);
                    }
                }
                var icn = Selectors.q('.state-icon', item);
                if (!icn) {
                    icn = document.createElement('i');
                    if (item.firstChild)
                        item.insertBefore(icn, item.firstChild);
                    Classes.addClass(icn, 'state-icon');
                    var state = (checkbox.checked ? 'on' : 'off');
                    Classes.addClass(icn, self.settings[state].icon);
                }
                Classes.addClass(checkbox, 'hidden');
                AcmsEvent.add(item, 'click', self._onClick.bind(self));
                AcmsEvent.add(checkbox, 'change', self.updateDisplay.bind(self));

            }
            self.initialized = true;
        },

        updateDisplay: function (e) {
            var self = this,
                    o = self.options,
                    checkbox = e.target;
            var isChecked = checkbox.checked,
                    item = Selectors.closest(checkbox, o.itemclass), icn, newState, oldState, color, style;
            newState = (isChecked) ? "on" : "off";
            oldState = (isChecked) ? "off" : "on"
            item.acmsData('state', (isChecked) ? "on" : "off");

            icn = Selectors.q('.state-icon', item);
            if (icn) {
                Classes.toggleClass(icn, self.settings['on'].icon);
                Classes.toggleClass(icn, self.settings['off'].icon);
            }
            color = (item.acmsData('color') ? item.acmsData('color') : "primary");
            style = (item.acmsData('style') === "button" ? "button " : "collection-item-");
            if (true === isChecked) {
                Classes.addClass(item, style + color + ' active');
            } else {
                Classes.removeClass(item, style + color + ' ' + color + ' active');
            }
        },

        _onClick: function (e) {
            var self = this,
                    o = self.options,
                    item = e.target, checkbox;
            if (!Selectors.matches(item, o.itemclass)) {
                item = Selectors.closest(item, o.itemclass);
            }
            checkbox = Selectors.q('input[type=checkbox]', item);
            checkbox.checked = !checkbox.checked;
            if (checkbox.checked) {
                checkbox.setAttribute('checked', 'checked');
            } else {
                checkbox.removeAttribute('checked');
            }
            AcmsEvent.fireChange(checkbox);

            if (true === self.initialized) {
                var evt = AcmsEvent.createCustom(ActionListGroup.EVENT_CHANGED, {widget: item}), res = true;
                AcmsEvent.dispatch(item, evt);
                if (typeof o.onChange === 'function') {
                    res = o.onChange(item, checkbox, checkbox.checked);
                }
                if (e.defaultPrevented || true !== res) {
                    checkbox.checked = !checkbox.checked;
                    if (checkbox.checked) {
                        checkbox.setAttribute('checked', 'checked');
                    } else {
                        checkbox.removeAttribute('checked');
                    }
                    AcmsEvent.fireChange(checkbox);
                }
                ;
            }
        },
        
        /**
         * Vorbereiten
         * 
         * 
         */
        _prepare: function () {
            var self = this,
                    o = self.options
            self.settings = {
                on: {
                    icon: o.iconchecked
                },
                off: {
                    icon: o.iconunchecked
                }
            };
        }

    };

    return ActionListGroup;
});
