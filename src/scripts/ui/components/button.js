/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */

/**
 * 
 * @param {type} Utils
 * @param {type} Classes
 * @param {type} Selectors
 * @param {type} AcmsEvent
 * @returns {Button}
 */
define("ui/components/button", [
    "tools/utils",
    "core/classes",
    "core/selectors",
    "events/event"
], function(Utils, Classes, Selectors, AcmsEvent){

    'use strict';

    /**
     * Buttons Komponente
     * 
     * Eine Hilfs-Komponente für Radia/Checkbox inputs die als Button 
     * dargestellt werden. Das Modul sucht alle Inputs aus dem übergebenen 
     * HTMLElement und hilft beim setzen der checked Attribute. Zusätzlich bietet 
     * das Modul Hooks und Events für weitere Aktionen.
     *
     * @param {HTMLElement}     element     Das Button-Wrapper Element
     * @param {Object|NULL}     options     Eigene Optionen oder NULL
     * 
     * @returns {Buttons}
     * 
     * @exports ui/components/button
     */
    var Buttons = function (element, options) {
        this.element =
        this.inputs =
        this.isLoading =
        this.options = null;

        this.initialize(element, options);
    };

    /**
     * Module Version
     *
     * @type {String}
     */
    Buttons.VERSION  = '1.5.0';

    /**
     * Module Name
     *
     * @type {String}
     */
    Buttons.MODULE  = 'Button Toggle';
    
    /**
     * Module Namespace
     *
     * @type {String}
     */
    Buttons.NS  = "acms.ui.components.button";
    
    /**
     * Event "onchange"
     * 
     * Das Event wird auf dem übergebenen HTMLElement getriggert, wenn ein 
     * Button toggle ausgeführt wurde.
     *
     * @type {String}
     */
    Buttons.EVENT_ONCHANGE = Buttons.NS + '.onchange';

    /**
     * Default Options
     * 
     * | Option             | Beschreibung                                                              | Typ       |
     * |--------------------|---------------------------------------------------------------------------|-----------|
     * | onChange           | on Change Callback nachdem der Toggle mit Änderung durchgeführt wurde.    | Callable  |
     * | loadingText        | Text der den "Loading..." Status kennzeichnet                             | String    |
     *
     * @type {Object}
     */
    Buttons.DEFAULT_OPTIONS = {
        loadingText: ((typeof Acms !== 'undefined' && 
                        typeof Acms.Translator !== 'undefined' && 
                        typeof Acms.Translator._ !== 'undefined'
                        ) ? 
                          Acms.Translator._('Loading...') : 
                          'Loading...'),
        
        /**
         * OnChange Callback
         * 
         * @param {Buttons} self
         * @param {HTMLElement} button
         * @param {HTMLElement|NULL} $parent
         * @param {HTMLElement|NULL} $input
         * 
         * 
         */
        onChange: function(self, button, $parent, $input) {}
    };
    
    /**
     * Modul Loader einbinden
     * 
     * @type {Boolean}
     */
    Buttons.needInit = true;
    
    /**
     * Modul Initialisierung
     *
     * @param {HTMLElement}     element     Das Button-Wrapper Element
     * @param {Object|NULL}     options     Eigene Optionen oder NULL
     * 
     * @returns {Buttons}
     */
    Buttons.init = function(element, options) {
        var btn = new Buttons(element, options);
        btn.element.acmsData(Buttons.NS, btn);
    };

    /**
     * Button Class Body
     */
    Buttons.prototype = {
        
        constructor: Buttons,
        
        /**
         * Interner Constructor
         * 
         * @param {Object} element
         * @param {Object} options
         * 
         */
        initialize: function(element, options) {
            this.element  = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },
        
        /**
         * Gibt die Standard-Optionen zurück
         * 
         * @returns {Buttons.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function() {
            return Buttons.DEFAULT_OPTIONS;
        },

        /**
         * Bildet die Optionen
         *
         * <p>
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         * </p>
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function(options) {
            return Utils.extend(
                    {},
                    this.getDefaultOptions(),
                    options,
                    this.element.acmsData()
                    );
        },
        
        /**
         * Setzen des Button State
         * 
         * @param {String} state    Neuer State
         * 
         */
        setState: function (state) {
            var d    = 'disabled',
                self = this,
                $el  = self.element,
                val  = $el.tagName.toLowerCase() === 'input' ? 'value' : 'innerHTML',
                data = $el.acmsData();
            state = state + 'Text';
            if (data.resetText === null) $el.acmsData('resetText', $el[val]());
            setTimeout(function () {
                $el[val](data[state] === null ? self.options[state] : data[state]);
                if (state === 'loadingText') {
                  self.isLoading = true;
                  Classes.addClass($el,d);
                  $el.setAttribute(d, d);
                } else if (self.isLoading) {
                  self.isLoading = false;
                  Classes.removeClass($el, d);
                  $el.removeAttribute(d);
                }
            }, 0);
        },
        
        /**
         * Toggle des übergebenen Buttons
         * 
         * @param {HTMLElement}     button
         * 
         */
        toggle: function(button) {
            var self = this,
                changed = true,
                element = self.element,
                o = self.options,
                $parent =  Selectors.closest(button, '[data-toggle="buttons"]'),
                $input = null;
            
            if ($parent) {
                $input = Selectors.q('input', button);
                if($input &&
                        $input.type && 
                        $input.type.trim().toLowerCase() === 'radio') {
                    if ($input.checked && Classes.hasClass(element,'active')) {
                        changed = false;
                    } else {
                        var active = $parent.querySelector('.active');
                        if(active) Classes.removeClass(active, 'active');
                    }
                }
                if ($input && changed) {
                    var checked = !Classes.hasClass(button, 'active');
                    $input.checked = checked;
                    $input.setAttribute('checked', checked);
                    AcmsEvent.fireChange($input);
                }
            } else {
                element.setAttribute('aria-pressed', !Classes.hasClass(element, 'active'));
            }
            if (changed) {
                Classes.toggleClass(button, 'active');
                o.onChange(self, button, $parent, $input);
                var evt = AcmsEvent.createCustom(Buttons.EVENT_ONCHANGE, {
                    relatedTarget: button,
                    parent: $parent,
                    input: $input,
                    component: self
                });
                AcmsEvent.dispatch(element, evt);
            }
        },
        
        /**
         * Aufsetzen der Event Listener
         * 
         * 
         */
        listen: function() {
            var self = this,
                btns = Selectors.qa('button, .button', this.element), 
                i;
            for(i = 0; i < btns.length; i++) {
                AcmsEvent.add(btns[i], 'click', self._onClick);
            }
        },
        
        /**
         * Entfernen der Event Listener
         * 
         * 
         */
        off: function() {
            var self = this,
                btns = Selectors.qa('button, .button', this.element), 
                i;
            for(i = 0; i < btns.length; i++) {
                AcmsEvent.remove(btns[i], 'click', self._onClick);
            }
        },
        
        destroy: function() {
            var self = this;
            self.off();
            self.element.acmsData(Buttons.NS, null);
            self.element = null;
        },
        
        /**
         * OnClick Event Handler
         * 
         * @param {Event} e Das Event
         * 
         * @returns {Boolean}
         */
        _onClick: function(e) {
            e.preventDefault();
            var self = this, btn = e.target;
            if(!Classes.hasClass(btn, 'button')) {
                btn = Selectors.closest(btn, '.button');
            }
            if(!btn) {
                return false;
            }
            if(Classes.hasClass(btn, 'disabled')) {
                return false;
            }
            self.toggle(btn);
            return false;
        },
        
        /**
         * Vorbereiten
         * 
         * 
         */
        _prepare: function() {
            var self = this, 
                inputs = Selectors.qa('input', self.element), 
                i, input, btn;
            for(i = 0; i < inputs.length; i++) {
                input = inputs[i];
                if(Classes.hasClass(input, 'disabled') || 
                        input.getAttribute('disabled')) {
                    btn = Selectors.closest(input, '.button');
                    if(btn) Classes.addClass(btn, 'disabled');
                }
            }
            self.inputs = inputs;
            self.isLoading = false;
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        }
    };

    return Buttons;
});

