/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('ui/components/action-buttons', [
    'tools/utils',
    'core/selectors',
    'core/classes',
    'events/event',
    'core/acms'
], function (Utils, Selectors, Classes, AcmsEvent, A) {

    "use strict";

    /**
     * Action Buttons
     * 
     * Action Buttons sind einfache Call-To-Action Buttons für verschiedene
     * Zwecke. Da die Buttons selbst keinen klickbaren Link haben, bekommen 
     * sie dieses Modul als Unterstützung.
     * 
     * @param {HTMLElement}     element     Das HTML-Element
     * @param {Object}          options     Die Optionen
     * 
     * @link https://ui.amaryllis-support.de/content/components/action-buttons/ Tutorial
     * 
     * @exports ui/components/action-buttons
     */
    var ActionItems = function (element, options) {
        this.element =
                this.options =
                this.toggles = null;
        this.options = ActionItems.DEFAULT_OPTIONS;

        this.initialize(element, options);
    };

    /**
     * Modul Name
     * 
     * @var {String}
     */
    ActionItems.MODULE = "Action Items";

    /**
     * Modul Version
     * 
     * @var {String}
     */
    ActionItems.VERSION = "1.5.0";

    /**
     * Modul Namespace
     * 
     * @var {String}
     */
    ActionItems.NS = "acms.ui.action-buttons";

    /**
     * Standard-Optionen des Moduls
     * 
     * @var {Object}
     */
    ActionItems.DEFAULT_OPTIONS = {
        items: '.action-items .action-button',
        website: null,
        remote: null
    };

    /**
     * Sagt dem Modul-Loader, dass das Modul initialisiert werden muss, wenn es 
     * über diesen geladen wird.
     * 
     * @var {Boolean}
     */
    ActionItems.needInit = true;

    /**
     * Modul Initialisierung
     * 
     * @param {HTMLElement}     element     Das HTML-Element
     * @param {Object}          options     Die Optionen
     * 
     * @returns {ActionItems}
     */
    ActionItems.init = function (element, options) {
        var v = new ActionItems(element, options);
        v.element.acmsData(ActionItems.NS, v);

        return v;
    };
    

    /**
     * Klassen Definition
     */
    ActionItems.prototype = {

        constructor: ActionItems,

        /**
         * Interner Constructor
         * 
         * @param {HTMLElement}     element     Das HTML-Element
         * @param {Object}          options     Die Optionen
         * 
         * 
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },

        /**
         * Aufsetzen der Event-Listener
         * 
         */
        listen: function () {
            var self = this,
                    o = self.options,
                    element = self.element,
                    items = self.toggles;
            items.forEach(function(item) {
                if(!item.acmsData('target')) {
                    var link = Selectors.q('.links a', item);
                    if(link !== null) {
                        AcmsEvent.add(item, 'click', self._process.bind(self));
                    }
                }
            });
        },
        
        /**
         * Gibt die Standard-Optionen zurück
         * 
         * @returns {ActionItems.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return ActionItems.DEFAULT_OPTIONS;
        },

        /**
         * Bilden der Optionen
         * 
         * Bildet die Optionen basierend auf den Standard-Optionen, den 
         * übergebenen Optionen des Constructors und den Element Data-Attributen. 
         * Letztere gewinnen den Kampf, wenn gesetzt.
         * 
         * @param {Object} options    Die eignenen Optionen
         * @returns {Object|ActionItems.DEFAULT_OPTIONS}
         */
        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },
        
        /**
         * Durchführen der Click-Aktion
         * 
         * @param {Event}   e   Event
         */
        _process: function(e) {
            var item = Selectors.closest(e.target, '.action-button');
            var link = Selectors.q('.links a', item);
            if(!link) {
                return;
            }
            if(link.acmsData('external') === 'true' 
                    || link.acmsData('external') === true) {
                AcmsEvent.fireClick(link);
            } else {
                window.location.href = link.getAttribute('href');
            }
        },
        
        /**
         * Vorbereiten des Moduls
         * 
         * 
         */
        _prepare: function () {
            var self = this,
                    o = self.options,
                    element = self.element;
            self.toggles = Selectors.qa(o.items, element);
        }

    };

    return ActionItems;

});
