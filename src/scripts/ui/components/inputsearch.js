/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */

/**
 * Input-Search
 *
 * @param {Object} doc
 * @param {Object} Utils
 * @param {Object} AcmsEvent
 * @param {Object} Classes
 * @param {Object} Selectors
 * @param {Object} Style
 *
 * @returns {InputSearch}
 */
define("ui/components/inputsearch", [
    "core/document",
    "tools/utils",
    "events/event",
    "core/classes",
    "core/selectors",
    'core/style',
    'templates/template-engine'
], function (Doc, Utils, AcmsEvent, Classes, Selectors, Style, TemplateEngine) {

    "use strict";

    let INPUTSEARCH_DATA = "acms.ui.components.inputsearch",
            NS = "." + INPUTSEARCH_DATA,
            EVENT_SELECTED = "selected" + NS,
            EVENT_TOKEN_CREATE = "token-create" + NS,
            EVENT_TOKEN_CREATED = "token-created" + NS,
            EVENT_EDIT = "edit" + NS,
            EVENT_EDITED = "edited" + NS,
            EVENT_REMOVE = "remove" + NS,
            EVENT_REMOVED = "removed" + NS,
            EVENT_CHANGE = "change";

    /**
     * Constructor
     * 
     * Live-Suche Feld, welches mittels Autocompletet im Backend nach möglichen
     * Optionen sucht.
     *
     * @param {Object} element  Das Input-Element
     * @param {Object} options  Die eigenen Optionen
     *
     * @exports ui/components/inputsearch
     */
    let InputSearch = function (element, options) {
        this.element =
                this.options =
                this.input =
                this.mirror =
                this.copyHelper =
                this.textDirection =
                this._delimiters =
                this._disabled =
                this._firstDelimiter =
                this._readonly =
                this._triggerKeys =
                this.focused =
                this.preventInputFocus =
                this.preventDeactivation =
                this.preventCreateTokens =
                this.maxTokenWidth =
                this.limit =
                this.lastKeyUp =
                this.lastKeyDown =
                this.lastInputValue =
                this.ignorechange =
                this.wrapper = null;
        this.initialize(element, options);
    };

    /**
     * Komponenten Name
     *
     * @var {String}
     */
    InputSearch.MODULE = "Input-Search";

    /**
     * Standard-Optionen
     *
     * @var {Object}
     */
    InputSearch.DEFAULT_OPTIONS = {
        minLength: 3,
        allowEditing: false,
        allowPasting: false,
        allowCreating: false,
        limit: 0,
        autocomplete: {},
        autocompleteData: {},
        showAutocompleteOnFocus: false,
        createTokensOnBlur: false,
        delimiter: ',',
        beautify: true,
        inputType: 'text',
        remoteurl: null,
        tokenClass: 'token',
        tokenWrapper: 'tokens',
        tokenTemplate: 'templates/html/ui/form/field/inputsearch/token.hbs'
    };

    InputSearch.needInit = true;

    InputSearch.css = "media/css/components/inputsearch.min.css";

    InputSearch.init = function (element, options) {
        let IS = new InputSearch(element, options);
        IS.element.acmsData(INPUTSEARCH_DATA, IS);
        return IS;
    };

    /**
     * Klassen Definition
     */
    InputSearch.prototype = {
        /**
         * Constructor
         *
         * @var InputSearch
         */
        constructor: InputSearch,
        /**
         * Interner Constructor
         *
         * @param {Element} element
         * @param {Object} options
         * 
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            let self = this, o = self.options, ele = self.element;
            self.textDirection = o.textDirection ? o.textDirections : (Acms.General.isRTL ? 'right' : 'left');
            if (o.maxitems) {
                o.limit = o.maxitems;
            }
            self._delimiters = (typeof o.delimiter === 'string') ? [o.delimiter] : o.delimiter;
            self._triggerKeys = self._delimiters.map(function (delimiter, index, arr) {
                return delimiter.charCodeAt(0);
            });
            self._firstDelimiter = self._delimiters[0];
            let dash, whitespace;
            for (let j = 0; j < self._delimiters.length; j++) {
                if (self._delimiters[j] === ' ') {
                    whitespace = true;
                } else if (self._delimiters[j] === '-') {
                    dash = true;
                }
            }

            if (whitespace === true)
                self._delimiters.push('\\s');

            if (dash === true) {
                delete self._delimiters[dash];
                self._delimiters.unshift('-');
            }

            let specialCharacters = ['\\', '$', '[', '{', '^', '.', '|', '?', '*', '+', '(', ')'];
            self._delimiters.forEach(function (character, index) {
                let pos = specialCharacters.indexOf(character);
                if (pos >= 0)
                    self._delimiters[index] = '\\' + character;
            });
            let elStyleWidth = ele.style.width || 0,
                    elWidth = ele.offsetWidth;

            let hidingPosition = self.textDirection,
                    originalStyles = {position: window.getComputedStyle(ele, null).getPropertyValue('position')};
            originalStyles[hidingPosition] = parseInt(window.getComputedStyle(ele, null).getPropertyValue(hidingPosition));

            ele
                .acmsData('original-styles', originalStyles);
            ele
                .acmsData('original-tabindex', ele.tabindex || -1);
            ele.style.position = 'absolute';
            ele.style[hidingPosition] = '-10000px';
            ele.tabindex = -1;
            self.wrapper = document.createElement('div');
            Classes.addClass(self.wrapper, 'inputsearch v1');
            self.tokenWrapper = document.createElement('div');
            self.wrapper.appendChild(self.tokenWrapper);
            Classes.addClass(self.tokenWrapper, o.tokenWrapper);
            self.input = document.createElement('input');
            Classes.addClass(self.input, 'token-input field');
            self.input.type = o.inputType || 'text';
            self.input.autocomplete = "off";
            if (Classes.hasClass(ele, 'large'))
                Classes.addClass(self.input, 'large');
            if (Classes.hasClass(ele, 'small'))
                Classes.addClass(self.input, 'small');
            let id = ele.getAttribute('id') || new Date().getTime() + '' + Math.floor((1 + Math.random()) * 100);
            self.input.setAttribute('id', 'inputsearch-' + id);
            self.wrapper.appendChild(self.input);
            self.input.placeholder = ele.placeholder || '';
            self.input.setAttribute('aria-autocomplete', 'list');
            self.input.setAttribute('autocorrect', 'off');
            self.input.setAttribute('autocapitalize', 'off');
            self.input.setAttribute('spellcheck', 'off');
            self.input.setAttribute('name', ele.name + '-inputsearch');
            self.input.name = ele.name + '-inputsearch';
            self.input.tabindex = ele.acmsData('original-tabindex');
            let label = document.querySelector('label[for="' + ele.id + '"]');
            if (label) {
                label.for = self.input.id;
            }
            self._buildCopyHelper(hidingPosition);

            if (ele.disabled) {
                self.disable();
            }
            if (ele.readonly) {
                self.readonly();
            }
            self.mirror = document.createElement('span');
            self.mirror.style.position = 'absolute';
            self.mirror.style.top = '-999px';
            self.mirror.style.left = '0';
            self.mirror.style.whiteSpace = 'pre';
            
            document.body.appendChild(self.mirror);
            let fGroup = Selectors.closest(ele, '.form-group');
            fGroup.appendChild(self.wrapper);
            //self.update();
            self.setTokens(o.tokens, false, !ele.value && o.tokens, () => {
				
			});
			self.listen();
            if (o.remoteurl) {
                self._initializeAutoComplete();
            }
        },
        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {InputSearch.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return InputSearch.DEFAULT_OPTIONS;
        },
        /**
         * Bilden der eigenen Optionen
         *
         * @param {Object} options  Optionen
         *
         * @returns {Object}
         */
        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },
        /**
         *
         * @param {Object|String|Number} attrs  Ein Objekt mit value und label
         *                                      Keys, oder nur das Value, wenn
         *                                      kein Label.
         * @param {Boolean}     triggerChange   Doll das Change-Event gefeuert werden?
         * @param {CallableFunction} callback   
         *
         * 
         */
        createToken: function (attrs, triggerChange, callback) {
            let self = this, o = this.options;
            if (typeof attrs === 'string') {
                attrs = {value: attrs, label: attrs};
            } else {
                attrs = Utils.extend({}, attrs);
            }
            if (typeof triggerChange === 'undefined') {
                triggerChange = true;
            }
            if(typeof triggerChange !== 'boolean') {
                triggerChange = true;
            }

            attrs.label = attrs.label && attrs.label.length ? attrs.label.trim() : attrs.value;
            if (!attrs.value || !attrs.label.length || attrs.label.length <= o.minLength) {
                return;
            }
            if (o.limit && self.getTokens().length >= o.limit) {
                return;
            }

            let e = AcmsEvent.createCustom(EVENT_TOKEN_CREATE, {attrs: attrs});
            AcmsEvent.dispatch(self.element, e);
            if ((e.detail && !e.detail.attrs) || e.defaultPrevented)
                return;
            let data = {
                    token: {
                        label: attrs.label,
                        value: attrs.value
                    }
                },
                cb = () => {
                    if(callback) callback();
                    return self._onCreateToken(attrs, triggerChange);
                };
           
            TemplateEngine.compileResource(o.tokenTemplate, data, cb, self.tokenWrapper);

            return true;
        },

        _onCreateToken: function(attrs, triggerChange) {
            let self = this, o = this.options;
            let token = Selectors.q('[data-value="'+attrs.value+'"]', self.tokenWrapper), close;
            if(!token) {
                return;
            }
            close = Selectors.q('[data-role="close"]', token);
            token.acmsData('attrs', attrs);
            if(close) {
                close.acmsData('attrs', attrs);
            }
            AcmsEvent.addMultiple(token, ['touchstart','mousedown'], function (e) {
                if (self._disabled || self._readonly) {
                    return false;
                }
                self.preventDeactivation = true;
            });
            AcmsEvent.add(close, 'click', function (e) {
                e.preventDefault();
                let target = e.target, tkn = Selectors.closest(target, '.' + o.tokenClass);
                let evo = {attrs: self.getTokenData(tkn), relatedTarget: tkn},
                ev = AcmsEvent.createCustom(EVENT_REMOVE, evo),
                        ev2 = AcmsEvent.createCustom(EVENT_REMOVED, evo);
                AcmsEvent.dispatch(self.element, ev);
                if (ev.defaultPrevented) {
                    return false;
                }
                tkn.parentNode.removeChild(tkn);
                self.element.value = self.getTokensList();
                self.element.setAttribute('value', self.getTokensList());
                AcmsEvent.dispatch(self.element, ev2);
                self.ignorechange = true;
                AcmsEvent.fireChange(self.element);
                //self.update();
                self.input.focus();
                return false;
            });
            AcmsEvent.addMultiple(token, ['tap', 'click'], function (e) {
                if (self._disabled || self._readonly) {
                    return false;
                }
                let tkn = e.target;
                if (!Classes.hasClass(tkn, o.tokenClass)) {
                    tkn = Selectors.closest(tkn, '.' + o.tokenClass);
                }
                if (!tkn) {
                    return;
                }
                self.preventDeactivation = false;
                if (e.ctrlKey || e.metaKey) {
                    e.preventDefault();
                    return self.toggle();
                }
                self.activate(tkn, e.shiftKey, e.shiftKey);
            });
            AcmsEvent.addMultiple(token, ['tap', 'click'], function (e) {
                let tkn = e.target;
                if (!Classes.hasClass(tkn, o.tokenClass)) {
                    tkn = Selectors.closest(tkn, '.' + o.tokenClass);
                }
                if (!tkn) {
                    return;
                }
                if (self._disabled || self._readonly || !self.options.allowEditing)
                    return false;
                self.edit(tkn);
            });
            let ev3 = AcmsEvent.createCustom(EVENT_TOKEN_CREATE, {
                attrs: attrs,
                relatedTarget: token
            });
            AcmsEvent.dispatch(self.element, ev3);

            if (triggerChange) {
                let tokens = self.getTokensList();
                //console.log(tokens);
                self.element.value = tokens;
                self.element.setAttribute('value', tokens);
                self.ignorechange = true;
                AcmsEvent.fireChange(self.element);

            }
            //self.update();
        },

        /**
         * Setzt die Tokens
         *
         * @param {Object|String}    tokens         Die Tokens
         * @param {Boolean}          add
         * @param {Boolean}          triggerChange  Soll das Change Event gefeuert werden
         * @param {CallableFunction} callback       Callback will be called once the token has been appended
         * 
         * @returns {Element}   Das InputSearch Element
         */
        setTokens: function (tokens, add, triggerChange, callback) {
            let self = this, o = this.options;
            if (!add) {
                let tkens = Selectors.qa('.' + o.tokenClass, self.tokenWrapper), j;
                for (j = 0; j < tkens.length; j++) {
                    self.tokenWrapper.removeChild(tokens[j]);
                }
            }
            if (!tokens)
                return;

            if (typeof triggerChange === 'undefined') {
                triggerChange = true;
            }

            if (typeof tokens === 'string') {
                try {
                    let jtokens = JSON.parse(tokens);
                    tokens = jtokens;
                } catch (E) {
                    if (self._delimiters.length) {
                        tokens = tokens.split(new RegExp('[' + this._delimiters.join('') + ']'));
                    } else {
                        tokens = [tokens];
                    }
                }
			}
			let k, len = tokens.length, processed = 0;
            for (k = 0; k < len; k++) {
                self.createToken(tokens[k], triggerChange, () => {
					processed++;
                    if(processed == len || len === 1) {
                        if(callback) {
                            callback();
                        }
                    }
                });
            }

            return this.element;
        },
        /**
         * Gibt die Token-Daten eines Tokens zurück
         *
         * @param {Element|Node} token  Das Token HTML Element
         *
         * @returns {Object} Ein Objekt mit den Keys label und value
         */
        getTokenData: function (token) {
            if (!token) {
                return;
            }
            let data = token.acmsData('attrs');
            if (data && data.length == 1) {
                data = data[0];
            }

            return data;
        },
        /**
         * Gibt die Tokens als Array zurück
         *
         * @param {Boolean} active  Nur "aktive" Tokens?
         *
         * @returns {Array} Ein Array mit gefundenen Tokens
         */
        getTokens: function (active) {
            let self = this,
                o = self.options,
                    tokens = [],
                    activeClass = active ? '.active' : '',
                    eles = Selectors.qa('.' + o.tokenClass + activeClass, self.tokenWrapper), j;
            for (j = 0; j < eles.length; j++) {
                tokens.push(self.getTokenData(eles[j]));
            }
            return tokens;
        },
        /**
         * Gibt die Token Liste zurück
         *
         * @param {String}      delimiter   Der Delimeter
         * @param {Boolean}     beautify    Verschönern?
         * @param {Boolean}     active      Nur Aktive?
         *
         * @returns {String}    String mit einem Delimeter verbunden
         */
        getTokensList: function (delimiter, beautify, active) {
            delimiter = delimiter || this._firstDelimiter;
            beautify = (typeof beautify !== 'undefined' && beautify !== null) ? beautify : this.options.beautify;

            let separator = delimiter + (beautify && delimiter !== ' ' ? ' ' : ''),
                    tokens = this.getTokens(active);
            if (tokens.length) {
                return tokens.map((token) => {
                    return token ? token.value : '';
                }).join(separator);
            }
            return '';
        },
        /**
         * Gibt den Wert des Input zurück
         *
         * @returns {String}
         */
        getInput: function () {
            return this.input.value;
        },
        /**
         * Aufsetzen der Event-Listener
         *
         * 
         */
        listen: function () {
            let self = this;
            AcmsEvent.add(self.element, 'change', self.change.bind(self));
            AcmsEvent.add(self.wrapper, 'mousedown,touchstart,tap', self.focusInput.bind(self));
            AcmsEvent.add(self.input, 'focus', self.focus.bind(self));
            AcmsEvent.add(self.input, 'blur', self.blur.bind(self));
            AcmsEvent.add(self.input, 'paste', self.paste.bind(self));
            AcmsEvent.add(self.input, 'keydown', self.keydown.bind(self));
            AcmsEvent.add(self.input, 'keypress', self.keypress.bind(self));
            AcmsEvent.add(self.input, 'keyup', self.keyup.bind(self));

            AcmsEvent.add(self.copyHelper, 'focus', self.focus.bind(self));
            AcmsEvent.add(self.copyHelper, 'blur', self.blur.bind(self));
            AcmsEvent.add(self.copyHelper, 'keydown', self.keydown.bind(self));
            AcmsEvent.add(self.copyHelper, 'keyup', self.keyup.bind(self));

            AcmsEvent.add(self.input, 'keypress', self.update.bind(self));
            AcmsEvent.add(self.input, 'keyup', self.update.bind(self));

            AcmsEvent.add(window, 'resize', self.update.bind(self));
        },
        keydown: function (e) {

            if (!this.focused)
                return;
            let self = this,
                o = self.options,
                    KEYS = {
                        BACKSPACE: 8,
                        TAB: 9,
                        ENTER: 13,
                        ESCAPE: 27,
                        ARROW_LEFT: 37,
                        ARROW_UP: 38,
                        ARROW_RIGHT: 39,
                        ARROW_DOWN: 40,
                        A: 65,
                        COMMA: 188
                    };

            switch (e.keyCode) {
                case KEYS.BACKSPACE:
                    if (self.input != Doc.activeElement) {
                        break;
                    }
                    this.lastInputValue = this.input.value;
                    break;
                case KEYS.ARROW_LEFT:
                    leftRight(this.textDirection === 'rtl' ? 'next' : 'prev');
                    break;
                case KEYS.ARROW_UP:
                    upDown('prev');
                    break;
                case KEYS.ARROW_RIGHT:
                    leftRight(this.textDirection === 'rtl' ? 'prev' : 'next');
                    break;
                case KEYS.ARROW_DOWN:
                    upDown('next');
                    break;
                case KEYS.A:
                    if (this.input.value.length > 0 || !(e.ctrlKey || e.metaKey))
                        break;
                    this.activateAll();
                    e.preventDefault();
                    break;
                case KEYS.TAB:
                case KEYS.ENTER:
                    e.preventDefault();
                    if (this.input.acmsData('acms.ui.app.autocomplete') && 
                            this.input.acmsData('acms.ui.app.autocomplete').ul.querySelectorAll("li.selected").length)
                        break;
                    if (this.options.remoteurl && false === this.options.allowCreating) {
                        break;
                    }
                    if (self.input == Doc.activeElement && 
                            this.input.value.length || 
                            this.input.acmsData('edit')) {
                        return this.createTokensFromInput(e, this.input.acmsData('edit'));
                    }
                    if (e.keyCode === KEYS.ENTER) {
                        if (self.copyHelper != Doc.activeElement || 
                            this.wrapper.querySelectorAll('.'+ o.tokenClass +'.active').length !== 1) {
                            break;
                        }
                        if (!self.options.allowEditing) {
                            break;
                        }
                        this.edit(this.wrapper.querySelector('.'+ o.tokenClass +'.active'));
                    }
                    break;
                default:
                    break;
            }

            function leftRight(direction) {
                if (self.input == Doc.activeElement) {
                    if (self.input.value.length > 0) {
                        return;
                    }
                    direction += 'All';
                    let token = self.input[direction]('.'+ o.tokenClass +':first-child');
                    if (!token.length) {
                        return;
                    }
                    self.preventInputFocus = true;
                    self.preventDeactivation = true;
                    self.activate(token);
                    e.preventDefault();
                } else {
                    self[direction](e.shiftKey);
                    e.preventDefault();
                }
            }

            function upDown(direction) {
                if (!e.shiftKey)
                    return;
                let token;
                if (self.input == Doc.activeElement) {
                    if (self.input.value.length > 0)
                        return;
                    if (direction == 'prev') {
                        token = Selectors.previousAll(self.input);
                    } else {
                        token = Selectors.nextAll(self.input);
                    }
                    if (!token.length)
                        return;
                    self.activate(token);
                }
                /*
                 let opposite = direction === 'prev' ? 'next' : 'previous',
                 position = direction === 'prev' ? 'first' : 'last',
                 tokens = Selectors[opposite + 'All'](self.firstActiveToken, '.token');
                 self.firstActiveToken().forEach(function () {
                 self.deactivate(this);
                 });
                 self.activate(self.$wrapper.find('.token:' + position), true, true);
                 */
                e.preventDefault();
            }

            this.lastKeyDown = e.keyCode;
        },
        /**
         *
         * @param {Object} e
         * 
         */
        keypress: function (e) {
            /*if ($.inArray(e.which, this._triggerKeys) !== -1 && this.$input.is(Doc.activeElement)) {
             if (this.$input.val()) {
             this.createTokensFromInput(e)
             }
             return false;
             }*/
        },
        keyup: function (e) {
            this.preventInputFocus = false;
            let self = this,
            o = self.options;
            if (!self.focused) {
                return;
            }

            switch (e.keyCode) {
                /** Backspace **/
                case 8:
                    if (self.input == Doc.activeElement) {
                        if (self.input.value.length || self.lastInputValue.length && self.lastKeyDown === 8)
                            break;

                        self.preventDeactivation = true;
                        let prevToken = Selectors.previousAll(self.input, '.'+ o.tokenClass + ':first-child');

                        if (!prevToken.length)
                            break;

                        this.activate(prevToken);
                    } else {
                        this.remove(e);
                    }
                    break;
                    /** delete **/
                case 46:
                    this.remove(e, 'next');
                    break;
            }
            this.lastKeyUp = e.keyCode;
        },
        focus: function (e) {
            this.focused = true;
            Classes.addClass(this.wrapper, 'focus');
            if (this.input == Doc.activeElement) {
                let active = this.wrapper.querySelector('.active');
                if(active) Classes.removeClass(active, 'active');
                this.firstActiveToken = null;
            }
        },
        blur: function (e) {

            this.focused = false;
            Classes.removeClass(this.wrapper, 'focus');
            if (!this.preventDeactivation && 
                    false === (this.element != Doc.activeElement)) {
                let active = this.wrapper.querySelector('.active');
                if(active) Classes.removeClass(active, 'active');
                this.firstActiveToken = null;
            }

            if (!this.preventCreateTokens && (this.input.acmsData('edit') && this.input != (Doc.activeElement) || this.options.createTokensOnBlur)) {
                this.createTokensFromInput(e);
            }

            this.preventDeactivation = false;
            this.preventCreateTokens = false;
        },
        paste: function (e) {
            let self = this;

            if (self.options.allowPasting) {
                setTimeout(function () {
                    self.createTokensFromInput(e);
                }, 1);
            }
        },
        change: function (e) {
            if (e.initiator === 'inputsearch')
                return;
            if (typeof e.details === 'object' && e.details.initiator === 'inputsearch') {
                return;
            }
            if (this.ignorechange === true) {
                this.ignorechange = false;
                return;
            }
            this.setTokens(this.element.value, true, true, () => {});
        },
        createTokensFromInput: function (e, focus) {
            if (this.input.value.length < this.options.minLength) {
                return;
            }

            let tokensBefore = this.getTokensList();
            this.setTokens(this.input.value, true, true, () => {
                if (tokensBefore === this.getTokensList() && this.input.value.length) {
                    //return false;
                }
                this.input.value = '';
                if (this.input.acmsData('edit')) {
                    this.unedit(focus);
                }
            });

            return false;
        },
        next: function (add) {
            let self = this, o = self.options;
            if (add) {
                let firstActiveToken = this.tokenWrapper.querySelector('.active:first-child'),
                    deactivate = firstActiveToken && this.firstActiveToken !== firstActiveToken ? firstActiveToken : false;

                if (deactivate)
                    return this.deactivate(firstActiveToken);
            }

            let lastActiveToken = this.tokenWrapper.querySelector('.active:last-child'),
                nextToken = lastActiveToken ? Selectors.nextAll(lastActiveToken, '.'+ o.tokenClass + ':first-child') : [];
            if (!nextToken.length) {
                this.input.focus();
                return;
            }

            this.activate(nextToken, add);
        },
        prev: function (add) {
            let self = this, o = self.options;

            if (add) {
                let lastActiveToken = this.wrapper.querySelector('.active:last-child'),
                    deactivate = lastActiveToken && this.firstActiveToken ? lastActiveToken != this.firstActiveToken : false;

                if (deactivate)
                    return this.deactivate(lastActiveToken);
            }

            let firstActiveToken = this.wrapper.querySelector('.active:first-child'),
                prevToken = firstActiveToken ? Selectors.previousAll(firstActiveToken, '.'+ o.tokenClass + ':first-child') : [];

            if (!prevToken.length) {
                prevToken = this.wrapper.querySelector('.'+ o.tokenClass);
            }
            if (!prevToken.length && !add) {
                this.input.focus();
                return;
            }

            this.activate(prevToken, add);
        },
        activate: function (token, add, multi, remember) {
            let self = this, o = self.options;
            if (!token)
                return;

            if (typeof remember === 'undefined')
                remember = true;

            if (multi)
                add = true;

            this.copyHelper.focus();

            if (!add) {
                let active = this.wrapper.querySelector('.active');
                if(active) Classes.removeClass(active, 'active');
                if (remember) {
                    this.firstActiveToken = token;
                } else {
                    delete this.firstActiveToken;
                }
            }
            /*
             if (multi && this.firstActiveToken) {
             let i = this.firstActiveToken.index() - 2
             , a = token.index() - 2
             , self = this
             
             this.$wrapper.find('.token').slice(Math.min(i, a) + 1, Math.max(i, a)).each(function () {
             self.activate($(this), true)
             })
             }
             */
            Classes.addClass(token, 'active');
            this.copyHelper.value = this.getTokensList(null, null, true);
        },
        activateAll: function () {
            let self = this, o = self.options,
                    tokens = Selectors.qa(self.tokenWrapper, '.'+ o.tokenClass), i;
            for (i = 0; i < tokens.length; i++) {
                self.activate(tokens[i], i !== 0, false, false);
            }
        },
        deactivate: function (token) {
            if (!token)
                return;

            Classes.removeClass(token, 'active');
            this.copyHelper.value = this.getTokensList(null, null, true);
        },
        toggle: function (token) {
            if (!token) {
                return;
            }
            Classes.toggleClass(token, 'active');
            this.copyHelper.value = this.getTokensList(null, null, true);
        },
        edit: function (token) {
            if (!token)
                return;

            let attrs = token.acmsData('attrs'),
                    options = {attrs: attrs, relatedTarget: token},
            e = AcmsEvent.createCustom(EVENT_EDIT, options);
            AcmsEvent.dispatch(this.element, e);
            if (e.defaultPrevented)
                return;

            let label = token.querySelector('[data-role="label"]');
            label.textContent = attrs.value;
            let $_input = this.input;

            token.parentNode.replaceChild(token, $_input);

            this.preventCreateTokens = true;

            this.input.value = attrs.value;

            this.input.acmsData('edit', true);
            //this.update();
            let e2 = AcmsEvent.createCustom(EVENT_EDITED, options);
            AcmsEvent.dispatch(this.element, e2);
        },
        unedit: function (focus) {
            let $_input = this.input;
            this.wrapper.appendChild($_input);

            this.input.acmsData('edit', false);
            this.mirror.textContent = '';

            //this.update();
            if (focus) {
                let self = this;
                setTimeout(function () {
                    self.input.focus();
                }, 1);
            }
        },
        remove: function (e, direction) {
            if (this.input === Doc.activeElement || this._disabled || this._readonly)
                return;

            let self = this, o = self.options, token = (e.type === 'click') ?
                    Selectors.closest(e.target, '.'+ o.tokenClass) :
                    this.wrapper.querySelector('.'+ o.tokenClass +'.active'),
                firstToken;
            if (!token) {
                return;
            }
            if (e.type !== 'click') {
                if (!direction)
                    direction = 'prev';
                this[direction]();
                if (direction === 'prev' && token)
                    firstToken = Selectors.previousAll(token, '.'+ o.tokenClass + ':first-child').length === 0;
            }

            let options = {attrs: this.getTokenData(token), relatedTarget: token},
            removeEvent = AcmsEvent.createCustom(EVENT_REMOVE, options);

            AcmsEvent.dispatch(this.element, removeEvent);

            if (removeEvent.defaultPrevented)
                return;

            let removedEvent = AcmsEvent.createCustom(EVENT_REMOVED, options),
                changeEvent = AcmsEvent.createCustom('change', {initiator: 'inputsearch'});
            token.parentNode.removeChild(token);

            this.element.value = this.getTokensList();
            AcmsEvent.dispatch(this.element, removedEvent);
            AcmsEvent.dispatch(this.element, changeEvent);
            AcmsEvent.fireChange(this.element);

            if (!self.tokenWrapper.querySelector('.'+ o.tokenClass) || e.type === 'click' || firstToken)
                this.input.focus();
            //this.update();

            e.preventDefault();
        },
        /**
         * Update inputsearch dimensions
         *
         * @param {Object} e Event-Objekt
         */
        update: function (e) {
            let self = this, o = self.options,
                value = self.input.value,
                inputPaddingLeft = parseInt(window.getComputedStyle(self.input, null).getPropertyValue('padding-left')),
                inputPaddingRight = parseInt(window.getComputedStyle(self.input, null).getPropertyValue('padding-right')),
                inputPadding = inputPaddingLeft + inputPaddingRight;

            if (self.input.acmsData('edit')) {
                if (!value) {
                    value = self.input.placeholder;
                }
                if (value === self.mirror.textContent)
                    return;

                //self.mirror.textContent = value;
                //let mirrorWidth = self.mirror.offsetWidth + 10;
                //if (mirrorWidth > self.wrapper.offsetWidth) {
                //    self.input.style.width = self.wrapper.offsetWidth;
                //    return;
                //}
                //self.input.style.width = mirrorWidth;
            } else {
                //let w = (self.textDirection === 'rtl') ?
                //        self.input.offsetLeft + self.input.offsetWidth - self.wrapper.offsetLeft - parseInt(window.getComputedStyle(self.wrapper, null).getPropertyValue('padding-left')) - inputPadding - 1 :
                //        self.wrapper.offsetLeft + self.wrapper.offsetWidth + parseInt(window.getComputedStyle(self.wrapper, null).getPropertyValue('padding-left')) - self.input.offsetLeft - inputPadding;
                //self.input.style.width = isNaN(w) ? '100%' : w + 'px';
            }
        },
        focusInput: function (e) {
            let self = this, o = self.options;
            let token = Selectors.closest(e.target, '.'+ o.tokenClass),
                    tokenInput = Selectors.closest(e.target, '.token-input');
            if (token || tokenInput) {
                return;
            }
            setTimeout(function () {
                self.input.focus();
            }, 2);
        },
        /**
         * Schaltet das Element aus und fügt das Attribut "disabled" hinzu
         *
         * 
         */
        disable: function () {
            this.setProperty('disabled', true);
        },
        /**
         * (Re-)Aktiviert das Element und entfernt bei bedarf das "disabled"
         * Attribut.
         *
         * 
         */
        enable: function () {
            this.setProperty('disabled', false);
        },
        /**
         * Markiert das Element als "readonly"
         *
         * 
         */
        readonly: function () {
            this.setProperty('readonly', true);
        },
        /**
         * Entfernt das Attribut "readonly", wenn es gesetzt ist
         *
         * 
         */
        writeable: function () {
            this.setProperty('readonly', false);
        },
        /**
         * Setzt Eigenschaften des Elementes
         *
         * @param {String} property Eigenschaft
         * @param {Mixed} value     Der Wert der Eigenschaft
         *
         * 
         */
        setProperty: function (property, value) {
            this['_' + property] = value;
            this.input[property] = value;
            this.element[property] = value;
            if (value) {
                Classes.addClass(this.wrapper, property);
            } else {
                Classes.removeClass(this.wrapper, property);
            }
        },
        /**
         * Zerstört das Element
         *
         * @returns {Object}    Das Input-Search Element
         */
        destroy: function () {
            this.element.value = this.getTokensList();
            this.element.setAttribute('value', this.element.value);
            AcmsEvent.fireChange(this.element);
            Style.setStyles(this.element, this.element.acmsData('original-styles'));
            this.element.tabindex = this.element.acmsData('original-tabindex');
            let label = document.querySelector('label[for="' + this.input.getAttribute('id') + '"]');
            if (label) {
                label.for = this.element.getAttribute('id') || this.element.name;
            }
            label.parentNode.appendChild(this.element);

            this.element.acmsData('original-styles', null);
            this.element.acmsData('original-tabindex', null);
            this.element.acmsData(InputSearch.NS, null);
            this.wrapper.parentNode.removeChild(this.wrapper);
            this.tokenWrapper.parentNode.removeChild(this.tokenWrapper);
            this.mirror.parentNode.removeChild(this.mirror);

            return this.element;
        },
        /**
         * Aufsetzen des AutoComplete
         *
         * 
         */
        _initializeAutoComplete: function () {
            let self = this,
                    o = this.options,
                    autocomplete = o.autocomplete,
                    autocompleteData = o.autocompleteData, custom;
            if(typeof autocompleteData !== 'object') {
                try {
                    custom = JSON.parse(autocompleteData);
                } catch(E) {
                    console.log(E);
                    custom = {};
                }
            }
            let span = document.createElement('span');
            span.setAttribute('role', 'status');
            Classes.addClass(span, 'hidden');
            span.setAttribute('aria-live', 'polite');
            let spanID = 'inputsearch-' + Utils.generateUniqeId();
            span.setAttribute('id', spanID);
            self.input.setAttribute('aria-describedby', spanID);
            self.statusRegion = span;
            self.input.parentNode.insertBefore(span, self.input);
            let remoteurl = o.remoteurl, msg,
                    autocompleteOptions = {
                        minChars: o.showAutocompleteOnFocus ? 0 : o.minlength || 3,
                        remote: remoteurl,
                        data: custom,
                        onResponse: function (xhr, response, statusText) {
                            if (response !== null && typeof response === "object") {
                                if (response.length != 1) {
                                    msg = "Sie haben " + response.length + " Ergebnisse. Bitte nutzen Sie die Hoch-/Runter- Pfeiltasten für die Auswahl.";
                                } else if (response.length == 1) {
                                    msg = "Sie haben ein Ergebnis. Bitte nutzen Sie die Hoch-/Runter- Pfeiltasten für die Auswahl.";
                                } else {
                                    msg = "Sie haben leider keine Ergebnisse. Bitte nutzen Sie andere Suchworte.";
                                }
                                self.statusRegion.innerHTML = msg;
                            }
                            console.log(response);
                        },
                        onSelect: function (e, value, label, item) {
                            self.createToken({label: label, value: value, item: item}, true, () => {
								self.input.value = '';
                                self.statusRegion.innerHTML = '';
                                if (self.input.acmsData('edit')) {
                                    self.unedit(true);
                                }
							});
                            let selOpts = {value: value, label: label, item: item, target: e.target};
                            let e1 = AcmsEvent.createCustom(EVENT_SELECTED, selOpts);
                            AcmsEvent.dispatch(self.element, e1);
                            AcmsEvent.dispatch(self.input, e1);
                        }
                    };
            if (autocomplete && autocomplete.length) {
                for (let prop in autocomplete) {
                    if (autocomplete.hasOwnProperty(prop)) {
                        autocompleteOptions[prop] = autocomplete[prop];
                    }
                }
            }
            require(['ui/app/autocomplete'], function (AutoComplete) {
                AutoComplete.init(self.input, autocompleteOptions);
            });

        },
        _buildCopyHelper: function (hidingPosition) {
            let self = this, o = self.options;
            self.copyHelper = document.createElement('input');
            self.copyHelper.type = "text";
            self.copyHelper.style.position = 'absolute';
            self.copyHelper.style[hidingPosition] = '-10000px';
            self.copyHelper.tabindex = -1;
            self.wrapper.appendChild(self.copyHelper);
        }
    };

    return InputSearch;

});
