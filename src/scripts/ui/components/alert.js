/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */


define("ui/components/alert", [
    "tools/utils",
    "core/classes",
    "core/selectors",
    "events/event",
    "apps/ui/helpers/fade",
    "core/window"
], function(Utils, Classes, Selectors, AcmsEvent, Fade, win) {

    'use strict';

    var AL_DATA = "acms.ui.components.alert",
        NS = "." + AL_DATA,
        EVENT_SHOW = "show" + NS,
        EVENT_SHOWN = "shown" + NS,
        EVENT_CLOSE = "close" + NS,
        EVENT_CLOSED = "closed" + NS;

    /**
     * Alert Class Definition
     *
     * @param {Object} element
     * @param {Object} options
     * @returns {alert_L35.Alert}
     */
    var Alert   = function (element, options) {
        this.element    =
        this.options    =
        this.debug      = null;
        this.initialize(element, options);
    };

    /**
     * Komponenten Name
     *
     * @var {String}
     */
    Alert.MODULE = 'alert';

    /**
     * Komponenten Version
     *
     * @var {String}
     */
    Alert.VERSION = '1.5.0';

    /**
     * Standard Optionen
     *
     * @var {Object}
     */
    Alert.DEFAULT_OPTIONS = {
        onShow :  function(element, self){},
        onHide :  function(element, self){},
        remove:         true,
        target:         '.alert',
        induration:     300,
        outduration:    150,
        hidden:         'alert-closed',
        dismissable:    '[data-dismiss="alert"]'
    };

    //Alert.css = "media/css/components/alert.min.css";

    Alert.needInit = true;

    Alert.init = function(element, options) {
        var a = new Alert(element, options);
        a.element.acmsData(AL_DATA, a);

        return a;
    };

    /**
     * Class Body
     */
    Alert.prototype = {

        constructor: Alert,

        /**
         * Interner Constructor
         * 
         * @param {Object} element Element
         * @param {Object} options Options
         */
        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this.supportTransition = win.AmaryllisCMS.supportTransition;
            this.listen();
            if(!this.element.hasAttribute('aria-hidden')) {
                this.element.setAttribute('aria-hidden', this.isHidden());
            }
        },

        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {Alert.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function() {
            return Alert.DEFAULT_OPTIONS;
        },

        /**
         * Bildet die Optionen
         *
         * <p>
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         * </p>
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function(options) {
            return Utils.extend(
                    {},
                    this.getDefaultOptions(),
                    options,
                    this.element.acmsData()
                    );
        },

        listen: function() {
            var self = this, 
                o = self.options,
                element = self.element,
                close = Selectors.qa(o.dismissable, element), j;
            for(j = 0; j < close.length; j++) {
                AcmsEvent.add(close[j], 'click', function(e) {
                    return self.close(e);
                });
            }
        },

        isHidden: function() {
            var o = this.options;
            return Classes.hasClass(this.element, o.hidden);
        },

        show: function() {
            if(false === this.isHidden()) {
                return false;
            }
            var e = AcmsEvent.create(EVENT_SHOW), 
                self = this,
                o = self.options,
                element = self.element;
            AcmsEvent.dispatch(element, e);
            if(e.defaultPrevented) {
                return;
            }
            Fade.fadeIn(element, o.induration);
            setTimeout(function() {
                Classes.removeClass(element, o.hidden);
                element.setAttribute('aria-hidden', 'false');
                if(typeof o.onShow === "function") {
                    o.onShow(element, self);
                }
                var e = AcmsEvent.create(EVENT_SHOWN);
                AcmsEvent.dispatch(element, e);
            }, o.induration);
        },

        /**
         * Closeing an Alert
         *
         * Method closing an Alert
         *
         * @param {Object} event Event
         */
        close: function(event) {
            if(true === this.isHidden()) {
                //return false;
            }
            if (event) {
                event.preventDefault();
            }
            var self = this,
                o = self.options,
                element = self.element,
                related = event ? event.target : element,
                e = AcmsEvent.createCustom(EVENT_CLOSE, {relatedTarget: related});
            AcmsEvent.dispatch(this.element, e);
            if (e.defaultPrevented) {
                return;
            }
            Fade.fadeOut(element, o.outduration);
            setTimeout(function() {
                Classes.addClass(element, o.hidden);
                element.setAttribute('aria-hidden', 'true');
                self.remove();
                if(typeof o.onHide === "function") {
                    o.onHide(element, self);
                }
                var e = AcmsEvent.create(EVENT_CLOSED);
                AcmsEvent.dispatch(element, e);
            }, o.outduration);
        },

        /**
         * Versteckt oder entfernt den Alert
         *
         * Je nach Einstellung wird der Alert aus dem DOM entfernt oder 
         * versteckt.
         *
         * 
         */
        remove: function() {
            var self = this, o = self.options, element = self.element;
            if(o.remove) {
                element.parentNode.removeChild(element);
            }
        }
    };

    return Alert;
});
