/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */

/**
 * checkbox-switch Modul
 * 
 * Hilfs-Wrapper für die checkbox-Komponenten des Systems. Das tool dient zur 
 * Unterstützung für die toggle-States von Checkboxen und entsprechnde state 
 * Änderungen.
 * 
 * @param {Object|Utils}        Utils           Das Utils Modul
 * @param {Object|Selectors}    Selectors       Das Selectors Modul
 * @param {Object|Classes}      Classes         Das Classes Modul
 * @param {Object|AcmsEvent}    AcmsEvent       Das AcmsEvent Modul
 * @returns {CheckboxSwitch}    Das checkbox-switch-modul
 */
define('ui/components/checkbox-switch', [
    "tools/utils",
    "core/selectors",
    "core/classes",
    "events/event"
], function(Utils, Selectors, Classes, AcmsEvent) {
    
    "use strict";
    
    /**
     * Checkbox Switch
     * 
     * Hilfs-Wrapper für die checkbox-Komponenten des Systems. Das tool dient zur 
     * Unterstützung für die toggle-States von Checkboxen und entsprechnde state 
     * Änderungen.
     * 
     * @param {Element} element      Das Element
     * @param {Object}  options      Die Optionen
     * 
     * @exports ui/components/checkbox-switch
     */
    var CheckboxSwitch = function(element, options) {
        this.element =
        this.options =
        this.label =
        this.checkbox = 
        this.state =
        this.disabled =
        this.isInModal = null;
        this.initialize(element, options);
    };
    
    /**
     * Modul Name
     * 
     * @type {String}
     */
    CheckboxSwitch.MODULE = "Checkbox Switch";
    
    /**
     * Modul Namespace
     * 
     * @type {String}
     */
    CheckboxSwitch.NS = "acms.ui.components.checkbox-switch";
    
    /**
     * Standard Optionen
     * 
     * @type {Object}
     */
    CheckboxSwitch.DEFAULT_OPTIONS = {
        disabled: false, // true oder false. Kann alternativ auch über eine 
                         // Checkbox mit dem state disabled umgeschaltet werden.
        beforeChecked: function(self, e) {
            return true;
        },
        onChecked: function(self, e) {
            
        },
        onUnchecked: function(self, e) {
            
        },
        beforeUnchecked: function(self, e) {
            return true;
        }
    };
    
    /**
     * Modul Loader Einbindung
     * 
     * @type {Boolean}
     */
    CheckboxSwitch.needInit = true;
    
    /**
     * Module Initialisierung
     * 
     * Erstellt eine neue Instanz des Moduls.
     * 
     * @param {Element} element      Das Element
     * @param {Object}  options      Die Optionen
     * 
     * @returns {CheckboxSwitch}    Instanz des Moduls
     */
    CheckboxSwitch.init = function(element, options) {
        var cs = new CheckboxSwitch(element, options);
        cs.element.acmsData(CheckboxSwitch.NS, cs);
        
        return cs;
    };
    
    /**
     * Klassen Definitionen
     */
    CheckboxSwitch.prototype = {
        
        constructor: CheckboxSwitch,
        
        /**
         * Interner Constructor des Moduls
         * 
         * @param {Element} element Das Element
         * @param {Object}  options Eigene Optionen
         * 
         * 
         */
        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },
        
        /**
         * Gibt die Standard-Optionen zurück
         * 
         * @returns {CheckboxSwitch.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function() {
            return CheckboxSwitch.DEFAULT_OPTIONS;
        },
        
        /**
         * Bilden der eigenen Optionen
         * 
         * @returns {CheckboxSwitch.DEFAULT_OPTIONS|Object}
         */
        buildOptions: function(options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },
        
        /**
         * Wechseln des disabled-state
         * 
         * Wechselt den disabled-state zu disabled/enabled, je nach aktuellem 
         * Status.
         * 
         * 
         */
        toggleDisabled: function() {
            if(this.disabled) {
                this.disabled = false;
                this.checkbox.disabled = false;
                this.checkbox.removeAttribute('disabled');
            } else {
                this.disabled = true;
                this.checkbox.disabled = true;
                this.checkbox.setAttribute('disabled', 'disabled');
            }
        },
        
        /**
         * Aufsetzen der Event Listener
         * 
         * 
         */
        listen: function() {
            var self = this;
            AcmsEvent.add(self.label, 'click', self._processClick);
        },
        
        /**
         * On-Klick Aktion
         * 
         * @param {Event} event   Das Click-Event
         * 
         * 
         */
        _processClick: function(evt) {
            var self = this, o = self.options, m, m2, res;
            if(self.disabled) {
                return;
            }
            if(evt) evt.preventDefault();
            m = self.state ? 'beforeUnchecked' : 'beforeChecked';
            m2 = self.state ? 'onUnchecked' : 'onChecked';
            res = o[m](self, event);
            if(true !== res) {
                return;
            }
            self.state = !self.state;
            if(this.state) {
                this.checkbox.setAttribute('checked', 'checked');
            } else {
                this.checkbox.removeAttribute('checked');
            }
            this.checkbox.checked = this.state;
            AcmsEvent.fireChange(self.checkbox);
            
            o[m2](self, event);
        },
        
        /**
         * Vorbereiten der Modul Instanz
         * 
         * 
         */
        _prepare: function() {
            var self = this,
                element = self.element,
                o = self.options,
                modal = Selectors.closest(element, '.modal');
            self.isInModal = typeof modal === "object" && null !== modal;
            self.label = element.querySelector('label');
            self.checkbox = element.querySelector('input[type=checkbox]');
            self.state = ((self.checkbox && self.checkbox.checked) || false);
            self.disabled = (o.disabled || self.checkbox && self.checkbox.disabled);
            if(!self.checkbox) {
                console.log('Missing Checkbox in element ');
                console.log(self.element);
            }
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        }
    };
    
    return CheckboxSwitch;
    
});
