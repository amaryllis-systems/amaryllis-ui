/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */


define('ui/components/slider', [
    "tools/utils",
    "core/selectors",
    "core/classes",
    "apps/ui/helpers/show-hide",
    "apps/ui/helpers/animation-helper",
    "events/event",
    "core/acms"
], function (Utils, Selectors, Classes, ShowHide, Animation, AcmsEvent, A) {

    "use strict";

    /**
     * Constructor
     *
     * @param {Object} element  Der Slider Container
     * @param {Object} options  Die eigenen Optionen für den Slider
     * 
     * @returns {Slider}
     */
    var Slider = function (element, options) {
        this.element =
                this.slides =
                this.outerPosition =
                this.interval =
                this.options = null;
        this.slideDone = false;
        this.pausing = false;
        this.currentIndex = 0;
        this.options = Slider.DEFAULT_OPTIONS;
        this.initialize(element, options);
    };

    /**
     * Komponenten Name
     *
     * @var {String}
     */
    Slider.MODULE = "Slider";

    Slider.NS = "acms.ui.components.slider";
    
    Slider.EVENT_SLIDE = Slider.NS + '.slide';
    
    Slider.EVENT_CLEAR_ACT = Slider.NS + '.clearAct';
    
    Slider.EVENT_SHOW_ACT = Slider.NS + '.showAct';
    
    Slider.EVENT_NEXT = Slider.NS + '.next';
    
    Slider.EVENT_PREVIOUS = Slider.NS + '.previous';
    
    Slider.EVENT_NEXT = Slider.NS + '.slideTo';

    /**
     * Standard-Optionen
     *
     * @var {Object}
     */
    Slider.DEFAULT_OPTIONS = {
        controls: true,
        slide: '.slider',
        delay: 30,
        pauseOnHover: false,
        next: '[data-trigger=next]',
        prev: '[data-trigger=prev]',
        pause: '[data-trigger=pause]',
        play: '[data-trigger=play]',
        stop: '[data-trigger=stop]',
        
    };

    Slider.needInit = true;

    Slider.init = function (element, options) {
        var slide = new Slider(element, options);
        slide.element.acmsData(Slider.NS, slide);
        return slide;
    };

    Slider.css = "media/css/components/slider.min.css";

    /**
     * Class Definition
     */
    Slider.prototype = {
        /**
         * Initializing the Slider
         *
         * Method is called during the constructor of the Slider.
         *
         * @param {Object} element The Element to initialize the slider on
         * @param {Object} options Custom Options for the slider
         *
         * @returns {Slider}
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            var self = this;
            setTimeout(function() {
                self._prepare();
                self.listen();
                self.startScene();
            }, 500);
        },
        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {Slider.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return Slider.DEFAULT_OPTIONS;
        },

        /**
         * Bildet die Optionen
         *
         * Basierend auf den Standard-Optionen, den übergebenen Optionen des
         * Constructors und den Element Data-Attributen werden die aktuellen
         * Konfigurationen gebildet. Letztenendes gewinnen die Element
         * Data-Attribute.
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function (options) {
            var o = Utils.extend(
                    {},
                    this.getDefaultOptions(),
                    options,
                    this.element.acmsData()
                    );
            if (o.controls === '1' || o.controls === 'true' || o.controls === true || o.controls === 'on') {
                o.controls = true;
            } else {
                o.controls = false;
            }
            if (o.pauseOnHover === '1' || o.pauseOnHover === 'true' || o.pauseOnHover === true || o.pauseOnHover === 'on') {
                o.pauseOnHover = true;
            } else {
                o.pauseOnHover = false;
            }
            return o;
        },
        
        /**
         * Slide-To
         * 
         * Führt den Screen zum Index
         * 
         * @param {Integer} index   Slider-Index
         * 
         */
        slideTo: function (index) {
            var self = this, 
                slide = self.get(index);
            if(false === slide) {
                return;
            }
            self.stopScene();
            self.currentIndex = index;
            setTimeout(function() {
                self._slideTo(slide);
            }, 100);
            self._triggerEvent('slideTo', slide);
            setTimeout(function() {
                self.slideDone = false;
                self.startScene();
            }, 1000);
        },
        /**
         * Slide-To Next
         * 
         * Führt den Screen zum nächsten Slider
         * 
         * @param {Integer} index   Slider-Index
         * 
         */
        next: function () {
            var self = this;
            self.stopScene();
            self.currentIndex += 1;
            if (self.currentIndex === self.slides.length) {
                self.currentIndex = 0;
            }
            var slide = self.get(self.currentIndex);
            self._triggerEvent('next', slide);
            setTimeout(function() {
                self._slideTo(slide);
            }, 100);
            setTimeout(function() {
                self.slideDone = false;
                self.startScene();
            }, 1000);
            
        },
        /**
         * Slide-To Prev
         * 
         * Führt den Screen zum vorherigen Slider
         * 
         * @param {Integer} index   Slider-Index
         * 
         */
        previous: function () {
            var self = this;
            self.stopScene();
            
            self.currentIndex -= 1;
            if (self.currentIndex < 0) {
                self.currentIndex = self.slides.length - 1;
            }
            var slide = self.get(self.currentIndex);
            self._triggerEvent('previous', slide);
            setTimeout(function() {
                self._slideTo(slide);
            }, 100);
            setTimeout(function() {
                self.slideDone = false;
                self.startScene();
            }, 1000);
            
        },
        
        /**
         * Aufsetzen der Event Listener
         * 
         * 
         */
        listen: function () {
            var self = this, o = this.options;
            if(o.pauseOnHover) {
                self.slides.forEach(function (slider) {
                    self._prefixEvent(slider, "AnimationStart", self._onAnimationStart);
                    AcmsEvent.add(slider, 'mouseenter', self._onSlideHover);
                    AcmsEvent.add(slider, 'mouseleave', self._onSlideHover);
                });
            }
            
            if (self.controls) {
                var controls = Selectors.qa('.control', self.controls);
                controls.forEach(function (control) {
                    AcmsEvent.add(control, 'click', self._onClick);
                });
            }
        },
        
        /**
         * Gibt den Slider mit dem Index zurück oder FALSE
         * 
         * @param {Integer} i
         * @returns {Element|HTMLElement|Node|Boolean}
         */
        get: function (i) {
            var self = this, slides = self.slides;
            if (typeof slides[i] !== 'undefined') {
                return slides[i];
            }
            return false;
        },
        
        /**
         * Stoppen der Szene
         * 
         * 
         */
        stopScene: function() {
            var self = this;
            if(self.stopped === true) {
                return;
            }
            clearInterval(self.interval);
            var current = self.get(self.currentIndex);
            if(current) {
                self._clearActs(current);
            }
            this.slideDone = false;
            self.stopped = true;
            self._triggerEvent('stopped');
        },
        /**
         * Starten der Szene
         * 
         * 
         */
        startScene: function () {
            var self = this;
            self.stopped = false;
            this.interval = setInterval(function () {
                if (self.slideDone) {
                    self.currentIndex++;

                    if (self.currentIndex === self.slides.length) {
                        self.currentIndex = 0;
                    }
                    var slide = self.get(self.currentIndex);
                    self._slideTo(slide);
                }
            }, 500);
            self._triggerEvent('started');
        },

        _onSlideHover: function (e) {
            var self = this, o = this.options, target = e.target;
            if (!o.pauseOnHover) {
                return;
            }
            if (!Selectors.matches(target, o.slide)) {
                target = Selectors.closest(target, o.slide);
            }
            if(e.type == 'mouseleave' && self.stopped) {
                self.next();
            } else if(e.type == 'mouseenter' &&  !self.stopped) {
                self.stopScene();
            }
        },

        _onClick: function (e) {
            var self = this, control = e.target, index;
            e.preventDefault();
            if (!Classes.hasClass(control, 'control')) {
                control = Selectors.closest(control, '.control');
            }
            index = parseInt(control.acmsData('controls'));
            var slide = self.get(index);
            slide && self.slideTo(index);

            return false;
        },

        _slideTo: function (slide) {
            var self = this, o = this.options, element = self.element;
            self.slideDone = false;
            var current = Selectors.q('.slider.active', element), animClass= slide.acmsData('animation');
            if(current) {
                ShowHide.hide(current);
                Classes.removeClass(current, 'active')
                var currAnim = current.acmsData('animation')
                currAnim && Classes.removeClass(current, currAnim);
            }
            ShowHide.show(slide);
            Classes.addClass(slide, 'active');
            var animOpts = {
                duration: 550,
                animationName: animClass
            };
            Animation.animate(slide, animOpts);
            self._updateControls(slide);
            self._triggerEvent('slide', slide);
            self._showActs(slide);
        },
        
        _showActs: function(slide) {
            var self = this, o = this.options, acts = Selectors.qa('[data-act]', slide), element = self.element;
            self.slideDone = false;
            if(!acts.length) {
                setTimeout(function() {
                    self.slideDone = true;
                }, parseInt(o.delay, 30) * 1000);
                return;
            }
            acts.sort(function(a, b) {
                var val1 = parseInt(a.acmsData('act'));
                var val2 = parseInt(b.acmsData('act'));
                if (val1 > val2) {
                    return 1;
                }
                if (val1 < val2) {
                    return -1;
                }
                return 0;
            });
            acts.forEach(function(act, ind) {
                ShowHide.hide(act);
            });
            var animDelay = acts.length * 1500;
            acts.forEach(function(act, ind) {
                var anim = act.acmsData('animation'), curr = ind +1;
                act.acmsData('curr', ind);
                var animOpts = {
                    duration: 550,
                    animationName: anim
                };
                if(anim) {
                    var apply = function(a) {
                        ShowHide.show(a);
                        Animation.animate(a, animOpts);
                    };
                    var done = function(a, as) {
                        if(parseInt(a.acmsData('curr')) === as.length -1 || self.stopped) {
                            self.slideDone = true;
                        }
                    };
                    var to1 = setTimeout(function() {
                        apply(this, acts);
                    }.bind(act), curr * 2000);
                    var to2 = setTimeout(function() {
                        done(this, acts);
                    }.bind(act), curr * animDelay);
                    act.acmsData('timer1', to1);
                    act.acmsData('timer2', to2);
                }
            });
            
        },
        
        _onLoad: function(e) {
            
        },
        
        _updateControls: function(slide) {
            var self = this, 
                element = self.element,
                controls = self.controls, items,
                si = slide.acmsData('index');
            if(!controls) {
                return;
            }
            items = Selectors.qa('.control', element);
            items.forEach(function(item) {
                var ind = item.acmsData('controls');
                if(ind === si) {
                    Classes.addClass(item, 'active');
                } else {
                    Classes.removeClass(item, 'active');
                }
            });
        },
        
        _clearActs: function(slide) {
            var self = this, acts = Selectors.qa('[data-act]', slide);
            if(acts.length === 0) {
                return;
            }
            acts.forEach(function(act) {
                var to1 = parseInt(act.acmsData('timer1'));
                var to2 = parseInt(act.acmsData('timer2'));
                clearTimeout(to1);
                clearTimeout(to2);
                ShowHide.show(act);
                self._triggerEvent('clearAct', act);
            });
        },

        _prepare: function () {
            var self = this,
                    o = self.options,
                    element = self.element, controls, ul, control;
            var slides = Selectors.q('.slides', element);
            self.slides = Selectors.qa(o.slide, slides);
            self.controls = Selectors.q('.controls', element);
            var current = 0;
            if (!self.controls && o.controls === true) {
                controls = document.createElement('div');
                ul = document.createElement('ul');
                controls.appendChild(ul);
                self.element.appendChild(controls);
                Classes.addClass(controls, 'controls');
                self.controls = controls;
            } else if(self.controls) {
                ul = Selectors.q('ul', self.controls);
            }
            var max = o.minHeight || 550;
            self.slides.forEach(function (slide, i) {
                slide.acmsData('index', i);
                if(slide.offsetHeight > max) {
                    max = slide.offsetHeight;
                }
                if (Classes.hasClass(slide, 'active')) {
                    current = i;
                    
                } else {
                    ShowHide.hide(slide);
                }
                if (ul && o.controls === true) {
                    control = document.createElement('li');
                    ul.appendChild(control);
                    Classes.addClass(control, 'control');
                    control.acmsData('controls', i);

                }
            });
            self.slides.forEach(function (slide, i) {
                slide.style.minHeight = max + 'px';
            });
            element.style.minHeight = (max + 150) + 'px';
            self.currentIndex = current;
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
            self._prepareButtons();
            var slide = self.get(current);
            self._slideTo(slide)
        },

        _prepareButtons: function () {
            var self = this,
                    o = self.options,
                    element = self.element, stop, next, prev, pause, play;
            stop = Selectors.q(o.stop, element);
            pause = Selectors.q(o.pause, element);
            play = Selectors.q(o.play, element);
            next = Selectors.q(o.next, element);
            prev = Selectors.q(o.prev, element);
            next && AcmsEvent.add(next, 'click', self.next.bind(self));
            prev && AcmsEvent.add(prev, 'click', self.previous.bind(self));
            
        },
        
        _triggerEvent: function(name, slideOrAct) {
            var self = this,
                o = self.options,
                element = self.element,
                on = 'on' + name.charAt(0).toUpperCase() + name.substr(1);
            if(typeof o[on] === 'function') {
                o[on](element, slideOrAct);
            }
            var e = AcmsEvent.createCustom(Slider.NS + '.' + name, {relatedTarget: slideOrAct, current: self.currentIndex});
            AcmsEvent.dispatch(element, e);
        },

        _prefixEvent: function (slide, type, cb) {
            var pfx = ["webkit", "moz", "MS", "o", ""];
            for (var p = 0; p < pfx.length; p++) {
                if (!pfx[p])
                    type = type.toLowerCase();
                AcmsEvent.add(slide, pfx[p] + type, cb);
            }

        }
        
    };

    return Slider;
});
