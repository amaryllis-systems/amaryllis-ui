/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */


define("ui/components/paginator", [
    "tools/utils",
    'core/selectors',
    'core/classes',
    'apps/ui/helpers/fade',
    'events/event'
], function(Utils, Selectors, Classes, Fade, AcmsEvent) {

    "use strict";

    /**
     * Constructor
     * 
     * @param {Object} element
     * @param {Object} options
     * @returns {Paginator}
     */
    var Paginator = function(element, options) {
        this.element =
        this.target =
        this.total =
        this.options = null;
        this.options = Paginator.DEFAULT_OPTIONS;
        this.initialize(element, options);
    };

    /**
     * Komponenten Name
     *
     * @var {String}
     */
    Paginator.MODULE = "Paginator";

    /**
     * Komponenten Version
     *
     * @var {String}
     */
    Paginator.VERSION = "1.5.0";
    
    /**
     * Komponenten Namespace
     *
     * @var {String}
     */
    Paginator.NS = "acms.ui.components.paginator";

    /**
     * Standard-Optionen
     *
     * @var {String}
     */
    Paginator.DEFAULT_OPTIONS = {
        page: '.page',
        pagination: '.pagination',
        pager: 'li',
        onTotal: function(self, page) {
            return true;
        },
        onPage: function(self, page) {
            return true;
        },
        remove: true,
        disabled: 'disabled'
    };
    
    Paginator.needInit = true;
    
    Paginator.init = function(element, options) {
        var pg = new Paginator(element, options);
        pg.element.acmsData(Paginator.NS, pg);
        
        return pg;
    },

    Paginator.prototype = {

        constructor: Paginator,

        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },

        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {Paginator.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function() {
            return Paginator.DEFAULT_OPTIONS;
        },

        /**
         * Bildet die Optionen
         *
         * Basierend auf den Standard-Optionen, den übergebenen Optionen des
         * Constructors und den Element Data-Attributen werden die aktuellen
         * Konfigurationen gebildet. Letztenendes gewinnen die Element
         * Data-Attribute.
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function(options) {
            return Utils.extend(
                                {},
                                this.getDefaultOptions(),
                                options,
                                this.element.acmsData()
                            );
        },

        /**
         * Setter/Getter für die Optionen
         *
         * @param {String|NULL} a   Der Optionen-Schlüssel oder NULL
         * @param {Mixed}       b   Undefined oder der Wert für a
         * @returns {Paginator.options|Boolean|Mixed|Paginator.prototype}
         */
        option: function (a, b) {
            if (!a) {
                return this.options;
            }
            if (typeof b === "undefined") {
                return this.options[a] || false;
            }
            this.options[a] = b;
            return this;
        },

        listen: function() {
            var self = this, o = this.options;
            self.pages.forEach(function(page, index) {
                page.acmsData('index', index);
                Fade.fadeOut(page);
            });
            self.pagers.forEach(function(pager, index) {
                pager.acmsData('index', index);
                if(Classes.hasClass('active', o.active)) {
                    Fade.fadeIn(self.pages[index]);
                    self.current = index;
                }
                AcmsEvent.add(pager, 'click', self._onClick);
            });
        },
        
        _onClick: function(e) {
            var self = this, element = self.element, o = self.options, target = e.target, current, index;
            if(!Selectors.matches(target, o.pagination)) {
                target = Selectors.closest(target, o.pagination);
            }
            index = parseInt(target.acmsData('index'));
            current = self.pages[self.current];
            Fade.fadeOut(current);
            Fade.fadeIn(self.pages[index]);
            self.current = index;
            if(typeof o.onPage === 'function') {
                o.onPage(self, self.pages[index]);
            }
            if(self.current === (self.pages.length -1)) {
                if(typeof o.onTotal === 'function') {
                    o.onTotal(self, self.pages[index]);
                }
            }
        },

        _prepare: function() {
            var self = this, element = self.element, o = self.options;
            self.pages = Selectors.qa(o.page, element);
            self.pagination = Selectors.q(o.pagination, element);
            self.pagers = Selectors.qa(o.pager, self.pagination);
            
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        }
    };

    return Paginator;
});
