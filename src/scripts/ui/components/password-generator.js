/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */


define('ui/components/password-generator', [
    'tools/utils',
    'core/selectors',
    'events/event'
], function (Utils, Selectors, AcmsEvent) {

    "use strict";

    /**
     * Constructor
     * 
     * @param {Element} element Das HTML Element
     * @param {Object} options  Die Optionen
     * 
     * @returns {Generator}
     */
    var Generator = function (element, options) {
        this.element =
        this.options =
        this.scope =
        this._serial =
        this._pwd =
        this.button =
        this._repeat = null;
        this.options = Generator.DEFAULT_OPTIONS;
        this.initialize(element, options);
    };

    Generator.SCOPE_PWD = 1;

    Generator.SCOPE_SERIAL = 2;

    Generator.MODULE = "Password Generator";

    Generator.VERSION = "1.5.0";

    Generator.NS = "acms.ui.components.password-generator";

    Generator.DEFAULT_OPTIONS = {
        trigger: '[data-trigger="generate"]',
        pwd: null,
        pwdrepeat: null,
        keytarget: null,
        nums: true,
        upper: true,
        lower: true,
        special: true,
        length: 9,
        terms: 4,
        subterms: 5
    };

    Generator.NUMS = "0123456789";

    Generator.LOWER = "abcdefghijklmnopqrstuvwxyz";

    Generator.UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    Generator.SPECIAL = ":;?!@#$%^&*()_+-=";

    Generator.needInit = true;

    /**
     * Modul Initialisierung
     * 
     * @param {Element} element Das HTML Element
     * @param {Object} options  Die Optionen
     * 
     * @returns {Generator}
     */
    Generator.init = function (element, options) {
        var pwdg = new Generator(element, options);
        pwdg.element.acmsData(Generator.NS, pwdg);

        return pwdg;
    };

    Generator.prototype = {
        constructor: Generator,

        /**
         * Interner Constructor
         * 
         * @param {Element} element Das HTML Element
         * @param {Object} options  Die Optionen
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },

        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {Generator.DEFAULT_OPTIONS|Object}
         */
        getDefaultOptions: function () {
            return Generator.DEFAULT_OPTIONS;
        },

        /**
         * Bildet die neuen Optionen
         *
         * @param {Object} options
         * @returns {Object}
         */
        buildOptions: function (options) {
            options = (options && typeof options === "object") ? options : {};
            return Utils.extend(
                    {},
                    this.getDefaultOptions(),
                    options,
                    this.element.acmsData());
        },

        /**
         * Setter/Getter für die Optionen
         *
         * @param {String|NULL} a   Der Optionen-Schlüssel oder NULL
         * @param {Mixed}       b   Undefined oder der Wert für a
         * @returns {Generator.options|Boolean|Mixed|Generator.prototype}
         */
        option: function (a, b) {
            if (!a) {
                return this.options;
            }
            if (typeof b === "undefined") {
                return this.options[a] || false;
            }
            this.options[a] = b;
            return this;
        },

        listen: function () {
            var self = this;
            if (Generator.SCOPE_PWD === self.scope) {
                AcmsEvent.add(self.button, 'click', self._onPasswordClick);
            } else if (Generator.SCOPE_SERIAL === self.scope) {
                AcmsEvent.add(self.button, 'click', self._onSerialClick);

            }
        },
        
        _onPasswordClick: function(e) {
            e.preventDefault();
            var self = this, pwd = self.generatePassword();
            self._updateElements(pwd);
        },
        
        _onSerialClick: function(e) {
            e.preventDefault();
            var self = this, pwd = self.generateSerial();
            self._updateElements(pwd);
        },

        generatePassword: function () {
            var result = "",
                    o = this.options,
                    i, auch, chars, arr;
            chars = this._getChars();
            arr = this._charsToArray(chars);
            for (i = 0; i < o.length; i++) {
                auch = (Math.floor((Math.random() * arr.length) + 1)) - 1;
                result += arr[auch];
            }

            return result;
        },

        generateSerial: function () {
            var result = "",
                    o = this.options,
                    result1 = "",
                    i, j, k, auch, chars, arr;
            chars = this._getChars();
            arr = this._charsToArray(chars);
            for (i = 1; i < o.terms; i++) {
                for (j = 0; j < o.subterms; j++) {
                    auch = (Math.floor((Math.random() * arr.length) + 1)) - 1;
                    result1 += arr[auch];
                }
                result += result1 + "-";
                result1 = "";
            }
            for (k = 0; k < o.subterms; k++) {
                auch = (Math.floor((Math.random() * arr.length) + 1)) - 1;
                result += arr[auch];
            }

            return result;
        },

        _updateElements: function (value) {
            if (this.scope === Generator.SCOPE_PWD) {
                return this._updatePwdScope(value);
            } else {
                this._serial.value = value;
                AcmsEvent.fireChange(this._serial);
            }
        },

        _updatePwdScope: function (value) {
            var self = this, pwd = self._pwd;
            if (null === pwd) {
                return;
            }
            if (pwd.tagName.toLowerCase() === 'input') {
                pwd.value = value;
                AcmsEvent.fireChange(pwd);
                if (self._repeat) {
                    self._repeat.value = value;
                    AcmsEvent.fireChange(self._repeat);
                }
            } else {
                pwd.textContent = value;
            }
        },

        _updateSerialScope: function (value) {
            var self = this, serial = self._serial;
            if (null === serial) {
                return;
            }
            if (serial.tagName.toLowerCase() === 'input') {
                serial.value = value;
                AcmsEvent.fireChange(serial);
            } else {
                serial.textContent = value;
            }
        },

        /**
         * Gibt die verfügbaren Zeichen basieredn auf den Optionen zurück
         *
         * @returns {String}
         */
        _getChars: function () {
            var chars = "",
                    o = this.options;
            if (o.nums) {
                chars += Generator.NUMS;
            }
            if (o.lower) {
                chars += Generator.LOWER;
            }
            if (o.upper) {
                chars += Generator.UPPER;
            }
            if (o.special) {
                chars += Generator.SPECIAL;
            }

            return chars;
        },

        /**
         * Generiert einen Array aus den Zeichen
         *
         * @param {String} chars    Die verfügbaren Zeichen für die Generierung
         *
         * @returns {Array}
         */
        _charsToArray: function (chars) {
            var arr = new Array();
            for (var i = 0; i < chars.length; i++) {
                arr.push(chars[i]);
            }
            return arr;
        },

        _prepare: function () {
            var self = this,
                    o = this.options, element;
            self.button = Selectors.q(o.trigger, element);
            if (o.keytarget !== null) {
                self._serial = Selectors.q(o.keytarget, element);
                self.scope = Generator.SCOPE_SERIAL;
            } else if (o.pwd !== null) {
                self.scope = Generator.SCOPE_PWD;
                self._pwd = Selectors.q(o.pwd, element);
                if (o.pwdrepeat !== null) {
                    self._repeat = Selectors.q(o.pwd, element);
                }
            }
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        }
    };

    return Generator;

});