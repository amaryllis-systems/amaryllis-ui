/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */

/**
 * Cards Komponente
 *
 * @param {Object} c Core
 * @param {Object} doc Document Objekt
 * @returns {Cards}
 */
define("ui/components/cards", [
    "tools/utils",
    "core/classes",
    "core/selectors",
    "core/style",
    "events/event"
], function(Utils, Classes, Selectors, Style, AcmsEvent) {

    'use strict';

    var CARD_DATA = "acms.ui.components.cards",
        NS = "." + CARD_DATA,
        EVENT_SHOW = "show" + NS,
        EVENT_SHOWN = "shown" + NS,
        EVENT_HIDE = "hide" + NS,
        EVENT_HIDDEN = "hidden" + NS
        ;

    
    /**
     * Cards Constructor
     * 
     * @param {Object} element
     * @param {Object} options
     * @returns {Cards}
     */
    var Cards = function(element, options) {
        this.element   =
        this.options    = null;
        this.initialize(element, options);
    }

    Cards.needInit = true;

    //Cards.css = 'media/css/components/cards.css';

    /**
     * Komponenten Name
     *
     * @var {Object}
     */
    Cards.MODULE = "Cards";

    /**
     * Komponenten-Version
     *
     * @var {String}
     */
    Cards.VERSION = "1.5.0";

    /**
     * Standard-Optionen
     *
     * @var {Object}
     */
    Cards.DEFAULT_OPTIONS = {
        reveal: '.card-reveal',
        activator: '[data-toggle="activator"]'
    };

    /**
     *
     * @param {Object} element Das Card-Element
     * @param {Object} options
     * @returns {Cards}
     */
    Cards.init = function(element, options) {
        var ca = new Cards(element, options);
        ca.element.acmsData(CARD_DATA, ca);
        return ca;
    };

    /**
     * Klassen Definitionen
     */
    Cards.prototype = {
        constructor: Cards,
        /**
         * Eigens gebildete Optionen
         * 
         * @var {Cards.DEFAULT_OPTIONS|Object}
         */
        options: null,

        /**
         * Initialisiert die Komponente
         * 
         * @param {Object} element
         * @param {Object} options
         *
         * 
         */
        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this.listen();
        },

        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {Cards.DEFAULT_OPTIONS|Object}
         */
        getDefaultOptions: function() {
            return Cards.DEFAULT_OPTIONS;
        },

        /**
         * Bildet die Optionen
         *
         * <p>
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         * </p>
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },

        /**
         * Aufsetzen der Event-Listener
         * 
         * 
         */
        listen: function() {
            var self = this;
            var closers = Selectors.qa('[data-toggle="close"]', this.element),
                activators = Selectors.qa('[data-toggle="activator"]', this.element),
                cb = function(e) {
                    return self.close(e);
                },
                cb2 = function(e) {
                    return self.activate(e);
                }, i, j;
            for(i = 0; i < closers.length; i++) {
                AcmsEvent.add(closers[i], 'click', cb);
            }
            for(i = 0; i < activators.length; i++) {
                AcmsEvent.add(activators[i], 'click', cb2);
            }
        },

        /**
         * Aktiviert eine Karte
         * 
         * 
         */
        activate: function(e) {
            var self = this,
                e = AcmsEvent.createCustom(EVENT_SHOW, {relatedTarget: e.target}),
                e2 = AcmsEvent.createCustom(EVENT_SHOWN, {relatedTarget: e.target});
            AcmsEvent.dispatch(self.element, e);
            if(e.defaultPrevented) {
                return;
            }
            var reveal = self.element.querySelector(self.options.reveal);
            if(!reveal) {
                return false;
            }
            Style.setStyles(reveal, {
                  display               : 'block',
                  
                  "transform"           : "translate(0,-100%)"
                });
            reveal.setAttribute('aria-hidden', 'false');
            AcmsEvent.dispatch(self.element, e2);
        },

        /**
         * Schließt eine aktivierte Karte
         * 
         * 
         */
        close: function(e) {
            var self = this,
                e = AcmsEvent.createCustom(EVENT_HIDE, {relatedTarget: e.target}),
                e2 = AcmsEvent.createCustom(EVENT_HIDDEN, {relatedTarget: e.target});
            AcmsEvent.dispatch(self.element, e);
            if(e.defaultPrevented) {
                return;
            }
            var reveal = self.element.querySelector(self.options.reveal);
            Style.setStyles(reveal, {
                  
                  "transform"           : "translate(0,0);"
                });
            reveal.setAttribute('aria-hidden', 'false');
            setTimeout(function() {
                Style.setStyle(reveal, 'display', 'none');
                AcmsEvent.dispatch(self.element, e2);
            }, 800);
            
        }
    };

     return Cards;
});
