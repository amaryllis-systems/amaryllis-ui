/*
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */

define("ui/components/listsorter", [
    'tools/utils',
    'core/classes',
    'core/selectors',
    'events/event'
], function (Utils, Classes, Selectors, AcmsEvent) {
    
    'use_strict';


    var LISTSORTER_DATA = "acms.ui.components.listsorter",
        NAMESPACE = "." + LISTSORTER_DATA,
        EVENT_ORDERING = "ordering" + NAMESPACE,
        EVENT_ORDERED = "ordered" + NAMESPACE;

    /**
     * ListSorter Constructor
     *
     * @param {Object} element Das Lst-Sorter Element
     * @param {Object} options Optional Options passing to the Constructor
     */
    var ListSorter = function (element, options) {
        this.options            =
        this.element            =
        this.currentOrder       =
        this.currentOrdername   =
        this.debug              = null;
        this.initialize(element, options);
    };

    /**
     * Komponenten Name
     *
     * @var {String}
     */
    ListSorter.MODULE = 'List Sorter';

    ListSorter.ORDER_ASC = 'asc';
    ListSorter.ORDER_DESC = 'desc';

    /**
     * Standard Optionen
     *
     * Die folgenden Standard-Optionen k�nnen genutzt werden:
     *
     * 	order:              Sort Typ (@see this.ORDER_*) Kann asc oder desc sein
     * 	ordersource:        Kann entweder "data" oder "content" sein. "data"
     *                      erfordert dass die Option "ordername" gesetzt ist 
     *                      und ebenso der ordername in den List-items auftaucht.
     *                      "content" bedeutet den List-Item Content
     *  ordername:          Der Name des data-Attributes welches f�r die
     *                      Sortierung genommen werden soll
     *  texttype:           Kann entwerder "html" f�r den HTML-Content oder
     *                      "text" f�r einfachen Text sein.
     *  ascSorter:         Ein Button, Link oder Select-Element f�r die
     *                      Umsortierung in aufsteigender Reihenfolge.
     *  desc-sorter:        Ein Button, Link oder Select-Element f�r die
     *                      Umsortierung in absteigender Reihenfolge.
     *  oder-changer:       Ein Button, Link oder Select-Element f�r die
     *                      �nderung des Order-Namens
     *  active-class :      Die CSS-Klasse f�r die aktivierung eines der vorigen
     *                      Elemente
     *
     *  @var {Object}
     */
    ListSorter.DEFAULT_OPTIONS = {
        order: ListSorter.ORDER_ASC,
        ordersource: 'data',
        ordername: null,
        texttype: 'html',
        ascSorter: null,
        descSorter: null,
        orderChanger: null,
        activeClass : 'active',
        tagname: 'ul',
        listitem: 'li'
    };

    ListSorter.needInit = true;

    /**
     * Module-Initializer
     *
     * Initialisierung mittels module-loader.
     *
     * @param {Object} element
     * @param {Object} options
     *
     * @returns {ListSorter}
     */
    ListSorter.init = function(element, options) {
        var l = new ListSorter(element, options);
            l.element.acmsData(LISTSORTER_DATA, l);
        l.update();
        return l;
    }

    ListSorter.prototype = {
        constructor: ListSorter,
        /**
         * ListSorter interner Constructor
         *
         * @param {type} element
         * @param {type} options
         *
         * 
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this.debug = Acms.General.debug;
            this.currentOrder = this.options.order;
            this.currentOrdername = this.options.ordername;
            this.listen();
            if (this.debug === true) {
                Acms.Logger.writeLog("ListSorter initialisiert.");
            }
            Acms.Logger.logDeprecated('Module is deprecated. use `apps/ui/list-sorter instead`');
        },
        /**
         * Standard-Optionen
         *
         * Gibt die Standard-Optionen der Komponente zur�ck
         * 
         * @returns {ListSorter.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return ListSorter.DEFAULT_OPTIONS;
        },

        /**
         * Bilden der eigenen optionen
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },

        /**
         *
         * @param {type} option
         * @param {type} value
         *
         * @returns {ListSorter}
         */
        setOption: function(option, value) {
            this.options[option] = value;
            return this;
        },

        /**
         * Ordnen der List-Items
         * @param {string} _orderBy
         *
         * 
         */
        order: function (_orderBy) {
            if (this.debug === true) {
                Acms.Logger.writeLog('ListSorter starts sorting');
            }

            var self = this,
                o = self.options,
                element = self.element,
                list = Selectors.q(o.tagname, element),
                items = Selectors.qa(o.listitem, list),
                order = _orderBy || self.options.order,
                sorted, i, ordername = this.currentOrdername
            ;
            this.currentOrder = order;
            switch (order) {
                case ListSorter.ORDER_ASC:
                    sorted = self.sortItems(items, ordername);
                    break;
                case ListSorter.ORDER_DESC:
                    sorted = self.sortItems(items, ordername);
                    sorted.reverse();
                    break;
                default:
                    Acms.Logger.logWarning("Invalide Sortierung angegeben!");
                    throw new Error('Invalid option');
            }
            for(i = 0; i < sorted.length; i++) {
                list.appendChild(sorted[i]);
            }
            var e = AcmsEvent.createCustom(EVENT_ORDERING, {list: self.element});
            AcmsEvent.dispatch(self.element, e);
            if (e.defaultPrevented) {
                return;
            }
            
            var e2 = AcmsEvent.createCustom(EVENT_ORDERED, {list: self.element});
            AcmsEvent.dispatch(self.element, e2);
        },

        update:function(_orderName, _orderBy) {
            var self = this,
                ordername = _orderName || self.options.ordername,
                order = _orderBy || self.currentOrder;
            this.currentOrdername = ordername;
            this.currentOrder = order;
            this.order(order);
        },
        
        isNumber: function (n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        },

        sortItems: function(items, ordername) {
            var self = this, o = self.options;
            var asc = items.sort(function (a, b) {
                if (o.ordersource === 'data') {
                    var $text1 = a.acmsData(ordername),
                            $text2 = b.acmsData(ordername);
                } else if (o.ordersource === 'content') {
                    if (o.texttype == 'html') {
                        var $text1 = a.innerHTML,
                            $text2 = b.innerHTML;
                    } else {
                        var $text1 = a.textContent,
                            $text2 = b.textContent;
                    }
                }
                if(self.isNumber($text1) || self.isNumber($text2)) {
                    return self.sortNumber($text1, $text2);
                } else if($text1) {
                    return self.sortText($text1, $text2);
                }
                return 0;
            });
            return asc
        },

        sortNumber: function(a, b) {
            a = parseInt(a);
            b = parseInt(b);
            if(a < b) {
                return -1;
            } else if(a > b) {
                return 1;
            } else {
                return 0;
            }
        },

        sortText: function(a, b) {
            return a.localeCompare(b);
        },
        
        /**
         * Aufsetzen der Event-Listener
         *
         * 
         */
        listen: function () {
            var self = this,
                o = self.options,
                element = self.element,
                active = o['activeClass'],
                asc = o['ascSorter'],
                desc = o['descSorter'],
                sorter = o['orderChanger'], ascendingSorter, descendingSorter, sortChanger;
            if(asc) {
                ascendingSorter = Selectors.q(asc, element);
            }
            if(desc) {
                descendingSorter = Selectors.q(desc, element);
            }
            if(sorter) {
                sortChanger = Selectors.qa(sorter, element);
            }

            if (ascendingSorter) {
                var tagName = ascendingSorter.tagName.toLowerCase();
                if (tagName === 'select') {
                    AcmsEvent.add(ascendingSorter, 'change', function (e) {
                        e.preventDefault();
                        var $this = e.target,
                            target = $this.acmsData('target');
                        if (typeof target === "string" && document.querySelector(target)) {
                            var $target = document.querySelector(target);
                            var ls = $target.acmsData(LISTSORTER_DATA);
                            ls && ls.order(ListSorter.ORDER_ASC);
                        } else {
                            self.order(ListSorter.ORDER_ASC);
                        }
                        return false;
                    });
                } else if (tagName === "a" || tagName === 'button') {
                    
                    AcmsEvent.add(ascendingSorter, 'click', function (e) {
                        e.preventDefault();
                        var $this = e.target,
                        tagname = e.target.tagName.toLowerCase();
                        if(tagname !== sorterTagName) {
                            $this = Selectors.closest(e.target, sorterTagName); 
                        }
                        var target = $this.acmsData('target');
                        if(descendingSorter) {
                            Classes.removeClass(descendingSorter, active);
                        }
                        Classes.addClass($this, active);
                        if (typeof target === "string" && document.querySelector(target)) {
                            var $target = document.querySelector(target);
                            var ls = $target.acmsData(LISTSORTER_DATA);
                            ls && ls.order(ListSorter.ORDER_ASC);
                        } else {
                            self.order(ListSorter.ORDER_ASC);
                        }
                        return false;
                    });
                }
            }
            if (descendingSorter) {
                var descTagName = descendingSorter.tagName.toLowerCase();
                if (descTagName === 'select') {
                    AcmsEvent.add(descendingSorter, 'change', function (e) {
                        e.preventDefault();
                        var $this = e.target,
                                target = $this.acmsData('target');
                        if (typeof target === "string" && document.querySelector(target)) {
                            var $target = document.querySelector(target);
                            var ls = $target.acmsData(LISTSORTER_DATA);
                            ls && ls.order(ListSorter.ORDER_DESC);
                        } else {
                            self.order(ListSorter.ORDER_DESC);
                        }
                        return false;
                    });
                } else if (descTagName === 'a' || descTagName === 'button') {
                    AcmsEvent.add(descendingSorter, 'click', function (e) {
                        
                        e.preventDefault();
                        var $this = e.target,
                            tagname = e.target.tagName.toLowerCase();
                        if(tagname !== sorterTagName) {
                            $this = Selectors.closest(e.target, sorterTagName); 
                        }
                        var target = $this.acmsData('target');
                        if(ascendingSorter) {
                            Classes.removeClass(ascendingSorter, active);
                        }
                        $this.addClass(active);
                        if (typeof target === "string" && document.querySelector(target)) {
                            var $target = document.querySelector(target);
                            var ls = $target && $target.acmsData(LISTSORTER_DATA);
                            ls && ls.order(ListSorter.ORDER_DESC);
                        } else {
                            self.order(ListSorter.ORDER_DESC);
                        }
                        return false;
                    });
                }
            }
            if (sortChanger) {
                var sorterTagName = sortChanger[0].tagName.toLowerCase(), j, k, sortCB, sortTrigger;
                if (sorterTagName === 'select') {
                    sortTrigger = 'change';
                    sortCB = function (e) {
                        e.preventDefault();
                        var $this = e.target,
                            tagname = e.target.tagName.toLowerCase();
                        if(tagname !== sorterTagName) {
                            $this = Selectors.closest(e.target, sorterTagName); 
                        }
                        var target = $this.acmsData('target'),
                            ordername = $this.acmsData('ordername');
                        if (typeof target === "string" && document.querySelector(target)) {
                            var $target = document.querySelector(target);
                            var ls = $target && $target.acmsData(LISTSORTER_DATA);
                            ls && ls.update(ordername);
                        } else {
                            self.update(ordername);
                        }
                        return false;
                    };
                } else if (sorterTagName === 'a' || sorterTagName === 'button') {
                    sortTrigger = 'click';
                    sortCB = function (e) {
                        
                        e.preventDefault();
                        var $this = e.target,
                            tagname = e.target.tagName.toLowerCase();
                        if(tagname !== sorterTagName) {
                            $this = Selectors.closest(e.target, sorterTagName); 
                        }
                        var target = $this.acmsData('target'),
                            ordername = $this.acmsData('ordername');
                        self.currentOrdername = ordername;
                        for(k = 0; k < sortChanger.length; k++) { 
                            Classes.removeClass(sortChanger[k], active);
                        }
                        Classes.addClass($this, active);
                        if (typeof target === "string" && document.querySelector(target)) {
                            var $target = document.querySelector(target);
                            var ls = $target && $target.acmsData(LISTSORTER_DATA);
                            ls && ls.update(ordername);
                        } else {
                            self.update(ordername);
                        }

                        return false;
                    };
                }
                for(j = 0; j < sortChanger.length; j++) { 
                    AcmsEvent.add(sortChanger[j], sortTrigger, sortCB);
                }
            }
        }

    };

    return ListSorter;

});
