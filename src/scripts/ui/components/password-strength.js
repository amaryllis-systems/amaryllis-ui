/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */

/**
 * Password Strength-Meter
 * 
 * @param {Object} Utils
 * @param {Object} Http
 * @param {Object} Classes
 * @param {Object} Selectors
 * @param {Object} AcmsEvent
 * @param {Object} UpdateBars
 * @param {Object} Request
 * 
 * @returns {PasswordStrength}
 */
define("ui/components/password-strength", [
    "tools/utils",
    "http/css-loader",
    "core/classes",
    "core/selectors",
    "events/event",
    "apps/ui/informations/update-bars",
    'http/request'
], function (Utils, CssLoader, Classes, Selectors, AcmsEvent, UpdateBars, Request) {

    "use strict";

    var PWSTRENGTH_DATA = "acms.ui.components.password-strength",
        NS = "." + PWSTRENGTH_DATA;

    /**
     * Constructor
     * 
     * @param {Object} element Password field Object
     * @param {Object} options
     * @returns {PasswordStrength}
     */
    var PasswordStrength = function (element, options) {
        this.element        =
        this.options        =
        this.form           =
        this.$progressbar   =
        this.errors = null;
        this.username = null;
        this.initialize(element, options);
    };

    /**
     * Component Name
     *
     * @var {String}
     */
    PasswordStrength.MODULE = 'Password Strength';

    /**
     * Default Options
     *
     * @var {Object}
     */
    PasswordStrength.DEFAULT_OPTIONS = {
        minChar: 8, // too short while less than this
        passIndex: 2, // Weak
        remote : Acms.Urls.baseUrl + '/c/validation/action/',
        // output verdicts, colours and bar %
        label: Acms.Translator._('Passwort stärke') + ': ',
        verdicts: [
            Acms.Translator._('zu kurz'),
            Acms.Translator._('sehr schwach'),
            Acms.Translator._('schwach'),
            Acms.Translator._('mittel'),
            Acms.Translator._('stark'),
            Acms.Translator._('sehr stark')
        ],
        colors: [
            'danger',
            'danger',
            'warning',
            'warning',
            'success',
            'royal'
        ],
        width: [
            '0',
            '10',
            '30',
            '60',
            '80',
            '100'
        ],
        // tweak scores here
        scores: [
            10,
            15,
            25,
            45
        ],
        // when in banned list, verdict is:
        bannedVerdict: Acms.Translator._('Das Passwort enthält ungültige Worte'),
        usernameVerdict: Acms.Translator._('Das Passwort enthält den Benutzernamen'),
        lastnameVerdict: Acms.Translator._('Das Passwort enthält den Nachnamen'),
        loginnameVerdict: Acms.Translator._('Das Passwort enthält den Login-Namen'),
        firstnameVerdict: Acms.Translator._('Das Passwort enthält den Vornamen'),
        emailVerdict: Acms.Translator._('Das Passwort enthält die E-Mail'),

        hasProgress: true

    };

    PasswordStrength.BAD_PASSWORDS = [
            '123456',
            '12345',
            '123456789',
            'password',
            'passwort',
            'iloveyou',
            'princess',
            'rockyou',
            '1234567',
            '12345678',
            'abc123',
            'monkey',
            'lovely',
            '654321',
            'qwerty',
            'qwertz',
            'password1',
            'welcome',
            'willkommen',
            'p@ssw0rd',
            'p@ssw0rt',
            'winter',
            'spring',
            'summer'
        ];

    /**
    * @type {Array}
    * @description The collection of regex checks and how much they affect the scoring
    */
    PasswordStrength.checks = [
        {
            re: /[a-z]/,
            score: 1
        },
        {
            re: /[A-Z]/,
            score: 5
        },
        {
            re: /([a-z].*[A-Z])|([A-Z].*[a-z])/,
            score: 2
        },
        {
            re: /(.*[0-9].*[0-9].*[0-9])/,
            score: 7
        },
        {
            re: /.[!@#$%^&*?_~]/,
            score: 5
        },
        {
            re: /(.*[!@#$%^&*?_~].*[!@#$%^&*?_~])/,
            score: 7
        },
        {
            re: /([a-zA-Z0-9].*[!@#$%^&*?_~])|([!@#$%^&*?_~].*[a-zA-Z0-9])/,
            score: 3
        },
        {
            re: /(.)\1+$/,
            score: 2
        }
    ];

    PasswordStrength.needInit = true;

    PasswordStrength.init = function(element, options) {
        var pws = new PasswordStrength(element, options);
        pws.element.acmsData(PWSTRENGTH_DATA, pws);

        return pws;
    }

    /**
     * Class Definition
     */
    PasswordStrength.prototype = {

        constructor: PasswordStrength,
        
        /**
         * Holding some possible field values of the form
         *
         * @var {Object}
         */
        customs: {
            username: false,
            firstname: false,
            lastname: false,
            email: false,
            loginname: false
        },
        
        /**
         * @param {Object} element Element to apply the password container on
         */
        initialize: function (element, options) {
            this.element = element;
            this.form = Selectors.closest(element, 'form');
            this.options = this.buildOptions(options);
            var formGroup = Selectors.closest(element, '.form-group');
            this.formGroup = formGroup;
            var progressbar = formGroup ? formGroup.querySelector('.progress') : false,
                self = this;
            if(formGroup && progressbar) {
                require(['ui/widgets/progressbar'], function(Bar) {
                    var thisBar = progressbar.acmsData('acms.ui.widgets.progressbar');
                    if(thisBar instanceof Bar) {
                        self.$progressbar = thisBar;
                    } else {
                        self.$progressbar = Bar.init(progressbar);
                    }
                });
            }
            CssLoader.load(UpdateBars.css);
            this.buildCustoms();
            this.listen();
            this.runPassword(this.element.value);
        },
        /**
         * Build the Options
         *
         * @param {Object} options
         */
        buildOptions: function (options) {
            return Utils.extend(
                    {},
                    this.getDefaultOptions(),
                    options,
                    this.element.acmsData()
                    );
        },
        
        /**
         * Gets the Default Options
         *
         * @returns {PasswordStrength.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return PasswordStrength.DEFAULT_OPTIONS;
        },

        /**
         * Gets the Bad Password List
         * 
         * @returns {Array|PasswordStrength.BAD_PASSWORDS}
         */
        getBadPasswords: function() {
            return PasswordStrength.BAD_PASSWORDS;
        },
        /**
         * Get the Regex checks and their scores
         *
         * @returns {PasswordStrength.checks|Array}
         */
        getChecks: function() {
            return PasswordStrength.checks;
        },

        buildCustoms: function() {
            var form = this.form,
                self = this,
                $username = form.querySelector('input[name=username]'),
                $email = form.querySelector('input[name=email]'),
                $firstname = form.querySelector('input[name=first_name]'),
                $lastname = form.querySelector('input[name=last_name]'),
                $loginname = form.querySelector('input[name=login_name]')
            ;
            this.customs = {};
            if($username) {
                this.customs.username = $username.value;
                AcmsEvent.add($username, 'change', function(event) {
                    self.customs.username = $username.value;
                    self.runPassword();
                });
            }
            if($loginname) {
                this.customs.loginname = $loginname.value;
                AcmsEvent.add($loginname, 'change', function(event) {
                    self.customs.loginname = $loginname.value;
                    self.runPassword();
                });
            }
            if($email) {
                this.customs.email = $email.value;
                AcmsEvent.add($email, 'change', function(event) {
                    self.customs.email = $email.value;
                    self.runPassword();
                });
            }
            if($firstname) {
                this.customs.firstname = $firstname.value;
                AcmsEvent.add($firstname, 'change', function(event) {
                    self.customs.firstname = $firstname.value;
                    self.runPassword();
                });
            }
            if($lastname) {
                this.customs.lastname = $lastname.value;
                AcmsEvent.add($lastname, 'change', function(event) {
                    self.customs.lastname = $lastname.value;
                    self.runPassword();
                });
            }
        },

        /**
         * Listen on keyup event
         * 
         * 
         */
        listen: function() {
            var self = this;
            AcmsEvent.add(self.element, 'keyup', function(event) {
                var password = event.target.value;
                self.runPassword(password);
            });
        },

        /**
         * @description Runs a password check on the keyup event
         * @param {String} password* Optionally pass a string or go to element getter
         * @fires StrongPass#fail StrongPass#pass
         * @returns {StrongPass}
         */
        runPassword: function (password) {
            password = password || this.element.value;
            var o = this.options;
            if(!password.length) {
                if(this.$progressbar) {
                    var width = 0,
                        background = o.colors[width];
                    this.$progressbar.update(width);
                    var bar = this.$progressbar.element;
                    Classes.addClass(bar, background);
                }
                return;
            }
            this.errors = new Array();
            var score = this.checkPassword(password),
                index = 0,
                s = o.scores,
                verdict;
            var bad = this.getBadPasswords(), badLength = bad.length, i, badone;
            for(i=0; i < badLength; i++) {
                var badone = bad[i];
                if(password.toLowerCase().indexOf(badone) > -1) {
                    score = 0;
                    verdict = o.bannedVerdict;
                }
            }
            
            if(!verdict) {
                if (score < 0 && score > -199) {
                    index = 0;
                } else {
                    s.push(score);
                    s.sort(function (a, b) {
                        return a - b;
                    });
                    index = s.indexOf(score) + 1;
                }

                verdict = o.verdicts[index] || o.verdicts[o.verdicts.length -1];
            }
            var width = o.width[index] || o.width[o.width.length -1],
                    background = o.colors[index] || o.colors[o.colors.length -1];
            this._makeUpdateBar(width, background, verdict);
            //this._remoteCheck(password, width);
            if (o.hasProgress && this.$progressbar) {
                this.$progressbar.update(width);
                var bar = this.$progressbar.element;
                Classes.removeClass(bar).addClass(bar, background).addClass(bar, 'progress');
            }
            
            this._prozessErrors();

            var $return = score > 0;
            var grp = Selectors.closest(this.element, '.form-group');
            if(false === $return) {
                Classes.addClass(grp, 'invalid');
                Classes.removeClass(grp, 'valid');
            } else {
                Classes.addClass(grp, 'valid');
                Classes.removeClass(grp, 'invalid');
            }
            return $return;
        },

        checkPassword: function (password) {
            var score = 0,
                    minChar = this.options.minChar,
                    len = password.length,
                    diff = len - minChar;
            (diff < 0 && (score -= 100))
                    || (diff >= 5 && (score += 18))
                    || (diff >= 3 && (score += 12))
                    || (diff === 2 && (score += 6));
            var checks = this.getChecks(), i, check;
            for(i=0;i<checks.length; i++) {
                check = checks[i];
                password.match(check.re) && (score += check.score);
            }
            score && (score += len);
            score = this.passwordMatchesEmail(password, score);
            score = this.passwordMatchesUsername(password, score);
            score = this.passwordMatchesLoginname(password, score);
            score = this.passwordMatchesFirstname(password, score);
            score = this.passwordMatchesLastname(password, score);
            
            return score;
        },

        passwordMatchesUsername: function(password, score) {
            if(!this.customs.username
                    || !this.customs.username.length ) {
                return score;
            }
            var username = this.customs.username;
            if (username && password.toLowerCase().match(username.toLowerCase())) {
                this.errors.push(this.options.usernameVerdict);
                return 0;
            }
            return score;
        },
        
        passwordMatchesLoginname: function(password, score) {
            if(!this.customs.loginname
                    || !this.customs.loginname.length ) {
                return score;
            }
            var loginname = this.customs.loginname;
            if (loginname && password.toLowerCase().match(loginname.toLowerCase())) {
                this.errors.push(this.options.loginnameVerdict);
                return 0;
            }
            return score;
        },

        passwordMatchesEmail: function(password, score) {
            if(!this.customs.email
                    || !this.customs.email.length ) {
                return score;
            }
            var email = this.customs.email;
            if (email && email.toLowerCase().indexOf(password.toLowerCase()) > -1) {
                this.errors.push(this.options.emailVerdict);
                this.errors.push(email)
                return 0;
            }
            return score;
        },

        passwordMatchesFirstname: function(password, score) {
            if(!this.customs.firstname
                    || !this.customs.firstname.length ) {
                return score;
            }
            var firstname = this.customs.firstname;
            if (firstname && password.toLowerCase().indexOf(firstname.toLowerCase()) > -1) {
                this.errors.push(this.options.firstnameVerdict);
                return 0;
            }
            return score;
        },

        passwordMatchesLastname: function(password, score) {
            if(!this.customs.lastname
                    || !this.customs.lastname.length ) {
                return score;
            }
            var lastname = this.customs.lastname;
            if (lastname && password.toLowerCase().indexOf(lastname.toLowerCase()) > -1) {
                this.errors.push(this.options.lastnameVerdict);
                return 0;
            }
            return score;
        },
        
        _makeUpdateBar: function(width, background, verdict)
        {
            var bar = this.formGroup.querySelector('.progress-state-bar');
            if(!bar) {
                UpdateBars.make({
                    message: verdict, 
                    type: 'progress-state-bar ' + UpdateBars.INFO, 
                    icon: 'acms-icon-info-circle'
                },
                {autoRemove: false},
                this.formGroup);
            } else {
                bar.querySelector('.text').innerHTML = verdict;
                var icon = bar.querySelector('.split > i');
                if(background == 'danger' || background == 'warning') {
                    Classes.removeClass(icon).addClass(icon, 'acms-icon,acms-icon-exclamation-triangle');
                    Classes.removeClass(bar, 'info,danger,warning,success,royal').addClass(bar, UpdateBars.DANGER);
                } else if(background == 'success') {
                    Classes.removeClass(icon).addClass(icon, 'acms-icon,acms-icon-info-circle');
                    Classes.removeClass(bar, 'info,danger,warning,success,royal').addClass(bar, UpdateBars.SUCCESS);
                } else {
                    Classes.removeClass(icon).addClass(icon, 'acms-icon,acms-icon-rocket');
                    Classes.removeClass(bar, 'info,danger,warning,success,royal').addClass(bar, UpdateBars.ROYAL);
                }
            }
        },
        
        _prozessErrors: function() {
            if(!this.errors.length) {
                return;
            }
            var len = this.errors.length, i;
            for(i = 0; i < len; i++) {
                UpdateBars.make({
                        message: this.errors[i], 
                        type: UpdateBars.DANGER, 
                        icon: 'acms-icon-exclamation-triangle'
                    },
                    {autoRemove: true, autoTimer: 7},
                    this.formGroup);
            }
        },
        
        _remoteCheck: function(password, width) {
            var o = this.options;
            if(!o.remote) {
                return;
            }
            if(width < 80) {
                return;
            }
            var data = {
                rule: 'checkpassword',
                value: password
            };
            Request.post(o.remote, data, function(response) {
                console.log(response);
            }, function(jqXHR, textStatus) {
                console.log(textStatus);
            })
            
        }
    };

    return PasswordStrength;

});
