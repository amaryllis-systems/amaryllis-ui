/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */

define('ui/components/product-grid', [
    'tools/utils',
    'core/selectors',
    'core/classes',
    'events/event',
    'apps/ui/helpers/fade',
    'core/acms'
], function (Utils, Selectors, Classes, AcmsEvent, Fade, A) {

    "use strict";

    /**
     * Constructor
     * 
     * @param {Element}     element     Das HTML-Element
     * @param {Object}      options     Die Optionen
     * @returns {Products}
     */
    var Products = function (element, options) {
        this.element =
                this.options =
                this.toggles = null;
        this.options = Products.DEFAULT_OPTIONS;

        this.initialize(element, options);
    };

    /**
     * Modul Name
     * 
     * @var {String}
     */
    Products.MODULE = "Produkte Grid";

    /**
     * Modul Namespace
     * 
     * @var {String}
     */
    Products.NS = "acms.ui.components.product-grid";

    /**
     * Standard-Optionen des Moduls
     * 
     * @var {Object}
     */
    Products.DEFAULT_OPTIONS = {
        grid: '.gridview',
        gritItem: '.grid-item',
        toggleClass: 'grid-row',
        toggle: '[data-toggle="product-toggle"]',
        gridIcon: 'acms-icon-th-large',
        listIcon: 'acms-icon-list',
        filterButton: '.filter-button'
    };

    /**
     * Sagt dem Modul-Loader, dass das Modul initialisiert werden muss, wenn es 
     * über diesen geladen wird.
     * 
     * @var {Boolean}
     */
    Products.needInit = true;

    /**
     * Modul Initialisierung
     * 
     * @param {Element}     element     Das HTML-Element
     * @param {Object}      options     Die Optionen
     * @returns {Products}
     */
    Products.init = function (element, options) {
        var v = new Products(element, options);
        v.element.acmsData(Products.NS, v);

        return v;
    };

    /**
     * Klassen Definition
     */
    Products.prototype = {

        constructor: Products,

        /**
         * Interner Constructor
         * 
         * @param {Element} element     HMTL Element
         * @param {Objct}   options     Eigene Optionen
         * 
         * 
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },

        /**
         * Aufsetzen der Event-Listener
         * 
         */
        listen: function () {
            var self = this,
                    o = self.options,
                    toggle = self.toggles;
            AcmsEvent.add(toggle, 'click', self.toggle.bind(self));
        },
        
        toggle: function(e) {
            var self = this,
                    o = self.options,
                    i = self.togglesIcon,
                    grid = self.gridview;
            e && e.preventDefault();
            Classes.toggleClass(grid, o.toggleClass);
            Classes.toggleClass(i, o.listIcon);
            Classes.toggleClass(i, o.gridIcon);
        },

        /**
         * Gibt die Standard-Optionen zurück
         * 
         * @returns {Products.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return Products.DEFAULT_OPTIONS;
        },

        /**
         * Bilden der Optionen
         * 
         * Bildet die Optionen basierend auf den Standard-Optionen, den 
         * übergebenen Optionen des Constructors und den Element Data-Attributen. 
         * Letztere gewinnen den Kampf, wenn gesetzt.
         * 
         * @param {Object} options    Die eignenen Optionen
         * @returns {Object|Products.DEFAULT_OPTIONS}
         */
        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },
        
        
        applyFilter: function () {
            var self = this, o = self.options;
            var items = Selectors.qa(o.gritItem, self.target), j;
            for (j = 0; j < items.length; j++) {
                var prod = items[j], type = prod.acmsData('category');
                
                if (self.filters[type] === false) {
                    //Classes.addClass(prod, 'hiding');
                    Fade.fadeOut(prod);
                } else {
                    //Classes.removeClass(prod, 'hiding');
                    Fade.fadeIn(prod);
                }
            }

        },

        /**
         * Vorbereiten des Moduls
         * 
         * 
         */
        _prepare: function () {
            var self = this,
                    element = self.element,
                    o = self.options;
            self.gridview = Selectors.q(o.grid, element);
            self.toggles = Selectors.q(o.toggle, element);
            self.togglesIcon = Selectors.q('.acms-icon', self.toggles);
            
            var btns = Selectors.qa('.filter-button', element),
                    j;
            self.buttons = btns;
            self.filters = {};
            for (j = 0; j < btns.length; j++) {
                AcmsEvent.add(btns[j], 'click', self._onFilterClick.bind(self));
                var curr = btns[j].value;
                if (Classes.hasClass(btns[j], 'active')) {
                    self.filters[curr] = true;
                } else {
                    self.filters[curr] = false;
                }
            }
        },
        
        _onFilterClick: function (e) {
            var self = this,
                    target = e.target;
            if (target.tagName.toLowerCase() !== 'button') {
                target = Selectors.closest(target, 'button');
            }
            var icn = Selectors.q('.acms-icon', target);
            
            Classes.toggleClass(target, 'active');
            if(icn) {
                Classes.toggleClass(icn, 'acms-icon-check-square');
                Classes.toggleClass(icn, 'acms-icon-square');
            }
            var curr = target.value;
            if (document.activeElement != document.body)
                document.activeElement.blur();
            self.filters[curr] = Classes.hasClass(target, 'active');
            self.applyFilter();
        }

    };

    return Products;

});

