/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */

/**
 * Equal Columns
 *
 * Plugin making columns equal height. The Plugin should be initialized on the
 * parent container. All Elements with a given Selector
 * (see EqualColumns.DEFAULT_OPTIONS) will be adjusted, optional to the tallest
 * or lowest height of all selectors
 *
 * @param {Object} c
 * @param {Object} u
 * 
 * @returns {EqualColumns}
 */
define("ui/components/equalcolumns", [
    "tools/utils",
    'core/selectors',
    'events/event'
], function (Utils, Selectors, AcmsEvent) {

    "use strict";

    /**
     * Constructor
     *
     * @param {Object} element Element to apply the equalizer too
     * @param {Object} options Custom Options
     * @returns {EqualColumns}
     */
    var EqualColumns = function (element, options) {
        this.element =
                this.options =
                this.columns = null;
        this.options = EqualColumns.DEFAULT_OPTIONS;

        this.initialize(element, options);
    };

    /**
     * Modul Name
     *
     * @var {String}
     */
    EqualColumns.MODULE = "Equal Columns";

    /**
     * Modul Version
     *
     * @var {String}
     */
    EqualColumns.VERSION = "1.5.0";

    /**
     * Modul Namespace
     * 
     * @var {String}
     */
    EqualColumns.NS = "acms.ui.components.equalcolumns";

    EqualColumns.EVENT_EQUALIZE = EqualColumns.NS + ".equalize";
    EqualColumns.EVENT_EQUALIZED = EqualColumns.NS + ".equalized";
    EqualColumns.EVENT_REFRESH = EqualColumns.NS + ".refresh";
    EqualColumns.EVENT_REFRESHED = EqualColumns.NS + ".refreshed";

    /**
     * Standard-Optionen
     *
     * @var {Object}
     */
    EqualColumns.DEFAULT_OPTIONS = {
        selector: '.equal-column',
        tallest: false,
        intab: true,
        onRefresh: function (cols, self) {
            return true;
        },
        onRefreshed: function (cols, self) {},
        onEqualize: function (cols, self) {
            return true;
        },
        onEqualized: function (cols, self) {}
    };

    /**
     * Module Loader
     */
    EqualColumns.needInit = true;
    /**
     * Initiierung durch den Module Loader
     *
     * @param {Object} element
     * @param {Object} options
     *
     * @returns {EqualColumns}
     */
    EqualColumns.init = function (element, options) {
        var eq = new EqualColumns(element, options);
        eq.element.acmsData(EqualColumns.NS, eq);

        return eq;
    };

    /**
     * EqualColumns Class definition
     */
    EqualColumns.prototype = {

        constructor: EqualColumns,

        /**
         * Interner Constructor
         *
         * @param {Object} element
         * @param {Object} options
         * @returns {EqualColumns}
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.equalize();
            this.listen();
        },

        /**
         * Standard-Option
         *
         * Gibt dir Standard-Optionen zurück
         *
         * @returns {Object|EqualColumns.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return EqualColumns.DEFAULT_OPTIONS;
        },

        /**
         * Bildet die Optionen
         *
         * <p>
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         * </p>
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },

        /**
         * Setter/Getter für die Optionen
         *
         * @param {String|NULL} a   Der Optionen-Schlüssel oder NULL
         * @param {Mixed}       b   Undefined oder der Wert für a
         * @returns {EqualColumns.options|Boolean|Mixed|EqualColumns.prototype}
         */
        option: function (a, b) {
            if (!a) {
                return this.options;
            }
            if (typeof b === "undefined") {
                return this.options[a] || false;
            }
            this.options[a] = b;
            return this;
        },

        listen: function () {
            var self = this;
            AcmsEvent.add(window, 'resize', self.refresh.bind(self));
        },

        equalize: function () {
            var self = this,
                    element = self.element,
                    o = self.options,
                    cols = self.columns,
                    heights = new Array(),
                    res = true, j, height, h,
                    e = AcmsEvent.create(EqualColumns.EVENT_EQUALIZE);
            AcmsEvent.dispatch(element, e);
            if (typeof o.onEqualize === 'function') {
                res = o.onEqualize(cols, self);
            }
            if (e.defaultPrevented || true !== res) {
                return;
            }
            for (j = 0; j < cols.length; j++) {
                height = self._getAbsoluteHeight(cols[j]);
                heights.push(height);
            }
            if (o.tallest) {
                h = Math.max.apply(null, heights);
            } else {
                h = Math.min.apply(null, heights);
            }
            for (j = 0; j < cols.length; j++) {
                cols[j].style['height'] = h + 'px';
            }
            e = AcmsEvent.create(EqualColumns.EVENT_EQUALIZED);
            AcmsEvent.dispatch(element, e);
            if (typeof o.onEqualized === 'function') {
                o.onEqualized(cols, self);
            }
        },

        refresh: function () {
            var self = this,
                    o = self.options, res,
                    element = self.element, e;
            e = AcmsEvent.create(EqualColumns.EVENT_REFRESH);
            AcmsEvent.dispatch(element, e);
            res = true;
            if (typeof o.onRefresh === 'function') {
                res = o.onRefresh(self.columns, self);
            }
            if (e.defaultPrevented) {
                return;
            }
            self.equalize();
            e = AcmsEvent.create(EqualColumns.EVENT_REFRESHED);
            AcmsEvent.dispatch(element, e);
            if (typeof o.onRefreshed === 'function') {
                o.onRefreshed(self.columns, self);
            }
        },

        _prepare: function () {
            var self = this,
                    element = self.element,
                    o = self.options;
            var cols = Selectors.qa(o.selector, element);
            self.columns = cols;
        },

        _getAbsoluteHeight: function (el) {
            var styles = window.getComputedStyle(el);
            var margin = parseFloat(styles['marginTop'], 0) +
                    parseFloat(styles['marginBottom'], 0);

            return Math.ceil(el.offsetHeight + margin);
        }
    };

    return EqualColumns;
});
