/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */

define('ui/components/presentation', [
    'tools/utils',
    'core/selectors',
    'core/classes',
    'core/style',
    'apps/ui/helpers/fade',
    'events/event'
], function (Utils, Selectors, Classes, Style, Fade, AcmsEvent) {

    "use strict";

    var Presentation = function (element, options) {
        this.element = null;
        this.options = null;
        this.debug = false;
        this.initialize(element, options);
    };

    Presentation.MODULE = "Präsentationen";

    Presentation.VERSION = "1.5.0";

    Presentation.needInit = true;

    Presentation.DEFAULT_OPTIONS = {
        height: '390',
        width: '100%',
        effect: 'random',
        duration: 1000,
        timeout: 2000,
        sceneTimeout: 3000
    };

    Presentation.init = function (element, options) {
        var pres = new Presentation(element, options);

        return pres;
    };

    Presentation.css = "media/css/components/presentation.min.css";

    Presentation.prototype = {
        acts: undefined,
        currentAct: 0,
        actDone: true,
        interval: undefined,
        effects: ['top', 'bottom', 'left', 'right'],
        actorPositions: [],
        constructor: Presentation,
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._buildPresentation();
            this.startScene();
        },
        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {Presentation.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return Presentation.DEFAULT_OPTIONS;
        },
        /**
         * Bildet die Optionen
         *
         * Basierend auf den Standard-Optionen, den übergebenen Optionen des
         * Constructors und den Element Data-Attributen werden die aktuellen
         * Konfigurationen gebildet. Letztenendes gewinnen die Element
         * Data-Attribute.
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function (options) {
            return Utils.extend(
                    {},
                    this.getDefaultOptions(),
                    options,
                    this.element.acmsData()
                    );
        },
        /**
         * Setter/Getter für die Optionen
         *
         * @param {String|NULL} a   Der Optionen-Schlüssel oder NULL
         * @param {Mixed}       b   Undefined oder der Wert für a
         * @returns {Presentation.options|Boolean|Mixed|Presentation.prototype}
         */
        option: function (a, b) {
            if (!a) {
                return this.options;
            }
            if (typeof b === "undefined") {
                return this.options[a] || false;
            }
            this.options[a] = b;
            return this;
        },
        stopScene: function() {
            clearInterval(this.interval);
        },
        startScene: function () {
            var self = this;
            this.interval = setInterval(function () {
                if (self.actDone) {
                    self.currentAct++;

                    if (self.currentAct == self.acts.length) {
                        self.currentAct = 0;
                    }
                    self._showAct();
                }
            }, 500);
        },
        
        _buildPresentation: function () {
            var self = this,
                element = self.element, 
                o = this.options, j,
                acts = Selectors.qa('.act', element);
            for(j = 0; j < acts.length; j++) {
                Style.setStyles(acts[j], {display: 'none'});
            }
            this.acts = acts;
            Style.setStyles(element, {
                height: o.height + 'px',
                'min-height': o.height + 'px',
                width: o.width,
                'position': 'relative'
            });
        },
        
        _closeAct: function () {
            var self = this, o = self.options;
            var index = self.currentAct;
            setTimeout(function () {
                if (self.acts[index] !== undefined)
                    Fade.fadeOut(self.acts[index], 1000, function () {
                        self.actDone = true;
                        self.acts[index].setAttribute('aria-expanded', false);
                    });
            }, o.sceneTimeout);
        },
        _showAct: function () {
            var self = this,
                    
                element = this.element,
                o = this.options,
                act = this.acts[this.currentAct],
                actors = Selectors.qa('.actor', act), j, i;
            this.actDone = false;
            act.setAttribute('aria-expanded', true);
            Fade.fadeIn(act, 1000);
            self._resetActors(actors);
            
            actors.forEach(function(v, i) {
                var actor = v;
                var positions = actor.acmsData('position').split(","),
                    pos = {
                            top: parseInt(positions[0]),
                            left: parseInt(positions[1])
                        }, actorEffect, actorDuration, actorTimeout;
                if(actor.acmsData('width')) {
                    Style.setStyle(actor, 'width', actor.acmsData('width') + 'px');
                }
                
                actorEffect = actor.acmsData('effect') || o.effect;
                if (actorEffect === 'random') {
                    actorEffect = self.effects[Math.floor(Math.random() * self.effects.length)];
                }
                actorDuration = actor.acmsData('duration') !== null ? parseInt(actor.acmsData('duration')) : o.duration;
                actorTimeout = actor.acmsData('timeout') !== null ? parseInt(actor.acmsData('timeout')) : parseInt(o.timeout);
                actorDuration = 2;
                if (actorEffect === 'top') {
                    setTimeout(function () {
                        Style.setStyles(actor, {
                            top: -(element.height() /2) + 'px',
                            left: pos.left + 'px',
                            display: 'block',
                            opacity: 0
                        });
                        var npos = -(element.height()) /2,
                            cb = function () {
                                if (actor == actors[actors.length - 1]) {
                                    self._closeAct();
                                }
                            };
                        animate(actor,'top','px',npos,pos.top,1000, cb);
                        /**
                        var id = setInterval(animateTop, actorDuration);
                        function animateTop() {
                            if (npos === pos.top) {
                                clearInterval(id);
                                actor.style.opacity = 1;
                                cb();
                            } else {
                                npos++; 
                                actor.style.top = npos + 'px';
                                actor.style.opacity = 1;
                            }
                        }**/
                    }, i * actorTimeout);
                } else if (actorEffect === 'bottom') {
                    setTimeout(function () {
                        Style.setStyles(actor, {
                            top: element.height() /2 + 'px',
                            left: pos.left + 'px',
                            display: 'block',
                            opacity: 0
                        });
                        var npos = element.height() /2,
                            cb = function () {
                                if (actor == actors[actors.length - 1]) {
                                    self._closeAct();
                                }
                            };
                        animate(actor,'top','px',npos,pos.top,1000, cb);
                        /**var id = setInterval(animateTop, actorDuration);
                        function animateTop() {
                            if (npos === pos.top) {
                                clearInterval(id);
                                actor.style.opacity = 1;
                                cb();
                            } else {
                                npos++; 
                                actor.style.top = npos + 'px'; 
                                actor.style.opacity = 1;
                            }
                        }
                        **/
                    }, i * actorTimeout);
                } else if (actorEffect === 'left') {
                    setTimeout(function () {
                        Style.setStyles(actor, {
                            left: -(element.width()) + 'px',
                            top: pos.top + 'px',
                            display: 'block',
                            opacity: 0
                        });
                        var npos = -(element.width() /2),
                            cb = function () {
                                if (actor == actors[actors.length - 1]) {
                                    self._closeAct();
                                }
                            };
                        animate(actor,'top','px',npos,pos.top,1000, cb);
                        /**var id = setInterval(animateTop, actorDuration);
                        function animateTop() {
                            if (npos === pos.left) {
                                clearInterval(id);
                                actor.style.opacity = 1;
                                cb();
                            } else {
                                npos++; 
                                actor.style.left = npos + 'px'; 
                                //actor.style.top = npos + 'px';
                                actor.style.opacity = 1;
                                
                            }
                        }**/
                    }, i * actorTimeout);
                    
                } else if (actorEffect === 'right') {
                    setTimeout(function () {
                        Style.setStyles(actor, {
                            left: element.width() + 'px',
                            top: pos.top + 'px',
                            display: 'block',
                            opacity: 0
                        });
                        var npos = element.width() /2,
                            cb = function () {
                                if (actor == actors[actors.length - 1]) {
                                    self._closeAct();
                                }
                            };
                        animate(actor,'left','px',npos,pos.left,1000, cb);
                        /**
                        var id = setInterval(animateTop, actorDuration);
                        function animateTop() {
                            if (npos === pos.left) {
                                clearInterval(id);
                                actor.style.opacity = 1;
                                cb();
                            } else {
                                npos++; 
                                actor.style.left = npos + 'px';
                                actor.style.opacity = 1;
                                
                            }
                        }**/
                    }, i * actorTimeout);
                } else {
                    setTimeout(function () {
                        Style.setStyles(actor, {
                            left: -(pos.left /2) + 'px',
                            top: pos.top + 'px',
                            display: 'block',
                            opacity: 0
                        });
                        var npos = -(pos.left /2),
                            cb = function () {
                                if (actor == actors[actors.length - 1]) {
                                    self._closeAct();
                                }
                            };
                        animate(actor,'left','px',npos,pos.left,1000, cb);
                        /**var id = setInterval(animateTop, actorDuration);
                        function animateTop() {
                            if (npos === pos.left) {
                                clearInterval(id);
                                actor.style.opacity = 1;
                                actor.style.top = pos.top + 'px';
                                cb();
                            } else {
                                npos++; 
                                actor.style.left = npos + 'px'; 
                                actor.style.opacity = 1;
                                
                            }
                        }**/
                    }, i * actorTimeout);
                }
            });
        },
        
        _resetActors: function(actors) {
            var j;
            for(j = 0; j < actors.length; j++) {
                Style.setStyles(actors[j],{
                opacity: 0,
                position: 'absolute',
                display: 'none'
            });
            }
        }
        
    };
    
    function animate(elem,style,unit,from,to,time, cb) {
        if( !elem) return;
        var start = new Date().getTime(),
            timer = setInterval(function() {
                elem.style.opacity = 1;
                var step = Math.min(1,(new Date().getTime()-start)/time);
                elem.style[style] = (from+step*(to-from))+unit;
                if( step == 1) {
                    clearInterval(timer);
                    cb && cb();

                };
            },25);
        elem.style[style] = from+unit;
    }

    return Presentation;
});
