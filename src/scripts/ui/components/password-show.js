/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */

/**
 * Password Show-Hide Modul
 * 
 * @param {Object} Utils
 * @param {Object} Classes
 * @param {Object} ShowHide
 * @param {Object} AcmsEvent
 * @param {Object} A
 * 
 * @returns {Password}
 */
define("ui/components/password-show", [
    "tools/utils",
    "tools/object/extend",
    "core/classes",
    "apps/ui/helpers/show-hide",
    "events/event",
    "core/acms"
], (Utils, Extend, Classes, ShowHide, AcmsEvent, A) => {

    'use strict';

    var PW_DATA = "acms.ui.components.password-show",
        NS = "." + PW_DATA,
        EVENT_SHOW = "show" + NS,
        EVENT_SHOWN = "shown" + NS,
        EVENT_HIDE = "hide" + NS,
        EVENT_HIDDEN = "hidden" + NS;

    /**
     * Constructor
     *
     * @param {Object} element DOM Element
     * @param {Object} options Custom Options
     * 
     * @exports ui/components/password-show
     */
    var Password = function(element, options) {
        this.element =
        this.options =
        this.isShown = null;
        this.initialize(element, options);
    };

    /**
     * Component Name
     *
     * @var {String}
     */
    Password.MODULE = 'Password';

    /**
     * Default Options
     *
     * 'placement'  =>  Can be 'after' or 'before'. Default: 'after'
     * 'message'    =>  Custom Hover Message when hovering over the icon
     * 'iconshown'  =>  Icon Classes to be used for show password (Default: 'acms-icon acms-icon-eye')
     * 'iconhidden' =>  Icon Classes to be used (Default: 'acms-icon acms-icon-eye-slash')
     * 'iconwrapper'=>  Icon Wrapper Element. Might be 'i' or 'span' in most cases
     */
    Password.DEFAULT_OPTIONS = {
        placement:      'after',
        message:        A.Translator._('Klicke hier um das Passwort zu zeigen/verbergen'),
        iconshown:      'acms-icon,acms-icon-eye',
        iconhidden:     'acms-icon,acms-icon-eye-slash',
        iconwrapper:    'i'
    };

    Password.needInit = true;

    Password.init = function(element, options) {
        var pw = new Password(element, options);
        pw.element.acmsData(PW_DATA, pw);

        return pw;
    };

    Password.prototype = {

        constructor: Password,

        initialize: function(element, options) {
            this.element = element;
            if(element.tagName.toLowerCase() != 'password') {
                this.element = this.element.querySelector('input[type=password]')
            }
            this.options   = this.buildOptions(options);
            this.isShown = false;
            this._prepareInput();
            this.listen();

        },

        /**
         * Build the Options
         *
         * @param {Object} options
         */
        buildOptions: function (options) {
            return Extend.flat(
                    {},
                    true,
                    this.getDefaultOptions(),
                    options,
                    this.element.acmsData()
                    );
        },
        
        /**
         * Gets the Default Options
         *
         * @returns {Password.DEFAULT_OPTIONS|Object}
         */
        getDefaultOptions: function () {
            return Password.DEFAULT_OPTIONS;
        },

        toggle: function(_relatedTarget) {
            this[!this.isShown ? 'show' : 'hide'](_relatedTarget);
        },

        show: function(_relatedTarget) {
            var e  = AcmsEvent.createCustom(EVENT_SHOW, {relatedTarget: _relatedTarget}),
                e2 = AcmsEvent.createCustom(EVENT_SHOWN, {relatedTarget: _relatedTarget}),
                o = this.options;
            AcmsEvent.dispatch(this.element, e);
            this.isShown = true;
            ShowHide.hide(this.element);
            ShowHide.show(this.$text);
            var i = this.$icon.querySelector(o.iconwrapper);
            Classes.removeClass(i, o.iconshown);
            Classes.addClass(i, o.iconhidden);
            AcmsEvent.dispatch(this.element, e2);
        },

        hide: function(_relatedTarget) {
            var e = AcmsEvent.createCustom(EVENT_HIDE, {relatedTarget: _relatedTarget}),
                e2 = AcmsEvent.createCustom(EVENT_HIDDEN, {relatedTarget: _relatedTarget}),
                o = this.options;
            AcmsEvent.dispatch(this.element, e);
            this.isShown = false;
            ShowHide.show(this.element);
            ShowHide.hide(this.$text);
            var i = this.$icon.querySelector(o.iconwrapper);
            Classes.removeClass(i, o.iconhidden);
            Classes.addClass(i, o.iconshown);
            AcmsEvent.dispatch(this.element, e2);
        },

        val: function(value) {
            if (typeof value === 'undefined') {
                return this.element.value;
            } else {
                this.element.value = value;
                this.$text.value = value;
            }
        },

        listen: function() {
            var self = this;
            AcmsEvent.add(this.$text, 'keyup', function(e) {
                self.element.value = self.$text.value;
                AcmsEvent.fireChange(self.element);
            });
            AcmsEvent.add(self.$icon, 'click', function(e) {
                self.$text.value = self.element.value;
                self.toggle(e.target);
            });
        },

        _prepareInput: function() {
            var o = this.options;
            var input = document.createElement('input');
            input.type = "text";
            Classes.addClass(input, 'field');
            input.style = this.element.style;
            input.setAttribute('placeholder', (this.element.getAttribute('placeholder') || ''));
            input.value = this.element.value;
            
            if (this.element.getAttribute('readonly')) {
                input.setAttribute('readonly', true);
                input.readonly = true;
            }
            
            var icn = document.createElement(o.iconwrapper);
            var span = document.createElement('span');
            span.setAttribute('tabindex', 100);
            span.setAttribute('title', o.message);
            Classes.addClass(span, 'add-on,field-icon,cursor-pointer');
            Classes.addClass(icn, o.iconshown);
            span.appendChild(icn);
            if (this.options.placement === 'before') {
                this.element.parentNode.insertBefore(input, this.element);
                this.element.parentNode.insertBefore(span, input);
            } else {
                this.options.placement = 'after';
                this.element.parentNode.appendChild(input);
                this.element.parentNode.appendChild(span);
            }
            ShowHide.hide(input)
            this.$text = input;
            this.$icon = span;
        }
        
    };

    return Password;

});

