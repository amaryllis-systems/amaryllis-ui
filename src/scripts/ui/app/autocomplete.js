/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('ui/app/autocomplete', [
    'tools/utils',
    'events/event',
    'core/classes',
    'core/selectors',
    'core/acms'
], function (Utils, AcmsEvent, Classes, Selectors, A) {
    
    'use strict';
    A.Logger.logDeprecated('ui/app/autocomplete is deprecated. use apps/searching/autocomplete instead');
    
    /**
     * Autocomplete
     *
     * @param {*} element
     * @param {*} options
     * 
     * @deprecated use `apps/searching/autocomplete` instead
     */
    var AutoComplete = function (element, options) {
        this.element    =
        this.options    =
        this.container  =
        this.value      =
        this.lastValue  =
        this.term       = null;
        this.initialize(element, options);
    };

    AutoComplete.MODULE = "Auto Complete";

    AutoComplete.NS = "acms.ui.app.autocomplete";

    AutoComplete.DEFAULT_OPTIONS = {
        remote: null, // String remote URL
        menuClass: '',
        updateValue: false,
        minChars: 3,
        offsetLeft: 0,
        offsetTop: 1,
        selected: 'selected', // string - CSS Klasse für gewählte Werte
        data: null, // NULL | Object - Zusätzliche Daten
        renderItem: function (item, search, list) {
            // escape special characters
            var isObj = typeof item === "object";
            var val = isObj ? item.value : item;
            var label = isObj ? item.label : item;
            search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
            var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
            var li = document.createElement('li');
            Classes.addClass(li, 'autocomplete-suggestion');
            li.setAttribute('data-value', val);
            li.setAttribute('data-label', label);
            li.acmsData('autocompleteitem', item);
            li.innerHTML = label.replace(re, "<b>$1</b>");
            list.appendChild(li);

            return li;
        },
        /**
         * onSelect Trigger, wenn das Element angeklickt wird
         * 
         * @param {Event}   e       Das Event
         * @param {String}  value   Der Wert
         * @param {String}  label   Das Label, wenn gesetzt, andernfalls das Value
         * @param {Element} item    Das HTML-Element, auf das geklickt wurde
         * 
         */
        onSelect: function (e, value, label, item) {
            console.log('click')
        },
        
        onResponse: function(xhr, response, statusText) {},

        css: AutoComplete.css,
        themecss: false
    };

    AutoComplete.css = "media/css/ui/autocomplete";
    
    AutoComplete.needInit = true;
    
    AutoComplete.init = function(element, options) {
        var ac = new AutoComplete(element, options);
        ac.element.acmsData(AutoComplete.NS, ac);
        
        return ac;
    }

    AutoComplete.prototype = {
        constructor: AutoComplete,
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this.isInModal = Selectors.closest(element, '.modal');
            this._buildContainer();
            if (this.element.tagName.toUpperCase() === 'INPUT') {
                this.element.setAttribute('autocomplete', 'off');
            }
            this.cache = {};
            this.listen();
        },
        listen: function () {
            var element = this.element,
                    self = this,
                    o = this.options, container = this.container;
            var resizeContainer = function (e) {
                return self.updateContainer(e)
            };
            AcmsEvent.add(container, 'resize', resizeContainer);

            AcmsEvent.add(self.element, 'keydown', self._onKeyDown.bind(self));
            AcmsEvent.add(self.element, 'keyup', self._onKeyUp.bind(self));
        },
        getDefaultOptions: function () {
            return AutoComplete.DEFAULT_OPTIONS;
        },
        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },
        updateContainer: function (resize, next) {
            var self = this,
                    o = this.options,
                    element = this.element, container = self.container;
            var rect = element.getBoundingClientRect();
            container.style.left = Math.round(rect.left + (window.pageXOffset || document.documentElement.scrollLeft) + o.offsetLeft) + 'px';
            container.style.top = Math.round(rect.bottom + (window.pageYOffset || document.documentElement.scrollTop) + o.offsetTop) + 'px';
            container.style.width = Math.round(rect.right - rect.left) + 'px'; // outerWidth
            if (!resize) {
                container.style.display = 'block';
                if(self.isInModal) {
                    container.style.zIndex = 1900;
                }
                self.ul.setAttribute('aria-hidden', 'false');
                if (!container.maxHeight) {
                    container.maxHeight = parseInt((window.getComputedStyle ? getComputedStyle(container, null) : container.currentStyle).maxHeight);
                }
                if (!container.suggestionHeight)
                    container.suggestionHeight = container.querySelector('.autocomplete-suggestion').offsetHeight;
                if (container.suggestionHeight)
                    if (!next)
                        container.scrollTop = 0;
                    else {
                        var scrTop = container.scrollTop, selTop = next.getBoundingClientRect().top - container.getBoundingClientRect().top;
                        if (selTop + container.suggestionHeight - container.maxHeight > 0)
                            container.scrollTop = selTop + container.suggestionHeight + scrTop - container.maxHeight;
                        else if (selTop < 0)
                            container.scrollTop = selTop + scrTop;
                    }
            }
        },
        _buildContainer: function () {
            this.container = document.createElement('div');
            var o = this.options, container = this.container;
            Classes.addClass(this.container, 'autocomplete v1');
            if (o.menuClass) {
                Classes.addClass(this.container, o.menuClass);
            }
            container.setAttribute('aria-live', 'polite');
            container.setAttribute('aria-hidden', 'true');
            var ul = document.createElement('ul');
            ul.setAttribute('id', Utils.generateUniqeId());
            ul.id = ul.getAttribute('id');
            container.appendChild(ul);
            this.ul = ul;
            this.ulID = ul.getAttribute('id');
            document.body.appendChild(container);
        },
        _onMouseLeave: function (e) {
            var self = this,
                    o = self.options,
                    container = self.container,
                    selected = e.target;
            if(!Selectors.matches(selected, '.autocomplete-suggestion')) {
                selected = Selectors.closest(selected, '.autocomplete-suggestion');
            }
            if (selected && Classes.hasClass(selected, o.selected)) {
                setTimeout(function () {
                    Classes.removeClass(selected, o.selected);
                }, 20);
            }
        },
        _onMouseOver: function (e) {
            var self = this,
                    o = self.options,
                    container = self.container,
                    target = e.target,
                    selected = container.querySelector('.autocomplete-suggestion.' + o.selected);
            if(!Selectors.matches(target, '.autocomplete-suggestion')) {
                target = Selectors.closest(target, '.autocomplete-suggestion');
            }
            if (selected) {
                setTimeout(function () {
                    Classes.removeClass(selected, o.selected);
                }, 20);
            }
            Classes.addClass(target, o.selected);
        },
        _onMouseDown: function (e) {
            var target = e.target,
                    self = this,
                    container = self.container,
                    o = self.options;
            if (!Classes.hasClass(target, 'autocomplete-suggestion')) {
                var closest = Selectors.closest(target, '.autocomplete-suggestion');
                if(closest) {
                    target = closest;
                }
            }
            if (Classes.hasClass(target, 'autocomplete-suggestion')) {
                var v = target.getAttribute('data-value'),
                        l = target.getAttribute('data-label');

                self.element.value = v;
                o.onSelect(e, v, l, target);
                container.style.display = 'none';
                container.setAttribute('aria-hidden', 'true');
            }
        },
        _onKeyDown: function (e) {
            var key = e.keyCode || e.which,
                    self = this,
                    o = self.options,
                    container = self.container;
            // down (40), up (38)
            if ((key === 40 || key === 38) && container.innerHTML) {
                var next, sel = container.querySelector('.autocomplete-suggestion.selected');
                if (!sel) {
                    next = (key === 40) ? 
                            container.querySelector('.autocomplete-suggestion') :
                            container.childNodes[container.childNodes.length - 1]; // first : last
                    Classes.addClass(next, 'selected');
                    if(o.updateValue) {
                        self.element.value = next.getAttribute('data-value');
                    }
                } else {
                    next = (key === 40) ? sel.nextSibling : sel.previousSibling;
                    if (next) {
                        Classes.removeClass(sel, 'selected');
                        Classes.addClass(next, 'selected');
                        if(o.updateValue) {
                            self.element.value = next.getAttribute('data-value');
                        }
                    } else {
                        Classes.removeClass(sel, 'selected');
                        if(o.updateValue) {
                            self.element.value = self.lastValue;
                        }
                        next = 0;
                    }
                }
                self.updateContainer(0, next);
                return false;
            }
            // esc
            else if (key === 27) {
                if(o.updateValue) {
                    self.element.value = self.lastValue;
                }
                container.style.display = 'none';
            }
            // enter
            else if (key === 13 || key === 9) {
                var sel = container.querySelector('.autocomplete-suggestion.selected');
                if (sel && container.style.display != 'none') {
                    o.onSelect(e, sel.getAttribute('data-value'), sel.getAttribute('data-label'), sel);
                    setTimeout(function () {
                        container.style.display = 'none';
                    }, 20);
                }
            }
        },
        _onKeyUp: function (e) {
            var key = e.keyCode || e.which;
            var self = this, 
                o = this.options,
                container = self.container;
            if (!key || (key < 35 || key > 40) && key != 13 && key != 27) {
                var val = self.element.value;
                if (val.length >= o.minChars) {
                    if (val != self.lastValue) {
                        self.lastValue = val;
                        clearTimeout(self.timer);
                        if (o.cache) {
                            if (val in self.cache) {
                                self._suggest(self.cache[val]);
                                return;
                            }
                            for (var i = 1; i < val.length - o.minChars; i++) {
                                var part = val.slice(0, val.length - i);
                                if (part in self.cache && !self.cache[part].length) {
                                    self._suggest([]);
                                    return;
                                }
                            }
                        }
                        self.timer = setTimeout(function () {
                            self._request(val, self._suggest.bind(self))
                        }, o.delay);
                    }
                } else {
                    self.lastValue = val;
                    container.style.display = 'none';
                }
            }
        },
        
        _request: function(term, cb) {
            var fData = new FormData(), 
                o = this.options, k,
                fail = function(xhr, textStatus) {
                    Acms.Logger.logWarning(textStatus);
                    console.log(textStatus);
                }, always = function(xhr, response, statusText) {
                    if(typeof o.onResponse === 'function') {
                        o.onResponse(xhr, response, statusText);
                    }
                };
            fData.append('term', term);
            if(typeof o.data === "object" && null !== o.data) {
                for(k in o.data) {
                    if(o.data.hasOwnProperty(k)) {
                        fData.append(k, o.data[k]);
                    }
                }
            }
            require(['http/request'], function(Request) {
                Request.post(o.remote, fData, cb, fail, always);
            });
        },
        
        _suggest: function (data) {
            var self = this,
                o = self.options,
                container = self.container,
                val = self.element.value, i, len = typeof data.length !== "undefined" ? data.length : Object.keys(data).length;
                self.cache[val] = data;
                self.ul.innerHTML = '';
                if (len && val.length >= o.minChars) {
                    if(typeof data.length !== "undefined") {
                        for (i = 0; i < len; i++) {
                            var item = o.renderItem(data[i], val, self.ul);
                            AcmsEvent.add(item, 'mouseleave', self._onMouseLeave.bind(self));
                            AcmsEvent.add(item, 'mouseover', self._onMouseOver.bind(self));
                            AcmsEvent.add(item, 'mousedown', self._onMouseDown.bind(self));

                        }
                    } else {
                        for(var prop in data) {
                            if(data.hasOwnProperty(prop)) {
                                var item = o.renderItem(data[prop], val, self.ul);
                                AcmsEvent.add(item, 'mouseleave', self._onMouseLeave.bind(self));
                                AcmsEvent.add(item, 'mouseover', self._onMouseOver.bind(self));
                                AcmsEvent.add(item, 'mousedown', self._onMouseDown.bind(self));
                            }
                        }
                    }
                    self.updateContainer(0);
                } else
                    container.style.display = 'none';
            },
        
    };

    return AutoComplete;
});
