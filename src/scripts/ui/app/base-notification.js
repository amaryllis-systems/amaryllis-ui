/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @copyright 2013 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright 2014 - 2018 Amaryllis Systems GmbH http://www.amws.eu/
 * @copyright 2017 - 2018 Amaryllis CMS Project http://www.amaryllis-cms.de/
 * @license https://www.amaryllis-cms.de/lizenzen/
 * @license Apache Licence 2.0 https://www.apache.org/licenses/LICENSE-2.0
 *
 * Dual-Licensed with Amaryllis CMS EULA. You may obtain a copy of the
 * EULA at
 *
 *     https://www.amaryllis-cms.de/lizenzen/eula/
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */



define("ui/app/base-notification", [
    "tools/utils",
    "core/selectors",
    "tools/string/string-tool",
    "events/event",
    "templates/template-engine"
], function(Utils, Selectors, StringTool, AcmsEvent, TemplateEngine) {
    
    'use strict';
    
    var number = 0;
    var incPosition = 0;
    
    var BaseNotification = {
        MODULE: "Web-Notification",
        
        POSITION_TOP_LEFT: 1,
        POSITION_TOP_RIGHT: 2,
        POSITION_BOTTOM_RIGHT: 3,
        POSITION_BOTTOM_LEFT: 4,
        
        TYPE_DEFAULT: 'text-default',
        TYPE_GROWL: 'text-muted',
        TYPE_DANGER: 'text-danger',
        TYPE_ERROR: 'text-danger',
        TYPE_SUCCESS: 'text-success',
        TYPE_WARNING: 'text-warning',
        TYPE_INFO: 'text-info',
        TYPE_ROYAL: 'text-royal',
        
        /**
         * 
         * @param {Object}  options Die eignene optionen
         * 
         * 
         */
        build: function(options) {
            var self = this,
                o = Utils.extend({}, self.getDefaultOptions(), options),
                notif, dismiss, delay, timer,
                notification = self._template(o, function(id, content) {
                    delay = o.delay || 5;
                    timer = setTimeout(function () {
                        self._hide(id);
                    }, 1000 * delay);
                    notif = document.querySelector('#notification-'+id);
                    dismiss = notif.querySelector('.dismiss');
                    AcmsEvent.add(dismiss, 'click', self._onClick.bind(self));
                    
                    notif.acmsData('timer', timer);
                });
            // div = document.createElement('div'), notif, dismiss, delay, timer;
            // div.innerHTML = notification.content;
            // while(div.firstChild) {
            //    document.body.appendChild(div.firstChild);
            //}
            //delay = o.delay || 5;
            //timer = setTimeout(function () {
            //    self._hide(notification.id);
            //}, 1000 * delay);
            //notif = document.querySelector('#notification-'+id);
            //dismiss = notif.querySelector('.dismiss');
            //AcmsEvent.add(dismiss, 'click', self._onClick.bind(self));
            
            //notif.acmsData('timer', timer);
        },
        
        /**
         * Gibt die Standard-Optionen zurück
         * 
         * @returns {Object|BaseNotification.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function() {
            return BaseNotification.DEFAULT_OPTIONS;
        },
        
        _template: function (options, cb) {
            if(!options.image || !StringTool.isUrl(options.image)) {
                options.image = false;
                options.icon = (!options.image ? this._notificationIconFromType(options.type) : options.image);
            }
            incPosition = number * 120;
            number = number + 1;
            options.id = number;
                        
            switch (parseInt(options.position, 10)) {
                case BaseNotification.POSITION_TOP_LEFT:
                    options.style = "top:" + incPosition + "px;";
                    options.custom = " top left";
                    break;
                case BaseNotification.POSITION_TOP_RIGHT:
                    options.style = "top:" + incPosition + "px;";
                    options.custom = " top right";
                    break;
                case BaseNotification.POSITION_BOTTOM_RIGHT:
                    options.style = "bottom:" + incPosition + "px;";
                    options.custom = " bottom right";
                    break;
                case BaseNotification.POSITION_BOTTOM_LEFT:
                    options.style = "bottom:" + incPosition + "px;";
                    options.custom = " bottom left";
                    break;
                default:
                    options.style = options.custom = '';
            }
            var callback = function(content) {
                cb(number, content);
            };
            TemplateEngine.compileResource(options.template, {options: options}, callback, document.body);
            
        },
        
        _hide: function(id) {
            var notification = document.querySelector('#notification-' + id);
            if(notification) {
                if(notification.parentNode) {
                    notification.parentNode.removeChild(notification);
                } else {
                    document.body.removeChild(notification);
                }
            }
            number = number - 1;
        },
        
        _onClick: function(e) {
            var self = this,
                target = e.target,
                notif = Selectors.closest(target, '.notification'), id, timer;
            id = parseInt(notif.acmsData('id'));
            timer = parseInt(notif.acmsData('timer'));
            clearTimeout(timer);
            self._hide(id);
            
        },
        
        _notificationIconFromType: function(type) {
	    if(typeof(type) === "undefined" || type === "") {
	        type = "default";
	    }
	    switch(type) {
	    	case BaseNotification.TYPE_SUCCESS:
	    	    return 'acms-icon acms-icon-check';
	    	case BaseNotification.TYPE_ERROR:
	    	case BaseNotification.TYPE_DANGER:
	    	    return 'acms-icon acms-icon-exclamation-triangle';
	    	case BaseNotification.TYPE_WARNING:
	    	    return 'acms-icon acms-icon-exclamation-circle';
	    	case BaseNotification.TYPE_INFO:
	    	    return 'acms-icon acms-icon-question-circle acms-iconrtext';
	    	default:
	    	    return 'acms-icon acms-icon-hand-point-right acms-iconr';
	    }
        }
    };
    
    BaseNotification.DEFAULT_OPTIONS = {
        title: '',
        content: '',
        image: '',
        position: BaseNotification.POSITION_TOP_RIGHT,
        animation: 'fadeInLeftBig',
        type: BaseNotification.TYPE_DEFAULT,
        delay: 5,
        template: 'templates/html/ui/informations/notification.hbs'
    };

    return BaseNotification;
});
