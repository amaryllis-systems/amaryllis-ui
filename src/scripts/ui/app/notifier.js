/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @copyright 2013 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright 2014 - 2018 Amaryllis Systems GmbH http://www.amws.eu/
 * @copyright 2017 - 2018 Amaryllis CMS Project http://www.amaryllis-cms.de/
 * @license https://www.amaryllis-cms.de/lizenzen/
 * @license Apache Licence 2.0 https://www.apache.org/licenses/LICENSE-2.0
 *
 * Dual-Licensed with Amaryllis CMS EULA. You may obtain a copy of the
 * EULA at
 *
 *     https://www.amaryllis-cms.de/lizenzen/eula/
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

define([
    'tools/utils',
    "tools/string/string-tool",
    'core/acms'
], function(Utils, StringTool, A) {
    
    "use strict";
    
    var Notifier = function(options) {
        this.options = null;
        this.initialize(options);
    };
    
    /**
     * Notification Type ERROR
     */
    Notifier.TYPE_ERROR = 'text-danger';
    /**
     * Notification Type DANGER
     */
    Notifier.TYPE_DANGER = 'text-danger';
    /**
     * Notification Type SUCCESS
     */
    Notifier.TYPE_SUCCESS = 'text-success';
    
    /**
     * Notification Type ROYAL
     */
    Notifier.TYPE_ROYAL = 'text-royal';
    /**
     * Notification Type INFO
     */
    Notifier.TYPE_INFO = 'text-info';
    /**
     * Notification Type WARNING
     */
    Notifier.TYPE_WARNING = 'text-warning';

    /**
     * Notification Type Growl
     */
    Notifier.TYPE_GROWL = 'text-muted';
    
    Notifier.TYPE_DEFAULT = 'text-default';

    /**
     * Notification Type Default
     */
    Notifier.TYPE_DEFAULT = 'default';
    
    Notifier.DEFAULT_OPTIONS = {
        notificationType: 'web',
        title: '',
        content: '',
        image: false,
        position: 2,
        animation: 'fadeInLeftBig',
        type: '',
        delay: 5,
        fallback: true
    };
    
    Notifier.DEFAULT_TYPE = "web";
    
    Notifier.prototype = {
        constructor: Notifier,
        initialize: function(options) {
            this.options = this.buildOptions(options);
            
        },
        
        /**
         * Erstellt die Benachrichtigung
         * 
         * 
         */
        show: function() {
            var type = this.options.notificationType || Notifier.DEFAULT_TYPE;
            switch(type) {
                case 'desktop':
                    this._showDesktopNotification();
                    break;
                case 'web':
                default:
                    this._showWebNotification();
                    break;
            }
        },
        
        _showDesktopNotification: function() {
            var self = this, o = this.options;
            require(["ui/app/desktop-notification"], function() {
                window.Notification.requestPermission(function(permission) {
                    if(permission === "granted") {
                        window.Notification(o.title, o);
                    } else if(permission === "unknown" || o.fallback === true) {
                        // nicht definiert.. nimmt automatisch das fallback, 
                        // da der Benutzer die Nachricht sonst verliert
                        return self._showWebNotification();
                    }
                });
            });
        },
        
        _showWebNotification: function() {
            var self = this;
            require(["ui/app/base-notification"], function(WebNotification) {
                WebNotification.build(self.options);
            });
        },
        
        getDefaultOptions: function() {
            return Notifier.DEFAULT_OPTIONS;
        },
        
        buildOptions: function(options) {
            var ret = Utils.extend({}, Notifier.DEFAULT_OPTIONS, options);
            if(ret.fallback === 'true' || parseInt(ret.fallback) === 1) {
                ret.fallback = true;
            }
            return ret;
        }
    }
    
    Acms.Notifications = {
        TYPE_ERROR: Notifier.TYPE_ERROR,
        TYPE_DANGER: Notifier.TYPE_DANGER,
        TYPE_SUCCESS: Notifier.TYPE_SUCCESS,
        TYPE_INFO: Notifier.TYPE_INFO,
        TYPE_WARNING: Notifier.TYPE_WARNING,
        TYPE_DEFAULT: Notifier.TYPE_DEFAULT,
        TYPE_ROYAL: Notifier.TYPE_ROYAL,
        
        create: function(options) {
            var notification = new Notifier(options);
            notification.show();
        },
        
        createFromResponse: function(response) {
            var settings = {
                image: null,
                title: response.title,
                content: response.message
            };
            if(response.status === 'success') {
                settings.type = Acms.Notifications.TYPE_SUCCESS
            } else {
                settings.type = Acms.Notifications.TYPE_DANGER
            }
            var notification = new Notifier(settings);
            notification.show();
        },

        createNotification: function(message, title, type, url, delayTime) {
            url = (typeof(url) !== "undefined" && StringTool.isUrl(url)) ? url : '';
            delayTime = (typeof(delayTime) === "undefined") ? 5 : delayTime;
            var settings = {
                notificationType: 'web',
                image: null,
                title: title,
                content: message,
                url: url,
                type: type,
                delay: delayTime
            };
            var notification = new Notifier(settings);
            notification.show();
        },
        
        createDesktopNotification: function(message, title, type, url, delayTime) {
            url = (typeof(url) !== "undefined" && StringTool.isUrl(url)) ? url : '';
            delayTime = (typeof(delayTime) === "undefined") ? 5 : delayTime;
            var settings = {
                notificationType: 'web',
                image: null,
                title: title,
                content: message,
                url: url,
                type: type,
                delay: delayTime
            };
            var notification = new Notifier(settings);
            notification.show();
        }

    };
    
    return Notifier;
});
