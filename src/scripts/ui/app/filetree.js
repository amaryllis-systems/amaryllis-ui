/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @copyright 2013 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright 2014 - 2018 Amaryllis Systems GmbH http://www.amws.eu/
 * @copyright 2017 - 2018 Amaryllis CMS Project http://www.amaryllis-cms.de/
 * @license https://www.amaryllis-cms.de/lizenzen/
 * @license Apache Licence 2.0 https://www.apache.org/licenses/LICENSE-2.0
 *
 * Dual-Licensed with Amaryllis CMS EULA. You may obtain a copy of the
 * EULA at
 *
 *     https://www.amaryllis-cms.de/lizenzen/eula/
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */


define('ui/app/filetree', [
    'tools/utils',
    'core/selectors',
    'core/classes',
    'events/event',
    "ui/app/media-manager/folder",
    'core/acms'
], function (Utils, Selectors, Classes, AcmsEvent, Folder, A) {

    "use strict";

    /**
     * Constructor
     * 
     * @param {Element}     element     Das HTML-Element
     * @param {Object}      options     Die Optionen
     * @returns {Filetree}
     */
    var Filetree = function (element, options) {
        this.element =
                this.options =
                this.toggles = null;
        this.options = Filetree.DEFAULT_OPTIONS;

        this.initialize(element, options);
    };

    /**
     * Modul Name
     * 
     * @var {String}
     */
    Filetree.MODULE = "File Tree";

    /**
     * Modul Namespace
     * 
     * @var {String}
     */
    Filetree.NS = "acms.ui.app.filetree";

    /**
     * Standard-Optionen des Moduls
     * 
     * @var {Object}
     */
    Filetree.DEFAULT_OPTIONS = {
        beforeOpen: function(tree, node, self) {
            return true;
        },
        onOpen: function(tree, node, self) {
            return true;
        },
        beforeClose: function(tree, node, self) {
            return true;
        },
        onClose: function(tree, node, self) {
            return true;
        },
        open: 'open', // CSS-State für offen
        multiopen: false, // Mehrere offen haben?
    };

    /**
     * Sagt dem Modul-Loader, dass das Modul initialisiert werden muss, wenn es 
     * über diesen geladen wird.
     * 
     * @var {Boolean}
     */
    Filetree.needInit = true;

    /**
     * Modul Initialisierung
     * 
     * @param {Element}     element     Das HTML-Element
     * @param {Object}      options     Die Optionen
     * @returns {Filetree}
     */
    Filetree.init = function (element, options) {
        var v = new Filetree(element, options);
        v.element.acmsData(Filetree.NS, v);

        return v;
    };

    /**
     * Klassen Definition
     */
    Filetree.prototype = {

        constructor: Filetree,

        /**
         * Interner Constructor
         * 
         * @param {Element} element     HMTL Element
         * @param {Objct}   options     Eigene Optionen
         * 
         * 
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },

        /**
         * Aufsetzen der Event-Listener
         * 
         */
        listen: function () {
            var self = this,
                    o = self.options,
                    element = self.element;
            self.checkboxes.forEach(function(chb) {
                AcmsEvent.add(chb, 'change', self._onChange);
            });
        },
        
        
        /**
         * Gibt die Standard-Optionen zurück
         * 
         * @returns {Filetree.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return Filetree.DEFAULT_OPTIONS;
        },

        /**
         * Bilden der Optionen
         * 
         * Bildet die Optionen basierend auf den Standard-Optionen, den 
         * übergebenen Optionen des Constructors und den Element Data-Attributen. 
         * Letztere gewinnen den Kampf, wenn gesetzt.
         * 
         * @param {Object} options    Die eignenen Optionen
         * @returns {Object|Filetree.DEFAULT_OPTIONS}
         */
        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },
        
        /**
         * Setter/Getter für die Optionen
         *
         * @param {String|NULL} a   Der Optionen-Schlüssel oder NULL
         * @param {Mixed}       b   Undefined oder der Wert für a
         * @returns {Accordion.options|Boolean|Accordion.prototype}
         */
        option: function (a, b) {
            if (!a) {
                return this.options;
            }
            if (typeof b === "undefined") {
                return (typeof this.options[a] === "undefined") ? false : this.options[a] || false;
            }
            this.options[a] = b;
            return this;
        },
        
        getCurrent: function() {
            return this.currentFolder;
        },

        /**
         * Change Event der Checkboxen
         * 
         * @param {Event} e Das Change Event der Checkbox
         * 
         * @returns {Boolean|undefined}
         */
        _onChange: function(e) {
            var target = e.target,
                item = Selectors.closest(target, 'li'),
                self = this, res,
                o = self.options;
            if(target.checked) {
                res = o.beforeOpen(self.tree, target, self);
                if(false === res) {
                    target.checked = false;
                    target.removeAttribute('checked');
                    AcmsEvent.fireChange(target);
                    return false;
                }
                target.setAttribute('checked', 'checked');
                Classes.addClass(item, o.open);
                if(!o.multiopen) {
                    var rest = Selectors.qa('input:not(#'+target.id+')', self.element);
                    console.log(rest)
                    rest.forEach(function(chb) {
                        if(!Selectors.closest(chb, '#'+item.getAttribute('id'))
                            && !Selectors.q('#'+item.getAttribute('id'), Selectors.closest(chb,'li'))) {
                        
                            self._closeAll(chb, item);
                        }
                    });
                    //self._closeAll(target, item);
                }
                self.currentFolder = target;
                o.onOpen(self.tree, target, self);
            } else {
                res = o.beforeClose(self.tree, target, self);
                if(false === res) {
                    target.checked = true;
                    target.setAttribute('checked', 'checked');
                    AcmsEvent.fireChange(target);
                    return false;
                }
                target.removeAttribute('checked');
                Classes.removeClass(item, o.open);
                o.onClose(self.tree, target, self);
                self.currentFolder = null;
                self._closeAll(target, item);
            }
        },

        _closeAll: function(target, item) {
            var self = this, i, chb, li, o = self.options,
                id = target.id, root = item.acmsData('root');
            var subs = Selectors.qa('input', item);
            subs.forEach(function(sub) {
                if(sub.checked) {
                    sub.checked = false;
                    sub.removeAttribute('checked');
                    AcmsEvent.fireChange(sub);
                    var itm = Selectors.closest(sub, 'li');
                    Classes.removeClass(itm, o.open);
                }
            });
            
            
        },

        /**
         * Vorbereiten des Moduls
         * 
         * 
         */
        _prepare: function () {
            var self = this,
                    element = self.element,
                    o = self.options, fol, i;
            self.checkboxes = Selectors.qa('input[type="checkbox"]', element);
            self.tree = Selectors.qa('input', element);
            self.folders = new Array();
            self.currentFolder = null;
            var itms = Selectors.qa('li', element), j, itm;
            for(j = 0; j < itms.length; j++) {
                itm = itms[j];
                if(!itm.id) {
                    itm.setAttribute('id', 'item-'+j);
                }
                fol = Folder.init(itm, o);
                i = parseInt(itm.getAttribute('data-id'));
                self.folders[i] = fol;
            }
            self.items = itms;
            
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        }

    };

    return Filetree;

});

