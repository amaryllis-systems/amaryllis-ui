/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @copyright 2013 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright 2014 - 2018 Amaryllis Systems GmbH http://www.amws.eu/
 * @copyright 2017 - 2018 Amaryllis CMS Project http://www.amaryllis-cms.de/
 * @license https://www.amaryllis-cms.de/lizenzen/
 * @license Apache Licence 2.0 https://www.apache.org/licenses/LICENSE-2.0
 *
 * Dual-Licensed with Amaryllis CMS EULA. You may obtain a copy of the
 * EULA at
 *
 *     https://www.amaryllis-cms.de/lizenzen/eula/
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */


define("ui/app/desktop-notification", [
    "tools/string/string-tool"
], function(StringTool) {
    
    var PERMISSION_GRANTED = 'granted',
        PERMISSION_DENIED = 'denied',
        PERMISSION_UNKNOWN = 'unknown';
	
	var a = [], iv, i=0;

	function swaptitle(title) {
            if(a.length===0){
                a = [document.title];
            }

            a.push(title);

            if(!iv){
                iv = setInterval(function() {
                    if(a.indexOf(document.title) === -1 ){
                        a[0] = document.title;
                    }
                    document.title = a[++i%a.length];
                }, 1000);
            }
	}

	function swapTitleCancel() {
            if(a.length===0){
                return;
            }
            if("external" in window && "msSiteModeClearIconOverlay" in window.external ){
                window.external.msSiteModeClearIconOverlay();
            }
            clearInterval(iv);
            iv = false;
            document.title = a[0];
            a = [];
	}
	
	function addEvent(el,name,func){
		if(name.match(" ")){
			var a = name.split(' ');
			for(var i=0;i<a.length;i++){
				addEvent( el, a[i], func);
			}
		}
		if(el.addEventListener){
			el.removeEventListener(name, func, false);
			el.addEventListener(name, func, false);
		}
		else {
			el.detachEvent('on'+name, func);
			el.attachEvent('on'+name, func);
		}
	}


	function check_permission() {
            if(("external" in window) && ("msIsSiteMode" in window.external)){
                return window.external.msIsSiteMode()? PERMISSION_GRANTED : PERMISSION_UNKNOWN;
            } else if("webkitNotifications" in window){
                return window.webkitNotifications.checkPermission() === 0 ? PERMISSION_GRANTED : PERMISSION_DENIED;
            } else if("mozNotification" in window.navigator){
                return PERMISSION_GRANTED;
            } else {
                return PERMISSION_UNKNOWN;
            }
	}

	function update_permission() {
            window.Notification.permission = check_permission();
            return window.Notification.permission;
	}

	if(!Object(window.Notification).permission){
		addEvent(window, "focus scroll click", swapTitleCancel);
		window.Notification = function(message, options){
			if(!(this instanceof window.Notification)){
				return new window.Notification(message,options);
			}
			var n, self = this;
			options = options || {};

			this.body = options.content || '';
			this.icon = StringTool.isUrl(options.image) ? options.image : '';
			this.lang = options.lang || '';
			this.tag = options.tag || '';
			this.close = function(){
				swapTitleCancel();
				if(Object(n).close){
					n.close();
				}

				self.onclose();
			};
			this.onclick = function(){};
			this.onclose = function(){};
			swaptitle(message);
			if(("external" in window) && ("msIsSiteMode" in window.external)){
                            if(window.external.msIsSiteMode()){
                                window.external.msSiteModeActivate();
                                if(this.icon){
                                        window.external.msSiteModeSetIconOverlay(this.icon, message);
                                }
                            }
			} else if("webkitNotifications" in window){
                            if(window.webkitNotifications.checkPermission() === 0){
                                n = window.webkitNotifications.createNotification(this.icon, message, this.body );
                                n.show();
                                n.onclick = function(){
                                    self.onclick();
                                    window.focus();
                                    setTimeout( function(){ n.cancel(); }, 1000);
                                };
                            }
			} else if( "mozNotification" in window.navigator ){
                            var m = window.navigator.mozNotification.createNotification( message, this.body, this.icon );
                            m.show();
			}
		};

		window.Notification.requestPermission = function(cb){
			cb = cb || function(){};
			if(("external" in window) && ("msIsSiteMode" in window.external)){
				try{
					if( !window.external.msIsSiteMode() ){
						window.external.msAddSiteMode();
						cb( PERMISSION_UNKNOWN );
					}
				}
				catch(e){}
				cb( update_permission() );
			}
			else if("webkitNotifications" in window){
                            window.webkitNotifications.requestPermission(function(){
                                cb( update_permission() );
                            });
			}
			else {
                            cb( update_permission() );
			}
		};
		update_permission();
	}
    
});
