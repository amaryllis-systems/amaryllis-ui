/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */


define("ui/widgets/grid", [
    'tools/utils',
    'core/selectors',
    'core/classes',
    'events/event'
], function(Utils, Selectors, Classes, AcmsEvent) {
    
    "use strict";
    
    var Grid = function(element, options) {
        this.element =
        this.options = null;
        this.initialize(element, options);
    };
    
    Grid.MODULE = "News Board";
    
    Grid.VERSION = "1.5.0";
    
    Grid.DEFAULT_OPTIONS = {
        paddingX: 10,
        paddingY: 20,
        noColumns: 3,
        marginBottom: 70,
        singleColumnBreakpoint: 700,
        artikel: 'article'
    };
    
    Grid.needInit = true;
    
    Grid.init = function(element, options) {
        var nb = new Grid(element, options);
        
        return nb;
    };
    
    Grid.css = "media/css/widgets/grid.min.css";
    
    Grid.prototype = {
        constructor: Grid,
        
        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this.listen();
        },
        
        getDefaultOptions: function() {
            return Grid.DEFAULT_OPTIONS;
        },
        
        buildOptions: function(options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },
        
        listen: function() {
            var self = this,
            resize_finish;
            var onResize = function() {
                clearTimeout(resize_finish);
                resize_finish = setTimeout( function () {
                    self.makeLayoutChange(self);
                }, 11);
            };
            AcmsEvent.add(window, onResize);
            self.makeLayoutChange(self);

            setTimeout(function() {
                var ev = AcmsEvent.create('resize');
                AcmsEvent.dispatch(window, ev);
            }, 500);
            
            var img = Selectors.qa('img', this.element), j;
            function loaded() {
                return self.makeLayoutChange(self);
            }
            for(j = 0; j < img.length; j++) {
                var i = img[j];
                if (i.complete) {
                  loaded();
                } else {
                    AcmsEvent.add(i, 'load', loaded);
                }
            }
        },
        
        calculate: function(singleColumnMode) {
            singleColumnMode = singleColumnMode || false;
            var self = this,
                o = this.options,
                tallest = 0,
                row = 0,
                element = this.element,
                element_width = element.offsetWidth,
                articles = Selectors.qa(o.artikel, element),
                article_width, i, columns, art, article;
            if(singleColumnMode === true) {
                article_width = element_width - parseInt(o.paddingX, 10);
            } else {
                article_width = (element_width - parseInt(o.paddingX, 10) * o.noColumns) / o.noColumns;
            }
            for(i = 0; i < articles.length; i++) {
                art = articles[i];
                article.style.width = article_width + 'px';
            }
            columns = o.noColumns;

            for(i = 0; i < articles.length; i++) {
                article = articles[i];
                var current_column,
                    left_out = 0,
                    top = 0,
                    prevAll = Selectors.previousAll(article),
                    index = i + 1;
                tallest = 0;

                if(singleColumnMode === false) {
                    current_column = (index % columns);
                } else {
                    current_column = 0;
                }

                for(var t = 0; t < columns; t++) {
                    Classes.removeClass(article, 'c'+t);
                }

                if(index % columns === 0) {
                    row++;
                }
                Classes.addClass(article, 'c'+current_column);
                Classes.addClass(article, 'r'+row);

                for(var j = 0; j < prevAll.length; j++) {
                    var that = prevAll[j];
                    if(Classes.hasClass(that, 'c' + current_column)) {
                        top += parseInt(that.scrollHeight) + parseInt(o.paddingY);
                    }
                }
                if(singleColumnMode === true) {
                    left_out = 0;
                } else {
                    left_out = (index % columns) * (article_width + o.paddingX);
                }
                article.style.left = left_out + 'px';
                article.style.top = top + 'px';
            }
            this.tallest(element);
            var ev = AcmsEvent.create('resize');
            AcmsEvent.dispatch(window, ev);
        },
        
        tallest: function (_container) {
            var o = this.options,
                column_heights = [],
                largest = 300, z;
            for(z = 0; z < o.noColumns; z++) {
                var temp_height = 400;
                var cols = Selectors.qa('.c'+z, _container), j;
                for(j = 0; j < cols.length; j++) {
                    var that = cols[j];
                    temp_height += that.scrollHeight;
                }
                column_heights[z] = temp_height;
            }

            largest = Math.max.apply(Math, column_heights);
            _container.style.height = (largest + (parseInt(o.paddingY) + parseInt(o.marginBottom))) + 'px';
        },
        
        makeLayoutChange: function(_self) {
            if(window.outerWidth < _self.options.singleColumnBreakpoint) {
                _self.calculate(true);
            } else {
                _self.calculate(false);
            }
        }
    };
    
    return Grid;
});
