/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */

define('ui/widgets/tagcloud/tag', [
    'tools/utils'
], function (Utils) {

    "use strict";

    /**
     * Constructor
     * 
     * @param {Element}     element     Das HTML-Element
     * @param {Object}      options     Die Optionen
     * @returns {Tag}
     */
    var Tag = function (name, count, url, epoch) {
        this.name = name;
        this.count = count;
        this.url = url;
        this.epoch = epoch;
        this.style = {};
        this.initClasses();
        this.initAnchorClasses();
        
    };
    
    Tag.fromElement = function(element) {
        var name = element.innerHTML;
        var count = element.acmsData('frequency');
        var url = element.getAttribute('href') || element.acmsData('url');
        var epoche = element.acmsData('webdate');
        var t = new Tag(name, count, url, epoche);
        t.element = element;
        
        return t;
    },

    /**
     * Modul Name
     * 
     * @var {String}
     */
    Tag.MODULE = "Tag in einer Tag-Cloud";

    /**
     * Modul Version
     * 
     * @var {String}
     */
    Tag.VERSION = "1.5.0";

    /**
     * Modul Namespace
     * 
     * @var {String}
     */
    Tag.NS = "acms.ui.widgets.tagcloud.tag";

    /**
     * Sagt dem Modul-Loader, dass das Modul nicht initialisiert werden muss, 
     * wenn es über diesen geladen wird. Der Module Loader steht hier nicht 
     * zur Verfügung..
     * 
     * @var {Boolean}
     */
    Tag.needInit = false;
    
    Tag.init = function(element, options) {
        var t = Tag.fromElement(element);
        
    };

    /**
     * Klassen Definition
     */
    Tag.prototype = {

        constructor: Tag,
        
        initClasses: function () {
            this.classes = ['tagcloud-base'];
        },
        
        initAnchorClasses: function () {
            this.anchorClasses = ['tagcloud-anchor'];
        },

        attachClass: function (className) {
            this.classes.push(className);
        },

        attachAnchorClass: function (className) {
            this.anchorClasses.push(className);
        },

        toElement: function() {
            if(this.element) {
                return this.element;
            }
            var element = document.createElement('li');
            var linkElement = document.createElement('a');
            linkElement.setAttribute('href', this.url);
            var text = document.createTextNode(this.name);
            linkElement.appendChild(text);
            linkElement.className = this.anchorClasses.join(" ");
            element.appendChild(linkElement);
            element.className = this.classes.join(" ");
            for (var prop in this.style)
              element.style[prop] = this.style[prop];
            return element;
        },

    };

    return Tag;

});

