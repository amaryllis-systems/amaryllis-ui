/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */

/**
 * Line Chart
 *
 * <p>
 *  Generator für einfache Line Charts.
 *  </p>
 *
 * @param {Utils} Utils
 * @param {AcmsEvent} AcmsEvent
 * @param {DefaultTheme} DefaultTheme
 *
 * @returns {line-chart_L16.LineChart}
 */
define("ui/widgets/charts/line-chart", [
    'tools/utils',
    'events/event',
    'ui/widgets/charts/line-themes/default'
], function (Utils, AcmsEvent, DefaultTheme) {

    "use strict";

    var LINECHART_DATA = "acms.ui.widgets.charts.line-chart",
            NS = "." + LINECHART_DATA,
            EVENT_BUILD_THEME = "buildTheme" + NS, /* Theme geladen aber nicht gebildet */
            EVENT_BUILT_THEME = "builtTheme" + NS; /* Theme geladen und gebildet (event.detail.theme) */

    /**
     * LineChart's constructor.
     *
     * @param {Object} element The DOM element container or canvas to use
     * @param {Object} options See Options.
     */
    var LineChart = function (element, options) {
        this.element =
                this.options =
                this.data = null;
        this.initialize(element, options);
    };

    LineChart.MODULE = 'Line Chart';

    LineChart.VERSION = "1.5.0";

    LineChart.DEFAULT_OPTIONS = {
        theme: 'default', /* 'flat', 'dot', 'require/path/to/theme',*/
        data: null,
        width: 750
    };

    LineChart.prototype = {
        constructor: LineChart,
        /**
         * Interner Constructor
         * @param {Canvas} element  Das Canvas Element
         * @param {type} options
         * 
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            var options = this.options, self = this;
            if(options.width === 'auto') {
                var width = element.parentNode.clientWidth;
                options.width = width;
                var winResize = function(event) {
                    console.log('resize')
                    var cwidth = element.parentNode.clientWidth,
                        cheight = Math.floor(cwidth * 0.67);
                    self.canvas.width = cwidth;
                    self.canvas.height = cheight;
                    self.options.width = cwidth;
                    self.options.height = cheight;
                    self.context.clearRect(0, 0, self.canvas.width, self.canvas.height);
                    if (window.devicePixelRatio > 1) {
                        self.canvas.style.height = self.canvas.height + "px";
                        self.canvas.style.width = self.canvas.width + "px";
                        self.canvas.height = self.canvas.height * window.devicePixelRatio;
                        self.canvas.width = self.canvas.width * window.devicePixelRatio;
                    }
                    self.resolution = window.devicePixelRatio || 1;
                    self.update();
                    self.render();
                };
                AcmsEvent.add(window, 'resize', winResize);
            }
            if (options.width && !options.height) {
                options.height = Math.floor(options.width * 0.67);
            }
            this.defaults = DefaultTheme(LineChart);
            if (typeof this.options.data === 'string') {
                try {
                    this.data = JSON.parse(this.options.data);
                } catch (E) {
                    console.log(E)
                    this.data = [];
                }
            } else {
                this.data = this.options.data || [];
            }

            var callback = function (Theme) {
                self.theme = Theme;
                self.defaults = LineChart._deepMerge(self.defaults, self.theme);
                for (var key in self.defaults) {
                    self.options = LineChart._deepMerge(self.defaults, self.options);
                }

                var styles = self.options.style,
                        defaultStyles = self.options.style["default"], k, style;
                for (style in styles) {
                    if (styles.hasOwnProperty(style)) {
                        for (k in defaultStyles) {
                            if (defaultStyles.hasOwnProperty(key)) {
                                self.options.style[style] = LineChart._deepMerge(defaultStyles, styles[style]);
                            }
                        }
                    }
                }
                self.indexes = [];
                ["fill", "axis", "tick", "line", "point", "label", "title"].forEach(function (feature) {
                    if(self.options[feature])
                    self.indexes[self.options[feature].index] = feature;
                });
                self.indexes = self.indexes.filter(function (val) {
                    if (val) {
                        return true;
                    }
                });
                if (self.element.getContext) {
                    self.canvas = element;
                    self.context = self.canvas.getContext("2d");
                } else {
                    self.canvas = document.createElement("canvas");
                    self.element.appendChild(self.canvas);
                    self.context = self.canvas.getContext("2d");
                }
                self.canvas.height = self.options.height;
                self.canvas.width = self.options.width;
                if (window.devicePixelRatio > 1) {
                    self.canvas.style.height = self.canvas.height + "px";
                    self.canvas.style.width = self.canvas.width + "px";
                    self.canvas.height = self.canvas.height * window.devicePixelRatio;
                    self.canvas.width = self.canvas.width * window.devicePixelRatio;
                }
                self.resolution = window.devicePixelRatio || 1;
                
                self.update();
                if (self.options.render) {
                    self.render();
                }
            };
            this._buildTheme(callback);
        },
        getDefaultOptions: function () {
            return LineChart.DEFAULT_OPTIONS;
        },
        /**
         *
         * @param {type} options
         * @returns {Object|LineChart.DEFAULT_OPTIONS}
         */
        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },
        refreshBounds: function () {
            var yMax = -Infinity;
            var yMin = Infinity;
            for (var key in this.data) {
                if (key !== "x") {
                    var max = -Infinity, min = Infinity;
                    this.data[key].forEach(function (v) {
                        if (v > max)
                            max = v;
                        if (v < min)
                            min = v;
                    });
                    yMax = (max > yMax) ? max : yMax;
                    yMin = (min < yMin) ? min : yMin;
                }
            }

            this.y = {
                max: (this.options.axis.y.max === undefined) ? yMax : this.options.axis.y.max,
                min: (this.options.axis.y.min === undefined) ? yMin : this.options.axis.y.min
            };

            this.y.range = this.y.max - this.y.min;
            if (this.data.x.length === 1 || typeof this.data.x === "number")
                this.x = {min: 0, max: this.data.x[0] || this.data.x};
            else
                this.x = {min: this.data.x[0], max: this.data.x[this.data.x.length - 1]};

            this.x.range = this.x.max - this.x.min;
        },
        /**
         * Updates LineChart's variables such as maxes and mins of the graphs
         * @return {null}
         */
        update: function () {
            var resolution = this.resolution;
            this.options.margin *= resolution;
            this.options.padding *= resolution;
            this.options.width *= resolution;
            this.options.height *= resolution;
            this.box = {
                x: this.options.margin,
                y: this.options.margin,
                x1: this.options.width - (2 * this.options.margin),
                y1: this.options.height - (2 * this.options.margin)
            };
            var self = this;
            this.refreshBounds();
            var data = this.getPoints();
            this.lines = data.lines;
            this.origin = data.origin;
            var padding = this.options.padding,
                    box = this.box;

            this.axis = {
                x: {
                    x: box.x - padding,
                    y: (box.y + box.y1 + padding),
                    x1: self.box.x + box.x1 + padding,
                    y1: (box.y + box.y1 + padding)
                },
                y: {
                    x: (box.x - padding),
                    y: box.y - padding,
                    x1: (box.x - padding),
                    y1: box.y + box.y1 + padding
                }
            };
        },
        /**
         * Rendrn des Graphen
         */
        render: function () {
            var self = this,
                    lines = this.lines,
                    origin = this.origin,
                    axis = this.axis,
                    defaults = self.options.style.default;
            this.element.width = this.canvas.width;
            var stepX = Math.floor(this.options.axis.x.steps),
                    stepY = Math.floor(this.options.axis.y.steps);
            var padding = this.options.padding,
                    box = this.box,
                    ox = origin.x,
                    oy = origin.y;
            this.indexes.forEach(function (feature) {
                switch (feature) {
                    case "point":
                        for (var line in lines)
                            if ((self.options.style[line] || defaults).point.visible)
                                lines[line].forEach(function (obj) {
                                    self.options.point.render.call(self, self.options.style[line] || defaults, obj.rx, obj.ry, obj.x, obj.y, obj.graph);
                                });
                        break;

                    case "axis":
                        if (defaults.axis.visible) {
                            if (defaults.axis.x.visible) {
                                self.options.axis.x.render.call(self, defaults, axis.x.x, (defaults.axis.y.fixed) ? axis.x.y : oy, axis.x.x1, (defaults.axis.y.fixed) ? axis.x.y1 : oy, "x");
                            }

                            if (defaults.axis.y.visible) {
                                self.options.axis.y.render.call(self, defaults, (defaults.axis.x.fixed) ? axis.y.x : ox, axis.y.y, (defaults.axis.x.fixed) ? axis.y.x1 : ox, axis.y.y1, "y");
                            }
                        }
                        break;

                    case "line":
                        for (var line in lines) {
                            var style = self.options.style[line] || defaults;
                            if (style.line.visible)
                                self.options.line.render.call(self, style, lines[line]);
                        }
                        break;

                    case "tick":
                        if (defaults.tick.visible) {
                            var disX = self.box.x1 / (stepX),
                                    disY = self.box.y1 / (stepY);

                            for (var i = 0; i < (stepX + 1); i++) {
                                self.options.tick.render.call(self, defaults, self.box.x + (disX * i), (defaults.tick.x.fixed) ? axis.x.y1 : oy, "x", i);
                            }
                            for (var i = 0; i < (stepY + 1); i++) {
                                self.options.tick.render.call(self, defaults, (defaults.tick.y.fixed) ? axis.y.x1 : ox, self.box.y + (disY * i), "y", i);
                            }
                        }
                        break;

                    case "label":
                        var disX = self.box.x1 / (stepX),
                                disY = self.box.y1 / (stepY);

                        if (defaults.label.x.visible)
                            for (var i = 0; i < (stepX + 1); i++)
                                self.options.label.render.call(self, defaults, self.x.min + (((self.x.max - self.x.min) / stepX) * i), self.box.x + (disX * i), (defaults.label.x.fixed) ? axis.x.y1 : oy, "x", i);

                        if (defaults.label.y.visible)
                            for (var i = 0; i < (stepY + 1); i++) {
                                var pos = stepY - i,
                                        label = self.y.min + ((self.y.max - self.y.min) / stepY) * pos;
                                self.options.label.render.call(self, defaults, label, (defaults.label.y.fixed) ? axis.y.x1 : ox, self.box.y + (disY * i), "y", i);
                            }

                        break;

                    case "fill":
                        for (var line in lines) {
                            var style = self.options.style[line] || defaults;
                            if (style.line.fill)
                                self.options.fill.render.call(self, style, lines[line]);
                        }
                        break;

                    case "title":
                        if (defaults.title.visible) {
                            // X an y title
                            var xLabel = self.options.title.x,
                                    yLabel = self.options.title.y;

                            if (defaults.title.x.visible)
                                self.options.title.render.call(self, defaults, xLabel, (self.box.x * 2 + self.box.x1) / 2, self.box.y + self.box.y1, "x");
                            if (defaults.title.y.visible)
                                self.options.title.render.call(self, defaults, yLabel, (self.box.x), (self.box.y * 2 + self.box.y1) / 2, "y");
                        }
                        break;
                }
            });
        },
        /**
         * Get the points from each graph and returns the <line> vs x.
         * @param  {Function} callback (optional) Run a function over a point.
         * @return {Object}            The lines store <name> : <point array> where a point is {rx (raster x), ry, x (actual x point), y}
         */
        getPoints: function (callback) {
            var lines = {},
                    Xmax = this.x.max,
                    Xmin = this.x.min,
                    Xrange = this.x.range,
                    Ymax = this.y.max,
                    Ymin = this.y.min,
                    Yrange = this.y.range,
                    bx = this.box.x,
                    by = this.box.y,
                    bx1 = this.box.x1,
                    by1 = this.box.y1,
                    Yorigin = by + ((by1 / Yrange) * Ymax),
                    Xorigin = bx + ((bx1 / Xrange) * Math.abs(Xmin));
            for (var key in this.data) {
                if (key === "x") {
                    continue;
                }

                lines[key] = [];

                var currArr = this.data[key],
                        length = currArr.length,
                        factor = 1;
                if (length > 1000) {
                    factor = 5;
                }
                if (length > 10000) {
                    factor = 50;
                }
                if (length > 100000) {
                    factor = 5000;
                }

                var count = length / factor;

                for (var i = 0; i < count; i++) {
                    var x = ((Xrange / (count - 1)) * i) + Xmin,
                            y = currArr[i],
                            rx = Xorigin + ((bx1 / Xrange) * x),
                            ry = Yorigin - ((by1 / Yrange) * y);

                    lines[key].push({x: x, y: y, rx: rx, ry: ry});

                    if (callback)
                        callback(rx, ry, x, y, key);
                }
            }

            return {
                lines: lines,
                origin: {
                    x: Xorigin,
                    y: Yorigin
                }
            };
        },
        /**
         * Canvas zu Image Konvertierung
         *
         * @returns {Image|LineChart.prototype.toImage.img}
         */
        toImage: function () {
            var img = new Image();
            img.src = this.canvas.toDataURL("image/png");
            return img;
        },
        /**
         * Bilden des Themas
         *
         * @param {Callable} callback Ein Callback, wenn das Layout geladen wurde
         *
         * 
         */
        _buildTheme: function (callback) {
            var themeName = this.options.theme, self = this;
            try {
                switch (themeName) {
                    case 'default':
                        require(['ui/widgets/charts/line-themes/default'], function(Theme) {
                            var e = AcmsEvent.createCustom(EVENT_BUILT_THEME, {theme: Theme});
                            AcmsEvent.dispatch(self.element, e);
                            var layout = Theme(LineChart);
                            callback(layout);
                            var e2 = AcmsEvent.createCustom(EVENT_BUILT_THEME, {theme: layout});
                            AcmsEvent.dispatch(self.element, e2);
                        });
                        break;
                    case 'flat':
                        require(['ui/widgets/charts/line-themes/flat'], function(Theme) {
                            var e = AcmsEvent.createCustom(EVENT_BUILT_THEME, {theme: Theme});
                            AcmsEvent.dispatch(self.element, e);
                            var layout = Theme(LineChart);
                            callback(layout);
                            var e2 = AcmsEvent.createCustom(EVENT_BUILT_THEME, {theme: layout});
                            AcmsEvent.dispatch(self.element, e2);
                        });
                        break;
                    case 'dot':
                        require(['ui/widgets/charts/line-themes/dot'], function(Theme) {
                            var e = AcmsEvent.createCustom(EVENT_BUILT_THEME, {theme: Theme});
                            AcmsEvent.dispatch(self.element, e);
                            var layout = Theme(LineChart);
                            callback(layout);
                            var e2 = AcmsEvent.createCustom(EVENT_BUILT_THEME, {theme: layout});
                            AcmsEvent.dispatch(self.element, e2);
                        });
                        break;
                    default:
                        if (typeof themeName !== 'string') {
                            throw new Error('Invalides Thema');
                        }
                        try {
                            require([themeName], function(Theme) {
                                var e = AcmsEvent.createCustom(EVENT_BUILD_THEME, {theme: Theme});
                                AcmsEvent.dispatch(self.element, e);
                                var layout = Theme(self);
                                callback(layout);
                                var e2 = AcmsEvent.createCustom(EVENT_BUILT_THEME, {theme: layout});
                                AcmsEvent.dispatch(self.element, e2);
                            });
                        } catch (E) {
                            console.log(E);
                            console.trace();
                        }
                }
            } catch (E) {
                console.log(E);
                console.trace();
            }
        }
    };

    /**
     * Deep merge two object a and b
     *
     * @private
     * @param  {Object} defaults The object to merge with
     * @param  {Object} options The recipient of the merge or the object to be merged into
     * @return {object}   The merged objects
     */
    LineChart._deepMerge = function (defaults, options) {
        return (function recur(defaults, options) {
            for (var key in defaults) {
                if (options[key] === undefined) {
                    options[key] = defaults[key];
                } else if (defaults[key] instanceof Object) {
                    options[key] = recur(defaults[key], options[key]);
                }
            }
            return options;
        })(defaults, options);
    };

    /**
     * LineChart's default render functions
     */
    LineChart.point = {
        circle: function (style, rx, ry, x, y, graph) {
            this.context.save();
            this.context.strokeStyle = style.point.stroke;
            this.context.lineWidth = style.point.width * this.resolution;
            this.context.fillStyle = style.point.fill;
            this.context.beginPath();
            this.context.arc(rx, ry, style.point.radius * this.resolution, 0, Math.PI * 2, true);
            this.context.fill();
            this.context.stroke();
            this.context.restore();
        }
    };

    LineChart.line = {
        line: function (style, points) {
            this.context.save();
            this.context.strokeStyle = style.line.stroke;
            this.context.lineWidth = style.line.width * this.resolution;
            this.context.beginPath();
            this.context.moveTo(points[0].rx, points[0].ry);
            var self = this;
            points.forEach(function (point) {
                self.context.lineTo(point.rx, point.ry);
            });
            this.context.stroke();
            this.context.restore();
        },
        fill: function (style, points) {
            this.context.save();
            this.context.fillStyle = style.line.fill;
            this.context.beginPath();
            this.context.moveTo(points[0].rx, points[0].ry);
            var self = this;
            points.forEach(function (point) {
                self.context.lineTo(point.rx, point.ry);
            });
            this.context.lineTo(points[points.length - 1].rx, this.box.y + this.box.y1 + ((style.line.fillToBaseLine) ? this.options.padding : 0));
            this.context.lineTo(points[0].rx, this.box.y + this.box.y1 + ((style.line.fillToBaseLine) ? this.options.padding : 0));
            this.context.closePath();
            this.context.fill();
            this.context.restore();

        }
    };

    LineChart.tick = {
        line: function (style, x, y, type, i) {
            this.context.save();
            this.context.strokeStyle = style.tick.stroke;
            this.context.lineWidth = style.tick.width * this.resolution;
            this.context.beginPath();

            var length = (i % 2 === 0) ? style.tick.major : style.tick.minor;
            length *= this.resolution;
            var mx = x, my = y;
            switch (style.tick.align) {
                case "middle":
                    if (type === "x") {
                        my = y - (length / 2);
                    } else if (type === "y") {
                        mx = x - (length / 2);
                    }
                    break;

                case "inside":
                    if (type === "x") {
                        my = y - length;
                    }
                    mx = x;
                    break;

                case "outside":
                    if (type === "x") {
                        my = y;
                    } else if (type === "y") {
                        mx = x - length;
                    }
                    break;
            }

            this.context.moveTo(mx, my);

            if (type === "x") {
                this.context.lineTo(mx, my + length);
            } else {
                this.context.lineTo(mx + length, my);
            }
            this.context.stroke();
            this.context.restore();
        }
    };

    LineChart.axis = {
        line: function (style, x, y, x1, y1, type) {
            this.context.save();
            this.context.strokeStyle = style.axis.stroke;
            this.context.lineWidth = style.axis.width * this.resolution;
            this.context.beginPath();
            this.context.moveTo(x, y);
            this.context.lineTo(x1, y1);
            this.context.stroke();
            this.context.restore();
        }
    };

    LineChart.label = {
        text: function (style, text, x, y, type, i) {
            if (i % this.options.label[type].step === 0) {
                var label = style.label[type], fillText;
                if (type === "x")
                    y = y + (style.tick.major + label.offsetY) * this.resolution;
                if (type === "y")
                    x = x - (style.tick.major + label.offsetX) * this.resolution, y += label.offsetY * this.resolution;

                this.context.font = label.fontStyle + " " + (label.fontSize * this.resolution) + "px " + label.font;
                this.context.fillStyle = label.color;
                this.context.textAlign = label.align;
                this.context.textBaseline = label.baseline;

                var substr = /(\-?\d+(\.\d)?)/.exec(text) || [];
                if(type === "x") {
                    if(typeof this.options.onLabel === 'function') {
                        fillText = this.options.onLabel(substr[0]);
                    } else {
                        fillText = substr[0];
                    }
                } else {
                    if(typeof this.options.onLabelY === 'function') {
                        fillText = this.options.onLabelY(substr[0]);
                    } else {
                        fillText = substr[0];
                    }
                }
                this.context.fillText(fillText, x, y);
                //this.context.fillText(substr[0], x, y);
            }
        }

    };

    LineChart.title = {
        text: function (style, text, x, y, type) {
            this.context.save();

            if (type === "x") {
                y += style.title.x.offsetY,
                        x += style.title.x.offsetX;
            } else if (type === "y") {
                y += style.title.y.offsetY,
                        x += style.title.y.offsetX;
            }
            this.context.font = style.title.fontStyle + " " + (style.title.fontSize * this.resolution) + "px " + style.title.font;
            this.context.fillStyle = style.title.color;

            this.context.translate(x, y);
            if (type === "y")
                this.context.rotate(Math.PI / 2);

            this.context.fillText(text, 0, 0);
            this.context.restore();
        }
    };

    return LineChart;

});
