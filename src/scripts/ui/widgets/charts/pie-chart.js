/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */

/**
 * SVG Pie Chart Modul
 * 
 * Erstellt einen SVG Pie Chart basierend auf den übergebenen Daten.
 * 
 * @param {Object} Utils        Das Utils Modul
 * @param {Object} Classes      Das Classes Modul
 * @param {Object} AcmsEvent    Das Event Handling Modul
 * 
 * @returns {PieChart}
 */
define("ui/widgets/charts/pie-chart", [
    'tools/utils',
    'core/classes',
    'events/event'
], function(Utils, Classes, AcmsEvent) {
    
    "use strict";
    
    var PIECHART_MODULE = "acms.ui.widgets.charts.pie-chart.js",
        NS = "." + PIECHART_MODULE,
        SVG_NS = "http://www.w3.org/2000/svg";
    
    /**
     * Constructor
     * 
     * @param {Element} element Das SVG-Element als Wrapper um den Chart
     * @param {Object} options  Eigene Optionen
     * 
     * @returns {PieChart}
     */
    var PieChart = function(element, options) {
        this.element =
        this.options =
        this.$tooltip = null;
        this.total = 0;
        this.totalPercent = 0;
        this.duration = 0;
        try {
        this.initialize(element, options);
        } catch(E) {
            console.log(E)
            console.trace()
        }
    };
    
    /**
     * Modul Name
     * 
     * @var {String}
     */
    PieChart.MODULE = "SVG Pie-Chart";
    
    /**
     * Modul Version
     * 
     * @var {String}
     */
    PieChart.VERSION = "1.5.0";
    
    /**
     * Standard-Optionen
     * 
     * <ul>
     *  <li>
     *      staticTotal {Boolean}<br>
     *      TRUE, wenn sich der Gesamt-Wert nicht ändert. In diesem Fall wird 
     *      keine Gesamtzahl neu berechnet und der Wert total bleibt als 
     *      Gesamt-Wert.
     *  </li>
     *  <li>
     *      total {Number}<br>
     *      Gesamt-Wert der values oder 0
     *  </li>
     *  <li>
     *      animated {Boolean}<br>
     *      Sollen die Pfade animiert erstellt werden?
     *  </li>
     *  <li>
     *      duration {Number}<br>
     *      Die Animations-Ablaufzeit in ms.
     *  </li>
     *  <li>
     *      size {Number}<br>
     *      Die Größe des Chart in px. Ansonsten wird die Dimension des Elements 
     *      genommen, mit dem der Chart erstellt wird. In diesem fall bitte mit 
     *      festen Größen arbeiten und die dimensionen des Elements per CSS 
     *      regeln
     *  </li>
     *  <li>
     *      tooltip {Boolean}<br>
     *      Sollen bei klick auf die Pfade ein Tooltip angezeigt werden?
     *  </li>
     *  <li>
     *      tooltipClass {String}<br>
     *      Optionale CSS Klasse für den Tooltip
     *  </li>
     *  <li>
     *      background {Boolean|String}<br>
     *      Optionale Hintergrund-Farbe für den Chart
     *  </li>
     *  <li>
     *      isPie {Boolean}<br>
     *      TRUE wenn es ein ausgefüllter Pie-Chart ist, FALSE wenn es ein Donut
     *      Chart ist
     *  </li>
     *  <li>
     *      colors {Array}<br>
     *      Standard-Farben für die Pfade. Gibt es mehr Pfade als Farben 
     *      angegeben sind, wird eine zufällige Farbe für den Pfad generiert
     *  </li>
     *  <li>
     *      definition {Array}<br>
     *      Objekt-Array oder ein entsprechender String, der mit JSON.parse 
     *      verarbeitet werden kann ([{name: 'name1', label: 'Schöner Name', value: 2},{...},{...}])
     *  </li>
     *  
     * </ul>
     */
    PieChart.DEFAULT_OPTIONS = {
        staticTotal: false,
        total: 0,
        animated: true,
        duration: 10,
        size: null,
        tooltip: true,
        tooltipClass: '',
        background: false,
        middleCircleColor: false,
        ringProportion: 0,
        addPiePathsAtLast: false,
        drawAtStart: true,
        isPie: true,
        colors: [
            '#00608A',
            '#11669D',
            '#FF8F2C',
            '#C42F16',
            '#B9295F'
        ],
        definition: []
    };
    /**
     * Modul Loader Anweisung
     * 
     * @var {Boolean}
     */
    PieChart.needInit = true;
    
    /**
     * Modul Loader initalisierung
     * 
     * Kann beispielsweise vom Modul-Loader verwendet werden um einen Chart
     * zu erstellen.
     * 
     * @param {Element} element Das SVG-Element als Wrapper um den Chart
     * @param {Object} options  Eigene Optionen
     * 
     * @returns {PieChart}
     */
    PieChart.init = function(element, options) {
        var pc = new PieChart(element, options);
        pc.element.acmsData(PIECHART_MODULE, pc);
        
        return pc;
    };
    
    /**
     * PieChart CSS Datei
     */
    PieChart.css = "media/css/widgets/charts/svg-pie-chart.min.css";
    
    /**
     * Klassen Definitionen
     */
    PieChart.prototype = {
        
        constructor: PieChart,
        
        /**
         * Interner Constructor
         * 
         * @param {Element} element Das SVG-Element als Wrapper um den Chart
         * @param {Object} options  Eigene Optionen
         * 
         * 
         */
        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this._createChart();
            this._createBackgroundRing();
            var addPiePathsAtLast = !(this.options.tooltips) 
                                        ? false 
                                        : this.options.addPiePathsAtLast;
            if (addPiePathsAtLast) {
                this._createEmptyBackgroundRing();
                this._addPaths();
            } else {
                this._addPaths();
                this._createEmptyBackgroundRing();
            }
            if (this.options.tooltip) {
                this._createTooltipContainer();
            }
            element.appendChild(this.$chart);
            this._executeEvent('onAfterRender');
            if (this.options.drawAtStart) {
                this._drawPaths();
            }
        },
        
        /**
         * Aktualisiert den Chart Graph mit den neuen Daten
         * 
         * @param {Object} data
         * 
         * 
         */
        update: function(data) {
            var self = this;
            this._loopOverSectors(function (sector) {
                sector.value = parseInt(data[sector.name]);
                sector.$path.removeAttribute('d');
            });
            self._drawPaths();
        },
        
        /**
         * Setzt einen neuen Gesamt-Wert für die Anzahl an Gesamt-Daten
         * 
         * @param {Integer} newTotal    Neuer Gesamt-Wert
         * @returns {PieChart.prototype}
         */
        setTotal: function (newTotal) {
            this.total = Number(newTotal);
            
            return this;
        },
        /**
         * Gibt die Standard-Optionen zurück
         * 
         * @returns {Object|PieChart.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function() {
            return PieChart.DEFAULT_OPTIONS;
        },
        /**
         * Bildet die Optionen für den PieChart
         * 
         * @param {Object}  options Optionale eigene Optionen aus dem Constructor
         * 
         * @returns {@this;@call;_doExtend|Object}
         */
        buildOptions: function(options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },
        /**
         * Versteckt den Tooltip
         * 
         * 
         */
        hideTooltip: function() {
            this._setHiddenTooltipStyles();
        },
        
        
        _prepare: function() {
            var o = this.options, element = this.element;
            this.staticTotal = (o.staticTotal === true || o.staticTotal === "true" || parseInt(o.staticTotal) === 1) ? true : false;
            this.total = this.staticTotal ? parseInt(o.total) || 0 : 0;
            this.totalPercent = 0;
            this.duration = o.duration || 10;
            this.size = o.size || element.offsetWidth;
            this.centerOfTheCircle = this.size / 2;
            this.ringProportion = o.ringProportion || 0.35;
            var definition = o.definition;
            if(typeof(definition) === 'string') {
                try {
                    definition = JSON.parse(definition);
                } catch(E) {
                    console.log(E);
                    definition = [];
                }
                this.options.definition = definition;
            }
        },
        
        _draw: function() {
            var self = this;
            var startAngle = -(Math.PI) / 2;
            self._loopOverSectors(function (sector) {
                if (self._shouldDrawSection(sector)) {
                    startAngle += self._updatePath(sector.$path, startAngle, sector.percent);
                }
            });
        },
        
        _drawPaths: function() {
            var self = this,
                o = this.options;
            this._calculateTotal();
            this._calculatePercents();
            this._executeEvent('onUpdateStart');
            if (o.animated === true) {
                this._drawAnimated();
            } else {
                self._draw();
                this._executeEvent('onUpdateEnd');
            }
        },
        
        /**
         * Zeichnet mit Animierung aktiviert
         * 
         * 
         */
        _drawAnimated: function() {
            var o = this.options,
                startAngle = - Math.PI / 2,
                endAngle = 0,
                sectors = o.definition,
                length = sectors.length,
                counter = 0;
            this._applyAnimation(startAngle, length, counter);
        },
        
        _createChart: function() {
            this.$chart = this._createSvgElement("svg:svg");
            this.$chart.setAttribute("width", this.size);
            this.$chart.setAttribute("height", this.size);
            this.$chart.setAttribute("viewBox", "0 0 " + this.size + " " + this.size);
        },

        _createBackgroundRing: function() {
            var o = this.options,
                $background = this._createCircle(this.centerOfTheCircle, this.centerOfTheCircle);
            if (o.background && o.background.isColor()) {
                $background.setAttributeNS(null, "style", "fill: " + o.background);
            } else {
                $background.setAttributeNS(null, "class", "circle-chart-background");
            }
            this.$chart.appendChild($background);
        },

        _createEmptyBackgroundRing: function() {
            var o = this.options;
            if (o.isPie) {
                var $emptyCircle = this._createCircle(this.centerOfTheCircle, this.size * this.ringProportion);
                if (o.middleCircleColor && o.middleCircleColor.isColor()) {
                    $emptyCircle.setAttributeNS(null, "style", "fill: " + o.middleCircleColor);
                } else {
                    $emptyCircle.setAttributeNS(null, "class", "circle-chart-background-empty");
                }
                this.$chart.appendChild($emptyCircle);
            }
        },

        _createCircle: function(size, radius) {
            var circle = this._createSvgElement("circle");
            circle.setAttributeNS(null, "cx", size);
            circle.setAttributeNS(null, "cy", size);
            circle.setAttributeNS(null, "r", radius);
            return circle;
        },
        
        _applyAnimation: function(startAngle, length, counter) {
            var self = this, o = this.options;
            var sector = o.definition[counter], 
                timeout,
                currentValue = 0,
                newStartAngle = 0,
                timeoutFn = function () {
                    if (self._shouldDrawSection(sector)) {
                        currentValue += self._getSectorNextAnimationValue(sector, currentValue);
                        newStartAngle = self._updatePath(sector.$path, startAngle, currentValue);
                    }
                    if (currentValue < sector.percent) {
                        timeout = setTimeout(timeoutFn, self.duration);
                    } else if (counter < length - 1) {
                        counter += 1;
                        newStartAngle += startAngle;
                        currentValue = 0;
                        self._applyAnimation(newStartAngle, length, counter);
                    } else {
                        self._executeEvent('onUpdateEnd');
                    }
                };
            timeout = setTimeout(timeoutFn, self.duration);
        },
        
        _createTooltipContainer: function() {
            var self = this,
                o = this.options,
                $tooltip = document.createElement("div");
            if (o.tooltipClass) {
                Classes.addClass($tooltip, o.tooltipClass);
            }
            this.$tooltip = $tooltip;
            this._setHiddenTooltipStyles();
            this.element.appendChild($tooltip);
            var cb = function(event) {
                return self._onOutOffContainerClick(event);
            };
            AcmsEvent.add(document.body, 'click', cb);
        },
        
        _getPageScrollPosition: function() {
            var doc = document.documentElement;
            return {
                left: (window.pageXOffset || doc.scrollLeft) - (doc.clientLeft || 0),
                top: (window.pageYOffset || doc.scrollTop) - (doc.clientTop || 0)
            };
        },
        
        _onOutOffContainerClick: function(event) {
            var $target = event.target;
            if (this.element.contains($target) === false) {
                this.hideTooltip();
            }
        },
        
        _setHiddenTooltipStyles: function() {
            this.$tooltip.style.cssText = 'position: fixed; visibility: hidden; ' +
                    'opacity: 0; z-index: -1; color: #fff; ' +
                    'padding: 10px; ' +
                    'background-color: rgba(0,0,0,0.6); ' +
                    'transition: opacity .6s ease;';
        },
        
        _loopOverSectors: function(cb) {
            var self = this, 
                o = this.options,
                definition = o.definition,
                length = definition.length, 
                i;
            for (i = 0; i < length; i++) {
                cb.call(self, definition[i], i);
            }
        },
        
        _calculateTotal: function() {
            if(this.staticTotal) {
                return;
            }
            var self = this;
            this.total = 0;
            this._loopOverSectors(function (sector) {
                sector.value = Number(sector.value || 0);
                self.total += sector.value;
            });
        },
        
        /**
         * 
         * @param {Integer} index
         * 
         * @returns {Boolean}
         */
        _isLastSector: function(index) {
            return index + 1 === this.options.definition.length;
        },

        _calculatePercents: function() {
            this.totalPercent = 0;
            if (this.total === 0) {
                return;
            }
            var self = this;
            this._loopOverSectors(function (sector, index) {
                sector.percent = Math.round(100 * parseInt(sector.value) / self.total);
                self.totalPercent += sector.percent;
                if (self._isLastSector(index) && self.staticTotal === false) {
                    sector.percent += 100 - self.totalPercent;
                }
            });
        },
        
        _executeEvent: function(name, originalEvent) {
            var self = this, o = this.options;
            var fn = o[name];
            if (typeof fn === 'function') {
                fn.call(self, {
                    $chart: self.$chart,
                    chart: self,
                    total: self.total,
                    event: originalEvent,
                    $tooltip: self.$tooltip
                });
            }
        },
        
        _shouldDrawSection: function(section) {
            return section.value > 0 && section.percent > 0;
        },
        
        _getSectorNextAnimationValue: function(sector, currentValue) {
            var o = this.options,
                speed = o.speed || 1,
                percent = sector.percent,
                step = speed;
            if (currentValue + speed > percent) {
                step = percent - currentValue;
            }
            if (currentValue + step === 100) {
                step -= 0.0001;
            }
            return step;
        },
        
        _updatePath: function($path, startAngle, value) {
            var sectorAngle = (value / 100) * (Math.PI * 2),
                endRadius = startAngle + sectorAngle,
                centerOfTheCircle = this.centerOfTheCircle,
                largeArc = ((endRadius - startAngle) % (Math.PI * 2)) > Math.PI ? 1 : 0,
                startX = centerOfTheCircle + Math.cos(startAngle) * centerOfTheCircle,
                startY = centerOfTheCircle + Math.sin(startAngle) * centerOfTheCircle,
                endX = centerOfTheCircle + Math.cos(endRadius) * centerOfTheCircle,
                endY = centerOfTheCircle + Math.sin(endRadius) * centerOfTheCircle,
                d = [
                    'M', startX, startY,
                    'A', centerOfTheCircle, centerOfTheCircle, 0, largeArc, 1, endX, endY,
                    'L', centerOfTheCircle, centerOfTheCircle,
                    'Z'
                ];
            $path.setAttribute("d", d.join(' '));
            return sectorAngle;
        },

        _addPaths: function() {
            var self = this;
            var cb = function(sector, index) {
                return self._addPath(sector, index);
            };
            this._loopOverSectors(cb);
        },

        _addPath: function(sector, index) {
            var self = this, o = this.options;
            var $path = this._createSvgElement('path');
            if (sector.cls) {
                $path.setAttributeNS(null, "class", sector.cls);
            }
            var color = (sector.color && sector.color.isColor())
                            ? sector.color
                            : false;
            if(!color) {
                color = (typeof o.colors[index] !== "undefined")
                            ? o.colors[index]
                            : Utils.getRandomColor();
            }
            if (color || o.tooltip) {

                $path.setAttributeNS(null, "style",
                        (color ? ('fill: ' + color) + ';' : '') + ' ' +
                        (o.tooltip ? 'cursor: pointer;' : '')
                        );
            }
            self.$chart.appendChild($path);
            $path.acmsData("sector", sector);
            if (o.tooltip) {
                var cb = function(event) {
                    return self._onPathClick(event);
                };
                AcmsEvent.add($path, 'click', cb);
            }
            sector.$path = $path;
        },
        
        /**
         * Erstellt ein neues SVG-Element mit dem Typen
         * 
         * @param {String} type Element Typ
         * @returns {Element}
         */
        _createSvgElement: function(type) {
            return document.createElementNS(SVG_NS, type);
        },
        
        /**
         * Entfernen des Pie-Attributes vom Pfad
         * @param {Element} $path   Der Pfad
         * 
         */
        _removePathPie: function($path) {
            $path.removeAttribute('d');
        },
        
        /**
         * On Path Click Event
         * 
         * Event wird ausgeführt, wenn auf einen SVG Pfad geklickt wird.
         * 
         * @param {MouseEvent} event
         * 
         */
        _onPathClick: function(event) {
            var self = this, 
                o = this.options,
                sector = event.target.acmsData("sector"),
                scrollPosition;
            if (o.tooltip) {
                scrollPosition = self._getPageScrollPosition();
                self.$tooltip.textContent = sector.label + ' : ' + sector.value;
                self.$tooltip.style.left = (event.x + scrollPosition.left - self.$tooltip.offsetWidth / 2) + 'px';
                self.$tooltip.style.top = (event.y + scrollPosition.top - 20) + 'px';
                if (o.tooltipClass) {
                    Classes.toggleClass(self.$tooltip, 'pi-chart-tooltip-is-visible');
                } else {
                    self.$tooltip.style.visibility = 'visible';
                    self.$tooltip.style.opacity = '1';
                    self.$tooltip.style['z-index'] = 'auto';
                    self.$tooltip.style.transition = 'visibility 0s .6s, z-index 0s .6s, opacity .6s ease;';
                    self.$tooltip.style.position = 'absolute';
                }
            }
            self._executeEvent('onPathClick', event);
        }
    };
    
    return PieChart;
});
