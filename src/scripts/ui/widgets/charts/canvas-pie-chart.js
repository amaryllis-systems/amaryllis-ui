/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */

/**
 * Canvas Pie Chart
 * 
 * Donut und Pie Charts mit Canvas Elementen.
 * 
 * @param {Object} Utils    Utils Modul
 * 
 * @returns {CanvasPieChart}
 * 
 * @todo - einbauen der fehlenden Funktionen aus dem SVG Pie Chart
 *          (Aktualisierung der Daten, Tooltips, Animationen)
 */
define('ui/widgets/charts/canvas-pie-chart', [
    'tools/utils'
], function(Utils) {
    
    "use strict";
    
    var CANVASCHART_MODULE = "acms.ui.widgets.charts.canvas-pie-chart";
    
    /**
     * Constructor
     * 
     * @param {Object}  element Das Canvas Element für den Chart
     * @param {Object}  options Optionen
     * 
     * @returns {CanvasPieChart}
     */
    var CanvasPieChart = function(element, options) {
        this.element = 
        this.options =
        this.canvas =
        this.data =
        this.context = null;
        this.total = 0;
        this.len = 0;
        this.size = 100;
        this.initialize(element, options);
    };
    
    /**
     * Modul Name
     * 
     * @var {String}
     */
    CanvasPieChart.MODULE = "Canvas Pie Chart";
    
    /**
     * Modul Version
     * 
     * @var {String}
     */
    CanvasPieChart.VERSION = "1.5.0";
    
    CanvasPieChart.DEFAULT_OPTIONS = {
        type: 'donut', // 'donut', 'pie',
        size: 100
    };
    
    /**
     * Einbinden des Modul Loaders
     * 
     * @var {Boolean}
     */
    CanvasPieChart.needInit = true;
    
    /**
     * Modul Initialisierung
     * 
     * @param {Element} element HTML Canvas Element in dem der Chart erstellt werden 
     *                          soll
     * @param {Object} options  Optionen
     * 
     * @returns {CanvasPieChart}
     */
    CanvasPieChart.init = function(element, options) {
        var cpc = new CanvasPieChart(element, options);
        cpc.element.acmsData(CANVASCHART_MODULE, cpc);
        return cpc;
    };
    
    CanvasPieChart.css = "media/css/widgets/charts/canvas-pie-chart.min.css";
    
    /**
     * Klassen Definitionen
     */
    CanvasPieChart.prototype = {
        constructor: CanvasPieChart,
        
        /**
         * Interner Constructor
         * 
         * @param {Element} element HTML Canvas Element
         * @param {Object} options  Optionen
         * 
         */
        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this.size = this.options.size || 100;
            this.context = this.element.getContext("2d");
            var definition = this.options.definition;
            if(typeof definition === "string") {
                try {
                    definition = JSON.parse(definition);
                } catch(E) {
                    console.log(E);
                    definitions = [];
                }
            }
            this.definition = definition;
            this.len = definition.length;
            this.draw();
        },
        
        /**
         * Gibt die Standard-Optionen zurück
         * 
         * @returns {CanvasPieChart.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function() {
            return CanvasPieChart.DEFAULT_OPTIONS;
        },
        
        /**
         * Bilden der Optionen
         * 
         * @param {Object} options  Optionale eigene Optionen
         * @returns {@this;@call;_doExtend|Object}
         */
        buildOptions: function(options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },
        
        /**
         * Gibt den Gesamt-Wert zurück
         * 
         * @returns {Number}
         */
        getTotal: function(){
            var self = this, value = self.data;
            var totalValue = 0;
            for (var i = 0; i < self.len; i++) {
               totalValue += ( typeof value[i].value === 'number') ? value[i].value  : 0;
           }
           return totalValue;
       },
       
       draw : function() { 
            var self = this,
                o = this.options,
                startx, starty,
                lastend = 0,
                total = self.getTotal(),
                data =  self.definition,
                canvas = this.element, 
                context = this.context;

            canvas.setAttribute('width', '300');
            canvas.setAttribute('height', '300');
            startx = canvas.width /2;
            starty = canvas.height / 2;
            context.clearRect(0, 0, canvas.width, canvas.height);

            this._drawShadow();
            var accumPt = 0;                
            for (var i = 0; i < self.len; i++) {
                var percent = data[i].value / total;
                if(o.type === 'donut') {
                    drawDoghnut(data[i]);
                } else if(o.type === 'pie') {
                    drawPie(data[i]);
                }

                lastend += Math.PI * 2 * percent;

                context.textAlign = 'center';
                context.textBaseline = 'middle';
                context.fillStyle = 'black';

                var midPt = accumPt + data[i].value / 2;
                var midAngle = Math.PI * 2 * midPt / 100;
                accumPt += data[i].value;
                var x = (self.size*1.5) + self.size * Math.cos(midAngle);
                var y = (self.size*1.5) + self.size * Math.sin(midAngle);
                context.fillText(data[i].label +" "+ (percent*100)+"%", x, y);
            }

            function drawPie(data){
                var percent = data.value / total;
                context.fillStyle = data.color;
                context.beginPath();            
                context.moveTo(self.size*1.5, self.size*1.5);
                context.arc(self.size*1.5, self.size*1.5, self.size, lastend, lastend + (Math.PI * 2 * percent), false);
                context.lineTo(self.size*1.5, self.size*1.5);
                context.fill();
            };

            function drawDoghnut(data){
                var percent = data.value / total;
                context.beginPath();
                context.lineWidth = 30;
                context.strokeStyle =  data.color;
                context.arc(self.size*1.5, self.size*1.5, self.size-15, lastend, lastend + (Math.PI * 2 * percent), false);
                context.stroke();
            };
        },
        
        _drawShadow: function() {
            var self = this,
                context = this.context;
            context.save();
            context.fillStyle = "white";
            context.shadowColor = "#888";
            context.shadowBlur = 15;
            context.shadowOffsetX = 1;
            context.shadowOffsetY = 3;
            context.beginPath();
            context.arc(self.size*1.5, self.size*1.5, self.size, 0, Math.PI * 2, false);
            context.fill();
            context.closePath();
            context.restore();
        }
       
    };
    
    return CanvasPieChart;
});
