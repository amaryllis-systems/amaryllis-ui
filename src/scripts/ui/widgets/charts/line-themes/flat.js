/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @copyright 2013 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright 2014 - 2018 Amaryllis Systems GmbH http://www.amws.eu/
 * @copyright 2017 - 2018 Amaryllis CMS Project http://www.amaryllis-cms.de/
 * @license https://www.amaryllis-cms.de/lizenzen/
 * @license Apache Licence 2.0 https://www.apache.org/licenses/LICENSE-2.0
 *
 * Dual-Licensed with Amaryllis CMS EULA. You may obtain a copy of the
 * EULA at
 *
 *     https://www.amaryllis-cms.de/lizenzen/eula/
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

define('ui/widgets/charts/line-themes/flat', [
    
], function () {

    "use strict";

    var Flat = function (Chart) {
        return {
            margin: 70,
            padding: 20,
            fill: {
                fillToBaseLine: false
            },
            style: {
                y: {
                    line: {
                        fill: "#aadcce"
                    }
                },
                y1: {
                    line: {
                        fill: "#dce1a0"
                    }
                },
                y2: {
                    line: {
                        fill: "#f19482"
                    }
                },
                default: {
                    point: {
                        fill: "#000",
                        radius: 2,
                        width: 0,
                        visible: false
                    },
                    line: {
                        stroke: "#000",
                        width: 2,
                        visible: false,
                        fillToBaseLine: false
                    },
                    axis: {
                        stroke: "#ddd",
                        width: 3,
                        x: {
                            visible: false
                        }
                    },
                    tick: {
                        align: "inside", //"outside", "inside",
                        stroke: "#ddd",
                        width: 2,
                        minor: 5,
                        major: 5,
                        visible: false
                    },
                    label: {
                        x: {
                            font: "Helvetica",
                            fontSize: 10,
                            fontStyle: "normal",
                            color: "#ddd",
                            align: "center",
                            baseline: "bottom",
                            offsetY: 14,
                            offsetX: 3,
                            visible: false
                        },
                        y: {
                            font: "Helvetica",
                            fontSize: 10,
                            fontStyle: "normal",
                            color: "#ddd",
                            align: "center",
                            baseline: "bottom",
                            offsetY: 8,
                            offsetX: 8
                        }
                    },
                    title: {
                        color: "#777",
                        font: "georgia",
                        fontSize: "16",
                        fontStyle: "italic",
                        x: {
                            offsetX: 0,
                            offsetY: 60,
                        },
                        y: {
                            offsetX: -135,
                            offsetY: 10,
                            visible: true
                        }
                    }
                }
            }
        };
    };

    return Flat;
});