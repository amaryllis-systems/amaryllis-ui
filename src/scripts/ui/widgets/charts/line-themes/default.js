/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @copyright 2013 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright 2014 - 2018 Amaryllis Systems GmbH http://www.amws.eu/
 * @copyright 2017 - 2018 Amaryllis CMS Project http://www.amaryllis-cms.de/
 * @license https://www.amaryllis-cms.de/lizenzen/
 * @license Apache Licence 2.0 https://www.apache.org/licenses/LICENSE-2.0
 *
 * Dual-Licensed with Amaryllis CMS EULA. You may obtain a copy of the
 * EULA at
 *
 *     https://www.amaryllis-cms.de/lizenzen/eula/
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

define('ui/widgets/charts/line-themes/default', [
], function () {

    "use strict";

    var DefaultTheme = function (Chart) {
        return {
            width: 640,
            height: 400,
            margin: 70,
            padding: 20,
            render: true,
            fill: {
                index: 0,
                render: Chart.line.fill,
                fillToBaseLine: true
            },
            axis: {
                index: 1,
                render: Chart.axis.line,
                x: {
                    steps: 5,
                    render: Chart.axis.line
                },
                y: {
                    steps: 10,
                    render: Chart.axis.line
                }
            },
            tick: {
                index: 2,
                render: Chart.tick.line
            },
            line: {
                index: 3,
                render: Chart.line.line
            },
            point: {
                index: 4,
                render: Chart.point.circle
            },
            label: {
                index: 5,
                render: Chart.label.text,
                x: {
                    step: 1
                },
                y: {
                    step: 1
                }
            },
            title: {
                index: 6,
                render: Chart.title.text,
                x: "x",
                y: "y"
            },
            style: {
                default: {
                    point: {
                        stroke: "#00608A",
                        fill: "#00608A",
                        radius: 3,
                        width: 2,
                        visible: true
                    },
                    line: {
                        stroke: "#00608A",
                        width: 2,
                        fill: "#0095D7",
                        visible: true
                    },
                    axis: {
                        stroke: "#ddd",
                        width: 3,
                        visible: true,
                        x: {
                            visible: true,
                            fixed: true
                        },
                        y: {
                            visible: true,
                            fixed: true
                        }
                    },
                    tick: {
                        align: "middle", //"outside", "inside",
                        stroke: "#ddd",
                        width: 2,
                        minor: 10,
                        major: 15,
                        visible: true,
                        x: {
                            fixed: true
                        },
                        y: {
                            fixed: true
                        }
                    },
                    label: {
                        x: {
                            font: "Helvetica",
                            fontSize: 14,
                            fontStyle: "normal",
                            color: "#494949",
                            align: "center",
                            baseline: "bottom",
                            offsetY: 8,
                            offsetX: 3,
                            visible: true,
                            fixed: true
                        },
                        y: {
                            font: "Helvetica",
                            fontSize: 10,
                            fontStyle: "normal",
                            color: "#494949",
                            align: "center",
                            baseline: "bottom",
                            offsetY: 8,
                            offsetX: 8,
                            visible: true,
                            fixed: true
                        }
                    },
                    title: {
                        color: "#494949",
                        font: "ubuntu,georgia,serif",
                        fontSize: "16",
                        fontStyle: "italic",
                        visible: true,
                        x: {
                            offsetX: 0,
                            offsetY: 60,
                            visible: true
                        },
                        y: {
                            offsetX: -135,
                            offsetY: 10,
                            visible: true
                        }
                    }
                }
            }
        };
    };

    return DefaultTheme;

});