/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */

/**
 * Countdown Modul
 * 
 * Einfaches Countdown Modul
 * 
 * @param {Object} Utils        Utils Modul
 * @param {Object} Classes      Classes Modul
 * @param {Object} AcmsEvent    Acms Event Handler
 * 
 * @returns {countdown_L18.Countdown}
 */
define('ui/widgets/countdown', [
    'tools/utils',
    "core/classes",
    "events/event"
], function(Utils, Classes, AcmsEvent) {

    "use strict";

    var COUNT_DATA = "acms.ui.widgets.countdown",
        NS = "." + NS,
        EVENT_INIT = "init" + NS,
        EVENT_ALARM = "alarm" + NS,
        EVENT_TICK = "tick" + NS

    /**
     * Constructor
     * 
     * @param {Element} element HTML Wrapper Element
     * @param {type} options    Eigene Optionen
     * 
     * @returns {Countdown}
     */
    var Countdown = function(element, options) {
        this.element =
        this.options =
        this._interval =
        this._interval2 =
        this._onDate = null;
        this.initialize(element, options);
    };
    
    /**
     * Modul Name
     * 
     * @var {String}
     */
    Countdown.MODULE = "Countdown";
    
    /**
     * Modul Version
     * 
     * @var {String}
     */
    Countdown.VERSION = "1.5.0";

    /**
     * Standard Optionen
     * 
     * @var {Object}
     */
    Countdown.DEFAULT_OPTIONS = {
        stop: false,
        days: false,
        hours: false,
        minutes: false,
        seconds: false,
        disabled: 'disabled',
        colors: {
            bg: 'purple darken-3',
            digit: 'white-text',
            divider: 'grey-text text-darken-3',
            label: 'blue-grey-text'
        },
        labels: {
            'days': Acms.Translator._('Tage'),
            'hours': Acms.Translator._('Stunden'),
            'minutes': Acms.Translator._('Minuten'),
            'seconds': Acms.Translator._('Sekunden')
        },
        onTick: function(d, h, m, s){},
        onStop: function(){}
    };
    
    /**
     * Standard CSS Datei
     * 
     * @var {String}
     */
    Countdown.css = "media/css/widgets/countdown.min.css";
    
    /**
     * Einbinden des Modul Loaders
     * 
     * @var {Boolean}
     */
    Countdown.needInit = true;
    
    /**
     * Modul Initialisierung
     * 
     * @param {Element} element HTML Wrapper Element
     * @param {type} options    Optionen
     * 
     * @returns {countdown_L16.Countdown}
     */
    Countdown.init = function(element, options) {
        var cd = new Countdown(element, options);
        cd.element.acmsData(COUNT_DATA, cd);

        return cd;
    };
    
    Countdown.prototype = {

        constructor: Countdown,
        /**
         * Intenrer Constructor
         * 
         * @param {Elemenz} element
         * @param {Object} options
         * 
         */
        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            var o = this.options,
                dm = 24*60*60*1000,
                hm = 60*60*1000,
                mm = 60*1000,
                sm = 1000;

            if (o.stop !== false) {
                this._onDate = new Date(o.stop);
            } else {
                this._onDate = new Date();
            }
            if (o.days !== false) {
                if (typeof this._onDate === 'object') {
                    this._onDate = this._onDate.getTime();
                }
                this._onDate = this._onDate + o.days*dm;
            }

            if (o.hours !== false) {
                if (typeof this._onDate === 'object') {
                    this._onDate = this._onDate.getTime();
                }
                this._onDate = this._onDate + o.hours*hm;
            }

            if (o.minutes !== false) {
                if (typeof this._onDate === 'object') {
                    this._onDate = this._onDate.getTime();
                }
                this._onDate = this._onDate + o.minutes*mm;
            }

            if (o.seconds !== false) {
                if (typeof this._onDate === 'object') {
                    this._onDate = this._onDate.getTime();
                }
                this._onDate = this._onDate + o.seconds*sm;
            }


            this._buildCountdown();
            this.element.querySelector('.digit').textContent = '0';
            this.start();
        },

        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {Countdown.DEFAULT_OPTIONS|Object}
         */
        getDefaultOptions: function() {
            return Countdown.DEFAULT_OPTIONS;
        },

        /**
         * Bildet die neuen Optionen
         *
         * @param {Object} options
         * 
         */
        buildOptions: function(options) {
            return Utils.extend(
                    {},
                    this.getDefaultOptions(),
                    options,
                    this.element.acmsData());
        },

        /**
         * Setter/Getter für die Optionen
         *
         * @param {String|NULL} a   Der Optionen-Schlüssel oder NULL
         * @param {Mixed}       b   Undefined oder der Wert für a
         * @returns {Countdown.options|Boolean|Mixed|Countdown.prototype}
         */
        option: function (a, b) {
            if (!a) {
                return this.options;
            }
            if (typeof b === "undefined") {
                return this.options[a] !== undefined ? this.options[a] : false;
            }
            this.options[a] = b;
            return this;
        },

        /**
         * Erstellt das Countdown Markup
         *
         * 
         */
        _buildCountdown: function() {
            var o = this.options, self = this;
            var parts = ['days', 'hours', 'minutes', 'seconds'];
            var p, d;
            parts.map(function(v) {
                p = document.createElement('div');
                p.textContent = ':';
                Classes.addClass(p, 'part' + v);
                p.setAttribute('data-day-text', o.labels[v]);
                self.element.appendChild(p);
                var dig1 = document.createElement('div');
                var dig2 = document.createElement('div');
                Classes.addClass(dig1, 'digit');
                Classes.addClass(dig2, 'digit');
                p.appendChild(dig1);
                p.appendChild(dig2);
                if (o.colors.label && o.colors.label.isColor()) {
                    p.style.color = o.colors.label;
                } else {
                    Classes.addClass(p, o.colors.label);
                }
                
                if (o.colors.bg && o.colors.bg.isColor()) {
                    dig1.style.background = o.colors.bg;
                    dig2.style.background = o.colors.bg;
                } else {
                    Classes.addClass(dig1, o.colors.bg);
                    Classes.addClass(dig2, o.colors.bg);
                }
                if (o.colorDigit.isColor()) {
                    dig1.style.color = o.colorDigit;
                    dig2.style.color = o.colorDigit;
                } else {
                    Classes.addClass(dig1, o.colorDigit);
                    Classes.addClass(dig2, o.colorDigit);
                }

                if (v !== 'seconds') {
                    d =document.createElement('div');
                    Classes.addClass(d, 'divider');
                    d.textContent = ':';
                    self.element.appendChild(d);
                    if (o.colorDivider && o.colorDivider.isColor()) {
                        d.style.color = o.colorDivider;
                    } else {
                        Classes.addClass(d, o.colorDivider);
                        d.addClass(o.colorDivider);
                    }
                }
            });
            var ev = AcmsEvent.create(EVENT_INIT);
            AcmsEvent.dispatch(this.element, ev);
        },
        
        /**
         * Wechseln der "tick" CSS Klasse
         * 
         * 
         */
        toggleTick: function() {
            Classes.toggleClass(this.element, 'tick');
        },
        
        /**
         * Starten des Countdown
         * 
         * 
         */
        start: function(){
            var self = this;
            this._interval2 = setInterval(function(){
                self.toggleTick();
            }, 500);

            this._interval = setInterval(function(){
                self._tickInterval();
            }, 1000);
        },
        
        /**
         * Stoppen des Countdown
         * 
         * 
         */
        stop: function(){
            clearInterval(this._interval);
            clearInterval(this._interval2);
        },
        
        /**
         * Tick-Intervall
         * 
         * 
         */
        _tickInterval: function() {
            var self = this,
                o = this.options,
                el = this.element;
            var days = 24*60*60,
                hours = 60*60,
                minutes = 60,
                left, d, h, m, s;

            left = Math.floor((self._onDate - (new Date())) / 1000);
            if (left < 0) {left = 0;}

            d = Math.floor(left / days);
            left -= d*days;
            self._update('days', d);

            if (d === 0) {
                var dayEl = el.querySelector('.part.days');
                Classes.addClass(dayEl, o.disabled);
            }

            h = Math.floor(left / hours);
            left -= h*hours;
            self._update('hours', h);

            if (d === 0 && h === 0) {
                var hourEl = el.querySelector('.part.hour');
                Classes.addClass(hourEl, o.disabled);
            }

            m = Math.floor(left / minutes);
            left -= m*minutes;
            self._update('minutes', m);

            if (d === 0 && h === 0 && m === 0) {
                var minEl = el.querySelector('.part.minutes');
                Classes.addClass(minEl, o.disabled);
            }

            self._update('seconds', left);

            if (typeof o.onTick === 'function') {
                o.onTick(d, h, m, s);
            }

            if (d === 0 && h === 0 && m === 0 && s === 0) {
                var part = el.querySelector('.part');
                Classes.addClass(part, o.disabled);
                if (typeof o.onStop === 'function') {
                    o.onStop();
                }
                self.stop('all');
                var ev = AcmsEvent.create(EVENT_ALARM);
                AcmsEvent.dispatch(this.element, ev);
                clearInterval(self._interval);
            }
        },
        
        /**
         * Aktualisiert einen Part des Countdown
         * 
         * @param {String} part     Der zu aktualisierende Part
         * @param {Number} value    Der neue Wert
         * 
         * 
         */
        _update: function(part, value) {
            var el = this.element;
            var hauptWert = Math.floor(value/10)%10;
            var tiefWert = value%10;
            var hauptZahl, tiefZahl;

            hauptZahl = el.querySelector("."+part+" .digit:eq(0)");
            tiefZahl = el.querySelector("."+part+" .digit:eq(1)");
            if (tiefWert !== parseInt(tiefZahl.textContent)) {
                Classes.toggleClass(tiefZahl, 'in');
                setTimeout(function(){
                    tiefZahl.textContent = tiefWert;
                    Classes.toggleClass(tiefZahl, 'in');
                }, 500);
            }
            if (hauptWert !== parseInt(hauptZahl.textContent)) {
                Classes.toggleClass(hauptZahl, 'in');
                setTimeout(function() {
                    hauptZahl.textContent = hauptWert;
                    Classes.toggleClass(hauptZahl, 'in');
                }, 500);
            }
        }
    };

    return Countdown;

});
