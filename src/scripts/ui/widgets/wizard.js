/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */

/**
 * Wizard
 * 
 * Basis-Wizard Widget zur einfachen Benutzung und Erweiterung.
 * 
 * @param {Object} Utils        Utils Modul
 * @param {Object} Classes      Classes Modul
 * @param {Object} Selectors    Selectors Modul
 * @param {Object} AcmsEvent    AcmsEvent Modul
 * @param {Object} ShowHide     ShowHide Modul
 * @param {Object} Fade         Fade Modul
 * 
 * @returns {Wizard}
 */
define("ui/widgets/wizard", [
    "tools/utils",
    "core/classes",
    "core/selectors",
    "events/event",
    "apps/ui/helpers/show-hide",
    "apps/ui/helpers/fade"
], function (Utils, Classes, Selectors, AcmsEvent, ShowHide, Fade) {

    "use strict";

    var WIZARD_DATA = "acms.ui.widgets.wizard",
        NS = "." + WIZARD_DATA,
        EVENT_CLICK_HELP = "click-help" + NS,
        EVENT_CLICK_NEXT = "click-next" + NS,
        EVENT_CLICK_PREV = "click-prev" + NS,
        EVENT_CLICK_FINISH = "click-finish" + NS;

    /**
     * Wizard
     * 
     * Formular-Wizard Komponente zur Steuerung von Formular-Schritten mit optionaler Validierung per Schritt,
     *
     * @param {Object} element
     * @param {Object} options
     *
     * @returns {Wizard}
     * 
     * @exports ui/widgets/wizard
     */
    var Wizard = function (element, options) {
        this.options =
        this.$steps =
        this.current =
        this._width =
        this._height =
        this.$actionBar =
        this.length = null;
        this.initialize(element, options);
    };

    /**
     * Komponenten Name
     *
     * @var {String}
     */
    Wizard.MODULE = "Wizard";

    /**
     * Komponenten Version
     *
     * @var {String}
     */
    Wizard.VERSION = "1.5.0";

    /**
     * Standard-Optionen
     *
     * @var {Object}
     */
    Wizard.DEFAULT_OPTIONS = {
        start: 0,
        finish: 'default',
        allowBack: true,
        allowTabclick: true,
        hasHelp: false,

        active: 'active',
        step: '.step',
        steplist: '.step-list > .step-link',
        actionBar: '.actions',
        onPrevious: function (page, index, wiz) {
            return true;
        },
        onNext: function (page, index, wiz) {
            return true;
        },
        onFinish: function (page, index, wiz) {
        },
        onHelp: function (page, index, wiz) {
        },
        onPage: function (page, index, wiz) {
        }
    };
    /**
     * Module Loader
     */
    /** Einleiten der Initiierung **/
    Wizard.needInit = true;
    
    
    //Wizard.css = "media/css/widgets/wizard.min.css";
    
    /**
     * Initiierung durch den Module Loader
     *
     * @param {Object} element
     * @param {Object} options
     *
     * @returns {Wizard}
     */
    Wizard.init = function (element, options) {
        var wiz = new Wizard(element, options);
        wiz.element.acmsData(WIZARD_DATA, wiz);

        return wiz;
    };

    Wizard.prototype = {

        constructor: Wizard,
        /**
         * Interner Constructor
         *
         * @param {Object} element
         * @param {Object} options
         *
         * 
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._height = 0;
            this._width = 0;
            this._load();
            this.updateHeight();
            this.updateWidth();
            this.$actionBar = element.querySelector('.actions');
            this._adjustActionBar();
            this.listen();
            this.step(this.options.start);
        },

        /**
         * Standard-Option
         *
         * Gibt dir Standard-Optionen zurück
         *
         * @returns {Object|Wizard.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function() {
            return Wizard.DEFAULT_OPTIONS;
        },

        /**
         * Bildet die Optionen
         *
         * <p>
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         * </p>
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function (options) {
            options = typeof options === "object" ? options : {};
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },

        /**
         * Setter/Getter für die Optionen
         *
         * @param {String|Null} a   Der Optionen-Schlüssel oder NULL
         * @param {Mixed}       b   Undefined oder der Wert für a
         * @returns {Wizard.options|Boolean|Mixed|Wizard.prototype}
         */
        option: function (a, b) {
            if (!a) {
                return this.options;
            }
            if (typeof b === "undefined") {
                return this.options[a] !== undefined ? this.options[a] : false;
            }
            this.options[a] = b;
            return this;
        },

        refresh: function() {
            this._load();
            this.updateHeight();
            this.updateWidth();
            this._adjustActionBar();
        },

        step: function (index) {
            var o = this.options, self = this, element = this.element;
            var steps = this.$steps,
                steplist = this.steplist,
                len = steps.length, i, j, k;
            
            for(i = 0; i < len; i++) {
                Classes.removeClass(steps[i], o.active+',previous,next');
                if(steplist && steplist[i]) {
                    Classes.removeClass(steplist[i], o.active);
                }
            }
            if(!steps[index]) {
                console.log('invalider step ' + index);
                return;
            }
            for(j = 0; j < index; j++) {
                ShowHide.hide(steps[j]);
                Classes.addClass(steps[j], 'previous');
                steps[j].setAttribute('aria-hidden', true);
            }
            for(k = index + 1; k > index, k < len; k++) {
                ShowHide.hide(steps[k]);
                Classes.addClass(steps[k], 'next');
                steps[j].setAttribute('aria-hidden', true);
            }
            Classes.addClass(steps[index], o.active);
            ShowHide.show(steps[index]);
            Fade.fadeIn(steps[index], 1000);
            steps[index].setAttribute('aria-hidden', false);
            steps[index].style.width = this._width + 'px';
            if(steplist && steplist[index]) {
                Classes.addClass(steplist[index], o.active);
            }
            this._step = steps[index];
            this._adjustActionBar();
            this._adjustButtons(index);
            if (typeof o.onPage === 'function') {
                o.onPage(steps[index], self.current, self);
            }
        },
        prev: function () {
            var self = this, o = this.options;
            var newStep = this.current -1;
            if (newStep < 0) {
                return false;
            }
            var res = true;
            if (typeof o.onPrev === 'function') {
                res = o.onPrev(self._step, self.current, self);
            }
            if(false === res) {
                return false;
            }
            this.current = newStep;
            this.step(newStep);
            return true;
        },
        next: function () {
            var self = this, o = this.options;
            var newStep = this.current + 1;
            if (newStep > this.length) {
                return false;
            }
            var res = true;
            if (typeof o.onNext === 'function') {
                res = o.onNext(self._step, self.current, self);
            }
            if(false === res) {
                return false;
            }
            this.current = newStep;
            this.step(newStep);
            return true;
        },

        updateWidth: function() {
            var length = this.length;
            var div = document.createElement('div');
            // div.width;
            this._width = this.element.width - ((length - 1) * 24 + (length));
        },

        updateHeight: function() {
            var self = this, o = this.options, steps = this.$steps, i, step;
            for(i = 0; i < this.length; i++) {
                step = steps[i];
                var outer = step.height;
                if (outer > self._height) {
                    self._height = outer;
                }
                if (step.hasClass(o.active)) {
                    self.current = i;
                }
            }
            
            for(i = 0; i < this.length; i++) {
                step = steps[i];
                step.style.height = this._height + 78;
            }
            
        },

        listen: function() {
            var self = this, o = this.options, element = this.element;
            var btnFinish = Selectors.qa('.button.finish', element),
                btnNext = Selectors.qa('.button.next', element),
                btnPrev = Selectors.qa('.button.previous', element),
                i;
            var eventFuncFinish = function(event) {
                event.preventDefault();
                var e = AcmsEvent.createCustom(EVENT_CLICK_FINISH);
                AcmsEvent.dispatch(element, e);
                if (typeof o.onFinish === 'function') {
                    o.onFinish(self._step, self.current, self);
                }
            };
            var eventFuncNext = function(event) {
                event.preventDefault();
                var e = AcmsEvent.createCustom(EVENT_CLICK_NEXT);
                AcmsEvent.dispatch(element, e);
                self.next();
            };
            var eventFuncPrev = function(event) {
                event.preventDefault();
                var e = AcmsEvent.createCustom(EVENT_CLICK_PREV);
                AcmsEvent.dispatch(element, e);
                self.prev();
            };
            for(i=0; i<btnFinish.length; i++) {
                AcmsEvent.add(btnFinish[i], 'click', eventFuncFinish);
            }
            for(i=0; i<btnNext.length; i++) {
                AcmsEvent.add(btnNext[i], 'click', eventFuncNext);
            }
            for(i=0; i<btnPrev.length; i++) {
                AcmsEvent.add(btnPrev[i], 'click', eventFuncPrev);
            }
            
            var helpBtn = element.querySelector('.button.help');
            var helpFunc = function(event) {
                event.preventDefault();
                var e = AcmsEvent.createCustom(EVENT_CLICK_HELP);
                AcmsEvent.dispatch(element, e);
                self._onHelpClick();
            };
            if(o.hasHelp && helpBtn) {
                AcmsEvent.on(element, 'click', '.help', helpFunc);
            }
            var resizeWindow = function() {
                self.updateHeight();
                self.updateWidth();
                self._adjustActionBar();
            };
            AcmsEvent.add(window, 'resize', resizeWindow);
            
        },
        
        _onHelpClick: function() {
            var o = this.options, self = this;
            if (typeof o.onHelp === 'function') {
                o.onHelp(self._step, self.current, self);
            }
        },

        _adjustActionBar: function () {
            var o = this.options, element = this.element;
            var current = this._step;
            var rect = current.getBoundingClientRect();
            var left = rect.left, right = current.width();
            this.$actionBar.style.left = left;
            this.$actionBar.style.width = '100%';
        },
        
        _adjustButtons: function(index) {
            var element = this.element, steps = this.$steps, o = this.options;
            var btnPrev = element.querySelector('.button.previous');
            var btnNext = element.querySelector('.button.next');
            var btnFinish = element.querySelector('.button.finish');
            if(btnPrev) {
                if (index === 0) {
                    btnPrev.setAttribute('disabled', true);
                } else {
                    btnPrev.removeAttribute('disabled');
                }
            }
            if (index === steps.length -1 && btnNext) {
                btnNext.setAttribute('disabled', true);
            } else if (btnNext) {
                btnNext.removeAttribute('disabled');
            }
            if (index !== parseInt(o.finish)) {
                btnFinish.setAttribute('disabled', true);
            } else {
                btnFinish.removeAttribute('disabled');
            }
        },

        _load: function() {
            var o = this.options, element = this.element;
            var $steps = Selectors.qa(o.step, element);
            var $steplist = Selectors.qa(o.steplist, element);
            var length = $steps.length - 1;
            var Step = element.querySelector(o.step + '.' + this.option('active'));
            this.steplist = $steplist;
            this.$steps = $steps;
            this.length = length;
            this.current = 0;
            if(!Step) {
                Step = $steps[0];
                Classes.addClass($steps[0], 'active');
                Step.setAttribute('aria-hidden', false);
                if(this.steplist && this.steplist[0]) {
                    this.steplist[0].setAttribute('aria-hidden', false);
                    Classes.addClass(this.steplist[0], 'active');
                }
            } else {
                
            }
            if(o.finish === 'default') {
                this.options.finish = length;
            }
            
            this._step = Step;
            
            var btnPrev = element.querySelector('.button.previous');
            var btnNext = element.querySelector('.button.next');
            var btnFinish = element.querySelector('.button.finish');
            var btnBar = element.querySelector('.button-bar ');
            if(!btnPrev) {
                var btn = document.createElement('button');
                btn.innerHTML = 'zurück';
                
            }
        }
    };

    return Wizard;
});
