/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */


define('ui/widgets/recommend', [
    'tools/utils',
    'events/event',
    'core/window'
], function(Utils, AcmsEvent, Win) {
    
    "use strict";
    
    
    var Module = function(element, options) {
        this.$element =
        this.element = 
        this.options = null;
        this.initialize(element, options);
    }
    
    Module.MODULE = "Empfehlungs-Toggle";
    
    Module.VERSION = "1.5.0";
    
    Module.NS = "acms.ui.widgets.recommend";
    
    Module.needInit = true;
    
    Module.DEFAULT_OPTIONS = {
        remote: null,
        id: null,
        reload: false
    };
    
    Module.init = function(element, options) {
        var Mod = new Module(element, options);
        Mod.element.acmsData(Module.NS, Mod);
    }
    
    Module.prototype = {
        constructor: Module,
        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this.recommending = false;
            this.listen();
        },
        
        getDefaultOptions: function() {
            return Module.DEFAULT_OPTIONS;
        },
        
        buildOptions: function(options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },
        
        listen: function() {
            var self = this, o = this.options;
            AcmsEvent.add(self.element, 'click', function(e) {
                e.preventDefault();
                if(self.recommending === true) {
                    return false;
                }
                self.recommending = true;
                var remote = o.remote, data = {op: 'recommend', id: o.id};
                require(['http/request', 'notifier'], function(Request) {
                    var done = function(response) {
                        Acms.Notifications.createFromResponse(response);
                        if(response.status === "success") {
                            if(o.reload) {
                                Win.refresh(500);
                            } else {
                                self.element.parentNode.removeChild(self.element);
                            }
                        }
                    }, fail = function(jqXHR, textStatus) {
                        Acms.Logger.writeLog(textStatus)
                    }, always = function() {self.recommending = false;};
                    
                    Request.post(remote, data, done, fail, always);
                });
                return false;
            });
            
        }
    };
    
    return Module;
});