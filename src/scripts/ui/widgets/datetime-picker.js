/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */


define("ui/widgets/datetime-picker", [
    'moment',
    'tools/object/extend',
    'core/classes',
    'core/selectors',
    'core/style',
    "ui/widgets/datetimepicker/template",
    "ui/widgets/datetimepicker/calendar-generator",
    "ui/widgets/datetimepicker/clock-generator",
    "events/event"
], function (moment, Extend, Classes, Selectors, Style, template, generator, clockGenerator, AcmsEvent) {

    "use strict";

    var hasAcmsTranslator = (typeof Acms !== "undefined" && 
                                typeof Acms.Translator !== "undefined" && 
                                typeof Acms.Translator._ !== "undefined");

    /**
     * Date-Time-Picker
     * 
     * Erstellt einen Date-/Time-/DateTime-Picker auf einem Element. Der Picker kann über die Optionen 
     * konfiguriert werden und über dass SASS Framework im Layout angepasst werden.
     * 
     * @param {HTMLElement}     element  Das Input Element auf dem der Picker erstellt werden soll
     * @param {Object|NULL}     options  Eigene Optionen
     * 
     * @exports ui/tools/datetime-picker
     */
    var DateTimePicker = function (element, options) {
        this.currentView        =
        this.minDate            =
        this.maxDate            =
        this.element            =
        this.isEditingYear      = 
        this.options            = 
        this.calendarGenerator  =
        this.name               = null;
        this.options = DateTimePicker.DEFAULT_OPTIONS;
        this.initialize(element, options);
    };

    /**
     * Modul Name
     *
     * @var {String}
     */
    DateTimePicker.MODULE = "Date-Time-Picker";

    /**
     * Modul Namespace
     *
     * @var {String}
     */
    DateTimePicker.NS = "acms.ui.widgets.datetime-picker";

    DateTimePicker.css = "media/css/ui/dtp";

    /**
     * Standard-Optionen
     * 
     * | Option         | Beschreibung                                              | Typ           |
     * |----------------|-----------------------------------------------------------|---------------|
     * | date           | Datum-Auswahl erstellen?                                  | Boolean       |
     * | time           | Zeit-Auswahl erstellen?                                   | Boolean       |
     * | format         | Das Ausgabe/Speicher Format für die Übermittlung          | String        |
     * | minDate        | mindest-Datum/Zeit                                        | String        |
     * | maxDate        | maximale Zeit/Datum Auswahl                               | String        |
     * | currentDate    | Aktuelles Datum als Date oder moment                      | Date/Moment   |
     * | locale         | Die Sprach-Auswahl als 2-Buchstaben Code (z.B. 'de')      | String        |
     * | weekStart      | Nummer Wochentag des Kalender Start-Tages [^1]            | Integer [0-6] |
     * | cancelButton   | Cancel-Button Text                                        | String        |
     * | okButton       | OK-Button Text                                            | String        |
     * | onRender       | Callback bei der Renderung des Kalenders/Pickers [^2]     | Callable      |
     * 
     * [^1]:    Der erste Tag einer Woche des Kalenders. Typischerweise Mo oder So.
     * 
     * [^2]:    Der Callback wird aufgerufen nachdem das HTML Gerüst erstellt 
     *          wurde. Übergeben werden die Parameter tpl (Das Template Gerüst) 
     *          und self (die Picker Instanz)
     * 
     * @var {Object}
     */
    DateTimePicker.DEFAULT_OPTIONS = {
        date:           true,
        time:           true,
        format:         'YYYY-MM-DD HH:mm:ss',
        minDate:        null,
        maxDate:        null,
        currentDate:    null,
        locale:         'de',
        weekStart:      0,
        shortTime:      false,
        cancelButton:   hasAcmsTranslator ? Acms.Translator._('Abbrechen') : "Abbrechen",
        okButton:       hasAcmsTranslator ? Acms.Translator._('Ok') : "Ok",
        
        css: DateTimePicker.css,
        themecss: false,
        onRender: function(tpl, self) {},
    };

    /**
     * Modul Loader Einbindung
     */
    DateTimePicker.needInit = true;

    /**
     * Initialisierung
     * 
     * @param {Element} element  Das Element
     * @param {Object}  options  Eigene Optionen
     * 
     * @returns {DateTimePicker}
     */
    DateTimePicker.init = function(element, options) {
        
        var dtp = new DateTimePicker(element, options);
        dtp.element.acmsData(DateTimePicker.NS, dtp);
        
        return dtp;
    };
    
    /**
     * On Show Event
     * 
     * Das Event wird ausgeführt, bevor der Datetime Picker gezeigt wird
     * 
     * @type {String}
     */
    DateTimePicker.EVENT_SHOW = DateTimePicker.NS + ".show";
    
    /**
     * On Shown Event
     * 
     * Das Event wird ausgeführt, wenn der Datetime Picker im Browser sichtbar 
     * ist.
     * 
     * @type {String}
     */
    DateTimePicker.EVENT_SHOWN = DateTimePicker.NS + ".shown";
    
    /**
     * On Show Event
     * 
     * Das Event wird ausgeführt, bevor der Datetime Picker versteckt/ausgeblendet wird
     * 
     * @type {String}
     */
    DateTimePicker.EVENT_HIDE = DateTimePicker.NS + ".hide";
    
    /**
     * On Shown Event
     * 
     * Das Event wird ausgeführt, wenn der Datetime Picker im Browser unsichtbar 
     * ist.
     * 
     * @type {String}
     */
    DateTimePicker.EVENT_HIDDEN = DateTimePicker.NS + ".hidden";
    
    /**
     * On Build Template Event
     * 
     * Das Event wird ausgeführt, bevor ein Template gerendert wird
     * 
     * @type {String}
     */
    DateTimePicker.EVENT_BUILD_TEMPLATE = DateTimePicker.NS + ".build-template";
    
    /**
     * On Built Template Event
     * 
     * Das Event wird ausgeführt, nachdem ein Template gerendert wurde
     * 
     * @type {String}
     */
    DateTimePicker.EVENT_BUILT_TEMPLATE = DateTimePicker.NS + ".built-template";
    
    /**
     * On Activate Am Event
     * 
     * Das Event wird ausgeführt, bevor der Datetime Picker auf AM umgestellt 
     * wird.
     * 
     * @type {String}
     */
    DateTimePicker.EVENT_AM_ACITVATE = DateTimePicker.NS + ".activate-am";
    
    /**
     * On Activated AM Event
     * 
     * Das Event wird ausgeführt, nachdem der Datetime Picker auf AM umgestellt 
     * wurde.
     * 
     * @type {String}
     */
    DateTimePicker.EVENT_AM_ACITVATED = DateTimePicker.NS + ".activated-am";
    
    /**
     * On Activate Pm Event
     * 
     * Das Event wird ausgeführt, bevor der Datetime Picker auf PM umgestellt 
     * wird.
     * 
     * @type {String}
     */
    DateTimePicker.EVENT_PM_ACITVATE = DateTimePicker.NS + ".activate-pm";
    
    /**
     * On Activated PM Event
     * 
     * Das Event wird ausgeführt, nachdem der Datetime Picker auf PM umgestellt 
     * wurde.
     * 
     * @type {String}
     */
    DateTimePicker.EVENT_PM_ACITVATED = DateTimePicker.NS + ".activated-pm";
    
    /**
     * On Date Slected Event
     * 
     * Das Event wird ausgeführt, wenn ein Datum ausgewählt wurde.
     * 
     * @type {String}
     */
    DateTimePicker.EVENT_DATE_SELECTED = DateTimePicker.NS + ".date-selected";
    
    /**
     * On Hour Slected Event
     * 
     * Das Event wird ausgeführt, wenn eine Stunde ausgewählt wurde.
     * 
     * @type {String}
     */
    DateTimePicker.EVENT_HOUR_SELECTED = DateTimePicker.NS + ".hour-selected";
    
    /**
     * On Minute Slected Event
     * 
     * Das Event wird ausgeführt, wenn eine Minute ausgewählt wurde.
     * 
     * @type {String}
     */
    DateTimePicker.EVENT_MINUTE_SELECTED = DateTimePicker.NS + ".minute-selected";
    
    
    /**
     * Before Change Date Event
     * 
     * Das Event wird ausgeführt, bevor der Input sich ändert
     * 
     * @type {String}
     */
    DateTimePicker.EVENT_DATE_BEFORE_CHANGE = DateTimePicker.NS + ".before-change";
    
    /**
     * After Change Date Event
     * 
     * Das Event wird ausgeführt, nachdem der Input sich geändert hat.
     * 
     * @type {String}
     */
    DateTimePicker.EVENT_DATE_AFTER_CHANGE = DateTimePicker.NS + ".after-changed";
    
    /**
     * Klassen Definitionen
     */
    DateTimePicker.prototype = {
        
        constructor: DateTimePicker,
        
        /**
         * Interner Constructor
         * 
         * @param {Element} element     Das Element
         * @param {Object}  options     Eigene Optionen
         * 
         * 
         */
        initialize: function (element, options) {
            moment.locale('de');
            //this.minDate;
            //this.maxDate;
            this.element = element;
            this.options = this.buildOptions(options);
            var currentView = this.options.date === false ? 1 : 0;
            this.currentView = currentView;
            this.name = "acms-datetime-picker-" + this.setName();
            this.element.setAttribute("data-dtp", this.name);
            this._buildDayRange();
            this.calendarGenerator = generator.init(this._days);
            this._buildDateInstances();
            this._buildTemplate();
            try {
                this.on();
            } catch(E) {
                console.log(E.message);
                console.log(E.stack);
            }
        },

        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {DateTimePicker.DEFAULT_OPTIONS|Object}
         */
        getDefaultOptions: function() {
            return DateTimePicker.DEFAULT_OPTIONS;
        },

        /**
         * Bildet die neuen Optionen
         *
         * @param {Object} options
         * 
         */
        buildOptions: function(options) {
            var o = Extend.flat(
                    {},
                    true,
                    this.getDefaultOptions(),
                    options,
                    this.element.acmsData());
            if(o.date === false || o.date === 'false' || o.date === 0 || o.date === '0') {
                o.date = false;
            }
            if(o.time === false || o.time === 'false' || o.time === 0 || o.time === '0') {
                o.time = false;
            }
            if(o.shortTime === 'false' || o.shortTime === false || o.shortTime === 0 || o.shortTime === '0') {
                o.shortTime = false;
            }
            o.weekStart = parseInt(o.weekStart, 10);
            return o;
        },

        /**
         * Setter/Getter für die Optionen
         *
         * @param {String|NULL} a   Der Optionen-Schlüssel oder NULL
         * @param {Mixed}       b   Undefined oder der Wert für a
         * @returns {DateTimePicker.options|Boolean|Mixed|DateTimePicker.prototype}
         */
        option: function (a, b) {
            if (!a) {
                return this.options;
            }
            if (typeof b === "undefined") {
                return this.options[a] || false;
            }
            this.options[a] = b;
            return this;
        },

        /**
         * Konvertiert die Stunden-Anzahl bei "PM" Einstellung
         *
         * @param {Integer} h   Die zu konvertierende Stunden-Anzahl
         *
         * @returns {Integer}
         */
        convertHours: function (h) {
            var ret = h;
            if ((h < 12) && this.isPM()) {
                ret += 12;
            }
            return ret;
        },
        setDate: function (date) {
            this.options.currentDate = date;
            this._buildDateInstances();
        },

        setMinDate: function (date) {
            this.options.minDate = date;
            this._buildDateInstances();
        },

        setMaxDate: function (date) {
            this.options.maxDate = date;
            this._buildDateInstances();
        },
        destroy: function () {
            //this.off();
            document.body.removeChild(this.picker);
        },
        /**
         * Setzt den Status des Datetime Pickers auf sichtbar
         *
         * 
         */
        show: function () {
            var e = AcmsEvent.createCustom(DateTimePicker.EVENT_SHOW);
            AcmsEvent.dispatch(this.element, e);
            if(e.defaultPrevented) {
                return false;
            }
            this.toggleHidden(this.picker, false);
            // this.applyCentering();
            var e2 = AcmsEvent.createCustom(DateTimePicker.EVENT_SHOWN);
            AcmsEvent.dispatch(this.element, e2);
        },
        /**
         * Setzt den Status des Datetime Pickers auf versteckt
         *
         * 
         */
        hide: function () {
            var e = AcmsEvent.createCustom(DateTimePicker.EVENT_HIDE);
            AcmsEvent.dispatch(this.element, e);
            if(e.defaultPrevented) {
                return false;
            }
            this.toggleHidden(this.picker, true);
            var e2 = AcmsEvent.createCustom(DateTimePicker.EVENT_HIDDEN);
            AcmsEvent.dispatch(this.element, e2);
        },
        resetDate: function () {
        },
        /**
         * Zentriert den Picker in der Mitte des Fensters
         *
         * 
         * 
         * @deprecated To be removed soon
         */
        applyCentering: function () {
            return;
            
        },
        
        _dimension: function(elm) {
            var elmHeight, elmMargin;
            if(document.all) {// IE
                elmHeight = elm.currentStyle.height;
                elmMargin = parseInt(elm.currentStyle.marginTop, 10) + parseInt(elm.currentStyle.marginBottom, 10) + "px";
            } else {// Mozilla
                elmHeight = document.defaultView.getComputedStyle(elm, '').getPropertyValue('height');
                elmMargin = parseInt(document.defaultView.getComputedStyle(elm, '').getPropertyValue('margin-top')) + parseInt(document.defaultView.getComputedStyle(elm, '').getPropertyValue('margin-bottom')) + "px";
            }
            return (elmHeight+elmMargin);
        },

        /**
         * Aufsetzen der Event Listener
         * 
         * 
         */
        on: function() {
            var self = this, picker = this.picker;
            var cancel = picker.querySelector('.picker-button-cancel');
            AcmsEvent.add(cancel, 'click', function(e) {
                e.preventDefault();
                return self._onClickCancel(e);
            });
            
            var ok = picker.querySelector('.picker-button-ok');
            AcmsEvent.add(ok, 'click', function(e) {
                e.preventDefault();
                return self._onClickOk(e);
            });
            var prevMonth = picker.querySelector('a.picker-prev-month');
            AcmsEvent.add(prevMonth, 'click', function(e) {
                e.preventDefault();
                return self._onClickPrevMonth(e);
            });
            var nextMonth = picker.querySelector('a.picker-next-month');
            AcmsEvent.add(nextMonth, 'click', function(e) {
                e.preventDefault();
                return self._onClickNextMonth(e);
            });
            var prevYear = picker.querySelector('a.picker-prev-year');
            AcmsEvent.add(prevYear, 'click', function(e) {
                e.preventDefault();
                return self._onClickPrevYear(e);
            });
            var nextYear = picker.querySelector('a.picker-next-year');
            AcmsEvent.add(nextYear, 'click', function(e) {
                e.preventDefault();
                return self._onClickNextYear(e);
            });
            var prevYearFast = picker.querySelector('a.picker-prev-year-fast');
            AcmsEvent.add(prevYearFast, 'click', function(e) {
                e.preventDefault();
                return self._onClickPrevYearFast(e);
            });
            var nextYearFast = picker.querySelector('a.picker-next-year-fast');
            AcmsEvent.add(nextYearFast, 'click', function(e) {
                e.preventDefault();
                return self._onClickNextYearFast(e);
            });
            var actualYear = picker.querySelector('.actual-year');
            AcmsEvent.add(actualYear, 'dblclick', self._onEditYear.bind(this));
            
            /**AcmsEvent.add(window, 'resize', function(e) {
                self.applyCentering(e);
            });**/
            var pickerContent = picker.querySelector('.picker-content');
            AcmsEvent.add(pickerContent, 'click', function(e) {
                e.preventDefault();
                e.stopPropagation();
            });
            AcmsEvent.add(picker, 'click', function(e) {
                e.preventDefault();
                e.stopPropagation();
                return self.hide();
            });
            var close = picker.querySelector('.picker-close > a');
            AcmsEvent.add(close, 'click', function(e) {
                e.preventDefault();
                e.stopPropagation();
                return self.hide();
            });
            AcmsEvent.add(this.element, 'click', function(e) {
                return self._onClick(e);
            });
        },
        /**
         * Entfernen der Event Listener
         * 
         * 
         */
        off: function() {
            this.picker.querySelector('.picker-button-cancel').off('click');
            this.picker.querySelector('.picker-button-ok').off('click');
            this.picker.querySelector('a.picker-prev-month').off('click');
            this.picker.querySelector('a.picker-next-month').off('click');
            this.picker.querySelector('a.picker-prev-year').off('click');
            this.picker.querySelector('a.picker-next-year').off('click');
            this.picker.querySelector('.picker-content').off('click');
            this.picker.off('click');
            this.picker.querySelector('.picker-close > a').off('click');
            this.picker.querySelector('.picker-close > a').off('click');
        },

        /**
         * Fügt die Event-Listener für die AM/PM Auswahl zu
         *
         * 
         */
        initMeridienButtons: function () {
            var self = this, picker = self.picker;
            var Am = this.picker
                .querySelector('a.meridien-am');
            AcmsEvent.add(Am, 'click', function(e) {
                    self._onSelectAM(e);
                });
            var Pm = picker.querySelector('a.meridien-pm');
            AcmsEvent.add(Pm, 'click', function(e) {
                    self._onSelectPM(e);
                });
        },

        /**
         * Prüft ob ein Datum größer als das gesetzte Mindes-Datum ist
         * 
         * @param {String|Date} date        Datum Angabe
         * @param {Boolean} checkHour       Sollen Stunden geprüft werden?
         * @param {Boolean} checkMinute     Sollen Minuten geprüft werden?
         *
         * @returns {Boolean}
         */
        isBiggerThanMinDate: function (date, checkHour, checkMinute) {
            var ret = true;

            if (typeof (this.minDate) !== 'undefined' && 
                    this.minDate !== null) {
                var minD = moment(this.minDate);
                var datum = moment(date);
                if (!checkHour && !checkMinute) {
                    minD.hour(0);
                    minD.minute(0);
                    datum.hour(0);
                    datum.minute(0);
                }
                minD.second(0);
                datum.second(0);
                minD.millisecond(0);
                datum.millisecond(0);
                if (!checkMinute) {
                    datum.minute(0);
                    minD.minute(0);
                    ret = (parseInt(datum.format("X")) >= parseInt(minD.format("X")));
                } else {
                    ret = (parseInt(datum.format("X")) >= parseInt(minD.format("X")));
                }
            }

            return ret;
        },
        
        /**
         * Prüft ob ein Datum kleiner als das gesetzte Maximal-Datum ist
         * 
         * @param {String|Date} date        Datum Angabe
         * @param {Boolean} checkHour       Sollen Stunden geprüft werden?
         * @param {Boolean} checkMinute     Sollen Minuten geprüft werden?
         *
         * @returns {Boolean}
         */
        isSmallerThanMaxDate: function (date, checkTime, checkMinute) {
            var ret = true;
            if (typeof (this.maxDate) !== 'undefined' && this.maxDate !== null) {
                var datumMax = moment(this.maxDate);
                var datum = moment(date);
                if (!checkTime && !checkMinute) {
                    datumMax.hour(0);
                    datumMax.minute(0);
                    datum.hour(0);
                    datum.minute(0);
                }
                datumMax.second(0);
                datum.second(0);
                datumMax.millisecond(0);
                datum.millisecond(0);
                if (!checkMinute) {
                    datum.minute(0);
                    datumMax.minute(0);
                    ret = (parseInt(datum.format("X")) <= parseInt(datumMax.format("X")));
                } else {
                    ret = (parseInt(datum.format("X")) <= parseInt(datumMax.format("X")));
                }
            }

            return ret;
        },
        
        showDate: function (date) {
            if (date) {
                var picker = this.picker, o = this.options;
                picker.querySelector('.actual-day').innerHTML = date.locale(o.locale).format('dddd');
                picker.querySelector('.actual-month').innerHTML = date.locale(o.locale).format('MMM').toUpperCase();
                picker.querySelector('.actual-number').innerHTML = date.locale(o.locale).format('DD');
                picker.querySelector('.actual-year').innerHTML = date.locale(o.locale).format('YYYY');
            }
        },
        
        /**
         * Zeigt das Time Panel
         * 
         * @param {String|Date} date    Datum
         * 
         * 
         */
        showTime: function (date) {
            if (date) {
                var o = this.options, picker = this.picker;
                var minutes = (5 * Math.round(date.minute() / 5)),
                    content = ((o.shortTime) ? 
                                date.format('hh') :
                                date.format('HH')) + ':' + (
                                    (minutes.toString().length == 2) ? 
                                        (minutes < 60 ? minutes : 0) : '0' + minutes);
                if (o.date) {
                    picker.querySelector('.actual-time').innerHTML = content;
                } else {
                    if (o.shortTime) {
                        picker.querySelector('.actual-day').innerHTML = date.format('A');
                    } else {
                        picker.querySelector('.actual-day').innerHTML = ' ';
                    }
                    picker.querySelector('.actual-maxtime').innerHTML = content;
                }
            }
        },
        selectDate: function (date) {
            if (date) {
                this.currentDate.date(date);
                this.showDate(this.currentDate);
                var opts = {date: this.currentDate};
                var e = AcmsEvent.createCustom(DateTimePicker.EVENT_DATE_SELECTED, opts);
                AcmsEvent.dispatch(this.element, e);
            }
        },
        
        /**
         * Setzen eines Unique Names für den Picker
         * 
         * @return {String} Unique Name
         */
        setName: function () {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            for (var i = 0; i < 5; i++) {
                text += possible.charAt(Math.floor(Math.random() * possible.length));
            }
            return text;
        },

        /**
         * Ist der Time Picker auf PM eingestellt?
         * 
         * @returns {Boolean}
         */
        isPM: function () {
            var self = this, pm = self.picker.querySelector('a.meridien-pm');
            return Classes.hasClass(pm, 'selected');
        },

        /**
         * Setzen des Wertes für den Original Input
         */
        setElementValue: function () {
            var self = this, element = self.element;
            var opts = {date: self.currentDate}, o = self.options;
            var e = AcmsEvent.createCustom(DateTimePicker.EVENT_DATE_BEFORE_CHANGE, opts);
            AcmsEvent.dispatch(self.element, e);
            if(e.defaultPrevented) {
                return false;
            }
            var newVal = moment(self.currentDate)
                            .locale(o.locale)
                            .format(o.format);
            
            this.element.value = newVal;
            opts.value = this.element.value;
            var e2 = AcmsEvent.createCustom(DateTimePicker.EVENT_DATE_AFTER_CHANGE, opts);
            AcmsEvent.dispatch(element, e2);
            AcmsEvent.fireChange(element);
        },

        /**
         * Toggle Buttons
         */
        toggleButtons: function (date) {
            var self = this,
                o = self.options,
                picker = self.picker,
                prevMonth = picker.querySelector('a.picker-prev-month'),
                nextMonth = picker.querySelector('a.picker-next-month'),
                prevYear = picker.querySelector('a.picker-prev-year'),
                nextYear = picker.querySelector('a.picker-next-year'),
                prevYearFast = picker.querySelector('a.picker-prev-year-fast'),
                nextYearFast = picker.querySelector('a.picker-next-year-fast');
            if (date && date.isValid()) {
                var startOfMonth = moment(date).locale(o.locale).startOf('month');
                var endOfMonth = moment(date).locale(o.locale).endOf('month');
                if (!self.isBiggerThanMinDate(startOfMonth, false, false)) {
                    Classes.addClass(prevMonth, 'invisible');
                } else {
                    Classes.removeClass(prevMonth, 'invisible');
                }

                if (!self.isSmallerThanMaxDate(endOfMonth, false, false)) {
                    Classes.addClass(nextMonth, 'invisible');
                } else {
                    Classes.removeClass(nextMonth, 'invisible');
                }
                var startOfYear = moment(date).locale(o.locale).startOf('year');
                var endOfYear = moment(date).locale(o.locale).endOf('year');
                if (!self.isBiggerThanMinDate(startOfYear, false, false)) {
                    Classes.addClass(prevYear, 'invisible');
                    Classes.addClass(prevYearFast, 'invisible');
                } else {
                    Classes.removeClass(prevYear, 'invisible');
                    Classes.removeClass(prevYearFast, 'invisible');
                }
                if (!self.isSmallerThanMaxDate(endOfYear, false, false)) {
                    Classes.addClass(nextYear, 'invisible');
                    Classes.addClass(nextYearFast, 'invisible');
                } else {
                    Classes.removeClass(nextYear, 'invisible');
                    Classes.removeClass(nextYearFast, 'invisible');
                }
            }
        },

        /**
         * Toggle des Time pickers
         */
        toggleTime: function (isHours) {
            var self = this,
                datum = moment(self.currentDate), i, sel;
            if (isHours) {
                var hourSel = Selectors.qa('a.picker-select-hour', self.picker), hour;
                for(i = 0; i < hourSel.length; i++) {
                    sel = hourSel[i];
                    hour = sel.acmsData('hour');
                    datum
                        .hour(self.convertHours(hour))
                        .minute(0)
                        .second(0);
                    if (self.isBiggerThanMinDate(datum, true, false) === false || 
                            self.isSmallerThanMaxDate(datum, true, false) === false) {
                        sel.disabled = true;
                        Classes.addClass(sel, "disabled");
                        sel.setAttribute('disabled', 'disabled');
                    } else {
                        sel.disabled = false;
                        Classes.removeClass(sel, 'disabled');
                        sel.removeAttribute('disabled');
                    }
                    AcmsEvent.add(sel, 'click', self._onSelectHour.bind(this));
                }
            } else {
                var minuteSel = Selectors.qa('a.picker-select-minute', self.picker), minute;
                
                for(i = 0; i < minuteSel.length; i++) {
                    sel = minuteSel[i],
                    minute = sel.acmsData('minute');
                    datum.minute(minute).second(0);
                    if (self.isBiggerThanMinDate(datum, true, true) === false || 
                            self.isSmallerThanMaxDate(datum, true, true) === false) {
                        sel.disabled = true;
                        Classes.addClass(sel, "disabled");
                        sel.setAttribute('aria-disabled', true);
                    } else {
                        sel.setAttribute('aria-disabled', false);
                        Classes.removeClass(sel, "disabled");
                        sel.disabled = false;
                    }
                    AcmsEvent.add(sel, 'click', self._onSelectMinute.bind(this));
                }
            }
        },

        /**
         * Wechseln des selected-Status eines Elements
         * 
         * @param  {HTMLElement}    ele     Das Element für das der Status geändert werden soll
         * @param  {Boolean}        state   TRUE wenn Selected, FALSE andernfalls
         */
        toggleSelected: function(ele, state) {
            if(true === state) {
                Classes.addClass(ele, 'selected');
                ele.setAttribute('aria-selected', 'true');
            } else {
                Classes.removeClass(ele, 'selected');
                ele.setAttribute('aria-selected', 'false');
            }
        },

        
        /**
         * Setzen des Hidden Status
         * 
         * @param  {HTMLElement}    ele     Das Element
         * @param  {Boolean}        state   TRUE wenn Hidden
         */
        toggleHidden: function(ele, state) {
            if(true === state) {
                Classes.addClass(ele, 'hidden');
                ele.setAttribute('aria-hidden', 'true');
            } else {
                Classes.removeClass(ele, 'hidden');
                ele.setAttribute('aria-hidden', 'false');
            }
        },
        
        /**
         * OnEditYear Event Handler
         * 
         * @access private
         */
        _onEditYear: function(e) {
            if(e) e.preventDefault();
            if(this.isEditingYear === true) {
                return;
            }
            var self = this,
                picker = self.picker,
                year = picker.querySelector('.actual-year'),
                div = document.createElement('div'),
                input = document.createElement('input');
            input.setAttribute('type', 'text');
            input.type = 'text';
            input.setAttribute('value', year.innerHTML);
            input.value = year.innerHTML;
            input.style.width = 24*4 + 'px';
            input.name = this.name + '_year';
            year.innerHTML = "";
            year.appendChild(input);
            var actualYear = picker.querySelector('.actual-year');
            AcmsEvent.add(input, 'blur', self._onUnEditYear.bind(this));
            AcmsEvent.add(input, 'keydown', function(e) {
                // Allow: backspace, delete, tab, escape, enter and .
                var mainCodes = [46, 8, 9, 27,10, 13, 110, 190];
                if(mainCodes.indexOf(e.keyCode) === -1) {
                    if (
                         // Allow: Ctrl+A, Command+A
                        (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || 
                         // Allow: home, end, left, right, down, up
                        (e.keyCode >= 35 && e.keyCode <= 40)) {
                             return;
                    }
                }
                if(e.keyCode === 13 || e.keyCode === 10) {
                    e.preventDefault();
                    return self._onUnEditYear();
                }
                if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                    e.preventDefault();
                }
            });
            this.isEditingYear = true;
        },
        
        /**
         * Event Handler nachdem das Jahr bearbeitet wurde
         * 
         * @param {Event} e Das Event
         * 
         * @returns {Boolean}
         * 
         * @access private
         */
        _onUnEditYear: function(e) {
            e.preventDefault();
            e.stopPropagation();
            var self = this,
                picker = self.picker,
                year = picker.querySelector('.actual-year'),
                input = year.querySelector('input'),
                value = parseInt(input.value);
            year.removeChild(input);
            if('NaN' === value || value < 0 || value > 9999) {
                var oldYear = this.currentDate.get('year');
                this.isEditingYear = false;
                year.removeChild(input);
                year.innerHTML = oldYear;
                return false;
            }
            
            year.innerHTML = value;
            this.currentDate.year(value);
            this._buildDateInstance(this.currentDate);
            this.isEditingYear = false;
            return false;
        },
        

        /**
         * On Click Event Handler
         * 
         * @param {Event} e Das Event
         * 
         * @returns {Boolean}
         * 
         * @access private
         */
        _onClick: function () {
            var self = this,
                picker = self.picker,
                o = self.options, ele;
            self.currentView = 0;
            self.element.blur();
            self._buildDateInstances();
            self.show();
            if (o.date) {
                ele = picker.querySelector('.picker-date');
                self.toggleHidden(ele, false);
                self._buildDateInstance();
            } else if (o.time) {
                ele = picker.querySelector('.picker-time');
                self.toggleHidden(ele, false);
                clockGenerator.generateHours(self);
            }
        },
        

        /**
         * On Click Ok-Button Event Handler
         * 
         * @param {Event} e Das Event
         * 
         * @returns {Boolean}
         * 
         * @access private
         */
        _onClickOk: function () {
            switch (this.currentView) {
                case 0:
                    if (this.options.time === true || this.options.time === "true" || this.options.time === '1') {
                        clockGenerator.generateHours(this);
                    } else {
                        this.setElementValue();
                        this.hide();
                    }
                    break;
                case 1:
                    clockGenerator.generateMinutes(this);
                    break;
                case 2:
                    this.setElementValue();
                    this.hide();
                    break;
            }
        },


        /**
         * On Click Cancel Button Event Handler
         * 
         * @param {Event} e Das Event
         * 
         * @returns {Boolean}
         * 
         * @access private
         */
        _onClickCancel: function () {
            if (this.options.time) {
                switch (this.currentView) {
                    case 0:
                        this.hide();
                        break;
                    case 1:
                        if (this.options.date) {
                            this._buildDateInstance();
                        } else {
                            this.hide();
                        }
                        break;
                    case 2:
                        clockGenerator.generateHours(this);
                        break;
                }
            } else {
                this.hide();
            }
        },

        /**
         * On Click Prev Month Event Handler
         * 
         * @param {Event} e Das Event
         * 
         * @returns {Boolean}
         * 
         * @access private
         */
        _onClickPrevMonth: function (e) {
            if(e) e.preventDefault();
            this.currentDate.subtract(1, 'months');
            this._buildDateInstance(this.currentDate);
        },

        /**
         * On Click Next Month Event Handler
         * 
         * @param {Event} e Das Event
         * 
         * @returns {Boolean}
         * 
         * @access private
         */
        _onClickNextMonth: function (e) {
            if(e) e.preventDefault();
            this.currentDate.add(1, 'months');
            this._buildDateInstance(this.currentDate);
        },

        /**
         * On Click Prev Year Event Handler
         * 
         * @param {Event} e Das Event
         * 
         * @returns {Boolean}
         * 
         * @access private
         */
        _onClickPrevYear: function (e) {
            if(e) e.preventDefault();
            this.currentDate.subtract(1, 'years');
            this._buildDateInstance(this.currentDate);
        },
        
        /**
         * On Click Next Year Event Handler
         * 
         * @param {Event} e Das Event
         * 
         * @returns {Boolean}
         * 
         * @access private
         */
        _onClickNextYear: function (e) {
            if(e) e.preventDefault();
            this.currentDate.add(1, 'years');
            this._buildDateInstance(this.currentDate);
        },
        

        /**
         * On Click Prev Year Fast Event Handler
         * 
         * @param {Event} e Das Event
         * 
         * @returns {Boolean}
         * 
         * @access private
         */
        _onClickPrevYearFast: function (e) {
            if(e) e.preventDefault();
            this.currentDate.subtract(10, 'years');
            this._buildDateInstance(this.currentDate);
        },

        
        /**
         * On Click Next Year Fast Event Handler
         * 
         * @param {Event} e Das Event
         * 
         * @returns {Boolean}
         * 
         * @access private
         */
        _onClickNextYearFast: function (e) {
            if(e) e.preventDefault();
            this.currentDate.add(10, 'years');
            this._buildDateInstance(this.currentDate);
        },

        /**
         * On-Select-Date Event
         *
         * @param {Event} e Das Event
         *
         * 
         * 
         * @access private
         */
        _onSelectDate: function (e) {
            if(e) e.preventDefault();
            var act = this.picker.querySelector('a.picker-select-day.selected');
            this.toggleSelected(act, false);
            var curr = e.target;
            this.toggleSelected(curr, true);
            this.selectDate(curr.parentNode.acmsData("date"));
        },

        /**
         * On-Select-Hour Event
         * 
         * @param {Event} e Das Event
         *
         * 
         * 
         * @access private
         */
        _onSelectHour: function (e) {
            var target = e.target,
                self = this,
                picker = self.picker,
                element = self.element;
            if(target.tagName.toLowerCase() !== 'a') {
                target = Selectors.closest(target, 'a');
            }
            if(target.getAttribute('disabled')) {
                return;
            }
            var ev = AcmsEvent.createCustom(DateTimePicker.EVENT_HOUR_SELECTED, {relatedTarget: picker, eTarget: e.currentTarget});
            AcmsEvent.dispatch(element, ev);
            if(ev.defaultPrevented) {
                return;
            }
            var act = picker.querySelector('a.picker-select-hour.selected');
            if(act) self.toggleSelected(act, false);
            self.toggleSelected(target, true);
            var dataHour = parseInt(target.acmsData('hour'));
            if (self.isPM())
                dataHour += 12;
            self.currentDate.hour(dataHour);
            self.showTime(self.currentDate);
            var handHour = picker.querySelector('.picker-hand-hour'),
                handMinute = picker.querySelector('.picker-hand-minute');
            clockGenerator.animateHands(self, handHour, handMinute);
        },

        /**
         * On-Select-Minute Event
         *
         * @param {Event} e Das Event
         *
         * 
         * 
         * @access private
         */
        _onSelectMinute: function (e) {
            var target = e.target,
                self = this,
                picker = self.picker,
                element = self.element;
            if(target.tagName.toLowerCase() !== 'a') {
                target = Selectors.closest(target, 'a');
            }
            if(target.getAttribute('disabled')) {
                return;
            }
            var ev = AcmsEvent.createCustom(DateTimePicker.EVENT_MINUTE_SELECTED, {relatedTarget: picker,eTarget: e.currentTarget});
            AcmsEvent.dispatch(element, ev);
            if(ev.defaultPrevented) {
                return;
            }
            var act = picker.querySelector('a.picker-select-minute.selected');
            if(act) self.toggleSelected(act, false);
            self.toggleSelected(target, true);
            self.currentDate.minute(parseInt(target.acmsData('minute')));
            self.showTime(self.currentDate);
            var handHour = picker.querySelector('.picker-hand-hour'),
                handMinute = picker.querySelector('.picker-hand-minute');
            clockGenerator.animateHands(self, handHour, handMinute);
            
        },

        /**
         * On-Select-AM Event
         *
         * @param {Event} e Das Event
         *
         * 
         * 
         * @access private
         */
        _onSelectAM: function (e) {
            var self = this, 
                picker = self.picker, 
                opt = {relatedTarget: picker},
                target = e.target;
            var ev = AcmsEvent.createCustom(DateTimePicker.EVENT_AM_ACITVATE, opt);
            AcmsEvent.dispatch(self.element, ev);
            if(ev.defaultPrevented) {
                return;
            }
            var act = picker.querySelector('.actual-meridien .selected');
            if(act) this.toggleSelected(act, false);
            this.toggleSelected(target, true);
            if (this.currentDate.hour() >= 12) {
                if (this.currentDate.subtract(12, 'hours')) {
                    this.showTime(this.currentDate);
                }
            }
            this.toggleTime((this.currentView === 1));
            var e2 = AcmsEvent.createCustom(DateTimePicker.EVENT_AM_ACITVATED);
            AcmsEvent.dispatch(self.element, e2);
        },
        
        /**
         * On Select PM
         * 
         * @param {Event} e     Das Event
         * 
         * 
         * 
         * @access private
         */
        _onSelectPM: function (e) {
            var self = this,
                target = e.target,
                picker = self.picker,
                opt = {relatedTarget: picker};
            var ev = AcmsEvent.createCustom(DateTimePicker.EVENT_PM_ACITVATE, opt);
            AcmsEvent.dispatch(self.element, ev);
            if(ev.defaultPrevented) {
                return;
            }
            var act = picker.querySelector('.actual-meridien .selected');
            if(act) this.toggleSelected(act, false);
            this.toggleSelected(target, true);
            if (this.currentDate.hour() < 12) {
                if (this.currentDate.add(12, 'hours')) {
                    this.showTime(this.currentDate);
                }
            }
            this.toggleTime((this.currentView === 1));
            var e2 = AcmsEvent.createCustom(DateTimePicker.EVENT_PM_ACITVATED);
            AcmsEvent.dispatch(self.element, e2);
        },
        
        /**
         * Bilden des Template
         * 
         * 
         * 
         * @access private
         */
        _buildTemplate: function () {
            var self = this, 
                o = self.options,
                element = self.element,
                opt = {pickername: self.name, options: o},
                e;
            e = AcmsEvent.createCustom(DateTimePicker.EVENT_BUILD_TEMPLATE, opt);
            AcmsEvent.dispatch(element, e);
            if(e.defaultPrevented) {
                return;
            }
            this.template = template.render(
                                this.name,
                                o.cancelButton,
                                o.okButton);
            o.onRender(this.template, self);
            if (! document.body.querySelector("#" + this.name)) {
                var div = document.createElement('div');
                div.innerHTML = this.template;
                while(div.firstChild) {
                    document.body.appendChild(div.firstChild);
                }
                this.picker = document.querySelector("#" + this.name);
            }

            opt.picker = this.picker;
            var e2 = AcmsEvent.createCustom(DateTimePicker.EVENT_BUILT_TEMPLATE, opt);
            AcmsEvent.dispatch(this.element, e2);
        },

        /**
         * @access private
         */
        _buildDateInstances: function () {
            var self = this,
                o = self.options,
                element = self.element;
                
            if (element.value.length > 0) {
                if (typeof (o.format) !== 'undefined' && o.format !== null) {
                    self.currentDate = moment(element.value, o.format).locale(o.locale);
                } else {
                    self.currentDate = moment(element.value).locale(o.locale);
                }
            } else {
                if (typeof (element.getAttribute('value')) !== 'undefined' && 
                            element.getAttribute('value') !== null && 
                            element.getAttribute('value') !== "") {
                    if (typeof (element.getAttribute('value')) === 'string') {
                        if (typeof (o.format) !== 'undefined' && o.format !== null) {
                            self.currentDate = moment(element.getAttribute('value'), o.format).locale(o.locale);
                        } else {
                            self.currentDate = moment(element.getAttribute('value')).locale(o.locale);
                        }
                    }
                } else {
                    if (typeof (o.currentDate) !== 'undefined' && o.currentDate !== null) {
                        if (typeof (o.currentDate) === 'string') {
                            if (typeof (o.format) !== 'undefined' && o.format !== null) {
                                self.currentDate = moment(o.currentDate, o.format).locale(o.locale);
                            } else {
                                self.currentDate = moment(o.currentDate).locale(o.locale);
                            }
                        } else {
                            if (typeof (o.currentDate.isValid) === 'undefined' || 
                                    typeof (o.currentDate.isValid) !== 'function') {
                                var x = o.currentDate.getTime();
                                self.currentDate = moment(x, "x").locale(o.locale);
                            } else {
                                self.currentDate = o.currentDate;
                            }
                        }
                        element.value = self.currentDate.format(o.format);
                        element.setAttribute('value', element.value);
                    } else {
                        self.currentDate = moment();
                    }
                }
            }

            if (typeof (o.minDate) !== 'undefined' && o.minDate !== null) {
                if (typeof (o.minDate) === 'string') {
                    if (typeof (o.format) !== 'undefined' && o.format !== null) {
                        self.minDate = moment(o.minDate, o.format).locale(o.locale);
                    } else {
                        self.minDate = moment(o.minDate).locale(o.locale);
                    }
                } else {
                    if (typeof (o.minDate.isValid) === 'undefined' || 
                            typeof (o.minDate.isValid) !== 'function') {
                        var r = o.minDate.getTime();
                        self.minDate = moment(r, "x").locale(o.locale);
                    } else {
                        self.minDate = o.minDate;
                    }
                }
            }

            if (typeof (o.maxDate) !== 'undefined' && o.maxDate !== null) {
                if (typeof (o.maxDate) === 'string') {
                    if (typeof (o.format) !== 'undefined' && o.format !== null) {
                        self.maxDate = moment(o.maxDate, o.format).locale(o.locale);
                    } else {
                        self.maxDate = moment(o.maxDate).locale(o.locale);
                    }
                } else {
                    if (typeof (o.maxDate.isValid) === 'undefined' || 
                            typeof (o.maxDate.isValid) !== 'function') {
                        var m = o.maxDate.getTime();
                        self.maxDate = moment(m, "x").locale(o.locale);
                    } else {
                        self.maxDate = o.maxDate;
                    }
                }
            }

            if (!self.isBiggerThanMinDate(self.currentDate)) {
                self.currentDate = moment(self.minDate);
            }
            if (!self.isSmallerThanMaxDate(self.currentDate)) {
                self.currentDate = moment(self.maxDate);
            }
        },

        /**
         * @access private
         */
        _buildDateInstance: function (d) {
            this.currentView = 0;
            var ele = this.picker.querySelector('.picker-calendar'), self = this;
            self.toggleHidden(ele, false);
            var ele2 = self.picker.querySelector('.picker-datetime');
            self.toggleHidden(ele2, true);

            var datum = ((typeof (self.currentDate) !== 'undefined' && 
                            self.currentDate !== null) ? self.currentDate : null);
            var cal = self.calendarGenerator.generate(self.currentDate, moment, self);
            ele.innerHTML = "";
            if (typeof (cal.week) !== 'undefined' && 
                    typeof (cal.days) !== 'undefined') {
                var tmp = self.calendarGenerator.generateHtml(datum, cal, self, moment),
                    div = document.createElement('div');
                div.innerHTML = tmp;
                while(div.firstChild) {
                    ele.appendChild(div.firstChild);
                }
                var select = Selectors.qa('a.picker-select-day'), j;
                for(j = 0; j < select.length; j++) {
                    AcmsEvent.add(select[j], 'click', self._onSelectDate.bind(self));
                }
                self.toggleButtons(datum);
            }
            //self.applyCentering();
            self.showDate(datum);
        },

        /**
         * Bilden der Tages-Auswahl einer Woche
         * 
         * 
         * 
         * @access private
         */
        _buildDayRange: function () {
            this._days = [];
            for (var i = this.options.weekStart; this._days.length < 7; i++) {
                if (i > 6) {
                    i = 0;
                }
                this._days.push(i.toString());
            }
        }
    };

    return DateTimePicker;

});
