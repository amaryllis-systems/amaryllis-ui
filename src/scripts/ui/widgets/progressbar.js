/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("ui/widgets/progressbar", [
    "tools/utils",
    "core/selectors",
    "core/classes",
    "events/event",
    "core/acms",
], function (Utils, Selectors, Classes, AcmsEvent, A) {

    "use strict";

    /**
     * Progress Bar
     * 
     * Die Progress-Bar bietet einfache Optionen zur Darstellung einer 
     * Fortschritts-Anzeige.
     * 
     * ``` html
     * <div class="progress-bar" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" data-min=0 data-start=20 data-max=100 data-animate="true" data-module="ui/widgets/progressbar">
     *  <div class="progress-meter">
     *      <span class="progress-status">0</span>
     *  </div>
     * </div>
     * ```
     * 
     * @param {Element} element Das HTML Element
     * @param {Object} options  Die optionalen eigenen Optionen
     * 
     * @exports ui/widgets/progressbar
     */
    var ProgressBar = function (element, options) {
        this.element =
        this.start =
        this.now =
        this.vertical =
        this.interval =
        this.options = null;
        A.Logger.logDeprecatedModule('ui/widgets/progessbar', 'apps/ui/widgets/progress-bar', '4.0');
        this.initialize(element, options);
    };

    /**
     * Component Name
     *
     * @var {String}
     */
    ProgressBar.MODULE = "ProgressBar";
    
    /**
     * Component Namespace
     *
     * @var {String}
     */
    ProgressBar.NS = "acms.ui.widgets.progressbar";
    
    /**
     * Update Event
     * 
     * @var {String}
     */
    ProgressBar.EVENT_UPDATE = "update." + ProgressBar.NS;
    
    /**
     * Finish Event
     * 
     * @var {String}
     */
    ProgressBar.EVENT_FINISH = "finish." + ProgressBar.NS;
    
    /**
     * Reset Event
     * 
     * @var {String}
     */
    ProgressBar.EVENT_RESET = "reset." + ProgressBar.NS;

    /**
     * Default Options
     *
     * @var {Object}
     */
    ProgressBar.DEFAULT_OPTIONS = {
        delay: 10,
        refresh_speed: 50,
        text: 'percent', // percent oder currency
        use_percentage: true,
        formatPercent: function(percent) { return percent + '%'; },
        formatAmount: function(amount_part, amount_max, amount_min) { return amount_part + ' / ' + amount_max; },
        currency: '€',
        update: function(meter, status, bar){},
        finish: function(meter, status, bar){},
        reset: function(meter, status, bar){},
        min: 0,
        max: 100,
        start: 0,
        animate: true
    };

    /**
     * Einbinden des Module Loaders
     * 
     * @var {Boolean}
     */
    ProgressBar.needInit = true;

    /**
     * Modul Initialisierung
     * 
     * @param {Element} element
     * @param {Object} options
     * @returns {ProgressBar}
     */
    ProgressBar.init = function(element, options) {
        var bar = new ProgressBar(element, options);
        bar.element.acmsData(ProgressBar.NS, bar);
        return bar;
    };
    
    /**
     * CSS-Datei
     * 
     * @var {String}
     */
    ProgressBar.css = "media/css/widgets/progressbar.min.css";

    ProgressBar.prototype = {

        constructor: ProgressBar,

        /**
         * Intenrer Constructor
         * 
         * @param {Element} element
         * @param {Object} options
         * 
         * 
         */
        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
        },

        /**
         * Standard-Option
         *
         * Gibt dir Standard-Optionen zurück
         *
         * @returns {Object|ProgressBar.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function() {
            return ProgressBar.DEFAULT_OPTIONS;
        },

        /**
         * Bildet die Optionen
         *
         * <p>
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         * </p>
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function (options) {
            var o = Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
            o.start = parseInt(o.start);
            o.min = parseInt(o.min);
            o.max = parseInt(o.max);
            o.delay = parseInt(o.delay);
            if(o.animate === 'true' || o.animate === true || parseInt(o.animate) === 1) {
                o.animate = true;
            } else {
                o.animate = false;
            }
            
            return o;
        },

        /**
         * Setter/Getter für die Optionen
         *
         * @param {String|NULL} a   Der Optionen-Schlüssel oder NULL
         * @param {Mixed}       b   Undefined oder der Wert für a
         * @returns {ProgressBar.options|Boolean|Mixed|ProgressBar.prototype}
         */
        option: function (a, b) {
            if (!a) {
                return this.options;
            }
            if (typeof b === "undefined") {
                return this.options[a] !== undefined ? this.options[a] : false;
            }
            this.options[a] = b;
            return this;
        },
        
        /**
         * Gibt den aktuellen Wert zurück
         * 
         * @returns {Integer}
         */
        getValue: function() {
            return this.now;
        },

        /**
         * Aktualisiert den Wert der Progress-Bar auf den neuen Status
         * 
         * @param {Integer} value
         * @param {String} text
         * 
         */
        update: function(value, text) {
            var self = this, o = self.options;
            value = value && parseInt(value) || 0;
            if(o.animate) {
                self._updateAnimated(value, text);
            } else {
                self._updateValue(value, text);
            }
        },
        
        /**
         * Beendet den Progress und setzt den Wert auf den Maximal-Wert der 
         * Optionen
         * 
         * @param {String} text Status Text
         * 
         * 
         */
        finish: function(text) {
            var self = this, o = self.options;
            text = text || o.max + '%';
            self.status.innerHTML = text;
            this._updateValue(o.min);
        },

        /**
         * Setzt den Wert auf den Start-Wert zurück
         * 
         * @param {String} text Status Text
         * 
         * 
         */
        reset: function(text) {
            var self = this, o = self.options;
            text = text || o.start + '%';
            self.status.innerHTML = text;
            this._updateValue(o.start);
            if(typeof o.reset === 'function') {
                o.reset(self.meter, self.status, self);
            }
            var e = AcmsEvent.createCustom(ProgressBar.EVENT_RESET, {
                bar: self,
                meter: self.meter,
                value: o.start
            });
            AcmsEvent.dispatch(self.element, e);
        },
        
        /**
         * Light-Animation des Status-Wertes
         * 
         * @param {Integer} value   Der neue Wert
         * 
         * 
         */
        _updateAnimated: function(value, text) {
            var self = this, 
                o =self.options, 
                val = parseInt(value), start = self.now;
            if(this.interval) {
                clearInterval(this.interval);
            }
            this.interval = setInterval(function() {
                if(val < start) {
                    start -= 1;
                } else {
                    start += 1;
                }
                self._updateValue(start, text);
                if (start === val) {
                    clearInterval(self.interval);
                }
            }, o.delay);
        },
        
        /**
         * Aktualisiert den Wert im Progress-Meter
         * 
         * @param {Integer} value
         * 
         * 
         */
        _updateValue: function(value, text) {
            var self = this, o = self.options;
            self.meter.setAttribute('aria-valuenow', value);
            var prop = (this.vertical === true ? 'height' : 'width');
            self.meter.style[prop] = parseInt(value) + '%';
            self.now = value;
            self._updateText(value, text);
            if(typeof o.update === 'function') {
                o.update(self.meter, self.status, self);
                var e = AcmsEvent.createCustom(ProgressBar.EVENT_UPDATE, {
                    bar: self,
                    meter: self.meter,
                    value: value
                });
                AcmsEvent.dispatch(self.element, e);
            }
            if(value === o.max) {
                if(typeof o.finish === 'function') {
                    o.finish(self.meter, self.status, self);
                }
                var e2 = AcmsEvent.createCustom(ProgressBar.EVENT_FINISH, {
                    bar: self,
                    meter: self.meter,
                    value: value
                });
                AcmsEvent.dispatch(self.element, e2);
            }
        },
        
        _updateText: function(value, text) {
            var self = this, o = this.options;
            var postfix = (o.text === 'percent' ? '%' : o.currency);
            text = text || value + postfix;
            self.status.innerHTML = text;
        },
        
        /**
         * Vorbereiten der Progress-Bar
         * 
         * 
         */
        _prepare: function() {
            var self = this, 
                o = self.options, 
                element = self.element, 
                meter = Selectors.q('.progress-meter', element),
                status = Selectors.q('.progress-status', element);
            if(Classes.hasClass(element, 'vertical')) {
                this.vertical = true;
                element.acmsData('vertical', true);
            }
            meter.setAttribute('aria-valuemin', o.min);
            meter.setAttribute('aria-valuemax', o.max);
            meter.setAttribute('aria-valuenow', o.start);
            self.meter = meter;
            self.status = status;
            self.start = parseInt(o.start);
            self.now = self.start;
            self.interval = null;
            self.update(self.start);
        }

    };

    return ProgressBar;

});
