/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */


define("ui/widgets/datetimepicker/calendar-generator", [], function() {

    var Generator = {
        MODULE: 'Datetime Kalender Generator',

        VERSION: '1.5.0',
        days: null,

        /**
         * Initialisiert den Generator
         * 
         * @param {Array} days
         * @returns {Generator}
         */
        init: function(days) {
            Generator.days = days;

            return this;
        },

        /**
         * Generiert einen Kalender basierend auf der Moment Konfiguration
         * und dem übergebenen Datum
         * 
         * @param {String}          date     Datum
         * @param {Moment}          m        Moment.js Instanz
         * @param {DateTimePicker}  picker   Datetime-Picker
         *
         * @returns {Object}
         */
        generate: function(date, m, picker) {
            var cal = {}, locale = picker.option('locale');
            if (date === null) {
                return cal;
            }
            var startMonth = m(date).locale(locale).startOf('month'),
                endMonth = m(date).locale(locale).endOf('month'),
                iNumDay = startMonth.format('d'),
                startDate = startMonth.date(),
                endDate = endMonth.date(),
                i
            ;

            cal.week = this.days;
            cal.days = [];

            for (i = startDate; i <= endMonth.date(); i++) {
                if (i === startDate) {
                    var iWeek = cal.week.indexOf(iNumDay.toString());
                    if (iWeek > 0) {
                        for (var x = 0; x < iWeek; x++) {
                            cal.days.push(0);
                        }
                    }
                }
                cal.days.push(m(startMonth).locale(locale).date(i));
            }

            return cal;
        },

        /**
         * Generiert einen HTML Kalender
         * 
         * @param {Date}            date        Datum
         * @param {Object}          calendar    siehe this.generate()
         * @param {DateTimePicker}  picker      Datetime-Picker
         *
         * @returns {String}
         */
        generateHtml: function (date, calendar, picker, mo) {
            var tmp = "", o = picker.option(), i;
            tmp += '<div class="picker-picker-month">'
                        + date.locale(o.locale).format('MMMM YYYY')
                    + '</div>';
            tmp += '<table class="picker-picker-days"><thead>';
            for (i = 0; i < calendar.week.length; i++) {
                tmp += '<th>'
                            + mo(parseInt(calendar.week[i]), "d")
                                .locale(o.locale)
                                .format("dd")
                                .substring(0, 1)
                     + '</th>';
            }
            tmp += '</thead>';
            tmp += '<tbody><tr>';
            for (i = 0; i < calendar.days.length; i++) {
                if (i % 7 === 0) {
                    tmp += '</tr><tr>';
                }
                tmp += '<td data-date="' + mo(calendar.days[i])
                                            .locale(o.locale)
                                            .format("D") + '">';
                if (calendar.days[i] !== 0) {
                    if (picker.isSmallerThanMaxDate(mo(calendar.days[i]), false, false) === false
                            || picker.isBiggerThanMinDate(mo(calendar.days[i]), false, false) === false) {
                        tmp += '<span class="picker-select-day">'
                                + mo(calendar.days[i]).locale(o.locale).format("DD")
                               + '</span>';
                    } else {
                        var selected = (mo(calendar.days[i]).locale(o.locale).format("DD") === mo(picker.currentDate).locale(o.locale).format("DD"))
                                        ? ' selected'
                                        : '';
                        tmp += '<a href="javascript:void(0);" class="picker-select-day' + selected +'">'
                                    + mo(calendar.days[i]).locale(o.locale).format("DD")
                                    + '</a>';
                    }
                    tmp += '</td>';
                }
            }
            tmp += '</tr></tbody></table>';

            return tmp;
        }

    };

    return Generator;

});
