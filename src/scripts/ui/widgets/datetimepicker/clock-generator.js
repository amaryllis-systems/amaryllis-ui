/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 */

/**
 * 
 * @param {Object} c    Das Core Modul
 * 
 * @returns {Generator}
 */
define("ui/widgets/datetimepicker/clock-generator", [
    'core/style',
    'core/classes',
    'events/event',
    'core/selectors'
], function(Style, Classes, AcmsEvent, Selectors) {

    "use strict";

    var Generator = {

        MODULE: 'Datetime Clock Generator',

        VERSION: '1.5.0',

        /**
         *
         * @param {DateTimePicker} picker
         * 
         */
        generateHours: function (picker) {
            picker.currentView = 1;
            var clock = picker.picker.querySelector('.picker-clock'),
                clockParent = clock.parentNode.parentNode,
                content = picker.picker.querySelector('.picker-content'),
                pickerPicker = picker.picker.querySelector('.picker-picker');
            if (!picker.option('date')) {
                var w = content.offsetWidth,
                    ml = Style.getStyle(clock, 'marginLeft').replace('px', ''),
                    mr = Style.getStyle(clock, 'marginRight').replace('px', ''),
                    pl = Style.getStyle(pickerPicker, 'paddingLeft').replace('px', ''),
                    pr = Style.getStyle(pickerPicker, 'paddingRight').replace('px', '');
                clock.innerWidth = (w - (parseInt(ml) + parseInt(mr) + parseInt(pl) + parseInt(pr))) + 'px';
            } else {
                ele = picker.picker.querySelector('.picker-date');
                picker.toggleHidden(ele, true);
                ele = picker.picker.querySelector('.picker-time');
                picker.toggleHidden(ele, false);
            }
            picker.showTime(picker.currentDate);
            picker.initMeridienButtons();

            var ele = picker.picker.querySelector('.picker-datetime');
            picker.toggleHidden(ele, false);
            var ele2 = picker.picker.querySelector('.picker-calendar');
            picker.toggleHidden(ele2, true);

            if (picker.currentDate.hour() < 12) {
                var am = picker.picker.querySelector('a.meridien-am');
                AcmsEvent.fireClick(am);
            } else {
                var pm = picker.picker.querySelector('a.meridien-pm');
                AcmsEvent.fireClick(pm);
            }
            var pL = parseInt(window.getComputedStyle(clockParent, null).getPropertyValue('padding-left')),
                pT = parseInt(window.getComputedStyle(clockParent, null).getPropertyValue('padding-top')),
                mL = parseInt(window.getComputedStyle(clock, null).getPropertyValue('margin-left')),
                mT = parseInt(window.getComputedStyle(clock, null).getPropertyValue('margin-top')),
                r = clock.offsetWidth / 2,
                j = (r / 1.2),
                hours = [], h, x, y, hour, cH, hourLink;
            clock.innerHTML = '';
            for (h = 0; h < 12; h++) {
                    x = j * Math.sin(Math.PI * 2 * (h / 12)),
                    y = j * Math.cos(Math.PI * 2 * (h / 12)),
                    hour = document.createElement('div');
                    Classes.addClass(hour, 'picker-picker-time');
                    var css = {
                        'marginLeft': (r + x + pL / 2) - (pL + mL/1.5) +  'px',
                        'marginTop': (r - y - mT / 2) - (pT + mT/2.5) + 'px'
                    };
                    Style.setStyles(hour, css);
                    
                    cH = ((picker.currentDate.format('h') === 12)
                            ? 0
                            : picker.currentDate.format('h')),
                    hourLink = document.createElement('a');
                    hourLink.setAttribute('href', 'javascript:void(0);');
                    Classes.addClass(hourLink, 'picker-select-hour');
                    hourLink.acmsData('hour', h)
                    hourLink.textContent = (h == 0 ? 12 : h);
                if (h == parseInt(cH)) {
                    picker.toggleSelected(hourLink, true);
                }
                hour.appendChild(hourLink);
                hours.push(hour);
                clock.appendChild(hour);
            }

            //picker.picker.querySelector('a.picker-select-hour').off('click');
            picker.toggleTime(true);
            Style.setStyle(clock, 'height', (clock.offsetWidth /*+ (pT + mT)*/ + 'px'));
            this.generateHands(true, picker);
        },

        /**
         *
         * @param {DateTimePicker} picker
         * 
         */
        generateMinutes: function (picker) {
            var ampm;
            picker.currentView = 2;
            picker.showTime(picker.currentDate);
            picker.initMeridienButtons();
            if (picker.currentDate.hour() < 12) {
                ampm = picker.picker.querySelector('a.meridien-am');
            } else {
                ampm = picker.picker.querySelector('a.meridien-pm');
            }
            AcmsEvent.fireClick(ampm);
            var ele = picker.picker.querySelector('.picker-calendar'),
                ele2 = picker.picker.querySelector('.picker-datetime'),
                clock = picker.picker.querySelector('.picker-clock'),
                clockParent = clock.parentNode.parentNode;
            clock.innerHTML = '';
            picker.toggleHidden(ele, true);

            picker.toggleHidden(ele2, false);
            var pL = parseInt(window.getComputedStyle(clockParent, null).getPropertyValue('padding-left'));
            var pT = parseInt(window.getComputedStyle(clockParent, null).getPropertyValue('padding-top'));
            var mL = parseInt(window.getComputedStyle(clock, null).getPropertyValue('margin-left'));
            var mT = parseInt(window.getComputedStyle(clock, null).getPropertyValue('margin-top'));
            var r = (clock.offsetWidth / 2);
            var j = r / 1.2;
            var minutes = [], m, x, y, minute;

            for (m = 0; m < 60; m += 5) {
                
                x = j * Math.sin(Math.PI * 2 * (m / 60));
                y = j * Math.cos(Math.PI * 2 * (m / 60));
                minute = document.createElement('div');
                Classes.addClass(minute, 'picker-picker-time');
                Style.setStyles(minute, {
                                marginLeft: (r + x + pL / 2) - (pL + mL/1.5) + 'px',
                                marginTop: (r - y - mT / 2) - (pT + mT/2.5) + 'px'
                              });
                              
                var minuteLink = document.createElement('a');
                minuteLink.setAttribute('href', 'javascript:void(0);');
                Classes.addClass(minuteLink, 'picker-select-minute');
                minuteLink.acmsData('minute', m)
                minuteLink.textContent = (m.toString().length === 2)
                                            ? m : '0' + m;
                if (m == 5 * Math.round(picker.currentDate.minute() / 5)) {
                    picker.toggleSelected(minuteLink, true);
                    picker.currentDate.minute(m);
                }
                minute.appendChild(minuteLink);
                minutes.push(minute);
                clock.appendChild(minute);
            }
            picker.toggleTime(false);
            //Style.setStyle(clock, 'height', (clock.offsetWidth + (parseInt(pT) + parseInt(mT))) + 'px');
            Style.setStyle(clock, 'height', (clock.offsetWidth /*+ (pT + mT)*/ + 'px'));
            this.generateHands(false, picker);

            //picker.applyCentering();
        },

        generateHands: function (t, picker) {
            var clock = picker
                            .picker
                            .querySelector('.picker-clock'),
                clockParent = clock.parentNode.parentNode,
                div = document.createElement('div');
            div.innerHTML = '<div class="picker-hand picker-hand-hour"></div>' +
                            '<div class="picker-hand picker-hand-minute"></div>' +
                            '<div class="picker-clock-center"></div>';
            while(div.firstChild) {
                clock.appendChild(div.firstChild);
            }
            var clockCenter = clock.querySelector('.picker-clock-center'),
                handHour = clock.querySelector('.picker-hand-hour'),
                handMinute = clock.querySelector('.picker-hand-minute'),
                pL = parseInt(window.getComputedStyle(clockParent, null).getPropertyValue('padding-left')),
                pT = parseInt(window.getComputedStyle(clockParent, null).getPropertyValue('padding-top')),
                mL = parseInt(window.getComputedStyle(clock, null).getPropertyValue('margin-left')),
                mT = parseInt(window.getComputedStyle(clock, null).getPropertyValue('margin-top')),
                w = clockCenter.offsetWidth / 2,
                h = clockCenter.offsetWidth / 2,
                r = (clock.offsetWidth / 2),
                j = r / 1.2,
                HL = r / 1.7,
                ML = r / 1.5;
            var css = {
                    left: r + (mL * 1.5) + 'px',
                    height: HL + 'px',
                    marginTop: (r - HL - pL) + 'px'
                }, css2 = {
                        left: r + (mL * 1.5) + 'px',
                        height: ML + 'px',
                        marginTop: (r - ML - pL) + 'px'
                };
            Style.setStyles(handHour, css);
            if(t === true)
                Classes.addClass(handHour, 'on');
            Style.setStyles(handMinute, css2)
            if(t === false)
                Classes.addClass(handMinute,  'on');
            Style.setStyles(clockCenter, {
                            left: r + pL + mL - w + 'px',
                            marginTop: (r - (mL / 2)) - h + 'px'
                        });
            this.animateHands(picker, handHour, handMinute);

            //picker.applyCentering();
        },

        animateHands: function (picker, handHour, handMinute) {
            var h = picker.currentDate.hour();
            var m = picker.currentDate.minute();

            this.rotateElement(handHour, (360 / 12) * h);
            this.rotateElement(handMinute, ((360 / 60) * (5 * Math.round(m / 5))));
        },

        rotateElement: function (el, deg) {
            Style.setStyles(el, {
                        'transform': 'rotate(' + deg + 'deg)'
                    });
        }

    };

    return Generator;

});
