/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */

/**
 * Datetime Picker Template
 *
 * @param {Object} u
 * 
 * @returns {Template}
 */
define("ui/widgets/datetimepicker/template", [
    "tools/string/sprintf"
], function(sprintf) {

    var Template = {
        MODULE: 'Datetime HTML Template',

        VERSION: '1.5.0',
        string: '<div class="datetime-picker v1 hidden" aria-hidden="true" id="' + '%s' + '">'
                    + '<div class="picker-content">'
                        + '<div class="picker-date-view">'
                            + '<header class="picker-header">'
                                + '<div class="actual-day">Montag</div>'
                                + '<div class="picker-close">'
                                    +'<a href="javascript:void(0);">'
                                    +'<i aria-hidden="true" class="acms-icon acms-icon-times"></i>'
                                + '</div>'
                            + '</header>'
                            + '<div class="picker-date hidden" aria-hidden="true">'
                                + '<div>'
                                    + '<div class="text-center percentage-10">'
                                        + '<a href="javascript:void(0);" class="picker-prev-month">'
                                            + '<i aria-hidden="true" class="acms-icon acms-icon-chevron-left"></i>'
                                        +'</a>'
                                    + '</div>'
                                    + '<div class="actual-month percentage-80">JAN</div>'
                                    + '<div class="text-center percentage-10">'
                                        + '<a href="javascript:void(0);" class="picker-next-month">'
                                            + '<i aria-hidden="true" class="acms-icon acms-icon-chevron-right"></i>'
                                        + '</a>'
                                    + '</div>'
                                    + '<div class="clearfix"></div>'
                                + '</div>'
                                + '<div class="actual-number">13</div>'
                                + '<div>'
                                    + '<div class="text-center percentage-10">'
                                        + '<a href="javascript:void(0);" class="picker-prev-year">'
                                            + '<i aria-hidden="true" class="acms-icon acms-icon-chevron-left"></i>'
                                        + '</a>'
                                    + '</div>'
                                    + '<div class="text-center percentage-10">'
                                        + '<a href="javascript:void(0);" class="picker-prev-year-fast">'
                                            + '<i aria-hidden="true" class="acms-icon acms-icon-angle-double-left"></i>'
                                        + '</a>'
                                    + '</div>'
                                    + '<div class="actual-year percentage-60">2015</div>'
                                    
                                    + '<div class="text-center percentage-10">'
                                        + '<a href="javascript:void(0);" class="picker-next-year">'
                                            + '<i aria-hidden="true" class="acms-icon acms-icon-chevron-right"></i>'
                                        + '</a>'
                                    + '</div>'
                                    + '<div class="text-center percentage-10">'
                                        + '<a href="javascript:void(0);" class="picker-next-year-fast text-bold">'
                                            + '<i aria-hidden="true" class="acms-icon acms-icon-angle-double-right"></i>'
                                        + '</a>'
                                    + '</div>'
                                    + '<div class="clearfix"></div>'
                                + '</div>'
                            + '</div>'
                            + '<div class="picker-time hidden" aria-hidden="true">'
                                /** @todo + '<div class="time-up">'
                                        + '<a href="javascript:void(0);" class="picker-hour-up">'
                                            + '<i aria-hidden="true" class="acms-icon acms-icon-chevron-up"></i>'
                                        + '</a>'
                                        + '<a href="javascript:void(0);" class="picker-minute-up">'
                                            + '<i aria-hidden="true" class="acms-icon acms-icon-chevron-up"></i>'
                                        + '</a>'
                                + '</div>' */
                                + '<div class="actual-maxtime">23:55</div>'
                                /** @todo + '<div class="time-down">'
                                        + '<a href="javascript:void(0);" class="picker-hour-down">'
                                            + '<i aria-hidden="true" class="acms-icon acms-icon-chevron-down"></i>'
                                        + '</a>'
                                        + '<a href="javascript:void(0);" class="picker-minute-down">'
                                            + '<i aria-hidden="true" class="acms-icon acms-icon-chevron-down"></i>'
                                        + '</a>'
                                + '</div>' */
                            + '</div>'
                            + '<div class="picker-picker">'
                                + '<div class="picker-calendar"></div>'
                                + '<div class="picker-datetime hidden" aria-hidden="true">'
                                    + '<div class="actual-meridien">'
                                        + '<div class="left floated percentage-20">'
                                            + '<a class="meridien-am" href="javascript:void(0);">AM</a>'
                                        + '</div>'
                                        + '<div class="actual-time left floated"></div>'
                                        + '<div class="right floated percentage-20">'
                                            + '<a class="meridien-pm" href="javascript:void(0);">PM</a>'
                                        + '</div>'
                                        + '<div class="clearfix"></div>'
                                    + '</div>'
                                    + '<div class="picker-clock"></div>'
                                + '</div>'
                            + '</div>'
                        + '</div>' 
                        + '<div class="picker-buttons">'
                            + '<button class="picker-button-cancel button small">'
                                   + '<i aria-hidden="true" class="acms-icon acms-icon-times"></i>'
                                   + '<span class="src-only">' + '%s' + '</span>'
                            + '</button>'
                            + '<button class="picker-button-ok button small">'
                                   + '<i aria-hidden="true" class="acms-icon acms-icon-check"></i>'
                                   + '<span class="src-only">' + '%s' + '</span>'
                            + '</button>'
                        + '</div>'
                        + '<div class="dtp-copyright"><small>&copy; 2013 - '
                                + new Date().getFullYear()
                                + ' <a href="https://www.amaryllis-systems.de/" '
                                    + 'target="_blank" '
                                    + 'title="Kalender Widget von Amaryllis Systems GmbH">Amaryllis Systems</a>'
                                +'</small></div>'
                    + '</div>'
                + '</div>',

        render: function(name, cancelText, okText) {
            return sprintf(this.string, name, cancelText, okText);
        }
    };

    return Template;

});
