/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */

/**
 * Clock
 * 
 * @param {Object} Utils
 * @param {Object} Fade
 * @returns {Clock}
 */
define("ui/widgets/clock", [
    "tools/utils",
    "apps/ui/helpers/fade"
], function(Utils, Fade) {

    "use strict";

    var CLOCK_DATA = "acms.ui.widgets.clock",
        NS = "." + CLOCK_DATA;

    /**
     * Clock
     * 
     * Darstellung einer Digitalen Uhr
     * 
     * @param {HtmlElement} element Das Element
     * @param {Object|NULL} options Optionen
     * 
     * @exports ui/widgets/clock
     */
    var Clock = function(element, options) {
        this.element =
        this.options = null;
        this.initialize(element, options);
    };

    /**
     * Komponenten Name
     *
     * @var {String}
     */
    Clock.MODULE = "Clock";

    /**
     * Komponenten Version
     *
     * @var {String}
     */
    Clock.VERSION = "1.5.0";

    /**
     * Clock-Typ Digital
     *
     * @var {String}
     */
    Clock.TYPE_DIGITAL = "digital";

    /**
     * Clock-Typ Analog
     *
     * @var {String}
     */
    Clock.TYPE_ANALOG = "analog";

    /**
     * Standard-Uhr
     *
     * @var {Object}
     */
    Clock.DEFAULT_OPTIONS = {
        type: Clock.TYPE_DIGITAL
    };

    /**
     * Module Initialisierung
     *
     * @var {Boolean}
     */
    Clock.needInit = true;


    Clock.init = function(element, options) {
        var uhr = new Clock(element, options);
        
        return uhr;
    };

    Clock.css = "media/css/widgets/clock.min.css";

    Clock.prototype = {
        constructor: Clock,

        aHour:          [],
        aMinute:        [],
        aSecond:        [],
        dtDate:         new Date(),
        isCurrentHour:      -1,
        isCurrentMinute:    -1,
        isCurrentSecond:    -1,
        isStepSize:      10,
        animateTimer:  null,
        updateTimer:   null,
        
        
        initialize: function(element, options) {
            this.element = element;
            this.options = Clock.DEFAULT_OPTIONS;
            var self = this;
            this.animateTimer = setInterval(function() {
                self.animate();
            }, 20);
            this.updateTimer = setInterval(function() {
                self._update();
            }, 20);
            this._initDigital();
            
        },
        
        getDefaultOptions: function() {
            return Clock.DEFAULT_OPTIONS;
        },
        
        buildOptions: function(options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },

        animate:       function() {
            var self = this,
                o = this.options;
            switch(o.type) {
                case Clock.TYPE_ANALOG:
                    if (self.aSecond.length > 0) {
                        self._rotate("analogsecond", self.aSecond[0]);
                        self.aSecond = self.aSecond.slice(1);
                    }
                    break;
                case Clock.TYPE_DIGITAL:
                    if (self.aHour.length > 0) {
                        self._rotate("digitalhour", self.aHour[0], this.isCurrentHour);
                        self.aHour = self.aHour.slice(1);
                    }
                    if (self.aMinute.length > 0) {
                        self._rotate("digitalminute", self.aMinute[0], this.isCurrentMinute);
                        self.aMinute = self.aMinute.slice(1);
                    }
                    if (this.aSecond.length > 0) {
                        self._rotate("digitalsecond", self.aSecond[0], this.isCurrentSecond);
                        self.aSecond = self.aSecond.slice(1);
                    }
            }
            
        },
        
        _rotate:        function(sID, iDeg, curr) {
            var sCSS = ("rotate(" + iDeg + "deg)");
            var val = this._formatTime(curr);
            document.querySelector("#" + sID).innerHTML = val;
        },
        
        fStepSize:     function(iTo, iFrom) {
            var self = this,
                o = this.options,
                iAnimDiff;
            switch(o.type) {
                case Clock.TYPE_ANALOG:
                    iAnimDiff = (iFrom - iTo);
                    break;
                case Clock.TYPE_DIGITAL:
                    iAnimDiff = (iTo - iFrom);
                    break;
            }
            if (iAnimDiff > 0) {
                iAnimDiff -= 360;
            }

            return iAnimDiff / this.isStepSize;

        },

        _update:        function() {
            var self = this,
                o = this.options;
            this.dtDate = new Date();

            switch(o.type) {
                case Clock.TYPE_ANALOG:
                    var tmp = this.fGetHour();
                    if (this.iHourRotation != tmp) {
                        this.iHourRotation = tmp;
                        this._rotate("analoghour", tmp, this.isCurrentHour);
                    }
                    tmp = this.fGetMinute();
                    if (this.iMinuteRotation != tmp) {
                        this.iMinuteRotation = tmp;
                        this._rotate("analogminute", tmp, this.isCurrentMinute);
                    }
                    if (this.isCurrentSecond != this.dtDate.getSeconds()) {
                        var iRotateFrom = (6 * this.isCurrentSecond);
                        this.isCurrentSecond = this.dtDate.getSeconds();
                        var iRotateTo = (6 * this.isCurrentSecond);

                        // push steps into array
                        var iDiff = this.fStepSize(iRotateTo, iRotateFrom);
                        for (var i = 0; i < this.isStepSize; i++) {
                            iRotateFrom -= iDiff;
                            this.aSecond.push(Math.round(iRotateFrom));
                        }
                    }
                    break;
                case Clock.TYPE_DIGITAL:
                    if (this.isCurrentHour != this.dtDate.getHours()) {
                        var iRotateFrom = (360 - (15 * this.isCurrentHour));
                        this.isCurrentHour = this.dtDate.getHours();
                        var iRotateTo = (360 - (15 * this.isCurrentHour));
                        var iDiff = this.fStepSize(iRotateTo, iRotateFrom);
                        for (var i = 0; i < this.isStepSize; i++) {
                            iRotateFrom += iDiff;
                            this.aHour.push(Math.round(iRotateFrom));
                        }
                    }
                    if (this.isCurrentMinute != this.dtDate.getMinutes()) {
                        var iRotateFrom = (360 - (6 * this.isCurrentMinute));
                        this.isCurrentMinute = this.dtDate.getMinutes();
                        var iRotateTo = (360 - (6 * this.isCurrentMinute));
                        var iDiff = this.fStepSize(iRotateTo, iRotateFrom);
                        for (var i = 0; i < this.isStepSize; i++) {
                            iRotateFrom += iDiff;
                            this.aMinute.push(Math.round(iRotateFrom));
                        }
                    }
                    if (this.isCurrentSecond != this.dtDate.getSeconds()) {
                        var iRotateFrom = (360 - (6 * this.isCurrentSecond));
                        this.isCurrentSecond = this.dtDate.getSeconds();
                        var iRotateTo = (360 - (6 * this.isCurrentSecond));
                        var iDiff = this.fStepSize(iRotateTo, iRotateFrom);
                        for (var i = 0; i < this.isStepSize; i++) {
                            iRotateFrom += iDiff;
                            this.aSecond.push(Math.round(iRotateFrom));
                        }
                    }
                    break;
            }
        },

        fGetHour:     function() {
            var iHours = this.dtDate.getHours();
            if (iHours > 11) {
                iHours -= 12;
            }
            return Math.round((this.dtDate.getHours() * 30) + (this.dtDate.getMinutes() / 2) + (this.dtDate.getSeconds() / 120));
        },
        fGetMinute:     function() {
            return Math.round((this.dtDate.getMinutes() * 6) + (this.dtDate.getSeconds() / 10));
        },

        _initAnalog:          function() {
            this.iHourRotation = this.fGetHour();
            this._rotate("analoghour", this.iHourRotation, this.isCurrentHour);

            this.iMinuteRotation = this.fGetMinute();
            this._rotate("analogminute", this.iMinuteRotation, this.isCurrentMinute);

            this.isCurrentSecond = this.dtDate.getSeconds();
            this._rotate("analogsecond", (6 * this.isCurrentSecond), this.isCurrentSecond);
        },

        _initDigital: function() {
            this.isCurrentHour = this.dtDate.getHours();
            this.isCurrentMinute = this.dtDate.getMinutes();
            this.isCurrentSecond = this.dtDate.getSeconds();
            this._rotate("digitalhour", (360 - (15 * this.isCurrentHour)), this.isCurrentHour);
            this._rotate("digitalminute", (360 - (6 * this.isCurrentMinute)), this.isCurrentMinute);
            this._rotate("digitalsecond", (360 - (6 * this.isCurrentSecond)), this.isCurrentMinute);
            
            var el = document.querySelector("#clockdigital div");
            Fade.fadeTo(el, 500, 0.8);
        },
        
        _formatTime: function(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
	}
    };

    return Clock;
    
});
