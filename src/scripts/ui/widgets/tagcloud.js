/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */

define('ui/widgets/tagcloud', [
    "tools/utils",
    "core/selectors",
    "core/classes"
], function(Utils, Selectors, Classes) {

    "use strict";

    var TagCloud = function(element, options) {
        this.element =
        this.options =
        this.ctx =
        this.canvas = null;
        this.options = TagCloud.DEFAULT_OPTIONS;
        this.initialize(element, options);
    };

    /**
     * Komponenten Name
     *
     * @var {String}
     */
    TagCloud.MODULE = "Tag Cloud";

    /**
     * Komponenten Version
     *
     * @var {String}
     */
    TagCloud.VERSION = "1.5.0";

    /**
     * Standard Optionen
     *
     * @var {Object}
     */
    TagCloud.DEFAULT_OPTIONS = {
        tag: '.tag',
        ctxFont: 'Arial',
        colors: null
    };

    TagCloud.css = "media/css/widgets/tagcloud.min.css";

    TagCloud.prototype = {
        
        colors: ["aqua", "black", "blue", "fuchsia", "gray", "green", "lime", "maroon", "navy", "olive", "orange", "purple", "red", "silver", "teal"],
        
        constructor: TagCloud,
        
        /**
         * Interner Constructor
         * 
         * @param {Element} element
         * @param {Object|TagCloud.DEFAULT_OPTIONS} options
         * 
         */
        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            
        },

        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {TagCloud.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function() {
            return TagCloud.DEFAULT_OPTIONS;
        },

        /**
         * Bildet die Optionen
         *
         * Basierend auf den Standard-Optionen, den übergebenen Optionen des
         * Constructors und den Element Data-Attributen werden die aktuellen
         * Konfigurationen gebildet. Letztenendes gewinnen die Element
         * Data-Attribute.
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function(options) {
            var o = Utils.extend(
                                {},
                                this.getDefaultOptions(),
                                options,
                                this.element.acmsData()
                            );
            if(typeof o.colors === 'string') {
                try {
                    var obj = JSON.parse(o.colors);
                    o.colors = Object.keys(obj).map(function(k) { return obj[k];});
                } catch(E) {
                    o.colors = false;
                }
            }
            if(null === o.colors) {
                o.colors = false;
            }
            
            return o;
        },

        /**
         * Setter/Getter für die Optionen
         *
         * @param {String|NULL} a   Der Optionen-Schlüssel oder NULL
         * @param {Mixed}       b   Undefined oder der Wert für a
         * @returns {TagCloud.options|Boolean|Mixed|TagCloud.prototype}
         */
        option: function (a, b) {
            if (!a) {
                return this.options;
            }
            if (typeof b === "undefined") {
                return this.options[a] || false;
            }
            this.options[a] = b;
            return this;
        },
        
        setShape: function () {
            this.shape = "circle";
        },
        
        placeTag: function (tag) {
            var self = this, placement;
            while (!(placement = self._getNonOverlappingPlaceWithBestSize(self.fontSize, tag)))
                this.fontSize *= 0.9;

            this.ctx.fillStyle = this._getRandomColor();
            this.ctx.fillText(tag, placement.x, placement.y);
        },
        
        render: function () {
            var self = this,
                o = self.options,
                tags = self.tags;
            self.ctx.textBaseline = "top";
            tags.forEach(function (tag) {
                this.placeTag(tag);
            }, this);
        },
        
        getLineHeight: function () {
            return this.ctx.measureText('M').width * 1.2;
        },
        
        _getNonOverlappingPlaceWithBestSize: function (fontSize, tag) {
            this.ctx.font = "" + fontSize + "pt " + "Arial";
            var lineHeight=this.getLineHeight(fontSize);
            var tagWidth = this.ctx.measureText(tag).width;

            var base = new BasePlacement(
                (this.canvasWidth - tagWidth) * Math.random(),
                (this.canvasHeight - lineHeight) * Math.random(),
                lineHeight
                );

            var placement;
            while (placement = base.nextPlaceToTry()) {
                if (this._isPlaceEmpty(placement, tagWidth, lineHeight))
                    break;
            }
            return placement;
        },

        _prepare: function() {
            var self = this,
                o = self.options,
                element = self.element,
                canvas = Selectors.q('canvas', element), ctx;
            ctx = this.canvas.getContext("2d");
            self.canvasWidth = canvas.width;
            self.canvasHeight = canvas.height;
            self.canvas = canvas;
            self.ctx = ctx;
            self.fontSize = self.canvasHeight / 3;
            self.shape = "rectangle";
            self.tags = Selectors.qa(o.tag, element);
        },
        
        _getRandomColor: function (){
            var self = this,
                o = self.options,
                colors = (o.colors && 'length' in o.colors) ? o.colors : self.colors;
        
            return colors[Math.floor(colors.length * Math.random())];
        },
        
        _isPlaceEmpty: function (placement, width, height) {
            var self = this, pix, i, n;
            if (placement.x < 0 || 
                    placement.y < 0 || 
                    placement.x + width > self.canvasWidth || 
                    placement.y + height > self.canvasHeight) {
                return false;
            }

            pix = self.ctx.getImageData(placement.x, placement.y, width, height).data;
            for (i = 0, n = pix.length; i < n; i += 4) {
                if (pix[i+3]) {
                    return false;
                }
            }
            
            return [[placement.x, placement.y], 
                    [placement.x + width, placement.y], 
                    [placement.x, placement.y + height], 
                    [placement.x + width, placement.y + height]].every(
                        function(pos) {
                            var a = self.canvasWidth / 2;
                            var b = self.canvasHeight / 2;
                            var X = pos[0] - a;
                            var Y = pos[1] - b;
                            return (X * X / a / a + Y * Y / b / b < 1);
                        }, self);
        },
        
        getCoverage: function () {
            var self = this, pix = self.ctx.getImageData(0, 0, self.canvasWidth, self.canvasHeight).data,
                pixCount = 0, i, n;
            for (i = 0, n = pix.length; i < n; i += 4) {
                if (pix[i+3])
                    pixCount++;
            }
            return pixCount * 100 / self.canvasWidth / self.canvasHeight;
        }

    };
    
    function BasePlacement(x, y, h) {
        var baseX = x,
            baseY = y,
            scale = h,
            tryNumber = 0;

        this.nextPlaceToTry = function() {
            if (tryNumber < this._spiralOffsets.length)
                return {
                    x : baseX + this._spiralOffsets[tryNumber][0] * scale,
                    y : baseY + this._spiralOffsets[tryNumber++][1] * scale
                };
        };
    }

    function generateSpiralOffsets() {
        var spiralOffsets = [];
        var radius = 0;
        var dr = 0.2;
        for (var i = 0; radius < 40; i+=0.4, radius += dr) {
            spiralOffsets.push([
                       radius * Math.sin(i),
                       radius * Math.cos(i)
                    ]);
        }
        return spiralOffsets;
    }

    BasePlacement.prototype._spiralOffsets = generateSpiralOffsets();

    return TagCloud;
});
