/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */

define('ui/widgets/dock/remote-dockbar', [
    'tools/utils'
], function(Utils) {
    
    "use strict";
    
    var RemoteDockBar = function(element, options) {
        this.element =
        this.options =
        this.bar =
        this.dockers = null;
        this.initialize(element, options);
    };
    
    RemoteDockBar.MODULE = "Dock-Bar Remote Unterstützung";
    
    RemoteDockBar.VERSION = "1.5.0";
    
    RemoteDockBar.DEFAULT_OPTIONS = {
        remote: null,
        load: true,
        save: 5 //save every n seconds
    };
    
    RemoteDockBar.needInit = true;
    
    RemoteDockBar.init = function(element, options) {
        var rdb = new RemoteDockBar();
        
        return rdb;
    };
    
    RemoteDockBar.prototype = {
        constructor: RemoteDockBar,
        
        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            
        },
        
        getDefaultOptions: function() {
            return RemoteDockBar.DEFAULT_OPTIONS;
        },
        
        buildOptions: function(options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },
        
        _intitRemote: function() {
            var fn = function() {
                return self.process();
            }
            setInterval(fn, 4000);
        },
        
        _load: function() {
            var self = this, o = this.options;
            if(this.options.load === 'true' || this.options.load === true || parseInt(this.options.load) === 1) {
                require(['http/request'], function(Request) {
                    var fData = new FormData();
                    fData.append('op', 'load');
                    var done = function(response) {
                        if(response.status === 'success') {
                            if(response.data) {
                                // build Bar
                            }
                            self._intitRemote();
                        }
                    };
                    Request.post(o.remote, fData, done);
                });
            } else {
                this._intitRemote();
            }
        }
        
    };
    
    return RemoteDockBar;
    
});