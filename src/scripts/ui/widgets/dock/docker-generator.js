/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */


/**
 * 
 * @param {Object} c    Das Core Modul
 * 
 * @returns {Generator}
 */
define("ui/widgets/dock/docker-generator", [
    'core/style',
    'core/classes',
    'events/event',
    'core/selectors',
    'ui/widgets/window-docker'
], function(Style, Classes, AcmsEvent, Selectors, WindowDocker) {

    "use strict";

    var Generator = {

        MODULE: 'Window Docker Generator',

        VERSION: '1.5.0',
        
        generate: function() {
            
        }

    };

    return Generator;

});
