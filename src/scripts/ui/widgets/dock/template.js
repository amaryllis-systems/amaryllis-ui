/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */

define('ui/widgets/dock/template', [
     'tools/string/sprintf'
], function(sprintf) {
    
    "use strict";
    
    var Template = {
        MODULE: 'Window-Docker HTML Template',

        VERSION: '1.5.0',
        string: '<div class="window-docker no-footer popped-out" id="%s">'
                    + '<div class="window-docker-header">'
                          + '<div class="header-icons">'
                            + '<button data-trigger="header-action" class="header-action action-close">'
                                 + '<i class="icon-window-docker-close acms-icon acms-icon-window-close-o"></i>'
                                 + '<span class="src-only">'+Acms.Translator._('Schließen')+'</span>'
                            + '</a>'
                            + '<button data-trigger="header-action" class="header-action action-expand">'
                                 + '<i class="icon-window-docker-popout acms-icon acms-icon-expand"></i>'
                                 + '<span class="src-only">'+Acms.Translator._('Maximieren')+'</span>'
                            + '</button>'
                            + '<button href="#" data-trigger="header-action" class="header-action action-popout">'
                                 + '<i class="icon-window-docker-popout acms-icon acms-icon-window-restore"></i>'
                                 + '<span class="src-only">'+Acms.Translator._('Wiederherstellen')+'</span>'
                            + '</button>'
                            +  '<button href="#" data-trigger="header-action" class="header-action action-minimize">'
                                 + '<i class="icon-window-docker-minimize acms-icon acms-icon-window-minimize"></i>'
                                 + '<span class="src-only">'+Acms.Translator._('Minimieren')+'</span>'
                            + '</button>'
                            + '<button data-trigger="header-action" class="header-action action-move">'
                                 + '<i class="icon-window-docker-minimize acms-icon acms-icon-expand-arrows-alt-alt"></i>'
                                 + '<span class="src-only">'+Acms.Translator._('Verschieben')+'</span>'
                            + '</button>'
                          + '</div>'
                          + '<div class="title-text">%s</div>'
                          + '<div class="title-image"><img class="docker-image"  /></div>'
                    + '</div>'
                    + '<div class="window-docker-body">'
                          + '<div class="window-docker-content"></div>'
                    + '</div>'
                    + '<div class="window-docker-footer">'
                          + '<div class="window-docker-footer-buttonset">'
                                
                          +'</div>'
                    + '</div>'
                *'</div>',

        render: function(id, titelText) {
            return sprintf(this.string, id, titelText);
        }
    };
    
    return Template;
});
