/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @copyright 2013 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright 2014 - 2018 Amaryllis Systems GmbH http://www.amws.eu/
 * @copyright 2017 - 2018 Amaryllis CMS Project http://www.amaryllis-cms.de/
 * @license https://www.amaryllis-cms.de/lizenzen/
 * @license CC BY-ND https://creativecommons.org/licenses/by-nd/4.0/
 */


define('ui/widgets/feature-discovery', [
    'tools/utils',
    'core/classes',
    'core/selectors',
    'events/event'
], function(Utils, Classes, Selectors, AcmsEvent) {
    
    "use strict";
    
    /**
     * Feature Discovery
     * 
     * Öffnet eine Feature Discovery Box in den Ecken des Bildschirms
     * 
     * @param {type} element
     * @param {type} options
     * @returns {feature-discoveryL#17.FeatureDiscovery}
     */
    var FeatureDiscovery = function(element, options) {
        this.element =
        this.options =
        this.target = null;
        this.initialize(element, options);
    };
    
    FeatureDiscovery.MODULE = "Feature Discovery";
    
    FeatureDiscovery.VERSION = "1.5.0";
    
    FeatureDiscovery.NS = "acms.ui.widgets.feature-discovery";
    
    FeatureDiscovery.DEFAULT_OPTIONS = {
        target: null,
        beforeOpen: function(self, target, evt) {},
        onOpen: function(self, target, evt) {},
        beforeClose: function(self, target, evt) {},
        onClose: function(self, target, evt) {},
        positionY: 'bottom', // bottom|top
        positionX: 'left', // left|right
        openClass: 'open', // open class
        btnClass: 'active'
        
    };
    
    FeatureDiscovery.needInit = true;
    
    FeatureDiscovery.init = function(element, options) {
        var fd = new FeatureDiscovery(element, options);
        fd.element.acmsData(FeatureDiscovery.NS, fd);
        
        return fd;
    };
    
    FeatureDiscovery.prototype = {
        constructor: FeatureDiscovery,
        
        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
        },
        
        getDefaultOptions: function() {
            return FeatureDiscovery.DEFAULT_OPTIONS;
        },
        
        buildOptions: function(options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        }
        
        
    }
    
    
    return FeatureDiscovery;
    
});
