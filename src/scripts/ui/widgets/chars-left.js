/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */

define('ui/widgets/chars-left', [
    'tools/utils',
    'core/selectors',
    'core/classes',
    'events/event'
], function(Utils, Selectors, Classes, AcmsEvent) {
    
    "use strict";
    
    var CHAR_LEFT_DATA = "acms.ui.widgets.chars-left",
        NS = "." + CHAR_LEFT_DATA;
    
    /**
     * Chars Left
     * 
     * Hilfs-Tool für Text-Areas um verbleibende Zeichen zu zählen.
     * 
     * @param {HTMLElement} element Element
     * @param {Object|NULL} options Optionen
     * 
     * @exports ui/widgets/chars-left
     */
    var CharsLeft = function(element, options) {
        this.element    =
        this.options    = 
        this.current    =
        this.form       =
        this.textarea   =
        this.counter    = null;
        this.initialize(element, options);
    };
    
    /**
     * Modul Name
     * 
     * @var {String}
     */
    CharsLeft.MODULE = "Zeichen-Zähler";
    
    /**
     * Modul Namespace
     * 
     * @var {String}
     */
    CharsLeft.NS = "acms.ui.widgets.chars-left";
    
    
    /**
     * Standard Optionen
     * 
     * @var {Object}
     */
    CharsLeft.DEFAULT_OPTIONS = {
        maxchars: 300,
        minchars: 0,
        counter: '.character-counter',
        field: 'textarea',
        countertext: Acms.Translator._("{count} Zeichen übrig")
    };
    
    /**
     * Modul Loader einbinden
     * 
     * @var {Boolean}
     */
    CharsLeft.needInit = true;
    
    /**
     * Modul Initialisierung
     * 
     * @param {HTMLElement} element Element
     * @param {Object|NULL} options Optionen
     * 
     * @returns {CharsLeft}
     */
    CharsLeft.init = function(element, options) {
        var cl = new CharsLeft(element, options);
        cl.element.acmsData(CHAR_LEFT_DATA, cl);
        
        return cl;
    };
    
    CharsLeft.prototype = {
        constructor: CharsLeft,
        
        /**
         * Intener Constructor
         * 
         * @param {Node} element
         * @param {Object} options
         * 
         * 
         */
        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },
        /**
         * Gibt die Standard Optionen zurück
         * 
         * @returns {CharsLeft.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function() {
            return CharsLeft.DEFAULT_OPTIONS;
        },
        
        /**
         * Bilden der Optionen
         * 
         * @param {type} options
         * @returns {CharsLeft.DEFAULT_OPTIONS|Object}
         */
        buildOptions: function(options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },
        
        /**
         * Aufsetzen der Event Listener
         * 
         * 
         */
        listen: function() {
            var self = this,
                o = this.options;
            AcmsEvent.add(this.textarea, 'keyup', self._onKeyup);
            AcmsEvent.add(this.textarea, 'keydown', self._onKeyDown);
        },
        
        /**
         * On Key Down Callback
         * 
         * @param {Event} e2 Das Event
         * 
         * @returns {undefined|Boolean}
         */
        _onKeyDown: function(e2) {
            var max = this.max,
                o = this.options,
                tarea = this.textarea,
                value = tarea.tagName === 'INPUT' || tarea.tagName === 'TEXTAREA' ? tarea.value : tarea.textContent,
                count = parseInt(value.length);
            var key = e2.keyCode || e2.which;
            if(key === 8 || key === 46) {
                return;
            }
            if(max <= count) {
                e2.stopPropagation();
                e2.preventDefault();
                e2.returnValue = false;
                e2.cancelBubble = true;
                return false;
            }
        },
        
        /**
         * On Key Up Callback
         * 
         * @param {Event} e Das Event
         * 
         * @returns {undefined|Boolean}
         */
        _onKeyup: function(e) {
            var o = this.options,
                tarea = this.textarea, 
                max = this.max,
                value = tarea.tagName === 'INPUT' || tarea.tagName === 'TEXTAREA' ? tarea.value : tarea.textContent,
                count = parseInt(value.length),
                text;
            
            if (count > max) {
                var lines = value.split(/\r\n|\n/), i = 0;
                value = '';
                while (value.length < max && i < lines.length) {
                    value += lines[i].substring(0, max - value.length) + '\r\n';
                    i++;
                }
                if(tarea.tagName === 'INPUT' || tarea.tagName === 'TEXTAREA') {
                    tarea.value = value.substring(0, max);
                } else {
                    tarea.textContent = value.substring(0, max);
                }
                
                tarea.scrollTop = tarea.scrollHeight;
                count = max;
            }
            if(max >= count) {
                Classes.addClass(tarea, 'exceeded');
            } else if(max < count && Classes.hasClass(tarea, 'exceeded')){
                Classes.removeClass(tarea, 'exceeded');
            }
            this.current = count;
            text = o.countertext.replace("{count}", (max - count));
            this.counter.innerHTML = text;
            
        },
        
        /**
         * Vobereiten der Variablen
         * 
         * 
         */
        _prepare: function() {
            var self = this, o = self.options, element = self.element, val, text, max = o.maxchars;
            self.max = parseInt(o.maxchars);
            self.min = parseInt(o.minchars);
            self.form = Selectors.closest(element, 'form');
            self.textarea = Selectors.q(o.field, element);
            self.counter = Selectors.q(o.counter, element);
            if(!self.counter) {
                var counter = document.createElement('span');
                Classes.addClass(counter, 'hidden');
                if(o.counter.substring(0, 1) === '.') {
                    Classes.addClass(counter, o.counter.substring(1));
                }
                element.appendChild(counter);
                self.counter = counter;
            }
            if(self.textarea.tagName.toUpperCase() === 'TEXTAREA' || 
                    (self.textarea.tagName.toUpperCase() === 'INPUT' && 
                        self.textarea.type === 'text')) {
                val = self.textarea.value;
            } else {
                val = self.textarea.textContent;
            }
            self.current = val.length && parseInt(val.length) || 0;
            text = o.countertext.replace("{count}", (self.max - self.current));
            
            self.counter.innerHTML = text;
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        }
    };
    
    return CharsLeft;
});
