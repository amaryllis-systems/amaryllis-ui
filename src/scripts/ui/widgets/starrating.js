/**!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @copyright 2013 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright 2014 - 2018 Amaryllis Systems GmbH http://www.amws.eu/
 * @copyright 2017 - 2018 Amaryllis CMS Project http://www.amaryllis-cms.de/
 * @license https://www.amaryllis-cms.de/lizenzen/
 * @license Apache Licence 2.0 https://www.apache.org/licenses/LICENSE-2.0
 *
 * Dual-Licensed with Amaryllis CMS EULA. You may obtain a copy of the
 * EULA at
 *
 *     https://www.amaryllis-cms.de/lizenzen/eula/
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

define("ui/widgets/starrating", [
    'tools/utils',
    "core/selectors",
    "core/classes",
    "events/event"
], function (Utils, Selectors, Classes, AcmsEvent) {

    "use strict";

    var STAR_DATA = "acms.ui.widgets.starrating",
        NS = "." + STAR_DATA,
        EVENT_RATE = "rate" + NS,
        EVENT_RATED = "rated" + NS;

    var Starrating = function (element, options) {
        this.element =
        this.options = null;

        this.initialize(element, options);
    };

    Starrating.MODULE = "Star-Rating";

    Starrating.VERSION = "1.5.0";

    /**
     * Standard-Optionen
     *
     * @var {Object}
     */
    Starrating.DEFAULT_OPTIONS = {
        stars: 5,
        value: 0,
        half: true,
        static: false,
        showScore: true,
        scoreTitle: Acms.Translator._("Gesamtwertung"),
        currentScore: 0,
        size: 'default',
        colorRate: false,
        build: true,
        remote: null,
        dirname: null,
        item: null,
        itemid: null,
        onRate: function (v, s, w) {
            return true;
        },
        onRated: function (v, s, w) {
        },
        mapping: {
            0: Acms.Translator._("Nicht bewerted"),
            1: Acms.Translator._("Sehr Schlecht"),
            2: Acms.Translator._("Schlecht"),
            3: Acms.Translator._("Durchschnitt"),
            4: Acms.Translator._("Gut"),
            5: Acms.Translator._("Excellent")
        }
    };

    Starrating.css = "media/css/widgets/starrating.min.css";

    Starrating.needInit = true;

    Starrating.init = function(element, options) {
        var rate = new Starrating(element, options);
        rate.element.acmsData(STAR_DATA, rate);

        return rate;
    };

    Starrating.prototype = {
        _value: 0,
        _values: [],

        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            var o = this.options;
            this._value = parseFloat(o.value);
            this._values[0] = Math.ceil(o.stars * 1 / 3);
            this._values[1] = Math.ceil(o.stars * 2 / 3);
            this._values[2] = o.stars;
            if(o.build) {
                this._buildRating();
            }
            this.listen();
            this._setValue(this._value);
            this._setScore(this._value);
        },

        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {Starrating.DEFAULT_OPTIONS|Object}
         */
        getDefaultOptions: function() {
            return Starrating.DEFAULT_OPTIONS;
        },

        /**
         * Bildet die neuen Optionen
         *
         * @param {Object} options
         * 
         */
        buildOptions: function(options) {
            options = (options && typeof options === "object") ? options : {};
            return Utils.extend(
                    {},
                    this.getDefaultOptions(),
                    options,
                    this.element.acmsData());
        },

        rate: function(e, star) {
            var self = this,
                    element = this.element,
                    o = this.options;
            if (o.static || Classes.hasClass(element, 'static') || 
                    element.acmsData('static') === true) {
                if(e) {
                    e.preventDefault();
                    e.stopPropagation();
                }
                return false;
            }
            var result, value = star.acmsData('star-value');

            if (typeof o.onRate === 'function') {
                if (false === o.onRate(value, star, element)) {
                    return false;
                }
            }

            if (typeof o.onRated === 'function') {
                o.onRated(value, star, element);
            }
            self.value(value);
            
        },

        value: function (value) {
            if (value !== undefined) {
                this._value = value;
                this._setValue();
                this._setScore();
            } else {
                return this._value;
            }
        },

        listen: function () {
            var self = this,
                element = this.element,
                o = this.options,
                stars;
            
            var onClickStar = function(e) {
                self.rate(e, this);
            }, i;
            stars = Selectors.qa('.star', element);
            for(i = 0; i < stars.length; i++) {
                AcmsEvent.add(stars[i], 'click', onClickStar);
            }
        },

        _buildRating: function() {
            var element = this.element, o = this.options;
            var i, star, stars, score;
            if (!Classes.hasClass(element, 'rating')) {
                Classes.addClass(element, 'rating');
            }
            switch (o.size) {
                case 'small':
                    if(!Classes.hasClass(element, 'small')) Classes.addClass(element, 'small');
                    break;
                case 'large':
                    if(!Classes.hasClass(element, 'large')) Classes.addClass(element, 'large');
                    break;
                default:
                    break;
            }
            if (o.static) {
                Classes.addClass(element, 'static');
                element.acmsData('static', true);
            }
            for (i = 0; i < o.stars; i++) {
                star = document.createElement('span');
                star.acmsData('star-value', i + 1);
                element.appendChild(star);
            }

            if (o.showScore) {
                score = document.createElement('span');
                element.appendChild(score);
                score.innerHTML(o.scoreTitle + " " + o.currentScore);
            }
        },

        _setValue: function () {
            var stars, o = this.options, element = this.element, i;
            if (o.stars) {
                stars = Selectors.qa('.star', element);
                for(i = 0; i < stars.length; i++) {
                    Classes.removeClass(stars[i], 'on,half');
                }
                var index = Math.floor(this._value) - 1;
                var half = (this._value - Math.floor(this._value)) * 10 > 0;
                var $rated = stars[index];
                Classes.addClass($rated, 'on');
                var prevs = Selectors.previousAll($rated);
                for(i = 0; i < prevs.length; i++) {
                    Classes.addClass(prevs[i], 'on');
                }
                if (half) {
                    var next = Selectors.next($rated);
                    if(next) Classes.addClass(next, 'on,half');
                }
            }
            if (o.colorRate) {
                Classes.removeClass(element, 'poor,regular,good');
                if (this._value <= this._values[0]) {
                    Classes.addClass(element, 'poor');
                } else if (this._value > this._values[0] && this._value <= this._values[1]) {
                    Classes.addClass(element, 'regular');
                } else if (this._value > this._values[1]) {
                    Classes.addClass(element, 'good');
                }
            }
        },
        _setScore: function () {
            var value = this._value, element = this.element, o = this.options;

            if (value !== undefined) {
                var score = element.querySelector(".score");
                score.innerHTML(o.scoreTitle + " " + value);
            }
        }
        
    };

    return Starrating;

});
