/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */

/**
 * Ticker
 * 
 * @param {Object} c
 * @param {Object} u
 * @returns {Ticker}
 */
define('ui/widgets/ticker', [
    'tools/utils',
    'core/selectors',
    'events/event'
], function (Utils, Selectors, AcmsEvent) {

    "use strict";

    /**
     * Constructor
     *
     * @param {Object} element
     * @param {Object} options
     * @returns {Ticker}
     */
    var Ticker = function (element, options) {
        this.element =
        this.items =
        this.prevButton =
        this.nextButton =
        this.startButton =
        this.stopButton =
        this.options = null;

        this.options = Ticker.DEFAULT_OPTIONS;

        this.initialize(element, options);
    };

    Ticker.MODULE = "(News-) Ticker";

    Ticker.VERSION = "1.5.0";
    
    Ticker.NS = "acms.ui.widgets.ticker";

    Ticker.DEFAULT_OPTIONS = {
        items: 'li',
        duration: 1500,
        direction: 'up',
        autostart: 1,
        pauseOnHover: 1,
        nextButton: null,
        prevButton: null,
        startButton: null,
        stopButton: null,
        move: function (self) {},
        moveUp: function (self, item) {},
        moveDown: function (self, item) {},
        moveNext: function (self) {},
        movePrev: function (self) {},
        start: function (self) {},
        stop: function (self) {},
        pause: function (self) {},
        unpause: function (self) {}
    };

    Ticker.css = "media/css/widgets/ticker.min.css";

    Ticker.needInit = true;

    Ticker.init = function (element, options) {
        var t = new Ticker(element, options);
        t.element.acmsData(Ticker.NS, t);

        return t;
    };

    /**
     * Ticker gestoppt
     *
     * @var {Integer}
     */
    Ticker.STATE_STOPPED = 0;

    /**
     * Ticker gestartet
     *
     * @var {Integer}
     */
    Ticker.STATE_STARTED = 0;

    /**
     * Ticker pausiert
     *
     * @var {Integer}
     */
    Ticker.STATE_PAUSED = 2;

    Ticker.prototype = {
        constructor: Ticker,
        /**
         * Initializing the Ticker
         *
         * Method is called during the constructor of the Ticker.
         *
         * @param {Object} element The Element to initialize the slider on
         * @param {Object} options Custom Options for the slider
         *
         * @returns {Ticker}
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
            if (this.options.autostart) {
                this.start();
            }
        },
        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {Ticker.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return Ticker.DEFAULT_OPTIONS;
        },
        /**
         * Bildet die Optionen
         *
         * Basierend auf den Standard-Optionen, den übergebenen Optionen des
         * Constructors und den Element Data-Attributen werden die aktuellen
         * Konfigurationen gebildet. Letztenendes gewinnen die Element
         * Data-Attribute.
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function (options) {
            var o = Utils.extend(
                    {},
                    this.getDefaultOptions(),
                    options,
                    this.element.acmsData()
                    );
            o.duration = parseInt(o.duration);
            var autostart = o.autostart, pause = o.pauseOnHover;
            if(autostart === true || autostart === 1 || autostart === '1' || autostart === 'true' || autostart === 'on') {
                o.autostart = true;
            } else {
                o.autostart = false;
            }
            if(pause === true || pause === 1 || pause === '1' || pause === 'true' || pause === 'on') {
                o.pauseOnHover = true;
            } else {
                o.pauseOnHover = false;
            }
            
            return o;
        },
        /**
         * Setter/Getter für die Optionen
         *
         * @param {String|NULL} a   Der Optionen-Schlüssel oder NULL
         * @param {Mixed}       b   Undefined oder der Wert für a
         * @returns {Ticker.options|Boolean|Mixed|Ticker.prototype}
         */
        option: function (a, b) {
            if (!a) {
                return this.options;
            }
            if (typeof b === "undefined") {
                return (typeof this.options[a] === 'undefined' ? false : this.options[a]);
            }
            this.options[a] = b;
            if(a === 'duration') {
                this.resetInterval();
            }
            return this;
        },
        start: function () {
            if (!this.state) {
                this.state = Ticker.STATE_STARTED;
                this.resetInterval();
                this._dispatch('start');
            }
        },
        stop: function () {
            if (this.state) {
                clearInterval(this.timer);
                this.state = Ticker.STATE_STOPPED;
                this._dispatch('stop');
            }
        },
        resetInterval: function () {
            var self = this, o = self.options;
            if (this.state) {
                clearInterval(self.timer);
                self.timer = setInterval(function () {
                    self.move();
                }.bind(self), o.duration);
            }
        },
        move: function () {
            if (!this.paused) {
                var self = this;
                self.moveNext();
                self._dispatch('move');
            }
        },
        moveNext: function () {
            var self = this, o = self.options;
            self._dispatch('moveNext');
            if (o.direction === 'down')
                self.moveDown();
            else if (self.options.direction === 'up')
                self.moveUp();
        },
        movePrev: function () {
            var self = this, o = self.options;
            self._dispatch('movePrev');
            if (o.direction === 'down') {
                self.moveUp();
            } else if (o.direction === 'up') {
                self.moveDown();
            }
        },
        pause: function () {
            if (!this.paused) {
                this.paused = 1;
                this._dispatch('pause');
            }
        },
        unpause: function () {
            if (this.paused) {
                this.paused = 0;
                this._dispatch('unpause');
            }
        },
        moveDown: function () {
            var self = this, item, top;
            self.index--;
            if(self.index < 0) {
                self.index = self.length - 1;
            }
            item = self.items[self.index];
            self._dispatch('moveDown', item);
            top = item.getAttribute("data-top");
            self.wrapper.style.top = "-" + top + "px";
        },
        moveUp: function () {
            var self = this, item, top;
            self.index++;
            if(self.index === self.length) {
                self.index = 0;
            }
            item = self.items[self.index];
            self._dispatch('moveUp', item);
            top = item.getAttribute("data-top");
            self.wrapper.style.top = "-" + top + "px";

        },
        
        getState: function () {
            if (this.paused)
                return Ticker.STATE_PAUSED;
            else
                return this.state;
        },
        
        listen: function() {
            var self = this, o = self.options, element = self.element;
            if(o.pauseOnHover) {
                AcmsEvent.add(element, 'mouseenter', self.pause.bind(self));
                AcmsEvent.add(element, 'mouseleave', self.unpause.bind(self));
            }
            if(self.nextButton !== null) {
                AcmsEvent.add(self.nextButton, 'click', self.moveNext.bind(self));
            }
            if(self.prevButton !== null) {
                AcmsEvent.add(self.prevButton, 'click', self.movePrev.bind(self));
            }
            if(self.stopButton !== null) {
                AcmsEvent.add(self.stopButton, 'click', self.stop.bind(self));
            }
            if(self.startButton !== null) {
                AcmsEvent.add(self.startButton, 'click', self.start.bind(self));
            }
        },
        
        _prepare: function() {
            var self = this, o = self.options, element = self.element;
            self.items = Selectors.qa(o.items, element);
            self.wrapper = Selectors.q(".ticker-wrapper-inner", element);
            self.length = self.items.length;
            if(o.prevButton) {
                self.prevButton = Selectors.q(o.prevButton, element);
            }
            if(o.nextButton) {
                self.nextButton = Selectors.q(o.nextButton, element);
            }
            if(o.stopButton) {
                self.stopButton = Selectors.q(o.stopButton, element);
            }
            if(o.startButton) {
                self.startButton = Selectors.q(o.startButton, element);
            }
            self._setElementOffsets();
            self.timer = null;
            self.index = 0;
        },
        
        _setElementOffsets: function() {
            var self = this, o = self.options, element = self.element;
            var li = self.items, height = 0;

            for( var i = 0; i < self.length; ++i ) {
                    var item = li[i];
                    var top = item.offsetTop;

                    item.setAttribute( "data-top", top );
            }
        },
        
        _dispatch: function(evt, item) {
            var self = this, o = self.options;
            if(typeof o[evt] === 'function') {
                o[evt](self, item);
            }
            var name = Ticker.NS + '.' + evt, params = {relatedTarget: item, ticker: self};
            var e = AcmsEvent.createCustom(name, params);
            AcmsEvent.dispatch(e);
            return e;
        }
    };

    return Ticker;
});
