/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */


/**
 * Funding Bar
 * 
 * Die Funding-Bar bietet einfache Optionen zur Darstellung einer 
 * Status-/Ziel-Anzeige.
 * 
 * @param {Object} Utils
 * @param {Object} Selectors
 * @param {Object} Classes
 * @param {Object} AcmsEvent
 * 
 * @returns {FundingBar}
 */
define("ui/widgets/fundingbar", [
    "tools/utils",
    "core/selectors",
    "core/classes",
    "events/event",
    "core/style"
], function (Utils, Selectors, Classes, AcmsEvent, Style) {

    "use strict";

    /**
     * FundingBar Constructor
     * 
     * Erstellt eine neue Funding-Bar
     *
     * @param {Element} element Das HTML Element
     * @param {Object} options  Die optionalen eigenen Optionen
     * 
     * @returns {FundingBar}
     */
    var FundingBar = function (element, options) {
        this.element =
        this.start =
        this.now =
        this.vertical =
        this.interval =
        this.options = null;
        this.initialize(element, options);
    };

    /**
     * Component Name
     *
     * @var {String}
     */
    FundingBar.MODULE = "FundingBar";
    
    /**
     * Component Version
     *
     * @var {String}
     */
    FundingBar.VERSION = "1.5.0";
    
    /**
     * Component Namespace
     *
     * @var {String}
     */
    FundingBar.NS = "acms.ui.widgets.fundingbar";
    
    /**
     * Update Event
     * 
     * @var {String}
     */
    FundingBar.EVENT_UPDATE = "update." + FundingBar.NS;
    
    /**
     * Finish Event
     * 
     * @var {String}
     */
    FundingBar.EVENT_FINISH = "finish." + FundingBar.NS;
    
    /**
     * Reset Event
     * 
     * @var {String}
     */
    FundingBar.EVENT_RESET = "reset." + FundingBar.NS;

    /**
     * Default Options
     *
     * @var {Object}
     */
    FundingBar.DEFAULT_OPTIONS = {
        delay: 100,
        precision: 2,
        text: 'percent', // percent oder currency
        use_percentage: true,
        formatPercent: function(percent) { return percent + '%'; },
        formatAmount: function(amount_part, amount_max, amount_min) { return amount_part + ' / ' + amount_max; },
        currency: '€',
        update: function(meter, status, bar){},
        finish: function(meter, status, bar){},
        reset: function(meter, status, bar){},
        min: 0,
        max: 100,
        start: 0,
        animate: true
    };

    /**
     * Einbinden des Module Loaders
     * 
     * @var {Boolean}
     */
    FundingBar.needInit = true;

    /**
     * Modul Initialisierung
     * 
     * @param {Element} element
     * @param {Object} options
     * 
     * @exports ui/widgets/FundingBar
     */
    FundingBar.init = function(element, options) {
        var bar = new FundingBar(element, options);
        bar.element.acmsData(FundingBar.NS, bar);
        return bar;
    };
    
    /**
     * CSS-Datei
     * 
     * @var {String}
     */
    FundingBar.css = "media/css/widgets/funding-bar.min.css";

    FundingBar.prototype = {

        constructor: FundingBar,

        /**
         * Intenrer Constructor
         * 
         * @param {Element} element
         * @param {Object} options
         * 
         * 
         */
        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
        },

        /**
         * Standard-Option
         *
         * Gibt dir Standard-Optionen zurück
         *
         * @returns {Object|FundingBar.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function() {
            return FundingBar.DEFAULT_OPTIONS;
        },

        /**
         * Bildet die Optionen
         *
         * <p>
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         * </p>
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function (options) {
            var o = Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
            o.precision = parseInt(o.precision);
            o.start = parseFloat(o.start).toFixed(o.precision);
            o.min = parseFloat(o.min).toFixed(o.precision);
            o.max = parseFloat(o.max).toFixed(o.precision);
            o.delay = parseInt(o.delay);
            if(o.animate === 'true' || o.animate === true || parseInt(o.animate) === 1) {
                o.animate = true;
            } else {
                o.animate = false;
            }
            
            return o;
        },

        /**
         * Setter/Getter für die Optionen
         *
         * @param {String|NULL} a   Der Optionen-Schlüssel oder NULL
         * @param {Mixed}       b   Undefined oder der Wert für a
         * @returns {FundingBar.options|Boolean|Mixed|FundingBar.prototype}
         */
        option: function (a, b) {
            if (!a) {
                return this.options;
            }
            if (typeof b === "undefined") {
                return this.options[a] !== undefined ? this.options[a] : false;
            }
            this.options[a] = b;
            return this;
        },
        
        /**
         * Gibt den aktuellen Wert zurück
         * 
         * @returns {Integer}
         */
        getValue: function() {
            return this.now;
        },

        /**
         * Aktualisiert den Wert der Funding-Bar auf den neuen Status
         * 
         * @param {Integer} value
         * @param {String} text
         * 
         */
        update: function(value, text) {
            var self = this, o = self.options;
            if(o.animate) {
                self._updateAnimated(value, text);
            } else {
                self._updateValue(value, text);
            }
        },
        
        /**
         * Beendet den Funding und setzt den Wert auf den Maximal-Wert der 
         * Optionen
         * 
         * @param {String} text Status Text
         * 
         * 
         */
        finish: function(text) {
            var self = this, o = self.options;
            this._updateValue(o.min, text);
        },

        /**
         * Setzt den Wert auf den Start-Wert zurück
         * 
         * @param {String} text Status Text
         * 
         * 
         */
        reset: function(text) {
            var self = this, o = self.options;
            this._updateValue(o.start, text);
            if(typeof o.reset === 'function') {
                o.reset(self.meter, self.status, self);
            }
            var e = AcmsEvent.createCustom(FundingBar.EVENT_RESET, {
                bar: self,
                meter: self.meter,
                value: o.start
            });
            AcmsEvent.dispatch(self.element, e);
        },
        
        /**
         * Light-Animation des Status-Wertes
         * 
         * @param {Integer} value   Der neue Wert
         * 
         * 
         */
        _updateAnimated: function(value, text) {
            var self = this, 
                o =self.options, 
                val = value && (parseFloat(value)).toFixed(o.precision) || 0,
                decimals = val - Math.floor(val),
                start = self.now + decimals.toFixed(o.precision),
                total = self.total;
            this.interval = setInterval(function() {
                if(val < start) {
                    start -= 1;
                } else {
                    start += 1;
                }
                
                self._updateValue(start, text);
                if (start >= val) {
                    clearInterval(self.interval);
                }
            }, o.delay);
        },
        
        /**
         * Aktualisiert den Wert im Funding-Meter
         * 
         * @param {Integer} value
         * 
         * 
         */
        _updateValue: function(value, text) {
            var self = this, o = self.options;
            value = parseFloat(value).toFixed(o.precision) || 0;
            self.meter.setAttribute('aria-valuenow', value);
            var prop = (this.vertical === true ? 'height' : 'width');
            var percent = parseFloat((value / self.total) * 100).toFixed();
            if(percent > 100) {
                percent = 100;
            }
            if(percent < 15) {
                self.adJustInfo = true;
            }
            var style = 'translateY(' + percent + '%)';
            var props = ['transform', '--webkit-transform', '--o-transform', '--moz-transform', '--ms-transform'];
            props.forEach(function(prop) {
                self.meter.style[prop] = style;
            });
            self.now = value;
            self._updateText(value, text);
            if(typeof o.update === 'function') {
                o.update(self.meter, self.status, self);
                var e = AcmsEvent.createCustom(FundingBar.EVENT_UPDATE, {
                    bar: self,
                    meter: self.meter,
                    value: value
                });
                AcmsEvent.dispatch(self.element, e);
            }
            if(value === o.max) {
                if(typeof o.finish === 'function') {
                    o.finish(self.meter, self.status, self);
                }
                var e2 = AcmsEvent.createCustom(FundingBar.EVENT_FINISH, {
                    bar: self,
                    meter: self.meter,
                    value: value
                });
                AcmsEvent.dispatch(self.element, e2);
            }
        },
        
        _updateText: function(value, text) {
            var self = this, o = this.options;
            text = text || (value + '/' + self.total);
            self.status.innerHTML = text;
        },
        
        /**
         * Vorbereiten der Funding-Bar
         * 
         * 
         */
        _prepare: function() {
            var self = this, 
                o = self.options, 
                element = self.element, 
                meter = Selectors.q('.bar', element),
                status = Selectors.q('.funding-info-inner', element),
                info = Selectors.q('.funding-info', element);
            
            meter.setAttribute('aria-valuemin', o.min);
            meter.setAttribute('aria-valuemax', o.max);
            meter.setAttribute('aria-valuenow', o.start);
            self.meter = meter;
            self.status = status;
            self.info = info;
            self.start = parseInt(o.start);
            self.total = o.max;
            self.now = self.start;
            self.interval = null;
            self.update(self.start);
        }

    };

    return FundingBar;

});
