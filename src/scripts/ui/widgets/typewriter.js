/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */

/**
 * Type Writer
 * 
 * Hilfs-Tool als Life-Type Writer zur Änderung von animirtem Text im Element.
 * 
 * @param {Objet}           Utils       Das Utils Modul
 * @param {Object|Classes}  Classes     Das Classes Modul
 * @param {Object|Style}    Style       Das Style Modul
 * @returns {TypeWriter}
 */
define("ui/widgets/typewriter", [
    'tools/utils',
    "core/classes",
    "core/style"
], function (Utils, Classes, Style) {

    "use strict";

    /**
     * Constructor
     * 
     * @param {Element} element     Element
     * @param {Object}  options     Eigene Optionen
     * 
     * @exports ui/widgets/typewriter
     */
    var TypeWriter = function (element, options) {
        this.element = 
        this.options =
        this.period = 
        this.current =
        this.content =
        this.isDeleting = null;
        this.options = TypeWriter.DEFAULT_OPTIONS;
        
        this.initialize(element, options);
    };
    
    
    /**
     * Modul Name
     *
     * @var {String}
     */
    TypeWriter.MODULE = "TypeWriter";

    /**
     * Modul Version
     *
     * @var {String}
     */
    TypeWriter.VERSION = "1.5.0";

    /**
     * Modul Namespace
     *
     * @var {String}
     */
    TypeWriter.NS = "acms.ui.widgets.typewriter";

    /**
     * Standard-Optionen
     * 
     * @var {Object}
     */
    TypeWriter.DEFAULT_OPTIONS = {
        type: null,
        period: 2000,
        borderWidth: '0.08em',
        borderTye: 'solid',
        borderColor: '#FFF',
        beforeTick: function(self) {},
        afterTick: function(self) {}
    };
    
    /**
     * Einbinden des Modul Loader
     * 
     * @var {Boolean}
     */
    TypeWriter.needInit = true;
    
    /**
     * Modul Initialisierung
     * 
     * @param {Element} element     Element
     * @param {Object}  options     Eigene Optionen
     * 
     * @returns {TypeWriter}
     */
    TypeWriter.init = function(element, options) {
        var tw = new TypeWriter(element, options);
        tw.element.acmsData(TypeWriter.NS, tw);
        
        return tw;
    };
    
    TypeWriter.prototype = {
       
        constructor: TypeWriter,
        
        /**
         * Interner Constructor
         * 
         * @param {Element} element     Element
         * @param {Object}  options     Eigene Optionen
         * 
         * @returns {TypeWriter}
         */
        initialize:  function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this.current = 0;
            this.period = parseInt(period, 10) || 2000;
            this.content = '';
            this.tick();
            this.isDeleting = false;
        },
        
        
        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {TypeWriter.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return TypeWriter.DEFAULT_OPTIONS;
        },

        /**
         * Bildet die Optionen
         *
         * <p>
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         * </p>
         *
         * @param {Object} options
         *
         * @returns {Object|TypeWriter.DEFAULT_OPTIONS} Die gebildeten Optionen
         */
        buildOptions: function (options) {
            var o = Utils.extend(
                    {},
                    this.getDefaultOptions(),
                    options,
                    this.element.acmsData()
                    );
            o.period = parseInt(o.period, 10) || 2000;
            try {
                o.type = JSON.parse(o.type);
            } catch(E) {
                o.type = o.type || "";
            }
            
            return o;
        },
        
        /**
         * Weiter zum nächsten Text
         * 
         * 
         */
        tick: function () {
            var self = this, 
                o = self.options, 
                element = self.element,
                length = (typeof o.type === "object") ? o.type.length : 1,
                i = self.current % length,
                fullTxt = self.toRotate[i],
                delta;

            if (self.isDeleting) {
                self.content = fullTxt.substring(0, self.content.length - 1);
            } else {
                self.content = fullTxt.substring(0, self.content.length + 1);
            }
            o.beforeTick(self);
            var span = document.createElement('span');
            span.innerHTML = self.content;
            element.innerHTML = '';
            element.appendChild(span);
            Classes.addClass(span, 'wrap');
            Style.setStyles(span, {
                'border-right-width': o.borderWidth,
                'border-right-style': o.borderTye,
                'border-right-color': o.borderColor
            });

            delta = 200 - Math.random() * 100;
            if (self.isDeleting) {
                delta /= 2;
            }

            if (!self.isDeleting && self.content === fullTxt) {
                delta = self.period;
                self.isDeleting = true;
            } else if (this.isDeleting && self.content === '') {
                self.isDeleting = false;
                self.current++;
                delta = 500;
            }
            o.afterTick(self);
            setTimeout(function () {
                self.tick();
            }, delta);
        }
    };
    
    return TypeWriter;

});
