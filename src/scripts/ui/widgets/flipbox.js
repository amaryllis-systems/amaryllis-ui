/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */


define('ui/widgets/flipbox', [
    'tools/utils',
    'core/selectors',
    'core/classes',
    'events/event'
], function (Utils, Selectors, Classes, AcmsEvent) {

    "use strict";

    /**
     * Constructor
     *
     * @param {Object} element
     * @param {Object} options
     * 
     * @exports ui/widgets/FlipBox
     */
    var FlipBox = function (element, options) {
        this.element =
            this.$front =
            this.$back =
            this.trigger =
            this.options =
            this.state = null;
        this.options = FlipBox.DEFAULT_OPTIONS;
        this.initialize(element, options);
    };

    /**
     * Komponenten Name
     *
     * @var {String}
     */
    FlipBox.MODULE = "FlipBox";

    /**
     * Komponenten Namespace
     *
     * @var {String}
     */
    FlipBox.NS = "acms.ui.widgets.flipbox";

    
    FlipBox.EVENT_SHOW = FlipBox.NS + '.show';

    FlipBox.EVENT_SHOWN = FlipBox.NS + '.shown';

    FlipBox.EVENT_HIDE = FlipBox.NS + '.hide';

    FlipBox.EVENT_HIDDEN = FlipBox.NS + '.hidden';

    /**
     * Standard Optionen
     *
     * @var {Object}
     */
    FlipBox.DEFAULT_OPTIONS = {
        trigger: '[data-trigger="flip"]',
        onHover: false,
        flipped: 'active',
        front: '.front',
        back: '.back',
        css: FlipBox.css,
        themecss: false,
        onShow: function (self, front, back, ele) { return true; },
        onShown: function (self, front, back, ele) { },
        onHide: function (self, front, back, ele) { return true; },
        onHidden: function (self, front, back, ele) { }
    };

    FlipBox.needInit = true;

    FlipBox.init = function (element, options) {
        var fb = new FlipBox(element, options);
        fb.element.acmsData(FlipBox.NS, fb);

        return fb;
    };

    FlipBox.css = "media/css/widgets/flip-box.css";

    FlipBox.prototype = {

        constructor: FlipBox,

        /**
         * @param {HTMLElement} element Das Element
         * @param {Object|NULL} options Eigene Optionen
         * 
         * @memberof 
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this.state = this.isFlipped();
            this._prepare();
            this.listen();
        },

        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {FlipBox.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return FlipBox.DEFAULT_OPTIONS;
        },

        /**
         * Bildet die Optionen
         *
         * <p>
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         * </p>
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function (options) {
            return Utils.extend(
                {},
                this.getDefaultOptions(),
                options,
                this.element.acmsData()
            );
        },

        /**
         * Toggle zwischen Front/Back
         */
        toggle: function () {
            return (this.isFlipped() ? this.front() : this.back());
        },

        /**
         * Aktivieren der Front Card
         */
        front: function () {
            var e = AcmsEvent.createCustom(FlipBox.EVENT_SHOW, { relatedTarget: this.$front }),
                o = this.options,
                self = this, res = true;
            AcmsEvent.dispatch(self.element, e);
            if (typeof o.onShow === 'function') {
                res = o.onShow(self, self.$front, self.$back, self.element);
            }
            if (false === res || e.defaultPrevented) {
                return;
            }
            Classes.removeClass(self.element, o.flipped);
            this.$front.setAttribute('aria-hidden', 'false');
            this.$back.setAttribute('aria-hidden', 'true');
            var evt = AcmsEvent.createCustom(FlipBox.EVENT_SHOWN, { relatedTarget: this.$front });
            AcmsEvent.dispatch(self.element, evt);
            if (typeof o.onShown === 'function') {
                o.onShown(self, self.$front, self.$back, self.element);
            }
        },

        /**
         * Aktivieren der Back Card
         */
        back: function () {
            var e = AcmsEvent.createCustom(FlipBox.EVENT_HIDE, { relatedTarget: this.$back }),
                o = this.options,
                self = this, res;
            AcmsEvent.dispatch(self.element, e);
            if (typeof o.onHide === 'function') {
                res = o.onHide(self, self.$front, self.$back, self.element);
            }
            if (false === res || e.defaultPrevented) {
                return;
            }
            Classes.addClass(self.element, o.flipped);
            this.$front.setAttribute('aria-hidden', 'true');
            this.$back.setAttribute('aria-hidden', 'false');
            var evt = AcmsEvent.createCustom(FlipBox.EVENT_HIDDEN, { relatedTarget: this.$back[0] });
            AcmsEvent.dispatch(self.element, evt);
            if (typeof o.onHidden === 'function') {
                o.onHidden(self, self.$front, self.$back, self.element);
            }
        },
        
        
        /**
         * Ist aktuell flipped?
         */
        isFlipped: function () {
            var o = this.options, active = o.flipped;
            return Classes.hasClass(this.element, active);

        },

        /**
         * Aufsetzen der Event Listener
         */
        listen: function () {
            var self = this, o = this.options, j;

            if (null !== self.trigger) {
                for (j = 0; j < self.trigger.length; j++)
                    AcmsEvent.add(self.trigger[j], 'click', self._onClick.bind(self));
            }
            if (o.onHover) {
                AcmsEvent.add(self.$front, 'mouseenter', self._onMouseEnter.bind(self));
                AcmsEvent.add(self.$back, 'mouseleave', self._onMouseLeave.bind(self));
            }
        },

        /**
         * On Mouse Enter Event Handler
         * 
         * @param  {Event} e
         * 
         * @returns {Boolean|void}  
         */
        _onMouseEnter: function (e) {
            if (!this.mouseEntered) {
                this.mouseEntered = true;
                e.preventDefault();
                e.stopPropagation();
                this.toggle();
                return false;
            }
        },

        /**
         * On Mouse Leave Event Handler
         * 
         * @param  {Event} e
         * 
         * @returns {Boolean|void}  
         */
        _onMouseLeave: function (e) {
            if (this.mouseEntered) {
                this.mouseEntered = false;
                e.preventDefault();
                e.stopPropagation();
                this.toggle();
                return false;
            }
        },

        /**
         * On Click Event Handler
         * 
         * @param  {Event} e
         * 
         * @returns {Boolean|void}  
         */
        _onClick: function (e) {
            e.preventDefault();
            e.stopPropagation();
            this.toggle();
            return false;
        },
        
        /**
         * Vorbereiten
         */
        _prepare: function () {
            var self = this, o = self.options, element = self.element;
            self.$front = Selectors.q(o.front, element);
            self.$back = Selectors.q(o.back, element);
            if (o.trigger) {
                self.trigger = Selectors.qa(o.trigger, element);
            }
        }

    };

    return FlipBox;
});
