/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */


define('ui/widgets/screen-lock', [
    'tools/utils'
], function(Utils) {
    
    "use strict";
    
    /**
     * Screen Lock
     * 
     * @param {Object} element
     * @param {Object} options
     * @returns {ScreenLock}
     */
    var ScreenLock = function(element, options) {
        this.$element =
        this.element = 
        this.options = null;
        this.initialize(element, options);
    };
    
    
    /**
     * Modul Name
     *
     * @var {String}
     */
    ScreenLock.MODULE = "ScreenLock-Formular";

    /**
     * Modul Version
     *
     * @var {String}
     */
    ScreenLock.VERSION = "1.5.0";
    
    /**
     * Modul Namespace
     *
     * @var {String}
     */
    ScreenLock.NS = "acms.ui.widgets.screen-lock";
    
    
    /**
     * Komponenten Name
     * 
     * @var {String}
     */
    ScreenLock.MODULE = "Screen Lock";
    
    /**
     * Komponenten Version
     * 
     * @var {String}
     */
    ScreenLock.VERSION = "1.5.0";
    
    /**
     * Standard Optionen
     * 
     * @var {Object}
     */
    ScreenLock.DEFAULT_OPTIONS = {
        remote: null,
        locked: 'locked'
    };
    
    ScreenLock.needInit = true;
    
    ScreenLock.init = function(element, options) {
        var sl = new ScreenLock(element, options);
        sl.element.acmsData(ScreenLock.NS, sl);
    };
    
    ScreenLock.prototype = {
        constructor: ScreenLock,
        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            
        },
        
        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {ScreenLock.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return ScreenLock.DEFAULT_OPTIONS;
        },
        /**
         * Bildet die Optionen
         *
         * <p>
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         * </p>
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function (options) {
            return Utils.extend(
                    {},
                    this.getDefaultOptions(),
                    options,
                    this.element.acmsData()
                    );
        }
        
    }
    
    return ScreenLock;
});