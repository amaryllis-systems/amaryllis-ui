/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amaryllis-systems.de>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 */

/**
 * Up- und Down-Voting Modul
 * 
 * @param {Object} Utils
 * @param {Object} Selectors
 * @param {Object} Classes
 * @param {Object} AcmsEvent
 * @param {Object} A
 * @returns {VoteRating}
 */
define("ui/widgets/rating-vote", [
    'tools/utils',
    "core/selectors",
    "core/classes",
    "events/event",
    "core/acms"
], function (Utils, Selectors, Classes, AcmsEvent, A) {

    "use strict";

    /**
     * Constructor
     * 
     * @param {Element} element
     * @param {Object} options
     * @returns {VoteRating}
     */
    var VoteRating = function (element, options) {
        this.element =
        this.btnUp =
        this.btnDown = 
        this.disabled = 
        this.options = null;
        this.initialize(element, options);
    };
    
    /**
     * Modul Name
     * 
     * @type {String}
     */
    VoteRating.MODULE = "Up-Down-Rating";
    
    /**
     * Modul Version
     * 
     * @type {String}
     */
    VoteRating.VERSION = "1.5.0";
    
    /**
     * Modul Namespace
     * 
     * @type {String}
     */
    VoteRating.NS = "acms.ui.widgets.rating-vote";
        
    /**
     * Event vor dem Voting
     * 
     * @type {String}
     */
    VoteRating.EVENT_VOTING = VoteRating.NS + '.voting';
    
    /**
     * Event nach dem Voting
     * 
     * @type {String}
     */
    VoteRating.EVENT_VOTED = VoteRating.NS + '.voted';

    /**
     * Standard-Optionen
     *
     * @var {Object}
     */
    VoteRating.DEFAULT_OPTIONS = {
        remote: null,
        dirname: null,
        item: null,
        itemid: null,
        
        /**
         * Wird vor dem Voting ausgeführt
         * 
         * @param {Element} element Das Element
         * @param {type} rating     Die Wertung (1 bei Up-, -1 bei Down-Voting)
         * 
         * @returns {Boolean}
         */
        onVoting: function (element, rating) {
            return true;
        },
        /**
         * Wird nach erfolgreichem Voting ausgeführt
         * 
         * @param {Element} element Das Rating Element
         * @param {Object} rating   Das Response-Rating Object mit den Rating Informationen
         * 
         * 
         */
        onVoted: function (element, rating) {
        }
    };
    
    /**
     * CSS-Datei
     * 
     * @type {String}
     */
    VoteRating.css = "media/css/widgets/starrating.min.css";

    /**
     * Module Loader Einbindung
     * 
     * @type {Boolean}
     */
    VoteRating.needInit = true;

    /**
     * Modul Initialisierung
     * 
     * @param {Element} element
     * @param {Object} options
     * 
     * @returns {VoteRating}
     */
    VoteRating.init = function (element, options) {
        var rate = new VoteRating(element, options);
        rate.element.acmsData(VoteRating.NS, rate);

        return rate;
    };

    VoteRating.prototype = {
        constructor: VoteRating,
        
        /**
         * Initerner Constructor
         * 
         * @param {Element} element
         * @param {Object} options
         * 
         * 
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },
        
        /**
         * Ausführen des Up-Votings
         * 
         * @returns {VoteRating._onVoteDown}
         */
        voteUp: function() {
            return this._onVoteUp();
        },
        
        /**
         * Ausführen des Down-Votings
         * 
         * @returns {VoteRating._onVoteDown}
         */
        voteDown: function() {
            return this._onVoteDown();
        },
        
        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {VoteRating.DEFAULT_OPTIONS|Object}
         */
        getDefaultOptions: function () {
            return VoteRating.DEFAULT_OPTIONS;
        },
        
        /**
         * Bildet die neuen Optionen
         *
         * @param {Object} options
         * 
         */
        buildOptions: function (options) {
            return Utils.extend(
                    {},
                    this.getDefaultOptions(),
                    options,
                    this.element.acmsData());
        },
        
        /**
         * Aufsetzen der Event-Listener
         * 
         * 
         */
        listen: function () {
            var self = this, ele = this.$element, o = this.options;
            if(self.btnUp) {
                AcmsEvent.add(self.btnUp, 'click', self._onVoteUp);
            }
            if(self.btnDown) {
                AcmsEvent.add(self.btnDown, 'click', self._onVoteDown);
            }
        },
        
        /**
         * Ausführen des Up-Voting
         * 
         * @param {MouseEvent|null} e   Das MouseEvent, wenn auf Button-Click
         * 
         * @returns {Boolean}           Immer FALSE wegen alten IE
         */
        _onVoteUp: function(e) {
            e && e.preventDefault();
            var self = this,
                o = self.options,
                fData = self._buildFormData("voteup");
            self._hide();
            self._trigger(VoteRating.EVENT_VOTING, 'onVote', 1);
            self._process(fData);
            
            return false;
        },
        
        /**
         * Ausführen des Down-Voting
         * 
         * @param {MouseEvent|null} e   Das MouseEvent, wenn auf Button-Click
         * 
         * @returns {Boolean}           Immer FALSE wegen alten IE
         */
        _onVoteDown: function(e) {
            e && e.preventDefault();
            var self = this,
                o = self.options,
                fData = self._buildFormData("votedown");
            self._hide();
            self._trigger(VoteRating.EVENT_VOTING, 'onVote', -1);
            self._process(fData);
            
            return false;
        },
        
        /**
         * Durchführen der Wertung im Backend
         * 
         * @param {FormData} fData  Die Form Daten
         * 
         * 
         */
        _process: function(fData) {
            var self = this,
                o = self.options;
            require(['http/request', 'notifier'], function(Request) {
                Request.post(o.remote, fData, self._done);
            });
        },
        
        /**
         * Ausführen von Events
         * 
         * @param {String}          name        Der Event-Name
         * @param {String}          onName      Der "on" Event Name für die 
         *                                      Methoden der Optionen.
         * @param {Integer|Object}  rating      Das Rating oder die Rating 
         *                                      Response.
         * 
         * @returns {window.CustomEvent}
         */
        _trigger: function(name, onName, rating) {
            var self = this,
                o = self.options;
            if(typeof o[onName] === 'function') {
                o[onName](element, rating);
            }
            var e = AcmsEvent.createCustom(name, {rating: rating});
            AcmsEvent.dispatch(self.element, e);
            
            return e;
        },
        
        /**
         * Ausführung nach Server-Antwort
         * 
         * @param {Object} response Die Server-Response
         * 
         * 
         */
        _done: function(response) {
            var self = this;
            A.Notifications.createFromResponse(response);
            if(response.status === 'success') {
                self.element.parentNode.removeChild(self.element);
                self._trigger(VoteRating.EVENT_VOTED, 'onVoted', response.rating);
            }
        },
        
        _hide: function() {
            Classes.addClass(this.element, 'hidden');
        },
        
        /**
         * Bilden der Formular-Daten
         * 
         * @param {String} op   Die Vote-Operation
         * @returns {FormData}
         */
        _buildFormData: function(op) {
            var self = this, 
                o = self.options, 
                fData = new FormData();
            fData.append('module', o.dirname);
            fData.append('item', o.item);
            fData.append('itemid', o.itemid);
            fData.append('op', op);
            fData.append('rate', (op === "voteup" ? 1 : -1));
            
            return fData;
        },
        
        /**
         * Vorbereiten des Moduls
         * 
         * 
         */
        _prepare: function() {
            var self = this, o = self.options, element = self.element;
            self.btnUp = Selectors.q('[data-trigger="voteup"]');
            self.btnDown = Selectors.q('[data-trigger="votedown"]');
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        }
    };

    return VoteRating;

});
