/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('ui/tools/scroll-animate', [
    'tools/utils',
    'core/selectors',
    'core/classes',
    'core/window',
    'core/mutation-observer',
    'core/weak-map',
    'events/event',
    'core/acms'
], function (Utils, Selectors, Classes, Win, MutationObserver, WeakMap, AcmsEvent, A) {

    "use strict";

    var indexOf = [].indexOf || function (item) {
            for (var i = 0, l = this.length; i < l; i++) {
                if (i in this && this[i] === item)
                    return i;
            }
            return -1;
        },
        getComputedStyle = window.getComputedStyle || function (el, pseudo) {
            this.getPropertyValue = function (prop) {
                var ref;
                if (prop === 'float') {
                    prop = 'styleFloat';
                }
                if (getComputedStyleRX.test(prop)) {
                    prop.replace(getComputedStyleRX, function (_, _char) {
                        return _char.toUpperCase();
                    });
                }
                return ((ref = el.currentStyle) != null ? ref[prop] : void 0) || null;
            };
            return this;
        },
        getComputedStyleRX = /(\-([a-z]){1})/g;
    

    /**
     * Constructor
     * 
     * @param {Element}     element     Das HTML-Element
     * @param {Object}      options     Die Optionen
     * @returns {ScrollAnimations}
     */
    var ScrollAnimations = function (element, options) {
        this.element =
                this.options =
                this.toggles = null;
        this.options = ScrollAnimations.DEFAULT_OPTIONS;

        this.initialize(element, options);
    };

    /**
     * Modul Name
     * 
     * @var {String}
     */
    ScrollAnimations.MODULE = "Scroll Animation Listener";

    /**
     * Modul Namespace
     * 
     * @var {String}
     */
    ScrollAnimations.NS = "acms.ui.tools.scroll-animate";

    /**
     * Standard-Optionen des Moduls
     * 
     * @var {Object}
     */
    ScrollAnimations.DEFAULT_OPTIONS = {
        boxClass: 'animation-observer',
        animateClass: 'animated',
        offset: 0,
        mobile: true,
        live: true,
        onShow: function (ele, self) {},
        scrollContainer: null
    };

    /**
     * Sagt dem Modul-Loader, dass das Modul initialisiert werden muss, wenn es 
     * über diesen geladen wird.
     * 
     * @var {Boolean}
     */
    ScrollAnimations.needInit = true;

    /**
     * Modul Initialisierung
     * 
     * @param {Element}     element     Das HTML-Element
     * @param {Object}      options     Die Optionen
     * @returns {ScrollAnimations}
     */
    ScrollAnimations.init = function (element, options) {
        var v = new ScrollAnimations(element, options);
        v.element.acmsData(ScrollAnimations.NS, v);

        return v;
    };

    /**
     * Klassen Definition
     */
    ScrollAnimations.prototype = {

        vendors: ["moz", "webkit"],

        constructor: ScrollAnimations,

        /**
         * Interner Constructor
         * 
         * @param {Element} element     HMTL Element
         * @param {Objct}   options     Eigene Optionen
         * 
         * 
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this.scrolled = true;
            if (this.options.scrollContainer !== null) {
                this.options.scrollContainer = document.querySelector(this.options.scrollContainer);
            }
            this.animationNameCache = new WeakMap();
            this.mEvent = AcmsEvent.createEvent(this.options.boxClass);

            this._prepare();
            this.listen();
        },

        /**
         * Aufsetzen der Event-Listener
         * 
         */
        listen: function () {
            var self = this,
                    o = self.options,
                    element = self.element;
        },

        /**
         * Gibt die Standard-Optionen zurück
         * 
         * @returns {ScrollAnimations.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function () {
            return ScrollAnimations.DEFAULT_OPTIONS;
        },

        /**
         * Bilden der Optionen
         * 
         * Bildet die Optionen basierend auf den Standard-Optionen, den 
         * übergebenen Optionen des Constructors und den Element Data-Attributen. 
         * Letztere gewinnen den Kampf, wenn gesetzt.
         * 
         * @param {Object} options    Die eignenen Optionen
         * @returns {Object|ScrollAnimations.DEFAULT_OPTIONS}
         */
        buildOptions: function (options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },

        start: function () {
            var self = this, element = self.element, o = self.options, box, j, len, ref;
            self.stopped = false;
            var results;
            ref = Selectors.qa("." + o.boxClass, element);
            results = [];
            for (j = 0, len = ref.length; j < len; j++) {
                box = ref[j];
                results.push(box);
            }
            self.boxes = results;
            var l, k, leng, b, results2;
            b = self.boxes;
            l = b.length;
            results2 = [];
            for (k = 0, l; k < l; k++) {
                box = b[k];
                results2.push(box);
            }
            self.all = results2;
            if (l) {
                if (self.disabled()) {
                    self.resetStyle();
                } else {
                    ref = self.boxes;
                    for (j = 0, leng = ref.length; j < leng; j++) {
                        box = ref[j];
                        self.applyStyle(box, true);
                    }
                }
            }
            if (!self.disabled()) {
                AcmsEvent.add(o.scrollContainer || window, 'scroll', self._scrollHandler);
                AcmsEvent.add(window, 'resize', self._scrollHandler);
                self.interval = setInterval(self._scrollCallback, 50);
            }
            if (o.live) {
                self.mo = new MutationObserver((function (_this) {
                    return function (records) {
                        var k, len1, node, record, results;
                        results = [];
                        for (k = 0, len1 = records.length; k < len1; k++) {
                            record = records[k];
                            results.push((function () {
                                var l, len2, ref1, results1;
                                ref1 = record.addedNodes || [];
                                results1 = [];
                                for (l = 0, len2 = ref1.length; l < len2; l++) {
                                    node = ref1[l];
                                    results1.push(this.doSync(node));
                                }
                                return results1;
                            }).call(_this));
                        }
                        return results;
                    };
                })(this)).observe(document.body, {
                    childList: true,
                    subtree: true
                });
            }
        },

        stop: function () {
            var self = this, o = this.options;
            self.stopped = true;
            AcmsEvent.remove(o.scrollContainer || window, 'scroll', self._scrollHandler);
            AcmsEvent.remove(window, 'resize', self._scrollHandler);
            if (self.interval !== null) {
                return clearInterval(self.interval);
            }
        },

        disabled: function () {
            return !this.options.mobile && Win.AmaryllisCMS.device.isMobileDevice;
        },

        cacheAnimationName: function (box) {
            return this.animationNameCache.set(box, this.animationName(box));
        },

        cachedAnimationName: function (box) {
            return this.animationNameCache.get(box);
        },

        offsetTop: function (element) {
            var top;
            while (element.offsetTop === void 0) {
                element = element.parentNode;
            }
            top = element.offsetTop;
            while (element = element.offsetParent) {
                top += element.offsetTop;
            }
            return top;
        },

        show: function (box) {
            var self = this, o = self.options;
            self.applyStyle(box);
            Classes.addClass(box, o.animateClass);
            if (typeof o.onShow === 'function') {
                o.onShow(box, self);
            }
            AcmsEvent.dispatch(box, this.mEvent);
            AcmsEvent.add(box, 'animationend', self._resetAnimation);
            AcmsEvent.add(box, 'oanimationend', self._resetAnimation);
            AcmsEvent.add(box, 'webkitAnimationEnd', self._resetAnimation);
            AcmsEvent.add(box, 'MSAnimationEnd', self._resetAnimation);

            return box;
        },

        applyStyle: function (box, hidden) {
            var delay, duration, iteration;
            duration = box.acmsData('animation-duration');
            delay = box.acmsData('animation-delay');
            iteration = box.acmsData('animation-iteration');
            return this.animate((function (_this) {
                return function () {
                    return _this.customStyle(box, hidden, duration, delay, iteration);
                };
            })(this));
        },

        animate: (function () {
            if ('requestAnimationFrame' in window) {
                return function (callback) {
                    return window.requestAnimationFrame(callback);
                };
            } else {
                return function (callback) {
                    return callback();
                };
            }
        })(),

        resetStyle: function () {
            var box, j, len, ref, results;
            ref = this.boxes;
            results = [];
            for (j = 0, len = ref.length; j < len; j++) {
                box = ref[j];
                results.push(box.style.visibility = 'visible');
            }
            return results;
        },

        customStyle: function (box, hidden, duration, delay, iteration) {
            if (hidden) {
                this.cacheAnimationName(box);
            }
            box.style.visibility = hidden ? 'hidden' : 'visible';
            if (duration) {
                this.vendorSet(box.style, {
                    animationDuration: duration
                });
            }
            if (delay) {
                this.vendorSet(box.style, {
                    animationDelay: delay
                });
            }
            if (iteration) {
                this.vendorSet(box.style, {
                    animationIterationCount: iteration
                });
            }
            this.vendorSet(box.style, {
                animationName: hidden ? 'none' : this.cachedAnimationName(box)
            });
            return box;
        },

        vendorSet: function (elem, properties) {
            var name, results, value, vendor;
            results = [];
            for (name in properties) {
                value = properties[name];
                elem["" + name] = value;
                results.push((function () {
                    var j, len, ref, results1;
                    ref = this.vendors;
                    results1 = [];
                    for (j = 0, len = ref.length; j < len; j++) {
                        vendor = ref[j];
                        results1.push(elem["" + vendor + (name.charAt(0).toUpperCase()) + (name.substr(1))] = value);
                    }
                    return results1;
                }).call(this));
            }
            return results;
        },

        vendorCSS: function (elem, property) {
            var j, len, ref, result, style, vendor;
            style = getComputedStyle(elem);
            result = style.getPropertyCSSValue(property);
            ref = this.vendors;
            for (j = 0, len = ref.length; j < len; j++) {
                vendor = ref[j];
                result = result || style.getPropertyCSSValue("-" + vendor + "-" + property);
            }
            return result;
        },

        animationName: function (box) {
            var animationName;
            try {
                animationName = this.vendorCSS(box, 'animation-name').cssText;
            } catch (_error) {
                animationName = getComputedStyle(box).getPropertyValue('animation-name');
            }
            if (animationName === 'none') {
                return '';
            } else {
                return animationName;
            }
        },

        isScrollAnimationsible: function (box) {
            var bottom, offset, top, viewBottom, viewTop;
            offset = box.acmsData('animation-offset') || this.options.offset;
            viewTop = (this.options.scrollContainer && this.options.scrollContainer.scrollTop) || window.pageYOffset;
            viewBottom = viewTop + Math.min(this.element.clientHeight, this.util().innerHeight()) - offset;
            top = this.offsetTop(box);
            bottom = top + box.clientHeight;
            return top <= viewBottom && bottom >= viewTop;
        },

        sync: function (element) {
            if (MutationObserver.notSupported) {
                return this.doSync(this.element);
            }
        },

        doSync: function (element) {
            var box, j, len, ref, results;
            if (element == null) {
                element = this.element;
            }
            if (element.nodeType !== 1) {
                return;
            }
            element = element.parentNode || element;
            ref = element.querySelectorAll("." + this.config.boxClass);
            results = [];
            for (j = 0, len = ref.length; j < len; j++) {
                box = ref[j];
                if (indexOf.call(this.all, box) < 0) {
                    this.boxes.push(box);
                    this.all.push(box);
                    if (this.stopped || this.disabled()) {
                        this.resetStyle();
                    } else {
                        this.applyStyle(box, true);
                    }
                    results.push(this.scrolled = true);
                } else {
                    results.push(void 0);
                }
            }
            return results;
        },

        _resetAnimation: function (event) {
            var target, self = this, o = self.options;
            if (event.type.toLowerCase().indexOf('animationend') >= 0) {
                target = event.target || event.srcElement;
                Classes.removeClass(target, o.animateClass);
            }
        },

        _scrollHandler: function () {
            return this.scrolled = true;
        },

        _scrollCallback: function () {
            var box, self = this;
            if (self.scrolled) {
                self.scrolled = false;
                self.boxes = (function () {
                    var j, len, ref, results;
                    ref = self.boxes;
                    results = [];
                    for (j = 0, len = ref.length; j < len; j++) {
                        box = ref[j];
                        if (!(box)) {
                            continue;
                        }
                        if (self.isScrollAnimationsible(box)) {
                            self.show(box);
                            continue;
                        }
                        results.push(box);
                    }
                    return results;
                }).call(this);
                if (!(self.boxes.length || self.options.live)) {
                    return self.stop();
                }
            }
        },

        /**
         * Vorbereiten des Moduls
         * 
         * 
         */
        _prepare: function () {
            var self = this,
                    element = self.element,
                    o = self.options;
            var ref;
            if ((ref = document.readyState) === "interactive" || ref === "complete") {
                this.start();
            } else {
                AcmsEvent.add(document, 'DOMContentLoaded', self.start.bind(self));
            }

            self.finished = [];
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        }

    };

    return ScrollAnimations;

});
