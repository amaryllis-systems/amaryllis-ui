/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('ui/tools/drag-n-drop', [], function() {
    "use strict";
    
    var DRAGGABLE_DATA = "acms.ui.tools.draggable",
        NS = "." + DRAGGABLE_DATA,
        EVENT_START = "start" + NS
    ;
    
    var hasTouch = Win.AmaryllisCMS.support.touch;

    var hasPointerEvents = (function() {
        var el    = Doc.createElement('div'),
            docEl = Doc.documentElement;
        if (!('pointerEvents' in el.style)) {
            return false;
        }
        el.style.pointerEvents = 'auto';
        el.style.pointerEvents = 'x';
        docEl.appendChild(el);
        var supports = Win.getComputedStyle && Win.getComputedStyle(el, '').pointerEvents === 'auto';
        docEl.removeChild(el);
        return !!supports;
    })();
    

    var Draggable = function(element, options) {
        this.$element   =
        this.element    =
        this.options    =
        this.$handle    = null;
        this.initiaize(element, options);
    };
    
    Draggable.MODULE = "Draggable";
    
    Draggable.VERSION = "1.5.0";

    Draggable.DEFAULT_OPTIONS = {
        handle: '.handle',
        nodeList        : 'ol',
        nodeItem        : 'li',
        classDrag       : 'drag',
        
        classRoot       : 'draggable',
        classList       : 'draggable-list',
        classItem       : 'draggable-item',
        classHandle     : 'draggable-handle',
        classCollapsed  : 'draggable-collapsed',
        classPlace      : 'draggable-placeholder',
        classNodrag     : 'draggable-nodrag',
        classEmpty      : 'draggable-empty',
    };

    Draggable.needInit = true;

    Draggable.init = function(element, options) {
        var drag = new Draggable(element, options);
        drag.element.acmsData(DRAGGABLE_DATA, drag);
        
        return drag;
    };

    Draggable.prototype = {
        constructor: Draggable,
        /**
         * @var Draggable.DEFAULT_OPTIONS
         */
        options: null,
        
        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this.reset();
            this.listen();
        },
        
        /**
         * Gibt die Standard Optionen zurück
         * @returns {Draggable.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function() {
            return Draggable.DEFAULT_OPTIONS;
        },

        /**
         * Bildet die Optionen
         *
         * Basierend auf den Standard-Optionen, den übergebenen Optionen des
         * Constructors und den Element Data-Attributen werden die aktuellen
         * Konfigurationen gebildet. Letztenendes gewinnen die Element
         * Data-Attribute.
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function(options) {
            return Utils.extend({},
                        this.getDefaultOptions(),
                        options,
                        this.element.acmsData());
        },
        
        /**
         * Setter/Getter für die Optionen
         *
         * @param {String|NULL} a   Der Optionen-Schlüssel oder NULL
         * @param {Mixed}       b   Undefined oder der Wert für a
         * @returns {Nestable.options|Boolean|Mixed|Nestable.prototype}
         */
        option: function (a, b) {
            if (!a) {
                return this.options;
            }
            if (typeof b === "undefined") {
                return this.options[a] || false;
            }
            this.options[a] = b;
            return this;
        },
        
        dragStart: function(e) {
            var self = this,
                o = this.options,
                mouse    = this.mouse,
                target   = e.target,
                dragItem = target.closest(o.nodeItem),
                tOffset = target.offset();

            this.placeEl.css('height', dragItem.height());
            
            mouse.offsetX = e.offsetX !== undefined ? e.offsetX : e.pageX - tOffset.left;
            mouse.offsetY = e.offsetY !== undefined ? e.offsetY : e.pageY - tOffset.top;
            mouse.startX = mouse.lastX = e.pageX;
            mouse.startY = mouse.lastY = e.pageY;

            this.dragRootEl = this.element;

            this.dragEl = Doc.createElement(o.nodeList);
            Classes.addClass(this.dragEl, o.classDrag);
            this.dragEl.width(dragItem.width());
            
            dragItem.parentNode.insertBefore(this.placeEl, dragItem.nextSibling);
            dragItem.parentNode.removeChild(dragItem);
            this.dragEl.appendChild(dragItem);
            
            Doc.body.appendChild(this.dragEl);
            this.dragEl.css({
                'left' : e.pageX - mouse.offsetX,
                'top'  : e.pageY - mouse.offsetY
            });
            var i, depth,
                items = Selectors.qa(o.nodeItem, this.dragEl);
            for (i = 0; i < items.length; i++) {
                depth = items[i].closest(o.nodeList).length;
                if (depth > this.dragDepth) {
                    this.dragDepth = depth;
                }
            }
        },

        dragStop: function(e) {
            var el = this.dragEl.querySelector(this.options.nodeItem + ":first-of-type")[0];
            el.parentNode.removeChild(el);
            this.placeEl.replaceWith(el);

            this.dragEl.remove();
            var event = AcmsEvent.create('change');
            AcmsEvent.dispatch(this.element, event);
            if (this.hasNewRoot) {
                AcmsEvent.dispatch(this.dragRootEl, event);
            }
            this.reset();
        },
        
        dragMove: function(e) {
            var list, parent, prev, next, depth,
                o   = this.options,
                mouse = this.mouse;

            this.dragEl.css({
                'left' : e.pageX - mouse.offsetX,
                'top'  : e.pageY - mouse.offsetY
            });

            mouse.lastX = mouse.nowX;
            mouse.lastY = mouse.nowY;
            mouse.nowX  = e.pageX;
            mouse.nowY  = e.pageY;
            mouse.distX = mouse.nowX - mouse.lastX;
            mouse.distY = mouse.nowY - mouse.lastY;
            mouse.lastDirX = mouse.dirX;
            mouse.lastDirY = mouse.dirY;
            mouse.dirX = mouse.distX === 0 ? 0 : mouse.distX > 0 ? 1 : -1;
            mouse.dirY = mouse.distY === 0 ? 0 : mouse.distY > 0 ? 1 : -1;
            var newAx   = Math.abs(mouse.distX) > Math.abs(mouse.distY) ? 1 : 0;
            if (!mouse.moving) {
                mouse.dirAx  = newAx;
                mouse.moving = true;
                return;
            }
            if (mouse.dirAx !== newAx) {
                mouse.distAxX = 0;
                mouse.distAxY = 0;
            } else {
                mouse.distAxX += Math.abs(mouse.distX);
                if (mouse.dirX !== 0 && mouse.dirX !== mouse.lastDirX) {
                    mouse.distAxX = 0;
                }
                mouse.distAxY += Math.abs(mouse.distY);
                if (mouse.dirY !== 0 && mouse.dirY !== mouse.lastDirY) {
                    mouse.distAxY = 0;
                }
            }
            mouse.dirAx = newAx;
            if (mouse.dirAx && mouse.distAxX >= o.threshold) {
                mouse.distAxX = 0;
                prev = Selectors.previous(this.placeEl, o.nodeItem);
                if (mouse.distX > 0 && prev.length && !Classes.hasClass(prev, o.classCollapsed)) {
                    // eq(-1) => last()
                    var lists = Selectors.qa(o.nodeList, prev);
                    list = Utils.last(lists);
                    depth = Core.$(this.placeEl).parents(o.nodeList).length;
                    if (depth + this.dragDepth <= o.maxDepth) {
                        if (!list.length) {
                            list = Core.$('<' + o.nodeList + '/>').addClass(o.classList);
                            list.append(this.placeEl);
                            Core.$(prev).append(list);
                            this.setParent(prev);
                        } else {
                            list = prev.children(o.nodeList).last();
                            list.append(this.placeEl);
                        }
                    }
                }
                if (mouse.distX < 0) {
                    next = this.placeEl.next(o.nodeItem);
                    if (!next.length) {
                        parent = this.placeEl.parent();
                        this.placeEl.closest(o.nodeItem).after(this.placeEl);
                        if (!parent.children().length) {
                            this.unsetParent(parent.parent());
                        }
                    }
                }
            }

            var isEmpty = false;
            if (!hasPointerEvents) {
                this.dragEl[0].style.visibility = 'hidden';
            }
            this.pointEl = Core.$(document.elementFromPoint(e.pageX - document.body.scrollLeft, e.pageY - (window.pageYOffset || document.documentElement.scrollTop)));
            if (!hasPointerEvents) {
                this.dragEl[0].style.visibility = 'visible';
            }
            if (this.pointEl.hasClass(o.classHandle)) {
                this.pointEl = this.pointEl.parent(o.nodeItem);
            }
            if (this.pointEl.hasClass(o.classEmpty)) {
                isEmpty = true;
            }
            else if (!this.pointEl.length || !this.pointEl.hasClass(o.classItem)) {
                return;
            }
            var pointElRoot = this.pointEl.closest('.' + o.classRoot),
                isNewRoot   = this.dragRootEl.data('nestable-id') !== pointElRoot.data('nestable-id');
            if (!mouse.dirAx || isNewRoot || isEmpty) {
                if (isNewRoot && o.group !== pointElRoot.data('nestable-group')) {
                    return;
                }
                depth = this.dragDepth - 1 + this.pointEl.parents(o.nodeList).length;
                if (depth > o.maxDepth) {
                    return;
                }
                var before = e.pageY < (this.pointEl.offset().top + this.pointEl.height() / 2);
                    parent = this.placeEl.parent();
                if (isEmpty) {
                    list = Core.$(document.createElement(o.nodeList)).addClass(o.classList);
                    list.append(this.placeEl);
                    this.pointEl.replaceWith(list);
                }
                else if (before) {
                    this.pointEl.before(this.placeEl);
                }
                else {
                    this.pointEl.after(this.placeEl);
                }
                if (!parent.children().length) {
                    this.unsetParent(parent.parent());
                }
                if (!this.dragRootEl.find(o.nodeItem).length) {
                    this.dragRootEl.append('<div class="' + o.classEmpty + '"/>');
                }
                if (isNewRoot) {
                    this.dragRootEl = pointElRoot;
                    this.hasNewRoot = this.el[0] !== this.dragRootEl[0];
                }
            }
        },
        
        setParent: function(li) {
            if (li.children(this.options.nodeList).length) {
                li.prepend(Core.$(this.options.expandBtnHTML));
                li.prepend(Core.$(this.options.collapseBtnHTML));
            }
            li.children('[data-action="expand"]').hide();
        },

        unsetParent: function(li) {
            li.removeClass(this.options.classCollapsed);
            li.children('[data-action]').remove();
            li.children(this.options.nodeList).remove();
        },
        
        reset: function() {
            this.mouse = {
                offsetX   : 0,
                offsetY   : 0,
                startX    : 0,
                startY    : 0,
                lastX     : 0,
                lastY     : 0,
                nowX      : 0,
                nowY      : 0,
                distX     : 0,
                distY     : 0,
                dirAx     : 0,
                dirX      : 0,
                dirY      : 0,
                lastDirX  : 0,
                lastDirY  : 0,
                distAxX   : 0,
                distAxY   : 0
            };
            this.isTouch    = false;
            this.moving     = false;
            this.dragEl     = null;
            this.dragRootEl = null;
            this.dragDepth  = 0;
            this.hasNewRoot = false;
            this.pointEl    = null;
        },
        
        listen: function() {
            var self = this, o = this.options;
            var onStartEvent = function(e) {
                var handle = e.target;
                if (! Classes.hasClass(handle, o.classHandle)) {
                    if (handle.closest('.' + o.classNodrag).length) {
                        return;
                    }
                    handle = handle.closest('.' + o.classHandle);
                }
                if (!handle.length || o.dragEl) {
                    return;
                }
                self.isTouch = /^touch/.test(e.type);
                if (self.isTouch && e.touches.length !== 1) {
                    return;
                }
                e.preventDefault();
                self.dragStart(e.touches ? e.touches[0] : e);
            };

            var onMoveEvent = function(e) {
                if (self.dragEl) {
                    e.preventDefault();
                    self.dragMove(e.touches ? e.touches[0] : e);
                }
            };

            var onEndEvent = function(e) {
                if (self.dragEl) {
                    e.preventDefault();
                    self.dragStop(e.touches ? e.touches[0] : e);
                }
            };

            if (hasTouch) {
                AcmsEvent.add(self.element, 'touchstart', onStartEvent);
                AcmsEvent.add(window,'touchmove', onMoveEvent);
                AcmsEvent.add(window,'touchend', onEndEvent);
                AcmsEvent.add(window,'touchcancel', onEndEvent);
            }
            AcmsEvent.add(self.element,'mousedown', onStartEvent);
            AcmsEvent.add(self.element,'mousemove', onMoveEvent);
            AcmsEvent.add(self.element,'mouseup', onEndEvent);
        }

    };

    return Draggable;

});
