/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('ui/tools/disallowable', [
    'tools/utils',
    'events/event',
    'core/classes'
], function(Utils, AcmsEvent, Classes) {
    
    "use strict";
    
    /**
     * verbietbares Element
     * 
     * Ein einfacher Warapper um das disbaled Property zu toggeln 
     * gemeinsam mit arai-disabled und der CSS-Klasse disabled.
     * 
     * @param {HTMLElement} element 
     * @param {Object|null} options 
     */
    var Disallowable = function(element, options) {
        this.element =
        this.disabled =
        this.options = null;
        this.initialize(element, options);
    };

    Disallowable.MODULE = "Disallowable";

    Disallowable.NS = "acms.ui.tools.disallowable";

    Disallowable.EVENT_ENABLE = Disallowable.NS + ".enable";

    Disallowable.EVENT_ENABLED = Disallowable.NS + ".enabled";

    Disallowable.EVENT_DISABLE = Disallowable.NS + ".disable";

    Disallowable.EVENT_DISABLED = Disallowable.NS + ".disabled";

    Disallowable.DEFAULT_OPTIONS = {
        beforeDisable: function() {return true;},
        onDisable: function() {},
        beforeEnable: function() {return true;},
        onEnable: function() {}
    };
    
    Disallowable.prototype = {
        
        constructor: Disallowable,

        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
        },

        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {Disallowable.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function() {
            return Disallowable.DEFAULT_OPTIONS;
        },

        /**
         * Bildet die Optionen
         *
         * Basierend auf den Standard-Optionen, den übergebenen Optionen des
         * Constructors und den Element Data-Attributen werden die aktuellen
         * Konfigurationen gebildet. Letztenendes gewinnen die Element
         * Data-Attribute.
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function(options) {
            return Utils.extend(
                            {},
                            this.getDefaultOptions(),
                            options,
                            this.element.acmsData()
                        );
        },
        
        listen: function() {
            var self = this;
            AcmsEvent.add(self.element, 'click', self._onClick);
        },

        off: function() {
            var self = this;
            AcmsEvent.remove(self.element, 'click', self._onClick);
        },

        isDisabled: function() {
            return (this.disabled === true);
        },
        
        toggle: function() {
            if(true === this.disabled) {
                return this.enable();
            } else {
                return this.disable();
            }
        },
        
        /**
         * Reaktiviert ein deaktiviertes Element
         * 
         * 
         */
        enable: function() {
            if(false === this.disabled) {
                return;
            }
            var e = AcmsEvent.create(Disallowable.EVENT_ENABLE);
            AcmsEvent.dispatch(this.element, e);
            if(e.defaultPrevented) {
                return;
            }
            if(typeof o.beforeEnable === 'function') {
                var res = o.beforeEnable();
                if(true !== res) {
                    return false;
                }
            }
            this.disabled = false;
            var element = this.element, o = this.options;
            element.setAttribute('aria-disabled', 'false');
            Classes.removeClass(element, 'disabled');
            element.setAttribute('disabled', false);
            if(typeof o.onEnable === 'function') {
                o.onEnable();
            }
            var e2 = AcmsEvent.create(Disallowable.EVENT_ENABLED);
            AcmsEvent.dispatch(element, e2);
        },

        /**
         * Deaktiviert/Verbietet ein Element
         *
         * 
         */
        disable: function() {
            if(true === this.disabled) {
                return;
            }
            var self = this, element = self.element, o = self.options;
            var e = AcmsEvent.create(Disallowable.EVENT_DISABLE);
            AcmsEvent.dispatch(element, e);
            if(e.defaultPrevented) {
                return;
            }
            if(typeof o.beforeDisable === 'function') {
                var res = o.beforeDisable();
                if(true !== res) {
                    return false;
                }
            }
            this.disabled = true;
            element.setAttribute('aria-disabled', 'true');
            Classes.addClass(element, 'disabled');
            if(element.tagName.toLowerCase() === 'button') {
                element.setAttribute('disabled', 'disabled');
            }
            if(typeof o.onDisable === 'function') {
                o.onDisable();
            }
            var e2 = AcmsEvent.create(Disallowable.EVENT_DISABLED);
            AcmsEvent.dispatch(element, e2);
        },

        _onClick: function(e) {
            var target = e.target || self.element;
            if(this.disabled || 
                (target.hasAttribute('aria-diabled') && 
                    target.getAttribute('aria-diabled') == 'true')) {
                e.preventDefault();
                e.stopPropagation();
                return false;
            }
        },
        
        _prepare: function() {
            var self = this, element = self.element;
            if(Classes.hasClass(element, 'disabled') ||
                element.getAttribute('aria-disabled') === 'true') {
                self.disable();
            } else {
                self.disabled = false;
            }
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        }
    };

    return Disallowable;

});
