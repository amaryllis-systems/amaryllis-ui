/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('ui/tools/draggable', [
    'tools/utils',
    'core/document',
    'core/window',
    'core/classes',
    'core/selectors',
    'core/style',
    'events/event'
], function (Utils, Doc, Win, Classes, Selectors, Style, AcmsEvent) {

    'use strict';
    
    var DRAGGABLE_DATA = "acms.ui.tools.draggable",
        NS = "." + DRAGGABLE_DATA,
        EVENT_START = "start" + NS
    ;
    
    var hasTouch = Win.AmaryllisCMS.support.touch;

    var hasPointerEvents = (function() {
        var el    = Doc.createElement('div'),
            docEl = Doc.documentElement;
        if (!('pointerEvents' in el.style)) {
            return false;
        }
        el.style.pointerEvents = 'auto';
        el.style.pointerEvents = 'x';
        docEl.appendChild(el);
        var supports = Win.getComputedStyle && Win.getComputedStyle(el, '').pointerEvents === 'auto';
        docEl.removeChild(el);
        return !!supports;
    })();
    
    Draggable = function (element, options) {
        this.element =
        this.options =
        this.dragState =
        this.dragX = 
        this.dragY = 
        this.handle = null;
        this.initialize(element, options);
    };
    
    Draggable.MODULE = "Draggable";
    
    Draggable.DEFAULT_OPTIONS = {
        handle: '.draggable-handle'
    };
    
    Draggable.needInit = true;

    Draggable.init = function(element, options) {
        var drag = new Draggable(element, options);
        drag.element.acmsData(DRAGGABLE_DATA, drag);
        
        return drag;
    };


    Draggable.prototype = {
        constructor: Draggable,
        
        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            //_draggable(element, this.handle);
            this.listen();
        },
        
        listen: function() {
            var self = this, handle = self.handle;
            if(hasTouch) {
                AcmsEvent.add(handle, "touchstart", self._onDragStart.bind(self));
                AcmsEvent.add(document, "touchmove", self._onDragMove.bind(self));
                AcmsEvent.add(document, "touchend", self._onDragStop.bind(self));
            }
            AcmsEvent.add(handle, "mousedown", self._onDragStart.bind(self));
            AcmsEvent.add(document, "mousemove", self._onDragMove.bind(self));
            AcmsEvent.add(document, "mouseup", self._onDragStop.bind(self));
            
        },
        
        _onDragStart: function(e) {
            var self = this, 
                element = self.element,
                rect = element.getBoundingClientRect();
            rect = {top:element.offsetTop,left:element.offsetLeft};
            self.dragState = true;
            element.style.position = 'fixed';
            self.dragX = rect.leftM; //e.clientX - rect.left;
            self.dragY = rect.top; //e.clientY - rect.top;
            element.lastXPosition = e.clientX;
            element.lastYPosition = e.clientY;
            Classes.addClass(element, 'dragging');
            
            element.style.top = element.offsetTop + 'px';
            element.style.left = element.offsetLeft + 'px';
            var style = element.getAttribute('style'), styles = style.split(';'), nStyles = '', prev = {'transition': '', 'bottom': 'auto', 'right': 'auto'};
            for(var j = 0; j < styles.length; j++) {
                var s = styles[j].split(':'), prop = s[0].trim();
                if(prop in prev) {
                    element.style[prop] = '';
                    if(prop !== 'transition') {
                        nStyles += prop+': ' + prev[prop] + '; ';
                    }
                    continue;
                } else if(prop === 'height') {
                    nStyles += 'height: ' + element.offsetHeight+'px; ';
                } else if(prop === 'width') {
                    nStyles += 'width: ' + element.offsetWidth+'px; ';
                } else {
                    nStyles += s[0]+':'+s[1]+'; ';
                }
            }
            element.setAttribute('style', nStyles);
            
        },
        
        _onDragMove: function(e) {
            var self = this, element = self.element;
            if (self.dragState) {
                if (e.preventDefault) {
                    e.preventDefault(); // Necessary. Allows us to drop.
                }
                //element.style.left = (e.clientX - self.dragX) + "px";
                //element.style.top = (e.clientY - self.dragY) + "px";
                
                var style = element.style;
                var body = document.body;
                if(body.style.position) {
                    body.oldPosition = body.style.position;
                }
                body.style.position = 'relative';
                var elementXPosition = parseInt(style.left, 10);
                var elementYPosition = parseInt(style.top, 10);

                var elementNewXPosition = elementXPosition + (e.clientX - element.lastXPosition);
                var elementNewYPosition = elementYPosition + (e.clientY - element.lastYPosition);

                style.left = elementNewXPosition + 'px';
                style.top = elementNewYPosition  + 'px';

                element.lastXPosition = e.clientX;
                element.lastYPosition = e.clientY;
                
                AcmsEvent.onetimeEvent(document, "mouseup", self._onDragStop.bind(self));
                AcmsEvent.onetimeEvent(self.handle, "mouseup", self._onDragStop.bind(self));
                AcmsEvent.onetimeEvent(element, "mouseup", self._onDragStop.bind(self));
            }
        },
        
        _onDragStop: function(e) {
            var self = this, element = self.element;
            self.dragState = false;
            Classes.removeClass(element, 'dragging');
            if(document.body.oldPosition) {
                document.body.style.position = document.body.oldPosition;
            }
            
        },
        
        getDefaultOptions: function() {
            return Draggable.DEFAULT_OPTIONS;
        },
        
        buildOptions: function(options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },
        
        _prepare: function() {
            var self = this,
                o = self.options, 
                element = self.element,
                handle;
            if(!Classes.hasClass(element, 'draggable')) {
                Classes.addClass(element, 'draggable');
            }
            handle = Selectors.q(o.handle, element);
            if(!handle) {
                handle = element;
            }
            self.handle = handle;
            self.dragState = false;
            
        }
        
    };
    
    return Draggable;

});
