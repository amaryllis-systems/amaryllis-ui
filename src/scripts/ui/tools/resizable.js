/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('ui/tools/resizable', [
    'tools/utils',
    'core/selectors',
    'events/event'
], function(Utils, Selectors, AcmsEvent) {
    
    "use strict";
    
    var RESIZABLE_DATA = "acms.ui.tools.resizable",
        NS = "." + RESIZABLE_DATA,
        EVENT_RESIZED = "resized" + NS;
        
    
    var Resizable = function(element, options) {
        this.element =
        this.options = 
        this.resizer = 
        this.startX = 
        this.startY =
        this.startWidth =
        this.startHeight = null;
        this.initialize(element, options);
    };
    
    Resizable.MODULE = "Resizable";
    
    Resizable.DEFAULT_OPTIONS = {
        resizer: '[data-trigger=resizer]',
        onResized: function(evt, ele, resizer) {}
    };
    
    Resizable.needInit = true;
    
    Resizable.init = function(element, options) {
        var r = new Resizable(element, options);
        r.element.acmsData(RESIZABLE_DATA, r);
        
        return r;
    };
    
    Resizable.prototype = {
        constructor: Resizable,
        
        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },
        
        listen: function() {
            var self = this,
                o = self.options,
                resizer = self.resizer;
            if(!resizer) {
                return;
            }
            AcmsEvent.add(resizer, 'mousedown', self._initResizer.bind(self));
            AcmsEvent.add(resizer, 'mousemove', self._doDrag.bind(self));
            AcmsEvent.add(resizer, 'mouseup', self._stopDrag.bind(self));
        },
        
        _initResizer: function(e) {
            var self = this, element = self.element;
            self.startX = e.clientX;
            self.startY = e.clientY;
            self.startWidth = parseInt(document.defaultView.getComputedStyle(element).width, 10);
            self.startHeight = parseInt(document.defaultView.getComputedStyle(element).height, 10);
        },
        
        _doDrag: function(e) {
            var self = this, element = this.element;
            element.style.width = (self.startWidth + e.clientX - self.startX) + 'px';
            element.style.height = (self.startHeight + e.clientY - self.startY) + 'px';
        },
        
        _stopDrag: function(e) {
            var self = this, element = this.element, o = self.options;
            if(typeof o.onResized === 'function') {
                o.onResized(e, element, self);
            }
            var evt = AcmsEvent.create(EVENT_RESIZED);
            AcmsEvent.dispatch(element, evt);
        },
        
        getDefaultOptions: function() {
            return Resizable.DEFAULT_OPTIONS;
        },
        
        buildOptions: function(options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },
        
        _prepare: function() {
            var self = this,
                o = self.options;
            self.resizer = Selectors.q(o.resizer, self.element);
        }
    };
    
    return Resizable;
    
});
