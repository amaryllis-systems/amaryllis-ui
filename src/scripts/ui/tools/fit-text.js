/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('ui/tools/fit-text', [
    'tools/utils',
    'core/style',
    'events/event'
], (Utils, Style, AcmsEvent) => {

    "use strict";

    /**
     * 
     * @param {Element} element
     * @param {Object} options
     * 
     */
    var FitText = function (element, options) {
        this.element =
        this.options =
        this.elements = null;
        this.initialize(element, options);
    };

    FitText.MODULE = "Auto-Fit Text";
    
    FitText.DEFAULT_OPTIONS = {
        rotateText: null,
        fontSizeFactor: 0.8,
        maximumFontSize: null,
        limitingDimension: "both",
        horizontalAlign: "center",
        verticalAlign: "center",
        textAlign: "center",
        whiteSpace: "nowrap"
    };

    FitText.needInit = true;

    /**
     * Modul Initialisierung
     * 
     * @param {Element} element
     * @param {Object} options
     * 
     * @returns {FitText}
     */
    FitText.init = function (element, options) {
        var ft = new FitText(element, options);
        
        return ft;
    };

    FitText.prototype = {
        constructor: FitText,
        
        /**
         * Interner Constructor
         * 
         * @param {Element} element
         * @param {Object} options
         * 
         */
        initialize: function(element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this.listen();
        },
        
        /**
         * Gibt die Standard-Optionen zurück
         * 
         * @returns {FitText.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function() {
            return FitText.DEFAULT_OPTIONS;
        },
        
        /**
         * Bilden der eigenen Optionen
         * @param {Object} options
         * 
         * @returns {@this;@call;_doExtend|Object}
         */
        buildOptions: function(options) {
            return Utils.extend({}, this.getDefaultOptions(), options, this.element.acmsData());
        },
        
        listen: function() {
            var self = this;
            var onResize = function() {
                self._resize();
            };
            AcmsEvent.add(window, 'resize', onResize);
            self._resize();

            setTimeout(function() {
                var ev = AcmsEvent.create('resize');
                AcmsEvent.dispatch(window, ev);
            }, 500);
        },
        
        _resize: function() {
            var o = this.options,
                elem = this.element,
                parent = elem.parentNode;
            var css = {
                visibility: 'hidden',
                display: "inline-block",
                clear: "both",
                'float': "left",
                'font-size': (1000 * o.fontSizeFactor) + "px",
                'line-height': "1000px",
                'white-space': o.whiteSpace,
                "text-align": o.textAlign,
                position: "relative",
                padding: 0,
                margin: 0,
                left: "50%",
                top: "50%"
            };
            Style.setStyles(this.element, css);
            var parentPadding= {
                left: parseInt(Style.getStyle(parent, 'padding-left')),
                top: parseInt(Style.getStyle(parent, 'padding-top')),
                right: parseInt(Style.getStyle(parent, 'padding-right')),
                bottom: parseInt(Style.getStyle(parent, 'padding-bottom'))
            };

            var box= {
                width: elem.offsetWidth,
                height: elem.offsetHeight
            };
            var rotateCSS= {};
            if (o.rotateText !== null) {
                if (typeof o.rotateText !== "number")
                    throw "rotateText value muss eine Nummer sein";
                var rotate= "rotate(" + o.rotateText + "deg)";
                rotateCSS = {
                    "-webkit-transform": rotate,
                    "-ms-transform": rotate,
                    "-moz-transform": rotate,
                    "-o-transform": rotate,
                    "transform": rotate
                };
                Style.setStyles(elem, rotateCSS);
                var sin= Math.abs(Math.sin(o.rotateText * Math.PI / 180));
                var cos= Math.abs(Math.cos(o.rotateText * Math.PI / 180));
                box.width= elem.offsetWidth * cos + elem.offsetHeight * sin;
                box.height= elem.offsetWidth * sin + elem.offsetHeight * cos;
            }

            var widthFactor= (parent.clientWidth - parentPadding.left - parentPadding.right) / box.width;
            var heightFactor= (parent.clientHeight - parentPadding.top - parentPadding.bottom) / box.height;
            var lineHeight;

            if (o.limitingDimension.toLowerCase() === "width") {
                lineHeight= Math.floor(widthFactor * 1000);
                parent.clientHeight = lineHeight + 'px';
            } else if (o.limitingDimension.toLowerCase() === "height") {
                lineHeight= Math.floor(heightFactor * 1000);
            } else if (widthFactor < heightFactor)
                lineHeight= Math.floor(widthFactor * 1000);
            else if (widthFactor >= heightFactor)
                lineHeight= Math.floor(heightFactor * 1000);

            var fontSize= lineHeight * o.fontSizeFactor;
            if (o.maximumFontSize !== null && fontSize > o.maximumFontSize) {
                fontSize= o.maximumFontSize;
                lineHeight= fontSize / o.fontSizeFactor;
            }


            Style.setStyles(elem, {
                'font-size': Math.floor(fontSize)  + "px",
                'line-height': Math.ceil(lineHeight)  + "px",
                'margin-bottom': "0px",
                'margin-right': "0px"
            });
            if (o.limitingDimension.toLowerCase() === "height") {
                parent.clientWidth = (elem.clientWidth + 4) + "px";
            }
            var endCSS= {};

            switch(o.verticalAlign.toLowerCase()) {
                case "top":
                    endCSS.top = "0%";
                break;
                case "bottom":
                    endCSS.top = "100%";
                    endCSS['margin-top']= Math.floor(-elem.clientHeight) + "px";
                break;
                default:
                    endCSS['margin-top']= Math.floor((-elem.clientHeight / 2)) + "px";
                break;
            }

            switch(o.horizontalAlign.toLowerCase()) {
                case "left":
                    endCSS.left = "0%";
                break;
                case "right":
                    endCSS.left = "100%";
                    endCSS['margin-left'] = Math.floor(-elem.clientWidth) + "px";
                break;
                default:
                    endCSS['margin-left'] = Math.floor((-elem.clientWidth / 2)) + "px";
                break;
            }
            Style.setStyles(elem, endCSS);
            Style.setStyle(elem, "visibility", "visible");
        }
    };
    
    return FitText;

});
