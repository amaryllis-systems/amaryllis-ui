/**
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */


define("ui/elements/html", [
    "core/document",
    'tools/utils',
    'core/classes'
], function (Doc, Utils, Classes) {
    
    "use strict";

    var Html = {
        MODULE: 'HTML Elements',

        mk: function(tagName) {
            return Doc.createElement(tagName);
        },
        
        create: function(tag, cls, attrs, data) {
            var self = this, ele = self.mk(tag);
            self._process(ele, cls, attrs, data);
            
            return span;
        },
        
        /**
         * Erstellt ein span-Element
         * 
         * @param {String|null} cls     CSS Klasse(n)
         * @param {Object|null} attrs   Optionale Attribute
         * @param {Object|null} data    Optionale data-Attribute
         * 
         * @returns {Element}
         */
        span: function (cls, attrs, data) {
            var self = this, ele = self.mk('span');
            self._process(ele, cls, attrs, data);
            
            return ele;
        },
        
        /**
         * Erstellt ein div-Element
         * 
         * @param {String|null} cls     CSS Klasse(n)
         * @param {Object|null} attrs   Optionale Attribute
         * @param {Object|null} data    Optionale data-Attribute
         * 
         * @returns {Element}
         */
        div: function (cls, attrs, data) {
            var self = this, ele = self.mk('div');
            self._process(ele, cls, attrs, data);
            
            return ele;
        },
        
        /**
         * Erstellt ein anchor-Element
         * 
         * @param {String|null} cls     CSS Klasse(n)
         * @param {Object|null} attrs   Optionale Attribute
         * @param {Object|null} data    Optionale data-Attribute
         * 
         * @returns {Element}
         */
        anchor: function (cls, attrs, data) {
            var self = this, ele = self.mk('a');
            self._process(ele, cls, attrs, data);
            
            return ele;
        },
        
        /**
         * Erstellt ein ul-Element
         * 
         * @param {String|null} cls     CSS Klasse(n)
         * @param {Object|null} attrs   Optionale Attribute
         * @param {Object|null} data    Optionale data-Attribute
         * 
         * @returns {Element}
         */
        ul: function (cls, attrs, data) {
            var self = this, ele = self.mk('ul');
            self._process(ele, cls, attrs, data);
            
            return ele;
        },
        
        /**
         * Erstellt ein li-Element
         * 
         * @param {String|null} cls     CSS Klasse(n)
         * @param {Object|null} attrs   Optionale Attribute
         * @param {Object|null} data    Optionale data-Attribute
         * 
         * @returns {Element}
         */
        li: function (cls, attrs, data) {
            var self = this, ele = self.mk('li');
            self._process(ele, cls, attrs, data);
            
            return ele;

        },
        
        /**
         * Erstellt ein label-Element
         * 
         * @param {String|null} cls     CSS Klasse(n)
         * @param {Object|null} attrs   Optionale Attribute
         * @param {Object|null} data    Optionale data-Attribute
         * 
         * @returns {Element}
         */
        label: function (cls, attrs, data) {
            var self = this, ele = self.mk('label');
            self._process(ele, cls, attrs, data);
            
            return ele;
        },
        
        /**
         * Erstellt ein input-Element
         * 
         * @param {String|null} cls     CSS Klasse(n)
         * @param {Object|null} attrs   Optionale Attribute
         * @param {Object|null} data    Optionale data-Attribute
         * 
         * @returns {Element}
         */
        input: function (cls, attrs, data) {
            var self = this, ele = self.mk('input');
            self._process(ele, cls, attrs, data);
            
            return ele;
        },
        
        /**
         * Erstellt ein i-Element
         * 
         * @param {String|null} cls     CSS Klasse(n)
         * @param {Object|null} attrs   Optionale Attribute
         * @param {Object|null} data    Optionale data-Attribute
         * 
         * @returns {Element}
         */
        i: function(cls, attrs, data) {
            var self = this, ele = self.mk('i');
            self._process(ele, cls, attrs, data);
            
            return ele;
        },

        /**
         * Erstellt ein button-Element
         * 
         * @param {String|null} cls     CSS Klasse(n)
         * @param {Object|null} attrs   Optionale Attribute
         * @param {Object|null} data    Optionale data-Attribute
         * 
         * @returns {Element}
         */
        button: function(cls, attrs, data) {
            var self = this, ele = self.mk('button');
            self._process(ele, cls, attrs, data);
            
            return ele;
        },
        
        
        /**
         * Erstellt ein img-Element
         * 
         * @param {String|null} cls     CSS Klasse(n)
         * @param {Object|null} attrs   Optionale Attribute
         * @param {Object|null} data    Optionale data-Attribute
         * 
         * @returns {Element}
         */
        img: function(cls, attrs, data) {
            var self = this, ele = self.mk('img');
            self._process(ele, cls, attrs, data);
            
            return ele;
        },
        
        
        /**
         * Erstellt ein figure-Element
         * 
         * @param {String|null} cls     CSS Klasse(n)
         * @param {Object|null} attrs   Optionale Attribute
         * @param {Object|null} data    Optionale data-Attribute
         * 
         * @returns {Element}
         */
        figure: function(cls, attrs, data) {
            var self = this, ele = self.mk('figure');
            self._process(ele, cls, attrs, data);
            
            return ele;
        },
        
        _process: function(ele, cls, attrs, data) {
            var self = this;
            self._addClasses(ele, cls);
            self._appendAttr(ele, attrs);
            self._appendData(ele, data);
        },
        
        _addClasses: function(ele, cls) {
            if(cls && typeof cls === "string") {
                Classes.addClass(ele, cls);
            }
        },
        
        /**
         * Anhängen der data-Attribute
         * 
         * @param {Element}     ele     Das Element
         * @param {Object|null} data    Optionale Data-Attribute als Object
         * 
         * 
         */
        _appendData: function(ele, data) {
            var prop;
            if (null !== data && typeof data === "object") {
                for (prop in data) {
                    if (data.hasOwnProperty(prop)) {
                        ele.acmsData(prop, data[prop]);
                    }
                }
            }
        },
        
        /**
         * Anhängen der Attribute
         * 
         * @param {Element}     ele     Das Element
         * @param {Object|null} attrs   Optionale Data-Attribute als Object
         * 
         * 
         */
        _appendAttr: function(ele, attrs) {
            var prop;
            if (null !== attrs && typeof attrs === "object") {
                for (prop in attrs) {
                    if (attrs.hasOwnProperty(prop)) {
                        ele.setAttribute(prop, attrs[prop]);
                    }
                }
            }
        }
        
    };

    return Html;

});
