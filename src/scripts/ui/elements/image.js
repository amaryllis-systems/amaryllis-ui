/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */


define("ui/elements/image", [], function() {

    "use strict";

    /**
     * Constructor
     *
     * @param {Object} options  Optionen
     * @returns {Img}
     */
    var Img = function(options) {
        this.Image =
        this.options = null;
        this.initialize(options);
    };

    /**
     * Komponenten Name
     *
     * @var {String}
     */
    Img.MODULE = "HTML Image Element";

    /**
     * Klassen Definitionen
     */
    Img.prototype = {
        /** Constructor **/
        constructor: Img,

        /**
         * Interner Constructor
         *
         * Initialisiert das Img Element.
         *
         * @param {Object} options  Die Optionen
         *
         * 
         */
        initialize: function(options) {
            this.Image = new Image();
            this.options = this.buildOptions(options);
        },

        /**
         * Anhängen des Elementes an ein DOM Element
         *
         * Das Bild wird dem übergebenen DOM Element als Child-Element angehängt.
         *
         * @param {Object|NULL} ele Ein optionales Element dem das Bild
         *                          zugefügt werden soll. Ist kein Element
         *                          gegeben, wird der Body genommen.
         *                          
         * 
         */
        append: function(ele) {
            ele = ele || document.body;
            ele.appendChild(this.Image);
        },

        /**
         * Setter
         * 
         * @param {String}  a
         * @param {Mixed}   b
         * @returns {Img.prototype}
         */
        set: function(a, b) {
            if(false === this.Image.hasOwnProperty(a)) {
                return;
            }
            var dsc = this.Image.getOwnPropertyDescriptor(this.Image, a);
            if(!dsc || false === dsc.writable) {
                return;
            }
            this.Image[a] = b;
            return this;
        },

        /**
         * Getter
         *
         * Gibt den Wert für a zurück
         *
         * @param {String} a Der Schlüssel
         * 
         * @returns {mixed}
         */
        get: function(a) {
            return this.Image[a];
        },

        /**
         * Gibt das HtmlImageElement zurück
         * 
         * @returns {Object|Img.Image|Image}
         */
        getImage: function() {
            return this.Image;
        },

        /**
         * Bildet die Optionen
         * 
         * @param {Object}  options
         *
         * @throws Error    Wenn die Optionen kein Objekt sind.
         *
         * @returns {Object}
         */
        buildOptions: function(options) {
            var self = this, option;
            if(typeof options !== "object") {
                throw new Error("Options requires to be Object");
            }
            for(option in options) {
                if(options.hasOwnProperty(option)) {
                    self.Image[option] = options[option];
                }
            }
            return options;
        }
    };

    return Img;
});
