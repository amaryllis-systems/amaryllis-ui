/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@amws.eu>
 * @copyright 2011 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright Amaryllis Systems GmbH http://www.amws.eu/
 * @license http://www.amaryllis-systems.de/lizenzen
 * @license /docs/license-js.txt
 */

define('ui/elements/audio', [], function() {

    "use strict";

    /**
     * Constructor
     *
     * @param {Object} options  Optionen
     * @returns {Img}
     */
    var Ele = function(options) {
        this.Audio =
        this.options = null;
        this.initialize(options);
    };

    /**
     * Komponenten Name
     *
     * @var {String}
     */
    Ele.MODULE = "HTML Audio Element";

    /**
     * Klassen Definitionen
     */
    Ele.prototype = {
        /** Constructor **/
        constructor: Ele,

        /**
         * Interner Constructor
         *
         * Initialisiert das Img Element.
         *
         * @param {Object} options  Die Optionen
         *
         * 
         */
        initialize: function(options) {
            this.Audio = new Audio();
            this.options = this.buildOptions(options);
        },

        /**
         * Anhängen des Elementes an ein DOM Element
         *
         * Das Bild wird dem übergebenen DOM Element als Child-Element angehängt.
         *
         * @param {Object|NULL} el  Ein optionales Element dem das Audio Element
         *                          zugefügt werden soll. Ist kein Element
         *                          gegeben, wird der Body genommen.
         *
         * 
         */
        append: function(el) {
            el = el || document.body;
            el.appendChild(this.Audio);
        },

        /**
         * Setter
         *
         * @param {String}  a
         * @param {Mixed}   b
         * @returns {Ele.prototype}
         */
        set: function(a, b) {
            if(false === this.Audio.hasOwnProperty(a)) {
                return;
            }
            var dsc = this.Audio.getOwnPropertyDescriptor(this.Audio, a);
            if(!dsc || false === dsc.writable) {
                return;
            }
            this.Audio[a] = b;
            return this;
        },

        /**
         * Getter
         *
         * Gibt den Wert für a zurück
         *
         * @param {String} a Der Schlüssel
         *
         * @returns {mixed}
         */
        get: function(a) {
            return this.Audio[a];
        },

        /**
         * Gibt das HtmlAudioElement zurück
         *
         * @returns {Object|Ele.Audio|Audio}
         */
        getAudio: function() {
            return this.Audio;
        },

        /**
         * Bildet die Optionen
         *
         * @param {Object}  options
         *
         * @throws Error    Wenn die Optionen kein Objekt sind.
         *
         * @returns {Object}
         */
        buildOptions: function(options) {
            var self = this, option;
            if(typeof options !== "object") {
                throw new Error("Options requires to be Object");
            }
            for(option in options) {
                if(options.hasOwnProperty(option)
                        && self.Audio.hasOwnProperty(option)) {
                    self.Audio[option] = options[option];
                }
            }
            return options;
        }
    };

    return Ele;

});
