/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("apps/apf/approval/approval", [
	"core/base",
	"core/classes",
	"core/selectors",
	"events/event",
	"core/acms"
], (Base, Classes, Selectors, AcmsEvent, A) => {

	/**
	 * Approval Handling
	 * 
	 * Provides click handlers for default apf approvals
	 *
	 * @class
	 * @extends {Base}
	 * @author qm-b <https://bitbucket.org/qm-b/>
	 */
	class Approval extends Base {

		/**
		 * Module Namespace
		 *
		 * @readonly
		 * @static
		 * @memberof Approval
		 */
		static get NS() {
			return "apps.apf.approval.approval";
		}

		/**
		 * Module Name
		 *
		 * @readonly
		 * @static
		 * @memberof Approval
		 */
		static get MODULE() {
			return "Approval";
		}

		static get css() {
			return "media/css/ui/popover";
		}

		/**
		 * Default Options
		 *
		 * @readonly
		 * @static
		 * @memberof Approval
		 */
		static get DEFAULT_OPTIONS() {
			return {
				remote: null,
				id: null,
				remove: true, // remove element after approving/declining
				templates: {
					popover: 'templates/html/apf/approval/approval-popup.hbs'
				},
				selectors: {
					trigger: '[data-trigger]'
				},
				data: {
					trigger: 'trigger'
				},
				css: Approval.css,
				themecss: false
			};
		}

		/**
		 * Module Init
		 *
		 * @static
		 * 
		 * @param {HTMLElement} 	element		Wrapper Element
		 * @param {Object} 			options		Optional custom options
		 * 
		 * @returns {Approval}	New Instance
		 * 
		 * @memberof Approval
		 */
		static init(element, options) {
			let m = new Approval(element, options);
			m.element.acmsData(Approval.NS, m);

			return m;
		}

		/**
		 * Creates an instance of Approval.
		 * 
		 * @param {HTMLElement} 	element		Wrapper Element
		 * @param {Object} 			options		Optional custom options
		 * 
		 * @memberof Approval
		 */
		constructor(element, options) {
			super(element, options);
			this._initialize();
		}

		get processing() {
			return this._processing;
		}

		set processing(flag) {
			self._processing = (true === flag);
		}

		listen() {
			let self = this, element = self.element, o = self.options;
			AcmsEvent.on(element, 'click', o.selectors.trigger, self._onClickTrigger, true)
		}

		/**
		 * On Click Trigger Callback
		 *
		 * @param {Event} 		e	Click Event
		 * 
		 * @memberof Approval
		 */
		_onClickTrigger(e) {
			let self = this, o = self.options,sel = o.selectors, target = e.target;
			if(!Selectors.matches(target, sel.trigger)) {
				target = Selectors.closest(target, sel.trigger);
			}
			if(!target) {
				return;
			}
			if(self.processing) {
				return;
			}
			let cls = o.classNames, trigger = target.acmsData(o.data.trigger);
			if(trigger && trigger.trim() === 'decline') {
				return self._onClickDecline(target);
			}
			self.processing = true;
			let fData = new FormData();
			fData.append('op', trigger);
			fData.append('id', o.id);
			require(['http/request', 'notifier'], (Request) => {
				Request.post(o.remote, fData, self._done, self._fail, self._always);
			});
		}

		_onClickDecline(button) {
			let self = this, o = self.options;
			let popoverOptions = {
				placement: 'top',
				trigger: 'click',
				content: 'Geben Sie eine Nachricht mit für den Autor, warum der Beitrag abgelehnt wird. Dann kann der Beitrag vom Autor leihter nachgebessert werden.', // string, function or selector
				removeContent: true, // remove content wrapper if the content is a selector?
				template: o.templates.popover,
				//animation: true,
				title: '',
				//delay: 100,
				dismiss: true, // create dismiss button?
				html: true,
				container: false, // Optional HTML Container. Otherwise document.body will be used
				onCreate: function(popover, pover) {
				},
				onOpen: function(popover, pover) {
					AcmsEvent.on(popover, 'click', '[data-trigger]', (e) => {
						let message = Selectors.q('textarea', popover).value;
						let fData = new FormData();
						fData.append('op', 'dismiss');
						fData.append('message', message);
						fData.append('id', o.id);
						require(['http/request', 'notifier'], (Request) => {
							Request.post(o.remote, fData, self._done, self._fail, self._always);
							pover.close();
							pover.remove();
						});

					});
				},
				onClose: function(popover, pover) {
					self.processing = false;
				},
				customClass: 'danger', // 'primary', 'danger', ..
				id: null, // ID des Popover oder leer lassen für automatische ID (empfohlen),
				customData: null, // optional custom data to be assigned to the template. Variable is ignored in default template.
				classNames: {
					active: 'open'
				},
				selectors: {
					active: '.open',
					dismiss: '[data-dismiss="popover"]'
				},
				alwaysRemove: false, // remove always on close?
				dismissOutside: false, // dismiss on click outside popover?
				dismissResize: true, // dismiss on resize?
			};
			require(["apps/ui/informations/popover"], (Popover) => {
				let mod = Popover.init(button, popoverOptions);
				mod.open('top');
			});
		}

		/**
		 * On Remote Done Callback
		 *
		 * @param {Object} response		Remote Response
		 * 
		 * @memberof Approval
		 */
		_done(response) {
			let self = this, o = self.options;
			A.Notifications.createFromResponse(response);
			if(response.status === 'success' && o.remove) {
				self.element.parentNode.removeChild(self.element);
			}
		}

		_fail(xhr, textStatus) {
            A.Logger.logWarning(textStatus);
		}
		
		_always(xhr, textStatus) {
			A.Logger.logInfo(textStatus);
			this.processing = false;
		}

		/**
		 * Prepare Module
		 *
		 * @memberof Approval
		 */
		_prepare() {
			let self = this, o = self.options, element = self.element;
			self.options.remove = (o.remove === 'true' || o.remove === true || o.remove === '1' || o.remove === 1);
		}

	}

	return Approval;
});
