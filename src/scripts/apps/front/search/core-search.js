/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("apps/front/search/core-search", [
	"core/base",
	"core/acms"
], (Base, A) => {

	/**
	 * Core Search
	 *
	 * @class
	 * @extends {Base}
	 * @author qm-b <https://bitbucket.org/qm-b/>
	 */
	class CoreSearch extends Base {

		/**
		 * Module Namespace
		 *
		 * @readonly
		 * @static
		 * @memberof CoreSearch
		 */
		static get NS() {
			return "acms.apps.front.search.core-search";
		}

		/**
		 * Module Name
		 *
		 * @readonly
		 * @static
		 * @memberof CoreSearch
		 */
		static get MODULE() {
			return "Core Search";
		}

		/**
		 * Default Options
		 *
		 * @readonly
		 * @static
		 * @memberof CoreSearch
		 */
		static get DEFAULT_OPTIONS() {
			return {};
		}

		/**
		 * Module Init
		 *
		 * @static
		 * 
		 * @param {HTMLFormElement} element		Search Form
		 * @param {Object|null} 	options		Optional custom Options
		 * 
		 * @returns {CoreSearch}	New Instance
		 * 
		 * @memberof CoreSearch
		 */
		static init(element, options) {
			let m = new CoreSearch(element, options);
			m.element.acmsData(CoreSearch.NS, m);
			
			return m;
		}

		/**
		 * Creates an instance of CoreSearch.
		 * 
		 * @param {HTMLFormElement} element		Search Form
		 * @param {Object|null} 	options		Optional custom Options
		 * 
		 * @memberof CoreSearch
		 */
		constructor(element, options) {
			super(element, options);
			this._initialize();

		}


		_prepare() {
		}

	}

	return CoreSearch;
});
