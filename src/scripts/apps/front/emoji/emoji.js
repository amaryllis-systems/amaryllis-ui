
/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("apps/front/emoji/emoji", [
    "core/base",
    "text!apps/front/emoji/data/categories.json",
    "text!apps/front/emoji/data/emoji.json",
    "templates/template-engine",
    "tools/string/unique-id",
    "core/classes",
    "core/selectors",
    "events/event",
    "core/acms"
], (Base, CategoriesData, EmojiData, TemplateEngine, UniqueId, Classes, Selectors, AcmsEvent, A) => {

    /**
     * Emojis from Core
     */
    const emojis = JSON.parse(EmojiData);

    /**
     * Emoji Categories from Core
     */
    const categories = JSON.parse(CategoriesData);

    /**
     * Emoji Renderer for all Emojis
     *
     * @class
     * @extends {Base}
     */
    class Emoji extends Base {

        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof Emoji
         */
        static get NS() {
            return "apps.front.emoji.emoji";
        }

        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof Emoji
         */
        static get MODULE() {
            return "Emoji";
        }

        /**
         * Default Options
         *
         * @readonly
         * @static
         * @memberof Emoji
         */
        static get DEFAULT_OPTIONS() {
            return {
                templates: {
                    emoji: 'templates/html/front/emoji/emoji.hbs',
                    categories: 'templates/html/front/emoji/categories.hbs',
                },
                onSelect: (emoji, self) => {},
                onDrawed: (self) => {}
            };
        }

        /**
         * Module Init
         *
         * @static
         * 
         * @param {HTMLElement} element Wrapper Element around the Emojis
         * @param {Object}      options Custom Options
         * 
         * @returns {Emoji} New Instance
         * 
         * @memberof Emoji
         */
        static init(element, options) {
            let m = new Emoji(element, options);
            m.element.acmsData(Emoji.NS, m);
            
            return m;
        }

        /**
         * Creates an instance of Emoji.
         * 
         * @param {HTMLElement} element Wrapper Element around the Emojis
         * @param {Object}      options Custom Options
         * 
         * @memberof Emoji
         */
        constructor(element, options) {
            super(element, options);
            this._initialize();
        }

        /**
         * Emoji Categories
         *
         * @readonly
         * @memberof Emoji
         */
        get categories() {
            return categories;
        }

        /**
         * Emojis
         *
         * @readonly
         * @memberof Emoji
         */
        get emojis() {
            return emojis;
        }

        /**
         * Emoji Template
         *
         * @readonly
         * @memberof Emoji
         */
        get emojiTpl() {
            return this._emojiTpl;
        }

        /**
         * Emoji Category Template
         *
         * @readonly
         * @memberof Emoji
         */
        get categoryTpl() {
            return this._categoryTpl;
        }

        get unique() {
            return this._unique;
        }

        /**
         * Setup Event Listener
         *
         * @memberof Emoji
         */
        listen() {
            let self = this, element = self.element;
            AcmsEvent.on(element, 'dblclick', '.emoji', self._onClickEmoji, true);
            AcmsEvent.on(element, 'keydown', 'input[type="search"]', self._onSearch);
        }

        /**
         * On Dblclick Emoji Event Handler
         *
         * @param {Event} e Click Event
         * 
         * @memberof Emoji
         */
        _onClickEmoji(e) {
            let self = this, o = self.options, target = e.target;
            if(!Classes.hasClass(target, '.emoji')) {
                target = Selectors.closest(target, '.emoji');
            }
            if(!target) return;
            o.onSelect(target, self);
            return false;
        }

        _onSearch(e) {
            let self = this, element = self.element;
            let process = () => {
                self._breakSearch = false;
                Selectors.q('.results', element).innerHTML = "";
                return self._search(e);
            };
            self._breakSearch = true;
            clearTimeout(self.searchTimeout);
            self.searchTimeout = setTimeout(process, 700);
            
        }

        _search(e) {
            let self = this, element = self.element, 
                search = Selectors.q('input[type=search]', element), 
                results = Selectors.q('.results', element), 
                emoji = Selectors.qa('.emoji', element);
            let code = parseInt(e.keyCode || e.which);
            
            results.innerHTML = "";
            Selectors.q('.results', element).textContent = "";
            if (code === 27) {
                search.value = '';
                return;
            }
            
            let val = search.value.trim().replace(/ +/g, ' ').toLowerCase();
            let j = 0, len = emoji.length;
            while(j < len && false === self._breakSearch) {
                if(self._breakSearch === true) {
                    results.innerHTML = "";
                    break;
                }
                let i = emoji[j],
                    shortname = i.acmsData('code'), 
                    dataAlternates = i.acmsData('alternates'),
                    dataKeywords = i.acmsData('keywords'),
                    text, clon;
                j++;
                text = shortname.toLowerCase();
                if(text.indexOf(val) >= 0) {
                    clon = i.cloneNode();
                    results.appendChild(clon);
                    continue;
                }
                if(dataAlternates && dataAlternates.toLowerCase().indexOf(val) >= 0) {
                    clon = i.cloneNode();
                    results.appendChild(clon);
                    continue;
                }
                if(dataKeywords && dataKeywords.toLowerCase().indexOf(val) >= 0) {
                    clon = i.cloneNode();
                    results.appendChild(clon);
                    continue;
                }
            }
            

        }

        /**
         * Drawing the Emojis once the templates have been resolved
         *
         * @memberof Emoji
         */
        _draw() {
            if(this._drawn) {
                return;
            }
            this._drawn = true;
            this.rendered = 0;
            let self = this, 
                unique = 'em-' + UniqueId(), 
                tData = {
                    unique: unique,
                    categories: self.categories
                }, 
                cb = () => {
                    
                    for(let prop in self.emojis) {
                        if(self.emojis.hasOwnProperty(prop)) {
                            let em = self.emojis[prop], cat = em.category, ele = Selectors.q('.panel[data-panel="' + cat + '"]');
                            let data = {
                                emoji: em
                            };
                            TemplateEngine.compile(self.emojiTpl, data, ele);
                        }
                    }
                };
            self._unique = unique;
            TemplateEngine.compile(self.categoryTpl, tData, self.element);
            setTimeout(() => {
                cb();
            }, 200);
            let interval = setInterval(() => {
                if(Selectors.qa('.emoji', self.element).length === Object.keys(self.emojis).length) {
                    self._onDone();
                    clearInterval(interval);
                }
            });
        }

        /**
         * On Rendered Callback
         *
         * @memberof Emoji
         */
        _onDone() {
            let self = this, o = self.options;
            o.onDrawed(self);
        }

        /**
         * Prepare Module
         *
         * @memberof Emoji
         */
        _prepare() {
            let self = this, o = self.options, templates = o.templates;
            require(['text!' + templates.emoji,'text!' +  templates.categories], (EmojiTpl, CategoriesTpl) => {
                self._emojiTpl = EmojiTpl;
                self._categoryTpl = CategoriesTpl;
                self._draw();
            });
            self._breakSearch = false;
            self.searchTimeout = null;
        }

    }

    return Emoji;
});
