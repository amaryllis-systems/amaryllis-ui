/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('apps/front/up-top', [
    "core/base",
    'core/classes',
    'apps/ui/helpers/show-hide',
    "core/window",
    'events/event'
], (Base, Classes, ShowHide, Win, AcmsEvent) => {

    const abs = Math.abs;

    /**
     * UpTop Scroll
     *
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     * 
     * @property {HTMLElement} target   Target Element
     */
    class UpTop extends Base {

        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof UpTop
         */
        static get MODULE() {
            return 'Up-Top';
        }

        /**
         * Module Version
         *
         * @readonly
         * @static
         * @memberof UpTop
         */
        static get NS() {
            return "acms.apps.front.up-top";
        }

        /**
         * Default Options
         *
         * @readonly
         * @static
         * @memberof UpTop
         */
        static get DEFAULT_OPTIONS() {
            return {
                target: "#acmsTop",
                
                showButtonAfter: 200, // show Button after .. (px)
                animate: false, // [false|normal|linear] - for false no aditional settings are needed

                duration: 500,
                normal: { // applys only if [animate: normal] - set scroll loop distanceLeft/steps|ms
                    steps: 20, // the more steps per loop the slower animation gets
                    ms: 10 // the less ms the quicker your animation gets
                },
                linear: { // applys only if [animate: linear] - set scroll px|ms
                    px: 30, // the more px the quicker your animation gets
                    ms: 10 // the less ms the quicker your animation gets
                },
                easeInCubic: {
                    steps: 20
                },
                easeInOutQuintic: {
                    steps: 20
                },
                easeInOutQuad: {
                    steps: 20
                }

            };
        }

        /**
         * Module init
         *
         * @static
         * @param {HTMLElement} element HTML Element
         * @param {Object|null} options Custom Options
         * @memberof UpTop
         */
        static init(element, options) {
            let ut = new UpTop(element, options);
            ut.element.acmsData(UpTop.NS, ut);
            return ut;
        }

        /**
         * Creates an instance of UpTop.
         * 
         * @param {HTMLElement} element HTML Element
         * @param {Object|null} options Custom Options
         * @memberof UpTop
         */
        constructor(element, options) {
            super(element, options);
            this._initialize();
        }

        get target() {
            return this._target;
        }

        get stop() {
            return this._stop;
        }

        set stop(flag) {
            this._stop = (true === flag);
        }

        /**
         * Gets the current Position
         *
         * @returns {Number}
         * @memberof UpTop
         */
        getCurrentPosition() {
            return document.documentElement.scrollTop || document.body.parentNode.scrollTop || document.body.scrollTop;
        }

        getNewPosition() {
            const self = this;
            const o = self.options;
            let  position = 0,
                element = self.target;

            if (typeof (o.target) === 'string') {
                element = document.querySelector(o.target);

                if (element) {
                    position = (element.getBoundingClientRect()).top + window.scrollY;
                } else {
                    throw new Error('missing element');
                }
            } else if (typeof (o.position) === 'number') {
                position = o.position;
            } else {
                throw new Error('missing element or position');
            }

            return position;
        }


        /**
         * Prepare
         *
         * @memberof UpTop
         */
        _prepare() {
            let self = this, o = self.options, browser = Win.AmaryllisCMS.browser;
            self._target = document.querySelector(o.target);
            if(!self._target) {
                self._target = ((browser.isFirefox || browser.isIE) ? document.documentElement : document.body);
            }
            self.stop = false;
            self.start = self.getCurrentPosition();
            self.change = self.getNewPosition() - self.start;
            self.currentTime = 0;
            self.increment = 20;
        }
        
        /**
         * Aufsetzen des Click-Eventlistener
         *
         * Basierend auf der "target" Option mit Fallback auf das Link Attribut
         * wird das Ziel ermittelt und bei click die _scrollTo Methode
         * aufgerufen. Ist das Ziel nicht definiert (weder durch eine "target"
         * Option noch durch ein "href" Attribut) oder wird das Ziel nicht
         * gefunden, wird der Vorgang abgebrochen und die normale Aktion des
         * Elements wird weitergeführt.
         *
         * 
         */
        listen() {
            let self = this;
            AcmsEvent.add(self.element, 'click', self._onClickScrollTop);
            AcmsEvent.add(self.element, 'dblclick', self._onDblClickScrollTop);
            AcmsEvent.add(window, 'scroll', self._checkPosition);
            let position = this.getCurrentPosition();
            let cb = (e) => {
                    if (self.getCurrentPosition() > position) {
                        self.stopTimeout(200);
                    }
                    position = self.getCurrentPosition();
                },
                cb2 = (e) => {
                    if (e.deltaY > 0)
                        self.stopTimeout(200);
                };
            AcmsEvent.add(window, "scroll", cb);
            AcmsEvent.add(window, "wheel", cb2);
        }

        scroll() {
            let self = this, o = this.options;
            self.change = self.getNewPosition() - self.start;
            
            // find the value with the quadratic in-out easing function
            let func = self._easing();
            self.increment = o[func].steps;
            // increment the time
            self.currentTime += self.increment;
            let val = self['_' + func](self.currentTime, self.start, self.change, o[func].ms);
            // move the document.body
            self.target.scrollTop = val;
            // do the animation unless its over
            if (self.currentTime < o.duration) {
                Win.requestAnimationFrame(self.scroll.bind(self));
            } else {
                if (o.callback && typeof (o.callback) === 'function') {
                    // the animation is done so lets callback
                    o.callback();
                }
            }
        }

        _easing() {
            let self = this, o = self.options, easeType;
            switch (o.easing) {
                case 'easeInCubic':
                    easeType = 'easeInCubic';
                    break;
                case 'easeInOutQuintic':
                    easeType = 'easeInOutQuintic';
                    break;
                case 'easeInOutQuad':
                    easeType = 'easeInOutQuad';
                    break;
                case 'linear':
                    easeType = 'linear';
                    break;
                default:
                    easeType = 'easeInOutQuad';
                    break;
            }
            return easeType;
        }

        /**
         *
         *
         * @param {Number} t    Time
         * @param {Number} b    Start Position
         * @param {Number} c    Change position
         * @param {Number} d    Duration
         * 
         * @returns {Number}    New Scroll Position
         * 
         * @memberof UpTop
         */
        _easeInOutQuad(t, b, c, d) {
            t /= d / 2;
            if (t < 1) {
                return c / 2 * t * t + b
            }
            t--;
            return -c / 2 * (t * (t - 2) - 1) + b;
        }
    
        /**
         *
         *
         * @param {Number} t    Time
         * @param {Number} b    Start Position
         * @param {Number} c    Change position
         * @param {Number} d    Duration
         * 
         * @returns {Number}    New Scroll Position
         * 
         * @memberof UpTop
         */
        _easeInCubic(t, b, c, d) {
            var tc = (t /= d) * t * t;
            return b + c * (tc);
        }
    
        /**
         *
         *
         * @param {Number} t    Time
         * @param {Number} b    Start Position
         * @param {Number} c    Change position
         * @param {Number} d    Duration
         * 
         * @returns {Number}    New Scroll Position
         * 
         * @memberof UpTop
         */
        _easeInOutQuintic(t, b, c, d) {
            var ts = (t /= d) * t,
            tc = ts * t;
            return b + c * (6 * tc * ts + -15 * ts * ts + 10 * tc);
        }

        /**
         *
         *
         * @param {Number} t    Time
         * @param {Number} b    Start Position
         * @param {Number} c    Change position
         * @param {Number} d    Duration
         * 
         * @returns {Number}    New Scroll Position
         * 
         * @memberof UpTop
         */
        _linear(t, b, c, d) {
            let st, self = this, o = self.options;
            return - abs(o.linear.steps);
        }

        _scrollNoAnimate() {
            document.body.scrollTop = 0;
            document.documentElement.scrollTop = 0;
        }


        _checkPosition() {
            if (this.getCurrentPosition() > this.options.showButtonAfter) {
                ShowHide.show(this.element);

            } else {
                ShowHide.hide(this.element);
            }
        }

        /**
         * On ScrollTop Click Event Listener
         *
         * @param {Event} e
         * @memberof UpTop
         */
        _onClickScrollTop(e) {
            if(e) e.preventDefault();
            this.currentTime = 0;
            this.scroll();
        }
        
        /**
         * On ScrollTop Click Event Listener
         *
         * @param {Event} e
         * @memberof UpTop
         */
        _onDblClickScrollTop(e) {
            if(e) e.preventDefault();
            this.scrollNoAnimate();
        }


        stopTimeout(mils) {
            let self = this;
            self.stop = true;
            setTimeout(() => {
                self.stop = false;
            }, mils);
        }

    }

    return UpTop;

});
