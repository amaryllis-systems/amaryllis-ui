/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("apps/admin/module/module-loader", [
	"core/base",
	"core/classes",
	"core/selectors",
	"events/event",
	"apps/ui/informations/overlay",
    "apps/ui/informations/tooltip",
	"core/acms"
], (Base, Classes, Selectors, AcmsEvent, Overlay, Tooltip, A) => {

	/**
	 * Module Loader
	 *
	 * @class
	 * @extends {Base}
	 * @author qm-b <https://bitbucket.org/qm-b/>
	 */
	class ModuleLoader extends Base {

		/**
		 * Module Namespace
		 *
		 * @readonly
		 * @static
		 * @memberof ModuleLoader
		 */
		static get NS() {
			return "apps.admin.module.module-loader";
		}


		/**
		 * Module Name
		 *
		 * @readonly
		 * @static
		 * @memberof ModuleLoader
		 */
		static get MODULE() {
			return "Module Loader";
		}

		/**
		 * Default options
		 *
		 * @readonly
		 * @static
		 * @memberof ModuleLoader
		 */
		static get DEFAULT_OPTIONS() {
			return {
				remote: null
			};
		}

		/**
		 * Module init
		 *
		 * @static
		 * 
		 * @param {HTMLElement} element		HTML Wrapper Element
		 * @param {Object|null} options		Optional custom options
		 * 
		 * @returns {ModuleLoader}	New Instance
		 * 
		 * @memberof ModuleLoader
		 */
		static init(element, options) {
			let m = new ModuleLoader(element, options);
			m.element.acmsData(ModuleLoader.NS, m);

			return m;
		}


		/**
		 * Creates an instance of ModuleLoader.
		 * 
		 * @param {HTMLElement} element		HTML Wrapper Element
		 * @param {Object|null} options		Optional custom options
		 * 
		 * @memberof ModuleLoader
		 */
		constructor(element, options) {
			super(element, options);
			this._initialize();
			this.load();
		}

		/**
		 * Load
		 *
		 * @memberof ModuleLoader
		 */
		load() {
			let self = this,
                fData = new FormData();
            fData.append('op', 'load');
            require(['http/request', 'notifier'], (Request) => {
                Request.post(self.remote, fData, self._done, self._fail, self._always);
            });
		}

		/**
		 * Is Module installed?
		 *
		 * @param {String} dirname	Diretory name of the module to check
		 * 
		 * @returns {Boolean} `true` if installed, `false` otherwise.
		 * 
		 * @memberof ModuleLoader
		 */
		isInstalled(dirname) {
			let all = A.Modules,
                installed = false;
            all.forEach((modul) => {
                if (modul == dirname) {
                    installed = true;
                    return true;
                }
            });
            return installed;
		}

		listen() {
			let self = this,
                o = self.options,
                btn = self.button;
            if (btn) {
                AcmsEvent.add(btn, 'click', self._onClickSystemDownload);
            }
		}

		/**
		 * on Remote Request done Callback
		 *
		 * @param {Object} response		Remote Response Object
		 * 
		 * @memberof ModuleLoader
		 */
		_done(response) {
			let self = this;
            if (response.status === 'success') {
                let modules = response.modules,
                    html, dirname;
                if (!modules || !modules.length) {
                    return;
                }
                modules.forEach((mod) => {
                    html = mod.html;
                    let div = document.createElement('div');
                    div.innerHTML = html;
                    while (div.firstChild) {
                        self.target.appendChild(div.firstChild);
                    }
                });
                self._moduleListener();
            }
		}

		_moduleListener() {
			let self = this,
                items, dirname;
            items = Selectors.qa('[data-trigger="download"]', self.target);
            items.forEach((item) => {
                dirname = item.acmsData('dirname');
                let card = Selectors.closest(item, '[data-role="module"]');
                let title = Selectors.q('[data-role="name"]', card),
                    icn = document.createElement('i');
                icn.setAttribute('aria-hidden', 'true');
                title.appendChild(icn);
                if (self.isInstalled(dirname)) {
                    item.acmsData('op', 'update');
                    Tooltip.init(icn, {
                        title: 'Das Modul ist installiert'
                    });
                    Classes.addClass(icn, 'acms-icon,acms-icon-check-circle,text-success,floated,right');
                } else {
                    item.acmsData('op', 'install');
                    Tooltip.init(icn, {
                        title: 'Das Modul ist nicht installiert'
                    });
                    Classes.addClass(icn, 'acms-icon,acms-icon-times-circle,acms-iconr,text-default,floated,right');
                }

                AcmsEvent.add(item, 'click', self._onClickModuleDownload);

            });
		}

		/**
		 * On Click Module Download Event Handler
		 *
		 * @param {Event} e	Click Event
		 * 
		 * @memberof ModuleLoader
		 */
		_onClickModuleDownload(e) {
			let self = this,
                target = e.target,
                dirname, fData = new FormData();
            if (target.tagName.toLowerCase() !== 'button') {
                target = Selectors.closest(target, 'button');
            }
            self.lastButton = target;
            dirname = target.acmsData('dirname');
            fData.append('dirname', dirname);
            fData.append('op', 'download');
            self.overlay.append();
            require(['http/request', 'notifier'], (Request) => {
                Request.post(self.remote, fData, self._onDownloaded, self._fail, self._always);
            });
		}

		/**
		 * On Modul downloaded Callback
		 *
		 * @param {Object} response		Remote Response Object
		 * 
		 * @memberof ModuleLoader
		 */
		_onDownloaded(response) {
			let self = this,
                btn = self.lastButton,
                op, dirname;
            A.Notifications.createFromResponse(response);
            if (response.status === 'success' && btn) {
                op = btn.acmsData('op');
                dirname = btn.acmsData('dirname');
                let uri = A.Urls.baseUrl + '/admin/core/module/' + op + '/' + dirname + '/';
                window.location.href = uri;
            }
		}

		/**
		 * On Click System Download Event Handler
		 *
		 * @param {Event} e		Click Event
		 * 
		 * @memberof ModuleLoader
		 */
		_onClickSystemDownload(e) {
            let self = this,
                target = e.target,
                dirname, fData = new FormData();
            if (target.tagName.toLowerCase() !== 'button') {
                target = Selectors.closest(target, 'button');
            }
            fData.append('dirname', 'Core');
            fData.append('op', 'download');
            self.overlay.setLoadingContent();
            self.overlay.append();
            require(['http/request', 'notifier'], (Request) => {
                Request.post(
                    self.remote, fData, self._onDownloaded, self._onFail, self._onAlways);
            });
        }

        /**
         * Ausführung bei fehlgeschlagener Abfrage
         * 
		 * Wird ausgeführt, wenn keine Antwort vom Server kommt bzw die 
         * Anfrage fehlschlägt
         * 
         * @param {Object}  xhr    		Das XHR Abfrage Object
         * @param {String}	textStatus 	Text Status Message
		 * 
		 * @memberof ModuleLoader
         */
        _fail (xhr, textStatus) {
            A.Logger.writeLog(textStatus);
        }
        
        /**
         * Auführung bei jeder Abfrage
         * 
         * @param {String} textStatus Text Status
         * 
		 * @memberof ModuleLoader
         */
        _always (xhr, response, textStatus) {
			let self = this,
                o = self.options;
			A.Logger.writeLog(textStatus);
			if(typeof o.onResponse === 'function') {
				o.onResponse(xhr, response, textStatus, self);
			}
			self._processing = false;
			if(self.overlay.appended) {
				self.overlay.remove();
			}
			let loading = document.getElementById('loading');
            if(loading) loading.parentNode.removeChild(loading);
        }

		/**
		 * Prepare Module
		 *
		 * @memberof ModuleLoader
		 */
		_prepare() {
			let self = this,
                element = self.element,
                o = self.options;
            self.target = Selectors.q('#module-response');
            self.remote = o.remote;
            self.lastButton = false;
            self.button = Selectors.q('#force-system-download');
            self.overlay = Overlay.init(document.body);
		}

	}

	return ModuleLoader;
});
