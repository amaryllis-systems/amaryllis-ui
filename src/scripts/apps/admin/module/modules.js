/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("apps/admin/module/modules", [
	"core/base",
	"core/classes",
	"core/selectors",
	"events/event",
	"templates/template-engine",
	"apps/ui/informations/overlay",
	"core/acms"
], (Base, Classes, Selectors, AcmsEvent, TemplateEngine, Overlay, A) => {

	/**
	 * Modules Admin
	 *
	 * @class
	 * @extends {Base}
	 * @author qm-b <https://bitbucket.org/qm-b/>
	 */
	class ModulesAdmin extends Base {

		/**
		 * Module Namespace
		 *
		 * @readonly
		 * @static
		 * @memberof ModulesAdmin
		 */
		static get NS() {
			return "apps.admin.module.modules";
		}

		/**
		 * Module Name
		 *
		 * @readonly
		 * @static
		 * @memberof ModulesAdmin
		 */
		static get MODULE() {
			return "Module Admin";
		}

		/**
		 * Default Options
		 *
		 * @readonly
		 * @static
		 * @memberof ModulesAdmin
		 */
		static get DEFAULT_OPTIONS() {
			return {
				remote: null,
				css: ModulesAdmin.css,
				themecss: false
			};
		}

		/**
		 * Custom Stylesheet
		 *
		 * @readonly
		 * @static
		 * @memberof ModulesAdmin
		 */
		static get css() {
			return "media/css/admin/modules-admin";
		}

		/**
		 * Module Init
		 *
		 * @static
		 * 
		 * @param {HTMLElement} element		HTML Wrapper Element
		 * @param {Object|null} options		optional custom Options
		 * 
		 * @returns {ModulesAdmin} New Instance
		 * 
		 * @memberof ModulesAdmin
		 */
		static init(element, options) {
			let m = new ModulesAdmin(element, options);
			m.element.acmsData(ModulesAdmin.NS, m);
			
			return m;
		}

		/**
		 * Creates an instance of ModulesAdmin.
		 * 
		 * @param {HTMLElement} element		HTML Wrapper Element
		 * @param {Object|null} options		optional custom Options
		 * 
		 * @memberof ModulesAdmin
		 */
		constructor(element, options) {
			super(element, options);
			this._initialize();
			this.check();
		}

		set updateAll(flag) {
			this._updateAll = (true === flag);
		}

		get updateAll() {
			return this._updateAll;
		}

		get processing() {
			return this._processing;
		}

		set processing(flag) {
			this._processing = (true === flag);
		}

		/**
		 * Check for Updates
		 *
		 * @memberof ModulesAdmin
		 */
		check() {
			let self = this, 
				o = self.options,
				wrapper = Selectors.q('#installed-modules'),
				 fData = new FormData(), found, mod, j, mods = [];
			if(this.processing) return;
			this.processing = true;
			found = Selectors.qa('.module-card', wrapper);
			if(!found.length) {
				return;
			}
			for (j = 0; j < found.length; j++) {
                mod = found[j];
				mods[mod.acmsData('dirname')] = parseInt(mod.acmsData('build'));
			}
			fData.append('op', 'checkupdates');
			fData.append('modules', 'modules');
			require(['http/request', 'notifier'], (Request) => {
				Request.post(o.remote, fData, self._onUpdateRequest, self._fail, self._always);
			});
		}

		/**
		 * Setup Event Listener
		 *
		 * @memberof ModulesAdmin
		 */
		listen() {
			let self = this, forms = self.forms, j, form, btn, av = self.availableUpdates;
            for (j = 0; j < forms.length; j++) {
                form = forms[j];
                btn = Selectors.q('button[type=submit]', form);
                if(btn) AcmsEvent.add(btn, 'click', self._onSubmit);
			}
			AcmsEvent.on(av, 'click', '[data-role="update-module"]', self._onClickUpdateModule, true);
			AcmsEvent.on(av, 'click', '[data-role="update-all"]', self._onClickUpdateAllModules, true);
		}

		/**
		 * Update Module
		 *
		 * @param {String} dirname	Module Dirname
		 * 
		 * @memberof ModulesAdmin
		 */
		update(dirname) {
			let self = this, o = self.options, fData = new FormData();
			fData.append('op', 'download');
			fData.append('dirname', dirname);
			
			require(['http/request', 'notifier'], (Request) => {
				if(!self.overlay.appended) {
					self.overlay.append();
				}
				Request.post(o.remote, fData, self._onDownloaded, self._fail, self._always);
			});
		}

		/**
		 * On Module Downloaded Response Object
		 *
		 * @param {Object} response	Response Object from remote server
		 * 
		 * @memberof ModulesAdmin
		 */
		_onDownloaded(response) {
			A.Notifications.createFromResponse(response);
			let self = this, o = self.options, fData = new FormData();
			fData.append('op', 'doupdate');
			fData.append('dirname', self.current.acmsData('dirname'));
			
			require(['http/request', 'notifier'], (Request) => {
				if(!self.overlay.appended) {
					self.overlay.append();
				}
				Request.post(o.mremote, fData, self._onUpdated);
			});
		}

		/**
		 * On Module Updated Response Callback
		 *
		 * @param {Object} response		Remote Response Object
		 * 
		 * @memberof ModulesAdmin
		 */
		_onUpdated(response) {
			A.Notifications.createFromResponse(response);
			let self = this, o = self.options, current = self.current, mod;
			current.parentNode.removeChild(self.current);
			mod = Selectors.q('.module-card', self.availableUpdates);
			self.current = null;
			if(!mod) {
				if(self.overlay.appended) {
					self.overlay.remove();
				}
				if(self.updateAll) self.updateAll = false;
				let wrapper = Selectors.q('[data-role="update-all-wrapper"]');
				if(wrapper) wrapper.parentNode.removeChild(wrapper);
			
				return;
			}
			if(!self.updateAll) return;
			self.current = mod;
			self.update(mod.acmsData('dirname'));
		}

		/**
		 * on Click module Update Event Handler
		 *
		 * @param {Event} 	e		Event
		 * 
		 * @memberof ModulesAdmin
		 */
		_onClickUpdateModule(e) {
			e.preventDefault();
			e.stopPropagation();
			let self = this,target = e.target, mod = Selectors.closest(target, '.module-card');
			if(!mod) {
				return false;
			}
			self.current = mod;
			self.update(mod.acmsData('dirname'));
		}

		/**
		 * on Click Update all Modules Event Handler
		 *
		 * @param {Event} 	e		Event
		 * 
		 * @memberof ModulesAdmin
		 */
		_onClickUpdateAllModules(e) {
			e.preventDefault();
			e.stopPropagation();
			let self = this,target = e.target, mod = Selectors.q('.module-card', self.availableUpdates);
			if(!mod) {
				return false;
			}
			self.updateAll = true;
			self.current = mod;
			self.update(mod.acmsData('dirname'));
		}

		/**
		 * On Update Check response
		 *
		 * @param {Object} response	Response Object from Remote
		 * 
		 * @memberof ModulesAdmin
		 */
		_onUpdateRequest(response) {
			this.processing = false;
			if(response.status !== "success") {
				A.Notifications.createFromResponse(response);
				return;
			}
			if(!response.versionAvailable) {
				return;
			}
			let self = this, updates = response.newVersions;
			self.updates = updates;
			for(let dirname in updates) {
				if(typeof updates[dirname] === "undefined") {
					continue;
				}
				self._processUpdateAvailable(dirname, updates[dirname]);
			}
		}

		/**
		 * Process available module update from update check
		 *
		 * @param {String} dirname
		 * @param {Object} updated	Version Infos
		 * @memberof ModulesAdmin
		 */
		_processUpdateAvailable(dirname, updated) {
			let build = updated.build, version = updated.version;
			let self = this, o = self.options, 
				installed = Selectors.q('#installed-modules'), 
				mod = Selectors.q('[data-dirname="'+dirname+'"]', installed);
			if(!mod) {
				A.Logger.logError('Modul nicht gefunden: '+ dirname);
			}
			let img = Selectors.q('img.module-logo', mod);
			let template = 'templates/html/admin/module/update-available.hbs',
				tData = {
					module: {
						dirname: dirname,
						build: build,
						version: version,
						name: mod.acmsData('name'),
						image: img ? img.getAttribute('src') : '',
						released: updated.releaseDate
					}
				}, appendTo = self.availableUpdates;
			TemplateEngine.compileResource(template, tData, self._onAppended, appendTo);
		}

		/**
		 * On Appended available update Callback
		 *
		 * @memberof ModulesAdmin
		 */
		_onAppended() {
			let self = this;
			let all = Selectors.qa('.module-card', self.availableUpdates);
			if(all.length !== Object.keys(self.updates).length) {
				return;
			}
			let template = 'templates/html/admin/module/update-all.hbs',
				tData = {
				}, appendTo = self.availableUpdates;
			TemplateEngine.compileResource(template, tData, (content) => {
				
			}, appendTo);
		}

		/**
		 * on Click Submit Form
		 *
		 * @param {Event} e		Click Event
		 * 
		 * @memberof ModulesAdmin
		 */
		_onClickSubmit(e) {
			let self = this,
                    form = e.target;
			e.preventDefault();
			e.stopPropagation();
            if (form.tagName.toLowerCase() !== 'form') {
                form = Selectors.closest(form, 'form');
            }
            self.currentForm = form;
            let fData = new FormData(form);
            require(['http/request', 'notifier'], function (Request) {
                Request.post(form.getAttribute('action'), fData, self._done, self._fail, self._always);
            });

            return false;
		}

		/**
		 * on Remote Done Callback
		 *
		 * @param {object} response 	Response object
		 * 
		 * @memberof ModulesAdmin
		 */
		_done(response) {
			A.Notifications.createFromResponse(response);
            if (response.status === "success") {
                if (this.currentForm) {
                    this.currentForm.reset();
                    this.currentForm = null;
                }
                window.location.reload();
            }
		}
		
        /**
         * Ausführung bei fehlgeschlagener Abfrage
         * 
		 * Wird ausgeführt, wenn keine Antwort vom Server kommt bzw die 
         * Anfrage fehlschlägt
         * 
         * @param {Object}  xhr    		Das XHR Abfrage Object
         * @param {String}	textStatus 	Text Status Message
		 * 
		 * @memberof ModulesAdmin
         */
        _fail (xhr, textStatus) {
			A.Logger.logError(textStatus);
			this.processing = false;
        }
        
        /**
         * Auführung bei jeder Abfrage
         * 
         * @param {String} textStatus Text Status
         * 
		 * @memberof ModulesAdmin
         */
        _always (xhr, response, textStatus) {
			let self = this,
                o = self.options;
			A.Logger.writeLog(textStatus);
			if(typeof o.onResponse === 'function') {
				o.onResponse(xhr, response, textStatus, self);
			}
			if(this.overlay.appended) {
				this.overlay.remove();
			}
        }

		/**
		 * prepare Modul
		 *
		 * @memberof ModulesAdmin
		 */
		_prepare() {
			let self = this,
				o = self.options,
				element = self.element;
			let forms = Selectors.qa('.module-edit-form', element);
			self.forms = forms;
			self.availableUpdates = Selectors.q('#updates-available');
			self.overlay = Overlay.init(document.body);
		}

	}

	return ModulesAdmin;
});
