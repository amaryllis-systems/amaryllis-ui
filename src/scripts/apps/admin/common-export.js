/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('apps/admin/common-export', [
	"core/base",
    'core/selectors',
    'core/classes',
	'events/event',
	'tools/string/string-tool',
    'core/acms'
], (Base, Selectors, Classes, AcmsEvent, StringTool, A) => {

    /**
	 * Common Admin Export Trigger Handler
	 * 
	 * Export Helper for Export Buttons in Dashboard
	 * 
	 * @class
	 * @extends {Base}
	 * @author qm-b <https://bitbucket.org/qm-b/>
	 */
	class CommonExport extends Base {

		/**
		 * Module Namespace
		 *
		 * @readonly
		 * @static
		 * @memberof CommonExport
		 */
		static get NS() {
			return "common-module.apps.admin.common-export";
		}

		/**
		 * Module Name
		 *
		 * @readonly
		 * @static
		 * @memberof CommonExport
		 */
		static get MODULE() {
			return "Common Export";
		}

		/**
		 * Default Options
		 *
		 * @readonly
		 * @static
		 * @memberof CommonExport
		 */
		static get DEFAULT_OPTIONS() {
			return {
				remote: null
			};
		}

		/**
		 * Module Init
		 *
		 * @static
		 * 
		 * @param {HTMLElement} 	element		Wrapper Element
		 * @param {Object|null} 	options		Custom Options
		 * 
		 * @returns {CommonExport}	New Instance
		 * 
		 * @memberof CommonExport
		 */
		static init(element, options) {
			let m = new CommonExport(element, options);
			m.element.acmsData(CommonExport.NS, m);

			return m;
		}

		/**
		 * Creates an instance of CommonExport.
		 * 
		 * @param {HTMLElement} 	element		Wrapper Element
		 * @param {Object|null} 	options		Custom Options
		 * 
		 * @memberof CommonExport
		 */
		constructor(element, options) {
			super(element, options);
			this._initialize();
		}

		get processing() {
			return this._processing;
		}

		set processing(flag) {
			this._processing = (true === flag);
		}

		listen() {
			let self = this;
			AcmsEvent.on(self.element, 'click', 'button[data-trigger=export]', self._onClick, true);
		}

		/**
		 * On Click Export Button Event Handler
		 *
		 * @param {Event} e	Click Event
		 * 
		 * @memberof CommonExport
		 * @private
		 */
		_onClick(e) {
			let self = this, 
                o = self.options, 
                target = e.target, fData, remote;
			e.preventDefault();
			if(self.processing) {
				return false;
			}
			self.processing = true;
            if(target.tagName.toLowerCase() !== 'button') {
                target = Selectors.closest(target, 'button');
            }
            remote = target.acmsData('remote');
            if(!remote || false === StringTool.isUrl(remote)) {
                return false;
            }
			fData = new FormData();
			fData.append('format', (target.acmsData('format') || ''));
			fData.append('op', 'export');
            
            require(['http/request'], function(Request) {
                Request.post(remote, fData, self._done, self._fail, self._always);
            });
            
            return false;
		}

		_done(response) {
            A.Notifications.createFromResponse(response);
		}
		
		_fail(xhr, textStatus) {
            A.Logger.logWarning(textStatus);
		}
		
		_always(xhr, textStatus) {
			A.Logger.logInfo(textStatus);
			this.processing = false;
		}

		_prepare() {
			this.processing = false;
		}
	}

    return CommonExport;

});

