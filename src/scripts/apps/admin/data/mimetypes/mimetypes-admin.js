/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/admin/data/mimetypes/mimetypes-admin", [
    "core/base",
    "apps/ui/components/dual-list",
    "templates/template-engine",
    "core/classes",
    "core/selectors",
    "events/event",
    "core/acms"
], (Base, DualList, TemplateEngine, Classes, Selectors, AcmsEvent, A) => {

    const FIELDS = [
        'maxfiles', 'maxsize', 'maxwidth', 'maxheight',
        'minfiles', 'minsize', 'minwidth', 'minheight'
    ];

    /**
     * Mimetypes Admin
     *
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class MimetypesAdmin extends Base {

        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof MimetypesAdmin
         */
        static get NS() {
            return "acms.apps.admin.data.mimetypes.mimetypes-admin";
        }

        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof MimetypesAdmin
         */
        static get MODULE() {
            return "Mimetypes Admin";
        }

        static get DEFAULT_OPTIONS() {
            return {
                remote: null,
                templates: {
                    system: 'templates/html/admin/data/mimetypes/system-mimetypes.hbs',
                    available: 'templates/html/admin/data/mimetypes/mimetype.hbs'
                },
                selectors: {
                    duallist: '.dual-list',
                    hidden: '.hidden',
                    multiupload: '[data-multi-upload]'
                },
                classNames: {
                    hidden: 'hidden',
                }
            };
        }

        static init(element, options) {
            let m = new MimetypesAdmin(element, options);
            m.element.acmsData(MimetypesAdmin.NS, m);
            
            return m;
        }

        constructor(element, options) {
            super(element, options);
            this._initialize();
        }

        /**
         * Gets the Dual List instance
         * 
         * @var {DualList}
         * @readonly
         * @memberof MimetypesAdmin
         */
        get list() {
            return this._list;
        }

        listen() {
            let self = this,
                o = self.options,
                element = self.element;
            AcmsEvent.on(element, 'change', 'select', self._onChange);
            AcmsEvent.on(element, 'blur', 'input:not([type="search"])', self._onBlur);
            AcmsEvent.on(element, 'change', 'input[type="checkbox"]', self._onBlur);
        }

        refresh() {
            let self = this,
                o = self.options,
                templates = o.templates,
                done = response => {
                    let allowed = {},
                        system = {},
                        isMulti = response.setup.isMultiUpload,
                        prop, itm;
                    self._updateMulti(isMulti);
                    for(prop in response.allowed) {
                        if(response.allowed.hasOwnProperty(prop)) {
                            itm = response.allowed[prop];
                            itm.group = itm.name.split('/')[0];
                            allowed[prop] = itm;
                        }
                    }
                    for(prop in response.system) {
                        if(response.system.hasOwnProperty(prop) && !response.allowed.hasOwnProperty(prop)) {
                            itm = {
                                extension: prop,
                                name: response.system[prop]
                            };
                            itm.group = itm.name.split('/')[0];
                            system[prop] = itm;
                        }
                    }
                    let tdata = {
                        systemMimetypes: system,
                        allowedMimetypes: allowed
                    }, cb = () => {};
                    let ulLeft = Selectors.q('ul', self.list.leftList);
                    let ulRight = Selectors.q('ul', self.list.rightList);
                    ulLeft.innerHTML = '';
                    ulRight.innerHTML = '';
                    TemplateEngine.compileResource(templates.system, tdata, cb, ulLeft);
                    TemplateEngine.compileResource(templates.available, tdata, cb, ulRight);
                    let mimetype = allowed[Object.keys(allowed)[0]];
                    if(mimetype) {
                        FIELDS.forEach(field => {
                            let f = Selectors.q('input[name="'+field+'"]', self.element);
                            if(f) {
                                f.value = mimetype[field];
                                AcmsEvent.fireChange(f);
                            }
                        });
                        let chbx = Selectors.q('input[name="allow_overwrite"]', self.element);
                        if(chbx) {
                            if(mimetype.allow_overwrite) {
                                chbx.checked = true;
                                chbx.setAttribute('checked', 'checked');
                            } else {
                                chbx.checked = false;
                                chbx.removeAttribute('checked');
                            }
                            AcmsEvent.fireChange(chbx);
                        }
                    }
                };
            let form = Selectors.q('form', self.element), fData = new FormData(form);
            fData.append('op', 'load');
            self._process(fData, done);
        }

        _onChange(e) {
            let input = e.target;
            if(input.type !== 'search') {
                this.refresh();
            }
        }

        _updateMulti(isMulti) {
            let self = this, o = self.options, sel = o.selectors, eles = Selectors.qa(sel.multiupload, self.element);
            eles.forEach(ele => {
                if(isMulti) {
                    Classes.removeClass(ele, o.classNames.hidden);
                } else {
                    Classes.addClass(ele, o.classNames.hidden);
                }
            });
        }

        /**
         * On Keyup event Handler for input fields
         *
         * @param {Event} e
         * @memberof MimetypesAdmin
         */
        _onKeyUp(e) {
            let target = e.target;
        }

        _onBlur(e) {
            let self = this,
                o = self.options,
                formField = e.target,
                val = formField.value;
            if (formField.type !== 'checkbox' && val == formField.acmsData('value')) {
                return;
            }
            let form = Selectors.q('form', self.element), fData = new FormData(form),
                done = (response) => {
                    if (response.status === 'success') {
                        formField.acmsData('value', formField.value);
                        formField.setAttribute('data-value', formField.value);
                    }
                    A.Notifications.createFromResponse(response);
                };
            fData.append('op', 'update');
            self._process(fData, done);
        }

        _batchSubmit() {
            let self = this,
                o = self.options, 
                form = Selectors.q('form', self.element), 
                fData = new FormData(form);
            fData.append('op', 'batchSave');
            self._process(fData, () => {});
        }

        _batchRemove() {
            let self = this,
                o = self.options, 
                form = Selectors.q('form', self.element), 
                fData = new FormData(form);
            fData.append('op', 'batchRemove');
            self._process(fData, () => {});
        }

        _prepare() {
            let self = this, o = self.options, duallistOptions, ele, form;
            duallistOptions = o.duallist || {};
            duallistOptions.afterMoveToRight = (element, items, listLeft, listRight) => {
                
                self._batchSubmit();
            };
            duallistOptions.afterMoveToLeft = (element, items, listLeft, listRight) => {
                
                self._batchRemove();
            };
            ele = Selectors.q('.dual-list', self.element);
            self._list = DualList.init(ele, duallistOptions);
            form = Selectors.q('form', self.element);
            form.onsubmit = e => {
                if(e) e.preventDefault();
                return false;
            };
            this.refresh();
        }

        _process(fData, done) {
            let self = this, o = self.options;
            require(['http/request', 'notifier'], Request => {
                Request.post(o.remote, fData, done, self._fail, self._always);
            });
        }

		_fail(xhr, textStatus) {
            A.Logger.logWarning(textStatus);
		}
		
		_always(xhr, textStatus) {
			A.Logger.logInfo(textStatus);
			this._processing = false;
		}

        /**
         * Build Form Data
         *
         * @param {String} op 
         * @param {HTMLElement} item
         * 
         * @returns {FormData}
         * @memberof MimetypesAdmin
         */
        _buildFormData(op, item) {
            let self = this,
                    o = self.options,
                    element = self.element;
            let fData = new FormData();
            fData.append('op', op);
            let field = Selectors.q('select[name="field"]', element) || Selectors.q('input[name="field"]', element);
            fData.append('field', field.value);
            FIELDS.forEach(field => {
                let f = Selectors.q('input[name="'+field+'"]', element);
                fData.append(field, f.value);
            });
            fData.append('group_id', Selectors.q('select[name="group_id"]', element).value);
            if (item) {
                fData.append('extension', item.acmsData('extension'));
                fData.append('mime', item.acmsData('mime'));
            }

            return fData;
        }

    }

    return MimetypesAdmin;
});
