/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("apps/admin/data/language/language-admin", [
    "core/base",
    "templates/template-engine",
    "core/classes",
    "core/selectors",
    "events/event",
    "core/acms"
], (Base, TemplateEngine, Classes, Selectors, AcmsEvent, A) => {

    /**
     * Language Admin
     *
     * @class 
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class LanguageAdmin extends Base {

        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof LanguageAdmin
         */
        static get NS() {
            return "acms.apps.admin.data.language.language-admin";
        }


        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof LanguageAdmin
         */
        static get MODULE() {
            return "Language Admin";
        }

        /**
         * Default Options
         *
         * @readonly
         * @static
         * @memberof LanguageAdmin
         */
        static get DEFAULT_OPTIONS() {
            return {
                remote: null,
                templates: {
                    tabs: 'templates/html/admin/data/language/tabs.hbs',
                    language: 'templates/html/admin/data/language/language-installed.hbs',
                    uninstalled: 'templates/html/admin/data/language/language-uninstalled.hbs',
                    settings: 'templates/html/admin/data/language/settings.hbs',
                }
            };
        }

        /**
         * Module Init
         *
         * @static
         * 
         * @param {HTMLElement} element Wrapper Element
         * @param {Object} options  optional custom options
         * 
         * @returns {LanguageAdmin}
         * 
         * @memberof LanguageAdmin
         */
        static init(element, options) {
            let m = new LanguageAdmin(element, options);
            m.element.acmsData(LanguageAdmin.NS, m);

            return m;
        }


        /**
         * Creates an instance of LanguageAdmin.
         * 
         * @param {HTMLElement} element Wrapper Element
         * @param {Object} options  optional custom options
         * 
         * @memberof LanguageAdmin
         */
        constructor(element, options) {
            super(element, options);
            this._initialize();
            let self = this,
                o = self.options, 
                template = o.templates.tabs,
                tdata = {}, cb = () => {
                    return self.refresh();
                };
            TemplateEngine.compileResource(template, tdata, cb, element);
        }

        /**
         * Remote Refresh panels
         *
         * @memberof LanguageAdmin
         */
        refresh() {
            let self = this,
                    element = self.element,
                    o = self.options, fData = new FormData();
            fData.append('op', 'load');
            fData.append('token', o.token);
            require(['http/request', 'notifier'], Request => {
                Request.post(o.remote, fData, self._onLoadedLanguage);
            });
        }

        /**
         * Setup Event listeners on module init
         *
         * @memberof LanguageAdmin
         */
        listen() {
            let self = this,
                o = self.options, 
                element = self.element, tabs = Selectors.q('.language-tabs > .tab', element);
            if(tabs === null) {
                tabs = element;
            }
            AcmsEvent.on(tabs, 'change', 'input[type="text"]', self._onChange);
            AcmsEvent.on(tabs, 'change', 'input[type="checkbox"]', self._onChange);
            AcmsEvent.on(tabs, 'change', 'textarea', self._onChange);
            AcmsEvent.on(tabs, 'change', 'input[type="file"]', self._onUpload);
            AcmsEvent.on(element, 'change', 'select', self._onChangeSelect);
            AcmsEvent.on(tabs, 'click', '[data-trigger]', self._onClickTrigger);
        }

        _onUpload(e) {
            let self = this, o = self.options, target = e.target, form = Selectors.closest(target, 'form');
            
            let fData = new FormData(form);
            require(['http/request', 'notifier'], Request => {
                Request.post(o.remote, fData, self._onUploaded);
            });
        }

        _onUploaded(response) {
            A.Notifications.createFromResponse(response);
            this.refresh();
        }

        /**
         * On change Select callbacks
         *
         * @param {Event} e Change Event
         * 
         * @memberof LanguageAdmin
         */
        _onChangeSelect(e) {
            let self = this,
                o = self.options,
                target = e.target,
                item = Selectors.closest(target, '[data-foldername]');
            let fData = new FormData();
            fData.append('op', 'setDefault');
            fData.append('setting', target.name);
            fData.append('value', target.options[target.selectedIndex].value);
            fData.append('token', o.token);
            require(['http/request', 'notifier'], Request => {
                Request.post(o.remote, fData, self._onTriggered);
            });
        }

        /**
         * On Click trigger
         *
         * @param {Event} e Click Event
         * 
         * @memberof LanguageAdmin
         */
        _onClickTrigger(e) {
            let self = this,
                o = self.options,
                target = e.target,
                trigger = Selectors.matches(target, '[data-trigger]') ? target : Selectors.closest(target, '[data-trigger]'),
                item = Selectors.closest(target, '[data-foldername]');

            let fData = new FormData();
            fData.append('op', trigger.acmsData('trigger'));
            fData.append('agree', 1);
            fData.append('foldername', item.acmsData('foldername'));
            fData.append('token', o.token);
            require(['http/request', 'notifier'], Request => {
                Request.post(o.remote, fData, self._onTriggered);
            });
        }

        /**
         * On Triggered action callback
         *
         * @param {Object} response  Server Response Object
         * 
         * @memberof LanguageAdmin
         */
        _onTriggered(response) {
            let self = this;
            A.Notifications.createFromResponse(response);
            if(response.status === 'success') {
                self.refresh();
            }
        }

        /**
         * On Change Form Field Event Handler
         *
         * @param {Event} e     Change Event
         * 
         * @memberof LanguageAdmin
         */
        _onChange(e) {
            let self = this,
                o = self.options,
                target = e.target, form = Selectors.closest(target, 'form');
            let fData = new FormData(form);
            if(target.type === 'checkbox' && !target.checked) {
                fData.append(target.name, false);
            }
            require(['http/request', 'notifier'], Request => {
                Request.post(o.remote, fData, self._onUpdatedLanguage);
            });
        }

        /**
         * On Updated Language Callback
         *
         * @param {Object} response     Server response object
         * 
         * @memberof LanguageAdmin
         */
        _onUpdatedLanguage(response) {
            A.Notifications.createFromResponse(response);
            this.refresh();
        }

        /**
         * On Loaded Language Callback
         *
         * @param {Object} response     Server response object
         * 
         * @memberof LanguageAdmin
         */
        _onLoadedLanguage(response) {
            if(response.status !== 'success') {
                return A.Notifications.createFromResponse(response);
            }
            let self = this,
                o = self.options, 
                element = self.element,
                template = o.templates.language, 
                utemplate = o.templates.uninstalled,
                stemplate = o.templates.settings,
                cb = () => {
                };
            let settingsLanguage = {};
            let installed = response.languages.installed, ipanel = Selectors.q('[data-panel="installed"]', element), folder;
            ipanel.innerHTML = '';
            for(folder in installed) {
                let tdata = {
                    language: installed[folder],
                    settings: response.settings,
                    total: Object.keys(installed).length,
                    token: o.token,
                };
                TemplateEngine.compileResource(template, tdata, cb, ipanel);
                if(installed[folder].selectable && installed[folder].active) {
                    settingsLanguage[folder] = installed[folder];
                }
            }
            let uninstalled = response.languages.uninstalled, upanel = Selectors.q('[data-panel="uninstalled"]', element), foldername;
            upanel.innerHTML = '';
            for(foldername in uninstalled) {
                let tdata = {
                    language: uninstalled[foldername],
                    settings: response.settings,
                    total: Object.keys(uninstalled).length
                };
                TemplateEngine.compileResource(utemplate, tdata, cb, upanel);
            }
            

            let settdata = {
                    language: settingsLanguage,
                    settings: response.settings
                };
            let old = Selectors.q('.language-settings', element);
            if(old) {
                old.parentNode.removeChild(old);
            }
            TemplateEngine.compileResource(stemplate, settdata, cb, element);
        }

    }

    return LanguageAdmin;
});
