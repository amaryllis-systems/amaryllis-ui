/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("apps/admin/data/theme/themes-admin", [
    "core/base",
    "apps/admin/data/theme/theme",
    "templates/template-engine",
    "core/classes",
    "core/selectors",
    "events/event",
    "core/acms"
], (Base, Theme, TemplateEngine, Classes, Selectors, AcmsEvent, A) => {

    /**
     * Themes Admin
     *
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class ThemesAdmin extends Base {

        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof ThemesAdmin
         */
        static get NS() {
            return "acms.apps.admin.data.theme.themes-admin";
        }


        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof ThemesAdmin
         */
        static get MODULE() {
            return "Themes Admin";
        }

        /**
         * Default Options
         *
         * @readonly
         * @static
         * @memberof ThemesAdmin
         */
        static get DEFAULT_OPTIONS() {
            return {
                remote: null,
                templates: {
                    tabs: 'templates/html/admin/data/theme/tabs.hbs',
                    theme: 'templates/html/admin/data/theme/theme.hbs',
                    uninstalled: 'templates/html/admin/data/theme/theme-uninstalled.hbs',
                    settings: 'templates/html/admin/data/theme/settings.hbs',
                }
            };
        }

        /**
         * Module Init
         *
         * @static
         * 
         * @param {HTMLElement} element Wrapper Element
         * @param {Object} options  optional custom options
         * 
         * @returns {ThemesAdmin}
         * 
         * @memberof ThemesAdmin
         */
        static init(element, options) {
            let m = new ThemesAdmin(element, options);
            m.element.acmsData(ThemesAdmin.NS, m);

            return m;
        }


        /**
         * Creates an instance of ThemesAdmin.
         * 
         * @param {HTMLElement} element Wrapper Element
         * @param {Object} options  optional custom options
         * 
         * @memberof ThemesAdmin
         */
        constructor(element, options) {
            super(element, options);
            this._initialize();
            let self = this,
                o = self.options, 
                template = o.templates.tabs,
                tdata = {}, cb = () => {
                    return self.refresh();
                };
            TemplateEngine.compileResource(template, tdata, cb, element);
        }

        /**
         * Remote Refresh panels
         *
         * @memberof ThemesAdmin
         */
        refresh() {
            let self = this,
                    element = self.element,
                    o = self.options, fData = new FormData();
            fData.append('op', 'loadAdminThemes');
            require(['http/request', 'notifier'], Request => {
                Request.post(o.remote, fData, self._onLoadedThemes);
            });
        }

        /**
         * Setup Event listeners on module init
         *
         * @memberof ThemesAdmin
         */
        listen() {
            let self = this,
                o = self.options, 
                element = self.element, tabs = Selectors.q('.theme-tabs > .tab', element);
            if(tabs === null) {
                tabs = element;
            }
            AcmsEvent.on(tabs, 'change', 'input[name="name"]', self._onChange);
            AcmsEvent.on(tabs, 'change', 'input[type="checkbox"]', self._onChange);
            AcmsEvent.on(tabs, 'change', 'input[type="file"]', self._onUpload);
            AcmsEvent.on(element, 'change', 'select', self._onChangeSelect);
            AcmsEvent.on(tabs, 'click', '[data-trigger]', self._onClickTrigger);
        }

        _onUpload(e) {
            let self = this, o = self.options, target = e.target, form = Selectors.closest(target, 'form');
            
            let fData = new FormData(form);
            require(['http/request', 'notifier'], Request => {
                Request.post(o.remote, fData, self._onUploaded);
            });
        }

        _onUploaded(response) {
            A.Notifications.createFromResponse(response);
            this.refresh();
        }

        /**
         * On change Select callbacks
         *
         * @param {Event} e Change Event
         * 
         * @memberof ThemesAdmin
         */
        _onChangeSelect(e) {
            let self = this,
                o = self.options,
                target = e.target,
                item = Selectors.closest(target, '[data-foldername]');
            let fData = new FormData();
            fData.append('op', 'setDefault');
            fData.append('setting', target.name);
            fData.append('value', target.options[target.selectedIndex].value);
            require(['http/request', 'notifier'], Request => {
                Request.post(o.remote, fData, self._onTriggered);
            });
        }

        /**
         * On Click trigger
         *
         * @param {Event} e Click Event
         * 
         * @memberof ThemesAdmin
         */
        _onClickTrigger(e) {
            let self = this,
                o = self.options,
                target = e.target,
                trigger = Selectors.matches(target, '[data-trigger]') ? target : Selectors.closest(target, '[data-trigger]'),
                item = Selectors.closest(target, '[data-foldername]');

            let fData = new FormData();
            fData.append('op', trigger.acmsData('trigger'));
            fData.append('agree', 1);
            fData.append('foldername', item.acmsData('foldername'));
            require(['http/request', 'notifier'], Request => {
                Request.post(o.remote, fData, self._onTriggered);
            });
        }

        /**
         * On Triggered action callback
         *
         * @param {Object} response  Server Response Object
         * 
         * @memberof ThemesAdmin
         */
        _onTriggered(response) {
            let self = this;
            A.Notifications.createFromResponse(response);
            if(response.status === 'success') {
                self.refresh();
            }
        }

        /**
         * On Change Form Field Event Handler
         *
         * @param {Event} e     Change Event
         * 
         * @memberof ThemesAdmin
         */
        _onChange(e) {
            let self = this,
                o = self.options,
                target = e.target, form = Selectors.closest(target, 'form');
            let fData = new FormData(form);
            if(target.type === 'checkbox' && !target.checked) {
                fData.append(target.name, false);
            }
            require(['http/request', 'notifier'], Request => {
                Request.post(o.remote, fData, self._onUpdatedTheme);
            });
        }

        /**
         * On Updated Theme Callback
         *
         * @param {Object} response     Server response object
         * 
         * @memberof ThemesAdmin
         */
        _onUpdatedTheme(response) {
            A.Notifications.createFromResponse(response);
            this.refresh();
        }

        /**
         * On Loaded Themes Callback
         *
         * @param {Object} response     Server response object
         * 
         * @memberof ThemesAdmin
         */
        _onLoadedThemes(response) {
            if(response.status !== 'success') {
                return A.Notifications.createFromResponse(response);
            }
            let self = this,
                o = self.options, 
                element = self.element,
                template = o.templates.theme, 
                utemplate = o.templates.uninstalled,
                stemplate = o.templates.settings,
                cb = () => {
                };
            let settingsThemes = {};
            let installed = response.installed, ipanel = Selectors.q('[data-panel="installed"]', element), folder;
            ipanel.innerHTML = '';
            for(folder in installed) {
                let tdata = {
                    theme: installed[folder],
                    settings: response.settings,
                    total: Object.keys(installed).length
                };
                TemplateEngine.compileResource(template, tdata, cb, ipanel);
                if(installed[folder].selectable && installed[folder].active) {
                    settingsThemes[folder] = installed[folder];
                }
            }
            let uninstalled = response.uninstalled, upanel = Selectors.q('[data-panel="uninstalled"]', element), foldername;
            upanel.innerHTML = '';
            for(foldername in uninstalled) {
                let tdata = {
                    theme: uninstalled[foldername],
                    settings: response.settings,
                    total: Object.keys(uninstalled).length
                };
                TemplateEngine.compileResource(utemplate, tdata, cb, upanel);
            }
            

            let settdata = {
                    themes: settingsThemes,
                    settings: response.settings
                };
            let old = Selectors.q('.theme-settings', element);
            if(old) {
                old.parentNode.removeChild(old);
            }
            TemplateEngine.compileResource(stemplate, settdata, cb, element);
        }

    }

    return ThemesAdmin;
});
