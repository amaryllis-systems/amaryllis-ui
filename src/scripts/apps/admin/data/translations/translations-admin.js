/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("apps/admin/data/translations/translations-admin", [
    "core/base",
    "apps/ui/list-sorter",
    "templates/template-engine",
    "tools/string/unique-id",
    "apps/ui/informations/modal",
    "core/classes",
    "core/selectors",
    "events/event",
    "core/acms"
], (Base, ListSorter, TemplateEngine, UniqueId, Modal, Classes, Selectors, AcmsEvent, A) => {

    /**
     * Translations Admin
     *
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     * 
     * @property {HTMLElement} container
     * @property {ListSorter} sorter
     */
    class TranslationsAdmin extends Base {

        /**
         * Module Namespace
         *
         * @type {String}
         * 
         * @readonly
         * @static
         * @memberof TranslationsAdmin
         */
        static get NS() {
            return "acms.apps.admin.data.translations.translations-admin";
        }


        /**
         * Module Name
         * 
         * @type {String}
         * 
         * @readonly
         * @static
         * @memberof TranslationsAdmin
         */
        static get MODULE() {
            return "Translations Admin";
        }

        /**
         * Default Options
         * 
         * @type {Object}
         * 
         * @readonly
         * @static
         * @memberof TranslationsAdmin
         */
        static get DEFAULT_OPTIONS() {
            return {
                remote: null,
                token: null,
                templates: {
                    "form": "templates/html/admin/data/translations/form.hbs",
                    "row": "templates/html/admin/data/translations/translation.hbs"
                },
                /**
                 * On Loaded Callback
                 *
                 * @param {TranslationsAdmin} self
                 * 
                 * @memberof TranslationsAdmin
                 */
                onLoaded: (self) => {},
                
                /**
                 * On Deleted
                 *
                 * @param {TranslationsAdmin} self
                 * @param {Object} translation  Response Data of the deleted Translation Model
                 * @param {HTMLElement} ele The Element of the deleted Translation before removed from DOM
                 * 
                 * @memberof TranslationsAdmin
                 */
                onDeleted: (self, translation, ele) => {}
            };
        }

        static init(element, options) {
            let m = new TranslationsAdmin(element, options);
            m.element.acmsData(TranslationsAdmin.NS, m);
            
            return m;
        }


        constructor(element, options) {
            super(element, options);
            this._initialize();
            this._preloadData();
        }

        /**
         * Container
         * 
         * @type {ListSorter}
         *
         * @readonly
         * @memberof TranslationsAdmin
         */
        get sorter() {
            return this._sorter;
        }

        /**
         * Container
         * 
         * @type {HTMLElement}
         *
         * @readonly
         * @memberof TranslationsAdmin
         */
        get container() {
            return this._container;
        }

        /**
         * Textdomains
         * 
         * @type {Object}
         *
         * @readonly
         * @memberof TranslationsAdmin
         */
        get textdomains() {
            return this._textdomains;
        }

        /**
         * Locales
         * 
         * @type {Object}
         *
         * @readonly
         * @memberof TranslationsAdmin
         */
        get locales() {
            return this._locales;
        }

        /**
         * Refresh List
         *
         * @memberof TranslationsAdmin
         */
        refresh() {
            let self = this, o = self.options, fData = new FormData();
            fData.append('op', 'load');
            fData.append('token', o.token);
            fData.append('limit', 0);
            fData.append('offset', 0);
            self.container.innerHTML = '';
            require(['http/request', 'notifier'], Request => {
                Request.post(o.remote,fData, self._onLoaded, self._fail, self._always);
            });
        }

        /**
         * Setup Event Listener
         *
         * @memberof TranslationsAdmin
         * @private
         */
        listen() {
            let self = this, o = self.options, element = self.element;
            AcmsEvent.on(element, 'click', '[data-trigger="remove"]', self._onClickRemove, true);
            AcmsEvent.on(element, 'click', '[data-trigger="edit"]', self._onClickEdit, true);
            AcmsEvent.on(element, 'click', '[data-trigger="add"]', self._onClickAdd, true);
        }

        /**
         * On Click Add Event Handler
         *
         * @param {Event} e     Click Event
         * 
         * @memberof TranslationsAdmin
         * @private
         */
        _onClickAdd(e) {
            let self = this, o = self.options, tr, id, target;
            if(true === self._processing) {
                return;
            }
            self._processing = true;
            id = "translations-form-"+ UniqueId();
            target = e.target;
            if(target.tagName.toLowerCase() !== 'button') {
                target = Selectors.closest(target, 'button');
            }
            let cb = () => {
                let overlay = document.querySelector("#" + id);
                let mod = Modal.init(target, {
                    target: "#" + id,
                    onShow: function (element, target, modalBox) {},
                    onHide: function (element, target, modalBox) {
                        element.acmsData(Modal.NS, null);
                        AcmsEvent.remove(element, 'click', mod._onClickModalElement);
                        document.body.removeChild(overlay);
                        let clon = element.cloneNode(true);
                        element.parentNode.replaceChild(clon, element);
                        self._processing = false;
                    },
                });
                AcmsEvent.on(overlay, 'click', '[data-trigger="save"]', (e) => {
                    let emForm = Selectors.closest(e.target, 'form');
                    let fData = new FormData(emForm);
                    require(['http/request', 'notifier'], (Request) => {
                        Request.post(o.remote, fData, (response) => {
                            if(response.status !== 'success') {
                                return A.Notifications.createFromResponse(response);
                            }
                            let model = response.translation;
                            let textdomain = self.textdomains[model.textdomain];
                            let locale = self.locales[model.locale];
                            let data = {
                                translation: model,
                                locale: locale,
                                textdomain: textdomain
                            }, done = () => {
                                self.sorter.sort();
                                mod.hide();
                            } ;
                            TemplateEngine.compileResource(o.templates.row, data, done, self.container);
                        });
                    }, self._fail, self._always);
                }, true);
                mod.show();

            }, tdata = {
                id: id,
                translation: {
                    id: 0,
                    constant: '',
                    translation: '',
                    locale: 'de_DE',
                    textdomain: 'Theme',
                    mkjs: true,
                },
                token: o.token,
                textdomains: self.textdomains,
                locales: self.locales
            };
            TemplateEngine.compileResource(o.templates.form, tdata, cb, document.body);
        }

        /**
         * On Click Edit Event Handler
         *
         * @param {Event} e     Click Event
         * 
         * @memberof TranslationsAdmin
         * @private
         */
        _onClickEdit(e) {
            let self = this, o = self.options, translation, tdata, tr;
            translation = tr = Selectors.closest(e.target, 'tr');
            let target = e.target;
            if(target.tagName.toLowerCase() !== 'button') {
                target = Selectors.closest(target, 'button');
            }
            let id = UniqueId('translations-edit-');
            let cb = () => {
                let overlay = document.querySelector("#" + id);
                let mod = Modal.init(target, {
                    target: "#" + id,
                    onShow: function (element, target, modalBox) {},
                    onHide: function (element, target, modalBox) {
                        element.acmsData(Modal.NS, null);
                        AcmsEvent.remove(element, 'click', mod._onClickModalElement);
                        document.body.removeChild(overlay);
                        let clon = element.cloneNode(true);
                        element.parentNode.replaceChild(clon, element);
                        self._processing = false;
                    },
                });
                AcmsEvent.on(overlay, 'click', '[data-trigger="save"]', (e) => {
                    let emForm = Selectors.q('form', overlay);
                    let fData = new FormData(emForm);
                    require(['http/request', 'notifier'], (Request) => {
                        Request.post(o.remote, fData, (response) => {
                            if(response.status !== 'success') {
                                return A.Notifications.createFromResponse(response);
                            }
                            let model = response.translation;
                            tr.acmsData('constant', model.constant);
                            tr.acmsData('translation', model.translation);
                            tr.acmsData('mkjs', model.mkjs);
                            tr.acmsData('locale', model.locale);
                            tr.acmsData('textdomain', model.textdomain);
                            let tds = ['constant', 'translation', 'mkjs', 'locale', 'textdomain'];
                            tds.forEach(td => {
                                let ele = Selectors.q('[data-col="'+td+'"]', tr);
                                if(ele) {
                                    if(td === 'mkjs') {
                                        let icn = Selectors.q('.acms-icon', ele);
                                        if(true === model.mkjs) {
                                            Classes.addClass(icn, 'acms-icon-check-circle text-success');
                                            Classes.removeClass(icn, 'acms-icon-times-circle text-muted');
                                        } else {
                                            Classes.removeClass(icn, 'acms-icon-check-circle text-success');
                                            Classes.addClass(icn, 'acms-icon-times-circle text-muted');
                                        }
                                    } else if(td === 'locale' || td === 'textdomain') {
                                        let prop = td === 'locale' ? 'locales' : 'textdomains';
                                        let val = self[prop][model[td]];
                                        ele.innerHTML = val;
                                    } else {
                                        ele.innerHTML = tr.acmsData(td);
                                    }
                                }
                            });
                            mod.hide();
                        });
                    }, self._fail, self._always);
                }, true);
                mod.show();

            };
            tdata = {
                id: id,
                translation: {
                    id: translation.acmsData('id'),
                    constant: translation.acmsData('constant'),
                    translation: translation.acmsData('translation'),
                    locale: translation.acmsData('locale'),
                    textdomain: translation.acmsData('textdomain'),
                    mkjs: translation.acmsData('mkjs'),
                },
                token: o.token,
                textdomains: self.textdomains,
                locales: self.locales
            };
            TemplateEngine.compileResource(o.templates.form, tdata, cb, document.body);
        }

        /**
         * On Click Remove Event Handler
         *
         * @param {Event} e     Click Event
         * 
         * @memberof TranslationsAdmin
         * @private
         */
        _onClickRemove(e) {
            let self = this, o = self.options, translation, fData = new FormData();
            translation = Selectors.closest(e.target, 'tr');
            fData.append('op', 'remove');
            fData.append('id', translation.acmsData('id'));
            fData.append('token', o.token);
            require(['http/request', 'notifier'], Request => {
                Request.post(o.remote,fData, self._onDeleted, self._fail, self._always);
            });
        }

        /**
         * On Deleted
         *
         * @param {Response} response   Remote Response
         * 
         * @memberof TranslationsAdmin
         * @private
         */
        _onDeleted(response) {
            if(response.status !== 'success') {
                return A.Notifications.createFromResponse(response);
            }
            let self = this, o = self.options, translation = response.translation, id = translation.id;
            let removed = Selectors.q('[data-id="'+id+'"]', self.container);
            if(removed) {
                if(typeof o.onDeleted === 'function') o.onDeleted(self, translation, removed);
                removed.parentNode.removeChild(removed);
            }
        }

        /**
         * On Loaded
         *
         * @param {Response} response   Remote Response
         * 
         * @memberof TranslationsAdmin
         * @private
         */
        _onLoaded(response) {
            if(response.status !== 'success') {
                return A.Notifications.createFromResponse(response);
            }
            let self = this, o = self.options, translations = response.translations, rendered = 0, total = response.translations.length, cb = () => {
                rendered++;
                if(total === rendered) {
                    self.sorter.sort();
                    if(typeof o.onLoaded === 'function') o.onLoaded(self);
                }
            };
            translations.forEach(model => {
                let textdomain = self.textdomains[model.textdomain];
                let locale = self.locales[model.locale];
                let tData = {
                    translation: model,
                    textdomain: textdomain,
                    locale: locale
                };
                TemplateEngine.compileResource(o.templates.row, tData, cb, self.container);
            });
        }

        /**
         * preload Data
         *
         * @memberof TranslationsAdmin
         * @private
         */
        _preloadData() {
            let self = this, o = self.options, fData = new FormData(), cb;
            fData.append('op', 'loadTextdomainsAndLocales');
            fData.append('token', o.token);
            cb = (response) => {
                self._textdomains = response.textdomains;
                self._locales = response.locales;
                self.refresh();
            };
            require(['http/request', 'notifier'], Request => {
                Request.post(o.remote,fData, cb, self._fail, self._always);
            });
        }

        /**
         * prepare module
         *
         * @memberof TranslationsAdmin
         * @private
         */
        _prepare() {
            let self = this, o = self.options, sorterOptions;
            self._container = Selectors.q('tbody', self.element);
            sorterOptions = {
                direction: ListSorter.ASC, // initial direction

				source: 'data', // sort-source might be `data` to sort by data-attribute, `html` to sort by inner HTML content, `text` to sorty inner text content.
				attribute: 'constant', // initial attribute to sort by. Ignored/unused if the source is any other than `data`
				selectors: {
					active: 'active', // active elements in sort asc/desc and sort change triggers
					list: 'tbody', // list element. can be any listing wrapper
					item: 'tr', // list item element. can be any kind of item selector inside list element.
					asc: '[data-role="sort-asc"]', // trigger(s) to sort ascending. supported are button, select and radio-input elements
					desc: '[data-role="sort-desc"]', // trigger(s) to sort descending. supported are button, select and radio-input elements
					changer: '[data-role="sort-changer"]', // trigger(s) to toggle sort attribute. supported are button, select and radio-input elements
				}
            };
            self._sorter = ListSorter.init(self.element, sorterOptions);
            require(['http/css-loader'], CssLoader => {
                CssLoader.load(Modal.css);
            });
        }

		/**
         * On remote Fail
         *
         * @param {XMLHttpRequest} xhr
         * @param {String} textStatus
         * 
         * @memberof TranslationsAdmin
         * @private
         */
        _fail(xhr, textStatus) {
            A.Logger.logWarning(textStatus);
		}
		
		/**
         * On remote finsihed
         *
         * @param {XMLHttpRequest} xhr
         * @param {String} textStatus
         * 
         * @memberof TranslationsAdmin
         * @private
         */
		_always(xhr, textStatus) {
			A.Logger.logInfo(textStatus);
			this._processing = false;
		}

    }

    return TranslationsAdmin;
});
