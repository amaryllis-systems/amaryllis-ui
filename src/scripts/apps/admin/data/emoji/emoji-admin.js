/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("apps/admin/data/emoji/emoji-admin", [
    "core/base",
    "apps/ui/list-sorter",
    "templates/template-engine",
    "tools/string/unique-id",
    "apps/ui/informations/modal",
    "core/classes",
    "core/selectors",
    "events/event",
    "core/acms",
    "text!apps/front/emoji/data/categories.json",
], (Base, ListSorter, TemplateEngine, UniqueId, Modal, Classes, Selectors, AcmsEvent, A, CategoriesData) => {

    /**
     * Emoji Categories from Core
     */
    const categories = JSON.parse(CategoriesData);

    /**
     * Emoji Admin
     *
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class EmojiAdmin extends Base {

        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof EmojiAdmin
         */
        static get NS() {
            return "acms.apps.admin.data.emoji.emoji-admin";
        }

        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof EmojiAdmin
         */
        static get MODULE() {
            return "Emoji Admin";
        }

        /**
         * Default Options
         *
         * @readonly
         * @static
         * @memberof EmojiAdmin
         */
        static get DEFAULT_OPTIONS() {
            return {
                remote: null,
                token: null,
                templates: {
                    "form": "templates/html/admin/data/emoji/form.hbs",
                    "row": "templates/html/admin/data/emoji/row.hbs"
                }
            };
        }

        /**
         * Module Init
         *
         * @static
         * 
         * @param {HTMLElement} element     Wrapper Element
         * @param {Object}      options     optional custom Options
         * 
         * @returns {EmojiAdmin}    New instance
         * 
         * @memberof EmojiAdmin
         */
        static init(element, options) {
            let m = new EmojiAdmin(element, options);
            m.element.acmsData(EmojiAdmin.NS, m);
            
            return m;
        }

        /**
         * Creates an instance of EmojiAdmin.
         * 
         * @param {HTMLElement} element     Wrapper Element
         * @param {Object}      options     optional custom Options
         * 
         * @memberof EmojiAdmin
         */
        constructor(element, options) {
            super(element, options);
            this._initialize();
        }

        get categories() {
            return categories;
        }

        get sorter() {
            return this._sorter;
        }

        listen() {
            let self = this, element = self.element;
            AcmsEvent.on(element, 'click', '[data-trigger="edit"]', self._onClickEdit, true);
            AcmsEvent.on(element, 'click', '[data-trigger="remove"]', self._onClickDelete, true);
            AcmsEvent.on(element, 'click', '[data-trigger="add"]', self._onClickAdd, true);
        }

        _onClickEdit(e) {
            let self = this, o = self.options, tr, id, target;
            if(true === self._processing) {
                return;
            }
            self._processing = true;
            target = e.target;
            if(target.tagName.toLowerCase() !== 'button') {
                target = Selectors.closest(target, 'button');
            }
            id = "emoji-form-"+ UniqueId();
            tr = Selectors.closest(e.target, 'tr');
            let cb = () => {
                let overlay = document.querySelector("#" + id);
                let mod = Modal.init(target, {
                    target: "#" + id,
                    onShow: function (element, target, modalBox) {},
                    onHide: function (element, target, modalBox) {
                        element.acmsData(Modal.NS, null);
                        AcmsEvent.remove(element, 'click', mod._onClickModalElement);
                        document.body.removeChild(overlay);
                        let clon = element.cloneNode(true);
                        element.parentNode.replaceChild(clon, element);
                        self._processing = false;
                    },
                });
                AcmsEvent.on(overlay, 'click', '[data-trigger="save"]', (e) => {
                    let emForm = Selectors.q('form', overlay);
                    let fData = new FormData(emForm);
                    require(['http/request', 'notifier'], (Request) => {
                        Request.post(o.remote, fData, (response) => {
                            if(response.status !== 'success') {
                                return A.Notifications.createFromResponse(response);
                            }
                            let model = response.model;
                            tr.acmsData('code', model.code);
                            tr.acmsData('category', model.group_id);
                            tr.acmsData('unicode', model.unicode);
                            tr.acmsData('alt', model.alt);
                            tr.acmsData('keywords', model.keywords);
                            let tds = ['code', 'category', 'unicode', 'alt', 'keywords'];
                            tds.forEach(td => {
                                let ele = Selectors.q('[data-col="'+td+'"]', tr);
                                if(ele) ele.innerHTML = tr.acmsData(td);
                            });
                            mod.hide();
                        });
                    }, self._fail, self._always);
                }, true);
                mod.show();

            }, tdata = {
                emoji: {
                    id: tr.acmsData('id'),
                    code: tr.acmsData('code'),
                    alt: tr.acmsData('alt'),
                    keywords: tr.acmsData('keywords'),
                    group_id: tr.acmsData('category'),
                    unicode: tr.acmsData('unicode'),
                },
                categories: self.categories,
                token: o.token,
                id: id
            };
            TemplateEngine.compileResource(o.templates.form, tdata, cb, document.body);
        }
        
        _onClickAdd(e) {
            let self = this, o = self.options, tr, id, target;
            if(true === self._processing) {
                return;
            }
            self._processing = true;
            id = "emoji-form-"+ UniqueId();
            target = e.target;
            if(target.tagName.toLowerCase() !== 'button') {
                target = Selectors.closest(target, 'button');
            }
            let cb = () => {
                let overlay = document.querySelector("#" + id);
                let mod = Modal.init(target, {
                    target: "#" + id,
                    onShow: function (element, target, modalBox) {},
                    onHide: function (element, target, modalBox) {
                        element.acmsData(Modal.NS, null);
                        AcmsEvent.remove(element, 'click', mod._onClickModalElement);
                        document.body.removeChild(overlay);
                        let clon = element.cloneNode(true);
                        element.parentNode.replaceChild(clon, element);
                        self._processing = false;
                    },
                });
                AcmsEvent.on(overlay, 'click', '[data-trigger="save"]', (e) => {
                    let emForm = Selectors.q('form', overlay);
                    let fData = new FormData(emForm);
                    require(['http/request', 'notifier'], (Request) => {
                        Request.post(o.remote, fData, (response) => {
                            if(response.status !== 'success') {
                                return A.Notifications.createFromResponse(response);
                            }
                            let model = response.model;
                            model.category = self.categories[model.group_id];
                            let data = {
                                emoji: model,
                                categories: self.categories
                            }, done = () => {
                                self.sorter.sort();
                                mod.hide();
                            } ;
                            TemplateEngine.compileResource(o.templates.row, data, done, self.element.querySelector('tbody'));
                        });
                    }, self._fail, self._always);
                }, true);
                mod.show();

            }, tdata = {
                emoji: {
                    id: 0,
                    code: '',
                    alt: '',
                    keywords: '',
                    group_id: 'people',
                    unicode: '',
                },
                categories: self.categories,
                token: o.token,
                id: id
            };
            TemplateEngine.compileResource(o.templates.form, tdata, cb, document.body);
        }
        
        _onClickDelete(e) {
            let self = this, o = self.options, fData = new FormData(), tr;
            if(true === self._processing) {
                return;
            }
            self._processing = true;
            tr = Selectors.closest(e.target, 'tr');
            self._rowToDelete = tr;
            fData.append('op', 'delete');
            fData.append('id', tr.acmsData('id'));
            fData.append('token', o.token);
            require(['http/request', 'notifier'], (Request) => {
                Request.post(o.remote, fData, self._onDeleted, self._fail, self._always);
            });
        }

        _onDeleted(response) {
            let self = this, o = self.options;
            if(response.status !== 'success') {
                return A.Notifications.createFromResponse(response);
            }
            if(!self._rowToDelete) {
                return;
            }
            self._rowToDelete.parentNode.removeChild(self._rowToDelete);
        }
        

        _prepare() {
            let self = this, o = self.options, sorterOptions;
            sorterOptions = {
                direction: ListSorter.ASC, // initial direction

				source: 'data', // sort-source might be `data` to sort by data-attribute, `html` to sort by inner HTML content, `text` to sorty inner text content.
				attribute: 'code', // initial attribute to sort by. Ignored/unused if the source is any other than `data`
				selectors: {
					active: 'active', // active elements in sort asc/desc and sort change triggers
					list: 'tbody', // list element. can be any listing wrapper
					item: 'tr', // list item element. can be any kind of item selector inside list element.
					asc: '[data-role="sort-asc"]', // trigger(s) to sort ascending. supported are button, select and radio-input elements
					desc: '[data-role="sort-desc"]', // trigger(s) to sort descending. supported are button, select and radio-input elements
					changer: '[data-role="sort-changer"]', // trigger(s) to toggle sort attribute. supported are button, select and radio-input elements
				}
            };
            self._sorter = ListSorter.init(self.element, sorterOptions);
            require(['http/css-loader'], CssLoader => {
                CssLoader.load(Modal.css);
            });
        }

		_fail(xhr, textStatus) {
            A.Logger.logWarning(textStatus);
		}
		
		_always(xhr, textStatus) {
			A.Logger.logInfo(textStatus);
			this._processing = false;
		}

    }

    return EmojiAdmin;
});
