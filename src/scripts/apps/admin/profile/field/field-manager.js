/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/admin/profile/field/field-manager", [
	"core/base"
], (Base) => {

	
	/**
	 * Profile Field Manager
	 *
	 * @class
	 * @extends {Base}
	 * @author qm-b <https://bitbucket.org/qm-b/>
	 */
	class FieldManager extends Base {

		/**
		 * Module Namespace
		 *
		 * @readonly
		 * @static
		 * @memberof FieldManager
		 */
		static get NS() {
			return "acms.apps.admin.profile.field.field-manager";
		}

		/**
		 * Module Name
		 *
		 * @readonly
		 * @static
		 * @memberof FieldManager
		 */
		static get MODULE() {
			return "Profile Field Manager";
		}

		/**
		 * Default Options
		 *
		 * @readonly
		 * @static
		 * @memberof FieldManager
		 */
		static get DEFAULT_OPTIONS() {
			return {};
		}

		/**
		 * Init Module
		 *
		 * @static
		 * 
		 * @param {HTMLElement} element	Wrapper Element
		 * @param {Object|null} options	Options
		 * 
		 * @returns {FieldManager}	New FieldManager Instance
		 * 
		 * @memberof FieldManager
		 */
		static init(element, options) {
			let fm = new FieldManager(element, options);
			fm.element.acmsData(FieldManager.NS, fm);

			return fm;
		}

		/**
		 * Creates an instance of FieldManager.
		 *
		 * @param {HTMLElement} element		Wrapper Element
		 * @param {Object|null} options		Custom options
		 * 
		 * @memberof FieldManager
		 */
		constructor(element, options) {
			super(element, options);
			this._initialize();
		}

		/**
		 * On XHR Fail Callback
		 *
		 * @param {Object} 		xhr				XHR Request
		 * @param {string} 		textStatus		XHR Status Message
		 * 
		 * @memberof FieldManager
		 */
		_fail(xhr, textStatus) {
            A.Logger.writeLog(textStatus);
		}
		

        /**
		 * On Always XHR Callback
		 *
		 * @param {String} 			textStatus		XHR Status Message
		 * 
		 * @memberof FieldManager
		 */
		_always(textStatus) {
            A.Logger.writeLog(textStatus);
		}
		
	}

	return FieldManager;
});
