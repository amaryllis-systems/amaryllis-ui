/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("apps/admin/configs/configs-field", [
    "core/base",
    "core/classes",
    "core/selectors",
    "events/event",
    "core/acms"
], (Base, Classes, Selectors, AcmsEvent, A) => {

    /**
     * Configs Field Handling
     * 
     * Helper Module to auto-save the admin configs/preferences form fields on change.
     *
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     * 
     * @property {ModuleOptions} options final options combined from defaults, passed options and element data-Attributes
     * @property {HTMLElement} element Wrapper element, e.g. fieldset, around the config fields to process
     */
    class ConfigsField extends Base {
            
        /**
         * Remote Response after change has been processed by server
         * 
         * @typedef {Object} ConfigsField~Response
         * 
         * @property {('error'|'success')}  status      Text Status. can be 'success' or 'error'
         * @property {Number}               code        Might be `200` on success, error code otherwise
         * @property {String}               title       heading for response
         * @property {String}               message     Response message
         */
        
        /**
         * Custom Event Data passed along with callbacks and event dispatched by the module
         * 
         * @typedef {Object} ConfigsField~EventData
         * 
         * @property {String|Number}  value     New value 
         * @property {String}         field     Name of the field
         * @property {Number}         id        ID of the Configs Object in remote database
         * @property {Number}         module    ID of the Amaryllis-CMS Module in remote database
         * @property {String}         dirname   directory name of the Amaryllis-CMS Module in remote database
         * @property {('update')}     op        Operation to perform in Backend
         */

        /**
         * Default Module Options
         * 
         * @typedef {Object} ConfigsField~ModuleOptions
         * 
         * @property {String}   remote      The remote url.
         * @property {String}   dirname     Directory name of the Amaryllis-CMS module providing the option, 'Core' for the system itself
         * @property {Callback} onDone      On Done Callback
         * @property {Callback} onDone      On Done Callback
         */

        /**
         * A callback that will be triggered, if the user changed a value
         * 
         * @callback ConfigsField~changeCallback
         * 
         * @param {EventData} data custom data of the field
         * @param {Object} self Instance of this class
         */

        /**
         * A callback that will be triggered, if the value has sent to remote and remote sended a respons
         * 
         * @callback ConfigsField~doneCallback
         * @param {Response} response       Remote response object
         * @param {EventData} data custom data of the field
         * @param {Object} self Instance of this class
         */


        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof ConfigsField
         */
        static get NS() {
            return "acms.apps.admin.configs.configs-field";
        }

        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof ConfigsField
         */
        static get MODULE() {
            return "Admin Configs Field";
        }

        /**
         * Default Options
         * @type {ModuleOptions}
         * @readonly
         * @static
         * @memberof ConfigsField
         */
        static get DEFAULT_OPTIONS() {
            return {
                remote: null,
                dirname: null
            };
        }

        /**
         * Event "updated"
         * 
         * Event triggered if field changes have been updated in remote host
         *
         * @readonly
         * @static
         * @memberof ConfigsField
         */
        static get EVENT_UPDATED() {
            return ConfigsField.NS + '.updated';
        }

        /**
         * Module Init
         *
         * @static
         * 
         * @param {HTMLElement}     element Element
         * @param {Object}          options Custom options
         * 
         * @returns {Object} New instance
         * 
         * @memberof ConfigsField
         */
        static init(element, options) {
            let m = new ConfigsField(element, options);
            m.element.acmsData(ConfigsField.NS, m);
            
            return m;
        }

        /**
         * Creates an instance of ConfigsField.
         * 
         * @param {HTMLElement}     element Element
         * @param {Object}          options Custom options
         * 
         * @memberof ConfigsField
         */
        constructor(element, options) {
            super(element, options);    
            /**
             * Callback whenever the value of a field changes.
             * @member {ConfigsField~changeCallback}
             */
            this.onChange = null;
            /**
             * Callback when the user got a response from remote.
             * @member {ConfigsField~doneCallback}
             */
            this.onDone = null;
            /**
             * Remote URL
             * @var {String}
             */
            this.remote = null;
            
            this._initialize();

        }

        /**
         * Setup Event Listener
         *
         * @memberof ConfigsField
         * @private
         */
        listen() {
            let self = this, element = self.element;
            let fields = Selectors.qa('input,select,textarea', self.element), i;
            for(i = 0; i < fields.length; i++) {
                AcmsEvent.add(fields[i], 'change', self._onChange);
                if(fields[i].type && fields[i].type === 'text') {
                    AcmsEvent.add(fields[i], 'blur', self._onChange);
                }
            }
        }

        /**
         * On Change Callback
         *
         * @param {Event} e Change Event
         * 
         * @memberof ConfigsField
         * @private
         */
        _onChange(e) {
            let el = e.target || e.srcElement,
                    tagName = el.tagName.toLowerCase(),
                    self = this,
                    o = this.options,
                    val, dirname,
                    parent = Selectors.closest(el, 'div'),
                    container, checkboxesChecked, checkboxes, i;
            
            if (el.type && el.type === 'checkbox' && parent && Classes.hasClass(parent, 'switch') && Classes.hasClass(parent, 'toggle-variant')) {
                container = Selectors.closest(el, '.form-group');
                checkboxes = Selectors.qa('input[type=checkbox]', container);
                checkboxesChecked = [];
                for (i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].checked || checkboxes[i] === el && !el.checked) {
                        checkboxesChecked.push(checkboxes[i].value);
                    }
                }
                val = checkboxesChecked.join(',');
            } else if (el.type && el.type === 'checkbox' && parent && Classes.hasClass(parent, 'switch')) {
                val = !el.checked ? 'true' : 'false';
            } else if (el.type && el.type === 'checkbox') {
                container = Selectors.closest(el, '.form-group');
                checkboxes = Selectors.qa('input[type=checkbox]', container);
                if(checkboxes.length === 1) {
                    val = !checkboxes[0].checked ? 'true' : 'false';
                } else {
                    checkboxesChecked = [];
                    for (i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].checked) {
                            checkboxesChecked.push(checkboxes[i].value);
                        }
                    }
                    val = checkboxesChecked.join(',');
                }
            } else {
                val = el.value;
            }
            dirname = el.getAttribute('data-dirname') || o.dirname || 'Core';
            let fData = new FormData();
            fData.append('value', val);
            fData.append('field', el.getAttribute('name').replace('[]', ''));
            fData.append('id', el.getAttribute('data-id'));
            fData.append('module', el.getAttribute('data-moduleid'));
            fData.append('dirname', dirname);
            fData.append('op', 'update');
            let data = {
                value: val,
                field: el.getAttribute('name').replace('[]', ''),
                id: el.getAttribute('data-id'),
                module: el.getAttribute('data-moduleid'),
                dirname: dirname,
                op: 'update'
            },
                    done = function (response) {
                        return self._done(response, data);
                    },
                    fail = function (jqXHR, textStatus) {
                        Acms.Logger.logWarning(textStatus);
                    };
            if(typeof self.onChange === 'function') {
                self.onChange(data, self);
            }
            require(['http/request', 'notifier'], Request => {
                Request.post(o.remote, fData, done, fail);
            });
            
        }

        /**
         * On Remote done callback
         *
         * @param {Response}    response    Response Object
         * @param {EventData}   data        Event Data
         * 
         * @memberof ConfigsField
         */
        _done(response, data) {
            let self = this, 
                o = self.options, 
                opt = {
                    title: response.title,
                    content: response.message
                };
            if (response.status === "error") {
                opt.type = Acms.Notifications.TYPE_DANGER;
                A.Notifications.create(opt);
            } else {
                opt.type = Acms.Notifications.TYPE_SUCCESS;
                A.Notifications.create(opt);
            }
            let ev = AcmsEvent.createCustom(ConfigsField.EVENT_UPDATED, {
                status: response.status,
                value: data.value,
                field: data.field
            });
            AcmsEvent.dispatch(this.element, ev);
            if(typeof self.onDone === 'function') {
                self.onDone(response, data, self);
            }
        }


        /**
         * Prepare Module
         *
         * @memberof ConfigsField
         * @private
         */
        _prepare() {
            let self = this, o = self.options;
            
            self.remote = o.remote;
            if(typeof o.onChange === 'function')
                self.onChange = o.onChange;
            if(typeof o.onDone === 'function') 
                self.onDone = o.onDone;
        }

    }

    return ConfigsField;
});
