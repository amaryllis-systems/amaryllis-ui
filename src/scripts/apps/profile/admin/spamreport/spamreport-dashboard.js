/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("apps/profile/admin/spamreport/spamreport-dashboard", [
    "core/base",
    "core/classes",
    "core/selectors",
    "templates/template-engine",
    "events/event",
    "core/acms"
], (Base, Classes, Selectors, TemplateEngine, AcmsEvent, A) => {

    /**
     * Spamreport Dashboard
     *
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class SpamreportDashboard extends Base {

        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof SpamreportDashboard
         */
        static get NS() {
            return "apps.profile.admin.spamreport.spamreport-dashboard";
        }

        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof SpamreportDashboard
         */
        static get MODULE() {
            return "Spamreport Dashboard";
        }

        /**
         * Default Options
         * 
         * - `remote`: *String* remote url
         * - `templates`: *Object* Template setup
         *  - `templates.report`: *String* Template to be rendered for a single report
         *  - `templates.banned`: *String* Template to be rendered for a single banned user
         * - `selectors`: *Object* Selectors setup
         *  - `selectors.totalReports` *String* Selector for total report counters (e.g. badges)
         *  - `selectors.reports` *String* Selector for the element holding the reports loaded from remote
         *  - `selectors.report` *String* Selector for the single remote report card
         *  - `selectors.totalBanned` *String* Selector for total banned user counter
         *  - `selectors.banned` *String* Selector for banned section holding all banned user cards
         *  - `selectors.ban` *String* Selector for a single user card
         * - `limit` *Number* limit of remote items to get at once. It is highly recommended to get not more than a maximum of 100 at once
         * - `onLoaded`: *Callable* Callback will be tiggered after the `load` method has finished rendering.    
         *
         * @readonly
         * @static
         * @memberof SpamreportDashboard
         */
        static get DEFAULT_OPTIONS() {
            return {
                remote: null,
                templates: {
                    report: 'templates/html/profile/admin/spamreport/report.hbs',
                    banned: 'templates/html/profile/admin/spamreport/banned.hbs'
                },
                selectors: {
                    totalReports: '[data-role="total-reports"]',
                    reports: '[data-section="reports"]',
                    report: '[data-role="report"]',
                    totalBanned: '[data-role="total-banned"]',
                    banned: '[data-section="banned"]',
                    ban: '[data-role="banned"]'
                },
                limit: 75,
                onLoaded: (self) => {}
            };
        }

        /**
         * Module init
         *
         * @static
         * 
         * @param {HTMLElement} element Element
         * @param {Object|null} options optional custom options
         * 
         * @returns {SpamreportDashboard} New instance
         * 
         * @memberof SpamreportDashboard
         */
        static init(element, options) {
            let m = new SpamreportDashboard(element, options);
            m.element.acmsData(SpamreportDashboard.NS, m);
            
            return m;
        }

        /**
         * Creates an instance of SpamreportDashboard.
         * 
         * @param {HTMLElement} element Element
         * @param {Object|null} options optional custom options
         * 
         * @memberof SpamreportDashboard
         */
        constructor(element, options) {
            super(element, options);
            this._initialize();
            this.load();
        }

        get limit() {
            return this._limit;
        }

        set limit(limit) {
            this._limit = parseInt(limit) || this.options.limit;
        }

        get currentOffset() {
            return this._currentOffset;
        }

        set currentOffset(offset) {
            this._currentOffset = parseInt(offset) || 0;
        }

        /**
         * Load items
         *
         * @memberof SpamreportDashboard
         */
        load() {
            let self = this, o = self.options, fData = new FormData();
            fData.append('op', 'load');
            fData.append('limit', self.limit);
            fData.append('offset', self.currentOffset);
            require(['http/request', 'notifier'], (Request => {
                Request.post(o.remote, fData, self._done, self._fail, self._always);
            }));
        }

        /**
         * Refresh all loaded items
         *
         * @memberof SpamreportDashboard
         */
        refresh() {
            let self = this, o = self.options;
            self.currentOffset = 0;
            let reports = Selectors.q(o.selectors.reports, self.element),
                banned = Selectors.q(o.selectors.banned, self.element);
            reports.innerHTML = '';
            banned.innerHTML = '';
            this.load();
        }

        /**
         * Setup Event Listener
         *
         * @memberof SpamreportDashboard
         */
        listen() {
            let self = this, o = self.options;
            AcmsEvent.on(self.element, 'click', '[data-trigger="remove"]', self._onClickRemoveReport, true);
            AcmsEvent.on(self.element, 'click', '[data-trigger="ban"]', self._onClickBan, true);
            AcmsEvent.on(self.element, 'click', '[data-trigger="unban"]', self._onClickUnban, true);
        }

        /**
         * on click refresh event handler
         *
         * @param {Event} e
         * @memberof SpamreportDashboard
         */
        _onClickRefresh(e) {
            this.refresh();
        }

        /**
         * on Click remove report event handler
         *
         * @param {Event} e Click event
         * 
         * @memberof SpamreportDashboard
         */
        _onClickRemoveReport(e) {
            let self, o = self.options, target = e.target, report = Selectors.closest(target, o.selectors.report);
            let uid = report.acmsData('user');
            let fData = new FormData();
            fData.append('op', 'remove');
            fData.append('spam_id', uid);
            require(['http/request', 'notifier'], (Request => {
                Request.post(o.remote, fData, self._onClickRefresh, self._fail, self._always);
            }));
        }

        /**
         * on Click ban event handler
         *
         * @param {Event} e Click event
         * 
         * @memberof SpamreportDashboard
         */
        _onClickBan(e) {
            let self = this, o = self.options, target = e.target, report = Selectors.closest(target, o.selectors.report);
            let uid = report.acmsData('user');
            let fData = new FormData();
            fData.append('op', 'ban');
            fData.append('spam_id', uid);
            require(['http/request', 'notifier'], (Request => {
                Request.post(o.remote, fData, self._onClickRefresh, self._fail, self._always);
            }));
        }

        /**
         * on Click unban event handler
         *
         * @param {Event} e Click event
         * 
         * @memberof SpamreportDashboard
         */
        _onClickUnban(e) {
            let self = this, o = self.options, target = e.target, report = Selectors.closest(target, o.selectors.ban);
            let uid = report.acmsData('user');
            let fData = new FormData();
            fData.append('op', 'unban');
            fData.append('user', uid);
            require(['http/request', 'notifier'], (Request => {
                Request.post(o.remote, fData, self._onClickRefresh, self._fail, self._always);
            }));
        }

        /**
         * On Loaded callback
         *
         * @param {Object} response Remote response object
         * 
         * @memberof SpamreportDashboard
         */
        _done(response) {
            if(response.status !== 'success') {
                return A.Notifications.createFromResponse(response);
            }
            let self = this, o = self.options, reportsProcessed = false, bannedProcessed = false, doneA = () => {
                reportsProcessed = true;
                if(reportsProcessed && bannedProcessed) {
                    o.onLoaded(self);
                }
            }, doneB = () => {
                bannedProcessed = true;
                if(reportsProcessed && bannedProcessed) {
                    o.onLoaded(self);
                }
            };
            self._processReports(response, doneA);
            self._processBanned(response, doneB);
            self.currentOffset = self.currentOffset + self.limit;
        }

        /**
         * prosess loaded reports
         *
         * @param {Object}              response Response object
         * @param {CallableFunction}    callback Callback to be called, when all items are rendered
         * 
         * @memberof SpamreportDashboard
         */
        _processReports(response, callback) {
            let self = this, o = self.options, reports = response.reports, j, totalReports = response.total;
            Selectors.qa(o.selectors.totalReports, self.element).forEach((badge) => {
                badge.innerHTML = totalReports;
            });
            if(!reports.length) {
                callback();
            }
            let report, tdata, cb, appendTo = Selectors.q(o.selectors.reports, self.element), processed = 0;
            cb = () => {
                processed++;
                if(processed === reports.length) {
                    callback();
                }
            };
            for(j = 0; j < reports.length; j++) {
                report = reports[j];
                tdata = {
                    reports: report.reports,
                    user: report.user
                };
                TemplateEngine.compileResource(o.templates.report, tdata, cb, appendTo);
            }
        }

        /**
         * process banned
         *
         * @param {Object}              response response object
         * @param {CallableFunction}    callback callback to be called when all items are rendered
         * 
         * @memberof SpamreportDashboard
         */
        _processBanned(response, callback) {
            let self = this, o = self.options, banned = response.banned, total = response.totalBanned;
            Selectors.qa(o.selectors.totalBanned, self.element).forEach((badge) => {
                badge.innerHTML = total;
            });
            if(0 == total) {
                callback();
            }
            let j, tdata, cb, appendTo = Selectors.q(o.selectors.banned, self.element), processed = 0;
            cb = () => {
                processed++;
                if(processed === banned.length) {
                    callback();
                }
            };
            for(j = 0; j < banned.length; j++) {
                tdata = {
                    user: banned[j]
                };
                TemplateEngine.compileResource(o.templates.banned, tdata, cb, appendTo);
            }
        }

		_fail(xhr, textStatus) {
            A.Logger.logWarning(textStatus);
		}
		
		_always(xhr, textStatus) {
			A.Logger.logInfo(textStatus);
			this.processing = false;
		}

        /**
         * prepare class instance
         *
         * @memberof SpamreportDashboard
         */
        _prepare() {
            let self = this, o = self.options;
            self.limit = parseInt(o.limit) || 75;
            self.currentOffset = 0;
        }

    }

    return SpamreportDashboard;
});
