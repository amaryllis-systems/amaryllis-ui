/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("apps/profile/spamreport/report-profile", [
    "core/base",
    "core/classes",
    "core/selectors",
    "templates/template-engine",
    "tools/string/unique-id",
    "events/event",
    "core/acms"
], (Base, Classes, Selectors, TemplateEngine, UniqueId, AcmsEvent, A) => {

    /**
     * Report Profile as Spam
     *
     * @class ReportProfile
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     * 
     * @property {String} modalId ID of the modal box
     * @property {Modal} modal Instance of Modal box
     * @property {HTMLElement} target Target modal element
     * 
     */
    class ReportProfile extends Base {

        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * 
         * @memberof ReportProfile
         */
        static get NS() {
            return "apps.profile.spamreport.report-profile";
        }


        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof ReportProfile
         */
        static get MODULE() {
            return "Report Profile";
        }

        /**
         * Default Options
         * 
         * - `remote`: *{String}* Remote URL
         * - `templates`: *{Object}* Object with templates to use
         *  - `templates.form`: *{String}* Form Template for Spam Report Form
         * - `selectors`: *{Object}* Contains Selectors to use
         * 
         *
         * @readonly
         * @static
         * @memberof ReportProfile
         */
        static get DEFAULT_OPTIONS() {
            return {
                remote: null,
                templates: {
                    form: 'templates/html/profile/spamreport/form.hbs'
                },
                selectors: {}
            };
        }

        /**
         * Module Init
         *
         * @static
         * 
         * @param {HTMLElement} element Button trigger Element
         * @param {Object|null} options optional custom options
         * 
         * @returns {ReportProfile} New instance
         * @memberof ReportProfile
         */
        static init(element, options) {
            let m = new ReportProfile(element, options);
            m.element.acmsData(ReportProfile.NS, m);
            
            return m;
        }

        /**
         * Creates an instance of ReportProfile.
         * 
         * @param {HTMLElement} element Button trigger Element
         * @param {Object|null} options optional custom options
         * 
         * @memberof ReportProfile
         */
        constructor(element, options) {
            super(element, options);
            this._initialize();
            this._loadTemplate();
        }


        get modal() {
            return this._modal;
        }

        get modalId() {
            return this._id;
        }

        get target() {
            return this._target;
        }

        _loadTemplate() {
            let self = this, o = self.options, t = o.templates.form;
            let id = UniqueId('spamreport-form'), cb;
            let tData = {
                id: id,

            };
            self._id = id;
            cb = () => {
                require(['apps/ui/informations/modal', 'editor/editor'], (Modal, Editor) => {
                    self._modal = Modal.init(self.element, {target: '#' + id});
                    self._target = Selectors.q('#' + id);
                    let tarea = Selectors.q('textarea', self.target);
                    if(tarea) Editor.init(tarea, {editor: 'amaryllis', mode: 1});
                    self._listen();
                });
            };
            TemplateEngine.compileResource(t, tData, cb, document.body);
        }

        _listen() {
            let self = this, o = self.options, target = Selectors.q('#' + self.modalId);
            AcmsEvent.on(target, 'click', '[data-dismiss="modal"]', self._onClickDismissModal, true);
            AcmsEvent.on(target, 'click', '[data-trigger="save"]', self._onClickSubmitModal, true);
        }

        /**
         * on Click dismiss modal event handler
         *
         * @param {Event} e Click event
         * 
         * @memberof ReportProfile
         */
        _onClickDismissModal(e) {
            let self = this, o = self.options, target = self.target;
            let form = Selectors.q('form', target);
            form.reset();
        }

        /**
         * On Click Submit Modal Event Handler
         *
         * @param {Event} e Click event
         * 
         * @memberof ReportProfile
         */
        _onClickSubmitModal(e) {
            let self = this, o = self.options, target = self.target;
            let form = Selectors.q('form', target);
            let fData = new FormData(form);
            require(['http/request', 'notifier'], (Request) => {
                Request.post(self.remote, fData, self._done, self._fail, self._always);
            });
        }

        /**
         * On Response Callback
         *
         * @param {Object} response
         * 
         * @memberof ReportProfile
         */
        _done(response) {
            let self = this, o = self.options;
            A.Notifications.createFromResponse(response);
            if(response.code === 200) self.modal.hide();
        }

		_fail(xhr, textStatus) {
            A.Logger.logWarning(textStatus);
		}
		
		_always(xhr, textStatus) {
			A.Logger.logInfo(textStatus);
			this.processing = false;
		}

        _prepare() {
            let self = this, o = self.options;
            self.remote = o.remote;
            self.element.acmsData('remote', null);
            self.element.acmsData('module', null);
            self.element.removeAttribute('data-remote');
            self.element.removeAttribute('data-module');
        }

    }
    
    return ReportProfile;
});
