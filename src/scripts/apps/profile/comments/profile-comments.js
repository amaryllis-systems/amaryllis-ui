/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("apps/profile/comments/profile-comments", [
    "core/base",
    "core/classes",
    "core/selectors",
    "events/event",
    "tools/string/string-tool",
    "templates/template-engine",
    "core/acms"
], (Base, Classes, Selectors, AcmsEvent, StringTool, TemplateEngine, A) => {

    /**
     * Profile Comments
     *
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class ProfileComments extends Base {

        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof ProfileComments
         */
        static get NS() {
            return "acms.apps.profile.comments.profile-comments";
        }

        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof ProfileComments
         */
        static get MODULE() {
            return "Profile Comments";
        }

        /**
         * Default Options
         *
         * @readonly
         * @static
         * @memberof ProfileComments
         */
        static get DEFAULT_OPTIONS() {
            return {
                remote: null,
                selectors: {
                    comment: '[data-role="comment"]',
                    comments: '[data-role="comments"]',
                    trigger: '[data-trigger]',
                    upvoteCounter: '[data-role="vote-counter"]',
                    downvoteCounter: '[data-role="downvote-counter"]',
                    averageCounter: '[data-role="average-counter"]',
                    totalvoteCounter: '[data-role="totalvote-counter"]',
                    formSubmit: 'button[data-action]'
                },
                templates: {
                    comment: 'templates/html/profile/comments/comment.hbs'
                }
            };
        }

        /**
         * Module init
         *
         * @static
         * 
         * @param {HTMLElement} element Wrapper Element around comments
         * @param {Object} options Options
         * 
         * @returns {ProfileComments} New instance
         * 
         * @memberof ProfileComments
         */
        static init(element, options) {
            let m = new ProfileComments(element, options);
            m.element.acmsData(ProfileComments.NS, m);
            
            return m;
        }

        /**
         * Creates an instance of ProfileComments.
         * 
         * @param {HTMLElement} element Wrapper Element around comments
         * @param {Object} options Options
         * 
         * @memberof ProfileComments
         */
        constructor(element, options) {
            super(element, options);
            this._initialize();

        }

        get processing() {
            return this._processing;
        }

        get remote() {
            return this._remote;
        }
        
        /**
         * Setup Event Listener
         *
         * @memberof ProfileComments
         */
        listen() {
            let self = this, o = self.options, sel = o.selectors, element = self.element;
            AcmsEvent.on(element, 'click', sel.trigger, self._onClickTrigger, true);
            AcmsEvent.on(element, 'click', sel.formSubmit, self._onClickSubmitForm, true);
            Selectors.qa(sel.formSubmit, element);
        }

        /**
         * On Click submit comment form
         *
         * @param {Event} e Click event
         * @memberof ProfileComments
         */
        _onClickSubmitForm(e) {
            let self = this, o = self.options, sel = o.selectors, target = e.target, form = Selectors.closest(target, '[role=form]');
            e.preventDefault();
            e.stopPropagation();
            if(!form || self._processing) {
                return false;
            }
            let fData = new FormData(form), done = (response) => {
                return self._onDoneSubmitForm(response, form);
            };
            require(['http/request', 'notifier'], Request => {
                Request.post(self.remote, fData, self._onDoneSubmitForm, self._fail, self._always);
            });
        }

        /**
         * On done submit form
         *
         * @param {Object} response Remote response Object
         * @param {HTMLFormElement} form Form element
         * 
         * @memberof ProfileComments
         */
        _onDoneSubmitForm(response, form) {
            if(response.status !== 'success') {
                return A.Notifications.createFromResponse(response);
            }
            form.reset();
            let self = this, o = self.options, sel = o.selectors, tData = {
                comment: response.comment
            }, cb = () => {}, appendTo = Selectors.q(sel.comments, self.element);
            TemplateEngine.compileResource(o.templates.comment, tData, cb, appendTo, 'afterbegin');
        }

        /**
         * On Click Trigger
         *
         * @param {Event} e Click Event
         * 
         * @memberof ProfileComments
         */
        _onClickTrigger(e) {
            let self = this, o = self.options, sel = o.selectors, target = e.target;
            e.preventDefault();
            e.stopPropagation();
            if(!Selectors.matches(target, sel.trigger)) {
                target = Selectors.closest(target, sel.trigger);
            }
            if(!target || self._processing) {
                return;
            }
            let trigger = target.acmsData('trigger');
            if((trigger === 'upvote' || trigger === 'downvote') && Classes.hasClass(target, 'voted')) {
                return false;
            }
            self._lastTrigger = target;
            self._processing = true;
            let fData = new FormData();
            fData.append('op', trigger);
            fData.append('id', target.acmsData('id'));
            let cb = '_onDone' + StringTool.ucfirst(trigger);
            let done = (response) => {
                self[cb](response);
            };
            require(['http/request', 'notifier'], Request => {
                Request.post(self.remote, fData, done, self._fail, self._always);
            });
        }

        /**
         * On done Upvote
         *
         * @param {Object} response Remote response Object
         * 
         * @memberof ProfileComments
         */
        _onDoneUpvote(response) {
            let self = this, o = self.options, sel = o.selectors, target = self._lastTrigger;
            if(response.status !== 'success') {
                return A.Notifications.createFromResponse(response);
            }
            Classes.addClass(target, 'voted');
            self._updateCounters(response.counter, target, sel);
        }

        /**
         * On done Downvote
         *
         * @param {Object} response Remote response Object
         * 
         * @memberof ProfileComments
         */
        _onDoneDownvote(response) {
            let self = this, o = self.options, sel = o.selectors, target = self._lastTrigger;
            if(response.status !== 'success') {
                return A.Notifications.createFromResponse(response);
            }
            Classes.removeClass(target, 'voted');
            self._updateCounters(response.counter, target, sel);
        }

        /**
         * Update Counters
         *
         * @param {Object}      counters    Counters from remote
         * @param {HTMLElement} target      Current Trigger
         * @param {Object}      sel         Selectors setup from options
         * 
         * @memberof ProfileComments
         */
        _updateCounters(counters, target, sel) {
            let upcounter = Selectors.q(sel.upvoteCounter, target);
            if(upcounter) {
                upcounter.innerHTML = counters.upvotes;
            }
            let downcounter = Selectors.q(sel.downvoteCounter, target);
            if(downcounter) {
                downcounter.innerHTML = counters.downvotes;
            }
            let totalcounter = Selectors.q(sel.totalvoteCounter, target);
            if(totalcounter) {
                totalcounter.innerHTML = counters.total;
            }
            let averagecounter = Selectors.q(sel.averageCounter, target);
            if(averagecounter) {
                averagecounter.innerHTML = counters.average;
            }
        }

		_fail(xhr, textStatus) {
            A.Logger.logWarning(textStatus);
		}
		
		_always(xhr, textStatus) {
			A.Logger.logInfo(textStatus);
            this._processing = false;
            this._sending = false;
		}
        
        /**
         * Prepare Module
         *
         * @memberof ProfileComments
         */
        _prepare() {
            let self = this, o = self.options;
            if(!StringTool.isUrl(o.remote)) {
                throw new TypeError('Remote must be a valid url');
            }
            self._remote = o.remote;
        }

    }

    return ProfileComments;
});
