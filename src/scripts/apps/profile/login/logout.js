/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/profile/login/logout", [
    "core/base",
    "tools/string/string-tool",
    "events/event",
    "core/acms"
], (Base, StringTool, AcmsEvent, A) => {

    /**
     * Logout
     * 
     * Helper Class for handling logouts from user
     *
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class Logout extends Base 
    {

        /**
         * Module Namespace
         * 
         * @type {String}
         * 
         * @readonly
         * @static
         * @memberof Logout
         */
        static get NS() {
            return "acms.apps.profile.login.logout";
        }

        /**
         * Module Name
         *
         * @type {String}
         * 
         * @readonly
         * @static
         * @memberof Logout
         */
        static get MODULE() {
            return "Member Logout";
        }

        /**
         * Event "logged out"
         * 
         * @type {String}
         *
         * @readonly
         * @static
         * @memberof Logout
         */
        static get EVENT_LOGGED_OUT() {
            return Logout.NS + '.logout';
        }

        /**
         * Default options
         * 
         * @type {Object}
         * 
         * @readonly
         * @static
         * @memberof Logout
         */
        static get DEFAULT_OPTIONS() {
            return {
                remote: null,
                delay: 2,
                animation: 'rotateIn',
                beforeLogout: (self, evt) => {return true;},
                onLogout: (self, response) => {}
            };
        }

        /**
         * Module Init
         *
         * @static
         * 
         * @param {HTMLButtonElement} element
         * @param {Object} options
         * 
         * @returns {Logout}    New instance
         * 
         * @memberof Logout
         */
        static init(element, options) {
            let m = new Logout(element, options);
            m.element.acmsData(Logout.NS, m);
            
            return m;
        }

        /**
         * Creates an instance of Logout.
         * 
         * @param {HTMLButtonElement} element
         * @param {Object} options
         * 
         * @memberof Logout
         */
        constructor(element, options) {
            super(element, options);
            this._initialize();
        }

        /**
         * Aufsetzen der Event Listener
         * 
         * @memberof Logout
         * @private
         */
        listen() {
            const self = this, element = self.element;
            AcmsEvent.add(element, 'click', self._onClick);
        }
        
        /**
         * On Click Event Handler
         * 
         * @param {Event} evt   Das Click Event
         * 
         * @memberof Logout
         * @private
         */
        _onClick(evt) {
            const self = this;
            const fData = new FormData(); 
            const o = self.options;
            fData.append('op', 'logout');
            evt.preventDefault();
            if(typeof o.beforeLogout === "function") {
                const res = o.beforeLogout(self, evt);
                if(true !== res) {
                    evt.stopPropagation();
                    return false;
                }
            }
            require(['http/request', 'notifier'], Request => {
                Request.post(self.remote, fData, self._processLogout);
            });
            return false;
        }
        
        /**
         * Durchführen des Logout
         * 
         * Wird nach dem Klick aufgerufen um die Antwort des Servers zu 
         * verarbeiten.
         * 
         * @param {Response} response   Remote response
         * 
         * @memberof Logout
         * @private
         */
        _processLogout(response) {
            const self = this;
            const o = self.options;
            if(response.status === "success") {
                A.Notifications.create({
                    title: (response.title || A.Translator.t('Erfolgreich abgemeldet')),
                    content: response.message,
                    type: A.Notifications.TYPE_SUCCESS,
                    image: (response.user && response.user.avatar ? response.user.avatar : ''),
                    delay: o.delay,
                    animation: o.animation
                });
                if(typeof o.onLogout === 'function') {
                    o.onLogout(self, response);
                }
                self.emit('logout', response, self);
                const evt = AcmsEvent.create(Logout.EVENT_LOGGED_OUT);
                AcmsEvent.dispatch(document.body, evt);
                if(evt.defaultPrevented) {
                    return;
                }
                const redirect = (response.redirect && 
                                    (typeof response.redirect === 'string') && 
                                    StringTool.isUrl(response.redirect)) ? 
                                        response.redirect : 
                                        A.Urls.baseUrl;
                setTimeout(() => {
                    window.location.href = redirect;
                }, (o.delay * 1000));
            } else {
                A.Notifications.createFromResponse(response);
            }
        }
        
        /**
         * Prepare Module
         *
         * @memberof Logout
         * @private
         */
        _prepare() {
            const self = this;
            const o = self.options;
            const element = self.element;
            self.remote = o.remote || element.getAttribute('href');
            if(!StringTool.isUrl(self.remote)) {
                throw new Error('Remote url is mandatory');
            }
        }
        
    }

    return Logout;
});
