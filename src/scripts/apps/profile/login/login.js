/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/profile/login/login", [
    "core/base",
    "core/classes",
    "core/selectors",
    "events/event",
    "tools/string/string-tool",
    "core/window",
    "core/acms"
], (Base, Classes, Selectors, AcmsEvent, StringTool, Win, A) => {

    /**
     * Login
     * 
     * Login Handling for Amaryllis-CMS
     *
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     * 
     * @fires Login.EVENT_LOGGED_IN Event fired after successful login
     * 
     */
    class Login extends Base {

        /**
         * Module Namespace
         * 
         * @type {String}
         *
         * @readonly
         * @static
         * @memberof Login
         */
        static get NS() {
            return "acms.apps.profile.login.login";
        }

        /**
         * Module Name
         * 
         * @type {String}
         * 
         * @readonly
         * @static
         * @memberof Login
         */
        static get MODULE() {
            return "Login";
        }

        /**
         * Default options
         *
         * @readonly
         * @static
         * @memberof Login
         */
        static get DEFAULT_OPTIONS() {
            return {
                remote: null,
                delay: 2000,
                animation: 'rotateIn',
                trigger: 'button[type="submit"]'
            };
        }

        /**
         * Event "Logged in"
         *
         * @readonly
         * @static
         * @memberof Login
         */
        static get EVENT_LOGGED_IN() {
            return Login.NS + '.logged--in';
        }

        /**
         * Module init
         *
         * @static
         * @param {HTMLElement} element
         * @param {Object} options
         * 
         * @returns {Login} new instance
         * 
         * @memberof Login
         */
        static init(element, options) {
            let m = new Login(element, options);
            m.element.acmsData(Login.NS, m);
            
            return m;
        }

        /**
         * Creates an instance of Login.
         * 
         * @param {HTMLElement} element
         * @param {Object} options
         * 
         * @memberof Login
         */
        constructor(element, options) {
            super(element, options);
            this._initialize();
        }

        /**
         * Setup Event Listener
         *
         * @memberof Login
         * @private
         */
        listen() {
            const self = this;
            const element = self.element;
            const o = self.options;
            AcmsEvent.on(element, 'click', o.trigger, self._processLogin, true);
        }
        
        /**
         * Process login
         * 
         * @param {Event} e Click Event
         *
         * @memberof Login
         * @private
         */
        _processLogin(e) {
            AcmsEvent.stop(e);
            const self = this;
            const o = self.options;
            let target = e.target;
            if(!Selectors.matches(e.target, o.trigger)) {
                target = Selectors.closest(target, o.trigger);
            }
            if(Classes.hasClass(target, 'disabled')) {
                return false;
            }
            require(["http/request","notifier"], (Request) => {
                const form = Selectors.closest(target, 'form[role=form]');
                const data = new FormData(form);
                const url = o.remote || self.element.getAttribute('action');

                Request.post(url, data, self._done, self._fail, self._always);
            });
        }

        /**
         * On Done Callback
         *
         * @param {Response} response
         * 
         * @memberof Login
         * @private
         */
        _done(response) {
            let self = this, o = this.options;
            let acms_redirect = Selectors.q("input[name='acms_redirect']", self.element),
                redirect = (!acms_redirect) ? A.Urls.baseUrl : acms_redirect.value;
            if( response.status === "success") {
                A.Notifications.create({
                    title: (response.title || A.Translator._('Erfolgreich angemeldet')),
                    content: response.message,
                    type: A.Notifications.TYPE_SUCCESS,
                    image: response.user.avatar,
                    delay: o.delay,
                    animation: o.animation
                });
                let event = AcmsEvent.create(Login.EVENT_LOGGED_IN);
                AcmsEvent.dispatch(document.body, event);
                self.emit('login', response, self);
                if(event.defaultPrevented) {
                    return;
                }
                if(redirect && StringTool.isUrl(redirect)) {
                    setTimeout(() => {
                        Win.location.href = response.redirect;
                    }, o.delay);
                } else if(response.redirect && StringTool.isUrl(response.redirect)) {
                    setTimeout(() => {
                        Win.location.href = response.redirect;
                    }, o.delay);
                } else {
                    Win.refresh(50);
                }
            } else {
                Acms.Notifications.createFromResponse(response);
            }
        }

        _fail(jqXHR, textStatus) {
            let self = this, o = this.options;
            A.Logger.logWarning(textStatus);
            A.Notifications.createNotification(
                        A.Translator._('Ups! Da ist was schief gelaufen! Bitte versuchen Sie es später erneut!'),
                        A.Translator._('Anmeldefehler'),
                        A.Notifications.TYPE_ERROR,
                        false, o.delay);
        }

        _always(xhr, response, textStatus) {
            A.Logger.writeLog(textStatus);
        }

        /**
         * Vorbereitung der Formular-Felder
         * 
         * 
         */
        _prepare() {
            let self = this, element = self.element;
            let modal = Selectors.closest(element, '.modal');
            self._isInModal = modal ? true : false;
            self.$btn = element.querySelector('[data-role=acms-process-login]');
            let pw = Selectors.q('input[type=password]', element), uname, pwd,
                un = Selectors.q('input[name=username]', element);
            if(pw.value === "" || un.value === "") {
                Classes.addClass(self.$btn, 'disabled');
            } else {
                Classes.removeClass(self.$btn, 'disabled');
            }
            let cb = function() {
                pwd = self.element.querySelector('input[type=password]');
                uname = self.element.querySelector('input[name=username]');
                if(pwd.value !== "" && uname.value) {
                    Classes.removeClass(self.$btn, 'disabled');
                    self.$btn.removeAttribute('disabled');
                } else {
                    Classes.removeClass(self.$btn, 'disabled');
                    self.$btn.removeAttribute('disabled');
                }
            };
            AcmsEvent.add(pw, 'change', cb);
            AcmsEvent.add(un, 'change', cb);
            setTimeout(function (){
                cb();
            },5000);
        }

    }

    return Login;
});
