/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/profile/media-manager/media-manager-field", [
    "core/base",
    "tools/string/unique-id",
    "tools/object/extend",
    "core/classes",
    "core/selectors",
    "events/event",
    "apps/profile/media-manager/media-manager",
    "templates/template-engine",
    "core/acms"
], (Base, UniqueId, Extend,  Classes, Selectors, AcmsEvent, MediaManager, TemplateEngine, A) => {

    /**
     * media manager field
     * 
     * Class responsible for managing form fields with media manager
     *
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class MediaManagerField extends Base {

        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof MediaManagerField
         */
        static get NS() {
            return "acms.apps.profile.media-manager.media-manager-field";
        }

        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof MediaManagerField
         */
        static get MODULE() {
            return "Media Manager Field";
        }

        /**
         * Default Options
         *
         * @readonly
         * @static
         * @memberof MediaManagerField
         */
        static get DEFAULT_OPTIONS() {
            return {
                remote: null,
                token: null,
                field: '[data-role="manager-field"]',
                trigger: '[data-toggle="manager"]',
                targetInput: '[data-role="media"]',
                accept: [],
                templates: {
                    window: 'templates/html/profile/media-manager/media-manager.hbs'
                },
                manager: {},
                css: MediaManager.css,
                themecss: false,
                delay: 300
            };
        }

        /**
         * Module init
         *
         * @static
         * 
         * @param {HTMLElement} element     Form Field
         * @param {Object}      options     Optional custom options
         * 
         * @returns {MediaManagerField} New instance
         * 
         * @memberof MediaManagerField
         */
        static init(element, options) {
            let m = new MediaManagerField(element, options);
            m.element.acmsData(MediaManagerField.NS, m);
            
            return m;
        }

        /**
         * Creates an instance of MediaManagerField.
         * 
         * @param {HTMLElement} element     Form Field
         * @param {Object}      options     Optional custom options
         * 
         * @memberof MediaManagerField
         */
        constructor(element, options) {
            super(element, options);
            this._initialize();

        }

        /**
         * Media Manager instance
         * 
         * @var {MediaManager}
         *
         * @readonly
         * @memberof MediaManagerField
         */
        get manager() {
            return this._manager;
        }

        /**
         * Open Media Manager Window
         *
         * @memberof MediaManagerField
         */
        open() {
            const self = this, o = self.options;
            Classes.addClass(self._windowBox, 'opening');
            Classes.addClass(self._windowBox, 'active');
            setTimeout(() => {
                Classes.removeClass(self._windowBox, 'opening');
            }, o.delay);
        }

        /**
         * Close Media Manager Window
         *
         * @memberof MediaManagerField
         */
        close() {
            const self = this, o = self.options;
            Classes.addClass(self._windowBox, 'closing');
            setTimeout(() => {
                Classes.removeClass(self._windowBox, 'closing');
                Classes.removeClass(self._windowBox, 'active');
            }, o.delay);
        }

        /**
         * On Created Window Box from Template
         *
         * @memberof MediaManagerField
         * @private
         */
        _onCreated() {
            const self = this, o = self.options, element = self.element;
            self._windowBox = document.querySelector('#' + self._id);
            self._manager = MediaManager.init(self._windowBox, self._createOptions());
            self.manager.on('select', self._onSelect);
            AcmsEvent.on(element, 'click', o.trigger, self._onClickOpen, true);
            AcmsEvent.on(self._windowBox, 'click', '[data-dismiss="window"]', self._onClickClose, true);
        }

        _onSelect(data) {
            const self = this, item = data.item, id = data.id;
            self._targetInput.value = item.acmsData('title');
            self._mediaField.value = id;
            AcmsEvent.fireChange(self._targetInput);
            AcmsEvent.fireChange(self._mediaField);
            self.close();
        }

        /**
         * On Click open Window Box
         *
         * @param {Event} e Click Event
         * 
         * @memberof MediaManagerField
         * @private
         */
        _onClickOpen(e) {
            const self = this;
            AcmsEvent.stop(e);
            self.open();
        }

        /**
         * On click dismiss windwo box
         *
         * @param {Event}   e   Click Event
         * 
         * @memberof MediaManagerField
         * @private
         */
        _onClickClose(e) {
            const self = this;
            AcmsEvent.stop(e);
            self.close();
        }

        /**
         * Prepare Manager options
         *
         * @returns {Object}
         * 
         * @memberof MediaManagerField
         * @private
         */
        _createOptions() {
            const self = this, o = self.options, mo = {};
            mo.remote = o.remote;
            mo.token = o.token;
            mo.accept = o.accept;
            return Extend.flat({}, false, (o.manager || {}), mo);
        }

        /**
         * Prepare Module
         *
         * @memberof MediaManagerField
         * @private
         */
        _prepare() {
            const self = this, 
                  o = self.options, 
                  id = UniqueId('media-manager'),
                  element = self.element,
                  t = o.templates.window, 
                  cb = () => {
                    self._onCreated();
                  };
            self._id = id;
            self._targetInput = Selectors.q(o.targetInput, element);
            self._mediaField = Selectors.q(o.field, element);
            const tdata = {
                id: id,
            };
            TemplateEngine.compileResource(t, tdata, cb, document.body);
        }

    }

    return MediaManagerField;
});
