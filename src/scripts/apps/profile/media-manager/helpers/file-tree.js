/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('apps/profile/media-manager/helpers/file-tree', [
        "core/base",
        "core/selectors", 
        "core/classes",
        "events/event",
        "core/acms"
], (Base, Selectors, Classes, AcmsEvent, A) => {
	
    /**
	 * File Tree in Media Manager
	 *
	 * @class
	 * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     * 
     * @property {Boolean} loaded Is the tree already loaded?
     * @property {Boolean} loading Is the tree currently loading?
     * @property {MediaManager} manager Media Manager instance
     * @property {String} remote Remote URL
     * @property {Number} currentFolder Current selected folder
	 */
	class MediaManagerFileTree extends Base {

        /**
         * Default options
         *
         * @readonly
         * @static
         * @memberof MediaManagerFileTree
         */
        static get defaults() {
            return {
                open: '.open',
                checked: '.active'
            };
		}
		
		/**
		 * Module Namespace
		 *
		 * @readonly
		 * @static
		 * @memberof MediaManagerFileTree
		 */
		static get NS() {
			return "apps.profile.media-manager.helpers.file-tree";
		}

        /**
         * Creates an instance of MediaManagerFileTree.
         * 
         * @param {MediaManager} MediaManager 
         * 
         * @memberof MediaManagerFileTree
         */
        constructor(MediaManager) {
			super(MediaManager.ftree, MediaManager.options);
            this.currentSelection =
            this.tree = null;
            this._manager = MediaManager;
            this.ctrlDown = false;
            this.totalSeleted = 0;
            this._remote = MediaManager.remote;
            this._initialize();
        }
        
        get remote() {
            return this._remote;
        }

        get manager() {
            return this._manager;
        }

        get currentFolder() {
            return this._currentFolder;
        }

        get currentItem() {
            return this._currentItem;
        }

        set currentFolder(idx) {
            let id = parseInt(idx);
            this._currentFolder = id;
            this.manager.currentFolder = id;
            this.manager.filelist.refresh();
            this._toggleFolderActions(true);
        }

        get loaded() {
            return this._loaded;
        }

        get loading() {
            return this._loading;
        }
		
        /**
         * Refresh the tree
         * 
         * Force refreshing the tree from remote source.
         *
         * @memberof MediaManagerFileTree
         */
        refresh() {
            const self = this;
            self._loaded = false;
            self._loading = false;
            self.load();
            if(this.currentFolder) {
                setTimeout(() => {
                    self.manager.filelist.refresh();
                });
            }
        }

        /**
         * load tree nodes
         * 
         * Loads the tree nodes unless the tree is not loaded. 
         * In case of already loaded tree use refresh to load again.
         *
         * @memberof MediaManagerFileTree
         */
        load() {
            let self = this, element = self.manager.ftree, fData = new FormData();
            if(self._loaded || self._loading) {
                return;
            }
            fData.append('token', self.manager.token);
            fData.append('op', 'loadFolder');
            require(['http/request', 'notifier'], (Request) => {
                Request.post(self.remote, fData, self._onLoaded, self._fail, self._always);
            });
        }
        
		/**
		 * Setup Event Listener
		 *
		 * @memberof MediaManagerFileTree
         * @private
		 */
		listen() {
			let self = this, element = self.manager.ftree;

			AcmsEvent.on(element, 'click', 'li', self._onClick, true);
            AcmsEvent.on(element, 'change', 'input[type="checkbox"]', self._onChange);
            AcmsEvent.add(document, 'keydown', self._onKeyDown);
            AcmsEvent.add(document, 'keyup', self._onKeyUp);
		}

        /**
         * On KeyDown Event Handler
         * 
         * @param {Event} e Das Event
         * 
		 * @memberof MediaManagerFileTree
         * @private
         */
        _onKeyDown(e) {
            let self = this, code = parseInt(e.which || e.keyCode);
            if(code === 17) {
                self.ctrlDown = true;
                return true;
            }
            
        }
        
        /**
         * On KeyUp Event Handler
         * 
         * @param {Event} e Das Event
         * 
		 * @memberof MediaManagerFileTree
         * @private
         */
        _onKeyUp(e) {
            this.ctrlDown = false;
        }
        

        /**
         * On Click List-Item Event Handler
         * 
         * @param {Event} e Das Event
         * 
         * @returns {Boolean}
         * 
		 * @memberof MediaManagerFileTree
         * @private
         */
        _onClick(e) {
            if(!this.ctrlDown) {
                return true;
            }
            let target = e.target,
                item = Selectors.closest(target, 'li'),
                chb = Selectors.q('input[type="checkbox"]', item),
                self = this;
            if(chb) {
                chb.checked = !chb.checked;
                AcmsEvent.fireChange(chb);
            }
        }

        /**
         * Checkbox change Event Handler
         * 
         * @param {Event} e Das Event
         * 
         * @returns {Boolean}
         * 
		 * @memberof MediaManagerFileTree
         * @private
         */
        _onChange(e) {
            let target = e.target,
                item = Selectors.closest(target, 'li'),
                self = this, res, id, value, type,
                manager = self.manager,
                o = self.options, element = self.element;
            id = target.id; value = target.value;
            type = item.acmsData('role').trim() === 'folder' ? 'folder' : 'file';
            if(parseInt(target.value) === self.currentFolder) {
                AcmsEvent.stop(e);
                target.checked = !target.checked;
                return false;
            }
            const activeSubs = Selectors.qa('input:checked:not(#'+id+')', item);
            if(activeSubs.length > 0) {
                AcmsEvent.stop(e);
                target.checked = !target.checked;
                activeSubs.reverse().forEach(sub => {
                    sub.checked = false;
                    let itm = Selectors.closest(sub, 'li');
                    AcmsEvent.fireChange(sub);
                });
            }
            if(target.checked) {
                manager.dispatch('folderTree.beforeActivate', {tree: self, folder: target});
                self.currentFolder = target.value;
                self._currentItem = item;
                self._toggleChb(target, item);
                let others = Selectors.qa('input:checked:not(#'+id+')', element);
                others.forEach((chb) => {
                    let itm = Selectors.closest(chb, 'li');
                    if(itm && Selectors.q('li[data-folder="' + target.value + '"]', itm)) {
                        return;
                    }
                    chb.checked = false;
                    chb.removeAttribute('checked');
                    AcmsEvent.fireChange(chb);
                });
                
                manager.dispatch('folderTree.activate', {tree: self, folder: target});
            } else {
                self._toggleChb(target, item);
                manager.dispatch('folderTree.deactivate', {tree: self, folder: target});
                
            }
        }

        /**
         * Toggle Folder-Aware Actions
         *
         * @param {Boolean}     activated   Activated?
         * 
         * @memberof MediaManagerFileTree
         * @private
         */
        _toggleFolderActions(activated) {
            const self = this;
            Selectors.qa('[data-action="folder"]', self.manager.element).forEach(action => {
                if(true === activated) {
                    Classes.removeClass(action, 'disabled');
                    action.removeAttribute('disabled');
                } else {
                    Classes.addClass(action, 'disabled');
                    action.setAttribute('disabled', 'disabled');
                }
            });
        }
        
        /**
         * Wechseln der zusätzlichen Klassen und Attribute nach einem Checkbox
         * wechsel.
         * 
         * @param {HTMLInputElement} chb     Die Checkbox
         * @param {HTMLOListElement} item    Das übergeordnete List-Item
         * 
		 * @memberof MediaManagerFileTree
         * @private
         */
        _toggleChb(chb, item) {
            let self = this, o = self.manager.options, cls = o.classNames.fileTreefolder.active;
            if(chb.checked) {
                chb.setAttribute('checked', 'checked');
                Classes.addClass(item, cls);
            } else {
                chb.removeAttribute('checked');
                Classes.removeClass(item, cls);
            }
        }

        /**
         * On Loaded Callback
         *
         * @param {Response} response
         * 
		 * @memberof MediaManagerFileTree
         * @private
         */
        _onLoaded(response) {
            if(response.code !== 200) {
                return A.Notifications.createFromResponse(response);
            }
            const self = this;
            self._loaded = true;
            self.manager.token = response.token;
            self.manager.ftree.innerHTML = "";
            response.folders.forEach(folder => {
                self._makeFolder(folder);
            });
        }

        /**
         * Make Folder
         *
         * @param {Object} folder  Folder
         * 
		 * @memberof MediaManagerFileTree
         * @private
         */
        _makeFolder(folder) {
            const self = this,
                data = {
                    folder: folder,
                    current: self.currentFolder
                }, 
                manager = self.manager,
                template = manager.options.templates.folders.treefolder,
                templateEngine = manager.templateEngine,
                cb = content => {}, 
                appendTo = self.manager.ftree;
            templateEngine.compileResource(template, data, cb, appendTo);
        }

        /**
         * Prepare Module
         *
         * @memberof MediaManagerFileTree
         * @private
         */
        _prepare() {
            let self = this, element = self.manager.ftree;
            self._loaded = false;
            self._loading = false;
            self.load();
        }

        /**
         * Ausführung bei fehlgeschlagener Abfrage
         * 
		 * Wird ausgeführt, wenn keine Antwort vom Server kommt bzw die 
         * Anfrage fehlschlägt
         * 
         * @param {XMLHttpRequest}  xhr    		Das XHR Abfrage Object
         * @param {String}	        textStatus 	Text Status Message
		 * 
		 * @memberof MediaManagerFileTree
         * @private
         */
        _fail (xhr, textStatus) {
            A.Logger.writeLog(textStatus);
        }
        
        /**
         * Auführung bei jeder Abfrage
         * 
         * @param {XmlHttpRequest} xhr XHR Request instance
         * @param {String} textStatus Text Status
         * 
		 * @memberof MediaManagerFileTree
         * @private
         */
        _always (xhr, textStatus) {
            A.Logger.writeLog(textStatus);
            this._loading = false;
        }
        
    }

    return MediaManagerFileTree;
});
