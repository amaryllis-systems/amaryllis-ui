/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/profile/media-manager/helpers/file-list", [
	"core/base",
	"core/classes",
	"core/selectors",
	"events/event",
	"apps/ui/navs/context-menu",
	"core/acms"
], (Base, Classes, Selectors, AcmsEvent, ContextMenu, A) => {

	/**
	 * File List
     * 
     * Class representing the file and folder list in media manager window body.
	 *
	 * @class
	 * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     * 
     * @property {MediaManager} manager Media Manager instance
     * @property {Boolean} loaded already loaded the file list?
     * @property {Boolean} loading currently loading the file list?
     * @property {String} remote Remote URL
	 */
	class MediaManagerFileList extends Base {

		/**
		 * Module Namespace
		 *
         * @var {String}
         *
		 * @readonly
		 * @static
		 * @memberof MediaManagerFileList
		 */
		static get NS() {
			return "apps.profile.media-manager.helpers.file-list";
		}

		/**
         * Module Name
         * 
         * @var {String}
         *
         * @readonly
         * @static
         * @memberof MediaManagerFileList
         */
        static get MODULE() {
			return "Media Manager File List";
		}

		/**
		 * Default Options
		 *
         * @var {Object}
         *
		 * @readonly
		 * @static
		 * @memberof MediaManagerFileList
		 */
		static get DEFAULT_OPTIONS() {
			return {
				remote: null, // Remote Action Url
				open: 'checked', // CSS-Klasse für ausgewählte items
				folderId: null, // Ordner-ID
				beforeActivate: (chbs, chb, self) => {}, // wird vor der Aktivierung aufgerufen
				onActivate: (chbs, chb, self) => {}, // wird nach der Aktivierung aufgerufen
				onDeactivate: (chbs, chb, self) => {}, // wird nach der Deaktivierung aufgerufen
			};
        }
        
		/**
		 * Module Init
		 *
		 * @static
		 * 
		 * @param {MediaManager} MediaManager		Media Manager Instance
		 * 
		 * @returns {MediaManagerFileList}	New instance
		 * 
		 * @memberof MediaManagerFileList
		 */
		static init(MediaManager) {
			let m = new MediaManagerFileList(MediaManager);
			m.element.acmsData(MediaManagerFileList.NS, m);
			
			return m;
        }
        
		/**
         * Creates an instance of MediaManagerFileList.
         * 
         * @param {MediaManager} MediaManager  Instance of Media Manager
         * @memberof MediaManagerFileList
         */
        constructor(MediaManager) {
			super(MediaManager.flist, MediaManager.options);
            this._manager = MediaManager;
            this._remote = MediaManager.remote;
			this._initialize();

		}

		get manager() {
			return this._manager;
		}


        get loaded() {
            return this._loaded;
        }

        get loading() {
            return this._loading;
        }

        get remote() {
            return this._remote;
        }
		
        /**
         * Refresh folder list
         * 
         * Forces updating the file list, regardless of other current actions.
         *
         * @memberof MediaManagerFileList
         */
        refresh() {
            const self = this;
            self._loaded = false;
            self._loading = false;
            self._stopped = true;
            setTimeout(() => {
                self.load();
            }, 100);
        }

        /**
         * Load medias for current folder
         * 
         * Method starts loading the contents of a current selected folder in 
         * {@link MediaManagerFileTree}. 
         * 
         * @returns {Boolean}   `true` if the remote process has started, `false` if either 
         *                      no folder is selected in file-tree or file-list is already 
         *                      loading from remote.
         *
         * @memberof MediaManagerFileList
         */
        load() {
            let self = this, element = self.manager.flist, fData = new FormData();
            if(self._loaded || self._loading) {
                return false;
            }
            const current = self.manager.filetree.currentFolder;
            if(!current) {
                return false;
            }
            fData.append('folder_id', current);
            if(self.options.accept) {
                const accept = Object.keys(self.options.accept).map(x => self.options.accept[x]);
                fData.append('extension', accept.join(','));
            }
            fData.append('token', self.manager.token);
            fData.append('op', 'loadMedia');
            require(['http/request', 'notifier'], (Request) => {
                Request.post(self.remote, fData, self._onLoaded, self._fail, self._always);
            });
            return true;
        }

        /**
         * Sort Elements in file list
         *
         * @memberof MediaManagerFileList
         */
        sort() {
            const self = this, element = self.element, items = Selectors.qa('li', element);
            items.sort(self._compare);
        }

        /**
         * Aufsetzen der Event-Listener
         * 
         * @memberof MediaManagerFileList
         * @private
         */
        listen() {
            let self = this, rms = self._rm, element = self.element;
            AcmsEvent.add(self.element, 'contextmenu', (ev) => {
                ev.preventDefault();
                
                return false;
            }, false);
            
            AcmsEvent.add(document, 'keydown', self._onKeyDown);
            AcmsEvent.add(document, 'keyup', self._onKeyUp);
            AcmsEvent.on(element, 'click', 'li', self._onClick, true);
            AcmsEvent.on(element, 'dblclick', 'li', self._onDblClick, true);
            AcmsEvent.on(element, 'change', 'input[type="checkbox"]', self._onChange);
        }

        /**
         * On Loaded Callback
         *
         * @param {Response} response  Remote response callbacl
         * 
         * @memberof MediaManagerFileList
         * @private
         */
        _onLoaded(response) {
            if(response.code !== 200) {
                return A.Notifications.createFromResponse(response);
            }
            const self = this;
            self.manager.token = response.token;
            const total = response.folders.length + response.files.length;
            const o = self.options;
			let j, t, compiled = 0;
            const files = response.files, folders = response.folders;
            self._folders = [];
			self._files = [];
            self.manager.flist.innerHTML = '';
            const done = () => {
				compiled++;
            };
            let stopped = false;
            const isStopped = () => {
                if(self._stopped === true) {
                    //stopped = true;
                    //self._stopped = false;
                }
                return stopped;
            };
            folders.forEach(folder => {
                if(isStopped()) {
                    return false;
                }
                self._makeFolder(folder, done);
            });
            files.forEach(file => {
                if(isStopped()) {
                    return false;
                }
                self._makeFile(file, done);
            });
            let interval = setInterval(() => {
                if(isStopped()) {
                    clearInterval(interval);
                    return false;
                }
				if(total === compiled) {
                    clearInterval(interval);
                    self.sort();
					//if(cb) {
						//cb(self, data);
					//}
					if(typeof o.onLoadedMedias === 'function') {
						o.onLoadedMedias(folders, files, response, self);
					}
					self.manager.emit('loadedMedia', folders, files, response, self);
					
				}
			}, 10);
        }
        
		/**
		 * Make a Folder
		 *
		 * @param {Object} 				data	Folder Data
		 * @param {CallableFunction} 	done	Callback will we used to indicate if the template engine has rendererd the folder
		 * 
		 * @memberof MediaManagerFileList
         * @private
		 */
		_makeFolder(data, done) {
            const self = this;
            const current = self.manager.filetree.currentFolder, tdata = {folder: data, current: current}, o = self.options, template = o.templates.folders.folder, engine = self.manager.templateEngine;
			engine.compileResource(template, tdata, done, self.manager.flist);
		}

		/**
		 * Make a file
		 *
		 * @param {Object} 				data	file Data
		 * @param {CallableFunction} 	done	Callback will we used to indicate if the template engine has rendererd the file
		 * 
		 * @memberof MediaManagerFileList
         * @private
		 */
		_makeFile(data, done) {
			let self = this, tdata = {file: data}, o = self.options, template = o.templates.files.file, engine = self.manager.templateEngine;
			engine.compileResource(template, tdata, done, self.manager.flist);
		}

        /**
         * On KeyDown Event Handler
         * 
         * @param {Event} e Das Event
         * 
         * @memberof MediaManagerFileList
         * @private
         */
        _onKeyDown(e) {
            const self = this; 
            const code = parseInt(e.which || e.keyCode);
            switch(code) {
                // ctrl
                case 17:
                    self.ctrlDown = true;
                    return true;
                // F2
                case 113:
                    //self._onClickRename(e);
                    self.manager.emit('filelist.keydown.F2', e);
                    break;
                // entf
                case 46:
                    self.manager.emit('filelist.keydown.del', e);
                    break;
                default:
                    if(e.key === undefined) return; // stop unsupported keys
                    self.manager.emit('filelist.keydown.' + e.key, e);
                    break;
            }
        }
        
        /**
         * On KeyUp Event Handler
         * 
         * @param {Event} e Das Event
         * 
         * @memberof MediaManagerFileList
         * @private
         */
        _onKeyUp(e) {
            this.ctrlDown = false;
        }

        /**
         * On Click List-Item Event Handler
         * 
         * @param {Event} e Das Event
         * 
         * @returns {Boolean}
         * 
         * @memberof MediaManagerFileList
         * @private
         */
        _onClick(e) {
            let item = e.target;
            if(item.tagName.toLowerCase() !== 'li') {
                item = Selectors.closest(item, 'li');
            }
            const chb = Selectors.q('input[type="checkbox"]', item);
            if(chb) {
                chb.checked = !chb.checked;
                AcmsEvent.fireChange(chb);
                Classes.toggleClass(item, 'active');
            }
        }

        /**
         * On Dbl Click 
         *
         * @param {Event} e     Event
         * 
         * @memberof MediaManagerFileList
         * @private
         */
        _onDblClick(e) {
            const self = this;
            let item = e.target;
            if(item.tagName.toLowerCase() !== 'li') {
                item = Selectors.closest(item, 'li');
            }
            if(Selectors.q('input[type="text"]', item)) {
                return;
            }
            const id = item.acmsData('id');
            const role = item.acmsData('role');
            if(role === 'folder') {
                const chb = Selectors.q('input[type="checkbox"][value="' + id + '"]', self.manager.ftree);
                if(chb) {
                    chb.checked = true;
                    chb.setAttribute('checked', 'checked');
                    AcmsEvent.fireChange(chb);
                    Classes.toggleClass(item, 'active');
                }
            } else {
                const manager = self.manager;
                manager.dispatch('select', {id: id, item: item});
            }
        }
        
        /**
         * Checkbox change Event Handler
         * 
         * @param {Event} e Das Event
         * 
         * @memberof MediaManagerFileList
         * @private
         */
        _onChange(e) {
            let target = e.target,
                item = Selectors.closest(target, 'li'),
                self = this, res, id, value, type,
                o = self.options, element = self.element;
            id = target.id; value = target.value;
            type = Classes.hasClass(item, 'folder') ? 'folder' : 'file';
            if(target.checked) {
                res = o.beforeActivate(self.tree, target, self);
                if(false === res) {
                    AcmsEvent.stop(e);
                    return false;
                }
                if(null === self.currentSelection) {
                    self.currentSelection = {};
                    self.currentSelection.folder = {};
                    self.currentSelection.file = {};
                }
                if(self.ctrlDown === false) {
                    let others = Selectors.qa('input:checked:not(#'+id+')', element);
                    others.forEach(function(chb) {
                        chb.checked = false;
                        chb.removeAttribute('checked');
                        AcmsEvent.fireChange(chb);
                    });
                }
                self.totalSelected++;
                self._toggleChb(target, item);
                self.currentSelection[type][value] = item;
                if(self.totalSelected === 1) {
                    Selectors.qa('[data-action="selection"]', self.manager.element).forEach(i => {
                        self._toggleFolderActions(i, true);
                    });
                } else {
                    Selectors.qa('[data-action="selection"]', self.manager.element).forEach(i => {
                        self._toggleFolderActions(i, true);
                    });
                    Selectors.qa('[data-action="multi-selection"]', self.manager.element).forEach(i => {
                        self._toggleFolderActions(i, true);
                    });
                }
                
                o.onActivate(self.chbs, target, self);
                self.manager.dispatch('filelist.activated', {target: target, list: self});
            } else {
                if(typeof self.currentSelection[type][value] !== "undefined") {
                    delete self.currentSelection[type][value];
                }
                self._toggleChb(target, item);
                o.onDeactivate(self.chbs, target, self);
                self.totalSelected--;
                if(self.currentSelection.folder.length === 0 && 
                        self.currentSelection.file.length === 0) {
                    self.currentSelection = null;
                    self.totalSelected = 0;
                    
                }
                if(self.totalSelected === 1) {
                    Selectors.qa('[data-action="selection"]', self.manager.element).forEach(i => {
                        self._toggleFolderActions(i, true);
                    });
                } else if (self.totalSelected > 1) {
                    Selectors.qa('[data-action="selection"]', self.manager.element).forEach(i => {
                        self._toggleFolderActions(i, true);
                    });
                    Selectors.qa('[data-action="multi-selection"]', self.manager.element).forEach(i => {
                        self._toggleFolderActions(i, true);
                    });
                } else {
                    Selectors.qa('[data-action="selection"]', self.manager.element).forEach(i => {
                        self._toggleFolderActions(i, false);
                    });
                    Selectors.qa('[data-action="multi-selection"]', self.manager.element).forEach(i => {
                        self._toggleFolderActions(i, false);
                    });
                }
                
                if(self.ctrlDown === false) {
                    id = target.id;
                    let others = Selectors.qa('input:checked', element), it;
                    others.forEach(function(chb) {
                        chb.checked = false;
                        AcmsEvent.fireChange(chb);
                        chb.removeAttribute('checked');
                    });
                }
            }

        }
        
        /**
         * Toggle Folder-Aware Actions
         *
         * @param {HTMLElement} action      Action item
         * @param {Boolean}     activated   Activated?
         * 
         * @memberof MediaManagerFileTree
         * @private
         */
        _toggleFolderActions(action, activated) {
            const self = this;
            if(true === activated) {
                Classes.removeClass(action, 'disabled');
                action.removeAttribute('disabled');
            } else {
                Classes.addClass(action, 'disabled');
                action.setAttribute('disabled', 'disabled');
            }
        }
        
        /**
         * Wechseln der zusätzlichen Klassen und Attribute nach einem Checkbox
         * wechsel.
         * 
         * @param {Element} chb     Die Checkbox
         * @param {Element} item    Das übergeordnete List-Item
         * 
         * @memberof MediaManagerFileList
         * @private
         */
        _toggleChb(chb, item) {
            let self = this, o = self.options;
            if(chb.checked) {
                chb.setAttribute('checked', 'checked');
                Classes.addClass(item, o.open);
            } else {
                chb.removeAttribute('checked');
                Classes.removeClass(item, o.open);
            }
        }
        
        /**
         * Vorbereiten des Moduls
         * 
         * @memberof MediaManagerFileList
         * @private
         */
        _prepare() {
            let self = this, o = self.options, element = self.element, opts;
            self.ctrlDown = false;
            self.currentSelection = null;
            self.totalSelected = 0;
            self.mm = Selectors.closest(element, '.media-manager');
            self._rm = Selectors.qa('[data-role="remove"]', self.mm);
            self.ctx = Selectors.q('#mediamanger-context-menu');
            opts = {
                target: "#mediamanger-context-menu",
                onOpen: (ctx, menu) => {
                    if(self.currentSelection) {
                        
                    } else {
                        
                    }
                },
                onClose: (ctx, menu) => {
                    
                },
                onClick: (ctx, menu, ele) => {
                    console.log(ele);
                    console.log(ele.getAttribute('data-trigger'));
                },
            };
            self.contextMenu = ContextMenu.init(self.element, opts);
            self.refresh();
        }
        
        /**
         * Compare two elements
         *
         * @param {HTMLElement} v1  Element 1
         * @param {HTMLElement} v2  Element 2
         * 
         * @returns {Number} `1`, `0` , `-1`
         * 
         * @memberof MediaManagerFileList
         * @private
         */
        _compare(v1, v2) {
            const roleA = v1.acmsData('role');
            const roleB = v2.acmsData('role');
            const titleA = v1.acmsData('title');
            const titleB = v2.acmsData('title');
            if(roleA === roleB) {
                return titleA.localeCompare(titleB);
            }
            if(roleA === 'folder') {
                return 1;
            }
            return -1;
        }

	}

	return MediaManagerFileList;
});
