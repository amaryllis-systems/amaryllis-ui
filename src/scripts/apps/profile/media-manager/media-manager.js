/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/profile/media-manager/media-manager", [
	"core/base",
    "core/selectors",
	"core/classes",
	"tools/string/string-tool",
    "apps/ui/informations/window",
    "apps/profile/media-manager/commands/commands",
	"apps/profile/media-manager/helpers/file-tree",
	"apps/profile/media-manager/helpers/file-list",
	"templates/template-engine",
	"events/event",
    "core/acms"
], (Base, Selectors, Classes, StringTool, WindowBox, Commands, FileTree, FileList, TemplateEngine, AcmsEvent, A) => {

    /**
	 * Profile Media Manager
	 *
	 * @class
	 * @extends {Base}
	 * @author qm-b <https://bitbucket.org/qm-b/>
	 * 
	 * @property {String} token Token
	 */
	class MediaManager extends Base {

		/**
		 * Module Namespace
		 *
		 * @readonly
		 * @static
		 * @memberof MediaManager
		 */
		static get NS() {
			return "apps.profile.media-manager.media-manager";
		}

        static get MODULE() {
            return "Profile Media Manager";
        }

        /**
         * Modul Initialisisierung
         * 
         * @static
         * 
         * @param {HTMLElement} element Das HTML Element
         * @param {Object|null} options Optionale eigene Optionen
         *  
         * @returns {MediaManager}  Meue Instanz
         * 
         * @memberof MediaManager
         */
        static init(element, options) {
			try {
            let m = new MediaManager(element, options);
            m.element.acmsData(MediaManager.NS, m);
				return m;
			} catch(E) {
				console.log(E.message + ' in ' + E.fileName + '::' + E.lineNumber);
				console.log(E.stack);
				return false;
			}
		}
		
		/**
		 * Media Manager css
		 *
		 * @readonly
		 * @static
		 * @memberof MediaManager
		 */
		static get css() {
			return 'media/css/ui/media-manager';
		}

        /**
         * Standard Optionen
         * 
         * @readonly
         * @static
         * 
         * @memberof MediaManager
         */
        static get DEFAULT_OPTIONS() {
            return {
                accept: false,
				remote: null,
				token: null,
				classNames: {
					fileTree: 'my-files',
					fileTreefolder: {
						active: 'open'
					},
					fileList: 'file-list'
				},
				allowed: null,
				selectors: {
					fileTree: '.my-files',
					fileList: '.file-list',
                    commands: '[data-command]',
                    folder: {
						actions: '[data-action="folder"]', // actions requiring an activated folder in folder tree
						folder: '[data-role="folder"]', // folder in file-list
					},
					files: {
						file: '[data-role="file"]', // file-list file
					},
                    window: {
						// dismiss button of the window overlay
						dismissOverlay: '[data-dismiss="window-overlay"]',
						// notebook list of the current profile user
						myFiles: '.my-files'
					}
				},
				templates: {
					files: {
                        file: 'templates/html/profile/media-manager/file-list/file/file.hbs',
                        form: 'templates/html/profile/media-manager/file-list/file/form.hbs'
					},
					folders: {
						folder: 'templates/html/profile/media-manager/file-list/folder/folder.hbs',
						treefolder: 'templates/html/profile/media-manager/file-tree/folder/folder.hbs',
                        form: 'templates/html/profile/media-manager/file-list/folder/form.hbs'
					},
					confirmDelete: "templates/html/profile/media-manager/confirm-delete-form.hbs"
                },
				commands: {},
				css: MediaManager.css
            };
        }

        /**
         * Creates an instance of MediaManager.
         * 
         * @param {HTMLElement} element Das HTML Element
         * @param {Object|null} options Optionale eigene Optionen
         * 
         * @memberof MediaManager
         */
        constructor(element, options) {
            super(element, options);
            this._folders = {};
			this.root = 0;
			this._on = {};
            this._initialize();
        }

		/**
		 * File Tree Instance
		 * 
		 * @var {FileTree}
		 *
		 * @readonly
		 * @memberof MediaManager
		 */
		get filetree() {
            return this._filetree;
		}
		
		/**
		 * File Tree HTML Element
		 * 
		 * @var {HTMLElement}
		 *
		 * @readonly
		 * @memberof MediaManager
		 */
		get ftree() {
            return this._tree;
        }

		/**
		 * FileList instance
		 *
		 * @readonly
		 * @memberof MediaManager
		 */
		get filelist() {
            return this._filelist;
		}
		
		/**
		 * filelist HTML Element
		 *
		 * @readonly
		 * @memberof MediaManager
		 */
		get flist() {
            return this._flist;
        }

		/**
		 * Remote Url
		 *
		 * @readonly
		 * @memberof MediaManager
		 */
		get remote() {
            return this._remote;
		}
		
		/**
		 * Holding the Template engine
		 *
		 * @readonly
		 * @memberof MediaManager
		 */
		get templateEngine() {
			return TemplateEngine;
		}

		get currentFolder() {
			return this._currentFolder;
		}

		set currentFolder(id) {
			this._currentFolder = parseInt(id);
			this._toggleActions(this._currentFolder > 0);
		}
		
		get folders() {
			return this._folders;
		}

		get windowOverlay() {
			return this._windowOverlay;
		}

		get windowBox() {
			return this._windowBox;
		}

		get windowMenu() {
			return this._windowMenu;
		}

		get windowSideList() {
			return this._windowSideList;
		}

		get windowDetails() {
			return this._windowDetails;
		}

		get token() {
			return this._token;
		}

		set token(token) {
			this._token = token.trim();
		}

		/**
		 * Internal Event dispatch
		 * 
		 * Method used internal to dispatch an Event from Plugin Side. Checks for Callables added by `on()` and applies the Callback
		 *
		 * @param {String} evt	Name of the Event to dispatch
		 * @param {Object} data	Optional custom data passed to the Event and Callback
		 * 
		 * @memberof MediaManager
		 * @private
		 */
		dispatch(evt, data) {
			let self = this, name = MediaManager.NS + '.' + evt, params = {
				'related': self.element,
				'manager': self
			};
			if(data && typeof data === "object") {
				for(let prop in data) {
					if(data.hasOwnProperty(prop)) {
						params[prop] = data[prop];
					}
				}
			}
			AcmsEvent.emit(self.element, name, params);
			self.emit(evt, params);
		}
        
		/**
		 * Showing the window overlay
		 *
		 * @memberof MediaManager
		 */
		showWindowOverlay() {
			let self = this, 
                o = self.options;
            if(!self.overlay) {
                return;
            }
			Classes.toggleClass(self.overlay, 'active');
			self.overlay.setAttribute('aria-hidden', 'false');
		}

		/**
		 * Hiding the window overlay
		 *
		 * @memberof MediaManager
		 */
		hideWindowOverlay() {
			let self = this, 
                o = self.options;
            if(!self.overlay) {
                return;
            }
			Classes.toggleClass(self.overlay, 'active');
			self.overlay.setAttribute('aria-hidden', 'true');
		}

        /**
		 * Setup Event Listener
		 *
		 * @memberof MediaManager
		 * @private
		 */
		listen() {
            let self = this, element = self.element, o = self.options, sel = o.selectors;
            AcmsEvent.on(element, 'click', sel.window.dismissOverlay, self._onClickDismissOverlay, true);
		}
		
		buildOptions(options) {
			let o = super.buildOptions(options), accept;
			if(typeof o.accept === 'string') {
				try {
					accept = JSON.parse(o.accept);
				} catch(error) {
					accept = o.accept;
				}
				o.accept = accept;
			}

			return o;
		}

		/**
		 * On click dismiss overlay event handler
		 *
		 * @param {Event} e Click Event
		 * 
		 * @memberof Notes
		 */
		_onClickDismissOverlay(e) {
			let self = this, 
                o = self.options;
            if(!self.overlay) {
                return;
            }
            if(e)  {
				e.preventDefault();
				e.stopPropagation();
			}
            self.hideWindowOverlay();
		}

        /**
		 * Window Callback Event "Before overlay open"
		 *
		 * @param {HTMLElement} overlay		Window overlay
		 * @param {HTMLElement} trigger		Trigger that has been used to open the overlay
		 * 
		 * @memberof Notes
		 */
        _beforeOverlayOpen(overlay, trigger, wbox, woverlay) {
			const self = this, o = self.options;
			let matches, t1, t2, t3;
            t1 = Selectors.closest(trigger, '[data-role="note-add"]');
			/*
            matches = (Selectors.matches(trigger, '[data-role="note-add"]') || (null !== t1));
            if(matches) {
				self._beforeAddOverlayOpen(overlay, trigger, wbox, woverlay);
				return true;
			}
			t2 = Selectors.closest(trigger, '[data-role="note-edit"]');
            matches = (Selectors.matches(trigger, '[data-role="note-edit"]') || (null !== t2));
            if(matches) {
				self._beforeEditOverlayOpen(overlay, trigger, wbox, woverlay);
				return true;
			}
			t2 = Selectors.closest(trigger, o.selectors.notebook.remove);
			matches = (Selectors.matches(trigger, o.selectors.notebook.remove) || (null !== t2));
			if(matches) {
				self._beforeDeleteNotebookOverlayOpen(overlay, trigger, wbox, woverlay);
            }
            */
		    self.emit('window.overlay.close', overlay, trigger, wbox, woverlay);
            return true;
        }
        
        /**
		 * Window Callback Event "Before overlay dismiss"
		 *
		 * @param {HTMLElement} overlay		Window overlay
		 * @param {HTMLElement} trigger		Trigger that has been used to close the overlay
		 * 
		 * @memberof Notes
		 */
        _beforeOverlayDismiss(overlay, trigger, wbox, woverlay) {
            const self = this, folderId = parseInt(overlay.acmsData('folder'));
            //if(folderId && typeof self.folders[folderId] !== 'undefined') {
            //    self.folders[folderId].refresh();
			//}
			self.emit('window.overlay.close', overlay, trigger, wbox, woverlay);
            return true;
        }
    
        /**
		 * Window Callback Event "On overlay close"
		 *
		 * @param {HTMLElement} overlay		Window overlay
		 * @param {HTMLElement} trigger		Trigger that has been used to close the overlay
		 * 
		 * @memberof Notes
		 */
        _onOverlayClose(overlay, trigger, wbox, woverlay) {
			const self = this;
			let overlayContent;
			self.emit('window.overlay.close', overlay, trigger, wbox, woverlay);
            overlayContent = Selectors.q('.overlay-content', overlay);
            if(overlayContent) overlayContent.innerHTML = '';
		}
		
		_onOverlayOpen(overlay, trigger, wbox, woverlay) {
			const self = this;
			self.emit('window.overlay.close', overlay, trigger, wbox, woverlay);
		}

		_toggleActions(activate) {
			let self = this, o = self.options, factions, element = self.element;
			factions = Selectors.qa(o.selectors.folder.actions, element);
			factions.forEach((action) => {
				if(activate === true) {
					Classes.removeClass(action, 'disabled');
					action.removeAttribute('disabled');
				} else {
					Classes.addClass(action, 'disabled');
					action.setAttribute('disabled', 'disabled');
				}
			});
		}

        /**
         * Vorbereiten des Moduls imn Rahmen der Initialisierung
         * 
         * 
         * @memberof MediaManager
         */
        _prepare() {
            let self = this, o = self.options, element = self.element, callback = (cmds) => {
                self._loadedCommands = cmds;
			};
			self._remote = o.remote;
			self._token = o.token;
            if(typeof self.remote !== 'string' || false === StringTool.isUrl(self.remote)) {
                throw new Error('Remote URL nicht angegeben');
            }
            try {
                self._tree = Selectors.q(o.selectors.fileTree, element);
                self._flist = Selectors.q(o.selectors.fileList, element);
                self._commands = new Commands(self, callback);

                self._filetree = new FileTree(self);
                self._filelist = new FileList(self);
            } catch(E) {
                console.log(E.message);
            }
            self._windowOverlay = Classes.hasClass(element, 'window') ? element : Selectors.q('.window', element);
            self._prepareWindowOverlay();
            self.overlay = Selectors.q('.window-overlay', self.windowOverlay);
            self.dismissOverlay = Selectors.qa('[data-dismiss="window-overlay"]', self.windowOverlay);
		}
		
		/**
		 * Prepare Window Overlay Module
		 *
		 * @memberof Notes
		 */
		_prepareWindowOverlay() {
			let self = this, 
                o = self.options,
				element = self.element;
			let windowOptions = {
				owner: o.owner,
				overlayOptions: {
					beforeDismiss: function(overlay, trigger, wbox, woverlay) {
						return self._beforeOverlayDismiss(overlay, trigger, wbox, woverlay);
					},
					onDismiss: function(overlay, trigger, wbox, woverlay) {
						self._onOverlayClose(overlay, trigger, wbox, woverlay);
					},
					beforeOpen: function(overlay, trigger, wbox, woverlay) {
						return self._beforeOverlayOpen(overlay, trigger, wbox, woverlay);
					},
					onOpen: function(overlay, trigger, wbox, woverlay) {
						self._onOverlayOpen(overlay, trigger, wbox, woverlay);
					}
				}
			};
			self._windowBox = WindowBox.init(self.windowOverlay, windowOptions);
			self._windowMenu = Selectors.q('.window-menu', self.windowOverlay);
			self._windowDetails = Selectors.q('.window-details', self.windowOverlay);
			self._windowSideList = Selectors.q('.side-list', self.windowOverlay);
		}

    }

    return MediaManager;

});
