/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("apps/profile/media-manager/commands/commands", [
	"core/base",
	"core/classes",
	"core/selectors",
	"events/event",
	"core/acms"
], (Base, Classes, Selectors, AcmsEvent, A) => {

	/**
	 * Media Manager Commands
	 * 
	 * Class Managing Commands added to the Media Manager
	 *
	 * @class
	 * @extends {Base}
	 * @author qm-b <https://bitbucket.org/qm-b/>
	 * 
	 * @property {MediaManager} manager Media Manager instance
	 * 
	 */
	class Commands extends Base {

		/**
		 * Module Namespace
		 *
		 * @readonly
		 * @static
		 * @memberof Commands
		 */
		static get NS() {
			return "apps.profile.media-manager.commands.commands";
		}

		/**
		 * Module Name
		 *
		 * @readonly
		 * @static
		 * @memberof Commands
		 */
		static get MODULE() {
			return "Media Manager Commands";
		}

		/**
		 * Default Options
		 *
		 * @readonly
		 * @static
		 * @memberof Commands
		 */
		static get DEFAULT_OPTIONS() {
			return {};
		}

		/**
		 * Module Init
		 *
		 * @static
		 * 
		 * @param {MediaManager} 		MediaManager	Media Manager instance
		 * @param {CallableFunction} 	callback		HTML Wrapper around the media manager
		 * 
		 * @returns {Commands}	New Instance
		 * 
		 * @memberof Commands
		 */
		static init(MediaManager, callback) {
			let m = new Commands(MediaManager, callback);
			m.element.acmsData(Commands.NS, m);
			
			return m;
		}


        /**
		 * Creates an instance of Commands.
		 * 
		 * @param {MediaManager} 		MediaManager	Media Manager instance
		 * @param {CallableFunction} 	callback		HTML Wrapper around the media manager
		 * 
		 * @memberof Commands
		 */
		constructor(MediaManager, callback) {
			super(MediaManager.element, MediaManager.options);
            this._manager = MediaManager;
            this.required = 0;
            this.initialized = 0;
			this.done = false;
			this.callback = callback;
			this._commands = {};
			this._initialize();
        }

        /**
		 * Gets the Media Manager Instane
		 *
		 * @readonly
		 * @memberof Commands
		 */
		get manager() {
            return this._manager;
		}
		
		get commands() {
			return this._commands;
		}

        /**
         * Ist ein Command vorhanden?
         * 
         * @param {String} cmd Command Name
         * @returns {Boolean}   `TRUE` wenn es den Command gibt
         * 
         * @memberOf Commands
         */
        has(cmd) {
            return (typeof this.commands[cmd] !== "undefined");
		}
		
		listen() {
			let self = this, element = self.element;
			AcmsEvent.on(element, 'click', '[data-command]', self._onClickCommand, true);
		}

        /**
         * Ausführen eines Commands
         * 
         * @param {String} 		cmd  	Der Command Name
		 * @param {HTMLElement} target 	Command Button, if any
         * @param {Event} 		e     	Optionales Event, durch den der Command ausgelöst wurde
         * 
         * @memberOf Commands
         */
        exec(cmd, target, e) {
            if(false === this.has(cmd)) {
                return false;
            }
            return this.commands[cmd].apply(target, e);
        }

        /**
         * On Click Command Event Handler
         * 
         * @param {Event} e Das Event
         * 
         * @memberOf Commands
         */
        _onClickCommand(e) {
            let self = this, target = e.target, cmd, o = self.options;
            if(!target.acmsData('command')) {
                target = Selectors.closest(target, o.selectors.commands);
            }
            if(!target) {
                return false;
            }
            cmd = target.acmsData('command');
            if(self.has(cmd)) {
                //e.preventDefault();
                //e.stopPropagation();
                self.exec(cmd, target, e);
                return false;
            }
        }

        _prepare() {
            if(true === this.done) {
                return;
            }
			let self = this, element = self.element, o = self.options, cmds;
			cmds = Selectors.qa(o.selectors.commands, element);
			cmds.forEach(ele => {
                
                let cmd = ele.acmsData('command'), mod;
                if(cmd.indexOf('/') >= 0) {
                    mod = cmd;
                } else {
                    mod = 'apps/profile/media-manager/commands/'+cmd;
                }
                self._require(mod, ele);
			});
			
        }

        _require(mod, element) {
			let self = this;
			self._required++;
            require([mod], function(cmd) {
                let command = new cmd(self.manager, element);
                let c = element.acmsData('command');
                if(c.indexOf('/') >= 0) {
					element.acmsData('command', command.getCommand());
					c = command.getCommand();
                }
                self._commands[c] = command;
                self.initialized++;
                if(self.required === self.initialized) {
                    self.callback(self.commands);
                }
            });
        }
	}

	return Commands;
});
