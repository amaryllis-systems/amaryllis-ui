/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("apps/profile/media-manager/commands/upload", [
	"apps/profile/media-manager/commands/command",
	"core/selectors",
	"events/event",
	"core/acms"
], (Command, Selectors, AcmsEvent, A) => {

	/**
	 * Upload Command
	 *
	 * @class
	 * @extends {Command}
	 * @author qm-b <https://bitbucket.org/qm-b/>
	 * 
	 * @property {MediaManager} manager Media Manager instance
	 * @property {HTMLElement} element The HTML element holding the command action.
	 * @property {MediaManagerFileTree} filetree File Tree instance
	 * @property {MediaManagerFileList} filelist File List instance
	 */
	class UploadCommand extends Command {

		/**
		 * Standard Optionen
		 * 
		 * @readonly
		 * @static
		 * 
		 * @memberof UploadCommand
		 */
		static get defaults() {
			return {
			};
		}

		/**
		 * Creates an instance of UploadCommand.
		 * 
		 * @param {view/profile/media-manager/media-manager} MediaManager Der Medien Manager
		 * 
		 * @memberof UploadCommand
		 */
		constructor(MediaManager, element) {
			super(MediaManager, element, 'upload', A.Translator._('Hochladen'), A.Translator._('Neue Dateien hochladen'));
			const input = Selectors.q('input[type="file"]', element);
			AcmsEvent.add(input, 'change', this._onChange);
		}

		/**
		 * Ausführen des Commands
		 * 
		 * @param {HTMLElement} btn Element
		 * @param {Event} e Event
		 * 
		 * @memberof UploadCommand
		 */
		apply(btn, e) {
			const self = this;
		}

		/**
		 * On Change Event Handler 
		 * 
		 * @param {Event} e Das Event
		 * 
		 * @memberof UploadCommand
		 */
		_onChange(e) {
			this._processRequest(e);
			return false;
		}

		_processRequest(e) {
			const self = this,
				  remote = self.manager.remote, 
				  fData = new FormData(),
				  input = e.target;
			if(input.getAttribute('disabled')) {
				return;
			}
			fData.append("file", input.files[0]);
			fData.append('folder_id', self.filetree.currentFolder);
			fData.append('op', 'upload');
			fData.append('token', self.manager.token);
			require(['http/request', 'notifier'], (Request) => {
				Request.post(remote, fData, self._onDone, self._fail, self._always);
			});
		}

		/**
		 * On Done upload
		 *
		 * @param {Response} response
		 * @memberof UploadCommand
		 */
		_onDone(response) {
			if(response.code !== 200) {
				return A.Notifications.createFromResponse(response);
			}
			const self = this;
			self.manager.token = response.token;
			//self._makeFile(response.media, () => {});
			self.manager.filelist.refresh();
		}

		/**
		 * Make a file
		 *
		 * @param {Object} 				data	file Data
		 * @param {CallableFunction} 	done	Callback will we used to indicate if the template engine has rendererd the file
		 * 
		 * @memberof MediaManagerFileList
         * @private
		 */
		_makeFile(data, done) {
			const self = this, 
				  tdata = {folder: data}, 
				  o = self.options, 
				  template = o.templates.files.file, 
				  engine = self.manager.templateEngine;
			engine.compileResource(template, tdata, done, self.manager.flist);
		}

	}

	return UploadCommand;
});

