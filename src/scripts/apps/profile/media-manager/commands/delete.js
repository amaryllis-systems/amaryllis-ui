/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/profile/media-manager/commands/delete", [
    "apps/profile/media-manager/commands/command",
    "core/classes",
    "core/selectors",
    "events/event",
    "core/acms"
], (Command, Classes, Selectors, AcmsEvent, A) => {

    /**
     * Delete Command
     *
     * @class
     * @extends {Command}
     * @author qm-b <https://bitbucket.org/qm-b/>
     * 
	 * @property {MediaManager} manager Media Manager instance
	 * @property {HTMLElement} element The HTML element holding the command action.
	 * @property {MediaManagerFileTree} filetree File Tree instance
	 * @property {MediaManagerFileList} filelist File List instance
     */
    class DeleteCommand extends Command {

		/**
		 * Standard Optionen
		 * 
		 * @readonly
		 * @static
		 * 
		 * @memberof UploadCommand
		 */
		static get defaults() {
			return {
			};
		}

		/**
		 * Creates an instance of UploadCommand.
		 * 
		 * @param {MediaManager} MediaManager Der Medien Manager
		 * 
		 * @memberof UploadCommand
		 */
		constructor(MediaManager, element) {
            super(MediaManager, element, 'delete', A.Translator._('Entfernen'), A.Translator._('Dateien und Ordner entfernen'));
            const self = this;
            MediaManager.on('filelist.keydown.del', self._onPressDel);
        }

        /**
         * Apply Command
         *
         * @memberof DeleteCommand
         */
        apply() {
            const self = this;
            const item = self._findItems();
            if(false === item) {
                return;
            }
            const manager = self.manager,
                  windowBox = manager.windowBox,
                  overlay = windowBox.windowOverlay,
                  ele = overlay.element,
                  tpl = manager.options.templates.confirmDelete;
            ele.innerHTML = "";
            self.overlay = overlay;
            const cb = content => {
                const form = Selectors.q('form', ele);
                AcmsEvent.on(form, 'click', '[data-role="submit"]', self._onConfirm, true);
                AcmsEvent.on(form, 'click', '[data-role="abort"]', self._onAbort, true);
                overlay.open();
            };
            const data = item;
            data.token = self.manager.token;
            manager.templateEngine.compileResource(tpl, data, cb, ele);
        }
        
        /**
         * On Press `del` key
         *
         * @param {Event} e Click event
         * 
         * @memberof DeleteCommand
         * @private
         */
        _onPressDel(e) {
            this.apply();
        }

        /**
         * On Abort Callback
         *
         * @param {Event} e   Click Event
         * 
         * @memberof DeleteCommand
         * @private
         */
        _onAbort(e) {
            AcmsEvent.stop(e);
            const self = this;
            const overlay = self.overlay;
            const ele = overlay.element;
            overlay.dismiss();
            ele.innerHTML = "";
        }

        /**
         * on Confirm delete
         *
         * @param {Event} e     Click Event
         * 
         * @memberof DeleteCommand
         * @private
         */
        _onConfirm(e) {
            AcmsEvent.stop(e);
            const self = this;
            const form = Selectors.closest(e.target, 'form[role="form"]');
            const fData = new FormData(form);
            require(['http/request', 'notifier'], Request => {
                Request.post(self.manager.remote, fData, self._onDeleted);
            });
        }

        /**
         * On Deleted Callback
         *
         * @param {Response} response   Remote Response
         * 
         * @memberof DeleteCommand
         * @private
         */
        _onDeleted(response) {
            if(response.status !== 'success') {
                return A.Notifications.createFromResponse(response);
            }
            const self = this;
            self.manager.token = response.token;
            self.manager.filetree.refresh();
            self.overlay.dismiss();
            self.overlay.element.innerHTML = "";
        }

        /**
         * find Items
         *
         * @returns {Object|Boolean}    Current selection or false if nothing selected
         * 
         * @memberof DeleteCommand
         * @private
         */
        _findItems() {
            const self = this,
                  flist = self.filelist;
            if(null === flist.currentSelection) {
                return false;
            }
            const curr = flist.currentSelection;
            const ret = {
                files: {},
                totalFiles: 0,
                folders: {},
                totalFolders: 0
            };
            for(let prop in curr.folder) {
                if(curr.folder.hasOwnProperty(prop)) {
                    let itm = curr.folder[prop];
                    let id = itm.acmsData('id');
                    let item = {
                        id: id,
                        title: itm.acmsData('title')
                    };
                    ret.folders[id] = item;
                    ret.totalFolders++;
                }
            }
            for(let prop in curr.file) {
                if(curr.file.hasOwnProperty(prop)) {
                    let itm = curr.file[prop];
                    let item = {
                        id: itm.acmsData('id'),
                        title: itm.acmsData('title')
                    };
                    ret.files[id] = item;
                    ret.totalFiles++;
                }
            }
            return ret;
        }

    }

    return DeleteCommand;
});
