/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("apps/profile/media-manager/commands/command", [
	"tools/class/class-tool",
	"tools/object/extend",
	"core/acms"
], (ClassTools, Extend, A) => {

	/**
	 * Base Command
	 *
	 * @class
	 * @abstract
	 * @author qm-b <https://bitbucket.org/qm-b/>
	 * 
	 * @property {MediaManager} manager Media Manager instance
	 * @property {HTMLElement} element The HTML element holding the command action.
	 * @property {MediaManagerFileTree} filetree File Tree instance
	 * @property {MediaManagerFileList} filelist File List instance
	 */
	class Command {

		/**
		 * Creates an instance of Command.
		 * 
		 * @param {MediaManager} 	MediaManager	Media Manager instance
		 * @param {String} 			name			Name of the Media Manager
		 * @param {String} 			label			Label
		 * @param {String} 			hint			Hint
		 * @memberof Command
		 */
		constructor(MediaManager, element, name, label, hint) {
			this._manager = MediaManager;
			this._filetree = MediaManager.filetree;
			this._filelist = MediaManager.filelist;
			this._element = element;
			this._name = name;
			this._label = label;
			this._hint = hint;
			this._prepare();
		}

		/**
		 * Element
		 *
		 * @readonly
		 * @memberof Command
		 */
		get element() {
			return this._element;
		}

		/**
		 * Holds the Media Manager
		 *
		 * @readonly
		 * @memberof Command
		 */
		get manager() {
			return this._manager;
		}

        get filetree() {
            return this._filetree;
        }

        get filelist() {
            return this._filelist;
        }

		/**
		 * Name of the Command
		 *
		 * @return {String}	Name of the Command
		 * 
		 * @memberof Command
		 */
		getCommand() {
			return this._name;
		}

		/**
		 * Label
		 *
		 * @returns {String}
		 * 
		 * @memberof Command
		 */
		getLabel() {
			return this._label;
		}

		/**
		 * Get Hint
		 *
		 * @returns {String}
		 * 
		 * @memberof Command
		 */
		getHint() {
			return this._hint;
		}

		/**
		 * Apply Command
		 * 
		 * Apply the command on user action. {@link Commands|Commands} listens
		 * on clicks of the user on the data-command element to prevent
		 * having multiple listeners from every command.
		 * 
		 * !!! note Note
		 * This Method will be called by {@link Commands|Commands} on 
		 * click of the command button trigger. It __MUST__ be overwritten 
		 * by every command.
		 * !!!
		 *
		 * @memberof Command
		 */
		apply() {
			throw new Error('apply needs to be implemented in your command ' + this.name);
		}

        /**
         * Gibt die Standard-Optionen eines Commands zurück
         *
         * @returns {Object}    Objekt mit den Stanard-Optionen des Command
         */
        getDefaultOptions() {
            let self = this;
            if ("defaults" in self.constructor) {
                return self.constructor.defaults;
            }
            return {};
        }


		/**
		 * Prepare instance
		 *
		 * @memberof Command
		 * @private
		 */
		_prepare() {
			const self = this;
			const manager = self.manager;
			const cmd = this.getCommand();
			const options = manager.options;
			self.remote = manager.remote;
            if(options.commands &&
                    typeof options.commands[cmd] !== 'undefined') {
				const custom = options.commands[cmd];
				self.options = Extend.flat({}, false, this.getDefaultOptions(), manager.options, custom);
			} else {
				self.options = Extend.flat({}, false, this.getDefaultOptions(), manager.options);
			}
			ClassTools.bindMethods(this, self);
		}

        /**
         * Ausführung bei fehlgeschlagener Abfrage
         * 
		 * Wird ausgeführt, wenn keine Antwort vom Server kommt bzw die 
         * Anfrage fehlschlägt
         * 
         * @param {Object}  xhr    		Das XHR Abfrage Object
         * @param {String}	textStatus 	Text Status Message
		 * 
		 * @memberof Command
		 * @private
         */
        _fail (xhr, textStatus) {
            A.Logger.writeLog(textStatus);
        }
        
        /**
         * Auführung bei jeder Abfrage
         * 
         * @param {String} textStatus Text Status
         * 
		 * @memberof Command
		 * @private
         */
        _always (textStatus) {
            A.Logger.writeLog(textStatus);
            this._processing = false;
        }
        
	}

	return Command;
});
