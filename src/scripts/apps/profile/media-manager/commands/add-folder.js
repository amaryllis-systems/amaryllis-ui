/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 
 define("apps/profile/media-manager/commands/add-folder", [
	 "apps/profile/media-manager/commands/command",
	 "core/selectors",
	 "events/event",
	 "core/acms"
 ], (Command, Selectors, AcmsEvent, A) => {
 
	/**
     * Add Folder Command
     *
     * @class
     * @extends {Command}
     * @author qm-b <https://bitbucket.org/qm-b/>
     * 
	 * @property {MediaManager} manager Media Manager instance
	 * @property {HTMLElement} element The HTML element holding the command action.
	 * @property {MediaManagerFileTree} filetree File Tree instance
	 * @property {MediaManagerFileList} filelist File List instance
     */
    class AddFolderCommand extends Command {

        /**
         * Standard Optionen
         * 
         * @readonly
         * 
         * @memberof AddFolderCommand
         */
        static get defaults() {
            return {
                itemID: 'folder-tree-item-',
                op: 'addFolder'
            };
        }

        /**
         * Creates an instance of AddFolderCommand.
         * 
         * @param {view/profile/media-manager/media-manager} MediaManager Der Medien Manager
         * 
         * @memberof AddFolderCommand
         */
        constructor(MediaManager, element) {
            super(MediaManager, element, 'folder-add', A.Translator._('Neues Verzeichnis'), A.Translator._('Erstellt ein neues Verzeichnis'));
        }

        /**
         * Ausführen des Commands
         * 
         * @param {Event} e Event
         * 
         * @memberof AddFolderCommand
         */
        apply(btn, e) {
            AcmsEvent.stop(e);
            let self = this, 
                o = self.options,
                tree = self.manager.filetree,
                remote = self.remote, fData = new FormData(), val = A.Translator._('Neuer Ordner');
            fData.append('foldername', val);
            fData.append('parent', tree.currentFolder || 0);
            fData.append('op', 'addFolder');
            fData.append('token', self.manager.token);
            require(['http/request', 'notifier'], function(Request) {
                Request.post(remote, fData, self._onCreate, self._fail, self._always);
            });
            
        }

        /**
         * On Blur Event Handler 
         * 
         * @param {Event} e Das Event
         * 
         * @memberof AddFolderCommand
         */
        _onBlur(e) {
            e.stopPropagation();
            e.preventDefault();
            this._processRequest();
            return false;

        }

        /**
         * On KeyDown Event Handler
         * 
         * @param {Event} e     Das Event
         * 
         * @memberof AddFolderCommand
         */
        _onKeyDown(e) {
            let code = e.which || e.keyCode;
            switch(code) {
                case 10:
                case 13:
                    AcmsEvent.stop(e);
                    this._processRequest();
                    return false;
                default:
                    break;
            }
        }

        _processRequest() {
            let self = this, 
                o = self.options,
                remote = self.remote, fData = new FormData(), 
                item = self.currentItem,
                input = Selectors.q('input[type=text]', item);
            fData.append('foldername', input.value.trim());
            fData.append('op', 'renameFolder');
            fData.append('folder_id', item.acmsData('id'));
            fData.append('token', self.manager.token);
            require(['http/request', 'notifier'], function(Request) {
                Request.post(remote, fData, self._onDone, self._fail, self._always);
            });
        }

        _onCreate(response) {
            if(response.code !== 200) {
                return A.Notifications.createFromResponse(response);
            }
            const self = this;
            const o = self.options;
            const list = self.manager.flist;
            const template = self.manager.options.templates.folders.form;
            const folder = response.folder;
            self.manager.token = response.token;
            let item, append;
            const templateEngine = self.manager.templateEngine,
                  data = {
                        folder: folder
                  }, 
                  callback = content => {
                        let sel = o.selectors.folder;
                        let item = list.querySelector(sel.folder + '[data-id="' + response.folder.id + '"]');
                        if(item) {
                            let input = item.querySelector('input[type="text"]');
                            AcmsEvent.add(input, 'keydown', self._onKeyDown);
                            AcmsEvent.add(item, 'blur', self._onBlur);
                            if('selectionStart' in input) {
                                input.selectionStart = 0;
                                input.selectionEnd = input.value.length;
                            }
                            input.focus();
                        }
                        self.currentItem = item;
                    };
            const folders = Selectors.qa('[data-role=folder]', list);
            if(folders.length) {
                item = folders[folders.length - 1];
                append = 'afterend';
            } else {
                item = list;
                append = "afterbegin";
            }

            templateEngine.compileResource(template, data, callback, item, append);
        }

        _onDone(response) {
            const self = this, 
                o = self.options;
            if(response.status === 'success') {
                self.manager.token = response.token;
                self.removeTextbox(response.folder);
                self.filetree.refresh();
            } else A.Notifications.createFromResponse(response);
        }

        removeTextbox(folder) {
            const self = this;
            let item = self.currentItem;
            if(!item) return;
            const label = item.querySelector('label'), 
                title = folder ? folder.title : label.innerHTML;
            label.innerHTML = title;
        }

    }

    return AddFolderCommand;
 });
