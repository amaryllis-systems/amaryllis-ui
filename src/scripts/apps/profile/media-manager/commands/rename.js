/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/profile/media-manager/commands/rename", [
    "apps/profile/media-manager/commands/command",
    "core/classes",
    "core/selectors",
    "events/event",
    "core/acms"
], (Command, Classes, Selectors, AcmsEvent, A) => {

    /**
     * Rename Command
     *
     * @class
     * @extends {Command}
     * @author qm-b <https://bitbucket.org/qm-b/>
     * 
	 * @property {MediaManager} manager Media Manager instance
	 * @property {HTMLElement} element The HTML element holding the command action.
	 * @property {MediaManagerFileTree} filetree File Tree instance
	 * @property {MediaManagerFileList} filelist File List instance
     */
    class RenameCommand extends Command {

		/**
		 * Standard Optionen
		 * 
		 * @readonly
		 * @static
		 * 
		 * @memberof UploadCommand
		 */
		static get defaults() {
			return {
			};
		}

		/**
		 * Creates an instance of UploadCommand.
		 * 
		 * @param {MediaManager} MediaManager Der Medien Manager
		 * 
		 * @memberof UploadCommand
		 */
		constructor(MediaManager, element) {
            super(MediaManager, element, 'delete', A.Translator._('Entfernen'), A.Translator._('Dateien und Ordner entfernen'));
            const self = this;
            MediaManager.on('filelist.keydown.F2', self._onPressF2);
        }

        /**
         * Is currently processing?
         *
         * @readonly
         * @memberof RenameCommand
         */
        get processing() {
            return this._processing;
        }

        /**
         * Apply Command
         *
         * @memberof RenameCommand
         */
        apply() {
            const self = this;
            if(this.processing) return;
            this._processing = true;
            let toRename = self._findFileListItem();
            if(false === toRename && self.filetree.currentSelection > 0) {
                let itm = self.manager.ftree.querySelector('[]')
                toRename = {isFolder: true, isFile: false, item: self.filetree.currentItem};
            }
            if(false === toRename) {
                this._processing = false;
                return;
            }
            
            const item = toRename.item;
            const isFile = toRename.isFile;
            const isFolder = toRename.isFolder;
            const label = Selectors.q('label', item);
            const input = document.createElement('input');
            input.type = "text";
            input.value = label.innerHTML.trim();
            label.innerHTML = "";
            label.appendChild(input);
            input.acmsData('original', input.value);
            if('selectionStart' in input) {
                input.selectionStart = 0;
                input.selectionEnd = input.value.length;
            }
            input.focus();

            const cb = (evt) => {
                const code = evt.keyCode || evt.which;
                if(code === 27) {
                    AcmsEvent.remove(input, 'keydown', cb);
                    label.innerHTML = "";
                    label.innerHTML = input.acmsData('original');
                    this._processing = false;
                    return;
                }
                if(code !== 13) {
                    return;
                }
                const val = evt.target.value;
                const fData = new FormData();
                fData.append('op', true === isFolder ? 'renameFolder' : 'renameFile');
                fData.append('folder_id', true === isFolder ? item.acmsData('id') : item.acmsData('folder'));
                if(true === isFolder) {
                    fData.append('foldername', val);
                } else {
                    fData.append('filename', val);
                    fData.append('media_id', item.acmsData('id'));
                }
                fData.append('token', self.manager.token);
                    
                const done = (response) => {
                    if(response.code !== 200) {
                        return A.Notifications.createFromResponse(response);
                    }
                    AcmsEvent.remove(input, 'keydown', cb);
                    if(response.folder) {
                        label.innerHTML = response.folder.title;
                    } else if (response.file) {
                        label.innerHTML = response.file.title;
                    }
                    self.manager.token = response.token;
                    self.filelist.sort();
                    this._processing = false;
                };
                
                require(['http/request', 'notifier'], Request => {
                    Request.post(self.manager.remote, fData, done, self._fail, self._always);
                });
            };
            
            AcmsEvent.add(input, 'keydown', cb);
        }
        
        /**
         * on Press F2 key
         *
         * @param {Event} e keyDown Event
         * @memberof RenameCommand
         */
        _onPressF2(e) {
            this.apply();
        }

        /**
         * Find item to rename from current selection of file list
         *
         * @returns {Object|Boolean} item data or `false`
         * 
         * @memberof RenameCommand
         * @private
         */
        _findFileListItem() {
            const self = this,
                  flist = self.filelist;
            let isFolder = false, isFile = false, item, id;
            if(null === flist.currentSelection) {
                return false;
            }
            const curr = flist.currentSelection;
            const folders = curr.folder;
            const files = curr.file;
            const fileIds = Object.keys(files);
            const folderIds = Object.keys(folders);
            if(fileIds.length === 1 && folderIds.length === 0) {
                id = fileIds[0];
                item = files[id];
                isFile = true;
            } else if(fileIds.length === 0 && folderIds.length === 1) {
                id = folderIds[0];
                item = folders[id];
                isFolder = true;
            } else {
                return false;
            }
            
            if(!item) {
                return false;
            }

            return {item: item, isFile: isFile, isFolder: isFolder};
        }
    }

    return RenameCommand;
});
