/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/profile/friendship/friend-search", [
    "core/base",
    "events/event",
    "core/acms"
], (Base, AcmsEvent, A) => {

    /**
     * Friend Search
     *
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class FriendSearch extends Base {

        /**
         * Remote Response after change has been processed by server
         * 
         * @typedef {Object} FriendSearch~Response
         * 
         * @property {('error'|'success')}  status      Text Status. can be 'success' or 'error'
         * @property {Number}               code        Might be `200` on success, error code otherwise
         * @property {String}               title       heading for response
         * @property {String}               message     Response message
         */
        
        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof FriendSearch
         */
        static get MODULE() {
            return "Friend Search";
        }

        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof FriendSearch
         */
        static get NS() {
            return "apps/profile/friendship/search";
        }

        /**
         * Default Options
         *
         * @readonly
         * @static
         * @memberof FriendSearch
         */
        static get DEFAULT_OPTIONS() {
            return {
                searchbox: '[data-role="friend-search"]',
                searchform: '[data-role="friend-quicksearch-form"]',
                friendsbox: '#friends-search-results',
                remote: null,
                minChars: 3,
                offset: 0,
                template: "templates/html/profile/friendship/friend-search.hbs"
            };
        }

        /**
         * Module Init
         *
         * @static
         * @param {HTMLElement}     element HTML Wrapper
         * @param {Object|null}     options custom Options
         * 
         * @returns {FriendSearch}  
         * @memberof FriendSearch
         */
        static init(element, options) {
            let fs = new FriendSearch(element, options);
            fs.element.acmsData(FriendSearch.NS, fs);

            return fs;
        }

        /**
         * Creates an instance of FriendSearch.
         * 
         * @param {HTMLElement}     element HTML Wrapper
         * @param {Object|null}     options custom Options
         * 
         * @memberof FriendSearch
         */
        constructor(element, options) {
            super(element, options);
            this.searchBox =
            this.offset =
            this.loaded = null;
            this._initialize();
        }

        /**
         * Process Search
         *
         * @memberof FriendSearch
         */
        search() {
            let self = this,
                o = self.options,
                input = self.searchForm.querySelector('input[type=search]'),
                val = input.value;
            if(val.length < o.minChars) {
                require(["tools/string/sprintf", "notifier"], () => {
                    let tr = A.Translator.t('Bitte mindestens %s Zeichen zur Suche eingeben');
                    let msg = Sprintf(tr, o.minChars);
                    let title = A.Translator.t('Zu wenig Zeichen');
                    A.Notifications.createNotification(msg, title, A.Notifications.TYPE_INFO);
                    return false;
                });
                return false;
            }
            let fData = new FormData();
            fData.append('term', val);
            fData.append('offset', self.offset);
            fData.append('op', 'quicksearch');
            require(['http/request', 'notifier'], (Request) => {
                Request.post(o.remote, fData, self._done);
            });
        }

        listen() {
            let self = this, 
                o = self.options, 
                form = self.searchForm;
            AcmsEvent.add(self.searchBox, 'keyup', self._onKeyUp);
            form.onsubmit = function (ev) {
                self.search();
                return false;
            };
        }

        /**
         * Parse Response to Templates
         *
         * @param {Object} response
         * @memberof FriendSearch
         */
        _done(response) {
            let self = this, 
                list = self.friends, 
                o = self.options,
                total = 0,
                members, member, data, i, cb = () => {total++;};
            list.innerHTML = '';
            if (response.status === 'success') {
                members = response.members;
                
                self.offset = (members.length > 0 ? (self.offset + members.length + 1) : self.offset);
                require(['templates/template-engine'], (TemplateEngine) => {
                    for (i = 0; i < members.length; i++) {
                        member = members[i];
                        data = {
                            friend: member
                        };
                        TemplateEngine.compileResource(o.template, data, cb, list);
                    }
                });
            } else {
                A.Notifications.createNotification(response.message, response.title, A.Notifications.TYPE_DANGER);
            }
        }

        _fail(xhr, text) {
            A.Notifications.createNotification(text, A.Translator.t('Fehler!'), A.Notifications.TYPE_DANGER);
        }

        _always(xhr, text, statusCode) {

        }

        /**
         * On Key Up Event Handler
         *
         * @param {Event} e Event
         * 
         * @memberof FriendSearch
         */
        _onKeyUp(e) {
            let self = this,
                o = self.options,
                code = e.keyCode || e.whitch,
                input = self.searchForm.querySelector('input[type=search]'),
                val = input.value;

            switch (code) {
                case 10:
                case 13: // enter
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                default:
                    if(val.length < o.minChars) {
                        return;
                    }
                    self.offset = 0;
                    self.friends.innerHTML = '';
                    self.search();
            }
        }

        /**
         * Prepare Module
         *
         * @memberof FriendSearch
         */
        _prepare() {
            let self = this,
                o = self.options,
                element = self.element;
            self.searchBox = element.querySelector(o.searchbox);

            self.searchForm = element.querySelector(o.searchform);
            self.friends = element.querySelector(o.friendsbox);
            self.offset = parseInt(o.offset, 10) || 0;
        }
    }

    return FriendSearch;
});
