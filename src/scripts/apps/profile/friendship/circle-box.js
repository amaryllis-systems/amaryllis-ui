/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 
define("apps/profile/friendship/circle-box", [
	"core/base",
	"core/selectors",
	"core/classes",
    "events/event",
	"templates/template-engine",
	"apps/ui/informations/modal",
    "core/acms"
], (Base, Selectors, Classes, AcmsEvent, TemplateEngine, Modal, A) => {
	
	
	/**
	 * Circle Box
	 * 
	 * @class
	 * @extends {Base}
	 * @author qm-b <https://bitbucket.org/qm-b/>
	 */
	class CircleBox extends Base {

		/**
		 * Module Name
		 *
		 * @readonly
		 * @static
		 * @memberof CircleBox
		 */
		static get MODULE() {
			return "Circle Box";
		}

		/**
		 * Module Namespace
		 *
		 * @readonly
		 * @static
		 * @memberof CircleBox
		 */
		static get NS() {
			return "acms.apps.profile.friendship.circle-box";
		}

		/**
		 * Open Event for Accordion Module
		 *
		 * @readonly
		 * @static
		 * @memberof CircleBox
		 */
		static get OPEN_ACCORDION() {
			return "acms.apps.ui.navs.accordion.open";
		}

		/**
		 * Default Options
		 *
		 * @readonly
		 * @static
		 * @memberof CircleBox
		 */
		static get DEFAULT_OPTIONS() {
			return {
				remote: null, // remote action url
				circle: '.accordion-section', // circle wrapper
				friend: 'templates/html/profile/friendship/friend.hbs',
				accordion: '.accordion'
			};
		}

		/**
		 * Module Init
		 *
		 * @static
		 * 
		 * @param {HTMLElement} element	Wrapper Element around Circles
		 * @param {Object|null} options	Custom Options
		 * 
		 * @returns {CircleBox}	Box instance
		 * @memberof CircleBox
		 */
		static init(element, options) {
			let cb = new CircleBox(element, options);
			cb.element.acmsData(CircleBox.NS, cb);

			return cb;
		}

		/**
		 * Creates an instance of CircleBox.
		 * 
		 * @param {HTMLElement} element	Wrapper Element around Circles
		 * @param {Object|null} options	Custom Options
		 * 
		 * @memberof CircleBox
		 */
		constructor(element, options) {
			super(element, options);
			this.$box =
			this._current = null;
			this._initialize();
		}

		listen() {
			let self = this,
				element = self.element,
				o = self.options,
                accordion = Selectors.matches(element, o.accordion) ? element : Selectors.q(o.accordion, element);
            AcmsEvent.on(accordion, CircleBox.OPEN_ACCORDION, o.circle, self._loadBox);
		}

		/**
		 * On Request Failed
		 *
		 * @param {Request} xhr
		 * @param {String} textStatus
		 * @memberof CircleBox
		 */
		_fail(xhr, textStatus) {
            Acms.Logger.logWarning(textStatus);
		}
		
		/**
		 * On Change Event Handler
		 *
		 * @param {Event} e	Change Event
		 * @memberof CircleBox
		 */
		_onChange(e) {
            let self = this,
                o = self.options,
                checkbx = e.target,
                checked = checkbx.checked,
                userid = checkbx.acmsData('userid'),
                value = parseInt(checkbx.value),
                fData = new FormData();
            fData.append('op', (checked ? 'addmember' : 'removemember'));
            fData.append('circle_id', value);
			fData.append('friend_id', userid);
			let done = (response) => {
				return self._onChangeSucceed(response, checkbx);
			};
            require(['http/request', 'notifier'], (Request) => {
				Request.post(o.remote, fData, self._onChangeSucceed, self._fail);
			});
		}
		
		/**
		 * On Change succeed
		 *
		 * @param {Object} 				response	Remote Response
		 * @param {HTMLInputElement} 	checkbx		Checkbox
		 * @memberof CircleBox
		 */
		_onChangeSucceed(response, checkbx) {
			let self = this, 
				o = self.options, 
				element = self.element;
			if(response.status !== 'success') {
				checkbx.checked = !checkbx.checked;
				AcmsEvent.fireChange(checkbx);
			} else {
				let section = Selectors.qa(o.circle+'[data-id='+value+']', element);
				if(section) {
					section.acmsData('loaded', false);
					let badge = Selectors.q('.badge', section);
					if(badge) {
						let total = parseInt(badge.textContent);
						if(checked) {
							total += 1;
						} else {
							total -= 1;
						}
						badge.textContent = total;
					}
				}
			}
		}

		_loadBox(e) {
			let o = this.options, self = this;
            let target = e.target, circle, id;
            if(target.acmsData('loaded') === true) {
                return false;
            }
            target.acmsData('loaded', true);
            target.setAttribute('data-loaded', true);
            circle = target.acmsData('circle');
            id = parseInt(target.acmsData('id'));
            this._current = target;
            let data = {
                op: (id > 0 ? 'loadmemberships' : 'loadnomembership'),
                id: id
            }, done = function(response) {
                if(response.status === 'success') {
                    self._received(response);
                }
			};
			require(['http/request', 'notifier'], (Request) => {
				Request.post(o.remote, data, done, self._fail);
			});
            return false;
		}

		_received(response) {
			if(response.status === 'success') {
				let members = response.members, self = this, o = self.options, i, data, total = 0;
				let list = this._current.querySelector('ul.collection'), cb = () => {
					total+=1;
					if(total === members.length) {
						self._refreshLinks(list);
					}
				};
				list.innerHTML = '';
				for(i = 0; i < members.length; i++) {
					let member = members[i], div = document.createElement('div');
					data = {friend: member, member: member};
					try {
						TemplateEngine.compileResource(o.friend, data, cb, list);
					} catch(E) {
						console.log(E);
					}
				}
			}
		}

		_refreshLinks(list) {
			let self = this, current = this._current;
            let links = Selectors.qa('li', list), i;
            for(i = 0; i < links.length; i++) {
                let link = links[i];
                self._registerClick(link);
            }
		}

		_registerClick(link) {
			let self = this, o = this.options;
            let target = link.querySelector('[data-toggle="circles"]');
            if(target) {
                let userid = target.getAttribute('data-id'),
                    circles = target.getAttribute('data-circles').split(','),
                    ev = AcmsEvent.create(Modal.EVENT_SHOW), cb = (event) => {
                        let modalbox = self.$modalbox, i,
                            chkbxes = Selectors.qa('input[type=checkbox]', modalbox);
                        for(i =0; i < chkbxes.length; i++) {
                            let chb = chkbxes[i], val = chb.value;
                            chb.acmsData('userid', userid);
                            if(circles.indexOf(val) === -1) {
                                chb.checked = false;
                            } else {
                                chb.checked = true;
                            }
                            AcmsEvent.add(chb, 'change', self._onChange);
                        }
                    };
                let mod = Modal.init(target ,{target: '#changecircle-membership'});
                
                AcmsEvent.add(target, Modal.EVENT_SHOW, cb);
            }
		}
        
		/**
		 * Prepare Module
		 *
		 * @memberof CircleBox
		 */
		_prepare() {
			let self = this,
                o = self.options,
                element = self.element;
            self.$box = element.querySelector('.circlebox');
            self.$sections = Selectors.qa(o.circle, element);
            self.$modalbox = document.querySelector('#changecircle-membership');
		}
	}
    
    return CircleBox;
});
