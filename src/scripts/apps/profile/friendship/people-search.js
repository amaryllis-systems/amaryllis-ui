/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/profile/friendship/people-search", [
    "core/base",
    "core/selectors",
    "events/event",
    "tools/string/string-tool",
    "core/acms"
], (Base, Selectors, AcmsEvent, StringTool, A) => {

    /**
     * People Search
     * 
     * Quick People Search in profile people section
     *
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class PeopleSearch extends Base {

        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof PeopleSearch
         */
        static get MODULE() {
            return "People Search";
        }

        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof PeopleSearch
         */
        static get NS() {
            return "apps/profile/friendship/search";
        }

        /**
         * Default Options
         * 
         * - `remote`: _[String]_ Remote Action Url
         * - `owner`: _[Number]_ ID of the profile owner
         *
         * @readonly
         * @static
         * @memberof PeopleSearch
         */
        static get DEFAULT_OPTIONS() {
            return {
                searchbox: '[data-role="people-search"]',
                searchform: '[data-role="people-quicksearch-form"]',
                friendsbox: '#people-search-results',
                trigger: '[data-trigger]',
                usercard: '[data-role="user-card"]',
                remote: null,
                minChars: 2,
                offset: 0,
                template: "templates/html/profile/friendship/people-search.hbs",
                onLoaded(total, self, members) {},
                owner: null
            };
        }

        /**
         * Module Init
         *
         * @static
         * @param {HTMLElement}     element HTML Wrapper
         * @param {Object|null}     options custom Options
         * 
         * @returns {PeopleSearch}  
         * @memberof PeopleSearch
         */
        static init(element, options) {
            let fs = new PeopleSearch(element, options);
            fs.element.acmsData(PeopleSearch.NS, fs);

            return fs;
        }

        /**
         * Creates an instance of PeopleSearch.
         * 
         * @param {HTMLElement}     element HTML Wrapper
         * @param {Object|null}     options custom Options
         * 
         * @memberof PeopleSearch
         */
        constructor(element, options) {
            super(element, options);
            this.searchBox =
            this.offset =
            this.loaded = null;
            this._initialize();
        }

        /**
         * Process Search
         *
         * @memberof PeopleSearch
         */
        search() {
            let self = this,
                o = self.options,
                input = self.searchForm.querySelector('input[type=search]'),
                val = input.value;
            if(val.length < o.minChars) {
                require(["tools/string/sprintf", "notifier"], () => {
                    let tr = A.Translator.t('Bitte mindestens %s Zeichen zur Suche eingeben');
                    let msg = Sprintf(tr, o.minChars);
                    let title = A.Translator.t('Zu wenig Zeichen');
                    A.Notifications.createNotification(msg, title, A.Notifications.TYPE_INFO);
                    return false;
                });
                return false;
            }
            let fData = new FormData();
            fData.append('term', val);
            fData.append('offset', self.offset);
            fData.append('op', 'quicksearch');
            require(['http/request', 'notifier'], (Request) => {
                Request.post(o.remote, fData, self._done);
            });
        }

        /**
         * Setup Event Listener
         * 
         * @memberof PeopleSearch
         */
        listen() {
            let self = this, 
                o = self.options, 
                form = self.searchForm;
            AcmsEvent.add(self.searchBox, 'keyup', self._onKeyUp);
            AcmsEvent.on(self.friends, 'click', o.trigger, self._onClickTrigger, true);
            form.onsubmit = function (ev) {
                self.search();
                return false;
            };
        }


        /**
         * On Click trigger callback
         *
         * @param {Event} e     Click Event
         * 
         * @memberof PeopleSearch
         */
        _onClickTrigger(e) {
            let self = this, o = self.options, target = e.target;
            e.preventDefault();
            e.stopPropagation();
            if(!Selectors.matches(target, o.trigger)) {
                target = Selectors.closest(target, o.trigger);
            }
            if(!target) {
                return false;
            }
            let trigger = target.acmsData('trigger'), fData = new FormData();
            fData.append('op', trigger);
            fData.append('friend', o.owner);
            fData.append('friend2', target.acmsData('friend2'));

            let done = (response) => {
                return self._onDoneRequest(response, target);
            };

            require(['http/request', 'notifier'], Request => {
                Request.post(self.remote, fData, done, self._fail, self._always);
            });
        }

        _onDoneRequest(response, trigger) {
            A.Notifications.createFromResponse(response);
            if(response.code !== 200) {
                return;
            }
            let self = this, o = self.options, card = Selectors.closest(trigger, o.usercard);
            if(card) {
                card.parentNode.removeChild(card);
            }
        }

        /**
         * Parse Response to Templates
         *
         * @param {Object} response
         * @memberof PeopleSearch
         */
        _done(response) {
            let self = this, 
                list = self.friends, 
                o = self.options,
                total = 0,
                members, member, data, i;
            list.innerHTML = '';
            if (response.status === 'success') {
                
                require(['templates/template-engine', 'http/module-loader'], (TemplateEngine, ModuleLoader) => {
                    members = response.members;
                    let cb = () => {
                        total++;
                        if(total === members.length) {
                            ModuleLoader.load(list);
                        }
                        if(typeof o.onLoaded === 'function') {
                            o.onLoaded(total, self, members);
                        }
                    };
                    
                    self.offset = (members.length > 0 ? (self.offset + members.length + 1) : self.offset);
                    for (i = 0; i < members.length; i++) {
                        member = members[i];
                        data = {
                            friend: member
                        };
                        TemplateEngine.compileResource(o.template, data, cb, list);
                    }
                });
            } else {
                A.Notifications.createNotification(response.message, response.title, A.Notifications.TYPE_DANGER);
            }
        }

        _fail(xhr, text) {
            A.Notifications.createNotification(text, A.Translator.t('Fehler!'), A.Notifications.TYPE_DANGER);
        }

        _always(xhr, text, statusCode) {

        }

        /**
         * On Key Up Event Handler
         *
         * @param {Event} e Event
         * 
         * @memberof PeopleSearch
         */
        _onKeyUp(e) {
            let self = this,
                o = self.options,
                code = e.keyCode || e.whitch,
                input = self.searchForm.querySelector('input[type=search]'),
                val = input.value;

            switch (code) {
                case 10:
                case 13: // enter
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                default:
                    if(val.length < o.minChars) {
                        return;
                    }
                    self.offset = 0;
                    self.friends.innerHTML = '';
                    self.search();
            }
        }

        /**
         * Prepare Module
         *
         * @memberof PeopleSearch
         */
        _prepare() {
            let self = this,
                o = self.options,
                element = self.element;
            self.searchBox = element.querySelector(o.searchbox);
            self.remote = o.remote;
            if(!StringTool.isUrl(self.remote)) {
                throw new TypeError('Remote url ist erforderlich');
            }
            self.searchForm = element.querySelector(o.searchform);
            self.friends = element.querySelector(o.friendsbox);
            self.offset = parseInt(o.offset, 10) || 0;
        }
    }

    return PeopleSearch;
});
