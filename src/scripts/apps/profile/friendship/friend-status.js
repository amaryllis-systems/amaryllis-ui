/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/profile/friendship/friend-status", [
	"core/base",
	"core/classes",
	"core/selectors",
	"events/event",
	"core/acms"
], (Base, Classes, Selectors, AcmsEvent, A) => {

	/**
	 * Friend Status Toggle
	 * 
	 * Class is responsible for dealing with friendship requests in people section of
	 * a profile owner. The class can
	 * 
	 * - accept a pending friendship
	 * - reject a friend request
	 * - delete a pending request to remove a friend from list.
	 *
	 * @class
	 * @extends {Base}
	 * @author qm-b <https://bitbucket.org/qm-b/>
	 * 
	 * @property {HTMLElement} element Wrapper element around friendship managing
	 * @property {ModuleOptions} options Options based on default and custom options
	 */
	class FriendStatus extends Base {

		   
        /**
         * Remote Response after change has been processed by server
         * 
         * @typedef {Object} FriendStatus~Response
         * 
         * @property {('error'|'success')}  status      Text Status. can be 'success' or 'error'
         * @property {Number}               code        Might be `200` on success, error code otherwise
         * @property {String}               title       heading for response
         * @property {String}               message     Response message
         */
        
        /**
         * Custom Callbacks to get notified on status updates
         * 
         * @callback FriendStatus~call
         * 
         * @param {Respone} response 	Remote response
         * @param {HTMLElement} ele 	Wrapper element of the instance
		 * @param {FriendStatus} self 	Current instance
         */

        /**
         * Default Module Options
         * 
         * @typedef {Object} FriendStatus~ModuleOptions
         * 
         * @property {number} 	friend 		Friend id of the user
         * @property {number} 	user		ID of the profile owner
		 * @property {String} 	remote		Remote URL for Server-Related actions
		 * @property {String} 	[accept="[data-toggle=accept]"] 	
		 * 									Selector for Accept toggle button to accept a friendship
		 * @property {String} 	[reject="[data-toggle=reject]"] 	
		 * 									Selector for reject toggle button to reject a friendship
		 * @property {String}	[delete="[data-toggle=delete]"] 	
		 * 									Selector for delete button to delete a friendship
		 * @property {Boolean} 	[remove=true] 
		 * 									Remove the action card after toggle status?
		 * @property {String}	[parent='.card'] 
		 * 									The parent of the action triggers (the action card)
		 * @property {call}		onAccept	On Accept Friendship Callback
		 * @property {call}		onDelete	On Delete Friendship Callback
		 * @property {call}		onReject	On Reject Friendship Callback
         */

		/**
		 * Module Name
		 * 
		 * @type String
		 *
		 * @readonly
		 * @static
		 * @memberof FriendStatus
		 */
		static get MODULE() {
			return "Profile Friend Status";
		}

		/**
		 * Module Namespace
		 * 
		 * @type {String}
		 *
		 * @readonly
		 * @static
		 * @memberof FriendStatus
		 */
		static get NS() {
			return "acms.apps.profile.friendship.friend-status";
		}

		/**
		 * Default Options
		 * 
		 * @type {ModuleOptions}
		 *
		 * @readonly
		 * @static
		 * @memberof FriendStatus
		 */
		static get DEFAULT_OPTIONS() {
			return {
				friend: null, // friend id
				user: null, // user id or null to use global Acms user
				remote: null, // remote url
				accept: "[data-toggle=accept]", // accept toggle
				reject: "[data-toggle=reject]",	// reject toggle
				delete: "[data-toggle=delete]",	// delete toggle
				remove: true,
				parent: '.card',
				onAccept: (response, ele, self) => {},
				onDelete: (response, ele, self) => {},
				onReject: (response, ele, self) => {}
			};
		}

		/**
		 * Module Init
		 *
		 * @static
		 * @param {HTMLElement} element Button Element
		 * @param {Object|null} options custom options
		 * 
		 * @returns {FriendStatus}
		 * @memberof FriendStatus
		 */
		static init(element, options) {
			let fs = new FriendStatus(element, options);
			fs.element.acmsData(FriendStatus.NS, fs);

			return fs;
		}

		/**
		 * Creates an instance of FriendStatus.
		 * 
		 * @param {HTMLElement} element Button Element
		 * @param {Object|null} options custom options.
		 * 
		 * @memberof FriendStatus
		 */
		constructor(element, options) {
			super(element, options);
			this._initialize();
		}

		listen() {
			let self = this,
				element = self.element,
				o = self.options;
			if (!o.remote) {
				return;
			}
			AcmsEvent.on(element, "click", o.accept, self._onClickAccept, true);
			AcmsEvent.on(element, "click", o.reject, self._onClickReject, true);
			AcmsEvent.on(element, "click", o.delete, self._onClickDelete, true);
		}

		/**
		 * On Button Click Event Handler
		 *
		 * @param {Event} e Click Event
		 * @memberof FriendStatus
		 * @private
		 */
		_onClickAccept(e) {
			e.preventDefault();
			let self = this,
				o = self.options,
				target = e.target;
			if(!Selectors.matches(target, o.accept)) {
				target = Selectors.closest(target, o.accept);
			}
			let cb = (response) => {
				self._doneAccept(response, target);
			};
			Classes.addClass(target, "loading");
			require(["http/request", "notifier"], (Request) => {
				let remote = o.remote,
					fData = new FormData();
				fData.append('friend', target.acmsData('friend'));
				fData.append('friend2', (o.owner || A.User.userID || false));
				fData.append('op', 'accept');
				Request.post(remote, fData, cb, self._fail, self._always);
			});
		}

		/**
		 * On Button Click Event Handler
		 *
		 * @param {Event} e Click Event
		 * @memberof FriendStatus
		 * @private
		 */
		_onClickReject(e) {
			e.preventDefault();
			let self = this,
				o = self.options,
				target = e.target;
			if(!Selectors.matches(target, o.accept)) {
				target = Selectors.closest(target, o.accept);
			}
			let cb = (response) => {
				self._doneReject(response, target);
			};
			Classes.addClass(target, "loading");
			require(["http/request", "notifier"], (Request) => {
				let remote = o.remote,
					fData = new FormData();
				fData.append('friend2', target.acmsData('friend'));
				fData.append('friend', (o.owner || A.User.userID || false));
				fData.append('op', 'reject');
				Request.post(remote, fData, cb, self._fail, self._always);
			});
		}
		
		/**
		 * On Button Click Event Handler
		 *
		 * @param {Event} e Click Event
		 * 
		 * @memberof FriendStatus
		 * @private
		 */
		_onClickDelete(e) {
			AcmsEvent.stop(e);
			let self = this,
				o = self.options,
				target = e.target;
			if(!Selectors.matches(o.accept)) {
				target = Selectors.closest(target, o.accept);
			}
			let cb = (response) => {
				self._doneDelete(response, target);
			};
			Classes.addClass(target, "loading");
			require(["http/request", "notifier"], function (Request) {
				let remote = o.remote,
					fData = new FormData();
				fData.append("friend2", o.friend);
				fData.append("friend", o.owner || A.User.userID || false);
				fData.append("op", "delete");
				Request.post(remote, fData, cb, self._fail, self._always);
			});
		}


		/**
		 * On Remote Response Handler
		 *
		 * @param {Response} 	response 	Remote Response 
		 * @param {HTMLElement}	trigger		Button Trigger
		 * 
		 * @memberof FriendStatus
		 * @private
		 */
		_doneAccept(response, trigger) {
			let self = this,
				o = self.options;
			A.Notifications.createFromResponse(response);
			Classes.toggleClass(trigger, "loading");
			if (response.status === "success" &&
				typeof o.onAccept === "function") {
				o.onAccept(response, trigger, self);
				if (o.remove && o.parent) {
                    parent = Selectors.closest(trigger, o.parent);
                    if(parent && parent.parentNode) parent.parentNode.removeChild(parent);
                }
			}
		}

		/**
		 * On Remote Response Handler
		 *
		 * @param {Response} 	response 	Remote Response
		 * @param {HTMLElement}	trigger		Button Trigger
		 * 
		 * @memberof FriendStatus
		 * @private
		 */
		_doneReject(response, trigger) {
			let self = this,
				o = self.options;
			A.Notifications.createFromResponse(response);
			Classes.toggleClass(trigger, "loading");
			if (response.status === "success" &&
				typeof o.onReject === "function") {
				o.onReject(response, trigger, self);
				if (o.remove && o.parent) {
                    parent = Selectors.closest(trigger, o.parent);
                    if(parent && parent.parentNode) parent.parentNode.removeChild(parent);
                }
			}
		}

		/**
		 * On Remote Response Handler
		 *
		 * @param {Response} 	response 	Response object from remote
		 * @param {HTMLElement}	trigger		Button Trigger
		 * 
		 * @memberof FriendStatus
		 * @private
		 */
		_doneDelete(response, trigger) {
			let self = this,
				o = self.options;
			A.Notifications.createFromResponse(response);
			Classes.toggleClass(trigger, "loading");
			if (response.status === "success" &&
				typeof o.onDelete === "function") {
				o.onDelete(response, trigger, self);
				if (o.remove && o.parent) {
                    parent = Selectors.closest(trigger, o.parent);
                    if(parent && parent.parentNode) parent.parentNode.removeChild(parent);
                }
			}
		}

		/**
		 * On Remote Fail Response Handler
		 *
		 * @param {Request} xhr xhr Request Object
		 * @param {String} textStatus   Respone Text Status
		 * 
		 * @memberof FriendStatus
		 * @private
		 */
		_fail(xhr, textStatus) {
			A.Logger.logWarning(textStatus);
		}

		/**
		 * On Remote Response Handler
		 *
		 * @param {Request} xhr xhr Request Object
		 * @param {String} textStatus   Respone Text Status
		 * 
		 * @memberof FriendStatus
		 * @private
		 */
		_always(xhr, textStatus) {
			A.Logger.logInfo(textStatus);
		}

	}

	return FriendStatus;
});
