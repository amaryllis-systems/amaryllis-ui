/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/profile/friendship/friend-request", [
	"core/base",
	"core/classes",
	"core/selectors",
	"events/event",
	"core/acms"
], (Base, Classes, Selectors, AcmsEvent, A) => {

	/**
     * Friend Request
	 * 
	 * Class is responsible for sending the friend-request in a 
	 * user profile, if the visitor clicks the 'add to friends'
	 * button.
     *
     * @class
     * @extends {Base}
	 * @author qm-b <https://bitbucket.org/qm-b/>
     */
	class FriendRequest extends Base {
		   
        /**
         * Remote Response after change has been processed by server
         * 
         * @typedef {Object} FriendRequest~Response
         * 
         * @property {('error'|'success')}  status      Text Status. can be 'success' or 'error'
         * @property {Number}               code        Might be `200` on success, error code otherwise
         * @property {String}               title       heading for response
         * @property {String}               message     Response message
         */
        
        /**
         * Custom Callbacks to get notified on status updates
         * 
         * @callback FriendRequest~call
         * 
         * @param {Respone} 		response 	Remote response
         * @param {HTMLElement} 	ele 		Wrapper element of the instance
		 * @param {FriendRequest} 	self 		Current instance
         */

        /**
         * Default Module Options
         * 
         * @typedef {Object} FriendRequest~ModuleOptions
         * 
         * @property {number} 	friend 		Friend id of the user
         * @property {number} 	user		ID of the profile owner
		 * @property {String} 	remote		Remote URL for Server-Related actions
		 * @property {call}		onRequested	On Request Friendship Callback. 
		 * 									Callback will be triggered after remote reponse
         */

		/**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof FriendRequest
         */
		static get MODULE() {
			return "Profile Friend Request";
		}

		/**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof FriendRequest
         */
		static get NS() {
			return "acms.apps.profile.friendship.friend-request";
		}

		/**
         * Default Options
		 * 
		 * @type {ModuleOptions}
         *
         * @readonly
         * @static
         * @memberof FriendRequest
         */
		static get DEFAULT_OPTIONS() {
			return {
				friend: null, // friend id
				user: null, // user id or null to use global Acms user
				remote: null,
				/*eslint no-unused-vars: ["error", { "args": "none" }]*/
				onRequested: (response, ele, self) => {
				}
			};
		}

		/**
         * Module Init
         *
         * @static
         * @param {HTMLElement} element Button Element
         * @param {Object|null} options custom options
         * 
         * @returns {FriendRequest} New instance
		 * 
         * @memberof FriendRequest
         */
		static init(element, options) {
			let fs = new FriendRequest(element, options);
			fs.element.acmsData(FriendRequest.NS, fs);

			return fs;
		}

		/**
         * Creates an instance of FriendRequest.
         * 
         * @param {HTMLElement} element Button Element
         * @param {Object|null} options custom options
         * 
         * @memberof FriendRequest
         */
		constructor(element, options) {
			super(element, options);

			this._initialize();
		}

		/**
		 * Setup Event listener
		 *
		 * @memberof FriendRequest
		 * @private
		 */
		listen() {
			let self = this,
				element = self.element,
				o = self.options;
			if (!o.remote) {
				return;
			}
			AcmsEvent.add(element, "click", self._onClick);
		}

		/**
         * On Button Click Event Handler
         *
         * @param {Event} e Click Event
         * @memberof FriendRequest
		 * @private
         */
		_onClick(e) {
			e.preventDefault();
			let self = this,
				o = self.options,
				element = self.element;
			Classes.addClass(element, "loading");
			require(["http/request", "notifier"], function (Request) {
				let remote = o.remote,
					fData = new FormData();
				fData.append("friend2", o.friend);
				fData.append("friend", o.userid || A.User.userID || false);
				fData.append("op", "request");
				Request.post(remote, fData, self._done, self._fail, self._always);
			});
		}

		/**
         * On Remote Response Handler
         *
         * @param {Object} response Response
         * @memberof FriendRequest
		 * @private
         */
		_done(response) {
			let self = this,
				o = self.options;
			A.Notifications.createFromResponse(response);
			if (response.status === "success" && 
					typeof o.onRequested === "function") {
				o.onRequested(response, self.element, self);
			}
		}

		/**
         * On Remote Fail Response Handler
         *
         * @param {Request} xhr xhr Request Object
         * @param {String} textStatus   Respone Text Status
         * 
         * @memberof FriendRequest
		 * @private
         */
		_fail(xhr, textStatus) {
			A.Logger.logWarning(textStatus);
		}

		/**
         * On Remote Response Handler
         *
         * @param {Request} xhr xhr Request Object
         * @param {String} textStatus   Respone Text Status
         * 
         * @memberof FriendRequest
		 * @private
         */
		_always(xhr, textStatus) {
			A.Logger.logWarning(textStatus);
			Classes.toggleClass(this.element, "loading");
		}

	}

	return FriendRequest;
});
