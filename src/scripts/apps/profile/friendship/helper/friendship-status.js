/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/profile/friendship/helper/friendship-status", [], () => {

	/**
	 * Simple Wrapper to share the same status informations across multiple modules
	 *
	 * @class
	 * @author qm-b <https://bitbucket.org/qm-b/>
	 */
	class FriendshipStatus {

		/**
		 * Friendship Status "accepted"
		 *
		 * @readonly
		 * @static
		 * @memberof FriendshipStatus
		 */
		static get STATUS_ACCEPTED() {
			return 3;
		}

		/**
		 * Friendship Status "pending"
		 *
		 * @readonly
		 * @static
		 * @memberof FriendshipStatus
		 */
		static get STATUS_PENDING() {
			return 2;
		}

		/**
		 * Friendship Status "rejected"
		 *
		 * @readonly
		 * @static
		 * @memberof FriendshipStatus
		 */
		static get STATUS_REJECTED() {
			return 1;
		}

	}

	return FriendshipStatus;
});
