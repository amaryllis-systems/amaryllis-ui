/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/profile/avatar/avatar", [
    "core/base",
    "core/classes",
    "core/selectors",
    "templates/template-engine",
    "tools/string/string-tool",
    "events/event",
    "core/acms"
], (Base, Classes, Selectors, TemplateEngine, StringTool, AcmsEvent, A) => {

    /**
     * AvatarDashboard
     * 
     * Helper Module for avatar dashboard in profile
     *
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     * 
     * @property {AvatarDashboard.DEFAULT_OPTIONS} options Custom options
     * @property {HTMLElement} element Wrapper Element
     */
    class AvatarDashboard extends Base {

        /**
         * Custom Callbacks to get notified on updates
         * 
         * @callback AvatarDashboard~call
         * 
         * @param {Respone}           response 	Remote response
         * @param {Respone}           response 	Remote response
		 * @param {AvatarDashboard}   self 	    Current instance
         */

        /**
         * Default Module Options
         * 
         * @typedef {Object} AvatarDashboard~ModuleOptions
         * 
		 * @property {String} 	remote		Remote URL for Server-Related actions
         * @property {Object}   selectors   
         * @property {String}   [selectors.list=[data-role="avatar-list"]]
         *                                  Selector to get the list of custom uploaded avatars
         * @property {String}   [selectors.avatar=[data-role="avatar"]]
         *                                  Selector to get a single avatars from list
         * @property {String}   [selectors.trigger=[data-trigger]]
         *                                  Selector to get a trigger forcing a remote action
         * @property {String}   [selectors.toggle=[data-toggle]]
         *                                  Selector to get the "toggle active state" button
		 * @property {call}		afterDelete	Callback will be triggered after deleting an avatar
         * @property {call}		afterUpload	Callback will be triggered after uploading an avatar
         */

        /**
         * Module Namespace
         * 
         * @type {String}
         *
         * @readonly
         * @static
         * @memberof AvatarDashboard
         */
        static get NS() {
            return "acms.apps.profile.avatar.avatar";
        }

        /**
         * Module Name
         *
         * @type {String}
         *
         * @readonly
         * @static
         * @memberof AvatarDashboard
         */
        static get MODULE() {
            return "Avatar Dashboard";
        }

        /**
         * Default Options
         * 
         * @type {ModuleOptions}
         *
         * @readonly
         * @static
         * @memberof AvatarDashboard
         */
        static get DEFAULT_OPTIONS() {
            return {
                remote: null,
                selectors: {
                    list: '[data-role="avatar-list"]',
                    avatar: '[data-role="avatar"]',
                    iavatar: '[data-role="iavatar"]',
                    trigger: '[data-trigger]',
                    toggle: '[data-toggle]'
                },
                classNames: {
                    active: 'text-success',
                    inactive: 'text-muted text-success-hover'
                },
                templates: {
                    avatar: 'templates/html/profile/avatar/avatar.hbs'
                },
                afterDelete: (response, avatar, self) => {},
                afterUpload: (response, avatar, self) => {}
            };
        }

        /**
         * Module init
         *
         * @static
         * 
         * @param {HTMLElement} element Wrapper around the dashboard activities
         * @param {Object}      options Custom options
         * 
         * @returns {AvatarDashboard} New instance
         * 
         * @memberof AvatarDashboard
         */
        static init(element, options) {
            let m = new AvatarDashboard(element, options);
            m.element.acmsData(AvatarDashboard.NS, m);
            
            return m;
        }


        /**
         * Creates an instance of AvatarDashboard.
         * 
         * @param {HTMLElement} element Wrapper around the dashboard activities
         * @param {Object}      options Custom options
         * 
         * @memberof AvatarDashboard
         */
        constructor(element, options) {
            super(element, options);
            this._initialize();

        }

        /**
         * Setup event listener
         *
         * @memberof AvatarDashboard
         * @private
         */
        listen() {
            let self = this, o = self.options, sel = o.selectors, element = self.element;
            AcmsEvent.on(element, 'click', sel.trigger, self._onClickTrigger, true);
            AcmsEvent.on(element, 'click', sel.toggle, self._onClickToggle, true);
            if(self.fileupload) {
                AcmsEvent.add(self.fileupload, 'change', self._onChange);
            }
        }

        /**
         * On Uploaded
         * 
         * @param {Response} response Remote Response
         *  
         * @memberof AvatarDashboard
         * @private
         */
        _onUploaded(response) {
            if(response.status !== 'success') {
                return A.Notifications.createFromResponse(response);
            }
            let self = this, 
                o = self.options,
                av = response.avatar,
                tdata = {avatar: av, user: A.User},
                list = Selectors.q(o.selectors.list, self.element),
                cb = () => {
                    let ava = Selectors.q('[data-id='+av.id+']', list);
                    if(ava && typeof o.afterUpload === 'function')
                        o.afterUpload(response, ava, self);
                    self._deactivateAllExcept(av.id);
                    self._deactivateIAvatar();
                };
            TemplateEngine.compileResource(o.templates.avatar, tdata, cb, list, 'beforeend');

        }

        /**
         * On change
         *
         * @param {Event} e event
         * 
         * @memberof AvatarDashboard
         * @private
         */
        _onChange(e) {
            let self = this, files = [], input = e.target, f;
            require(['http/request', 'notifier'], AcmsHttpRequest => {
                for (let x = 0; x < input.files.length; x++) {
                    f = input.files[x];
                    files.push(f);
                    let fData = new FormData();
                    fData.append('avatar', f);
                    fData.append('op', 'upload');
                    Request.post(self.remote, fData, self._onUploaded, self._fail, self._always);
                }
                
            });
        }

        /**
         * On Click trigger
         *
         * @param {Event} e Click event
         * 
         * @memberof AvatarDashboard
         * @private
         */
        _onClickTrigger(e) {
            if(this._processing) return false;
            this._processing = true;
            let self = this, o = self.options, sel = o.selectors, target = e.target, av;
            if(!Selectors.matches(target, sel.trigger)) {
                target = Selectors.closest(target, sel.trigger);
            }
            if(!target) {
                return;
            }
            let trigger = target.acmsData('trigger');
            let done = '_onDone' + StringTool.ucfirst(trigger);
            let cb = (response) => {
                return self[done](response, target, av);
            };
            av = Selectors.closest(target, '[data-role="avatar"]');
            if(!av) return;
            let fData = new FormData();
            fData.append('op', trigger);
            fData.append('id', av.acmsData('id'));
            require(['http/request', 'notifier'], (Request) => {
                Request.post(o.remote, fData, cb, self._fail, self._always);
            });
        }

        /**
         * On Click toggle
         *
         * @param {Event} e Click event
         * 
         * @memberof AvatarDashboard
         * @private
         */
        _onClickToggle(e) {
            if(this._processing) return false;
            this._processing = true;
            let self = this, o = self.options, sel = o.selectors, target = e.target, av;
            if(!Selectors.matches(target, sel.toggle)) {
                target = Selectors.closest(target, sel.toggle);
            }
            if(!target) {
                return;
            }
            let state = target.acmsData('active');
            let done = '_onDone' + (state == true ? 'Deactivate' : 'Activate');
            console.log(done);
            console.log(self[done]);
            av = Selectors.closest(target, '[data-role="avatar"]');
            let cb = (response) => {
                return self[done](response, target, av);
            };
            if(!av) return;
            let fData = new FormData();
            fData.append('op', state == true ? 'deactivate' : 'activate');
            fData.append('id', av.acmsData('id'));
            require(['http/request', 'notifier'], (Request) => {
                Request.post(o.remote, fData, cb, self._fail, self._always);
            });
        }

        /**
         * On Done Delete
         *
         * @param {Object}      response    Remote response object
         * @param {HTMLElement} trigger     Button trigger
         * @param {HTMLElement} av          Avatar list item
         * 
         * @memberof AvatarDashboard
         * @private
         */
        _onDoneDelete(response, trigger, av) {
            let self = this, o = self.options;
            A.Notifications.createFromResponse(response);
            if(response.code !== 200) {
                return;
            }
            if(typeof o.afterDelete === 'function') {
                o.afterDelete(response, av, self);
            }
            av.parentNode.removeChild(av);
        }

        /**
         * On Done Activate
         *
         * @param {Response}      response    Remote response object
         * @param {HTMLElement} trigger     Button trigger
         * @param {HTMLElement} av          Avatar list item
         * 
         * @memberof AvatarDashboard
         * @private
         */
        _onDoneActivate(response, trigger, av) {
            let self = this, o = self.options;
            A.Notifications.createFromResponse(response);
            if(response.code !== 200) {
                return;
            }
            trigger.acmsData('active', true);
            Classes.addClass(trigger, o.classNames.active);
            Classes.removeClass(trigger, o.classNames.inactive);
            self._deactivateAllExcept(av.acmsData('id'));
            self._deactivateIAvatar();
        }

        /**
         * On Done Activate
         *
         * @param {Response}      response    Remote response object
         * @param {HTMLElement} trigger     Button trigger
         * @param {HTMLElement} av          Avatar list item
         * 
         * @memberof AvatarDashboard
         * @private
         */
        _onDoneDeactivate(response, trigger, av) {
            let self = this, o = self.options;
            A.Notifications.createFromResponse(response);
            if(response.code !== 200) {
                return;
            }
            trigger.acmsData('active', false);
            Classes.addClass(trigger, o.classNames.inactive);
            Classes.removeClass(trigger, o.classNames.active);
            self._activateIAvatar();
        }

        /**
         * Deactivate all except id
         *
         * @param {Number} id id to ignore
         * @memberof AvatarDashboard
         * @private
         */
        _deactivateAllExcept(id) {
            let self = this, o = self.options, sel = o.selectors, list = Selectors.q(sel.list, self.element);
            Selectors.qa(sel.avatar, list).forEach(item => {
                if(item.acmsData('id') != id) {
                    let icn = Selectors.q('[data-toggle="activate"]', item);
                    if(icn) {
                        trigger.acmsData('active', false);
                        Classes.addClass(trigger, o.classNames.active);
                        Classes.removeClass(trigger, o.classNames.inactive);
                    }
                }
            });
        }

        /**
         * Activate iavatar
         *
         * @memberof AvatarDashboard
         * @private
         */
        _activateIAvatar() {
            let self = this, o = self.options, sel = o.selectors, cls = o.classNames;
            let iavatar = Selectors.q(sel.iavatar, self.element);
            if(!iavatar) return;
            let def = Selectors.q('[data-role="default"]', iavatar);
            if(def) {
                Classes.addClass(def, cls.active);
                Classes.removeClass(def, cls.inactive);
            }
        }

        /**
         * Deactivate iavatar
         *
         * @memberof AvatarDashboard
         * @private
         */
        _deactivateIAvatar() {
            let self = this, o = self.options, sel = o.selectors, cls = o.classNames;
            let iavatar = Selectors.q(sel.iavatar, self.element);
            if(!iavatar) return;
            let def = Selectors.q('[data-role="default"]', iavatar);
            if(def) {
                Classes.addClass(def, cls.inactive);
                Classes.removeClass(def, cls.active);
            }
        }

		/**
         * On Fail
         *
         * @param {XMLHttpRequest} xhr
         * @param {String} textStatus
         * @memberof AvatarDashboard
         * @private
         */
        _fail(xhr, textStatus) {
            A.Logger.logWarning(textStatus);
		}
		
		/**
         * On Always
         *
         * @param {XMLHttpRequest} xhr
         * @param {String} textStatus
         * @memberof AvatarDashboard
         * @private
         */
        _always(xhr, textStatus) {
			A.Logger.logInfo(textStatus);
            this._processing = false;
		}
        
        /**
         * Prepare Module
         *
         * @memberof AvatarDashboard
         * @private
         */
        _prepare() {
            let self = this, o = self.options, element = self.element, sel = o.selectors;
            self.fileupload = Selectors.q('[data-role="avatar-upload"]', element);
            if(false === StringTool.isUrl(o.remote)) {
                throw new Error('Invald remote url');
            }
            self.remote = o.remote;
            let list = Selectors.q(sel.list, element);
            if(!list) return;
            let def = Selectors.qa(sel.avatar, list);
            let active = null;
            def.forEach(item => {
                let act = Selectors.q('[data-active]', item);
                if(act && act.acmsData('active')) {
                    active = act;
                }
            });
            if(active === null) self._activateIAvatar();
        }

    }

    return AvatarDashboard;
});
