/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/profile/avatar/avatar-form", [
    "core/base",
    "core/classes",
    "core/selectors",
    "events/event",
    "core/acms"
], (Base, Classes, Selectors, AcmsEvent, A) => {

    /**
     * Avatar Form
     * 
     * Simple module to handle default avatar form
     *
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class AvatarForm extends Base {

        /**
         * Remote Response after change has been processed by server
         * 
         * @typedef {Object} AvatarForm~ServerResponse
         * 
         * @property {('error'|'success')}  status          Text Status. can be 'success' or 'error'
         * @property {Number}               code            Might be `200` on success, error code otherwise
         * @property {String}               title           heading for response
         * @property {String}               message         Response message
         * @property {Object}               avatar          New Avatar
         * @property {String}               avatar.raw      base64 representation of the avatar
         * @property {String}               avatar.thumb    URL of the avatar image
         * @property {String}               avatar.avatar   Filename of the created avatar
         * @property {Boolean}              avatar.active   `true` if it is the current avatar
         * @property {Number}               avatar.id       ID of the avatar in database
         * @property {Number}               avatar.owner    ID of the avatar owner
         */
        
        /**
         * Custom Callbacks to get notified on updates
         * 
         * @callback AvatarForm~call
         * 
         * @param {ServerRespone}     response 	Remote response
		 * @param {AvatarForm}        self 	    Current instance
         */

        /**
         * Default Module Options
         * 
         * @typedef {Object} AvatarForm~ModuleOptions
         * 
		 * @property {String} 	remote		Remote URL for Server-Related actions
		 * @property {call}		onResponse	Callback will be triggered after remote reponse
         */

        /**
         * Module Namespace
         * 
         * @type {String}
         *
         * @readonly
         * @static
         * @memberof AvatarForm
         */
        static get NS() {
            return "acms.apps.profile.avatar.avatar-form";
        }


        /**
         * Module Namespace
         * 
         * @type {String}
         *
         * @readonly
         * @static
         * @memberof AvatarForm
         */
        static get MODULE() {
            return "Avatar Form";
        }

        /**
         * Default Options
         * 
         * @type {ModuleOptions}
         * 
         * @readonly
         * @static
         * @memberof AvatarForm
         */
        static get DEFAULT_OPTIONS() {
            return {
                remote: null,
                onResponse: (response, self) => {}
            };
        }

        /**
         * Module init
         *
         * @static
         * 
         * @param {HTMLFormElement} element Form Element
         * @param {Object|null}     options Optional custom options
         * 
         * @returns {AvatarForm}    New instance
         * 
         * @memberof AvatarForm
         */
        static init(element, options) {
            let m = new AvatarForm(element, options);
            m.element.acmsData(AvatarForm.NS, m);
            
            return m;
        }

        /**
         * Creates an instance of AvatarForm.
         * 
         * @param {HTMLFormElement} element     Form Element
         * @param {Object}          options     Optional custom options
         * 
         * @memberof AvatarForm
         */
        constructor(element, options) {
            super(element, options);
            this._initialize();

        }

        /**
         * Setup event listener
         *
         * @memberof AvatarForm
         * @private
         */
        listen() {
            let self = this, o = this.options, element = self.element;
            AcmsEvent.on(element, 'click', 'button[type="submit"]', self._onClickSubmit, true);
        }

        /**
         * On Click Submit Form
         *
         * @param {Event} e Click Event
         * @memberof AvatarForm
         * @private
         */
        _onClickSubmit(e) {
            e.preventDefault();
            e.stopPropagation();
            let self = this, o = self.options, form = Selectors.closest(e.target, '[role="form"]');
            let fData = new FormData(form);
            require(['http/request', 'apps/ui/informations/overlay', 'notifier'], (Request, Overlay) => {
                self.overlay = Overlay.init(document.body, o.overlayOptions || {});
                self.overlay.append();
                Request.post(self.remote, fData, self._onDone, self._fail, self._always);
            });
        }

        /**
         * On Done
         *
         * @param {ServerResponse} response  Remote response object
         * @memberof AvatarForm
         * @private
         */
        _onDone(response) {
            let self = this, o = self.options;
            self.element.reset();
            A.Notifications.createFromResponse(response);
            if(response.code !== 200) {
                return;
            }
            if(self.modal) {
                let dismiss = Selectors.q('[data-dismiss="modal"]', self.modal);
                if(dismiss) AcmsEvent.fireClick(dismiss);
            }
            if(typeof o.onResponse === 'function') {
                o.onResponse(response, self);
            }
        }

		/**
         * On Fail
         *
         * @param {XMLHttpRequest|ActiveXObject} xhr
         * @param {String} textStatus
         * @memberof AvatarForm
         * @private
         */
        _fail(xhr, textStatus) {
            A.Logger.logWarning(textStatus);
		}
		
		/**
         * On Always
         *
         * @param {XMLHttpRequest|ActiveXObject} xhr
         * @param {String} textStatus
         * @memberof AvatarForm
         * @private
         */
        _always(xhr, textStatus) {
			A.Logger.logInfo(textStatus);
            this._processing = false;
            this.overlay.remove();
		}
        
        /**
         * Prepare Form
         *
         * @memberof AvatarForm
         * @private
         */
        _prepare() {
            let self = this, o = self.options, element = self.element;
            self.modal = Selectors.closest(element, '.modal');
            self.remote = o.remote || element.getAttribute('action');
        }

    }

    return AvatarForm;
});
