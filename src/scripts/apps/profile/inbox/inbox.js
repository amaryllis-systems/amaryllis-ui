/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/profile/inbox/inbox", [
    "core/base",
    "core/classes",
    "core/selectors",
    "events/event",
    "tools/string/string-tool",
    "templates/template-engine",
    "tools/string/escaper",
    "core/acms"
], (Base, Classes, Selectors, AcmsEvent, StringTool, TemplateEngine, Escaper, A) => {

    const isearch = 'ui/components/inputsearch';

    /**
     * Inbox
     * 
     * Handling of profile inbox
     *
     * @author qm-b <https://bitbucket.org/qm-b/>
     * @class
     * @extends {Base}
     * 
     * @property {Boolean} processing   `true` if any remote process is in work
     */
    class Inbox extends Base 
    {

        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof Inbox
         */
        static get NS() {
            return "acms.apps.profile.inbox.inbox";
        }

        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof Inbox
         */
        static get MODULE() {
            return "Profile Inbox";
        }

        /**
         * Default options
         * 
         * - `remote`: _[String]_ Remote Url
         * - `view`: _[String]_ Current View provided by `$acmsContents['subview']`
         * - `templates`: _[Object]_ Object holding templates to use
         *  - `templates.inbox`: Inbox Template
         * - `selectors`: _[Object]_ Selectors setup
         *  - `selectors.pm`: _[String]_ Private Message Selector to find a parent private message of an (e.g. trigger) element
         *  - `selectors.trigger`: _[String]_ PM Selector for several triggers like trash, starring ...
         *  - `selectors.replyForm`: _[String]_ Selector to find the reply form in private message view
         *  - `selectors.replyButton`: _[String]_ Selector to find the submit button of reply from in private message view
         * - `classNames`: _[Object]_ Class Names Setup
         *      - `classNames.icons`: _[Object]_ Icons Class Names Setup for some Private Message icons
         *          - `classNames.icons.important`: _[String]_ Class Name for important pm icon
         *          - `classNames.icons.unimportant`: _[String]_ Class Name for unimportant pm icon
         * - `beforeSendReply`: _[CallableFunction]_ Callback will be called before sending reply to remote. If `false` is returned, the process is stopped.
         * - `afterSendReply`: _[CallableFunction]_ Callback will be called after sending reply to remote. If `false` is returned, the process is stopped.
         * 
         * @readonly
         * @static
         * @memberof Inbox
         */
        static get DEFAULT_OPTIONS() {
            return {
                remote: null,
                templates: {
                    inbox: 'templates/html/profile/inbox/inbox-pm-list.hbs',
                    reply: 'templates/html/profile/inbox/private-message-reply.hbs'
                },
                selectors: {
                    pm: '[data-role="private-message"]',
                    trigger: '[data-trigger]',
                    draft: '[data-role="draft"]',
                    replyForm: '[data-role="reply-form"]',
                    replyButton: '[data-role="reply-button"]',

                    pmform: '[data-role="private-message-form"]',
                    pmformTrigger: '[data-role="pm-form-trigger"]'
                },
                classNames: {
                        icons: {
                            important: 'acms-icon-star text-purple',
                            unimportant: 'acms-icon-star regular'
                        },
                        important: 'important',
                        unimportant: 'default'
                },
                view: null,
                frequence: 15,

                /**
                 * Before Send Callback
                 *
                 * @param {Inbox}       self        Current instance
                 * @param {string}      message     Message to send
                 * @param {HTMLElement} element     Wrapper Element
                 * @param {FormData}    fData       Form Data instance of the pending send request
                 * 
                 * @returns {Boolean} return `true` on success, `false` to prevent submitting the form
                 * @memberof Inbox
                 */
                beforeSendReply: (self, message, element, fData) => {return true;},
                
                /**
                 * After Send Callback
                 *
                 * @param {Inbox}       self        Current instance
                 * @param {HTMLElement} element     Wrapper Element
                 * @param {Object}      response    Server Response Object
                 * @memberof Inbox
                 */
                afterSendReply: (self, element, response) => {}
            };
        }
        
        /**
         * Event "Before Send Reply"
         *
         * @readonly
         * @static
         * @memberof Inbox
         */
        static get EVENT_BEFORE_SEND_REPLY() {
            return this.NS + '.beforeSendReply';
        }

        /**
         * Event "After Send Reply"
         *
         * @readonly
         * @static
         * @memberof Inbox
         */
        static get EVENT_AFTER_SEND_REPLY() {
            return this.NS + '.afterSendReply';
        }

        /**
         * Module Init
         *
         * @static
         * 
         * @param {HTMLElement}     element Wrapper Element
         * @param {Object|null}     options Optional custom options
         * 
         * @returns {Inbox} New instance
         * 
         * @memberof Inbox
         */
        static init(element, options) {
            let m = new Inbox(element, options);
            m.element.acmsData(Inbox.NS, m);
            
            return m;
        }
        
        /**
         * Creates an instance of Inbox.
         * 
         * @param {HTMLElement}     element Wrapper Element
         * @param {Object|null}     options Optional custom options
         * 
         * @memberof Inbox
         */
        constructor(element, options) {
            super(element, options);
            this._initialize();
        }

        get processing() {
            return this._processing;
        }

        /**
         * Setup Event Listener
         *
         * @memberof Inbox
         */
        listen() {
            let self = this, o = self.options, sel = o.selectors, element = self.element;
            AcmsEvent.on(self.element, 'click', sel.trigger, self._onClickTrigger, true);
            if(self._replyForm) {
                AcmsEvent.on(self._replyForm, 'click', sel.replyButton, self._onClickReply, true);
            }
            if(o.view === 'inbox-show' && parseInt(o.frequence) > 0) {
                self._start();
            }
            if(o.view === 'inbox-drafts') {
                AcmsEvent.on(element, 'click', '[data-apply="edit"]', self._onClickEditDraft, true);
            }
        }


        /**
         * On Click Edit draft
         *
         * @param {Event} e     Click Event for the edit trigger
         * 
         * @memberof Inbox
         */
        _onClickEditDraft(e) {
            let self = this, o = self.options, sel = o.selectors, form = Selectors.q(sel.pmform);
            if(!form) {
                console.log('draft form not found');
                return false;
            }
            let draft = Selectors.closest(e.target, sel.draft);
            if(!draft) {
                console.log('draft not found');
                return false;
            }
            let data = draft.acmsData('draft'), esc = new Escaper(), d = esc.unescape(data), obj = JSON.parse(d);
            let msg = Selectors.q('textarea', form);
            msg.value = obj.body;
            AcmsEvent.fireChange(msg);
            let subj = Selectors.q('input[name=subject]', form);
            subj.value = obj.title;
            AcmsEvent.fireChange(subj);
            let contentAware = Selectors.q('input[name=contentType]', form);
            contentAware.value = obj.ctype;
            AcmsEvent.fireChange(contentAware);
            let id = Selectors.q('input[name=draft_id]', form);
            id.value = obj.id;
            AcmsEvent.fireChange(id);
            let to = Selectors.q('input[name=to_user]', form);
            let inputsearch = to.acmsData("acms.ui.components.inputsearch");
            inputsearch.setTokens(obj.receiver, true, true);

            let btn = Selectors.q('[data-role="pm-form-trigger"]', self.element);
            AcmsEvent.fireClick(btn);
        }

        /**
         * On Click Reply
         *
         * @param {Event} e Click Event
         * @memberof Inbox
         */
        _onClickReply(e) {
            let self = this, o = self.options, sel = o.selectors, element = self.element, form = Selectors.q(sel.replyForm, element);
            e.preventDefault();
            e.stopPropagation();
            if(self._sending) {
                return false;
            }
            self._sending = true;
            
            let message = Selectors.q('textarea[name="message"]', form).value, fData = new FormData(form);
            if(!message) {
                require(["apps/ui/informations/update-bars"], function(UpdateBars) {
                    let appendTo = Selectors.closest(Selectors.q('textarea[name="message"]'), 'div'),
                        opts = {autoRemove: true, autoTimer: 1.5},
                        conf = {
                            message: Acms.Translator._('Dieses Feld ist erforderlich!'),
                            type: UpdateBars.TYPE_DANGER,
                            icon: 'acms-icon-exclamation-triangle'
                        };
                    UpdateBars.make(conf, opts, appendTo);
                });
                return false;
            }
            if(typeof(o.beforeSendReply) === "function") {
                let result = o.beforeSendReply(element, self, message, fData);
                if(false === result) {
                    return false;
                }
            }
            let event = AcmsEvent.createCustom(Inbox.EVENT_BEFORE_SEND_REPLY, {fData: fData, message: message});
            AcmsEvent.dispatch(element, event);
            require(['http/request', 'notifier'], (Request) => {
                Request.post(self.remote, fData, self._onDoneSendReply, self._fail, self._always);
            });
        }

        /**
         * On Click Trigger Event Handler
         *
         * @param {Event} e Click Event
         * 
         * @memberof Inbox
         */
        _onClickTrigger(e) {
            let self = this, o = self.options, element = self.element, target = e.target, fData = new FormData(), trigger, pm, done, cb;
            if(self._processing) {
                return;
            }
            self._processing = true;
            if(!target.acmsData('trigger')) {
                target = Selectors.closest(target, '[data-trigger]');
            }
            if(!target) return;
            trigger = target.acmsData('trigger');
            pm = o.view === 'inbox-drafts' ? Selectors.closest(target, o.selectors.draft) : Selectors.closest(target, o.selectors.pm);
            done = '_onDone' + StringTool.ucfirst(trigger);
            cb = (response) => {
                return self[done](response, pm, target);
            };
            let id = o.view === 'inbox-drafts' ? 'draft_id' : 'pmid';
            fData.append(id, (pm ? pm.acmsData('id') : target.acmsData('id')));
            fData.append('idx', (pm ? pm.acmsData('idx') : target.acmsData('idx')));
            fData.append('op', trigger);
            require(['http/request', 'notifier'], Request => {
                Request.post(self.remote, fData, cb, self._fail, self._always);
            });
        }

        /**
         * On Done Delete
         * 
         * @param {Object} response Remote response data
         * @param {HTMLElement|null} pm Private message
         * @param {HTMLElement} trigger    Clicked button trigger
         * 
         * @private
         * @memberof Inbox
         */
        _onDoneDelete(response, pm, trigger) {
            if(response.code !== 200) {
                return A.Notifications.createFromResponse(response);
            }
            let self = this, o = self.options;
            if(o.view === 'inbox-trash') {
                // TASK: delete all pms
                window.refresh(500);
            }
            let clearTrash = Selectors.q('[data-role="clear-cache"]', self.element);
            if(clearTrash) {
                Classes.addClass(clearTrash, 'opacity-0');
                Classes.removeClass(clearTrash, 'opacity-8');
            }
        }

        /**
         * On done star
         *
         * @param {Object} response Remote response data
         * @param {HTMLElement|null} pm Private message
         * @param {HTMLElement} trigger    Clicked button trigger
         * 
         * @private
         * @memberof Inbox
         */
        _onDoneStar(response, pm, trigger) {
            if(response.code !== 200) {
                return A.Notifications.createFromResponse(response);
            }
            let self = this, o = self.options, classNames = o.classNames, icn;
            icn = Selectors.q('.acms-icon', trigger);
            Classes.addClass(trigger, classNames.important);
            trigger.acmsData('trigger', 'unstar');
            if(pm) Classes.addClass(pm, classNames.important);
            if(icn) {
                Classes.removeClass(icn, classNames.icons.unimportant);
                Classes.addClass(icn, classNames.icons.important);
            }

        }

        /**
         * On done unstar
         *
         * @param {Object}      response    Server Response Object
         * @param {HTMLElement} pm          HTML Element representing the processed private message
         * @param {HTMLElement} trigger     Clicked trigger
         * 
         * @private
         * @memberof Inbox
         */
        _onDoneUnstar(response, pm, trigger) {
            if(response.code !== 200) {
                return A.Notifications.createFromResponse(response);
            }
            let self = this, o = self.options, classNames = o.classNames, icn;
            icn = Selectors.q('.acms-icon', trigger);
            trigger.acmsData('trigger', 'star');
            Classes.removeClass(trigger, classNames.important);
            if(pm) Classes.removeClass(pm, classNames.important);
            if(icn) {
                Classes.removeClass(trigger, classNames.icons.important);
                Classes.addClass(trigger, classNames.icons.unimportant);
            }
        }

        /**
         * 
         * @param {Object}      response    Server Response Object
         * @param {HTMLElement} pm          HTML Element representing the processed private message
         * @param {HTMLElement} trigger     Clicked trigger
         * 
         * @private
         * @memberof Inbox
         */
        _onDoneTrash(response, pm, trigger) {
            if(response.code !== 200) {
                return A.Notifications.createFromResponse(response);
            }
            let self = this, o = self.options;
            if(o.view === 'inbox-show') {
                history.go(-1);
            }
            if(pm) pm.parentNode.removeChild(pm);
            let clearTrash = Selectors.q('[data-role="clear-cache"]', self.element);
            if(clearTrash) {
                Classes.addClass(clearTrash, 'opacity-8');
                Classes.removeClass(clearTrash, 'opacity-0');
            }
        }
        
        /**
         * 
         * @param {Object}      response    Server Response Object
         * @param {HTMLElement} pm          HTML Element representing the processed private message
         * @param {HTMLElement} trigger     Clicked trigger
         * 
         * @private
         * @memberof Inbox
         */
        _onDoneDeleteDraft(response, pm, trigger) {
            if(response.code !== 200) {
                return A.Notifications.createFromResponse(response);
            }
            let self = this, o = self.options;
            if(pm) pm.parentNode.removeChild(pm);
        }
        
        /**
         * On Done Send reply
         * 
         * @private
         */
        _onDoneSendReply(response) {
            let self = this, o = this.options, element = self._replyForm, isInModal = Selectors.closest(self._replyForm, '.modal');
            self._sending = false;
            A.Notifications.createFromResponse(response);
            if(response.status !== 'success') {
                return;
            }
            self._replyForm.reset();
            if(isInModal) {
                let dismiss = Selectors.q('[data-dismiss="modal"]', isInModal);
                AcmsEvent.fireClick(dismiss);
            }
            let appendTo = Selectors.q('[data-role="replies"]', self.element), cb;
            let tdata = {reply: response.reply, from: response.from};
            cb = () => {
                let evt = AcmsEvent.createCustom(Inbox.EVENT_AFTER_SEND_REPLY, {response: response, message: message});
                AcmsEvent.dispatch(element, evt);
                if(typeof(o.afterSendReply) === "function") {
                    o.afterSendReply(self, element, response);
                }
            };
            TemplateEngine.compileResource(o.templates.reply, tdata, cb, appendTo);
        }

        /**
         * @private
         */
		_fail(xhr, textStatus) {
            A.Logger.logWarning(textStatus);
		}
		
        /**
         * @private
         */
		_always(xhr, textStatus) {
			A.Logger.logInfo(textStatus);
            this._processing = false;
            this._sending = false;
        }
        
        
        /**
         * @private
         */
        _start() {
            let self = this, o = self.options, ms = parseInt(o.frequence * 1000);
            self.timeout = setInterval(() => {
                self.update();
            }, ms);
        }
        
        /**
         * @private
         */
        _stop() {
            clearInterval(this.timeout);
        }
        
        /**
         * @private
         */
        update() {
            let self = this,
                o = self.options,
                fData = new FormData();
            fData.append('op', 'loadreplies');
            fData.append('idx', self._idx);
            require(['http/request', 'notifier'], (Request) => {
                //Request.post(self.remote, fData, self._onDoneLoadReplies, self._fail, self._always);
            });
        }
        
        /**
         * @private
         */
        _onDoneLoadReplies(response) {
            let self = this, o = this.options, element = this.element;
            if(response.status === 'success' && response.replies.length) {
                let replies = response.replies, i;
                for(i = 0; i < replies.length; i++) {
                    TemplateEngine.compileResource(o.templates.reply, tData, cb, self._replies);
                    rep.innerHTML = replies[i];
                    while(rep.firstChild) {
                        this.element.appendChild(rep.firstChild);
                    }
                }
                self._reorder();
            }
        }
        
        /**
         * @private
         */
        _reorderReplies() {
            let self = this, o = this.options, element = this.element, i, l;
            let messages = Selectors.qa(o.wrapper, element);
            messages.sort(function(a, b) {
                a = new Date(a.getAttribute('data-date'));
                b = new Date(b.getAttribute('data-date'));
                return (a > b ? 1 : -1);
            });
            element.innerHTML = "";

            for(i = 0, l = messages.length; i < l; i++) {
                element.appendChild(messages[i]);
            }
        }
        
        /**
         * Prepare Instance
         *
         * @memberof Inbox
         */
        _prepare() {
            let self = this, o = self.options, element = self.element;
            self._processing = false;
            self._sending = false;
            if(!o.remote || StringTool.isUrl(o.remote) === false) {
                throw new Error('Remote Url is required');
            }
            self.remote = o.remote;
            if(o.view == 'inbox-show') {
                self._replyForm = Selectors.q(o.selectors.replyForm, element);
                self._replies = Selectors.q('[data-role="replies"]', element);
                self._idx = Selectors.q('[data-role="private-message"]', element).acmsData('idx');
            }
        }

    }

    return Inbox;
});
