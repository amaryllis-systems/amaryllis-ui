/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("apps/profile/inbox/pm-form", [
    "core/base",
    "core/classes",
    "core/selectors",
    "events/event",
    "tools/string/string-tool",
    "core/acms"
], (Base, Classes, Selectors, AcmsEvent, StringTool, A) => {

    /**
     * Private Message Form Handling
     *
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class PmForm extends Base {

        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof PmForm
         */
        static get NS() {
            return "acms.apps.profile.inbox.pm-form";
        }

        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof PmForm
         */
        static get MODULE() {
            return "Private Message Form";
        }

        static get DEFAULT_OPTIONS() {
            return {
                remote: null,
                selectors: {
                    triggers: {
                        save: '[data-trigger="save-draft"]',
                        send: '[data-trigger="send-pm"]',
                        abort: '[data-trigger="abort-pm"]'
                    }
                }
            };
        }

        /**
         * Module init
         *
         * @static
         * 
         * @param {HTMLElement}     element Form Element
         * @param {Object|null}     options Optionale custom options
         * 
         * @returns {PmForm} New instance
         * 
         * @memberof PmForm
         */
        static init(element, options) {
            let m = new PmForm(element, options);
            m.element.acmsData(PmForm.NS, m);
            
            return m;
        }

        /**
         * Creates an instance of PmForm.
         * 
         * @param {HTMLElement}     element Form Element
         * @param {Object|null}     options Optionale custom options
         * 
         * @memberof PmForm
         */
        constructor(element, options) {
            super(element, options);
            this._initialize();

        }

        get processing() {
            return this._processing;
        }

        /**
         * Setup Event Listener
         *
         * @memberof PmForm
         */
        listen() {
            let self = this, o = self.options, element = self.element, trigger = o.selectors.triggers;
            AcmsEvent.on(element, 'click', trigger.save, self._onClickSaveDraft, true);
            AcmsEvent.on(element, 'click', trigger.abort, self._onClickAbort, true);
            AcmsEvent.on(element, 'click', trigger.send, self._onClickSend, true);
        }

        /**
         * On Click Send
         *
         * @param {Event} e
         * 
         * @memberof PmForm
         */
        _onClickSend(e) {
            let self = this, 
                o = self.options, 
                element = self.element,
                tagName = element.tagName.toLowerCase(),
                form = (tagName === 'form') ? element : Selectors.q('form', element), 
                fData,
                draftId = Selectors.q('input[name="draft_id"]', form);
            e.preventDefault();
            e.stopPropagation();
            if(self.processing) return;
            self._processing = true;
            fData = new FormData(form);
            if(draftId && parseInt(draftId.value) > 0) {
                fData.append('op', 'sendDraft');
            } else {
                fData.append('op', 'send');
            }
            require(['http/request', 'notifier'], Request => {
                Request.post(self.remote, fData, self._onSended, self._fail, self._always);
            });
        }

        /**
         * On Click Abort
         *
         * @param {Event} e Click Event
         * @memberof PmForm
         */
        _onClickAbort(e) {
            let self = this, o = self.options, tagName = self.element.tagName.toLowerCase(), form = (tagName === 'form') ? self.element : Selectors.q('form', self.element);
            if(form) form.reset();
        }

        /**
         * On Click Save Draft Event Handler
         *
         * @param {Event} e click Event
         * 
         * @memberof PmForm
         */
        _onClickSaveDraft(e) {
            let self = this, 
                o = self.options, 
                element = self.element,
                tagName = element.tagName.toLowerCase(),
                form = (tagName === 'form') ? element : Selectors.q('form', element), 
                fData,
                draftId = Selectors.q('input[name="draft_id"]', form);
            e.preventDefault();
            e.stopPropagation();
            if(self.processing) return;
            self._processing = true;
            fData = new FormData(form);
            if(draftId && parseInt(draftId.value) > 0) {
                fData.append('op', 'saveDraft');
            } else {
                fData.append('op', 'createDraft');
            }
            
            require(['http/request', 'notifier'], Request => {
                Request.post(self.remote, fData, self._onSaved, self._fail, self._always);
            });
        }

        /**
         * On Saved Draft callback
         *
         * @param {Object} response Remote response object
         * 
         * @memberof PmForm
         */
        _onSaved(response) {
            if(response.status !== 'success') {
                return A.Notifications.createFromResponse(response);
            }
            let self = this, 
                o = self.options, 
                element = self.element,
                tagName = element.tagName.toLowerCase(),
                form = (tagName === 'form') ? element : Selectors.q('form', element),
                draftId = Selectors.q('input[name="draft_id"]', form);
            let draft = response.draft;
            if(draftId && parseInt(draftId.value) === 0) {
                draftId.value = draft.id;
                draftId.setAttribute('value', draft.id);
                AcmsEvent.fireChange(draftId);
            }
        }

        /**
         * On Sended callback
         *
         * @param {Object} response Remote response object
         * 
         * @memberof PmForm
         */
        _onSended(response) {
            
            A.Notifications.createFromResponse(response);
            if(response.status !== 'success') {
                return;
            }
            
            let self = this, 
                o = self.options, 
                element = self.element,
                tagName = element.tagName.toLowerCase(),
                form = (tagName === 'form') ? element : Selectors.q('form', element);
            let trigger = Selectors.q(o.triggers.abort, form);
            if(trigger) AcmsEvent.fireClick(trigger);
        }

		_fail(xhr, textStatus) {
            A.Logger.logWarning(textStatus);
		}
		
		_always(xhr, textStatus) {
			A.Logger.logInfo(textStatus);
			this._processing = false;
		}

        /**
         * Prepare Module
         *
         * @memberof PmForm
         */
        _prepare() {
            let self = this, o = self.options, element = self.element;
            self.remote = o.remote || element.getAttribute('action');
            self.isInModal = Selectors.closest(element, '.modal');
            if(typeof self.remote != 'string' || ! StringTool.isUrl(self.remote)) {
                throw new Error('Remote Url is missing');
            }

        }

    }

    return PmForm;
});
