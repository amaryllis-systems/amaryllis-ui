/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/profile/inbox/inbox-check", [
    "core/base",
    "core/classes",
    "core/selectors",
    "events/event",
    "core/acms"
], (Base, Classes, Selectors, AcmsEvent, A) => {

    /**
     * Inbox Check
     * 
     * Simple wrapper around a counter to count all unread message types.
     *
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class InboxCheck extends Base {

        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof InboxCheck
         */
        static get NS() {
            return "acms.apps.profile.inbox.inbox-check";
        }

        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof InboxCheck
         */
        static get MODULE() {
            return "Inbox Check";
        }

        /**
         * Default options
         * 
         * - `remote`: _[String]_ Remote URL
         * - `frequency`: _[Number]_ Check frequency in seconds.  
         *    0 turns check off on init and requires manual start of inbox check.
         * - `targets`: _[Object]_ Counter => Target map
         *  - `targets.drafts`: counts drafts of the owner
         *  - `targets.sent`: counts sent messages by owner
         *  - `targets.important`: counts important flagged messages
         *  - `targets.trash`: counts messages in trash
         *  - `targets.unread`: counts any unread message
         *  - `targets.mentions`: counts all mentions notification in inbox
         *  - `targets.unreadMentions`: counts new mentions, that have __not__ be notified to oner
         * - `pause`: _[String]_ Selector to get a "pause check" button inside `self.element`
         * - `start`: _[String]_ Selector to get a "start check" button inside `self.element`
         * 
         * @readonly
         * @static
         * @memberof InboxCheck
         */
        static get DEFAULT_OPTIONS() {
            return {
                remote: null,
                frequency: 30,
                targets: {
                    'drafts': '[data-role="drafts-counter"]',
                    'sent': '[data-role="sent-counter"]',
                    'important': '[data-role="important-counter"]',
                    'trash': '[data-role="trash-counter"]',
                    'unread': '[data-role="inbox-counter"]',
                    'mentions': '[data-role="mentions-counter"]',
                    'unreadMentions': '[data-role="nunread-mentions-counter"]',
                    'unreadNotifications': '[data-role="notifications-counter"]',
                    'notifications': '[data-role="notifications-counter"]',
                    'unreadMessages': '[data-role="nunread-pm-counter"]',
                    'messages': '[data-role="pm-counter"]'
                },
                pause: '[data-role="pause-check"]',
                start: '[data-role="start-check"]'
            };
        }

        /**
         * Module init
         * 
         * @static
         * 
         * @param {HTMLElement} element     wrapper element
         * @param {Object}      options     Optonal custom options
         * 
         * @memberof InboxCheck
         */
        static init(element, options) {
            let m = new InboxCheck(element, options);
            m.element.acmsData(InboxCheck.NS, m);
            
            return m;
        }

        /**
         * Creates an instance of InboxCheck.
         * 
         * @param {HTMLElement} element     wrapper element
         * @param {Object}      options     Optonal custom options
         * 
         * @memberof InboxCheck
         */
        constructor(element, options) {
            super(element, options);
            this._initialize();
            if(this.options.frequency > 0) {
                this.start();
            }
        }

        /**
         * Setup Event Listener
         *
         * @memberof InboxCheck
         */
        listen() {
            let self = this, o = self.options, element = self.element;
            AcmsEvent.on(element, 'click', o.start, self._onClickStart, true);
            AcmsEvent.on(element, 'click', o.stop, self._onClickPause, true);
        }

        /**
         * Start Check
         * 
         * @memberof InboxCheck
         */
        start() {
            let self = this, o = self.options;
            let fn = () => {
                return self.process();
            }, ms = parseInt(o.frequency) * 1000;
            if(!ms) return;
            self.timer = setInterval(self._process, ms);
        }

        /**
         * Stop Check
         * 
         * @memberof InboxCheck
         */
        stop() {
            clearInterval(self.timer);
        }

        /**
         * On Click Start Callback
         *
         * @param {Event} e Click Event
         * 
         * @memberof InboxCheck
         */
        _onClickStart(e) {
            this.start();
        }

        /**
         * On Click Pause Callback
         *
         * @param {Event} e Click Event
         * 
         * @memberof InboxCheck
         */
        _onClickPause(e) {
            this.stop();
        }

        /**
         * Process check by sending request to remote
         * 
         * @memberof InboxCheck
         */
        _process() {
            let self = this, o = self.options, fData = new FormData();
            fData.append('op', 'check');
            require(['http/request', 'notifier'], Request => {
                Request.post(self.remote, fData, self._onDone, self._fail, self._always);
            });
        }

        /**
         * On Done Process callback
         *
         * @param {Object} response Remote response object
         * 
         * @memberof InboxCheck
         */
        _onDone(response) {
            if(response.status !== 'success') {
                return this.stop();
            }
            let self = this, o = self.options, targets = o.targets, prop, counter = response.counters, cnt, badges, badge, i;
            for(prop in targets) {
                if(typeof counter[prop] === 'undefined') {
                    continue;
                }
                cnt = counter[prop];
                badges = Selectors.qa(targets[prop]);
                for(i = 0; i < badges.length; i++) {
                    badge = badges[i];
                    badge.innerHTML = cnt;
                }
            }
        } 

		_fail(xhr, textStatus) {
            A.Logger.logWarning(textStatus);
		}
		
		_always(xhr, textStatus) {
			A.Logger.logInfo(textStatus);
            this._processing = false;
        }
        
        /**
         * Prepare Module
         * 
         * @memberof InboxCheck
         */
        _prepare() {
            let self = this, o = self.options;
            self.remote = o.remote;
            o.frequency = parseInt(o.frequency);
            self._processing = false;
        }

    }

    return InboxCheck;
});
