/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/profile/board/timeline", [
    "core/base",
    "core/classes",
    "core/selectors",
    "events/event",
    "core/acms"
], (Base, Classes, Selectors, AcmsEvent, A) => {

    /**
     * Profile Timeline
     *
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class Timeline extends Base {

        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof Timeline
         */
        static get NS() {
            return "acms.apps.profile.board.timeline";
        }

        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof Timeline
         */
        static get MODULE() {
            return "Profile Board Timeline";
        }

        /**
         * Default optiosn
         *
         * @readonly
         * @static
         * @memberof Timeline
         */
        static get DEFAULT_OPTIONS() {
            return {
                remote: null,
                limit: 20,
                trigger: '[data-role="update-timeline"]'
            };
        }

        /**
         * On Init Event
         *
         * @readonly
         * @static
         * @memberof Timeline
         */
        static get EVENT_INITIALIZED() {
            return Timeline.NS + '.initialized';
        }

        /**
         * On Update Event
         *
         * @readonly
         * @static
         * @memberof Timeline
         */
        static get EVENT_UPDATED() {
            return Timeline.NS + '.updated';
        }

        /**
         * Module Init
         *
         * @static
         * 
         * @param {HTMLElement}     element HTML Element
         * @param {Object|null}     options Optional custom options
         * 
         * @returns {Timeline}  New instance
         * 
         * @memberof Timeline
         */
        static init(element, options) {
            let m = new Timeline(element, options);
            m.element.acmsData(Timeline.NS, m);
            
            return m;
        }

        /**
         * Creates an instance of Timeline.
         * 
         * @param {HTMLElement}     element HTML Element
         * @param {Object|null}     options Optional custom options
         * 
         * @memberof Timeline
         */
        constructor(element, options) {
            super(element, options);
            this._initialize();
            this._loadTimeline();
        }

        /**
         * Aufsetzen der Event Listener
         * 
         * 
         */
        listen() {
            let self = this, o = this.options;
            if(o.trigger) {
                AcmsEvent.on(self.element, 'click', o.trigger, self._loadMore, true);
            }
        }
        
        /**
         * Hängt ein neues Timeline Item an die Einträge an
         * 
         * @param {String} content  Timeline Content
         * 
         * 
         */
        append(content) {
            let last = this.element.querySelector('li:last-child'), cls;
            if(last && !Classes.hasClass(last, 'timeline-inverted')) {
                cls = 'timeline-inverted';
            }
            let li = this._buildItem(cls, content);
            this.element.appendChild(li);
        }
        
        /**
         * Hängt ein neues Timeline Item vor die bestehenden Einträge
         * 
         * @param {String} content  Timeline Content
         * 
         * 
         */
        prepend(content) {
            let first = this.element.querySelector('li:first-child') , cls;
            if(first && !Classes.hasClass(first, 'timeline-inverted')) {
                cls = 'timeline-inverted';
            }
            let li = this._buildItem(cls, content);
            if(first) {
                this.element.insertBefore(li, first);
            } else {
                this.element.appendChild(li);
            }
        }
        
        /**
         * Bildet das neue Timeline Item
         * 
         * @param {Object} cls      Element CSS Klasse
         * @param {Object} content  Element Content
         * 
         * @returns {Element}   Das neue Element
         */
        _buildItem(cls, content) {
            let li = document.createElement('li');
            if(cls) {
                Classes.addClass(li, cls);
            }
            li.innerHTML = content;
            return li;
        }

        _loadMore(e) {
            if(e) e.preventDefault();
            
            let self = this,
                o = self.options,
                fData = new FormData();
            fData.append('limit', o.limit);
            fData.append('offset', self.offset);
            fData.append('action', 'load');
            require(['http/request'], Request => {
                Request.post(o.remote, fData, self._done, self._fail);
            });
            let ev = AcmsEvent.create(Timeline.EVENT_UPDATED);
            AcmsEvent.dispatch(self.element,ev);
            
            return false;
        }
        
        
        /**
         * Wird nach der Übermittlung ausgeführt
         * 
         * @param {object} response
         * 
         */
        _done(response) {
            let self = this, element = self.element, o = self.options;
            if(response.status === "success") {
                if(!response.timeline_data || !response.timeline_data.length) {
                    return true;
                }
                response.timeline_data.forEach(function(v) {
                    self.append(v.content);
                });

                self.total = parseInt(response.total);
                self.offset = response.offset + 1;
                self.counted = parseInt(Selectors.qa('li', self.element).length);
                if(self.total === self.counted) {
                    A.Logger.writeLog('Timeline ist vollständig');
                    self.finished = true;
                }
                if(typeof o.onUpdate === 'function') {
                    o.onUpdate(element, self);
                }
                A.Logger.writeLog('Timeline aktualisiert..');
            } else {
                A.Logger.writeLog('Timeline Lade-Fehler');
            }
        }

        /**
         * Wird im Falle eines Verbindungs-Problems zum Remote Server ausgeführt
         * @param {Object} xhr  Das XHR Objekt
         * @param {String} textStatus
         * 
         */
        _fail(xhr, textStatus) {
            A.Logger.writeLog(textStatus);
        }

        /**
         * Laden der Timeline Inhalte
         * 
         * 
         */
        _loadTimeline() {
            let self = this,
                o = self.options,
                fData = new FormData();
            fData.append('limit', o.limit);
            fData.append('offset', self.offset);
            fData.append('action', 'load');
            require(['http/request'], Request => {
                Request.post(o.remote, fData, self._done, self._fail);
            });
        }
        
        /**
         * Vorbereiten des Moduls
         * 
         * 
         */
        _prepare() {
            let self = this, o = self.options, element = self.element;
            self.offset = 0;
            let e = AcmsEvent.createCustom(Timeline.EVENT_INITIALIZED, {relatedTarget: element});
            AcmsEvent.dispatch(self.element,e);
        }

    }

    return Timeline;
});
