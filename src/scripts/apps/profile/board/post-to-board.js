/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define('apps/profile/board/post-to-board', [
    "core/base",
    "core/selectors",
    "apps/profile/board/timeline",
    "apps/ui/informations/overlay",
    "events/event",
    "core/acms"
], (Base, Selectors, Timeline, Overlay, AcmsEvent, A) => {

    /**
     * Post-To-Board Form
     *
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class PostToBoard extends Base {

        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof PostToBoard
         */
        static get NS() {
            return "acms.apps.profile.board.post-to-board";
        }

        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof PostToBoard
         */
        static get MODULE() {
            return "Post-To-Board Form";
        }

        /**
         * Default Options
         * 
         * - `remote`: _[String]_ Remote URL. If omitted, form action attribute will be used
         * - `timeline`: _[String]_ Selector of the timeline
         *
         * @readonly
         * @static
         * @memberof PostToBoard
         */
        static get DEFAULT_OPTIONS() {
            return {
                timeline: ".timeline",
                trigger: '[data-trigger="share"]',
                form: '[role="form"]',
                templates: {
                    board: 'templates/html/profile/board/board.hbs',
                    links: 'templates/html/profile/board/links.hbs',
                    audio: 'templates/html/profile/board/audio.hbs',
                    videos: 'templates/html/profile/board/videos.hbs',
                    pictures: 'templates/html/profile/board/pictures.hbs'
                },
                afterResponse: (self, form, response) => {},
                afterRendered: (self, form, response) => {}
            };
        }

        /**
         * Module init
         *
         * @static
         * 
         * @param {HTMLElement}     element HTML Element
         * @param {Object|null}     options Optional custom options
         * 
         * @returns {PostToBoard} New instance
         * 
         * @memberof PostToBoard
         */
        static init(element, options) {
            let m = new PostToBoard(element, options);
            m.element.acmsData(PostToBoard.NS, m);

            return m;
        }

        /**
         * Creates an instance of PostToBoard.
         * 
         * @param {HTMLElement}     element HTML Element
         * @param {Object|null}     options Optional custom options
         * 
         * @memberof PostToBoard
         */
        constructor(element, options) {
            super(element, options);
            this._initialize();
        }

        /**
         * Setup Event Listener
         *
         * @memberof PostToBoard
         */
        listen() {
            let self = this, element = self.element, o = self.options;
            AcmsEvent.on(element, 'click', o.trigger, self._onSubmit, true);
        }


        /**
         * Get Timeline
         *
         * @returns {HTMLElement|null}
         * @memberof PostToBoard
         */
        getTimeline() {
            let self = this, o = self.options, tl = Selectors.q(o.timeline, self.element);
            if(null === timeline || !timeline.acmsData(Timeline.NS)) {
                return null;
            }
            return timeline;
        }

        /**
         * On Submit Callback
         *
         * @param {Event} e Click Event
         *
         * @memberof PostToBoard
         */
        _onSubmit(e) {
            let self = this,
                action, form, fData, hasOverlay;
            e.preventDefault();
            e.stopPropagation();
            if(this._processing) return false;
            this._processing = true;
            form = Selectors.closest(e.target, o.form);
            fData = new FormData(form);
            self._currentForm = form;
            action = form.acmsData('remote') || form.getAttribute('action');
            hasOverlay = form.acmsData('overlay') || Selectors.q('input[type=file]', form);
            if(hasOverlay) {
                let overlay = Overlay.init(document.body, {closeOnEscape: false});
                overlay.setLoadingContent();
                overlay.append();
                self.overlay = overlay;
            }
            let done = (response) => {
                self._done(response, form);
            };
            fData.append('timeline', true);
            require(['http/request', 'notifier'], (Request) => {
                Request.post(action, fData, done, self._fail, self._always);
            });
            
            return false;
        }

        /**
         * Wird nach der Übermittlung ausgeführt
         * 
         * @param {object} response
         * 
         */
        _done(response, form) {
            A.Notifications.createFromResponse(response);
            if (response.status === "success") {
                if(typeof o.afterResponse === 'function')
                    o.afterResponse(self, form, response);
                // @fixme - use template engine to render timeline
                let tl = this.getTimeline();
                if (tl !== null && typeof tl === 'object' && response.timeline) {
                    tl.prepend(response.timeline);
                }
                //self._prependToTimeline();
                if (this.currentForm) {
                    this.currentForm.reset();
                    this.currentForm = null;
                }
            }
        }

        _prependToTimeline(response, form) {
            let self = this, o = self.options, template, tData, cb, tl;
            tl = Selectors.q(o.timeline, self.element);
            template = form.acmsData('template');
            cb = () => {
                if(typeof o.afterRendered === 'function')
                o.afterRendered(self, form, response);
            };

            require(['templates/template-engine'], TemplateEngine => {
                TemplateEngine.compileResource(template, response, cb, tl, 'afterbegin');
            });
        }

        /**
         * Wird im Falle eines Verbindungs-Problems zum Remote Server ausgeführt
         * @param {Object} xhr  Das XHR Objekt
         * @param {String} textStatus
         * 
         */
        _fail(xhr, textStatus) {
            A.Logger.writeLog(textStatus);
        }
        
        _always(xhr, textStatus) {
            A.Logger.writeLog(textStatus);
            if(this.overlay) this.overlay.remove();
            this.overlay = null;
            this._processing = false;
        }
        
        _prepare() {
            let self = this, o = self.options, element = self.element;
        }
    }

    return PostToBoard;

});
