/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/cookie-bar/cookie-bar", [
	"core/base",
	"http/cookie",
	"core/classes",
	"core/selectors",
	"events/event",
	"core/acms"
], function(Base, HttpCookie, Classes, Selectors, AcmsEvent, A) {
    
	/**
     * Cookie Bar
     *
     * @class
     * @extends {Base}
	 * @author qm-b <https://bitbucket.org/qm-b/>
     */
	class CookieBar extends Base {

		/**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof CookieBar
         */
		static get MODULE() {
			return "Cookie Bar";
		}

		/**
         * Modul Namespace
         *
         * @readonly
         * @static
         * @memberof CookieBar
         */
		static get NS() {
			return "acms.view.theme.cookie-bar";
		}
        
		/**
         * Default Options
         *
         * @readonly
         * @static
         * @memberof CookieBar
         */
		static get DEFAULT_OPTIONS() {
			return {
				delay: 2, // delay in s
				lifetime: 7, // cookie lifetime in d
				cookieName: "amarylliscookie",
				forUser: false,
				title: A.Translator.t("Cookie-Hinweis"),
				buttonText: A.Translator.t("Einverstanden"),
				force: false,
				position: "bottom-left",
				style: "inverted v2",
				template: "templates/html/cookie-bar.hbs",
				css: CookieBar.css,
				themecss: false 
			};
		}

		/**
         * CSS File
         *
         * @readonly
         * @static
         * @memberof CookieBar
         */
		static get css() {
			return "media/css/ui/cookie-bar";
		}

		/**
         * Module Init
         *
         * @static
         * @param {HTMLElement} element initial element
         * @param {Object} options  custom options
         * @returns {CookieBar} Neue Instanz
         * @memberof CookieBar
         */
		static init(element, options) {
			let cb = new CookieBar(element, options);
			cb.element.acmsData(CookieBar.NS, cb);

			return cb;
		}

		/**
         * Creates an instance of CookieBar.
         * 
         * @param {HTMLElement} element
         * @param {Object|null} options
         * @memberof CookieBar
         */
		constructor(element, options) {
			super(element, options);
			this.enabled =
            this.coreConfig = null;
			this.active = null;
			this._initialize();
			if(false === this.enabled) {
				this.remove();
				A.Nutzungsbedingungen.cookie = null;
			} else {
				this.draw();
			}
		}

		buildOptions(options) {
			let o = super.buildOptions(options);
			if(o.force === true || o.force === "true" || parseInt(o.force, 10) === 1) {
				o.force = true;
			} else {
				o.force = false;
			}
			if(o.forUser === true || o.forUser === "true" || parseInt(o.forUser, 10) === 1) {
				o.forUser = true;
			} else {
				o.forUser = false;
			}
			if(o.title === false || o.title === "false" || parseInt(o.title, 10) === 0) {
				o.title = false;
			}
			o.delay = parseInt(o.delay);

			return o;
		}

        
		remove() {
			let self = this, o = self.options, element = self.element;
			if(!element || self.removing === true) {
				return;
			}
			self.removing = true;
			if(self.bar) {
				Classes.removeClass(self.bar, "active");
				self.bar.setAttribute("aria-hidden", "true");
			}
			setTimeout(() => {
				if(!self.element) {
					return;
				}
				document.body.removeChild(self.element);
				self.element = null;
				self.removing = false;
			}, (o.delay * 1000));
			let vl = HttpCookie.readCookie(o.cookieName);
			if(!vl) {
				HttpCookie.setCookie(o.cookieName, true, 7);
			}
		}

		/**
         * Draw the bar
         *
         * @memberof CookieBar
         */
		draw() {
			let self = this, 
				o = self.options,
				element = self.element, 
				config = {
					style: o.style,
					position: o.position,
					title: o.title,
					closeText: o.buttonText
				},
				data = {
					message: self.coreConfig.body,
					title: self.coreConfig.title  || o.title,
					options: config,
					cookiesSupported: HttpCookie.isSupported()
				};
			require(["templates/template-engine"], (TemplateEngine) => {
				let cb = () => {
					self.bar = Selectors.q(".cookie-bar", element);
					if(self.bar) {
						Classes.addClass(self.bar, "active");
						self.bar.setAttribute("aria-hidden", "false");
						self.bar.setAttribute("aria-required", "true");
					}
					AcmsEvent.on(element, "click", "[data-dismiss=\"cookie\"]", self._onClickRemove, true);
				};
				TemplateEngine.compileResource(o.template, data, cb, self.element);
			});
		}


		_prepare() {
			let self = this, 
				o = self.options;
			self.coreConfig = (A && A.Nutzungsbedingungen && A.Nutzungsbedingungen.cookie) ? A.Nutzungsbedingungen.cookie : {};
			if(o.force) {
				self.enabled = true;
			} else if(!o.forUser && A.User.isUser) {
				self.enabled = false;
			} else {
				self.enabled = !HttpCookie.readCookie(o.cookieName);
			}
		}

        
		/**
         * On Click remove Event Listener
         * 
         * @param {Event} e 
         * 
         */
		_onClickRemove(e) {
			e.preventDefault();
			e.stopPropagation();
			this.remove();

			return false;
		}

	}
    
	return CookieBar;
});
