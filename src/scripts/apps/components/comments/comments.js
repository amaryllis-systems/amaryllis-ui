/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/components/comments/comments", [
    "core/base",
    "core/classes",
    "core/selectors",
    "events/event",
    "tools/string/string-tool",
    "core/acms"
], (Base, Classes, Selectors, AcmsEvent, StringTool, A) => {
    
    /**
     * Comments
     *
     * @class
     * @extends {Base}
     * 
     * @property {Boolean}  processing  `true` if any remote process is in work
     * @property {String}   remote      Remote URL for processing Requests
     */
    class Comments extends Base {
        
        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof Comments
         */
        static get NS() {
            return "acms.apps.components.comments.comments";
        }

        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof Comments
         */
        static get MODULE() {
            return "Comments";
        }

        /**
         * Default Options
         *
         * @readonly
         * @static
         * @memberof Comments
         */
        static get DEFAULT_OPTIONS() {
            return {
                remote: null,
                selectors: {
                    comment: '',
                    trigger: '[data-trigger]',
                    upvoteCounter: '[data-role="vote-counter"]',
                    downvoteCounter: '[data-role="downvote-counter"]',
                    averageCounter: '[data-role="average-counter"]',
                    totalvoteCounter: '[data-role="totalvote-counter"]',
                    formSubmit: 'button[data-action]'
                }
            };
        }

        /**
         * Module init
         *
         * @static
         * 
         * @param {HTMLElement} element Wrapper Element around comments
         * @param {Object} options Options
         * 
         * @returns {Comments} New instance
         * 
         * @memberof Comments
         */
        static init(element, options) {
            let m = new Comments(element, options);
            m.element.acmsData(Comments.NS, m);
            
            return m;
        }

        /**
         * Creates an instance of Comments.
         * 
         * @param {HTMLElement} element Wrapper Element around comments
         * @param {Object} options Options
         * 
         * @memberof Comments
         */
        constructor(element, options) {
            super(element, options);
            this._initialize();

        }

        get processing() {
            return this._processing;
        }

        get remote() {
            return this._remote;
        }
        
        /**
         * Setup Event Listener
         *
         * @memberof Comments
         */
        listen() {
            let self = this, o = self.options, sel = o.selectors, element = self.element;
            AcmsEvent.on(element, 'click', sel.trigger, self._onClickTrigger, true);
            AcmsEvent.on(element, 'click', sel.formSubmit, self._onClickSubmitForm, true);
            Selectors.qa(sel.formSubmit, element);
        }

        /**
         * On Click submit comment form
         *
         * @param {Event} e Click event
         * @memberof Comments
         */
        _onClickSubmitForm(e) {
            let self = this, o = self.options, sel = o.selectors, target = e.target, form = Selectors.closest(target, '[role=form]');
            e.preventDefault();
            e.stopPropagation();
            if(!form || self._processing) {
                return false;
            }
            let fData = new FormData(form);
            require(['http/request', 'notifier'], Request => {
                Request.post(self.remote, fData, self._onDoneSubmitForm, self._fail, self._always);
            });
        }

        /**
         * On done submit form
         *
         * @param {Object} response Remote response Object
         * 
         * @memberof Comments
         */
        _onDoneSubmitForm(response) {
            if(response.status !== 'success') {
                return A.Notifications.createFromResponse(response);
            }
            let self = this, o = self.options, sel = o.selectors;
            window.refresh(500);
        }

        /**
         * On Click Trigger
         *
         * @param {Event} e Click Event
         * 
         * @memberof Comments
         */
        _onClickTrigger(e) {
            let self = this, o = self.options, sel = o.selectors, target = e.target;
            e.preventDefault();
            e.stopPropagation();
            if(!Selectors.matches(target, sel.trigger)) {
                target = Selectors.closest(target, sel.trigger);
            }
            if(!target || self._processing) {
                return;
            }
            let trigger = target.acmsData('trigger');
            if((trigger === 'upvote' || trigger === 'downvote') && Classes.hasClass(target, 'voted')) {
                return false;
            }
            self._lastTrigger = target;
            self._processing = true;
            let fData = new FormData();
            fData.append('op', trigger);
            fData.append('id', target.acmsData('id'));
            let cb = '_onDone' + StringTool.ucfirst(trigger);
            let done = (response) => {
                self[cb](response);
            };
            require(['http/request', 'notifier'], Request => {
                Request.post(self.remote, fData, done, self._fail, self._always);
            });
        }

        /**
         * On done Upvote
         *
         * @param {Object} response Remote response Object
         * 
         * @memberof Comments
         */
        _onDoneUpvote(response) {
            let self = this, o = self.options, sel = o.selectors, target = self._lastTrigger;
            if(response.status !== 'success') {
                return A.Notifications.createFromResponse(response);
            }
            Classes.addClass(target, 'voted');
            self._updateCounters(response.counter, target, sel);
        }

        /**
         * On done Downvote
         *
         * @param {Object} response Remote response Object
         * 
         * @memberof Comments
         */
        _onDoneDownvote(response) {
            let self = this, o = self.options, sel = o.selectors, target = self._lastTrigger;
            if(response.status !== 'success') {
                return A.Notifications.createFromResponse(response);
            }
            Classes.removeClass(target, 'voted');
            self._updateCounters(response.counter, target, sel);
        }

        /**
         * Update Counters
         *
         * @param {Object}      counters    Counters from remote
         * @param {HTMLElement} target      Current Trigger
         * @param {Object}      sel         Selectors setup from options
         * 
         * @memberof Comments
         */
        _updateCounters(counters, target, sel) {
            let upcounter = Selectors.q(sel.upvoteCounter, target);
            if(upcounter) {
                upcounter.innerHTML = counters.upvotes;
            }
            let downcounter = Selectors.q(sel.downvoteCounter, target);
            if(downcounter) {
                downcounter.innerHTML = counters.downvotes;
            }
            let totalcounter = Selectors.q(sel.totalvoteCounter, target);
            if(totalcounter) {
                totalcounter.innerHTML = counters.total;
            }
            let averagecounter = Selectors.q(sel.averageCounter, target);
            if(averagecounter) {
                averagecounter.innerHTML = counters.average;
            }
        }

		_fail(xhr, textStatus) {
            A.Logger.logWarning(textStatus);
		}
		
		_always(xhr, textStatus) {
			A.Logger.logInfo(textStatus);
            this._processing = false;
            this._sending = false;
		}
        
        /**
         * Prepare Module
         *
         * @memberof Comments
         */
        _prepare() {
            let self = this, o = self.options;
            if(!StringTool.isUrl(o.remote)) {
                throw new TypeError('Remote must be a valid url');
            }
            self._remote = o.remote;
        }

    }

    return Comments;
});
