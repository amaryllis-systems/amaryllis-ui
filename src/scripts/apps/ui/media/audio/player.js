/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/ui/media/audio/player", [
    "core/base",
    "tools/string/string-tool",
    "core/classes",
    "core/selectors",
    "events/event",
    "core/acms"
], (Base, StringTool, Classes, Selectors, AcmsEvent, A) => {

    /**
     * Audio Player
     * 
     * Class tool to help building a custom audio player
     *
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     * 
     * @property {HTMLElement}      playlist    Playlist
     * @property {HTMLAudioElement} audio       Audio Element (HTML5 Player)
     */
    class AudioPlayer extends Base {

        /**
         * Custom Options of the module
         * 
         * @typedef {Object} AudioPlayer~ModuleOptions
         * 
         * @property {String}   [active="active"]       Active Class Name for current tracks.
         * @property {Number}   [initialVolume=0.10]    Initial Volume for the player
         * @property {Boolean}  [updateTitle=true]      Update head title tag with current audio title?
         * @property {Number}   [mode=1]                Initial repeat mode
         * @property {Boolean}  [muted=false]           Initial mute on?
         * @property {Object}   selectors
         * @property {String}   [selectors.controls='[data-role="controls"]']    
         *                                              Selector for Controls Element (Wrapper around control buttons)
         * @property {String}   [selector.trigger='[data-trigger]']      
         *                                              Selector of a control trigger
         * 
         */

        /**
         * Module Namespace
         * 
         * @var {String}
         *
         * @readonly
         * @static
         * @memberof AudioPlayer
         */
        static get NS() {
            return "acms.apps.ui.media.audio.player";
        }

        /**
         * Module Name
         * 
         * @var {String}
         *
         * @readonly
         * @static
         * @memberof AudioPlayer
         */
        static get MODULE() {
            return "Audio Player";
        }

        /**
         * Default Options
         * 
         * @type {AudioPlayer~ModuleOptions}
         *
         * @readonly
         * @static
         * @memberof AudioPlayer
         */
        static get DEFAULT_OPTIONS() {
            return {
                active: 'active',
                initialVolume: 0.10,
                selectors: {
                    controls: '[data-role="controls"]',
                    trigger: '[data-trigger]',
                    next: 'next',
                    last: 'last',
                    first: 'first',
                    prev: 'previous',
                    play: 'play',
                    pause: 'pause',
                    track: '[data-role="track"]',
                    audio: '[data-role="player"]',
                    playlist: '[data-role="current-playlist"]'
                },
                updateTitle: true,
                mode: AudioPlayer.REPEAT_ALL,
                muted: false,
                onReload: (element, self) => {}
            };
        }

        /**
         * Repeat Mode "None" 
         * 
         * Mode turns off any repeats. The player will stop 
         * if the end of the playlist is reached.
         * 
         * @var {Number}
         *
         * @readonly
         * @static
         * @memberof AudioPlayer
         */
        static get REPEAT_NONE() {
            return 0;
        }

        /**
         * Repeat Mode "All"
         * 
         * Repeats the whole playlist unless the user stops
         * playing music.
         *
         * @var {Number}
         *
         * @readonly
         * @static
         * @memberof AudioPlayer
         */
        static get REPEAT_ALL() {
            return 1;
        }

        /**
         * Repeat Mode "Current"
         * 
         * Repeats the current track
         * 
         * @var {Number}
         *
         * @readonly
         * @static
         * @memberof AudioPlayer
         */
        static get REPEAT_CURRENT() {
            return 2;
        }

        /**
         * Module init
         *
         * @static
         * 
         * @param {HTMLElement}                 element     Wrapper element around audio and playlist
         * @param {AudioPlayer~ModuleOptions}   options     optional custom options
         * 
         * @returns {AudioPlayer} New instance
         * 
         * @memberof AudioPlayer
         */
        static init(element, options) {
            
            const m = new AudioPlayer(element, options);
            m.element.acmsData(AudioPlayer.NS, m);
            
            return m;
        }

        /**
         * Creates an instance of AudioPlayer.
         * 
         * @param {HTMLElement}                 element     Wrapper element around audio and playlist
         * @param {AudioPlayer~ModuleOptions}   options     optional custom options
         * 
         * @memberof AudioPlayer
         */
        constructor(element, options) {
            super(element, options);
            this._initialize();
        }

        get playlist() {
            return this._playlist;
        }

        set playlist(playlist) {
            self._playlist = playlist;
        }

        get audio() {
            return this._audio;
        }

        set audio(audio) {
            this._audio = audio;
        }

        /**
         * Total Tracks
         * 
         * @var {Number}
         *
         * @readonly
         * @memberof AudioPlayer
         */
        get totalTracks() {
            return this._totalTracks;
        }

        /**
         * Previous Loudness
         *
         * @readonly
         * @memberof AudioPlayer
         */
        get previousVolume() {
            return this._previousVolume();
        }

        /**
         * Player Controls
         * 
         * @var {HTMLElement|null}
         *
         * @readonly
         * @memberof AudioPlayer
         */
        get controls() {
            return this._controls;
        }

        /**
         * Current Track Number
         * 
         * @var {Number}
         *
         * @readonly
         * @memberof AudioPlayer
         */
        get current() {
            return this._current;
        }

        /**
         * current track item
         * 
         * @var {HTMLElement}
         *
         * @readonly
         * @memberof AudioPlayer
         */
        get currentTrack() {
            return this._currentTrack;
        }

        /**
         * Repeat Mode
         * 
         * @var {Number}
         *
         * @readonly
         * @memberof AudioPlayer
         */
        get repeatMode() {
            return this._repeat;
        }

        /**
         * Is the player muted?
         * 
         * @var {Boolean}
         *
         * @readonly
         * @memberof AudioPlayer
         */
        get muted() {
            return this._muted;
        }

        /**
         * Get tracks
         *
         * @returns {Array} Array of available track items
         * @memberof AudioPlayer
         */
        getTracks() {
            const self = this, o = self.options, sel = o.selectors;
            return Selectors.qa(sel.track, self.playlist);
        }
        
        /**
         * Run a track
         *
         * @param {HTMLElement} track   List element holding the track
         * 
         * @memberof AudioPlayer
         */
        run (track) {
            const self = this,
                  o = self.options,
                  player = self.audio;
            let url;
            if(o.updateTitle) {
                const audioTitle = track.acmsData('title'), 
                    title = Selectors.q('title', Selectors.q('head'));
                if(title) title.innerHTML = audioTitle;
            }
            if(StringTool.isUrl(track.acmsData('url'))) {
                url = track.acmsData('url');
            } else {
                const anchor = Selectors.q('a', track);
                if(anchor) url = anchor.getAttribute('href');
                if(!url || !StringTool.isUrl(url)) {
                    throw new Error('unable to find audio url');
                }
            }

            player.src = url;
            const tracks = self.getTracks();
            let tr, t;
            for(t = 0; t < tracks.length; t++) {
                tr = tracks[t];
                Classes.removeClass(tr, o.active);
            }
            Classes.addClass(track, o.active);
            player.load();
            
            setTimeout(() => {
                self.play();
             }, 250);
        }

        /**
         * Repeat
         * 
         * Set the repeat mode of the player.
         * 
         * - `0`: Turn repeat off
         * - `1`: Repeat the whole playlist
         * - `2`: Repeat current Track
         *
         * @param {Number} mode Repeat Mode
         * 
         * @memberof AudioPlayer
         * @throws {Error} if the mode is invalid
         */
        repeat(mode) {
            switch(mode) {
                case AudioPlayer.REPEAT_CURRENT:
                    self._repeat = mode;
                    self.audio.loop = true;
                    break;
                case AudioPlayer.REPEAT_NONE:
                case AudioPlayer.REPEAT_ALL:
                    self._repeat = mode;
                    if(self.audio.loop) {
                        self.audio.loop = false;
                    }
                    break;
                default:
                    throw new Error('Invalid Repeat mode');

            }
        }

        /**
         * Start Playing if the player is paused
         *
         * @memberof AudioPlayer
         */
        play() {
            const self = this;
            if(self.audio.paused) {
                self.audio.play();
            }
        }
        
        /**
         * Pause Playing
         *
         * @memberof AudioPlayer
         */
        pause() {
            const self = this;
            if(!self.audio.paused) {
                self.audio.pause();
            }
        }

        /**
         * Reload the playlist tracks after remote-adding an item
         * 
         * @memberof AudioPlayer
         */
        reload() {
            const self = this,
                current = self.current,
                tracks = self.getTracks(),
                total = Selectors.qa('[data-role="total-audios"'),
                currentTrack = self.currentTrack;
            let j, i;
            self._totalTracks = tracks.length;
            if(total.length) {
                for(j = 0; j < total.length; j++) {
                    total[j].textContent = self.totalTracks;
                }
            }
            if(currentTrack !== tracks[current]) {
                for(i = 0; i < self.totalTracks; i++) {
                    if(currentTrack == tracks[i]) {
                        self._current = i;
                        self._currentTrack = tracks[i];
                        break;
                    }
                }
            }
            self.dispatch('reload', self.audio, self.currentTrack, self);
        }

        /**
         * Play first track
         *
         * @memberof AudioPlayer
         */
        first() {
            const self = this;
            const tracks = self.getTracks();
            self._current = 0;
            const link = tracks[self.current];
            self._currentTrack = link;
            self.run(link);
            self.dispatch('first', self.audio, self.currentTrack, self);
        }

        /**
         * Play last track
         *
         * @memberof AudioPlayer
         */
        last() {
            const self = this;
            const tracks = self.getTracks();
            self._current = tracks.length;
            const link = tracks[self.current];
            self._currentTrack = link;
            self.run(link);
            self.dispatch('last', self.audio, self.currentTrack, self);
        }

        /**
         * Play previous track
         *
         * @memberof AudioPlayer
         */
        previous() {
            const self = this;
            const tracks = self.getTracks();
            self._current--;
            let link;
            if(self.current < 0) {
                self._current = tracks.length;
                link = tracks[self.length];
            } else{
                link = tracks[self.current];
            }
            self._currentTrack = link;
            self.run(link);
            self.dispatch('previous', self.audio, self.currentTrack, self);
        }

        /**
         * Play next track
         * 
         * @memberof AudioPlayer
         */
        next() {
            const self = this;
            const tracks = self.getTracks();
            self.current ++;
            let link;
            if(self.current === tracks.length){
                self._current = 0;
                link = tracks[0];
            } else{
                link = tracks[self.current];
            }
            self._currentTrack = link;
            self.run(link);
            self.dispatch('next', self.audio, self.currentTrack, self);
        }

        /**
         * Mute the player
         *
         * @memberof AudioPlayer
         */
        mute() {
            this._previousVolume = this.audio.volume;
            this.audio.volume = 0;
            self.dispatch('mute', audio, volume, self);
        }

        /**
         * Make music quieter
         *
         * @memberof AudioPlayer
         */
        quieter() {
            const self = this, audio = self.audio;
            let volume = audio.volume;
            this._previousVolume = volume;
            this.audio.volume = volume--;
            self.dispatch('quieter', audio, volume, self);
        }

        /**
         * Make music louder
         *
         * @memberof AudioPlayer
         */
        louder() {
            const self = this, audio = self.audio;
            let volume = audio.volume;
            this._previousVolume = volume;
            this.audio.volume = volume++;
            self.dispatch('louder', audio, volume, self);
        }

        /**
         * Dispatch an event
         *
         * @param {String}  evt     Event Name
         * @param {...*}    args    Arguments
         * 
         * @memberof AudioPlayer
         * @private
         */
        dispatch(evt, ...args) {
            const self = this, o = self.options;
            self.emit(evt, ...args);
            const func = 'on' + StringTool.ucfirst(evt);
            if(typeof o[func] === 'function') {
                o[func](...args);
            }
        }
        
        /**
         * Setup Event Listener
         *
         * @memberof AudioPlayer
         * @private
         */
        listen() {
            const self = this, o = self.options, sel = o.selectors, element = self.element;
            if(self.controls) {
                AcmsEvent.on(self.controls, 'click', sel.trigger, self._onClickTrigger, true);
            }
            AcmsEvent.on(element, 'click', sel.track, self._onTrackClick, true);
            AcmsEvent.add(self.audio, 'ended', self._onPlayEnded);
            AcmsEvent.add(self.audio, 'play', self._onPlay);
            AcmsEvent.add(self.audio, 'pause', self._onPause);
        }

        /**
         * On Track click
         *
         * @param {Event} e     Click Event
         * 
         * @memberof AudioPlayer
         * @private
         */
        _onTrackClick(e) {
            AcmsEvent.stop(e);
            const self = this, o = self.options, sel = o.selectors;
            let target = e.target;
            if(!Selectors.matches(target, sel.track)) {
                target = Selectors.closest(target, sel.track);
            }
            self.run(target, self.audio);
            
            return false;
        }

        /**
         * On Click Control Trigger
         *
         * @param {Event} e Click Event
         * 
         * @memberof AudioPlayer
         * @private
         */
        _onClickTrigger(e) {
            const self = this, o = self.options, sel = o.selectors;
            let target = e.target, cb;
            if(!Selectors.matches(target, sel.trigger)) {
                target = Selectors.closest(target, sel.trigger);
            }
            if(!target) return;
            const trigger = target.acmsData('trigger');
            try {
                self[trigger]();
                self.emit(trigger, target, self.playlist, self.audio, self);
            } catch(E) {
                A.Logger.logError(E.message + ' in ' + AudioPlayer.NS);
            }
        }

        /**
         * On Play ended
         *
         * @param {Event} e Play ended Event
         * 
         * @memberof AudioPlayer
         * @private
         */
        _onPlayEnded(e) {
            const self = this;
            let link;
            if(self.audio.loop) {
                return;
            }
            self._current++;
            if(self.current === self.totalTracks && self._repeat !== AudioPlayer.REPEAT_NONE) {
                self_.current = 0;
                link = self.getTracks()[0];
                self.run(link);
            } else {
                link = self.getTracks()[self.current];
                self.run(link);
            }
        }

        _onPlay(e) {
            const self = this, o = self.options;
            Selectors.qa('[data-trigger="play"]', self.element).forEach((trigger) => {
                trigger.acmsData('trigger', 'pause', true);
                const icn = Selectors.q('.acms-icon', trigger);
                if(icn) {
                    Classes.addClass(icn, 'acms-icon-pause');
                    Classes.removeClass(icn, 'acms-icon-play');
                }
            });
        }

        _onPause(e) {
            const self = this, o = self.options;
            Selectors.qa('[data-trigger="pause"]', self.element).forEach((trigger) => {
                trigger.acmsData('trigger', 'play', true);
                const icn = Selectors.q('.acms-icon', trigger);
                if(icn) {
                    Classes.addClass(icn, 'acms-icon-play');
                    Classes.removeClass(icn, 'acms-icon-pause');
                }
            });
        }

        /**
         * Prepare Module
         *
         * @memberof AudioPlayer
         * @private
         */
        _prepare() {
            const self = this, o = self.options, sel = o.selectors, element = self.element;
            self._audio = Selectors.q(sel.audio);
            self._playlist = Selectors.q(sel.playlist, element);
            if(sel.controls) {
                self._controls = Selectors.q(sel.controls, element);
            }
            const tracks = self.getTracks();
            self._previousVolume = null;
            self._previousVolume = self.audio.volume;
            self.audio.volume = o.initialVolume;
            self._repeat = parseInt(o.mode);
            self._muted = o.muted;
            self._current = 0;
            self._totalTracks = tracks.length;

        }

    }

    return AudioPlayer;
});
