/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('apps/ui/media/images/cross-fading', [
    "core/base",
    "core/classes",
    "core/selectors",
    "events/event"
], (Base, Classes, Selectors, AcmsEvent) => {

    /**
     * Cross Fading Images
     * 
     * The module trades a container with image juxtapositions.
     *
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     * 
     * @see {@link https://www.amaryllis-ui.eu/ui-framework/media/images/gallery/cross-fading/|"Usage Example"}
     */
    class CrossFading extends Base {

        /**
         * Module Namespace
         * 
         * @type {String}
         *
         * @readonly
         * @static
         * @memberof CrossFading
         */
        static get NS() {
            return "apps.ui.media.images.cross-fading";
        }

        /**
         * Module Name
         * 
         * @type {String}
         *
         * @readonly
         * @static
         * @memberof CrossFading
         */
        static get MODULE() {
            return "cross-fading";
        }

        /**
         * Module Stylesheet
         * 
         * Custom Stylesheet will be loaded during module 
         * loader. you can change the style in options or
         * [by setting the global themecss](CssLoader.html) 
         * to `true` or your theme _directory_ name.
         * 
         * @type {String}
         *
         * @readonly
         * @static
         * @memberof CrossFading
         */
        static get css() {
            return "media/css/media/images/gallery";
        }

        /**
         * Default Options
         *
         * @type {Object}
         * 
         * @readonly
         * @static
         * @memberof CrossFading
         */
        static get DEFAULT_OPTIONS() {
            return {
                old: '.bilder-alt',
                new: '.bilder-neu',
                duration: 3, // Play duration
                autoplay: false,
                css: CrossFading.css,
                themecss: false
            };
        }

        /**
         * Module init
         *
         * @static
         * 
         * @param {HTMLElement} element     Wrapper Element
         * @param {Object}      options     Optional custom options
         * 
         * @returns {CrossFading} New instance
         * 
         * @memberof CrossFading
         */
        static init(element, options) {
            let m = new CrossFading(element, options);
            m.element.acmsData(CrossFading.NS, m);

            return m;
        }

        /**
         * Creates an instance of CrossFading.
         * 
         * @param {HTMLElement} element     Wrapper Element
         * @param {Object}      options     Optional custom options
         * 
         * @memberof CrossFading
         */
        constructor(element, options) {
            super(element, options);
            this.forward =
            this.current =
            this.total =
            this.$total =
            this.$current =
            this.alt =
            this.neu = null;
            this._initialize();
        }

        /**
         * Play all sets, one by one
         *
         * @memberof CrossFading
         */
        play() {
            let self = this, o = self.options;
            self._interval = setInterval(function() {
                self.next();
            }, (parseInt(o.duration) || 3) * 1000 );
            let pause = Selectors.qa('[data-trigger="pause"]', self.element);
            pause.forEach(function(btn) {
                btn.removeAttribute('disabled');
                btn.setAttribute('aria-disabled', 'false');
            });
        }

        /**
         * Pause autoplay
         * 
         * @memberof CrossFading
         */
        pause() {
            let self = this, o = self.options;
            clearInterval(this._interval);
            let pause = Selectors.qa('[data-trigger="pause"]', self.element);
            pause.forEach(function(btn) {
                btn.setAttribute('disabled', 'disabled');
                btn.setAttribute('aria-disabled', 'true');
            });
        }

        /**
         * Show first image-set
         * 
         * @memberof CrossFading
         */
        first() {
            let self = this, element = self.element, o = self.options;

            let imgA = element.querySelector(o.old +" img.top"),
                imgN = element.querySelector(o.new + " img.top"),
                nextAlt = self.alt[1],
                nextNeu = self.neu[1];
            this._hideImg(imgA);
            this._hideImg(imgN);
            this._showImg(nextNeu);
            this._showImg(nextAlt);
            this._updateCurrent(1);
        }
        
        /**
         * Show last image-set
         * 
         * @memberof CrossFading
         */
        last() {
            let self = this, element = self.element, o = self.options;

            let imgA = element.querySelector(o.old +" img.top"),
                imgN = element.querySelector(o.new + " img.top"),
                length = self.neu.length -1,
                nextAlt = self.alt[length],
                nextNeu = self.neu[length];
            this._hideImg(imgA);
            this._hideImg(imgN);
            this._showImg(nextNeu);
            this._showImg(nextAlt);
            this._updateCurrent(length);
        }
        
        /**
         * Show nex image-set
         * 
         * @memberof CrossFading
         */
        next() {
            let self = this, element = self.element, o = self.options;

            let imgA = element.querySelector(o.old +" img.top"),
                imgN = element.querySelector(o.new + " img.top"),
                current = parseInt(imgA.acmsData('index')),
                nextIndex = current + 1;
            if(!self.alt[nextIndex]) {
                return self.first();
            }
            let nextAlt = self.alt[nextIndex],
                nextNeu = self.neu[nextIndex],
                next = nextAlt.getAttribute('data-index');
            this._hideImg(imgA);
            this._hideImg(imgN);
            this._showImg(nextNeu);
            this._showImg(nextAlt);
            this._updateCurrent(next);
        }
        
        /**
         * Show previous image-set
         * 
         * @memberof CrossFading
         */
        prev() {
            let self = this, element = self.element, o = self.options;

            let imgA = element.querySelector(o.old +" img.top"),
                imgN = element.querySelector(o.new + " img.top"),
                current = parseInt(imgA.acmsData('index')),
                prevIndex = current - 1;
            if(!self.alt[prevIndex]) {
                return self.last();
            }
            let nextAlt = self.alt[prevIndex],
                nextNeu = self.neu[prevIndex],
                next = nextAlt.getAttribute('data-index');
            this._hideImg(imgA);
            this._hideImg(imgN);
            this._showImg(nextNeu);
            this._showImg(nextAlt);
            this._updateCurrent(next);
        }
        
        /**
         * Hide image
         *
         * @param {HTMLImageElement} img the image to hide
         * 
         * @memberof CrossFading
         * @private
         */
        _hideImg(img) {
            Classes.toggleClass(img, 'transparent top bottom');
            //Classes.removeClass(img, 'top');
            //Classes.addClass(img, 'bottom');
            img.setAttribute('aria-hidden', "true");
        }
        
        /**
         * Show image
         *
         * @param {HTMLImageElement} img the image to show
         * 
         * @memberof CrossFading
         * @private
         */
        _showImg(img) {
            Classes.toggleClass(img, 'transparent bottom top');
            //Classes.removeClass(img, 'bottom');
            //Classes.addClass(img, 'top');
            img.setAttribute('aria-hidden', "false");
        }
        
        /**
         * Update Current
         *
         * @param {String} value    New value
         * 
         * @memberof CrossFading
         * @private
         */
        _updateCurrent(value) {
            if (this.$current) {
                this.$current.innerHTML = value;
            }
        }
        
        /**
         * On Click Button Event Handler
         *
         * @param {Event} e Event
         * 
         * @memberof CrossFading
         * @private
         */
        _onClick(e) {
            let target = e.target, role, self = this;
            if(!target.acmsData('trigger')) {
                target = Selectors.closest(target, '[data-trigger]');
            }
            if(!target) {
                throw new Error('Kein Ziel gefunden');
            }
            role = target.acmsData('trigger');
            e.preventDefault();
            e.stopPropagation();
            self[role]();

            return false;
        }
        
        /**
         * Setup Event Listener
         * 
         * @memberof CrossFading
         * @private
         */
        listen () {
            let self = this;
            AcmsEvent.on(self.element, 'click', '[data-trigger]', self._onClick);
            if(self.options.autoplay) {
                self.play();
            }
        }

        /**
         * Prepare Module
         * 
         * @memberof CrossFading
         * @private
         */
        _prepare() {
            let self = this,
                o = self.options,
                element = self.element,
                wrA = Selectors.q(o.old, element),
                wrN = Selectors.q(o.new, element),
                wrAH = parseInt(wrA.offsetHeight),
                wrNH = parseInt(wrN.offsetHeight),
                h = (wrAH > wrNH ? wrAH : wrNH),
                imgsA = Selectors.qa('img', wrA),
                imgsN = Selectors.qa('img', wrN),
                imgsAlt = [], imgsNeu = [], aktuell = false, i;
            if(imgsA.length !== imgsN.length) {
                throw new Error('Bilder-Anzahl muss in beiden Containern gleich sein');
            }
            for (i = 0; i < imgsA.length; i++) {
                let imA = imgsA[i], aI = parseInt(imA.getAttribute('data-index')) || i + 1;
                let imN = imgsN[i];
                imA.acmsData('index', aI);
                imN.acmsData('index', aI);
                let hA = parseInt(imA.offsetHeight);
                let hN = parseInt(imN.offsetHeight);
                if (false === aktuell) {
                    let isCurrent = imA.getAttribute('data-current');
                    if (isCurrent) {
                        aktuell = aI;
                    }
                }
                if(hA > h) {
                    h = hA;
                }
                if (hN > h) {
                    h = hN;
                }
                imgsAlt[aI] = imA;
                imgsNeu[aI] = imN;
            }
            wrA.setAttribute('style', 'min-height: '+h+'px;');
            wrN.setAttribute('style', 'min-height: '+h+'px;');
            self.$total = element.querySelector("[data-role=total]");
            self.$current = element.querySelector("[data-role=current]");
            self.total = imgsA.length;
            if (self.$total) {
                self.$total.innerHTML = imgsA.length;
            }
            if (self.$current && aktuell) {
                self.$current.innerHTML = aktuell;
            }
            self.alt = imgsAlt;
            self.neu = imgsNeu;
            self._triggers = Selectors.qa('[data-trigger]', element);
        }
    }

    return CrossFading;

});
