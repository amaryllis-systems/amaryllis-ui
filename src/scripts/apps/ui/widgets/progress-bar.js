/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/ui/widgets/progress-bar", [
    "core/base",
    "core/classes",
    "core/selectors",
    "events/event",
    "tools/string/sprintf",
    "tools/number/bytes",
    "core/acms"
], (Base, Classes, Selectors, AcmsEvent, sprintf, Bytes, A) => {

    /**
     * Progress Bar
     * 
     * Die Progress-Bar bietet einfache Optionen zur Darstellung einer 
     * Fortschritts-Anzeige.
     * 
     * ``` html
     * <div class="progress-bar v1" role="progressbar" data-min=0 data-start=20 data-max=100 data-animate="true" data-module="apps/ui/widgets/progress-bar">
     *  <div class="progress-meter" data-role="progress-meter">
     *      <span class="progress-status" data-role="progress-status">0</span>
     *  </div>
     * </div>
     * ```
     * 
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     * 
     * @property {HTMLElement}                  element     Progress Bar Element
     * @property {ProgressBar~ModuleOptions}    options     Custom options for the instance
     * @property {Number}                       now         Current value
     * 
     * @example 
     * const prog = ProgressBar.init(document.getElementById('myProgress'), {animate: false});
     * let i = 0;
     * let interval = setInterval(() => {
     *      i++;
     *      prog.update(i);
     * }, 750);
     */
    class ProgressBar extends Base 
    {

        /**
         * On Status Change Callback
         * 
         * Callbacks of this type will be triggered during a status update (reset, update, finish progress)
         * 
         * @callback ProgressBar~onStatusChange
         * 
         * @param {HTMLElement} meter   the progress meter element
         * @param {HTMLElement} status  the progress status element
         * @param {ProgressBar} bar     instance of this class
         */

        /**
         * Format Value Callback
         * 
         * Callback will be called during status update to format the value to propper display.
         * The callbacks might only be changed, if the current format is not applicable.
         * 
         * @callback ProgressBar~formatValueCallback
         * 
         * @param {Number}                      part    the new value
         * @param {ProgressBar~ModuleOptions}   o       the options for this instance
         * @param {ProgressBar}                 self    ProgressBar instance
         * 
         * @returns {String}    proper formatted value
         */

         /**
          * Module Options
          * 
          * @typedef {Object} ProgressBar~ModuleOptions
          * 
          * @property {('currency'|'percent'|'bytes')}  [text=percent]  Formatting type for the text.
          *                                                             - `currency`: formatting the value as currency
          *                                                             - `percent`: formatting the value as currency
          *                                                             - `bytes`: Formats the number as a file size. Starting from bytes, it is converted to a more readable format.
          * @property {Number}                          [delay=10]      Delay for light animation
          * @property {Number}                          [step=1]        Step in light animation
          * @property {String}                          [currency=EUR]  Iso currency if `text` is set to `currency`
          * @property {String}                          [locale=de_DE]  Locale to format the number for if `text` is set to `currency`
          * @property {Number}                          [min=0]         Min value
          * @property {Number}                          [max=100]       Max value
          * @property {Number}                          [start=0]       Initial value. Will be set to Progress bar on reset and after module init.
          * @property {Boolean}                         [animate=true]  Apply the light animation on value update? The light animation increments/decreases step by step the current value to new value
          * @property {String}                          [meter='[data-role="progress-meter"]']
          *                                                             Selector to find the progress meter element
          * @property {String}                          [status='[data-role="progress-status"]']
          *                                                             Selector to find the progress status element
          * @property {Boolean}                         [vertical=false]
          *                                                             Is the progressbar vertical? if `true`, the `height` property will incremented instead of `width`
          * 
          * @property {ProgressBar~formatValueCallback} formatAmount    Function must return the display value (in progress status) for currency value
          * @property {ProgressBar~formatValueCallback} formatPercent   Function must return the display value (in progress status) for percent value
          * @property {ProgressBar~formatValueCallback} formateBytes    Function must return the display value (in progress status) for file-size value
          * 
          * @property {ProgressBar~onStatusChange}      onUpdate        On Update Progress Bar Callback
          * @property {ProgressBar~onStatusChange}      onFinish        On Finish Progress Bar Callback
          * @property {ProgressBar~onStatusChange}      onReset         On Reset Progress Bar Callback
          */

        /**
         * Module Namespace
         * 
         * @type {String}
         *
         * @readonly
         * @static
         * @memberof ProgressBar
         */
        static get NS() {
            return "acms.apps.ui.widgets.progress-bar";
        }

        /**
         * Module Name
         * 
         * @type {String}
         *
         * @readonly
         * @static
         * @memberof ProgressBar
         */
        static get MODULE() {
            return "Progress Bar";
        }

        /**
         * Event "finish"
         * 
         * @type {String}
         *
         * @readonly
         * @static
         * @memberof ProgressBar
         */
        static get EVENT_FINISH() {
            return ProgressBar.NS + '.finish';
        }

        /**
         * Event "update"
         * 
         * @type {String}
         *
         * @readonly
         * @static
         * @memberof ProgressBar
         */
        static get EVENT_UPDATE() {
            return ProgressBar.NS + '.update';
        }

        /**
         * Event "reset"
         * 
         * @type {String}
         *
         * @readonly
         * @static
         * @memberof ProgressBar
         */
        static get EVENT_RESET() {
            return ProgressBar.NS + '.update';
        }

        /**
         * Default options
         * 
         * @type {ProgressBar~ModuleOptions}
         *
         * @readonly
         * @static
         * @memberof ProgressBar
         */
        static get DEFAULT_OPTIONS() {
            return {
                delay: 10,
                text: 'percent', // percent oder currency
                currency: 'EUR',
                locale: 'de_DE',
                step: 1,
                min: 0,
                max: 100,
                start: 0,
                animate: true,
                vertical: false,
                meter: '[data-role="progress-meter"]',
                status: '[data-role="progress-status"]',
                formatPercent: (part, o, self) => { 
                    return `${part}%`;
                },
                formatAmount: (part, o, self) => {
                    // works mostly everywhere, except ie < 11
                    if(typeof Intl !== 'undefined' && 'MumberFormat' in Intl) {
                        let loc = o.locale.replace('_', '-');
                        let val = new Intl.NumberFormat(loc, { style: 'currency', currency: o.currency}).format(part);
                        let max = new Intl.NumberFormat(loc, { style: 'currency', currency: o.currency}).format(o.max);
                        return `${val}/${max}`;
                    }
                    // fallback to rare basic formatting
                    let val = part + o.currency;
                    let max = o.max + o.currency;
                    return `${val}/${max}`;
                },
                formateBytes: (part, o, self) => {
                    return Bytes(part, 3);
                },
                onReset: (meter, status, bar) => {},
                onUpdate: (meter, status, bar) => {},
                onFinish: (meter, status, bar) => {}
            };
        }

        /**
         * Module init
         *
         * @static
         * 
         * @param {HTMLElement}     element     Progress bar element
         * @param {Object}          options     Module Options
         * 
         * @returns {ProgressBar} new instance
         * 
         * @memberof ProgressBar
         */
        static init(element, options) {
            let m = new ProgressBar(element, options);
            m.element.acmsData(ProgressBar.NS, m);
            
            return m;
        }


        /**
         * Creates an instance of ProgressBar.
         * 
         * @param {HTMLElement}     element     Progress bar element
         * @param {Object}          options     Module Options
         * 
         * @memberof ProgressBar
         */
        constructor(element, options) {
            super(element, options);
            this._initialize();

        }

        get sprintf() {
            return sprintf;
        }

        /**
         * Gibt den aktuellen Wert zurück
         * 
         * @returns {Number}
         * 
         * @memberof ProgressBar
         */
        getValue() {
            return this.now;
        }

        /**
         * Aktualisiert den Wert der Progress-Bar auf den neuen Status
         * 
         * @param {Number}              value   New value
         * 
         * @memberof ProgressBar
         */
        update(value) {
            let self = this, o = self.options;
            value = value && parseInt(value) || 0;
            if(o.animate) {
                self._updateAnimated(value);
            } else {
                self._updateValue(value);
            }
        }
        
        /**
         * Beendet den Progress und setzt den Wert auf den Maximal-Wert der 
         * Optionen
         * 
         * @memberof ProgressBar
         */
        finish() {
            this._updateValue(o.max);
        }

        /**
         * Setzt den Wert auf den Start-Wert zurück
         * 
         * @memberof ProgressBar
         */
        reset() {
            this._updateValue(o.start);
            if(typeof o.onReset === 'function') {
                o.onReset(self.meter, self.status, self);
            }
            this.emit('reset', self.meter, self.status, self);
            const e = AcmsEvent.createCustom(ProgressBar.EVENT_RESET, {
                bar: self,
                meter: self.meter,
                value: o.start
            });
            AcmsEvent.dispatch(self.element, e);
        }
        
        /**
         * Light-Animation of Status-Value
         * 
         * @param {Number} value   new value
         * 
         * @memberof ProgressBar
         * @private
         */
        _updateAnimated(value) {
            const self = this;
            const o =self.options;
            let val = parseInt(value);
            let min = parseInt(o.min);
            let max = parseInt(o.max);
            if(val < min) {
                val = min;
            }
            if(val > max) {
                val = max;
            }
            const step = parseInt(o.step);
            let start = parseInt(self.now);
            if(this.interval) {
                clearInterval(this.interval);
            }
            // up or downwards
            const isLower = (val < start);
            this.interval = setInterval(() => {
                if(isLower === true) {
                    start -= step;
                } else {
                    start += step;
                }
                self._updateValue(start, false);
                if ((isLower && val >= start) || (!isLower && val <= start)) {
                    clearInterval(self.interval);
                }
            }, parseInt(o.delay));
        }
        
        /**
         * Aktualisiert den Wert im Progress-Meter
         * 
         * @param {Integer}     value               new value
         * @param {Boolean}     [clearing=true]     Clear interval?
         * 
         * @memberof ProgressBar
         * @private
         */
        _updateValue(value, clearing) {
            if(typeof clearing !== 'boolean') {
                clearing = true;
            }
            const self = this;
            const o =self.options;
            let val = parseInt(value);
            let min = parseInt(o.min);
            let max = parseInt(o.max);
            if(val < min) {
                val = min;
            }
            if(val > max) {
                val = max;
            }
            let start = parseInt(self.now);
            if(true === clearing && this.interval) {
                clearInterval(this.interval);
            }
            self.meter.setAttribute('aria-valuenow', val);
            // set the percent value for &.indicating
            self.element.acmsData('percent', val);
            let prop = (this.vertical === true ? 'height' : 'width');
            self.meter.style[prop] = parseInt(value) + '%';
            self.now = value;
            self._updateText(value);
            if(typeof o.onUpdate === 'function') {
                o.onUpdate(self.meter, self.status, self);
            }
            this.emit('update', self.meter, self.status, self);
            const e = AcmsEvent.createCustom(ProgressBar.EVENT_UPDATE, {
                bar: self,
                meter: self.meter,
                value: value
            });
            AcmsEvent.dispatch(self.element, e);
            
            if(val === max) {
                if(typeof o.onFinish === 'function') {
                    o.onFinish(self.meter, self.status, self);
                }
                let e2 = AcmsEvent.createCustom(ProgressBar.EVENT_FINISH, {
                    bar: self,
                    meter: self.meter,
                    value: value
                });
                AcmsEvent.dispatch(self.element, e2);
                this.emit('finish', self.meter, self.status, self);
            }
        }
        
        /**
         * Update Text
         *
         * @param {Number} value
         * 
         * @memberof ProgressBar
         * @private
         */
        _updateText(value) {
            const self = this;
            const o = this.options;
            let cb = 'format', txt;
            switch(o.text.trim()) {
                default:
                case 'percent':
                    cb += 'Percent';
                    break;
                case 'currency':
                    cb += 'Amount';
                    break;
                case 'bytes':
                    cb += 'Bytes';
                    break;
            }
            
            txt = o[cb]((value));
            self.status.innerHTML = txt;
        }
        
        /**
         * Vorbereiten der Progress-Bar
         * 
         * @memberof ProgressBar
         * @private
         */
        _prepare() {
            const self = this;
            const o = self.options;
            const element = self.element;
            const meter = Selectors.q(o.meter, element);
            const status = Selectors.q(o.status, element);
            if(Classes.hasClass(element, 'vertical')) {
                this.vertical = true;
                element.acmsData('vertical', true);
            }
            self.status = status;
            self.start = parseInt(o.start);
            self.now = self.start;
            self.interval = null;
            meter.setAttribute('aria-valuemin', o.min);
            meter.setAttribute('aria-valuemax', o.max);
            meter.setAttribute('aria-valuenow', self.start);
            self.meter = meter;
            self.update(self.start);
            
        }

    }

    return ProgressBar;
});
