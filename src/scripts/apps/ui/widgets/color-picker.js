/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/ui/widgets/color-picker", [
    "core/base",
    "apps/ui/helpers/drag-listener",
    "tools/color/color-converter",
    "core/classes",
    "core/selectors",
    "templates/template-engine",
    "tools/string/unique-id",
    "apps/ui/informations/popover",
    "events/event",
    "core/acms"
], (Base, ColorConverter, Classes, Selectors, TemplateEngine, UniqueId, Popover, AcmsEvent, A) => {

    
    const type = (window.SVGAngle || document.implementation.hasFeature("http://www.w3.org/TR/SVG11/feature#BasicStructure", "1.1") ? "svg" : "vml");
    if(type === 'vml' && !document.namespaces.v)
        document.namespaces.add('v', 'urn:schemas-microsoft-com:vml', '#default#VML');
    
    var picker, slide, hueOffset = 15, svgNS = 'http://www.w3.org/2000/svg';


    /**
     * Color Picker
     * 
     * Simple Color Picker Module to create a color picker of an element
     *
     * @class
     * @extends {Base}
     * 
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class ColorPicker extends Base {

        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof ColorPicker
         */
        static get NS() {
            return "acms.apps.ui.widgets.color-picker";
        }

        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof ColorPicker
         */
        static get MODULE() {
            return "Color-Picker";
        }

        /**
         * Default Options
         *
         * @readonly
         * @static
         * @memberof ColorPicker
         */
        static get DEFAULT_OPTIONS() {
            return {
                templates: {
                    // Main template is in popover options
                    // slide
                    slide: {
                        svg: 'templates/html/ui/widgets/color-picker/slide-svg.hbs',
                        vml: 'templates/html/ui/widgets/color-picker/slide-vml.hbs',
                    },
                    // picker
                    picker: {
                        svg: 'templates/html/ui/widgets/color-picker/picker-svg.hbs',
                        vml: 'templates/html/ui/widgets/color-picker/slide-vml.hbs',
                    }
                },
                selectors: {
                    input: '[data-role="picker-trigger"]' // supports input and button
                },
                trigger: 'focus', // supports focus (for input) and click (for buttons)
                initial: '#000000', // initial value
                popup: 'right',
                layout: 'default',
                alpha:  true,
                editor: true,
                editorFormat: 'hex',
                popover: {
                    placement: 'top',
                    trigger: 'focus',
                    content: '', // string, function or selector
                    removeContent: true, // remove content wrapper if the content is a selector?
                    template: 'templates/html/ui/widgets/color-picker/color-picker.hbs',
                    //animation: true,
                    title: A.Translator._('Farbe wählen'),
                    //delay: 100,
                    dismiss: true, // create dismiss button?
                    html: true,
                    container: false, // Optional HTML Container. Otherwise document.body will be used
                    onCreate: function(popover, self) {},
                    onOpen: function(popover, self) {},
                    onClose: function(popover, self) {},
                    customClass: '', // 'primary', 'danger', ..
                    id: null, // ID des Popover oder leer lassen für automatische ID (empfohlen),
                    customData: null, // optional custom data to be assigned to the template. Variable is ignored in default template.
                    classNames: {
                        active: 'open'
                    },
                    selectors: {
                        active: '.open',
                        dismiss: '[data-dismiss="popover"]'
                    },
                    alwaysRemove: false, // remove always on close?
                    dismissOutside: true, // dismiss on click outside popover?
                    dismissResize: true, // dismiss on resize?
                    css: Popover.css,
                    themecss: false
                
                }
            };
        }

        /**
         * Module init
         * 
         * @param {HTMLElement}     element     Wrapper Element (e.g. form-group)
         * @param {Object} options 
         * 
         * @returns {ColorPicker} New instance
         * 
         * @static
         * @memberof ColorPicker
         */
        static init(element, options) {
            let m = new ColorPicker(element, options);
            m.element.acmsData(ColorPicker.NS, m);
            
            return m;
        }

        /**
         * Creates an instance of ColorPicker.
         * 
         * @param {HTMLElement}     element     Wrapper Element (e.g. form-group)
         * @param {Object} options 
         * 
         * @memberof ColorPicker
         */
        constructor(element, options) {
            super(element, options);
            this._openProxy  = (e) => this.openHandler(e);
            this._initialize();
            
        }

        show() {}

        hide() {}

        toggle() {}

        _on() {}

        _off() {}

        /**
         * On Focus Event
         *
         * @param {Event}   e   Focus Event
         * @memberof ColorPicker
         */
        _onFocus(e) {
            let self = this, o = self.options;

            self.show();
        }

        /**
         * Return mouse position relative to the element.
         *
         * @param {Event} evt   Event
         * 
         * @returns {Object} Mouse position with properties x and y 
         * 
         * @memberof ColorPicker
         */
        _getMousePosition(evt) {
            // IE:
            if (window.event && window.event.contentOverflow !== undefined) {
                return { x: window.event.offsetX, y: window.event.offsetY };
            }
            // Webkit:
            if (evt.offsetX !== undefined && evt.offsetY !== undefined) {
                return { x: evt.offsetX, y: evt.offsetY };
            }
            // Firefox:
            let wrapper = evt.target.parentNode.parentNode;
            return { x: evt.layerX - wrapper.offsetLeft, y: evt.layerY - wrapper.offsetTop };
        }


        _prepare() {
            let self = this, o = self.options, tpl = o.templates, element = self.element, sel = o.selectors;
            self.picker = Selectors.q(sel.input, element);
            let poOptions = o.popover;
            poOptions.onCreate = 
            self._popover = Popover.init(self.picker, o.popover);
            self.guid = UniqueId('colorpicker');
            let cb = () => {
                let p = tpl.picker[type];
                let s = tpl.slide[type];
                let loaded = 0;
                let done = () => {
                    loaded++;
                    if(loaded === 2) {
                        self.on();
                    }
                };
                self.wrapper = element.querySelector('.color-picker');
                self.slideElement = wrapper.querySelector('.slide');
                self.pickerElement = wrapper.querySelector('.picker');
                self.slideIndicator = wrapper.querySelector('.slide-indicator');
                self.pickerIndicator = wrapper.querySelector('.picker-indicator');
                TemplateEngine.compileResource(p, {guid: self.guid}, done, self.pickerElement);
                TemplateEngine.compileResource(s, {guid: self.guid}, done, self.slideElement);
            };
            TemplateEngine.compileResource(tpl.wrapper, {guid: self.guid}, cb, element);
        }

    }

    return ColorPicker;
});
