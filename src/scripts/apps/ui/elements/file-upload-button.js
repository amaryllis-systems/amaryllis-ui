/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/ui/elements/file-upload-button", [
    "core/base",
    "core/classes",
    "core/selectors",
    "tools/number/bytes",
    "tools/string/escaper",
    "events/event",
    "core/acms"
], (Base, Classes, Selectors, Bytes, Escaper, AcmsEvent, A) => {

    /**
     * File Upload Button
     *
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class FileUploadButton extends Base {

        static get NS() {
            return "acms.apps.ui.elements.file-upload-button";
        }


        static get MODULE() {
            return "File Upload Button";
        }

        static get DEFAULT_OPTIONS() {
            return {};
        }

        static init(element, options) {
            let m = new FileUploadButton(element, options);
            m.element.acmsData(FileUploadButton.NS, m);
            
            return m;
        }

        /**
         * Creates an instance of FileUploadButton.
         * 
         * @param {*} element
         * @param {*} options
         * 
         * @memberof FileUploadButton
         */
        constructor(element, options) {
            super(element, options);
            this._initialize();
        }

        
        listen() {
            let self = this;
            AcmsEvent.add(self.$input, 'change', self._onChange);
        }

        /**
         * On Change Event Listener
         *
         * @param {Event} e
         * @memberof FileUploadButton
         */
        _onChange(e) {
            let self = this, files = [], input = e.target, f;
            let esc = new Escaper();
            let imgcb = (theFile) => {
                return function(e) {
                    let img = self.previewImage ? self.previewImage : new Image();
                    img.src = e.target.result;
                    img.alt = esc.escape(theFile.name);
                    img.title = esc.escape(theFile.name) + ' (' + Bytes(theFile.size) + ')';
                    if(!self.previewImage) {
                        self.previewImage = img;
                        self.preview.appendChild(img);
                    }
                };
            };
            if(self.hasList && self.list) {
                self.list.innerHTML = '';
            }
            for (let x = 0; x < input.files.length; x++) {
                f = input.files[x];
                files.push(esc.escape(f.name));
                if(f.type.match('image.*') && self.hasImage && self.preview) {
                                    
                    let reader = new FileReader();
                    reader.onload = imgcb(f);
                    reader.readAsDataURL(f);
                } else if(self.hasList && self.list) {
                    let li = document.createElement('li');
                    let label = document.createElement('label');
                    let span = document.createElement('span');
                    li.appendChild(label);
                    label.innerHTML = esc.escape(f.name);
                    label.appendChild(span);
                    span.innerHTML = '(' + Bytes(f.size) + ')';
                    self.list.appendChild(li);
                    Classes.addClass(li, 'file ' + esc.escape(f.name.split('.').pop().toLowerCase()));
                }
            }
            let target = self.label;
            if(target) {
                if(target.tagName.toLowerCase() === 'input')
                target.value = files.join(', ');
                else target.innerHTML = files.join(', ');
            }
        }


        _prepare() {
            let self = this, element = self.element, o = self.options;
            self.$input = Selectors.q('input[type="file"]', element);
            self.$parent = Selectors.closest(element, '.form-group');
            if(!self.$input) {
                A.Logger.logWarning('Input of field not found!');
                return;
            }
            self.label = Selectors.q('[data-role="file-label"]', element);
            if(!self.label) {
                A.Logger.logWarning('Label of field ' + self.$input.name + ' not found!');
            }
            if(o.hasList === 'true' || o.hasList === true || parseInt(o.hasList) === 1) {
                self.hasList = true;
                self.list = Selectors.q('[data-role="file-list"]', self.$parent);
            }
            if(o.hasImage === 'true' || o.hasImage === true || parseInt(o.hasImage) === 1) {
                self.hasImage = true;
                self.preview = Selectors.q('[data-role="image-preview"]', self.$parent);
                if(self.preview) self.previewImage = Selectors.q('img', self.preview);
            }
        }

    }

    return FileUploadButton;
});
