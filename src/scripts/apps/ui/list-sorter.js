/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("apps/ui/list-sorter", [
	"core/base",
	"tools/number/is-number",
	"core/classes",
	"core/selectors",
	"events/event",
	"core/acms"
], (Base, isNumber, Classes, Selectors, AcmsEvent, A) => {

	/**
	 * List Sorter
	 *
	 * @class
	 * @extends {Base}
	 * @author qm-b <https://bitbucket.org/qm-b/>
	 * 
	 * @property {('asc'|'desc')} 	direction		Direction to sort
	 * @property {String} 			attribute 		Attribute to sort by. Ignored if source is not `data`
	 * @property {('html'|'text'|'data')} source	Source
	 * @property {boolean} 			sorting 		Whether or not the ListSorter is currently sorting.
	 * 												Identifies the current state to prevent double-sorting 
	 * 												in big lists.
	 */
	class ListSorter extends Base {

		/**
		 * Module Namespace
		 *
		 * @readonly
		 * @static
		 * @memberof ListSorter
		 */
		static get NS() {
			return "apps.ui.list-sorter";
		}


		/**
		 * Module Name
		 *
		 * @readonly
		 * @static
		 * @memberof ListSorter
		 */
		static get MODULE() {
			return "List Sorter";
		}

		/**
		 * Direction "ASC" (ascending)
		 *
		 * @readonly
		 * @static
		 * @memberof ListSorter
		 */
		static get ASC() {
			return 'asc';
		}

		/**
		 * Direction "DESC" (descending)
		 *
		 * @readonly
		 * @static
		 * @memberof ListSorter
		 */
		static get DESC() {
			return 'desc';
		}

		/**
		 * Event "sorting"
		 * 
		 * Event will be fired before sorting the list
		 *
		 * @readonly
		 * @static
		 * @memberof ListSorter
		 */
		static get EVENT_SORTING() {
			return ListSorter.NS + '.sorting';
		}

		/**
		 * Event "sorted"
		 * 
		 * Event will be fired after sorted the list
		 *
		 * @readonly
		 * @static
		 * @memberof ListSorter
		 */
		static get EVENT_SORTED() {
			return ListSorter.NS + '.sorted';
		}

		/**
		 * Default Options
		 *
		 * @readonly
		 * @static
		 * @memberof ListSorter
		 */
		static get DEFAULT_OPTIONS() {
			return {
				direction: ListSorter.ASC, // initial direction

				source: 'data', // sort-source might be `data` to sort by data-attribute, `html` to sort by inner HTML content, `text` to sorty inner text content.
				attribute: 'title', // initial attribute to sort by. Ignored/unused if the source is any other than `data`
				selectors: {
					active: 'active', // active elements in sort asc/desc and sort change triggers
					list: 'ul', // list element. can be any listing wrapper
					item: 'li', // list item element. can be any kind of item selector inside list element.
					asc: '[data-role="sort-asc"]', // trigger(s) to sort ascending. supported are button, select and radio-input elements
					desc: '[data-role="sort-desc"]', // trigger(s) to sort descending. supported are button, select and radio-input elements
					changer: '[data-role="sort-changer"]', // trigger(s) to toggle sort attribute. supported are button, select and radio-input elements
				}
			};
		}

		/**
		 * Module Init
		 *
		 * @static
		 * 
		 * @param {HTMLUListElement} 	element		List
		 * @param {Object|null} 		options		optional custom options
		 * 
		 * @returns {ListSorter}
		 * 
		 * @memberof ListSorter
		 */
		static init(element, options) {
			let m = new ListSorter(element, options);
			m.element.acmsData(ListSorter.NS, m);
			
			return m;
		}


		/**
		 * Creates an instance of ListSorter.
		 * 
		 * @param {HTMLUListElement} 	element		List
		 * @param {Object|null} 		options		optional custom options
		 * 
		 * @memberof ListSorter
		 */
		constructor(element, options) {
			super(element, options);
			this._initialize();

		}

		get direction() {
			return this._direction;
		}
		set direction(direction) {
			if(typeof direction !== "string") {
				throw new TypeError();
			}
			direction = direction.toLowerCase();
			if(direction === ListSorter.ASC || direction === ListSorter.DESC) {
				this._direction = direction;
			} else {
				throw new Error("Direction is invalid. Acceptiong asc and desc");
			}
		}

		get attribute() {
			return this._attribute;
		}
		set attribute(attribute) {
			if(typeof attribute !== "string") {
				throw new TypeError('invalid Attribute');
			}
			this._attribute = attribute;
		}

		get source() {
			return this._source;
		}
		set source(source) {
			if(typeof source !== "string") {
				throw new TypeError('invalid Source');
			}
			if('data' !== source && 'html' !== source && 'text' !== source) {
				throw new Error('invalid source');
			}
			this._source = source;
		}

		get sorting() {
			return this._sorting;
		}

		set sorting(val) {
			this._sorting = (true === val);
		}

		/**
		 * Sort List
		 *
		 * @param {('asc'|'desc')} direction	optional direction to sort. otherwise {@link ListSorter#direction} will be used
		 * 
		 * @memberof ListSorter
		 */
		sort(direction) {
			
            let self = this,
                o = self.options,
				element = self.element,
				sel = o.selectors,
                list = Selectors.q(sel.list, element),
				sorted, i, params, e, e2;
			if(self.sorting) {
				return false;
			}
			self.sorting = true;
			self.direction = direction || self.direction;
			params = {
				list: list,
				self: self
			};
			e = AcmsEvent.createCustom(ListSorter.EVENT_SORTING, params);
            AcmsEvent.dispatch(element, e);
            if (e.defaultPrevented) {
				self.sorting = false;
                return;
            }
			sorted = self[self.direction]();
            for(i = 0; i < sorted.length; i++) {
                list.appendChild(sorted[i]);
            }
            
            e2 = AcmsEvent.createCustom(ListSorter.EVENT_SORTED, params);
			AcmsEvent.dispatch(element, e2);
			
			self.sorting = false;
		}

		/**
		 * Sort Asc
		 *
		 * @returns {Array} Array of List Elements
		 * @memberof ListSorter
		 */
		asc() {
			let self = this, 
				o = self.options,
				element = self.element,
				sel = o.selectors,
                list = Selectors.q(sel.list, element),
                items = Selectors.qa(sel.item, list),
				t1, t2;
            let asc = items.sort((a, b) => {
				switch(self.source) {
					case 'data':
						t1 = a.acmsData(self.attribute);
						t2 = b.acmsData(self.attribute);
						break;
					case 'html':
						t1 = a.innerHTML;
						t2 = b.innerHTML;
						break;
					case 'text':
						t1 = a.textContent;
						t2 = b.textContent;
						break;
				}
                
                if(isNumber(t1) || isNumber(t2)) {
                    return self._sortNumber(t1, t2);
                } else if(t1) {
                    return self._sortText(t1, t2);
                }
                return 0;
            });
            return asc;
		}

		/**
		 * Sort DESC
		 *
		 * @returns {Array} Array of List Elements
		 * @memberof ListSorter
		 */
		desc() {
			let sorted = this.asc();
            return sorted.reverse();
		}

		listen() {
			var self = this,
                o = self.options,
				element = self.element, sel = o.selectors;
			// asc/desc sorter
			AcmsEvent.on(element, 'click', 'button'+sel.desc, self._onClickSortDesc, true);
			AcmsEvent.on(element, 'click', 'button'+sel.asc, self._onClickSortAsc, true);
			AcmsEvent.on(element, 'change', 'select'+sel.desc, self._onClickSortDesc, true);
			AcmsEvent.on(element, 'change', 'select'+sel.asc, self._onClickSortAsc, true);
			AcmsEvent.on(element, 'change', 'radio'+sel.desc, self._onClickSortDesc, true);
			AcmsEvent.on(element, 'change', 'radio'+sel.asc, self._onClickSortAsc, true);
			// sort attribute changer
			AcmsEvent.on(element, 'click', 'button'+sel.changer, self._onClickSortChanger, true);
			AcmsEvent.on(element, 'change', 'select'+sel.changer, self._onClickSortChanger, true);
			AcmsEvent.on(element, 'change', 'radio'+sel.changer, self._onClickSortChanger, true);

		}

		/**
		 * @inheritdoc 
		 * @override
		 * 
		 * @param {Object|null} 	options	Optional custom options
		 * 
		 * @returns {Object}	options
		 * 
		 * @memberof ListSorter
		 */
		buildOptions(options) {
			let o = super.buildOptions(options);
			if(typeof o.selectors === 'string') {
				o.selectors = JSON.parse(o.selectors);
			}

			return o;
		}

		/**
		 * On Click "sort asc" button Event Handler
		 *
		 * @param {Event} e		Click/change Event
		 * 
		 * @memberof ListSorter
		 */
		_onClickSortAsc(e) {
			e.preventDefault();
			let self = this, 
				o = self.options, 
				element = self.element,
				sel = o.selectors,
				descs = Selectors.qa(sel.desc, element),
				ascs = Selectors.qa(sel.asc, element);
			self.sort(ListSorter.ASC);
			descs.forEach((item) => {
				Classes.removeClass(item, sel.active);
			});
			ascs.forEach((item) => {
				Classes.addClass(item, sel.active);
			});
		}
		
		/**
		 * On Click "sort desc" button Event Handler
		 *
		 * @param {Event} e		Click/change Event
		 * 
		 * @memberof ListSorter
		 */
		_onClickSortDesc(e) {
			e.preventDefault();
			let self = this, 
				o = self.options, 
				element = self.element,
				sel = o.selectors,
				descs = Selectors.qa(sel.desc, element),
				ascs = Selectors.qa(sel.asc, element);
			self.sort(ListSorter.DESC);
			descs.forEach((item) => {
				Classes.addClass(item, sel.active);
			});
			ascs.forEach((item) => {
				Classes.removeClass(item, sel.active);
			});
		}

		/**
		 * on Click Sort-Changer Event listener
		 *
		 * @param {Event} e		Click/Change Event
		 * 
		 * @memberof ListSorter
		 */
		_onClickSortChanger(e) {
			let self = this,
				o = self.options,
				element = self.element,
				sel = o.selectors,
				target = e.target,
				attribute, eles, attr;
			if(!Selectors.matches(target, sel.changer)) {
				target = Selectors.closest(target, sel.changer);
				if(!target) {
					return;
				}
			}
			attribute = target.value || target.acmsData('attribute');
			self.attribute = attribute;
			self.sort();
			eles = Selectors.qa(sel.changer, element);
			eles.forEach((item) => {
				attr = item.value || item.acmsData('attribute');
				if(attr == attribute) {
					Classes.addClass(item, sel.active);
				} else {
					Classes.removeClass(item, sel.active);
				}
			});
		}
		
        /**
		 * Sort Text
		 *
		 * @param {String} a
		 * @param {String} b
		 * 
		 * @returns {Number}
		 * 
		 * @memberof ListSorter
		 */
		_sortText(a, b) {
            return a.localeCompare(b);
		}
		
		/**
		 * Sort Number
		 *
		 * @param {Number} a	Number to compare
		 * @param {Number} b	Number 2 to compare
		 * 
		 * @returns {Number}	0, 1 or -1
		 * 
		 * @memberof ListSorter
		 */
		_sortNumber(a, b) {
			a = parseInt(a);
            b = parseInt(b);
            if(a < b) {
                return -1;
            } else if(a > b) {
                return 1;
            } else {
                return 0;
            }
		}
        
		/**
		 * prepare module
		 *
		 * @memberof ListSorter
		 */
		_prepare() {
			let self = this, 
				o = self.options;
			self.source = o.source;
			self.direction = o.direction;
			self.attribute = o.attribute;
		}

	}

	return ListSorter;
});
