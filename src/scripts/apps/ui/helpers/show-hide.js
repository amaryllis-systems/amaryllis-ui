/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('apps/ui/helpers/show-hide', [], () => {
    
    /**
     * Show/Hide tool to show/hide HTML Elements
     *
     * @class
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class ShowHide {

        /**
         * Show Elements
         *
         * @static
         * 
         * @param {HTMLElement|Array} elements
         * 
         * @memberof ShowHide
         */
        static show(elements) {
            if (elements && !elements.length) {
                return this._show(elements);
            }
            for (var i = 0; i < elements.length; ++i) {
                this._show(elements[i]);
            }
        }

        /**
         * Hide Elements
         *
         * @static
         * @param {HTMLElement|Array} elements
         * 
         * @memberof ShowHide
         */
        static hide(elements) {
            if (elements && !elements.length) {
                return this._hide(elements);
            }
            for (var i = 0; i < elements.length; ++i) {
                this._hide(elements[i]);
            }
        }

        static _show(element) {
            element.style.opacity = 1;
            element.style.display = 'block';
            element.style.visibility = 'visible';
            element.setAttribute('aria-hidden', 'false');
        }

        static _hide(element) {
            element.style.opacity = 0;
            element.style.display = 'none';
            element.style.visibility = 'hidden';
            element.setAttribute('aria-hidden', 'true');
        }

    }
    
    return ShowHide;

});

