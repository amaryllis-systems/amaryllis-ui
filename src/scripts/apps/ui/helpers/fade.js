/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('apps/ui/helpers/fade', [
    'core/style'
], (Style) => {
    
    /**
     * Fading
     * 
     * Fade in/out elements
     *
     * @class
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class Fade {

        /**
         * Fade in
         *
         * @static
         * 
         * @param {HTMLElement}         element     Element to fade in
         * @param {Number}              interval    Fade interval. Defaults to 16 if omitted
         * @param {CallableFunction}    cb          optional on done Callback
         * 
         * @memberof Fade
         */
        static fadeIn(element, interval, cb) {
            if (+element.style.opacity < 1) {
                interval = interval || 16;
                element.style.opacity = 0;
                element.style.display = 'block';
                let last = +new Date();
                let tick = (function (_tick) {
                    function tick() {
                        return _tick.apply(this, arguments);
                    }

                    tick.toString = () => {
                        return _tick.toString();
                    };

                    return tick;
                })(() => {
                    element.style.opacity = +element.style.opacity + (new Date() - last) / 100;
                    last = +new Date();

                    if (+element.style.opacity < 1) {
                        setTimeout(tick, interval);
                    }
                });
                tick();
            }
            element.setAttribute('aria-hidden', false);
            element.style.display = 'block'; //fallback IE8
            if(cb) cb();
        }

        /**
         * Fade out
         *
         * @static
         * 
         * @param {HTMLElement}         element     Element to fade out
         * @param {Number}              interval    Fade interval. Defaults to 16 if omitted
         * @param {CallableFunction}    cb          optional on done Callback
         * 
         * @memberof Fade
         */
        static fadeOut(element, interval, cb) {
            interval = interval || 16;
            element.style.opacity = 1;
            let last = +new Date();
            let tick = ((_tick2) => {
                function tick() {
                    return _tick2.apply(this, arguments);
                }

                tick.toString = () => {
                    return _tick2.toString();
                };

                return tick;
            })(() => {
                element.style.opacity = +element.style.opacity - (new Date() - last) / 100;
                last = +new Date();

                if (+element.style.opacity > 0) {
                    setTimeout(tick, interval);
                } else {
                    element.style.display = 'none';
                    element.setAttribute('aria-hidden', true);
                    if(cb) cb();
                }
            });
            tick();
        }
        
        /**
         * Fade To
         * 
         * @param {Node} element        Das Element
         * @param {Integer} rate        Delay Time
         * @param {Integer} fadeToVal   Opacity
         * @param {string} display  Display nach ende ('block', 'inlineBlock')
         * 
         */
        static fadeTo(element, rate, fadeToVal, display, cb) {
            let accuracy = 3; //    Decimals
            let assumedFps = 1 / 60; //    assumed frames per second
                assumedFps = assumedFps.toPrecision(1 + accuracy);
            element.style.opacity = Style.getStyle(element, 'opacity');
            (function fade() {
                let increments = assumedFps / rate;
                if (element.style.opacity > fadeToVal) {
                    if ((element.style.opacity -= increments) < fadeToVal) {
                        if (element.style.opacity <= 0.01 && display) {
                            Style.setStyle(element, 'display', display);
                            if(cb) cb();
                        }
                    } else {
                        requestAnimationFrame(element);
                    }
                } else {
                    let val = parseFloat(element.style.opacity);
                    if (((val += increments) < fadeToVal)) {
                        element.style.opacity = val;
                        if (element.style.opacity > 0.99 && display) 
                            Style.setStyle(element, 'display', display);
                        requestAnimationFrame(element);
                    }
                }
            })();
        }
    }

    return Fade;

});
