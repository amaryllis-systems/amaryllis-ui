/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/ui/helpers/scroll-to", [
    "core/base",
    "core/classes",
    "core/selectors",
    "events/event"
], (Base, Classes, Selectors, AcmsEvent) => {

    window.requestAnimFrame = (() => {
        return window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            ((callback) => {
                window.setTimeout(callback, 1000 / 60);
            });
    })();

    /**
     * Scroll To Helper
     * 
     * The ScrollTo component adds an event listener for 
     * the click event. When the item is clicked, the 
     * component searches for the target and scrolls 
     * to the item based on the configuration options.
     * 
     * The ScrollTo component works with almost all 
     * elements as long as you set the data attribute 
     * "target" or have a link with a "href" attribute.
     * data attributes are always preferred.
     *
     * __Examples__:
     * 
     * Simple Example by using the module direclty on an element
     * 
     * ``` html
     * <a href="#target1" data-module="apps/ui/helpers/scroll-to" data-speed="1500">Anchor</a>
     * <!--
     *      ... some more contents ...
     *  -->
     * <div id="target1">Target</div>
     * ```
     * 
     * Setup for a link collection
     * 
     * ``` html 
     * <nav data-module="apps/ui/helpers/scroll-to" data-multiple="true">
     *      <a href="#target1" data-role="scrollto" data-speed="1500">Anchor</a>
     *      <button data-target="#target2" data-role="scrollto">Button</button>
     * </nav>
     *  <!--
     *      ... some more contents ...
     *  -->
     *  <div id="target1">Target</div>
     *  <!--
     *      ... some more contents ...
     *  -->
     *  <div id="target2">Target 2</div>
     * ```
     * 
     * The target can be overwritten by giving an anchor a data attribute "target". data 
     * attributes are always preferred.
     * 
     * ``` html
     *  <a href="#target1" data-target="#target2" data-role="scrollto" data-speed="1500">Anchor</a>
     *  <!--
     *      ... some more contents ...
     *  -->
     *  <div id="target1">Link-Target</div>
     *  <div id="target2">real Target</div>
     * ```
     *
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class ScrollTo extends Base {

        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof ScrollTo
         */
        static get NS() {
            return "acms.apps.ui.helpers.scroll-to";
        }

        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof ScrollTo
         */
        static get MODULE() {
            return "Scroll to";
        }

        /**
         * Default Options
         *
         * @readonly
         * @static
         * @memberof ScrollTo
         */
        static get DEFAULT_OPTIONS() {
            return {
                target: null,
                easing: "easeInOutQuint",
                speed: 1500,
                highlight: 'bg-info',
                highlightTime: 2000,
                attr: 'scrollto',
                multiple: false
            };
        }

        /**
         * Module Init
         *
         * @static
         * @param {HTMLElement} element
         * @param {Object} options
         * 
         * @returns {ScrollTo} New instance
         * 
         * @memberof ScrollTo
         */
        static init(element, options) {
            let m = new ScrollTo(element, options);
            m.element.acmsData(ScrollTo.NS, m);
            
            return m;
        }

        /**
         * Creates an instance of ScrollTo.
         * 
         * @param {HTMLElement}         element     An Element to apply the scroll-to listener to.
         * @param {Object}              options
         * @param {String|null}         options.target  Target Element ID (e.g. `#my-elem`)
         * @param {('easeInOutQuint'|'easeInOutSine'|'easeOutSine')}      [options.easing=easeInOutQuint]
         *                              Easing Method
         * @param {Number}              [options.speed=1500] Scroll speed in `ms`
         * @param {String}              [options.highlight=bg-info]     Class name for target-highlighting
         * @param {Number}              [options.highlightTime=2000]    Highlight time in `ms`
         * 
         * @memberof ScrollTo
         */
        constructor(element, options) {
            super(element, options);
            this._initialize();

        }

        /**
         * Find position of element
         *
         * @param {HTMLElement} obj     Element
         * 
         * @returns {Array}
         * 
         * @memberof ScrollTo
         */
        findPos(obj) {
            let curtop = 0;
            if (obj.offsetParent) {
                do {
                    curtop += obj.offsetTop;
                } while (obj = obj.offsetParent);
                return [curtop];
            }
        }

        /**
         * Scroll to element target
         * 
         * Starts the scroll of window to start the scroll animation
         * 
         * @param {HTMLElement} $target     Target element
         * @memberof ScrollTo
         */
        scrollTo(target) {
            window.scroll(0, this.findPos(target));
        }

        /**
         * Scroll To
         *
         * @param {HTMLElement} target
         * 
         * @memberof ScrollTo
         * @private
         */
        _scrollTo(target) {
            const self = this, o = self.options;

            function scrollToY(scrollTargetY, speed, easing) {
                const scrollY = window.scrollY || document.documentElement.scrollTop;
                scrollTargetY = scrollTargetY || 0;
                speed = speed || 2000;
                easing = easing || 'easeOutSine';
                let currentTime = 0;
                const time = Math.max(0.1, Math.min(Math.abs(scrollY - scrollTargetY) / speed, 0.8));
                const easingEquations = {
                    easeOutSine(pos) {
                        return Math.sin(pos * (Math.PI / 2));
                    },
                    easeInOutSine(pos) {
                        return (-0.5 * (Math.cos(Math.PI * pos) - 1));
                    },
                    easeInOutQuint(pos) {
                        if ((pos /= 0.5) < 1) {
                            return 0.5 * Math.pow(pos, 5);
                        }
                        return 0.5 * (Math.pow((pos - 2), 5) + 2);
                    }
                };

                // add animation loop
                function tick() {
                    currentTime += 1 / 60;

                    const p = currentTime / time;
                    const t = easingEquations[easing](p);

                    if (p < 1) {
                        requestAnimFrame(tick);

                        window.scrollTo(0, scrollY + ((scrollTargetY - scrollY) * t));
                    } else {
                        window.scrollTo(0, scrollTargetY);
                        if (typeof o.onDone === 'function') {
                            o.onDone();
                        }
                        if (o.highlight) {
                            Classes.addClass(target, o.highlight);
                            setTimeout(() => {
                                Classes.removeClass(target, o.highlight);
                            }, (parseInt(o.highlightTime) || 2000));
                        }
                    }
                }
                tick();
            }
            scrollToY(self.findPos(target), (parseInt(o.speed) || 1500), o.easing);
        }

        /**
         * Aufsetzen des Click-Eventlistener
         *
         * Basierend auf der "target" Option mit Fallback auf das Link Attribut
         * wird das Target ermittelt und bei click die _scrollTo Methode 
         * aufgerufen. Ist das Target nicht definiert (weder durch eine "target"
         * Option noch durch ein "href" Attribut) oder wird das Target nicht
         * gefunden, wird der Vorgang abgebrochen und die normale Aktion des
         * Elements wird weitergeführt.
         *
         * @memberof ScrollTo
         * @private
         * 
         */
        listen() {
            const self = this;
            const element = self.element;
            const o = self.options;
            if(!o.multiple) {
                AcmsEvent.add(element, 'click', self._onClick);
            } else {
                AcmsEvent.on(element, 'click', '[data-role=scrollto]', self._onClick, true);
            }
        }

        /**
         * On Click handler
         *
         * @param {Event} e Click Event
         * 
         * @memberof ScrollTo
         * @private
         */
        _onClick(e) {
            const self = this;
            const o = self.options;
            const element = self.element;
            const curr = e.target;
            let target;
            let $target;
            AcmsEvent.stop(e);
            if(o.multiple) {
                target = curr.acmsData('target') || curr.getAttribute('href');
                if(!target) {
                    let tmp = Selectors.closest(curr, '[data-target]');
                    if(!tmp) {
                        tmp = Selectors.closest(curr, 'a[href]');
                        if(!tmp) return;
                    }
                    target = tmp.acmsData('target') || tmp.getAttribute('href');
                }
            } else {
                target = o.target || element.getAttribute('href');
            }
            $target = Selectors.q(target);
            if (!$target) {
                return;
            }

            self._scrollTo($target);
        }

    }

    return ScrollTo;
});
