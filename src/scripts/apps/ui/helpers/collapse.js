/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('apps/ui/helpers/collapse', [
    'core/base',
    "core/classes",
    'core/selectors',
    'events/event',
    'core/acms'
], (Base, Classes, Selectors, AcmsEvent, A) => {

    /**
     * Collapse 
     * 
     * Hilfs-Tool für die UI zum Expandieren
     *
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class Collapse extends Base {


        /**
         * Modul Name
         *
         * @var {String}
         */
        static get MODULE() {
            return 'Collapsible Tool';
        }

        /**
         * Modul Namespace
         *
         * @var {String}
         */
        static get NS() {
            return 'acms.ui.tools.collapse';
        }

        /**
         * Event "show"
         *
         * @var {String}
         */
        static get EVENT_SHOW() {
            return Collapse.NS + '.show';
        }

        /**
         * Event "shown"
         *
         * @var {String}
         */
        static get EVENT_SHOWN() {
            return Collapse.NS + '.shown';
        }

        /**
         * Event "hide"
         *
         * @var {String}
         */
        static get EVENT_HIDE() {
            return Collapse.NS + '.hide';
        }

        /**
         * Event "hidden"
         *
         * @var {String}
         */
        static get EVENT_HIDDEN() {
            return Collapse.NS + '.hidden';
        }

        /**
         * Event "enable"
         *
         * @var {String}
         */
        static get EVENT_ENABLE() {
            return Collapse.NS + '.enable';
        }

        /**
         * Event "enabled"
         *
         * @var {String}
         */
        static get EVENT_ENABLED() {
            return Collapse.NS + '.enabled';
        }

        /**
         * Event "disable"
         *
         * @var {String}
         */
        static get EVENT_DISABLE() {
            return Collapse.NS + '.disable';
        }

        /**
         * Event "enabled"
         *
         * @var {String}
         */
        static get EVENT_DISABLED() {
            return Collapse.NS + '.disabled';
        }

        /**
         * Standard Optionen
         *
         * @var {Object}
         */
        static get DEFAULT_OPTIONS() {
            return {
                target: null,
                parent: null,
                duration: 1,
                durationHide: 1,
                onShow(target, element, self) {},
                onHide(target, element, self) {},
                onEnable(target, element, self) {},
                onDisable(target, element, self) {},
                effect: 'default',
                icon: '.acms-icon',
                iconCollapsed: 'acms-icon-caret-square-left',
                iconExpanded: 'acms-icon-caret-square-down',
                iconRotate: false
            };
        }

        static get needInit() {
            return true;
        }

        static init(element, options) {
            let c = new Collapse(element, options);
            c.element.acmsData(Collapse.NS, c);

            return c;
        }

        constructor(element, options) {
            super(element, options);
            this._initialize();
        }

        /**
         * Bildet die Optionen
         *
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions(options) {
            let o = super.buildOptions(options);
            if (o.iconRotate === 'false' || o.iconRotate === false || o.iconRotate === '0' || parseInt(o.iconRotate) === 0) {
                o.iconRotate = false;
            }
            o.duration = parseInt(o.duration) * 1000;
            o.durationHide = parseInt(o.durationHide) * 1000;

            return o;
        }

        /**
         * Zeigt oder versteckt ein Element, je  nach aktuellem Status
         */
        toggle() {
            return (this.isCollapsed()) ? this.show() : this.hide();
        }

        /**
         * Expandiert eine Collapsible Ziel
         *
         * 
         */
        show() {
            let self = this,
                o = self.options,
                element = self.element,
                target = self.target;
            if (false === self.isCollapsed() ||
                true === self.disabled ||
                true === self.collapsing) {
                return;
            }
            self.collapsing = true;
            let param = {
                'module': self,
                'relatedTarget': self.target,
                'trigger': self.element
            };
            let e = AcmsEvent.createCustom(Collapse.EVENT_SHOW, param);
            let e2 = AcmsEvent.createCustom(Collapse.EVENT_SHOWN, param);
            AcmsEvent.dispatch(element, e);
            AcmsEvent.dispatch(target, e);

            Classes.addClass(element, 'expanded');
            element.setAttribute('aria-expanded', 'true');
            Classes.addClass(self.target, 'expanded');
            self.target.setAttribute('aria-expanded', 'true');
            if (self.icon) {
                if (o.iconRotate !== false) {
                    Classes.toggleClass(self.icon, o.iconRotate);
                } else {
                    Classes.toggleClass(self.icon, o.iconCollapsed);
                    Classes.toggleClass(self.icon, o.iconExpanded);
                }
            }

            switch (o.effect) {
                case 'fade':
                    require(["apps/ui/helpers/fade"], (Fade) => {
                        Fade.fadeIn(self.target, o.duration);
                        if (typeof o.onShow === "function") {
                            o.onShow(target, element, self);
                        }
                    });
                    break;
                case 'slide':
                case 'default':
                default:
                    setTimeout(() => {
                        Classes.addClass(self.target, 'showing');
                        Classes.removeClass(self.target, 'hiding');
                        AcmsEvent.dispatch(element, e2);
                    }, o.durationHide);

                    if (typeof o.onShow === "function") {
                        o.onShow(target, element, self);
                    }
                    break;
            }
            self.collapsed = false;
            self.collapsing = false;

        }

        /**
         * Zusammenziehen/verstecken eines Collapsible Ziels
         *
         * 
         */
        hide() {
            let self = this,
                o = self.options,
                element = self.element,
                target = self.target;
            if (true === self.collapsed || true === self.collapsing) {
                return;
            }
            self.collapsing = true;
            let params = {
                'module': self,
                'relatedTarget': self.target,
                'trigger': self.element
            };
            let e = AcmsEvent.createCustom(Collapse.EVENT_HIDE, params);
            let e2 = AcmsEvent.createCustom(Collapse.EVENT_HIDDEN, params);
            AcmsEvent.dispatch(element, e);
            AcmsEvent.dispatch(target, e);
            if (e.defaultPrevented) {
                return;
            }
            Classes.toggleClass(self.element, 'expanded');
            self.element.setAttribute('aria-expanded', 'false');
            Classes.removeClass(self.target, 'expanded');
            self.target.setAttribute('aria-expanded', 'false');
            if (self.icon) {
                if (o.iconRotate !== false) {
                    Classes.toggleClass(self.icon, o.iconRotate);
                } else {
                    Classes.toggleClass(self.icon, o.iconCollapsed);
                    Classes.toggleClass(self.icon, o.iconExpanded);
                }
            }

            switch (o.effect) {
                case 'fade':
                    require(["apps/ui/helpers/fade"], function (Fade) {
                        Fade.fadeOut(target, o.durationHide);
                        if (typeof o.onHide === "function") {
                            o.onHide(target, element, self);
                            AcmsEvent.dispatch(element, e2);
                            AcmsEvent.dispatch(target, e2);
                        }
                    });
                    break;
                case 'default':
                case 'slide':
                    setTimeout(function () {
                        Classes.addClass(target, 'hiding');
                    }, o.durationHide);
                    Classes.removeClass(target, 'showing');
                    if (typeof o.onHide === "function") {
                        o.onHide(target, element, self);
                        AcmsEvent.dispatch(element, e2);
                        AcmsEvent.dispatch(target, e2);
                    }
                    break;
            }
            self.collapsed = true;
            self.collapsing = false;
        }

        /**
         * Reaktiviert ein deaktiviertes Element
         * 
         * 
         */
        enable() {
            let self = this,
                o = self.options,
                element = self.element,
                target = self.target;
            if (false === self.disabled) {
                return;
            }
            let params = {
                'module': self,
                'relatedTarget': self.target,
                'trigger': self.element
            };
            let e = AcmsEvent.createCustom(Collapse.EVENT_ENABLE, params);
            AcmsEvent.dispatch(element, e);
            AcmsEvent.dispatch(target, e);
            self.disabled = false;
            element.setAttribute('aria-disabled', 'false');
            Classes.removeClass(element, 'disabled');

            if (element.tagName.toUpperCase() === 'BUTTON') {
                element.setAttribute('disabled', false);
            }
            if (typeof o.onEnable === 'function') {
                o.onEnable(target, element, self);
            }
            let ev = AcmsEvent.createCustom(Collapse.EVENT_ENABLED, params);
            AcmsEvent.dispatch(element, ev);
            AcmsEvent.dispatch(target, ev);
        }

        /**
         * Deaktiviert/Verbietet ein Element
         *
         * 
         */
        disable() {
            let self = this,
                o = self.options,
                element = self.element,
                target = self.target;
            if (true === self.disabled) {
                return;
            }
            let params = {
                'module': self,
                'relatedTarget': self.target,
                'trigger': self.element
            };
            let e = AcmsEvent.createCustom(Collapse.EVENT_DISABLE, params);
            AcmsEvent.dispatch(element, e);
            AcmsEvent.dispatch(target, e);
            self.disabled = true;
            element.setAttribute('aria-disabled', 'true');
            Classes.addClass(element, 'disabled');
            if (element.tagName.toUpperCase() === 'BUTTON') {
                element.setAttribute('disabled', 'disabled');
            }
            if (false === self.isCollapsed()) {
                self.hide();
            }
            if (typeof o.onDisable === 'function') {
                o.onDisable(target, element, self);
            }
            let ev = AcmsEvent.createCustom(Collapse.EVENT_DISABLED, params);
            AcmsEvent.dispatch(element, ev);
            AcmsEvent.dispatch(target, ev);
        }

        /**
         * Prüft, ob das Element zusammengeklappt ist
         *
         * @returns {Boolean}
         */
        isCollapsed() {
            return Classes.hasClass(this.element, 'expanded') ? false : true;
        }

        /**
         * Prüft, ob das Element deaktiviert/verboten ist
         *
         * @returns {Boolean}
         */
        isDisabled() {
            let self = this;
            if (Classes.hasClass(self.element, 'disabled')) {
                return true;
            }
            if (self.element.getAttribute('disabled')) {
                return true;
            }
            return false;
        }

        listen() {
            let self = this,
                element = self.element,
                parent = self.$parent;
            AcmsEvent.add(element, 'click', self._onClickTrigger);
            if (parent) {
                AcmsEvent.add(parent, Collapse.EVENT_HIDE, self._onHideParent);
            }
        }

        /**
         * On Click Trigger Event Handler
         *
         * @param {Event} e
         */
        _onClickTrigger(e) {
            e.preventDefault();
            e.stopPropagation();
            return this.toggle();
        }

        /**
         * On Hide Parent Event Handler
         *
         * @param {Event} e
         */
        _onHideParent(e) {
            return self.hide(e);
        }

        /**
         * Vorbereiten
         */
        _prepare() {
            let self = this,
                o = self.options,
                element = self.element;
            self.collapsed = self.isCollapsed();
            self.disabled = self.isDisabled();
            self.target = Selectors.q(o.target);
            self.collapsing = false;
            if (self.options.parent) {
                self.$parent = Selectors.closest(element, o.parent);
            } else {
                self.$parent = false;
            }
            self.icon = Selectors.q(o.icon, element);

        }

    }

    return Collapse;
});
