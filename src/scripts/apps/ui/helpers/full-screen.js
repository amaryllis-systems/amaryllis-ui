/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('apps/ui/helpers/full-screen', [
    "core/base",
    "core/document",
    "http/cookie",
    "events/event",
    "core/selectors",
    "core/classes",
    "apps/ui/helpers/show-hide"
], (Base, Doc, AcmsCookie, AcmsEvent, Selectors, Classes, ShowHide) => {

    /**
     * Full-Screen Modul
     * 
     * Simpler Trigger für den FullScreen Modus. Öffnet bzw schließt den Vollbild-
     * Modus via button trigger. Als Trigger kann im Prinzip jedes Element verwendet 
     * werden.
     * 
     * Das Modul ist einfach strukturiert und hat entsprechend wenig 
     * Konfigurations-Optionen:
     * 
     * - `inactive` `{String}`  
     *    Der Klassen-Name der CSS Klasse, wenn der FullScreen-Modus deaktiviert 
     *    wurde. (Standard: 'acms-icon-expand'
     * - `active` `{String}`  
     *    Der Klassen-Name der CSS Klasse, wenn der FullScreen-Modus aktiviert 
     *    wurde. (Standard: 'acms-icon-compress'
     * - `hide` `{String}`  
     *    Der CSS-Selector der Elemente, die versteckt werden sollen, wenn der 
     *    FullScreen-Modus aktiviert wurde. (Standard: '.hide-on-fullscreen'
     * 
     *
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class FullScreen extends Base {

        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof FullScreen
         */
        static get NS() {
            return "acms.ui.widgets.full-screen";
        }

        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof FullScreen
         */
        static get MODULE() {
            return "FullScreen Mode";
        }

        /**
         * Event "activated"
         *
         * @readonly
         * @static
         * @memberof FullScreen
         */
        static get EVENT_ACTIVATED() {
            return FullScreen.NS + '.activated';
        }

        /**
         * Event "deactivated"
         *
         * @readonly
         * @static
         * @memberof FullScreen
         */
        static get EVENT_DEACTIVATED() {
            return FullScreen.NS + '.deactivated';
        }

        /**
         * FullScreen Apis
         *
         * @readonly
         * @static
         * @memberof FullScreen
         */
        static get APIS() {
            return {
                w3: {
                    enabled: "fullscreenEnabled",
                    element: "fullscreenElement",
                    request: "requestFullscreen",
                    exit: "exitFullscreen",
                    events: {
                        change: "fullscreenchange",
                        error: "fullscreenerror"
                    }
                },
                webkit: {
                    enabled: "webkitIsFullScreen",
                    element: "webkitCurrentFullScreenElement",
                    request: "webkitRequestFullScreen",
                    exit: "webkitCancelFullScreen",
                    events: {
                        change: "webkitfullscreenchange",
                        error: "webkitfullscreenerror"
                    }
                },
                moz: {
                    enabled: "mozFullScreenEnabled",
                    element: "mozFullScreenElement",
                    request: "mozRequestFullScreen",
                    exit: "mozCancelFullScreen",
                    events: {
                        change: "mozfullscreenchange",
                        error: "mozfullscreenerror"
                    }
                },
                ms: {
                    enabled: "msFullscreenEnabled",
                    element: "msFullscreenElement",
                    request: "msRequestFullscreen",
                    exit: "msExitFullscreen",
                    events: {
                        change: "MSFullscreenChange",
                        error: "MSFullscreenError"
                    }
                }
            };
        }

        /**
         * Default Options
         *
         * @readonly
         * @static
         * @memberof FullScreen
         */
        static get DEFAULT_OPTIONS() {
            return {
                active: 'acms-icon-compress',
                inactive: 'acms-icon-expand',
                hide: '.hide-on-fullscreen',
                target: null,
                force: false,
                onToggle: function (enabled, api, self) {},
                beforeActivate: function (ele, api, self) {},
                onActivate: function (ele, api, self) {},
                onDeactivate: function (ele, api, self) {},
            };
        }

        /**
         * Module Init
         *
         * @static
         * 
         * @param {HTMLElement} element     Trigger Element to toggle Fullscreen Mode
         * @param {Object|null} options     Optional custom options
         * 
         * @returns {FullScreen}    New Instance
         * 
         * @memberof FullScreen
         */
        static init(element, options) {
            let f = new FullScreen(element, options);
            f.element.acmsData(FullScreen.NS, f);

            return f;
        }

        /**
         * Creates an instance of FullScreen.
         * 
         * @param {HTMLElement} element     Trigger Element to toggle Fullscreen Mode
         * @param {Object|null} options     Optional custom options
         * 
         * @memberof FullScreen
         */
        constructor(element, options) {
            super(element, options);
            this._initialize();
        }

        set changing(val) {
            this._changing = (val === true);
        }

        get changing() {
            return this._changing;
        }

        /**
         * Is FullScreen Activated?
         *
         * @returns {Boolean}   `true` if fullscreen mode is activated and `false` otherwise
         * @memberof FullScreen
         */
        isActivated() {
            return Doc.fullscreenElement;
        }

        activate() {
            let self = this,
                w3 = this.w3,
                o = self.options,
                api = this.api,
                el = self.target || Doc.documentElement;
            if (self.changing) {
                return;
            }
            self.changing = true;
            let res = o.beforeActivate(self.element, api, self);
            if (res === false) {
                return false;
            }
            AcmsEvent.onetimeEvent(el, api.events.change, function (ev) {
                if (Doc[api.enabled] || Doc[w3.enabled]) {
                    self.activated = true;
                    self.dispatch(FullScreen.EVENT_ACTIVATED, self.element);
                    self._hideElements();
                    o.onActivate(self.element, api, self);
                    document.body.acmsData('fullscreen', 1);
                } else {
                    self.activated = false;
                    self.dispatch(FullScreen.EVENT_DEACTIVATED, self.element);
                    self._showElements();
                    o.onDeactivate(self.element, api, self);
                    document.body.acmsData('fullscreen', 0);
                }
            });
            el.requestFullscreen();
            self.changing = false;
        }

        deactivate() {
            let self = this,
                w3 = this.w3,
                o = self.options,
                api = this.api,
                el = self.target || Doc.documentElement;
            if (self.changing) {
                return;
            }
            self.changing = true;
            AcmsEvent.onetimeEvent(el, api.events.change, function (ev) {
                if (Doc[api.enabled] || Doc[w3.enabled]) {
                    self.activated = true;
                    self.dispatch(FullScreen.EVENT_ACTIVATED, self.element);
                    self._hideElements();
                    document.body.acmsData('fullscreen', 1);
                } else {
                    self.activated = false;
                    self.dispatch(FullScreen.EVENT_DEACTIVATED, self.element);
                    self._showElements();
                    document.body.acmsData('fullscreen', 0);
                }
            });
            Doc.exitFullscreen();
            o.onDeactivate(self.element, api, self);
            self.changing = false;
        }

        toggle() {
            let self = this,
                o = self.options,
                ele = self.target || Doc,
                w3 = this.w3,
                api = self.api,
                enabled = Doc[api.enabled] || Doc[w3.enabled];
            o.onToggle(enabled, api, self);
            if (Doc.fullscreenElement) {
                this.deactivate();
            } else {
                this.activate();
            }
            if (self.target) Classes.toggleClass(self.target, 'in-fullscreen');
        }

        handleError(ele) {
            let self = this,
                w3 = self.w3,
                api = self.api;
            if (!ele) {
                ele = self.element;
            }
            self.dispatch(w3.events.error, ele);
        }

        handleChange(ele) {
            let self = this,
                w3 = self.w3,
                api = self.api;
            if (!ele) {
                ele = self.element;
            }
            Doc[w3.enabled] = Doc[api.enabled];
            Doc[w3.element] = Doc[api.element];
            self.dispatch(w3.events.change, ele);
        }

        /**
         * Auflösen eines Events
         * 
         * @param {String}      name     Event Name
         * @param {HTMLElement} target   Das Element
         * 
         * 
         */
        dispatch(name, target) {
            let self = this,
                prop = {
                    fullscreen: self,
                    relatedTarget: self.element
                };
            let evt = AcmsEvent.createCustom(name, prop);
            AcmsEvent.dispatch(target, evt);
        }

        listen() {
            let self = this,
                w3 = self.w3,
                api = self.api;
            if (!(w3.enabled in Doc) && api) {
                AcmsEvent.add(Doc, api.events.change, self._onChange, false);
                AcmsEvent.add(Doc, api.events.error, self._onError, false);
                Doc[w3.enabled] = Doc[api.enabled];
                Doc[w3.element] = Doc[api.element];
                Doc[w3.exit] = Doc[api.exit];
                Element.prototype[w3.request] = () => {
                    return this[api.request].apply(this, arguments);
                };
            }
            AcmsEvent.add(this.element, "click", self._onClickToggle);
        }

        /**
         * On Error Event Handler
         *
         * @param {Event} e
         * 
         * @memberof FullScreen
         */
        _onError(e) {
            console.log('Error');
            console.log(e);
            return this.handleError(e.target);
        }

        /**
         * On Change Event Handler
         *
         * @param {Event} e     Event
         * 
         * @memberof FullScreen
         */
        _onChange(e) {
            return this.handleChange(e.target);
        }

        /**
         * On Click Toggle Event Handler
         *
         * @param {Event} e     Click Event
         * 
         * @memberof FullScreen
         */
        _onClickToggle(e) {
            e.preventDefault();
            e.stopPropagation();
            this.toggle();
            return false;
        }


        /**
         * Hide all Elements that should not be visible in full screen mode
         *
         * @memberof FullScreen
         */
        _hideElements() {
            let self = this,
                o = self.options,
                toHide, el = self.target || Doc.documentElement,
                i;
            if (o.hide) {
                toHide = Selectors.qa(o.hide, el);
                for (i = 0; i < toHide.length; i++) {
                    ShowHide.hide(toHide);
                }
            }
            if (self.icon) {
                Classes.removeClass(self.icon, o.inactive);
                Classes.addClass(self.icon, o.active);
            }
        }

        /**
         * Make Full-Screen-only-hidden Elements visible
         *
         * @memberof FullScreen
         */
        _showElements() {
            let self = this,
                o = self.options,
                toHide, i, el = self.target || Doc.documentElement;
            if (o.hide) {
                toHide = Selectors.qa(o.hide, el);
                for (i = 0; i < toHide.length; i++) {
                    ShowHide.show(toHide);
                }
            }
            if (self.icon) {
                Classes.removeClass(self.icon, o.active);
                Classes.addClass(self.icon, o.inactive);
            }
        }


        /**
         * Prepare Module
         *
         * @memberof FullScreen
         */
        _prepare() {
            let self = this,
                o = self.options;
            self.w3 = FullScreen.APIS.w3;
            self.icon = self.element.querySelector('i');
            self._findApi();
            if (o.target instanceof HTMLElement) {
                self.target = o.target;
                return;
            }
            if (typeof o.target !== 'string') {
                return;
            }
            self.target = Selectors.q(o.target);
        }

        /**
         * Versucht die Api zu Setzen
         *
         * Je nach Browser wird versucht die passende API zu finden. Gibt es
         * keine ( = FullScreen nicht verfügbar in diesem Browser) wird das
         * Element versteckt.
         *
         *
         * @memberof FullScreen
         */
        _findApi() {
            let self = this,
                vendor;
            for (vendor in FullScreen.APIS) {
                if (FullScreen.APIS[vendor].enabled in Doc) {
                    self.api = FullScreen.APIS[vendor];
                    break;
                }
            }
            if (null === self.api) {
                ShowHide.hide(self.element);
            }
        }

    }

    return FullScreen;

});
