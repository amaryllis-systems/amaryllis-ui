/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/ui/helpers/animation-helper", [], () => {

    /**
     * Set the css animationDuration on an element (el). Leave duration undefined to reset;
     * 
     * @param {Element} el
     * @param {number} [duration] in ms
     * 
     * @memberof AnimationHelper
     * @private
     */
    const setAnimationDuration = (el, duration) => {
        const durationMs = duration ? `${duration}ms` : '';
        el.style['-webkit-animation-duration'] = durationMs;
        el.style['animation-duration'] = durationMs;
    };

    /**
     * Add or remove the 'animated' class and the animation type class.
     * 
     * @param {Element} el
     * @param {String} animationName
     * @param {Boolean} [doAdd=true] - set to false to remove the classes
     * 
     * @memberof AnimationHelper
     * @private
     */
    const setAnimateCssClasses = (el, animationName, doAdd) => {

        ['animated', animationName].forEach(str => {
            el.classList[doAdd !== false ? 'add' : 'remove'](str);
        });

    };

    /**
     * Detect animation event to use
     *
     * @returns {String}
     * 
     * @memberof AnimationHelper
     * @private
     */
    const whichAnimationEvent = () => {
        const el = document.createElement('fakeelement');
        let t;

        const animations = {
            'animation': 'animationend',
            'OAnimation': 'oAnimationEnd',
            'MozAnimation': 'animationend',
            'WebkitAnimation': 'webkitAnimationEnd'
        };

        for (t in animations) {
            if (el.style[t] !== undefined) {
                return animations[t];
            }
        }
    };

    /**
     * Animation Helper
     * 
     * @class
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class AnimationHelper {
        
        /**
         * Animate
         * 
         * @param {Element} el
         * @param {object} opts
         * @property {string} opts.animationName
         * @property {number} [opts.duration]
         * @param {function[]} [opts.callbacks=[]]
         * 
         * @memberof AnimationHelper
         */
        static animate(el, opts) {
            opts.callbacks = opts.callbacks || [];
            const animationEventName = whichAnimationEvent();
            const animEnd = () => {
                el.removeEventListener(animationEventName, animEnd);
                //remove the animate.css classes
                setAnimateCssClasses(el, opts.animationName, false);

                if (opts.duration) {
                    //reset animationDuration
                    setAnimationDuration(el);
                }
                // call the callbacks
                opts.callbacks.forEach(cb => {
                    cb();
                });
                opts.callbacks = [];
            };
            if (opts.duration) {
                setAnimationDuration(el, opts.duration);
            }
            setAnimateCssClasses(el, opts.animationName);
            el.addEventListener(animationEventName, animEnd);
        }
            
        /**
         * Show
         * 
         * @param {Element} el
         * @param {object} [opts]
         * @property {string} [opts.animationName='slideInDown']
         * @property {number} [opts.duration=300]
         * @param {function[]} [opts.callbacks]
         * 
         * @memberof AnimationHelper
         */
        static show(el, opts) {
            opts = opts || {};
            opts.animationName = opts.animationName || 'slideInDown';
            opts.duration = opts.duration || 350;
            el.classList.remove('hidden');
            AnimationHelper.animate(el, opts);
        }
            
        /**
         * Hide 
         * 
         * @param {Element} el
         * @param {object} [opts]
         * @property {string} [opts.animationName='slideInDown']
         * @property {number} [opts.duration=300]
         * @param {function[]} [opts.callbacks]
         * 
         * @memberof AnimationHelper
         */
        static hide(el, opts) {
            opts = opts || {};

            opts.animationName = opts.animationName || 'slideOutUp';
            opts.duration = opts.duration || 300;
            opts.callbacks = opts.callbacks || [];

            //if the element is already hidden
            if (el.classList.contains('hidden')) {
                //call the callbacks directly
                opts.callbacks.forEach(cb => {
                    cb();
                });
                //and get out
                return;
            }

            opts.callbacks.push(() => {
                el.classList.add('hidden');
            });
            AnimationHelper.animate(el, opts);
        }
    }

    return AnimationHelper;
});
