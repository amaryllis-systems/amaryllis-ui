/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("apps/ui/components/panel", [
    "core/base",
    "core/classes",
    "core/selectors",
    "events/event",
    "core/acms"
], (Base, Classes, Selectors, AcmsEvent, A) => {

    /**
     * Panel
     *
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class Panel extends Base {

        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof Panel
         */
        static get NS() {
            return "acms.apps.ui.components.panel";
        }

        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof Panel
         */
        static get MODULE() {
            return "Panel";
        }

        /**
         * Default Options
         *
         * @readonly
         * @static
         * @memberof Panel
         */
        static get DEFAULT_OPTIONS() {
            return {
                /**
                 * On Show Callback
                 * 
                 * @param {HTMLElement}         panel   Das Panel Element
                 * @param {HTMLElement}         content Der Content
                 * @param {ui/components/panel} self    Panel Modul
                 * 
                 * @memberof Panel
                 */
                onShow :  function(panel, content, self) {},
                /**
                 * On Hide Callback
                 * @param {HTMLElement}         panel   Das Panel Element
                 * @param {HTMLElement}         content Der Content
                 * @param {ui/components/panel} self    Panel Modul
                 * 
                 * @memberof Panel
                 */
                onHide :  function(panel, content, self) {},
                target:         'div.content',
                hidden:         'hiding',
                show:           'showing',
                trigger:        '.heading',
                panel:          '.panel',
                css:            'media/css/components/panels',
                themecss:       false
            };
        }

        /**
         * Event "show"
         *
         * @readonly
         * @static
         * @memberof Panel
         */
        static get EVENT_SHOW() {
            return Panel.NS + '.show';
        }
        
        /**
         * Event "shown"
         *
         * @readonly
         * @static
         * @memberof Panel
         */
        static get EVENT_SHOWN() {
            return Panel.NS + '.shown';
        }
        
        /**
         * Event "close"
         *
         * @readonly
         * @static
         * @memberof Panel
         */
        static get EVENT_CLOSE() {
            return Panel.NS + '.close';
        }
        
        /**
         * Event "closed"
         *
         * @readonly
         * @static
         * @memberof Panel
         */
        static get EVENT_CLOSED() {
            return Panel.NS + '.closed';
        }


        /**
         * Module init
         *
         * @static
         * 
         * @param {HTMLElement} element     HTML Panel Element
         * @param {Object}      options     Optionen
         * 
         * @returns {Panel} new instance
         * 
         * @memberof Panel
         */
        static init(element, options) {
            let m = new Panel(element, options);
            m.element.acmsData(Panel.NS, m);
            
            return m;
        }

        /**
         * Creates a new instance
         * 
         * @param {HTMLElement} element     HTML Panel Element
         * @param {Object|null} options     optional custom options
         */
        constructor(element, options) {
            super(element, options);
            this._initialize();
        }

        get toggling() {
            return this._toggling;
        }

        listen() {
            let self = this, o = self.options, element = self.element;
            AcmsEvent.on(element, 'click', o.trigger, self._onClick, true);
        }

        off() {
            let self = this, o = self.options, element = self.element;
            
            AcmsEvent.off(element, 'click', o.trigger, self._onClick);
        }


        /**
         * Ist das Target versteckt?
         *
         * @returns {Boolean}   `true` if the panel body is hidden, `false` otherwise
         * 
         * @memberof Panel
         */
        isHidden() {
            let self = this,
                element = self.element, 
                o = self.options, target = Selectors.q(o.target, element);
            return Classes.hasClass(target, o.hidden);
        }

        /**
         * Show panel body
         *
         * @memberof Panel
         */
        show() {
            let self = this, o = self.options, element = self.element;
            if(false === self.isHidden()) {
                self._toggling = false;
                return false;
            }
            let target = Selectors.q(o.target, element);
            let param = {
                relatedTarget: target,
                panel: self
            },
            e = AcmsEvent.createCustom(Panel.EVENT_SHOW, param);
            AcmsEvent.dispatch(element, e);
            if(e.defaultPrevented) {
                self._toggling = false;
                return;
            }
            
            Classes.removeClass(target, o.hidden);
            Classes.addClass(target, o.show);
            target.setAttribute('aria-hidden', false);
            Classes.removeClass(element, 'collapsed');
            if(typeof o.onShow === "function") {
                o.onShow(element, target, self);
            }
            let e2 = AcmsEvent.createCustom(Panel.EVENT_SHOWN, param);
            AcmsEvent.dispatch(element, e2);
            this._toggling = false;
        }

        /**
         * Hide panel body
         *
         * @memberof Panel
         */
        close() {
            let self = this, o = self.options, element = self.element;
            let target = Selectors.q(o.target, element),
                param = {
                    relatedTarget: target,
                    panel: self
                },
                e = AcmsEvent.createCustom(Panel.EVENT_CLOSE, param);
            if(true === self.isHidden()) {
                self._toggling = false;
                return false;
            }
            AcmsEvent.dispatch(element, e);
            if(e.defaultPrevented) {
                self._toggling = false;
                return;
            }
            
            Classes.addClass(target, o.hidden);
            Classes.removeClass(target, o.show);
            target.setAttribute('aria-hidden', true);
            Classes.addClass(element, 'collapsed');
            if(typeof o.onHide === "function") {
                o.onHide(element, target, self);
            }
            let e2 = AcmsEvent.createCustom(Panel.EVENT_CLOSED, param);
            AcmsEvent.dispatch(element, e2);
            self._toggling = false;
        }

        /**
         * Toggle Panel body visibility
         *
         * @memberof Panel
         */
        toggle() {
            if(this.toggling) {
                return;
            }
            this._toggling = true;
            if(this.isHidden()) {
                this.show();
            } else {
                this.close();
            }
            return false;
        }

        /**
         * On Click Event Handler
         *
         * @param {Event} e Click event
         * 
         * @returns {Boolean} always `false`
         * 
         * @memberof Panel
         */
        _onClick(e) {
            e.preventDefault();
            this.toggle();
            
            return false;
        }

        _prepare() {
            let self = this,
                o = self.options;
            self._toggling = false;
        }

    }

    return Panel;
});
