/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/ui/components/dual-list", [
    "core/base",
    "apps/ui/helpers/show-hide",
    "core/classes",
    "core/selectors",
    "events/event",
    "core/acms"
], (Base, ShowHide, Classes, Selectors, AcmsEvent, A) => {

    /**
     * Dual List
     * 
     * Comfortable List Handling for two-side lists.
     *
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class DualList extends Base {

        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof DualList
         */
        static get NS() {
            return "apps.ui.components.dual-list";
        }

        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof DualList
         */
        static get MODULE() {
            return "Dual List";
        }

        static get EVENT_BEFORE_STATECHANGE() {
            return this.NS + '.state-change';
        }

        static get EVENT_AFTER_STATECHANGE() {
            return this.NS + '.state-changed';
        }
        
        /**
         * "before move left" Event
         * 
         * Event tiggered before moving elements to left list
         *
         * @readonly
         * @static
         * @memberof DualList
         */
        static get EVENT_BEFORE_MOVE_LEFT() {
            return this.NS + '.before-move-left';
        }

        /**
         * "after move left" Event
         * 
         * Event tiggered after moving elements to left list
         *
         * @readonly
         * @static
         * @memberof DualList
         */
        static get EVENT_AFTER_MOVE_LEFT() {
            return this.NS + '.after-move-left';
        }

        /**
         * "before move right" Event
         * 
         * Event tiggered before moving elements to right list
         *
         * @readonly
         * @static
         * @memberof DualList
         */
        static get EVENT_BEFORE_MOVE_RIGHT() {
            return this.NS + '.before-move-right';
        }

        /**
         * "After move right" Event
         * 
         * Event tiggered after moving elements to right list
         *
         * @readonly
         * @static
         * @memberof DualList
         */
        static get EVENT_AFTER_MOVE_RIGHT() {
            return this.NS + '.after-move-right';
        }

        /**
         * Left Side
         *
         * @readonly
         * @static
         * @memberof DualList
         */
        static get SIDE_LEFT() {
            return 'left';
        }

        /**
         * Right Side
         *
         * @readonly
         * @static
         * @memberof DualList
         */
        static get SIDE_RIGHT() {
            return 'right';
        }

        /**
         * Default Options
         *
         * @readonly
         * @static
         * @memberof DualList
         */
        static get DEFAULT_OPTIONS() {
            return {
                selectors: {
                    side: {
                        left: '[data-role="dual-list-left"]',
                        right: '[data-role="dual-list-right"]',
                    },
                    search: '[data-role="dual-list-search"]',
                    state: {
                        active: '.active',
                        disabled: '.disabled',
                        moving: '.moving',
                        selected: '.selected'
                    },
                    move: '[data-role="move-button"]',
                    item: '[data-role="dual-list-item"]',
                    checkall: '[data-role="check-all"]',
                    icons: {
                        unchecked: '.acms-icon-square',
                        checked:    '.acms-icon-check-square',
                        checkedall: '.acms-icon-check-square',
                        uncheckedall: '.acms-icon-square',
                    },
                    list: '.dual-list'
                },
                classNames: {
                    state: {
                        active: 'active',
                        disabled: 'disabled',
                        moving: 'moving',
                        selected: 'selected'
                    },
                    icons: {
                        unchecked: 'acms-icon-square',
                        checked:    'acms-icon-check-square',
                        checkedall: 'acms-icon-check-square',
                        uncheckedall: 'acms-icon-square',
                    }
                },
                /**
                 * BeforeStateChange Event
                 * @param {HTMLElement} item    Das Element
                 * @param {Boolean} isActive    Ob das Item aktuell Aktiv ist
                 * @returns {Boolean}   Wenn nicht TRUE zurückgegeben, wird abgebrochen
                 * @memberof DualList
                 */
                beforeStateChange: function(item, isActive) {return true;},
                /**
                 * AfterStateChange Event
                 * @param {HTMLElement} item       Das gewechselte Element
                 * @param {type} isActive          Der neue Active Status des Item
                 * @memberof DualList
                 */
                afterStateChange: function(item, isActive) {},
                
                beforeMoveToLeft: function(element, items, listLeft, listRight) {return true;},
                afterMoveToLeft: function(element, items, listLeft, listRight) {},
                
                beforeMoveToRight: function(element, items, listLeft, listRight) {return true;},
                afterMoveToRight: function(element, items, listLeft, listRight) {}
            };
        }

        /**
         * Module Init
         *
         * @static
         * @param {HTMLElement} element
         * @param {Object} options
         * @returns {DualList}  New Instance
         * @memberof DualList
         */
        static init(element, options) {
            let m = new DualList(element, options);
            m.element.acmsData(DualList.NS, m);
            
            return m;
        }


        constructor(element, options) {
            super(element, options);
            this._initialize();
        }

        get leftList() {
            return this._listLeft;
        }

        get rightList() {
            return this._listRight;
        }

        listen() {
            let self = this,
                o = self.options,
                sel = o.selectors,
                element = self.element;
            AcmsEvent.on(element, 'click', sel.checkall, self._onClickCheckAll, true);
            AcmsEvent.on(element, 'keyup', sel.search, self._onKeyUp);
            AcmsEvent.on(element, 'click', sel.item, self._onClickItem, true);
            AcmsEvent.on(element, 'click', sel.move, self._onClickMove, true);
        }

        /**
         * On Search Field keyUp
         *
         * @param {Event} e
         * 
         * @memberof DualList
         */
        _onKeyUp(e) {
            let self = this,
                o = self.options,
                sel = o.selectors,
                target = e.target, list, code, j, items, val, item, text;
            list = target.acmsData('side') === DualList.SIDE_RIGHT ? 
                    self.rightList : 
                    self.leftList;
            code = parseInt(e.keyCode || e.which);
            if (code === 9) {
                return;
            }
            if (code === 27) {
                target.value = '';
            }
            val = target.value;
            items = Selectors.qa(sel.item, list);
            val = val.trim().replace(/ +/g, ' ').toLowerCase();
            for(j = 0; j < items.length; j++) {
                item = items[j];
                text = item.acmsData('value').replace(/\s+/g, ' ').toLowerCase();
                if(!~text.indexOf(val)) {
                    ShowHide.hide(item);
                } else {
                    ShowHide.show(item);
                }
            }
        }

        /**
         * On Click "check all" Event Handler
         *
         * @param {Event} e
         * @memberof DualList
         */
        _onClickCheckAll(e) {
            let self = this,
                o = self.options,
                sel = o.selectors,
                cls = o.classNames,
                checkbox = e.target;
            if(!Selectors.matches(checkbox, sel.checkall)) {
                checkbox = Selectors.closest(checkbox, sel.checkall);
            }
            let listType = checkbox.acmsData('list'),
                list = listType.trim() === DualList.SIDE_RIGHT ? self.rightList : self.leftList,
                checked = Classes.hasClass(checkbox, cls.state.selected), j, item, items, icn;
            Classes.toggleClass(checkbox, cls.state.selected);
            
            if (false === checked) {
                items = Selectors.qa(sel.item, list);
                for(j = 0; j < items.length; j++) {
                    item = items[j];
                    if(item.style.visibility == 'hidden' || 
                            Classes.hasClass(item, 'active')) {
                        continue;
                    }
                    AcmsEvent.fireClick(item);
                }
                icn = Selectors.q(sel.icons.uncheckedall, checkbox);
                if(icn) {
                    Classes.removeClass(icn, cls.icons.uncheckedall);
                    Classes.addClass(icn, cls.icons.checkedall);
                }
            } else {
                items = Selectors.qa(sel.state.active, list);
                for(j = 0; j < items.length; j++) {
                    item = items[j];
                    if(item.style.visibility == 'hidden') {
                        continue;
                    }
                    AcmsEvent.fireClick(item);
                }
                icn = Selectors.q(sel.icons.checkedall, checkbox);
                if(icn) {
                    Classes.removeClass(icn, cls.icons.checkedall);
                    Classes.addClass(icn, cls.icons.uncheckedall);
                }
            }
        }

        /**
         * On Click Move Event Handler
         *
         * @param {Event} e     Click Event
         * 
         * @memberof DualList
         */
        _onClickMove(e) {
            let self = this,
                o = self.options,
                sel = o.selectors,
                cls = o.classNames,
                button = e.target,
                evt, j, k, searchs, check, icn;
            e.preventDefault();
            if(!Selectors.matches(button, sel.move)) {
                button = Selectors.closest(button, sel.move);
            }
            if (button.acmsData('direction') === 'left') {
                self._moveFormRightToLeft();

            } else if (button.acmsData('direction') === 'right') {
                self._moveFormLeftToRight();
            }
            evt = AcmsEvent.create('keyup');
            evt.which = evt.keyCode = 8;
            searchs = Selectors.qa(sel.search, self.element);
            for(j = 0; j < searchs.length; j++) {
                searchs[j].value = '';
                AcmsEvent.fireChange(searchs[j]);
                AcmsEvent.dispatch(searchs[j], evt);
            }
            for(k = 0; k < self.checkEmAll.length; k++) {
                check = self.checkEmAll[k];
                icn = Selectors.q(sel.icons.checkedall, check);
                if(icn) {
                    Classes.removeClass(icn, cls.icons.checkedall);
                    Classes.addClass(icn, cls.icons.uncheckedall);
                }
                Classes.removeClass(check, 'selected');
                check.setAttribute('aria-selected', 'false');
            }
            
            return false;
        }

        _onClickItem(e) {
            let self = this,
                element = self.element,
                o = self.options,
                sel = o.selectors,
                cls = o.classNames,
                target = e.target, evt, ret, isActive, isDisabled;
            if(!Selectors.matches(target, sel.item)) {
                target = Selectors.closest(target, sel.item);
            }
            isActive = Classes.hasClass(target, cls.state.active);
            isDisabled = Classes.hasClass(target, cls.state.disabled);
            if(true === isDisabled) {
                return false;
            }
            evt = AcmsEvent.createCustom(DualList.EVENT_BEFORE_STATECHANGE, {active: isActive, element: target});
            AcmsEvent.dispatch(element, evt);
            if(evt.defaultPrevented) {
                return false;
            }
            if(typeof o.beforeStateChange === 'function') {
                ret = o.beforeStateChange(target, isActive);
                if(true !== ret) {
                    return ret;
                }
            }
            Classes.toggleClass(target, cls.state.active);
            self._toggleIconState(target, isActive);
            evt = AcmsEvent.createCustom(DualList.EVENT_AFTER_STATECHANGE, {active: isActive, element: target});
            AcmsEvent.dispatch(element, evt);
            if(typeof o.afterStateChange === 'function') {
                o.afterStateChange(target, !isActive);
            }
        }

        /**
         * Moves active items from right to left list
         *
         * @memberof DualList
         */
        _moveFormRightToLeft() {
            let self = this, 
                element = self.element,
                o = self.options,
                sel = o.selectors,
                cls = o.classNames,
                listRight = self.rightList,
                listLeft = self.leftList,
                actives, res, e, j, active, checkbox;
            actives = Selectors.qa(sel.state.active, self.rightList);
            e = AcmsEvent.createCustom(DualList.EVENT_BEFORE_MOVE_LEFT, {actives: actives});
            AcmsEvent.dispatch(listRight, e);
            let ul = Selectors.q('ul', listLeft);
            if(e.defaultPrevented) {
                return true;
            }
            if(typeof o.beforeMoveToLeft === 'function') {
                res = o.beforeMoveToLeft(element, actives, listLeft, listRight);
                if(true !== res) {
                    return false;
                }
            }
            for(j = 0; j < actives.length; j++) {
                active = actives[j];
                self._toggleIconState(active, true);
                Classes.removeClass(active, cls.state.active);
                checkbox = Selectors.q('input[type=checkbox]', active);
                if(checkbox !== null) {
                    checkbox.checked = false;
                    checkbox.removeAttribute('checked');
                }
                ul.appendChild(active);
            }
            
            e = AcmsEvent.createCustom(DualList.EVENT_AFTER_MOVE_LEFT, {});
            AcmsEvent.dispatch(listLeft, e);
            if(typeof o.afterMoveToLeft === 'function') {
                o.afterMoveToLeft(element, actives, listLeft, listRight);
            }
            
            return true;
        }

        /**
         * Move active items from left to right list
         *
         * @memberof DualList
         */
        _moveFormLeftToRight() {
            let self = this, 
                element = self.element,
                o = self.options,
                sel = o.selectors,
                cls = o.classNames,
                listRight = self.rightList,
                listLeft = self.leftList,
                actives, res, e, j, active, checkbox;
            actives = Selectors.qa(sel.state.active, listLeft);
            e = AcmsEvent.createCustom(DualList.EVENT_BEFORE_MOVE_RIGHT, {actives: actives});
            AcmsEvent.dispatch(listLeft, e);
            if(e.defaultPrevented) {
                return true;
            }
            let ul = Selectors.q('ul', listRight);
            if(typeof o.beforeMoveToRight === 'function') {
                res = o.beforeMoveToRight(element, actives, listLeft, listRight);
                if(true !== res) {
                    return false;
                }
            }
            for(j = 0; j < actives.length; j++) {
                active = actives[j];
                self._toggleIconState(active, true);
                Classes.removeClass(active, cls.state.active);
                checkbox = Selectors.q('input[type=checkbox]', active);
                if(checkbox !== null) {
                    checkbox.checked = true;
                    checkbox.setAttribute('checked', 'checked');
                    AcmsEvent.fireChange(checkbox);
                }
                ul.appendChild(active);
            }
            
            e = AcmsEvent.createCustom(DualList.EVENT_AFTER_MOVE_RIGHT, {});
            AcmsEvent.dispatch(listRight, e);
            
            if(typeof o.afterMoveToRight === 'function') {
                o.afterMoveToRight(element, actives, listLeft, listRight);
            }
        }

        /**
         * Toggle icon State
         *
         * @param {HTMLElement} target  HTML Element
         * @param {Boolean} state   New state
         * 
         * @memberof DualList
         */
        _toggleIconState(target, state) {
            let self = this,
                o = self.options,
                cls = o.classNames,
                sel = o.selectors,
                icn;
            if(false === state) {
                target.setAttribute('aria-selected', 'true');
                icn = Selectors.q(sel.icons.unchecked, target);
                Classes.removeClass(icn, cls.icons.unchecked);
                Classes.addClass(icn, cls.icons.checked);
            } else {
                target.setAttribute('aria-selected', 'false');
                icn = Selectors.q(sel.icons.checked, target);
                Classes.removeClass(icn, cls.icons.checked);
                Classes.addClass(icn, cls.icons.unchecked);
            }
        }

        _prepare() {
            let self = this,
                o = self.options,
                sel = o.selectors,
                element = self.element;
            self._listLeft = Selectors.q(sel.side.left, element);
            self._listRight = Selectors.q(sel.side.right, element);
            self.moveButtons = Selectors.qa(sel.move, element);
            self.checkEmAll = Selectors.qa(sel.checkall, element);
        }

    }

    return DualList;
});
