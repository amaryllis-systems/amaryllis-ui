/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define(
    'apps/ui/navs/context-menu',
    [
        "core/base",
        "core/window",
        "core/document",
        "core/classes",
        "core/selectors",
        "events/event"
    ],
    (Base, win, doc, Classes, Selectors, AcmsEvent) => {

        /**
         * Context Menu
         *
         * Erstellung eines Context-Menus (Rechts-Klick-Menu) in bestimmten HTML Areas.
         *
         * @class
         * @extends {Base}
         * @author qm-b <https://bitbucket.org/qm-b/>
         */
        class ContextMenu extends Base {

            /**
			 * Module Namespace
			 *
			 * @readonly
			 * @static
			 * @memberof ContextMenu
			 */
			static get NS() {
                return "apps.ui.navs.context-menu";
            }

            /**
			 * Module Name
			 *
			 * @readonly
			 * @static
			 * @memberof ContextMenu
			 */
			static get MODULE() {
                return "Context Menu";
            }

            /**
			 * Default Options
			 *
			 * @readonly
			 * @static
			 * @memberof ContextMenu
			 */
			static get DEFAULT_OPTIONS() {
                return {
                    target: '[data-role="context-menu"]',
                    targetitems: null,
                    onOpen: (self, menu) => {},
                    onClose: (self, menu) => {},
					onClick: (self, menu, item) => {},
					selectors: {
						taskitem: '.task',
                        menuitem: '.menu-item',
                        menuactive: '.active',
                        disabled: '.disabled',
                        menulabel: '[data-role="context-menu-label"]'
					},
                    classnames: {
                        taskitem: 'task',
                        menuitem: 'menu-item',
                        menuactive: 'active',
                        disabled: 'disabled'
                    }
                };
            }
			
			/**
			 * Module Init
			 *
			 * @static
			 * 
			 * @param {HTMLElement} element
			 * @param {Object} 		options
			 * 
			 * @memberof ContextMenu
			 */
			static init(element, options) {
                let cm = new ContextMenu(element, options);
                cm.element.acmsData(ContextMenu.NS, cm);
    
                return cm;
            }

            
            /**
             * Menu State
             * 
             * @var {Integer}
             */
            get menuState() {
                return this._menuState || 0;
            }
            
            get menu() {
                return this._menu;
            }

            get menuItems() {
                return this._menuItems;
            }

            /**
             * Aufsetzen der Event Listener
             * 
             */
            listen() {
                let self = this,
                    element = self.element;
                if(self.menu) {
                    AcmsEvent.add(element, 'contextmenu', self._onClickContextMenu);
                    AcmsEvent.add(doc, 'click', self._onClickElement);
                    AcmsEvent.add(win, 'keyup', self._onKeyUp);
                    AcmsEvent.add(win, 'resize', self._onResize);
                    AcmsEvent.add(win, 'orientationchange', self._onResize);
                }
            }

            /**
             * Diese Funktion dient der Überprüfung ob wir uns in einem
             * entsprechenden Ziel-Element befinden
             *
             * @param {Object} e            Das Event
             * @param {String} className    Der Klassen-Name des gesuchten 
             *                              Elements
             * 
             * @return {Element|Boolean}    Das Element oder FALSE
             */
            _clickInsideElement(e, className) {
                let el = e.srcElement || e.target, cl;

                if (Classes.hasClass(el, className)) {
                    return el;
                } else {
                    cl = Selectors.closest(el, '.'+ className);
                    if(cl) {
                        return cl;
                    }
                }

                return false;
            }

			/**
			 * Zeigt das Menu
			 *
			 * @param {Event} e
			 * 
			 * @memberof ContextMenu
			 */
			_toggleMenuOn(e) {
                let self = this, o = self.options, cl = o.classnames;
                if (self.menuState !== 1 ) {
                    self._menuState = 1;
                    Classes.addClass(self.menu, cl.menuactive);
                    self._positionMenu(e);
                    o.onOpen(self, self.menu);
                }
            }

            /**
             * Versteckt das Menu
             *
             * 
             */
            _toggleMenuOff() {
                let self = this, o = self.options, cl = o.classnames;
                if ( self.menuState !== 0 ) {
                    self.menuState = 0;
                    Classes.removeClass(self.menu, cl.menuactive);
                    o.onClose(self, self.menu);
                }
            }

            /**
             * Positioniert das Menu beim Mauszeiger
             * 
             * @param {Object} e      Klick-Event
             * 
             */
            _positionMenu(e) {
                e = e || window.event;
                let self = this, menu = self.menu, st = menu.style,
                    clickCoords = self._getPosition(e),
                    clickCoordsX = clickCoords.x,
                    clickCoordsY = clickCoords.y,
                    menuWidth = menu.offsetWidth + 4,
                    menuHeight = menu.offsetHeight + 4,
                    windowWidth = win.innerWidth,
                    windowHeight = win.innerHeight;

                if ( (windowWidth - clickCoordsX) < menuWidth ) {
                    st.left = windowWidth - menuWidth + "px";
                } else {
                    st.left = clickCoordsX + "px";
                }

                if ( (windowHeight - clickCoordsY) < menuHeight ) {
                    st.top = windowHeight - menuHeight + "px";
                } else {
                    st.top = clickCoordsY + "px";
                }
            }

            /**
             * Gibt die exakte Position des Click-Events zurück
             *
             * Es wird ein objekt mit x als X-Koordinate und y als Y-Koordinate
             * des Klick-Events zurückgegeben.
             *
             * @param {Object} e    Window Event
             *
             * @returns {Object}
             */
            _getPosition(e) {
                let posx = 0,
                    posy = 0,
                    docEl = doc.documentElement,
                    b = doc.body;

                if (!e) e = win.event;

                if (e.pageX || e.pageY) {
                    posx = e.pageX;
                    posy = e.pageY;
                } else if (e.clientX || e.clientY) {
                    posx = e.clientX + b.scrollLeft + docEl.scrollLeft;
                    posy = e.clientY + b.scrollTop + doc.docEl.scrollTop;
                }

                return {
                    x: posx,
                    y: posy
                };
            }
            
            /**
             * Context Menu Event Handler
             * 
             * @param {Event} e Das Event
             * 
             * 
             */
            _onClickContextMenu(e) {
                let self = this;
                e.preventDefault();
                self._toggleMenuOn(e);
            }
            
            
            /**
             * klick Event Handler
             *
             * @param {Event} e Das Event
             * 
             * 
             */
            _onClickElement(e) {
                let self = this, o = self.options, cl = o.classnames,
                    clickEl = self._clickInsideElement(e, cl.menuitem),
                    res;
                console.log(clickEl);
                if(clickEl) {
                    e.preventDefault();
                    if(!Classes.hasClass(clickEl, cl.disabled)) {
                        res = o.onClick(self, self.menu, clickEl);
                        if(res !== false) {
                            self._toggleMenuOff();
                        }
                    }
                } else {
                    let button = parseInt(e.which || e.button);
                    console.log('Button: ' + button);
                    if ( button === 1 ) {
                        self._toggleMenuOff();
                    }
                }
            }
            
            /**
             * Key Up Event Handler
             * 
             * @param {Event} e Das Event
             * 
             */
            _onKeyUp(e) {
                let self = this;
                if(e.keyCode === 27) {
                    self._toggleMenuOff();
                }
            }
            
            /**
             * Resize/Orientation Change Event Handler
             * 
             * @param {Event} e Das Event
             * 
             */
            _onResize() {
                let self = this;
                self._toggleMenuOff();
            }
            
            /**
             * Vorbereiten
             * 
             * 
             */
            _prepare() {
                let self = this, o = self.options, cl = o.classnames;
                self._menu = Selectors.q(o.target);
                self._menuItems = Selectors.qa('.'+cl.menuitem, self.menu);
                self._menuState = 0;
            }
        }
        
    return ContextMenu;
    
});
