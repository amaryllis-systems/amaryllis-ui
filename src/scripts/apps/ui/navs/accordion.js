/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('apps/ui/navs/accordion', [
	"core/base",
	'core/selectors',
	'core/classes',
	'events/event'
], (Base, Selectors, Classes, AcmsEvent) => {

	/**
	 * Accordion Navigation
	 *
	 * @class
	 * @extends {Base}
	 * @author qm-b <https://bitbucket.org/qm-b/>
	 */
	class Accordion extends Base {

		/**
		 * Module Name
		 *
		 * @readonly
		 * @static
		 * @memberof Accordion
		 */
		static get MODULE() {
			return "Accordion Menu";
		}

		/**
		 * Module Namespace
		 *
		 * @readonly
		 * @static
		 * @memberof Accordion
		 */
		static get NS() {
			return "acms.apps.ui.navs.accordion";
		}

		/**
		 * Event "Open"
		 *
		 * @readonly
		 * @static
		 * @memberof Accordion
		 */
		static get EVENT_OPEN() {
			return Accordion.NS + '.open';
		}

		/**
		 * Event "Opened"
		 *
		 * @readonly
		 * @static
		 * @memberof Accordion
		 */
		static get EVENT_OPENED() {
			return Accordion.NS + '.opened';
		}

		/**
		 * Event "Close"
		 *
		 * @readonly
		 * @static
		 * @memberof Accordion
		 */
		static get EVENT_CLOSE() {
			return Accordion.NS + '.close';
		}

		/**
		 * Event "Closed"
		 *
		 * @readonly
		 * @static
		 * @memberof Accordion
		 */
		static get EVENT_CLOSED() {
			return Accordion.NS + '.closed';
		}

		/**
		 * Module CSS
		 *
		 * @readonly
		 * @static
		 * @memberof Accordion
		 */
		static get css() {
			return 'media/css/ui/accordion';
		}

		/**
		 * Default Options
		 *
		 * @readonly
		 * @static
		 * @memberof Accordion
		 */
		static get DEFAULT_OPTIONS() {
			return {
				panes: '.accordion-section',
				content: '.content',
				heading: '.heading',
				active: 'open',
				disabled: 'disabled',
				toggle: '[data-toggle="accordion"]',
				next: '[data-toggle="next"]',
				prev: '[data-toggle="prev"]',
				onShow(tab, index, accordion) {
					return true;
				},
				onHide(tab, index, accordion) {
					return true;
				},
				onNext(tab, index, accordion) {
					return true;
				},
				onPrev(tab, index, accordion) {
					return true;
				},
				multiopen: false,
				trigger: 'click',
				css: Accordion.css,
				themecss: false
			};
		}


		/**
		 * Creates an instance of Accordion.
		 *
		 * @param {HTMLElement} element HTML Element
		 * @param {Object|null} options Custom Options
		 * 
		 * @memberof Accordion
		 * 
		 * @returns {Accordion} new instance
		 */
		static init(element, options) {
			let acc = new Accordion(element, options);
			acc.element.acmsData(Accordion.NS, acc);
			return acc;
		}

		/**
		 * Creates an instance of Accordion.
		 *
		 * @param {HTMLElement} element HTML Element
		 * @param {Object|null} options Custom Options
		 * 
		 * @memberof Accordion
		 */
		constructor(element, options) {
			super(element, options);
			this._initialize();
		}


		/**
		 * Gibt einen Tab nach index zurück
		 *
		 * Sucht einen Tab basierend auf seinem Index und gibt den Tab zurück.
		 * Wird der Tab nicht gefunden, wird FALSE zurückgegeben.
		 *
		 * @param {Integer} i   Der Tab-Index des gesuchten Tab
		 * @returns {Array|Boolean}
		 */
		get(i) {
			if (this.$tabs[i]) {
				return this.$tabs[i];
			}
			return false;
		}
		/**
		 * Erlaubt einen "verbotenen" Tab
		 *
		 * Entfern die "disabled" CSS Klasse aus den Optionen
		 *
		 * @param {type} i
		 * @returns {Boolean|Tabs.prototype}
		 */
		enable(i) {
			let self = this,
				oTab = self.get(i);
			if (false === oTab) {
				return false;
			}
			oTab.disabled = false;
			Classes.removeClass(oTab.tab, self.options.disabled);
			if (oTab.link) oTab.link.setAttribute('aria-disabled', "false");
			if (oTab.panel) oTab.panel.setAttribute('aria-disabled', "false");
			return this;
		}

		/**
		 * Verbietet einen "erlaubten" Tab
		 *
		 * Setzt die "disabled" CSS Klasse aus den Optionen
		 *
		 * @param {Number} i
		 * @returns {Boolean|Tabs.prototype}
		 */
		disable(i) {
			let self = this,
				oTab = self.get(i);
			if (false === oTab) {
				return false;
			}
			oTab.disabled = true;
			Classes.addClass(oTab.tab, self.options.disabled);
			if (oTab.link) oTab.link.setAttribute('aria-disabled', "true");
			if (oTab.panel) oTab.panel.setAttribute('aria-disabled', "true");

			return this;
		}

		/**
		 * Zeigt einen Tab nach Index
		 *
		 * Aktiviert einen neuen Tab und deaktiviert den akuell sichtbaren Tab
		 *
		 * @param {Number} i    Der Index
		 * @returns {Boolean|undefined}
		 */
		show(i) {
			let otab = this.get(i),
				self = this,
				o = self.options;
			if (false === otab) {
				return false;
			}
			let tab = otab.tab;
			if (Classes.hasClass(tab, o.disabled) || Classes.hasClass(tab, o.active)) {
				return false;
			}
			let ci = self.current,
				c = self.get(ci),
				jtab = otab.tab;
			self.current = i;

			if (!self.option('multiopen')) {
				let e = AcmsEvent.createCustom(Accordion.EVENT_CLOSE, {
					relatedTarget: jtab,
					module: self
				});
				AcmsEvent.dispatch(self.element, e);
				AcmsEvent.dispatch(tab, e);
				if (e.defaultPrevented) {
					return false;
				}
			}
			let e2 = AcmsEvent.createCustom(Accordion.EVENT_OPEN, {
				relatedTarget: jtab,
				module: self
			});
			AcmsEvent.dispatch(tab, e2);
			if (e2.defaultPrevented)
				return;
			if (!self.option('multiopen') && c) {
				self._deactivate(c, () => {
					let hiddenEvent = AcmsEvent.createCustom(Accordion.EVENT_CLOSED, {
						relatedTarget: jtab,
						hides: ci,
						module: self
					});
					AcmsEvent.dispatch(self.element, hiddenEvent);
				});
			}
			self._activate(otab, () => {
				let e = AcmsEvent.createCustom(Accordion.EVENT_OPENED, {
					relatedTarget: jtab,
					module: self
				});
				AcmsEvent.dispatch(tab, e);
				AcmsEvent.dispatch(self.element, e);
			});
		}

		/**
		 * Hides Tab
		 *
		 * @param {Number} i    Tab index
		 * 
		 * @memberof Accordion
		 */
		hide(i) {
			let self = this,
				otab = self.get(i),
				o = self.options;
			if (false === otab) {
				return false;
			}
			let tab = otab.tab;
			if (Classes.hasClass(tab, o.disabled) ||
				!Classes.hasClass(tab, o.active)) {
				return false;
			}
			let ci = self.current,
				c = this.get(ci);
			self.current = i;

			let e = AcmsEvent.createCustom(Accordion.EVENT_CLOSE, {
				relatedTarget: tab,
				module: self
			});
			AcmsEvent.dispatch(self.element, e);
			AcmsEvent.dispatch(tab, e);

			if (e.defaultPrevented)
				return;
			self._deactivate(otab, () => {
				let e = AcmsEvent.createCustom(Accordion.EVENT_CLOSED, {
					relatedTarget: tab,
					module: self
				});
				AcmsEvent.dispatch(self.element, e);
				AcmsEvent.dispatch(tab, e);
			});
		}

		/**
		 * Toggle State
		 *
		 * @param {Number} i    Index of the Tab
		 * 
		 * @memberof Accordion
		 */
		toggle(i) {
			let otab = this.get(i),
				self = this,
				o = this.options;
			if (false === otab) {
				return false;
			}
			let tab = otab.tab;
			if (Classes.hasClass(tab, o.disabled)) {
				return false;
			}
			if (Classes.hasClass(tab, o.active)) {
				this.hide(i);
			} else {
				this.show(i);
			}
		}

		/**
		 * Zeigt den nächsten Tab
		 *
		 * Sucht den nächsten erlaubten Tab nach dem aktuellen und zeigt diesen
		 *
		 *
		 */
		next() {
			let self = this,
				o = this.options,
				c = this.current,
				next;
			next = this._findNext(c);
			let i = next.id;
			if (typeof (o.onNext) === 'function') {
				o.onNext(next.tab, next.id, self);
			}
			return this.show(i);
		}

		/**
		 * Zeigt den vorherigen Tab
		 *
		 * Sucht den vorherigen erlaubten Tab vor dem aktuellen und zeigt diesen
		 *
		 *
		 */
		previous() {
			let self = this,
				o = this.options,
				c = this.current,
				prev;
			prev = this._findPrevious(c);
			let i = prev.id;
			if (typeof (o.onPrev) === 'function') {
				o.onPrev(prev.tab, prev.id, self);
			}
			return this.show(i);
		}

		/**
		 * Zeigt den ersten Tab
		 *
		 * Sucht den ersten erlaubten Tab und zeigt diesen
		 *
		 *
		 */
		first() {
			let f;
			f = this._findNext(-1);
			let i = next.id;
			return this.show(i);
		}

		/**
		 * Zeigt den letzten Tab
		 *
		 * Sucht den letzten erlaubten Tab und zeigt diesen
		 *
		 *
		 */
		last() {
			let l, length = this.length + 1;
			l = this._findPrevious(length);
			let i = l.id;
			return this.show(i);
		}

		/**
		 * Deaktiviert einen Tab
		 *
		 * @param {Object} ca
		 * @param {Callable} cb
		 *
		 */
		_deactivate(ca, cb) {
			let self = this,
				o = this.options;
			Classes.removeClass(ca.tab, o.active);
			if (ca.link) ca.link.setAttribute('aria-expanded', "false");
			if (ca.panel) ca.panel.setAttribute('aria-expanded', "false");
			if (typeof (o.onHide) === 'function') {
				o.onHide(ca.tab, ca.id, self);
			}
			if (cb) cb();
		}

		/**
		 * Aktiviert einen Tab
		 *
		 * @param {Object} ca
		 * @param {Callable} cb
		 *
		 */
		_activate(na, cb) {
			let o = this.options,
				self = this;
			Classes.addClass(na.tab, o.active);
			if (na.link) {
				na.link.setAttribute('aria-expanded', "true");
			}
			if (na.panel) na.panel.setAttribute('aria-expanded', "true");
			if (typeof (o.onShow) === 'function') {
				o.onShow(na.tab, na.id, self);
			}
			if (cb) cb();
		}

		/**
		 * Findet den nächsten Tab intern
		 *
		 * Der aufruf erfolgt intern, bis der nächste Tab gefunden wurde, der
		 * nicht deaktiviert ist
		 *
		 * @param {Number} i    Der Index
		 *
		 * @returns {Object|Boolean}
		 */
		_findNext(i) {
			let ni = i + 1;
			if (ni > this.length) {
				ni = 0;
			}
			let n = this.get(ni);
			if (Classes.hasClass(n.tab, this.option('disabled'))) {
				n = this._findNext(ni);
			}
			return n;
		}

		/**
		 * Findet den vorigen Tab intern
		 *
		 * Der aufruf erfolgt intern, bis der vorige Tab gefunden wurde, der
		 * nicht deaktiviert ist
		 *
		 * @param {Number} i    Der Index
		 *
		 * @returns {Object|Boolean}
		 */
		_findPrevious(i) {
			let pi = i - 1;
			if (pi < 0) {
				pi = this.length;
			}
			let p = this.get(pi);
			if (Classes.hasClass(p.tab, this.option('disabled'))) {
				let p = this._findPrevious(pi);
			}
			return p;
		}

		/**
		 * On Tab Click Event Handler
		 *
		 * @param {Event} e
		 * 
		 * @memberof Accordion
		 */
		_onTabClick(e) {
			let self = this,
				o = self.options,
				index, etarget;
			e.preventDefault();
			e.stopPropagation();
			etarget = e.target || e.srcElement;
			if (!Selectors.matches(etarget, o.toggle)) {
				etarget = Selectors.closest(etarget, o.toggle);
			}
			index = parseInt(etarget.acmsData('index'));

			return self.toggle(index);
		}

		/**
		 * Setup Event Listener
		 *
		 * @memberof Accordion
		 */
		listen() {
			let self = this,
				o = self.options,
				element = self.element;
			AcmsEvent.on(element, 'click', o.toggle, self._onTabClick, true);
		}

		/**
		 * Preparing Class instance
		 * 
		 * Preloads all tabs
		 *
		 * @memberof Accordion
		 */
		_prepare() {
			let self = this,
				o = self.options,
				element = self.element,

				tabs = [],
				tab, isDisabled, isActive, toggles, leng, link, j, panel;
			self._toggles = Selectors.qa(o.toggle, self.element);

			let $tabs = Selectors.qa(o.panes, element);
			let len = $tabs.length,
				i;
			for (i = 0; i < len; i++) {
				tab = $tabs[i];
				isDisabled = Classes.hasClass(tab, o.disabled);
				isActive = Classes.hasClass(tab, o.active);
				toggles = Selectors.qa(o.toggle, tab);
				leng = toggles.length;
				link = Selectors.q('.heading a,.heading button', tab);
				if (link && link.acmsData('target')) {
					panel = tab.querySelector(link.acmsData('target'));
				} else {
					panel = tab.querySelector('.content');
				}

				let oTab = {
					id: i,
					disabled: isDisabled,
					tab: tab,
					_tab: tab,
					panel: panel,
					active: isActive,
					link: link
				};

				for (j = 0; j < leng; j++) {
					toggles[j].setAttribute('data-index', i);
					//    AcmsEvent.add(toggles[j], 'click', self._onTabClick);
				}
				tabs[i] = oTab;
				if (!o.multiopen && isActive) {
					self.current = i;
				}
			}
			self.$tabs = tabs;
			self.length = self.$tabs.length;
		}
	}

	return Accordion;
});
