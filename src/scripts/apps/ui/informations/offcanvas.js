/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/ui/informations/offcanvas", [
    "core/base",
    "core/classes",
    "core/selectors",
    "events/event"
], (Base, Classes, Selectors, AcmsEvent) => {

    /**
     * OffCanvas Menu
     *
     * Ein einfaches Off-Canvas Modul zum öffnen und schließen von Off-Canvas
     * Bereichen.
     *
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class OffCanvas extends Base {

        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof OffCanvas
         */
        static get MODULE() {
            return "Off-Canvas Menu";
        }

        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof OffCanvas
         */
        static get NS() {
            return "acms.apps.ui.informations.offcanvas";
        }

        /**
         * Event "open"
         *
         * @readonly
         * @static
         * @memberof OffCanvas
         */
        static get EVENT_OPEN() {
            return OffCanvas.NS + ".open";
        }

        /**
         * Event "opened"
         *
         * @readonly
         * @static
         * @memberof OffCanvas
         */
        static get EVENT_OPENED() {
            return OffCanvas.NS + ".opened";
        }

        /**
         * Event "close"
         *
         * @readonly
         * @static
         * @memberof OffCanvas
         */
        static get EVENT_CLOSE() {
            return OffCanvas.NS + ".close";
        }

        /**
         * Event "closed"
         *
         * @readonly
         * @static
         * @memberof OffCanvas
         */
        static get EVENT_CLOSED() {
            return OffCanvas.NS + ".closed";
        }

        /**
         * Default Options
         *
         * @readonly
         * @static
         * @memberof OffCanvas
         */
        static get DEFAULT_OPTIONS() {
            return {
                open: "open",
                target: null,
                closeOnClickOutside: true,
                opacity: 1,
                position: null,
                duration: 400,
                outside: false,
                timeout: 0,
                onOpen() {},
                onClose() {},
                css: OffCanvas.css,
                themecss: false
            };
        }

        /**
         * Position "left"
         *
         * @readonly
         * @static
         * @memberof OffCanvas
         */
        static get POSITION_LEFT() {
            return "left";
        }

        /**
         * Position "right"
         *
         * @readonly
         * @static
         * @memberof OffCanvas
         */
        static get POSITION_RIGHT() {
            return "right";
        }

        /**
         * Position "top"
         *
         * @readonly
         * @static
         * @memberof OffCanvas
         */
        static get POSITION_TOP() {
            return "top";
        }

        /**
         * Position "bottom"
         *
         * @readonly
         * @static
         * @memberof OffCanvas
         */
        static get POSITION_BOTTOM() {
            return "bottom";
        }

        /**
         * Module CSS
         *
         * @readonly
         * @static
         * @memberof OffCanvas
         */
        static get css() {
            return "media/css/ui/offcanvas";
        }

        /**
         * OffCanvas init
         *
         * @static
         * @param {HTMLElement} element
         * @param {Object|null} options
         * 
         * @returns {OffCanvas}
         * 
         * @memberof OffCanvas
         */
        static init(element, options) {
            let o = new OffCanvas(element, options);
            o.element.acmsData(OffCanvas.NS, o);
            if (o.target) {
                o.target.acmsData(OffCanvas.NS, o);
            }
            return o;
        }

        /**
         * Creates an instance of OffCanvas.
         * @param {HTMLElement} element
         * @param {Object|null} options
         * @memberof OffCanvas
         */
        constructor(element, options) {
            super(element, options);
            this.target = this.exits = this.position = null;
            this._initialize();
        }

        /**
         * Open Ovvcanvas Menu
         * 
         * @memberof OffCanvas
         */
        open() {
            let self = this,
                element = self.element,
                o = self.options,
                param = { relatedTarget: self.target, module: self },
                e = AcmsEvent.createCustom(OffCanvas.EVENT_OPEN, param);
            if (element.acmsData("open") == true) {
                return;
            }
            element.acmsData("open", true, true);
            AcmsEvent.dispatch(element, e);
            if (e.defaultPrevented) {
                return;
            }
            self._toggleAttributes();
            let e2 = AcmsEvent.createCustom(OffCanvas.EVENT_OPENED, param);
            AcmsEvent.dispatch(element, e2);
            if (typeof o.onOpen === "function") {
                o.onOpen();
            }
        }

        /**
         * Schließt die Off-Canvas
         * @memberof OffCanvas
         */
        close() {
            let self = this,
                element = this.element,
                o = this.options,
                param = { relatedTarget: this.target },
                e = AcmsEvent.createCustom(OffCanvas.EVENT_CLOSE, param);
            AcmsEvent.dispatch(element, e);
            if (e.defaultPrevented) {
                return false;
            }
            element.acmsData("open", false, true);
            self._toggleAttributes();
            let e2 = AcmsEvent.createCustom(OffCanvas.EVENT_CLOSED, param);
            AcmsEvent.dispatch(element, e2);
            if (typeof o.onClose === "function") {
                o.onClose();
            }
        }

        /**
         * Toggle State
         *
         * Öffnet oder schließt das OffCanvas Menu je nach dem aktuellen Status
         * @memberof OffCanvas
         */
        toggle() {
            let self = this,
                o = this.options,
                isOpen = this.isOpen();
            if (!self.target) {
                return;
            }

            if (isOpen) {
                return self.close();
            } else {
                return self.open();
            }
        }

        /**
         * Überprüft ob die OffCanvas offen ist
         *
         * @returns {Boolean}
         * @memberof OffCanvas
         */
        isOpen() {
            return (
                this.element.acmsData("open") === true ||
                this.element.acmsData("open") === "true"
            );
        }

        /**
         * Togglet je nach Status die Attribute
         *
         * @private
         * @memberof OffCanvas
         */
        _toggleAttributes() {
            let self = this,
                isOpen = self.isOpen(),
                target = self.target,
                element = self.element;
            if (true === isOpen) {
                target.setAttribute("aria-hidden", "false");
                target.setAttribute("aria-expanded", "true");
                Classes.addClass(target, "open");
                Classes.addClass(element, "active");
            } else {
                target.setAttribute("aria-hidden", "true");
                target.setAttribute("aria-expanded", "false");
                Classes.removeClass(target, "open");
                Classes.removeClass(element, "active");
            }
        }

        /**
         * Setup Event Listener
         *
         * @memberof OffCanvas
         * @private
         */
        listen() {
            let self = this,
                o = self.options,
                element = self.element,
                target = self.target;
            if (!(target instanceof HTMLElement)) {
                return;
            }
            AcmsEvent.add(element, "click", self._onClickElement);

            AcmsEvent.on(
                target,
                "click",
                '[data-toggle="exit-offcanvas"]',
                self._onClickOffCanvasExit,
                true
            );
            if (o.closeOnClickOutside) {
                AcmsEvent.add(document, "click", self._onClickOutside);
            }
            if (o.outside === true) {
                AcmsEvent.add(
                    self.target,
                    "mouseleave",
                    self._onClickOffCanvasExit
                );
            }
        }

        /**
         * On Click Element event handler
         *
         * @param {Event} e
         *
         * @memberof OffCanvas
         * @private
         */
        _onClickElement(e) {
            let self = this;
            e.preventDefault();
            e.stopPropagation();
            self.toggle();
        }

        /**
         * On Click Exit event handler
         *
         * @param {Event} e
         *
         * @memberof OffCanvas
         * @private
         */
        _onClickOffCanvasExit(e) {
            let self = this;
            e.preventDefault();
            e.stopPropagation();
            self.close();

            return false;
        }

        /**
         * On Click Outside Handler
         *
         * @param {Event} e
         * @memberof OffCanvas
         * @private
         */
        _onClickOutside(e) {
            let self = this,
                o = self.options,
                target = e.target;
            let off = Selectors.closest(e.target, o.target);
            if (!off && self.element.acmsData("open") === true) {
                self.close();
            }
        }

        /**
         * Prepare Module
         *
         * @memberof OffCanvas
         * @private
         */
        _prepare() {
            let self = this,
                o = self.options,
                element = self.element,
                target = o.target;
            if (!target) {
                self.target = false;
                Acms.Logger.logWarning(
                    "Eine OffCanvas Navigation braucht ein Ziel. Es wurde aber kein Ziel angegeben!"
                );
                return;
            }
            self.target = Selectors.q(target);
            self.exits = Selectors.qa(
                '[data-toggle="exit-offcanvas"]',
                self.target
            );
        }
    }

    return OffCanvas;
});
