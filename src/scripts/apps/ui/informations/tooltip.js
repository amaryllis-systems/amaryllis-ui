/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/ui/informations/tooltip", [
    "core/base",
	"tools/string/unique-id",
	"tools/is-visible",
	"core/classes",
	"core/selectors",
	"templates/template-engine",
	"events/event",
	"core/acms"
], (Base, UniqueId, isVisible, Classes, Selectors, TemplateEngine, AcmsEvent, A) => {
    

    /**
     * Tooltip
     *
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class Tooltip extends Base {

        /** 
         * @callback Tooltip~callableOption
         * 
         * The cllbacks are called during the create/show/hide process
         * 
         * @param {HTMLElement}     tooltip The Tooltip element 
         * @param {Tooltip}         self    This Class instance
         */

        /** 
         * @typedef Tooltip~ModuleOptions
         * 
         * @type {Object}
         * 
         * @property {String}       [animation=fade]    Animation to apply
         * @property {('top'|'bottom'|'right'|'left')} 
         *                          [placement=top]     Placement of the Tooltip
         * @property {String}       [template=templates/html/tooltip.hbs] Template for Tooltip
         * @property {String}       [trigger='hover focus click'] Triggers to open the toolback
         * @property {String}       title   Optional custom title. if omitted, Tooltip will search for title and aria-label attributes
         */

        /**
         * Module Namespace
         *
         * @type {String}
         *
         * @readonly
         * @static
         * @memberof Tooltip
         */
        static get NS() {
            return "acms.apps.ui.informations.tooltip";
        }

        /**
         * Module Name
         * 
         * @type {String}
         *
         * @readonly
         * @static
         * @memberof Tooltip
         */
        static get MODULE() {
            return "Tooltip";
        }

		/**
		 * Default Template
		 *
		 * @readonly
		 * @static
		 * @memberof Tooltip
		 */
		static get TEMPLATE() {
			return "templates/html/ui/informations/tooltip.hbs";
		}

        static get DEFAULT_OPTIONS() {
            return {
                animation: 'fade',
                placement: 'top',
                selector: false,
                template: Tooltip.TEMPLATE,
                trigger: 'hover focus click',
                title: '',
                delay: 0,
                html: true,
                container: false,
                viewport: {
                    selector: 'body',
                    padding: 0
                },
                classNames: {
					active: 'open'
				},
				selectors: {
					active: '.open',
					dismiss: '[data-dismiss="tooltip"]'
                },
                dismiss: false,
                id: null, // ID des Tooltip oder leer lassen für automatische ID (empfohlen),
				alwaysRemove: true, // remove always on close?
				dismissOutside: true, // dismiss on click outside tooltip?
                dismissResize: true, // dismiss on resize?
                customData: null, // optional custom data to be assigned to the template. Variable is ignored in default template.
                onCreate: (tooltip, self) => {},
				onOpen: (tooltip, self) => {},
				onClose: (tooltip, self) => {}
            };
        }

        static init(element, options) {
            let m = new Tooltip(element, options);
            m.element.acmsData(Tooltip.NS, m);
            
            return m;
        }


        /**
         * Creates an instance of Tooltip.
         * 
         * @param {HTMLElement} element
         * @param {Object}      options
         * 
         * @memberof Tooltip
         */
        constructor(element, options) {
            super(element, options);
            this._initialize();
        }

		/**
		 * Tooltip title
		 *
		 * @memberof Tooltip
		 */
		get title() {
			return this._title;
		}

		set title(title) {
			if(typeof title !== 'string') {
				title = "";
			}
			this._title = title;
		}

		/**
		 * HTML ID of the current Tooltip
		 *
		 * @readonly
		 * @memberof Tooltip
		 */
		get id() {
			let self = this, o = self.options;
			if(!self._id) {
				self._id = UniqueId('tooltip');
			}
			return self._id;
		}

        
		get rect() {
			if(this.element instanceof HTMLElement)
			return this.element.getBoundingClientRect();
			return {};
		}

		get scroll() {
			return {
                y: window.pageYOffset || document.documentElement.scrollTop,
                x: window.pageXOffset || document.documentElement.scrollLeft
            };
		}

		get dimension() {
			return {
                w: (this.tooltip ? this.tooltip.offsetWidth : 0),
                h: (this.tooltip ? this.tooltip.offsetHeight : 0)
            };
		}

		get creating() {
			return this._creating;
		}

		/** 
         * Reverse Position
         * 
         * @type {('bottom'|'top'|'left'|'right'|'')}
         * @memberof Tooltip
         */
        get reversePosition() {
            let pos = this.options.placement;
            if (/top/.test(pos)) {
                return 'bottom';
            } else if (/bottom/.test(pos)) {
                return 'top';
            } else if (/left/.test(pos)) {
                return 'right';
            } else if (/right/.test(pos)) {
                return 'left';
			}
			return '';
        }

		/**
		 * Open tooltip
		 *
		 * @memberof Tooltip
		 */
		open(pos) {
			let self = this, o = self.options;
			if(!self.tooltip) {
				if(self.creating) {
					return;
				}
				let cb = () => {
					if(typeof o.onOpen === 'function') {
						o.onOpen(self.tooltip, self);
					}
				};
				return self._createTooltip(pos, cb);
			}
			if(self.isOpen()) {
				return;
			}
			self._applyStyle(pos);
			self._fixStyles();
			if(typeof o.onOpen === 'function') {
				o.onOpen(self.tooltip, self);
			}
		}

		/**
		 * Close tooltip
		 *
		 * @memberof Tooltip
		 */
		close() {
			let self = this,
				o = self.options,
				cls = o.classNames.active;
			if(!self.tooltip) {
				return;
			}
			if(!self.isOpen()) {
				return;
			}
			Classes.removeClass(self.tooltip, cls);
			Classes.removeClass(self.tooltip, 'in');
			if(typeof o.onClose === 'function') {
				o.onClose(self.tooltip, self);
			}
			if(true === o.alwaysRemove) {
				self.remove();
			}
		}

		/**
		 * Toggle visibility
		 *
		 * @memberof Tooltip
		 */
		toggle() {
			if(this.isOpen()) {
				return this.close();
			}
			return this.open();
		}

		/**
		 * Is Tooltip open/visible?
		 *
		 * @returns {Boolean} `true` if the tooltip is open
		 * 
		 * @memberof Tooltip
		 */
		isOpen() {
			let self = this,
				o = self.options,
				cls = o.classNames.active;
			if(!self.tooltip) {
				return false;
			}
			return Classes.hasClass(self.tooltip, cls);
		}

		/**
		 * Remove Tooltip from DOM
		 *
		 * @memberof Tooltip
		 */
		remove() {
			if(this.tooltip) {
				this.close();
				this._off();
				this.container.removeChild(this.tooltip);
				this.tooltip = null;
                this._id = null;
                
			}
		}
		
		/**
		 * Is Tooltip in viewport?
		 *
		 * @returns {Boolean}	`true` if in viewport, `false` otherwise
		 * 
		 * @memberof Tooltip
		 */
		isInViewport() {
			if(!this.tooltip) return false;
            return isVisible(this.tooltip, true);
        }

		_createTooltip(pos, callback) {
			let self = this, o = self.options;
			if(self.creating) {
				return;
			}
			self._creating = true;
			let template = o.template || Tooltip.TEMPLATE,
				tdata = {
					content: self.title,
					dismiss: o.dismiss,
					closeText: A.Translator.t('Schließen'),
					customClass: o.customClass,
					customData: o.customData,
					id: self.id
				},
				cb = (content) => {
					self.tooltip = document.getElementById(self.id);
					self._applyStyle(pos);
					if(o.dismiss) self._on();
					self._creating = false;
					if(typeof o.onCreate === 'function') {
						o.onCreate(self.tooltip, self);
					}
					if(typeof callback === 'function') {
						callback();
                    }
				};
			TemplateEngine.compileResource(template, tdata, cb, self.container);
		}

		_applyStyle(pos) {
			let self = this, o = self.options;
            let placement = pos || o.placement;
            Classes.addClass(self.tooltip, placement + ',' + o.classNames.active);
            let linkDim = {w: self.element.offsetWidth, h: self.element.offsetHeight};
            let td = self.dimension;
            let toolDim = {w: td.w, h: td.h};
            let scrollYOffset = self.scroll.y;
			let scrollXOffset = self.scroll.x;
			self.tooltip.style = '';
            if (/top/.test(placement)) {
                self.tooltip.style.top = self.rect.top + scrollYOffset - toolDim.h + 'px';
                self.tooltip.style.left = (self.rect.left + scrollXOffset - (toolDim.w / 2) + (linkDim.w / 2)) + 'px';

            } else if (/bottom/.test(placement)) {
                self.tooltip.style.top = self.rect.top + scrollYOffset + linkDim.h + 'px';
                self.tooltip.style.left = self.rect.left + scrollXOffset - toolDim.w / 2 + linkDim.w / 2 + 'px';

            } else if (/left/.test(placement)) {
                self.tooltip.style.top = self.rect.top + scrollYOffset - toolDim.h / 2 + linkDim.h / 2 + 'px';
                self.tooltip.style.left = self.rect.left + scrollXOffset - toolDim.w + 'px';

            } else if (/right/.test(placement)) {
                self.tooltip.style.top = self.rect.top + scrollYOffset - toolDim.h / 2 + linkDim.h / 2 + 'px';
                self.tooltip.style.left = self.rect.left + scrollXOffset + linkDim.w + 'px';
            }
		}

		_fixStyles() {
			let self = this, o = self.options, pos;
            let placement = null;
            if (!self.isInViewport()) {
				pos = self.reversePosition;
				self._applyStyle(pos);
			}
		}

		/**
		 * Setup Event Listener
		 *
		 * @memberof Tooltip
		 * @private
		 */
		listen() {
			let self = this, o = self.options, element = self.element;
            let events = ('onmouseleave' in element) ? ['mouseenter', 'mouseleave'] : ['mouseover', 'mouseout'];
            if (/hover/.test(o.trigger)) {
                AcmsEvent.add(element, events[0], self._onTriggerOpen);
                if (!o.dismiss) {
                    AcmsEvent.add(element, events[1], self._onTriggerClose);
                }
			}
			if (/click/.test(o.trigger)) {
                AcmsEvent.add(element, 'click', self._onTriggerToggle);
                
			}
			if (/focus/.test(o.trigger)) {
                AcmsEvent.add(element, 'focus', self._onTriggerOpen);
                if (!o.dismiss) {
                    AcmsEvent.add(element, 'blur', self._onTriggerClose);
                }
            }

            if (o.dismissOutside) {
                AcmsEvent.add(document, 'click', self._onClickDocument);
            }

            AcmsEvent.add(window, 'resize', self._onResize);
		}

		_on() {
			let self = this,
				o = self.options;
			AcmsEvent.on(self.tooltip, 'click', o.selectors.dismiss, self._onTriggerClose, true);
		}

		_off() {
			let self = this,
				o = self.options;
			AcmsEvent.off(self.tooltip, 'click', o.selectors.dismiss, self._onTriggerClose);
		}

		/**
		 * On Click Document Event Handler
		 *
		 * @param {Event} e Event
         * 
		 * @memberof Tooltip
		 * @private
		 */
		_onClickDocument(e) {
			let self = this;
			if(!self.isOpen()) {
				return;
			}
			let target = e.target;
			if(!Selectors.closest(target, '.tooltip')) {
				self.close();
			}
		}

        /**
         * On Resize
         * 
         * @param e Event
         * 
         * @memberof Tooltip
		 * @private
         */
		_onResize(e) {
			let self = this, o = self.options;
			if(!self.isOpen()) {
				return;
			}
			if(o.dismissResize) {
				return self.close();
			}
			self._applyStyle();
			self._fixStyles();
		}

		/**
		 * On Trigger open Event Handler
		 *
		 * @param {Event} e		Event
         * 
		 * @memberof Tooltip
		 * @private
		 */
		_onTriggerOpen(e) {
            AcmsEvent.stop(e);
			this.open();
		}
		
		/**
		 * On Trigger Close Event Handler
		 *
		 * @param {Event} e		Event
         * 
		 * @memberof Tooltip
		 * @private
		 */
		_onTriggerClose(e) {
			AcmsEvent.stop(e);
			this.close();
		}
				
		/**
		 * On Trigger Toggle Event Handler
		 *
		 * @param {Event} e		Event
         * 
		 * @memberof Tooltip
		 * @private
		 */
		_onTriggerToggle(e) {
			AcmsEvent.stop(e);
			this.toggle();
		}
        
        /**
         * Prepare Module
         * 
         * @memberof Tooltip
		 * @private
         */
		_prepare() {
			let self = this,
				o = self.options,
				element = self.element;
			self.title = (o.title || element.getAttribute('title') || element.getAttribute('aria-label') || '');
            self.container = (o.container || document.body);
            element.setAttribute('data-original-title', self.title);
            element.removeAttribute('title');
		}

    }

    return Tooltip;
});
