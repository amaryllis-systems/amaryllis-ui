/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/ui/informations/overlay", [
    "core/base",
    "tools/string/unique-id",
    'events/event',
    'core/classes',
    "templates/template-engine",
    'core/acms'
], (Base, UniqueId, AcmsEvent, Classes, TemplateEngine, A) => {

    /**
     * Einfache Overlay Komponente
     *
     * Diese Komponente öffnet ein einfaches Overlay und zeigt irgendeinen Inhalt.
     * Für einen Standard-Loading Inhalt ist ein schnelles Setup eingebaut,
     * aufwendigere Templates müssen selbst übergeben werden.
     *
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class Overlay extends Base {

        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof Overlay
         */
        static get NS() {
            return "acms.ui.overlay.overlay";
        }

        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof Overlay
         */
        static get MODULE() {
            return "Overlay";
        }

        /**
         * Event "Overlay appending"
         * 
         * Event is triggered before the overlay will be appended.
         *
         * @readonly
         * @static
         * @memberof Overlay
         */
        static get EVENT_APPENDING() {
            return Overlay.NS + '.appending';
        }

        /**
         * Event "Overlay appended"
         * 
         * Event is triggered after the overlay has been appended.
         *
         * @readonly
         * @static
         * @memberof Overlay
         */
        static get EVENT_APPENDED() {
            return Overlay.NS + '.appended';
        }
        
        /**
         * Event "Overlay removing"
         * 
         * Event is triggered before the overlay will be removed.
         *
         * @readonly
         * @static
         * @memberof Overlay
         */
        static get EVENT_REMOVING() {
            return Overlay.NS + '.removing';
        }
        
        /**
         * Event "Overlay removed"
         * 
         * Event is triggered after the overlay has been removed.
         *
         * @readonly
         * @static
         * @memberof Overlay
         */
        static get EVENT_REMOVED() {
            return Overlay.NS + '.removed';
        }

        /**
         * Default Options
         *
         * @readonly
         * @static
         * @memberof Overlay
         */
        static get DEFAULT_OPTIONS() {
            return {
                classNames: {
                    spinner: 'v16', // single-spinner version
                    version: 'v1' // overlay version
                },
                template: 'templates/html/ui/informations/overlay.hbs', // overlay template to be used
                loading: true, // is loading state? if true, the loading spinner appears in overlay
                custom: {}, // Optional custom data to be used in custom templates (In Handlebars: <p>{{#if custom/prop}}{{custom/prop}}{{else}}{{custom/prop2}}{{/if}}</p>)
                css: false,
                themecss: false,
                closeOnEscape: false, // close overlay on escape?
                onAppend: function(element, self) {},
                onRemove: function(element, self) {}
            };
        }

        /**
         * Module Init
         *
         * @static
         * 
         * @param {HTMLElement} element     The Target-Element to append the overlay to.
         * @param {Object|null} options     Optional custom options
         * 
         * @returns {Overlay}   New Instance
         * @memberof Overlay
         */
        static init(element, options) {
            let o = new Overlay(element, options);
            o.element.acmsData(Overlay.NS, o);

            return o;
        }

        /**
         * Creates an instance of Overlay.
         * 
         * @param {HTMLElement} element     The Target-Element to append the overlay to.
         * @param {Object|null} options     Optional custom options
         * 
         * @memberof Overlay
         */
        constructor(element, options) {
            super(element, options);
            this.overlay   = null;
            this.appended   = false;
            this._initialize();
        }

        /**
         * Get Appending state
         *
         * @memberof Overlay
         */
        get appending() {
            return this._appending;
        }

        set appending(state) {
            this._appending = (true === state);
        }

        /**
         * Is the overlay appended to DOM?
         *
         * @memberof Overlay
         */
        get appended() {
            return this._appended;
        }

        set appended(state) {
            this._appended = (true === state);
        }

        /**
         * Gets the loading state
         *
         * @memberof Overlay
         */
        get loading() {
            return this._loading;
        }

        set loading(state) {
            this._loading = (true === state);
        }

        /**
         * Gets the template to be used
         *
         * @memberof Overlay
         */
        get template() {
            return this._template;
        }

        set template(tpl) {
            this._template = tpl;
        }

        /**
         * Reset template to the template passed in options
         *
         * @memberof Overlay
         */
        resetTemplate() {
            this.template = this.options.template;
        }

        /**
         * Get Message
         *
         * @memberof Overlay
         */
        get message() {
            if(typeof this._message !== 'string') {
                return false;
            }
            return this._message;
        }
        
        set message(msg) {
            if(typeof msg !== 'string') {
                msg = false;
            }
            this._message = msg;
        }

        /**
         * Get title
         *
         * @memberof Overlay
         */
        get title() {
            if(typeof this._title !== 'string') {
                return false;
            }
            return this._title;
        }
        
        set title(title) {
            if(typeof title !== 'string') {
                title = false;
            }
            this._title = title;
        }

        /**
         * set subtitle
         *
         * @memberof Overlay
         */
        get subtitle() {
            if(typeof this._subtitle !== 'string') {
                return false;
            }
            return this._subtitle;
        }
        
        set subtitle(subtitle) {
            if(typeof subtitle !== 'string') {
                subtitle = false;
            }
            this._subtitle = subtitle;
        }

        /**
         * Gets the id of the current overlay
         *
         * @readonly
         * @memberof Overlay
         */
        get id() {
            return this._id;
        }

        /**
         * Remove Overlay from DOM
         *
         * @memberof Overlay
         */
        remove() {
            let self = this,
                o = self.options,
                element = self.element, e, e2, params;
            params = {self: self, relatedTarget: self.target};
            if(!self.appended || self.appending) {
                return;
            }
            e = AcmsEvent.createCustom(Overlay.EVENT_REMOVING, params);
            AcmsEvent.dispatch(element, e);
            if(e.defaultPrevented) {
                return;
            }
            self.appending = true;
            self.appended = false;
            let overlay = document.getElementById(self.id);
            if(self.isBody) {
                document.body.removeChild(overlay);
            } else {
                element.removeChild(overlay);
            }
            self._off();
            if(typeof o.onRemove === 'function') {
                o.onRemove(element, self);
            }
            e2 = AcmsEvent.createCustom(Overlay.EVENT_REMOVED, params);
            AcmsEvent.dispatch(element, e2);
            self.appending = false;
        }

        /**
         * Append Overlay to DOM
         *
         * @memberof Overlay
         */
        append() {
            let self = this,
                o = self.options,
                element = self.element, 
                e, data, params, id;
            params = {self: self, relatedTarget: self.target};
            if(self.appended || self.appending) {
                return;
            }
            e = AcmsEvent.createCustom(Overlay.EVENT_APPENDING, params);
            AcmsEvent.dispatch(element, e);
            if(e.defaultPrevented) {
                return;
            }
            self._id = UniqueId();
            self.appended = true;
            self.appending = true;
            data = {
                overlay: {
                    id: self.id,
                    isLoading: self.loading,
                    version: o.classNames.version,
                    message: self.message,
                    title: self.title,
                    subtitle: self.subtitle
                },
                options: {
                    spinner: o.classNames.spinner
                }, 
                custom: o.custom
            };
            
            TemplateEngine.compileResource(self.template, data, self._onTemplateProcessed, element);
        }

        /**
         * On Template Processed Callback
         * 
         * Callback will be triggered if the template engine has compiled and appended the overlay.
         *
         * @memberof Overlay
         */
        _onTemplateProcessed() {
            let self = this, o = self.options, element = self.element, e2, params;
            params = {self: self, relatedTarget: self.target};
            self._on();
            if(typeof o.onAppend === 'function') {
                o.onAppend(element, self);
            }
            e2 = AcmsEvent.createCustom(Overlay.EVENT_APPENDED, params);
            AcmsEvent.dispatch(element, e2);
            self.appending = false;
        }

        /**
         * Aktiviert den Event-Listener
         *
         * Wenn das Overlay beim Drücken der Escape-Taste schließen soll, wird
         * der Event Listener für die Escape Taste gedrückt.
         *
         * @memberof Overlay
         */
        _on() {
            let self = this;
            if(self.options.closeOnEscape) {
                AcmsEvent.add(document, 'keyup', self._onKeyUp);
            }
        }

        /**
         * Deaktiviert den Event-Listener
         *
         * Wurde der Event-Listener für die Escape-Taste aktiviert, wird er
         * hiermit wieder ausgeschaltet.
         *
         * @memberof Overlay
         */
        _off() {
            let self = this;
            if(self.options.closeOnEscape) {
                AcmsEvent.remove(document, 'keyup', self._onKeyUp);
            }
        }

        /**
         * On Keyup Event Handler
         *
         * @param {Event} e
         * @memberof Overlay
         */
        _onKeyUp(e) {
            let self = this, key = e.keyCode || e.which;
            if (key === 27) {
                self.remove();
            }
        }

        /**
         * Prepare Module
         *
         * @memberof Overlay
         */
        _prepare() {
            let self = this, o = self.options, element = self.element;
            self.isBody = element.tagName.toLowerCase() === 'body';
            self.template = o.template;
            self.loading = o.loading;
        }

    }

    return Overlay;
});
