/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/ui/informations/window", [
    "core/base",
    "core/selectors",
    "core/classes",
    "events/event",
    "device/match-media",
    "core/acms"
], (Base, Selectors, Classes, AcmsEvent, MatchMedia, A) => {

    /**
	 * Window Box
	 *
	 * @class
	 * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
	 * 
	 * @property {WindowDetails} windowDetails Details Box on the right side
	 * @property {WindowOverlay} windowOverlay Overlay Box over windows body
	 * @property {Boolean} menuState	Current Window Menu State
	 * @property {Boolean} menuOpening	Is Window menu currently opening?
	 * 
	 */
	class WindowBox extends Base {

        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof WindowBox
         */
        static get NS() {
            return "apps.ui.informations.window";
        }

        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof WindowBox
         */
        static get MODULE() {
            return "Window Box";
        }

        /**
         * Default Options
         *
         * @readonly
         * @static
         * @memberof WindowBox
         */
        static get DEFAULT_OPTIONS() {
            return {
                classNames: {
                    open: 'open',
                    menu: {
                        menu: 'window-menu'
                    }
                },
                selectors: {
                    open: '.open',
                    sidemenu: {
                        toggle: '[data-role="sidemenu-toggle"]'
                    },
                    menu: {
                        menu: '.window-menu',
                        details: '.window-details',
                        toggle: '.window-menutoggle',
                        detailsToggle: '[data-open="details"]'
                    }
                },
                menu: '.window-menu',
                menuToggle: '.window-menutoggle',
                resizable: false,
                dragable: false,
                maximizable: true,
                overlayOptions: {
                    beforeDismiss: (overlay, trigger, wbox, woverlay) => {return true;},
                    onDismiss: (overlay, trigger, wbox, woverlay) => {},
                    beforeOpen: (overlay, trigger, wbox, woverlay) => {return true;},
                    onOpen: (overlay, trigger, wbox, woverlay) => {}
                },
                resizableOptions: {},
                draggableOptions: {}
            };
        }

        /**
         * Module Init
         *
         * @static
         * 
         * @param {HTMLElement} element WindowBox HTML Element
         * @param {Object|null} options Optional custom Options
         * 
         * @returns {WindowBox} New Instance
         * 
         * @memberof WindowBox
         */
        static init(element, options) {
            let wb = new WindowBox(element, options);
            wb.element.acmsData(WindowBox.NS, wb);

            return wb;
        }

        /**
         * Creates an instance of WindowBox.
         * 
         * @param {HTMLElement} element WindowBox HTML Element
         * @param {Object|null} options Optional custom Options
         * 
         * @memberof WindowBox
         */
        constructor(element, options) {
            super(element, options);
            this._initialize();
        }

        // Side Menu
        get menuState() {
            return this._menuState;
        }
        
        get menuOpening() {
            return this._menuOpening;
        }

        get menu() {
            return this._menu;
        }

        // overlay
        get windowOverlay() {
            return this._windowOverlay;
        }

		// details
        get windowDetails() {
            return this._windowDetails;
        }

        /**
         * Toogle window Side-Menu state
         * 
         */
        toggleMenu() {
            let self = this, o = self.options;
            if(!self.menu) {
                return;
            }
            if(self.menuState) {
                self.closeMenu();
            } else {
                self.openMenu();
            }
        }
        
        /**
         * Open Sidebar Menu
         *
         * @memberof WindowBox
         */
        openMenu() {
            let self = this, o = self.options, menu = self.menu;
            if(self.menuState || self.menuOpening) {
                return;
            }
            self._menuOpening = true;
            self._menuState = true;
            Classes.addClass(menu, o.classNames.open);
            menu.setAttribute('aria-hidden', 'false');
            Classes.addClass(self.menuToggle, o.classNames.open);
            self._menuOpening = false;
        }
        
        /**
         * Close Sidebar Menu
         *
         * @memberof WindowBox
         */
        closeMenu() {
            let self = this, o = self.options, menu = self.menu;
            if(!self.menuState || self.menuOpening) {
                return;
            }
            self._menuOpening = true;
            self._menuState = false;
            Classes.removeClass(menu, o.classNames.open);
            menu.setAttribute('aria-hidden', 'true');
            Classes.removeClass(self.menuToggle, o.classNames.open);
            self._menuOpening = false;
        }
        
        // maximize/minimize
        
        maximize() {
            let self = this, o = self.options, element = self.element;
            if(!o.maximizable || self.maximizing) {
                return false;
            }
            if(true === self.maximized) {
                return self.restore();
            }
            self.maximizing = true;
            self.maximized = true;
            self.element.acmsData('style', element.getAttribute('style'));
            Classes.addClass(element, 'maximizing');
            Classes.addClass(element, 'maximized');
            let sty = "position: fixed;z-index: 5;";
            if(o.top) {
                sty += "top: " + o.top + ";";
            }
            if(o.left) {
                sty += "left: "+o.left + ";";
            }
            if(o.right) {
                sty += "right: "+o.right + ";";
            }
            if(o.bottom) {
                sty += "bottom: "+o.bottom + ";";
            }
            document.body.acmsData('style', document.body.getAttribute('style'));
            document.body.setAttribute('style', 'height: 100vh; width: 100vw; overflow: hidden;');
            element.setAttribute('style', sty);
            self.maximizing = false;
            return false;
        }
        
        restore() {
            let self = this, o = self.options, element = self.element;
            if(!o.maximizable || self.maximizing || !self.maximized) {
                return false;
            }
            self.maximizing = true;
            self.maximized = false;
            self.element.setAttribute('style', element.acmsData('style'));
            element.acmsData('style', null);
            Classes.addClass(element, 'restoring');
            if(document.body.acmsData('style')) {
                document.body.setAttribute('style', document.body.acmsData('style'));
            }
            document.body.acmsData('style', null);
            Classes.removeClass(element, 'maximized minimized');
            self.maximizing = false;
            Classes.removeClass(element, 'restoring');
            return false;
        }
        
        minimize() {
            let self = this, o = self.options, element = self.element;
            if(!o.maximizable || self.minimizing || !self.minimized) {
                return false;
            }
            let sty = "position: fixed;z-index: 5; bottom: 1rem;";
            self.minimizing = true;
            self.minimized = true;
            element.setAttribute('style', element.acmsData('style'));
            Classes.addClass(element, 'minimizing');
            Classes.addClass(element, 'minimized');
            self.minmizing = false;
            
            return false;
        }

        /**
         * Open Window Overlay
         *
         * @memberof WindowBox
         */
        openOverlay() {
			if(!this.windowOverlay) {
				return;
			}
            this.windowOverlay.open();
        }

        /**
         * Dismiss Window Overlay
         *
         * @memberof WindowBox
         */
        dismissOverlay() {
			if(!this.windowOverlay) {
				return;
			}
            this.windowOverlay.dismiss();
        }

        /**
         * Toggle Window Overlay State
         *
         * @memberof WindowBox
         */
        toggleOverlay() {
			if(!this.windowOverlay) {
				return;
			}
            this.windowOverlay.toggle();
        }

        /**
         * Open Window Details
         *
         * @memberof WindowBox
         */
        openDetails() {
			if(!this.windowDetails) {
				return;
			}
            this.windowDetails.open();
        }

        /**
         * Dismiss Window Details
         *
         * @memberof WindowBox
         */
        dismissDetails() {
			if(!this.windowDetails) {
				return;
			}
            this.windowDetails.dismiss();
        }

        /**
         * Toggle Window Details State
         *
         * @memberof WindowBox
         */
        toggleDetails() {
			if(!this.windowDetails) {
				return;
			}
            this.windowDetails.toggle();
        }

        // overwrites
        
        /**
         * @inheritdoc
         *
         * @param {Object|null} options Optional custom options
         * 
         * @returns {Object}    Options. See {core/base} for more informations.
         * @see {Base} 
         * @memberof WindowBox
         */
        buildOptions(options) {
            let o = super.buildOptions(options);
            o.maximizable = (o.maximizable === 'true' || o.maximizable === true || parseInt(o.maximizable) === 1);
            o.resizable = (o.resizable === 'true' || o.resizable === true || parseInt(o.resizable) === 1);
            o.dragable = (o.dragable === 'true' || o.dragable === true || parseInt(o.dragable) === 1);
            
            return o;
        }

        // Events

        /**
         * Setup Event Listener
         *
         * @memberof WindowBox
         */
        listen() {
            let self = this, sidemenu = self.sidemenu, o = self.options, sel = o.selectors;
            
            if(self.menuToggle) {
                AcmsEvent.add(self.menuToggle, 'click', self._onClickMenuToggle);
            }
            AcmsEvent.on(self.element, 'click', sel.sidemenu.toggle, self._onClickSideMenuToggle, true);
            let menuIcon = Selectors.q('.window-icon', self.element);
            if(menuIcon) {
                AcmsEvent.add(menuIcon, 'click', function(e) {
                    e.preventDefault();
                    let target = e.target, leftMenu = Selectors.q('.window-body > .window-side', self.element);
                    if(!leftMenu) {
                        return;
                    }
                    if(!Classes.hasClass(target, 'window-icon')) {
                        target = Selectors.closest(target, '.window-icon');
                    }
                    if(MatchMedia.match('only screen and (max-width: 1023px)')) {
                        Classes.toggleClass(target, 'open');
                        Classes.toggleClass(leftMenu, 'open');
                    }
                    return false;
                });
            }
            if(self.maximizeIcon && o.maximizable) {
                AcmsEvent.add(self.maximizeIcon, 'click', self._onClickMaximize);
            }
        }

        /**
         * On Click Maximize Event Handler
         *
         * @param {Event} e	Click Event
		 * 
         * @memberof WindowBox
         */
        _onClickMaximize(e) {
            e.preventDefault();
            e.stopPropagation();
            this.maximize();
            return false;
        }
        
        /**
         * On-Click-Sidemenu Event Handler
         * 
         * @param {Event} e  Das Event
         * 
         * @returns {Boolean}   Always false
		 * 
         * @memberof WindowBox
         */
        _onClickSideMenuToggle(e) {
            e.preventDefault();
            let self = this, target = e.target, o = self.options, sel = o.selectors, parent;
            if(!Selectors.matches(target, sel.sidemenu.toggle)) {
                target = Selectors.closest(target, sel.sidemenu.toggle);
            }
            parent = Selectors.closest(target, 'li');
            e.preventDefault();
            Classes.toggleClass(parent, 'open');
            return false;
        }

        /**
         * On-Click-Menu-Toggle Event Handler
         * 
         * @param {Event} e  Das Event
         * 
         * @returns {Boolean}   Always false
		 * 
         * @memberof WindowBox
         */
        _onClickMenuToggle(e) {
            let self = this;
            
            e.preventDefault();
            self.toggleMenu();
            return false;
        }

        // internals

        /**
         * Prepare Module 
         *
         * @memberof WindowBox
         */
        _prepare() {
            let self = this, 
                o = self.options, 
                sel = o.selectors,
                element = self.element;
            self.menuToggle = Selectors.q(sel.menu.toggle, element);
            self._menu = Selectors.q(sel.menu.menu, element);
            self.sidemenu = Selectors.qa(sel.sidemenu.toggle, element);
            self.maximizeIcon = Selectors.q('.window-maximize', element);
			self.overlay = Selectors.q('.window-overlay', element);
			self.details = Selectors.q('.window-details', element);
			self._prepareOverlay();
			self._prepareDetails();
            if(self.menu) {
                self._menuState = Classes.hasClass(self.menu, o.classNames.open);
            }
            if(o.resizable) {
                self._prepareResizable();
            }
            if(o.draggable) {
                self._prepareDragable();
            }
        }

        
        /**
         * Vorbereiten der Overlay-Option, wenn dieses gefunden wurde
         * 
         * @memberof WindowBox
         */
        _prepareOverlay() {
            let self = this;
            if(!self.overlay) {
                return;
            }
            let opts = self.options.overlayOptions || {};
            require(['apps/ui/informations/window/window-overlay'], (WindowOverlay) => {
                self._windowOverlay = WindowOverlay.init(self.overlay, opts, self);
            });
        }
        
        /**
         * Vorbereiten der Details-Option, wenn dieses gefunden wurde
         * 
         * @memberof WindowBox
         */
        _prepareDetails() {
            let self = this;
            if(!self.details) {
                return;
            }
            let opts = self.options.detailsOptions || {};
            require(['apps/ui/informations/window/window-details'], (WindowDetails) => {
                self._windowDetails = WindowDetails.init(self.details, opts, self);
            });
        }
        
        
        /**
         * Vorbereiten der resizable-Option, wenn diese gesetzt wurde
         * 
         * @memberof WindowBox
         */
        _prepareResizable() {
            let self = this;
            let opts = self.options.resizableOptions || {};
            require(['ui/tools/resizable'], function(Resizable) {
                self.resizable = Resizable.init(self.element, opts);
            });
        }
        
        /**
         * Vorbereiten der dragable-Option, wenn diese gesetzt wurde
         * 
         * @memberof WindowBox
         */
        _prepareDragable() {
            let self = this;
            let opts = self.options.draggableOptions || {};
            require(['ui/tools/draggable'], function(Dragable) {
                self.dragable = Dragable.init(self.element, opts);
            });
        }

    }

    return WindowBox;
});


