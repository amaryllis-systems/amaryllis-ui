/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/ui/informations/window/window-details", [
    "core/base",
    "core/selectors",
    "core/classes",
    "events/event",
    "apps/ui/informations/window",
    "core/acms"
], (Base, Selectors, Classes, AcmsEvent, WindowBox, A) => {

    /**
     * Window Details
     * 
     * Class Responsible for handling in-window details for window actions.
     *
     * @class
     * @author qm-b <https://bitbucket.org/qm-b/>
     * @extends {Base}
     */
    class WindowDetails extends Base {

        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof WindowDetails
         */
        static get NS() {
            "apps.ui.informations.window.window-details";
        }

        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof WindowDetails
         */
        static get MODULE() {
            return "Window Box Overlay";
        }

        /**
         * Event "opening"
         * 
         * Das Event wird ausgeführt, wenn das WindowDetails geöffnet werden soll
         *
         * @readonly
         * @static
         * @memberof WindowDetails
         */
        static get EVENT_OPENING() {
            return WindowDetails.NS + ".opening";
        }

        /**
         * Event "opened"
         * 
         * Das Event wird ausgeführt, wenn das WindowDetails geöffnet wurde
         *
         * @readonly
         * @static
         * @memberof WindowDetails
         */
        static get EVENT_OPENED() {
            return WindowDetails.NS + ".opened";
        }

        /**
         * Event "closing"
         * 
         * Das Event wird ausgeführt, wenn das WindowDetails geschlossen werden soll
         *
         * @readonly
         * @static
         * @memberof WindowDetails
         */
        static get EVENT_CLOSING() {
            return WindowDetails.NS + ".closing";
        }

        /**
         * Event "closed"
         * 
         * Das Event wird ausgeführt, wenn das WindowDetails geschlossen wurde
         *
         * @readonly
         * @static
         * @memberof WindowDetails
         */
        static get EVENT_CLOSED() {
            return WindowDetails.NS + ".closed";
        }

        /**
         * Default Options
         *
         * @readonly
         * @static
         * @memberof WindowDetails
         */
        static get DEFAULT_OPTIONS() {
            return {
                active: 'active', // CSS-Klasse wenn aktiv/offen
                trigger: '[data-open="window-details"]', // trigger zum öffnen
                dismiss: '[data-dismiss="window-details"]', // trigger zum schließen
                state: false, // aktueller Status
                window: null, // Window Modul Instanz
                /**
                 * BeforeOpen Aktion
                 * 
                 * Die Aktion wird aufgerufen, nachdem der Open-Trigger aufgerufen wurde
                 * und bevor das Overlay geöffnet wird. Über die Rückgabe TRUE|FALSE lässt 
                 * sich steuern, ob das Window geöffnet werden soll.
                 * 
                 * @param {HTMLElement}     details Das Window Details Element
                 * @param {HTMLElement}     trigger Der Button-Trigger, der die Aktion gestartet 
                 *                                  hat. Wenn nicht durch trigger ausgelöst: null
                 * @param {WindowBox}       wbox    WindowBox instance
                 * @param {WindowDetails}   self    Class instance
                 * 
                 * @returns {Boolean}   TRUE wenn alles weiter gehen kann. Andernfalls 
                 *                      FALSE um die Aktion abzubrechen.
                 * 
                 * @memberof WindowDetails
                 */
                beforeOpen: function(details, trigger, wbox, self) {
                    return true;
                },
                /**
                 * OnOpen Aktion
                 * 
                 * Die Aktion wird aufgerufen, nachdem der Open-Trigger aufgerufen wurde
                 * und nachdem das Overlay geöffnet wurde.
                 * 
                 * @param {HTMLElement}     details Das Window Details Element
                 * @param {HTMLElement}     trigger Der Button-Trigger, der die Aktion gestartet 
                 *                                  hat. Wenn nicht durch trigger ausgelöst: null
                 * @param {WindowBox}       wbox    WindowBox instance
                 * @param {WindowDetails}   self    Class instance
                 * 
                 * @memberof WindowDetails
                 */
                onOpen: function(details, trigger, wbox, self) {},
                /**
                 * BeforeDismiss Aktion
                 * 
                 * Die Aktion wird aufgerufen, nachdem der Dismiss-Trigger aufgerufen wurde
                 * und bevor das Overlay geschlossen wird. Über die Rückgabe TRUE|FALSE lässt 
                 * sich steuern, ob das Window geschlossen werden soll.
                 * 
                 * @param {HTMLElement}     details Das Window Details Element
                 * @param {HTMLElement}     trigger Der Button-Trigger, der die Aktion gestartet 
                 *                                  hat. Wenn nicht durch trigger ausgelöst: null
                 * @param {WindowBox}       wbox    WindowBox instance
                 * @param {WindowDetails}   self    Class instance
                 * 
                 * @returns {Boolean}   TRUE wenn alles weiter gehen kann. Andernfalls 
                 *                      FALSE um die Aktion abzubrechen.
                 * 
                 * @memberof WindowDetails
                 */
                beforeDismiss: function(details, trigger, wbox, self) {
                    return true;
                },
                /**
                 * OnDismiss Aktion
                 * 
                 * Die Aktion wird aufgerufen, nachdem der Dismiss-Trigger aufgerufen wurde
                 * und nachdem das Overlay geschlossen wurde.
                 * 
                 * @param {HTMLElement}     details Das Window Details Element
                 * @param {HTMLElement}     trigger Der Button-Trigger, der die Aktion gestartet 
                 *                                  hat. Wenn nicht durch trigger ausgelöst: null
                 * @param {WindowBox}       wbox    WindowBox instance
                 * @param {WindowDetails}   self    Class instance
                 * 
                 * @memberof WindowDetails
                 */
                onDismiss: function(details, trigger, wbox, self) {
                }
            };
        }

        /**
         * Module Init
         *
         * @static
         * 
         * @param {HTMLElement}     element     window details element
         * @param {Object|null}     options     optional custom options
         * @param {WindowBox}       wbox        Window-Box instance
         * 
         * @returns {WindowDetails} New Instance
         * 
         * @memberof WindowDetails
         */
        static init(element, options, wbox) {
            let l = new WindowDetails(element, options, wbox);
            l.element.acmsData(WindowDetails.NS, l);

            return l;
        }

        /**
         * Creates an instance of WindowDetails.
         * 
         * @param {HTMLElement}     element     window details element
         * @param {Object|null}     options     optional custom options
         * @param {WindowBox}       wbox        Window-Box instance
         * 
         * @memberof WindowDetails
         */
        constructor(element, options, wbox) {
            super(element, options);
            this.windowbox = wbox;
            this._initialize();
        }

        /**
         * Is the details currently opening?
         *
         * @memberof WindowDetails
         */
        get opening() {
            return this._opening;
        }

        /**
         * Change opening state
         *
         * @memberof WindowDetails
         */
        set opening(state) {
            this._opening = (true === state);
        }

        /**
         * Is the overly currently closing?
         *
         * @memberof WindowDetails
         */
        get closing() {
            return this._closing;
        }

        /**
         * Change closing state
         *
         * @memberof WindowDetails
         */
        set closing(state) {
            this._closing = (true === state);
        }

        /**
         * 
         * @param {type} trigger
         * 
         */
        toggle(trigger) {
            let self = this,
                element = self.element,
                o = self.options;
            if(Classes.hasClass(element, o.active)) {
                this.dismiss(trigger);
            } else {
                this.open(trigger);
            }
        }

        /**
         * Open/Show Overlay
         * 
         * @param {HTMLElement} trigger     Optional trigger, if the details opens by trigger.
         * 
         * 
         */
        open(trigger) {
            let self = this, element = self.element, o = self.options;
            if(self.opening === true || self.closing === true) {
                return;
            }
            if(typeof trigger === "undefined") {
                trigger = null;
            }
            self.opening = true;
            if(typeof o.beforeOpen === 'function') {
                let res = o.beforeOpen(element, trigger, self.windowbox, self);
                if(res !== true) {
                    return false;
                }
            }
            
            let props = {
                ele: element,
                related: trigger
            };
            let evt = AcmsEvent.createCustom(WindowDetails.EVENT_OPENING, props);
            AcmsEvent.dispatch(element, evt);
            Classes.addClass(element, o.active);
            element.setAttribute('aria-hidden', 'false');
            let evt2 = AcmsEvent.createCustom(WindowDetails.EVENT_OPENED, props);
            AcmsEvent.dispatch(element, evt2);
            if(typeof o.onOpen === 'function') {
                o.onOpen(element, trigger, self.windowbox, self);
            }
            
            self.opening = false;
        }

        /**
         * 
         * @param {HTMLElement|null|undefined} trigger
         * 
         */
        dismiss(trigger) {
            let self = this, element = self.element, o = self.options;
            if(self.opening === true || self.closing === true) {
                return;
            }
            self.closing = true;
            if(typeof trigger === "undefined") {
                trigger = null;
            }
            if(typeof o.beforeDismiss === 'function') {
                let res = o.beforeDismiss(element, trigger, self.windowbox, self);
                if(res !== true) {
                    return false;
                }
            }
            let props = {
                ele: element,
                related: trigger
            };
            let evt = AcmsEvent.createCustom(WindowDetails.EVENT_CLOSING, props);
            AcmsEvent.dispatch(element, evt);
            Classes.removeClass(element, o.active);
            element.setAttribute('aria-hidden', 'true');
            let evt2 = AcmsEvent.createCustom(WindowDetails.EVENT_CLOSED, props);
            AcmsEvent.dispatch(element, evt2);
            if(typeof o.onDismiss === 'function') {
                o.onDismiss(element, trigger, self.windowbox, self);
            }
            
            self.closing = false;
        }
        

        /**
         * Setup Event Listener
         *
         * @memberof WindowDetails
         */
        listen() {
            let self = this,
                element = self.element,
                o = self.options;
            
            AcmsEvent.on(element, 'click', o.dismiss, self._onClickDismiss, true);
            AcmsEvent.on(self.windowbox.element, 'click', o.trigger, self._onClickOpen, true);
        }

        /**
         * On Click Opten Event Handler
         * 
         * @param {Event} e     Click Event
         * 
         * @returns {Boolean}
         */
        _onClickOpen(e) {
            let self = this, element = self.element, o = self.options, target = e.target;
            e.preventDefault();
            e.stopPropagation();
            if(!Selectors.matches(target, o.trigger)) {
                target = Selectors.closest(target, o.trigger);
            }
            self.open(target);
            return false;
        }

        /**
         * On Click Dismiss Event Handler
         * 
         * @param {Event} e     Click Event
         * 
         * @returns {Boolean}
         */
        _onClickDismiss(e) {
            let self = this, element = self.element, o = self.options, target = e.target;
            e.preventDefault();
            e.stopPropagation();
            if(!Selectors.matches(target, o.dismiss)) {
                target = Selectors.closest(target, o.dismiss);
            }
            self.dismiss(target);
            return false;
        }

        /**
         * Prepare Module
         *
         * @memberof WindowDetails
         */
        _prepare() {
            let self = this, element = self.element, o = self.options;
            self.window = Selectors.closest(element, '.window');
            self.triggers = Selectors.qa(o.trigger, self.window);
            self.opening = false;
            self.closing = false;
        }

    }
    
    return WindowDetails;
    
});
