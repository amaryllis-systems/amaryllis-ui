/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/ui/informations/modal", [
    "core/base",
    'core/selectors',
    'core/classes',
    'apps/ui/helpers/fade',
    'apps/ui/helpers/show-hide',
    'tools/string/unique-id',
    'tools/object/extend',
    'events/event'
], (Base, Selectors, Classes, Fade, ShowHide, UniqueId, Extend, AcmsEvent) => {

    /**
     * Modal Overlay
     * 
     * A visual example can be found [here](https://www.amaryllis-ui.eu/ui-framework/informations/modal-v1/)
     * in our UI Documentation.
     *
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     * 
     * @property {Boolean} isShown  `true` if the Modal is visible
     * @property {Modal~ModuleOptions} options custom options for this instance
     * @property {HTMLButtonElement|HTMLAnchorElement}  element The Button trigger
     * @property {HTMLElement}  target The modal box
     * @property {HTMLElement|null}  overlay The modal overlay, if the modal is visible and overlay is enabled by options.
     * 
     * @fires Modal#acms.apps.ui.informations.modal.show
     * @fires Modal#acms.apps.ui.informations.modal.shown
     * @fires Modal#acms.apps.ui.informations.modal.hide
     * @fires Modal#acms.apps.ui.informations.modal.hidden
     * 
     * @example 
     * let mod = Modal.init(btn, customOptions);
     * mod.on('show', (btn, modal, inst) => {console.log('show')})
     *    .on('hide', (btn, modal, inst) => {console.log('hide')});
     * mod.show();
     * setTimeout(() => {
     *  mod.hide();
     * }, 5000);
     * 
     */
    class Modal extends Base {

        /** 
         * Module Options
         * 
         * @typedef {Object} Modal~ModuleOptions
         * 
         * @property {HTMLElement|String}   target                  Target Modal Overlay. Must be an instance of 
         *                                                          HTMLElement or an ID of an element in DOM
         * @property {Boolean}              [closeOnEscape=true]    Close Modal on Escape?
         * @property {Boolean}              [overlay=true]          Create an overlay on showing the modal?
         * @property {String}               [animateIn="fadeInDownBig"]
         *                                                          Animate-In CSS Class. Amaryllis UI has included 
         *                                                          the great animate css library from Daniel Eden 
         *                                                          [animate.css](https://daneden.github.io/animate.css/)
         *                                                          where you can find further examples.
         * @property {String}               [animateOut="fadeOutDownBig"]
         *                                                          Animate-Out CSS Class.
         * @property {CallableFunction}     onShow                  On Show Callback
         * @property {CallableFunction}     onHide                  On Hide Callback
         */

         /** 
          * on created modal callback
          * 
          * Callback will be triggered, after the {@link Modal~}
          * 
          * @callback Modal~onCreated
          * 
          * @param {Modal}          modal   instance of the created modal
          * @param {HTMLElement}    btn     The button element. If no button is given, a new button is created, but not appended to DOM.
          * @param {String}         content rendered html content
          * @param {Object}         tplData The final resulting template data passed to the template
          */

        /**
         * Module Namespace
         * 
         * @type {String}
         *
         * @readonly
         * @static
         * @memberof Modal
         */
        static get NS() {
            return "acms.apps.ui.informations.modal";
        }

        /**
         * Module Name
         *
         * @type {String}
         *
         * @readonly
         * @static
         * @memberof Modal
         */
        static get MODULE() {
            return "Modal Overlay";
        }

        /**
         * Default Options
         *
         * @type {Modal~ModuleOptions}
         *
         * @readonly
         * @static
         * @memberof Modal
         */
        static get DEFAULT_OPTIONS() {
            return {
                // Object | String | null
                target: null,
                // String | null
                remote: null,
                // Boolean
                closeOnEscape: true,
                // Boolean
                overlay: true,
                animateIn: 'fadeInDownBig',
                animateOut: 'fadeOutDownBig',
                onShow: function (element, target, self) {},
                onHide: function (element, target, self) {},
                css: Modal.css,
                themecss: false
            };
        }

        /**
         * Event "Show"
         *
         * @type {String}
         *
         * @readonly
         * @static
         * @memberof Modal
         */
        static get EVENT_SHOW() {
            return Modal.NS + '.show';
        }
        
        /**
         * Event "shown"
         *
         * @type {String}
         *
         * @readonly
         * @static
         * @memberof Modal
         */
        static get EVENT_SHOWN() {
            return Modal.NS + '.shown';
        }

        /**
         * Event "Hide"
         *
         * @type {String}
         *
         * @readonly
         * @static
         * @memberof Modal
         */
        static get EVENT_HIDE() {
            return Modal.NS + '.hide';
        }

        /**
         * Event "hidden"
         *
         * @type {String}
         *
         * @readonly
         * @static
         * @memberof Modal
         */
        static get EVENT_HIDDEN() {
            return Modal.NS + '.hidden';
        }

        /**
         * Default Stylesheet
         *
         * @readonly
         * @static
         * @memberof Modal
         */
        static get css() {
            return "media/css/ui/modal";
        }

        /**
         * Module init
         *
         * @static
         * 
         * @param {HTMLElement}             element     Modal trigger
         * @param {Modal~ModuleOptions}     options     Optional custom options
         * 
         * @returns {Modal} New instance
         * 
         * @memberof Modal
         */
        static init(element, options) {
            let m = new Modal(element, options);
            m.element.acmsData(Modal.NS, m, false);
            m.target.acmsData(Modal.NS, m, false);
            return m;
        }

        /**
         * Create Modal overlay on the fly
         *
         * @static
         * 
         * @param {String}              tpl                 Template Source of *hbs* Template (e.g. `templates/html/ui/informations/modal.hbs`) or HTML Template String for Handlebars
         * @param {Object}              data                Template data to pass to the template engine. Additionally any of the {@link Modal~ModuleOptions|"Module Options"} are supported.
         * @param {String}              data.title          Modal title
         * @param {String|Boolean}      data.htmlContent    Optional (additional) html content to pass to the template.
         * @param {String|Boolean}      data.content        Optional (additional) text content to pass to the template. 
         *                                                  __NOTE:__ Only `htmlContent` __OR__ `content` is used in default template!
         * @param {String|Boolean}      data.customButtons  Optional (additional) custom buttons that will be passed as htmstring to 
         *                                                  default template
         * @param {String|Boolean}      data.size           optional size to pass to the default template. 
         *                                                  (`tiny`, `small`, `medium`, `large`, `full-width`, `full-screen`, `bottom sheet`, `top sheet` are supported by UI as default)
         * @param {HTMLButtonElement}   btn                 Modal trigger button
         * @param {Modal~onCreated}     cb                  Callback Function; will be triggered once the modal box is initialized
         * 
         * ``` js
         *  let tpl = 'my-theme/templates/html/modal.hbs',
         *      data = {},
         *      btn = document.querySelector('button#my-modal-trigger'),
         *      cb = (modal, btn, content, tplData) => {
         *          alert('done!');
         *      };
         * Modal.createModal(tpl, data, btn, cb);
         * ```
         * 
         * @memberof Modal
         */
        static createModal(tpl, data, btn, cb) {
            let defaults = {
                    id: UniqueId('modal'),
                    title: '', // Modal title
                    htmlContent: false, // html Content
                    content: false, // text content als Alternative zu htmlContent
                    customButtons: false, // html string
                    size: false // small, large, full
                },
                tplData = Extend.flat({}, true, defaults, data),
                bdy = document.getElementsByTagName('body')[0];
            if (!tpl) {
                tpl = 'templates/html/ui/informations/modal.hbs';
            }
            let callback = function (content) {
                btn = btn || document.createElement('button');
                btn.acmsData('target', '#' + tplData.id);
                let opts = tplData;
                opts.target = '#' + tplData.id;
                let modal = Modal.init(btn, opts);
                cb(modal, btn, content, tplData);
            };

            require(['templates/template-engine'], function (TemplateEngine) {
                if (tpl.substr(tpl.trim().length - 4) === '.hbs') {
                    TemplateEngine.compileResource(tpl, tplData, callback, bdy);
                } else {
                    let content = TemplateEngine.compile(tpl, tplData, bdy);
                    callback(content);
                }

            });

        }

        /**
         * Creates an instance of Modal.
         * 
         * @param {HTMLElement}         element     Modal trigger
         * @param {Modal~ModuleOptions} options     Optional custom options
         * 
         * @memberof Modal
         */
        constructor(element, options) {
            super(element, options);
            this._initialize();
        }

        get isShown() {
            return this._isShown;
        }

        get overlay() {
            return this.$overlay;
        }

        
        /**
         * Show Modal overlay
         *
         * @memberof Modal
         */
        show() {
            if (!this.target) {
                return false;
            }
            let self = this,
                o = self.options,
                element = self.element,
                target = self.target,
                param = {
                    relatedTarget: target
                },
                e = AcmsEvent.createCustom(Modal.EVENT_SHOW, param);
            if (element) {
                AcmsEvent.dispatch(element, e);
            }
            AcmsEvent.dispatch(target, e);

            if (self.isShown || e.defaultPrevented)
                return;
            self._isShown = true;
            Classes.addClass(self.$body, 'modal-open');
            self._on();
            let cb = function () {
                let fade = Classes.hasClass(self.target, 'fade'),
                    e = AcmsEvent.createCustom(Modal.EVENT_SHOWN, param);
                if (fade) {
                    Classes.addClass(self.target, 'in,animated,' + o.animateIn);
                    self.target.setAttribute('aria-hidden', false);
                    AcmsEvent.dispatch(target, e);
                    if (element) {
                        AcmsEvent.dispatch(element, e);
                    }
                    Fade.fadeIn(self.target, 500);
                    ShowHide.show(self.target);
                } else {
                    Classes.addClass(self.target, 'in,animated,' + o.animateIn);
                    self.target.setAttribute('aria-hidden', false);
                    AcmsEvent.dispatch(target, e);
                    if (element) {
                        AcmsEvent.dispatch(element, e);
                    }
                    ShowHide.show(self.target);
                }
                if (typeof o.onShow === 'function') {
                    o.onShow(element, target, self);
                }
                self.emit('show', self.element, self.target, self);
            };
            this._overlay(cb);
        }

        /**
         * Hides a Modal Box
         * @param {Event} e Event that has been fired
         * 
         */
        hide(e) {
            let self = this,
                element = self.element,
                target = self.target;
            if (e)
                e.preventDefault();
            let modals = Selectors.qa('.modal'),
                i, o = self.options;
            let cb1 = function (e1) {
                    let $dialog = e1.target;
                    if ($dialog.tagName.toUpperCase() === "BUTTON") {
                        $dialog = Selectors.closest($dialog, '.modal-dialog');
                    }
                    if (Classes.hasClass($dialog, 'modal')) {
                        $dialog = $dialog.querySelector('.modal-dialog');
                    }
                    let prevDialog = $dialog.acmsData('previousDialog');
                    if (null !== prevDialog && typeof prevDialog === "object") {
                        Classes.removeClass(prevDialog, 'aside,aside-1,aside-2,aside-3,aside-4,aside-5');
                        $dialog.acmsData('previousDialog', false, false);
                    }
                },
                onShow = function (e) {
                    for (i = 0; i < modals.length; i++)
                        AcmsEvent.remove(modals[i], Modal.EVENT_HIDE, cb1);
                };

            for (i = 0; i < modals.length; i++) {
                AcmsEvent.add(modals[i], Modal.EVENT_HIDE, cb1);
            }

            let evt = AcmsEvent.create(Modal.EVENT_HIDE);
            AcmsEvent.dispatch(element, evt);
            AcmsEvent.dispatch(target, evt);
            if (!self.isShown) {
                return;
            }
            let previousDialog = self.target.querySelector('.modal-dialog').acmsData('previousDialog');
            if (null !== previousDialog && typeof previousDialog === "object") {
                Classes.removeClass(previousDialog, 'aside,aside-1,aside-2,aside-3,aside-4,aside-5');
                self.target.querySelector('.modal-dialog').acmsData('previousDialog', false, false);
            }
            self._isShown = false;
            self._off();
            Classes.removeClass(self.target, 'in,' + o.animateIn);
            Classes.addClass(self.target, 'animated,' + o.animateOut);

            target.setAttribute('aria-hidden', true);
            self._hideModal();
        }

        /**
         * Setup Event Listeners
         *
         * @private
         * @memberof Modal
         */
        listen() {
            let self = this;
            AcmsEvent.add(this.element, 'click', self._onClickModalElement.bind(self));
        }


        /**
         * On Click Modal Element Event Handler
         *
         * @param {Event} e Click Event
         * 
         * @memberof Modal
         */
        _onClickModalElement(e) {
            let self = this;
            e.preventDefault();
            if (Classes.hasClass(self.element, 'disabled') ||
                self.element.getAttribute('disabled')) {
                return false;
            }
            self.show();
        }

        /**
         * Aktiviert den Event-Listener
         *
         * Wenn das Overlay beim Drücken der Escape-Taste schließen soll, wird
         * der Event Listener für die Escape Taste gedrückt.
         *
         * @private
         * @memberof Modal
         */
        _on() {
            let self = this,
                j, m;
            if (this.options.closeOnEscape) {
                let onClickEscape = function (e) {
                        if (!self.isShown || e.defaultPrevented) {
                            return;
                        }
                        let k = e.keyCode || e.which;
                        if (k === 27) {
                            self.hide(e);
                        }
                    },
                    onHide3 = function (ev) {
                        AcmsEvent.remove(document, 'click', onClickEscape);
                    };
                AcmsEvent.add(document, 'keyup', onClickEscape);
                if (!this.isInModal)
                    AcmsEvent.add(this.target, Modal.EVENT_HIDE, onHide3);
            }
            let toggles = Selectors.qa('[data-toggle=modal]', this.target);
            if (toggles) {
                let cb1 = function (e2) {
                        let currentDialog = Selectors.closest(e2.target, '.modal-dialog'),
                            targetDialog = document.querySelector(e2.target.getAttribute('data-target') + ' .modal-dialog');
                        if (!currentDialog)
                            return;
                        targetDialog.acmsData('previousDialog', currentDialog, false);
                        Classes.addClass(currentDialog, 'aside');
                        let stackedDialogCount = Selectors.qa('.modal.in .modal-dialog.aside').length;
                        if (stackedDialogCount <= 5) {
                            Classes.addClass(currentDialog, 'aside-' + stackedDialogCount);
                        }
                    },
                    onHide2 = function (e) {
                        for (m = 0; m < toggles.length; m++)
                            AcmsEvent.remove(toggles[m], 'click', cb1);
                    };

                for (j = 0; j < toggles.length; j++) {
                    AcmsEvent.add(toggles[j], 'click', cb1);
                }
                //AcmsEvent.add(this.target, Modal.EVENT_HIDE, onHide2);
            }
            let closers = Selectors.qa('[data-dismiss="modal"]', this.target),
                i;
            let clickClose = function (e) {
                    return self.hide(e);
                },
                onHide = function (e) {
                    for (i = 0; i < closers.length; i++)
                        AcmsEvent.remove(closers[i], 'click', clickClose);
                };
            for (i = 0; i < closers.length; i++) {
                AcmsEvent.add(closers[i], 'click', clickClose);
            }
            //AcmsEvent.add(this.target, EVENT_HIDE, onHide);
        }

        /**
         * Deaktiviert den Event-Listener
         *
         * Wurde der Event-Listener für die Escape-Taste aktiviert, wird er
         * hiermit wieder ausgeschaltet.
         * 
         * @private
         * @memberof Modal
         */
        _off() {
            let self = this;
            let closers = Selectors.qa('[data-dismiss=modal]', this.target),
                i;
            let clickClose = function (e) {
                return self.hide(e);
            };
            for (i = 0; i < closers.length; i++) {
                AcmsEvent.add(closers[i], 'click', clickClose);
            }
        }

        /**
         * Findet das Target
         * 
         * @private
         * @memberof Modal
         */
        _findTarget() {
            let target, o = this.options, t = o.target;
            if(!t) {
                return;
            }
            if(t instanceof HTMLElement) {
                this.target = target;
                return;
            }
            target = Selectors.q(t);
            if (null === target) {
                return;
            }
            let parent = target.parentNode && target.parentNode.tagName;
            if (parent && parent.toLowerCase() !== 'body') {
                document.body.appendChild(target);
            }
            this.target = target;
        }

        /**
         * finally hide modal
         *
         * @private
         * @memberof Modal
         */
        _hideModal() {
            let self = this,
                o = self.options;
            setTimeout(function () {
                Classes.removeClass(self.target, 'animated,' + o.animateOut);
                ShowHide.hide(self.target);
                //Classes.removeClass(self.target, 'animated,fadeOutDownBig');
            }, 600);
            this._overlay(function () {

                if (!self.isInModal)
                    Classes.removeClass(self.$body, 'modal-open');

                if (self.options.remote) {
                    self.target.acmsData(Modal.NS, '');
                }
                let e = AcmsEvent.createCustom(Modal.EVENT_HIDDEN);
                AcmsEvent.dispatch(self.element, e);
                AcmsEvent.dispatch(self.target, e);
                if (typeof o.onHide === 'function') {
                    o.onHide(self.element, self.target, self);
                }
                self.emit('hide', self.element, self.target, self);
                self._cleanModals();
            });

        }

        /**
         * remove overlay from DOM
         *
         * @private
         * @memberof Modal
         */
        _removeOverlay(){
            if(self.isInModal) {
                return;
            }
            if (this.$overlay) document.body.removeChild(this.$overlay);
            this.$overlay = null;
            Selectors.qa('.modal-overlay').forEach(function (v) {
                let parent = v.parentNode;
                if (parent) {
                    parent.removeChild(v);
                } else {
                    document.body.removeChild(v);
                }
            });
        }

        /**
         * Create/remove overlay
         *
         * @param {CallableFunction}    cb  On done cb
         * 
         * @private
         * @memberof Modal
         */
        _overlay(cb) {
            let self = this,
                ins = 'in,animate,modal-overlay',
                animate = 'modal-overlay,fade';
            if (this.isShown && this.options.overlay && !this.isInModal) {
                let overlay = document.createElement('div');
                Classes.addClass(overlay, animate);
                document.body.insertBefore(overlay, document.body.firstChild);
                this.$overlay = overlay;
                Classes.addClass(this.$overlay, ins);
                if (cb) cb();
            } else if (!this.isShown && this.$overlay && !this.isInModal) {
                setTimeout(function () {
                    Classes.removeClass(self.$overlay, 'in');
                    self._removeOverlay();
                    if (cb) cb();
                }, 600);
            } else if (cb) {
                cb();
            }
        }

        /**
         * Clean nested modals
         *
         * @private
         * @memberof Modal
         */
        _cleanModals() {
            let modals = Selectors.qa('.modal-dialog'),
                i;
            for (i = 0; i < modals.length; i++) {
                let dialog = modals[i];
                let prev = dialog.acmsData('previousDialog');
                if (prev !== null && typeof prev === "object") {
                    continue;
                }
                let cls = ['aside', 'aside-1', 'aside-2', 'aside-3', 'aside-4', 'aside-5'];
                for (let j = 0; j < cls.length; j++) {
                    Classes.removeClass(dialog, cls[j]);
                }
            }
        }

        /**
         * Adjust overlay
         *
         * @private
         * @memberof Modal
         */
        _adjustOverlay() {
            if (Classes.hasClass(this.target, 'sheet')) {
                return;
            }
            this.$overlay.style.height = 0;
            this.$overlay.style.height = this.target.scrollHeight + 'px';
        }

        /**
         * Prepare instance
         *
         * @private
         * @memberof Modal
         */
        _prepare() {
            let self = this, o = self.options, element = self.element;
            self._isShown = false;
            self.isInModal = Selectors.closest(element, '.modal');
            self._findTarget();
            self.$body = document.documentElement || document.body;
            if (self.isInModal && Selectors.closest(self.target, '.modal')) {
                document.body.appendChild(self.target);
                self._findTarget();
            }
        }

    }

    return Modal;
});
