/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/ui/informations/popover", [
	"core/base",
	"tools/string/unique-id",
	"tools/is-visible",
	"core/classes",
	"core/selectors",
	"templates/template-engine",
	"events/event",
	"core/acms"
], (Base, UniqueId, isVisible, Classes, Selectors, TemplateEngine, AcmsEvent, A) => {

	/**
	 * Popover
	 *
	 * @class
	 * @extends {Base}
	 * @author qm-b <https://bitbucket.org/qm-b/>
	 */
	class Popover extends Base {

		/**
		 * Module Namespace
		 *
		 * @readonly
		 * @static
		 * @memberof Popover
		 */
		static get NS() {
			return "acms.apps.ui.informations.popover";
		}

		/**
		 * Module Name
		 *
		 * @readonly
		 * @static
		 * @memberof Popover
		 */
		static get MODULE() {
			return "Popover";
		}

		/**
		 * Default Template
		 *
		 * @readonly
		 * @static
		 * @memberof Popover
		 */
		static get TEMPLATE() {
			return "templates/html/ui/informations/popover.hbs";
		}

		static get css() {
			return "media/css/ui/popover";
		}

		/**
		 * Default Options
		 *
		 * @readonly
		 * @static
		 * @memberof Popover
		 */
		static get DEFAULT_OPTIONS() {
			return {
				placement: 'top',
				trigger: 'click',
				content: '', // string, function or selector
				removeContent: true, // remove content wrapper if the content is a selector?
				template: Popover.TEMPLATE,
				//animation: true,
				title: '',
				//delay: 100,
				dismiss: true, // create dismiss button?
				html: true,
				container: false, // Optional HTML Container. Otherwise document.body will be used
				onCreate: function(popover, self) {},
				onOpen: function(popover, self) {},
				onClose: function(popover, self) {},
				customClass: '', // 'primary', 'danger', ..
				id: null, // ID des Popover oder leer lassen für automatische ID (empfohlen),
				customData: null, // optional custom data to be assigned to the template. Variable is ignored in default template.
				classNames: {
					active: 'open'
				},
				selectors: {
					active: '.open',
					dismiss: '[data-dismiss="popover"]'
				},
				alwaysRemove: false, // remove always on close?
				dismissOutside: true, // dismiss on click outside popover?
				dismissResize: true, // dismiss on resize?
				css: Popover.css,
				themecss: false
			};
		}

		/**
		 * Module init
		 *
		 * @static
		 * 
		 * @param {HTMLElement} element		Popover Trigger Element
		 * @param {Object|null} options		Optional custom Options
		 * 
		 * @returns	{Popover} New instance
		 * 
		 * @memberof Popover
		 */
		static init(element, options) {
			let m = new Popover(element, options);
			m.element.acmsData(Popover.NS, m);

			return m;
		}

		/**
		 * Creates an instance of Popover.
		 * 
		 * @param {HTMLElement} element		Popover Trigger Element
		 * @param {Object|null} options		Optional custom Options
		 * 
		 * @memberof Popover
		 */
		constructor(element, options) {
			super(element, options);
			this._initialize();
		}

		/**
		 * Popover title
		 *
		 * @memberof Popover
		 */
		get title() {
			return this._title;
		}

		set title(title) {
			if(typeof title !== 'string') {
				title = "";
			}
			this._title = title;
		}

		/**
		 * HTML ID of the current Popover
		 *
		 * @readonly
		 * @memberof Popover
		 */
		get id() {
			let self = this, o = self.options;
			if(!self._id) {
				if(o.id) {
					self._id = o.id;
				}
				else self._id = UniqueId('popover');
			}
			return self._id;
		}

		get rect() {
			if(this.element)
			return this.element.getBoundingClientRect();
			return {};
		}

		get scroll() {
			return {
                y: window.pageYOffset || document.documentElement.scrollTop,
                x: window.pageXOffset || document.documentElement.scrollLeft
            };
		}

		get dimension() {
			return {
                w: (this.popover ? this.popover.offsetWidth : 0),
                h: (this.popover ? this.popover.offsetHeight : 0)
            };
		}

		get creating() {
			return this._creating;
		}

		get content() {
			let self = this, o = self.options;
			if(typeof o.content === 'function') {
				return o.content(self.element, self.popover, self);
			}
			if(typeof o.content === 'string' && o.content.indexOf('#') === 0) {
				let ele = document.querySelector(o.content), content;
                if(ele) {
					content = (o.html ? ele.innerHTML : ele.textContent);
					if(o.removeContent) {
						setTimeout(() => {
							ele.parentNode.removeChild(ele);
						}, 500);
					}
					return content;
                }
			}
			
			return '';
		}

		
        get reversePosition() {
            let pos = this.options.placement;
            if (/top/.test(pos)) {
                return 'bottom';
            } else if (/bottom/.test(pos)) {
                return 'top';
            } else if (/left/.test(pos)) {
                return 'right';
            } else if (/right/.test(pos)) {
                return 'left';
			}
			return '';
        }

		/**
		 * Open popover
		 *
		 * @memberof Popover
		 */
		open(pos) {
			let self = this, o = self.options;
			if(!self.popover) {
				if(self.creating) {
					return;
				}
				let cb = () => {
					if(typeof o.onOpen === 'function') {
						o.onOpen(self.popover, self);
					}
				};
				return self._createPopover(pos, cb);
			}
			if(self.isOpen()) {
				return;
			}
			self._applyStyle(pos);
			self._fixStyles();
			if(typeof o.onOpen === 'function') {
				o.onOpen(self.popover, self);
			}
		}

		/**
		 * Close popover
		 *
		 * @memberof Popover
		 */
		close() {
			let self = this,
				o = self.options,
				cls = o.classNames.active;
			if(!self.popover) {
				return;
			}
			if(!self.isOpen()) {
				return;
			}
			Classes.removeClass(self.popover, cls);
			Classes.removeClass(self.popover, 'in');
			if(typeof o.onClose === 'function') {
				o.onClose(self.popover, self);
			}
			if(o.alwaysRemove) {
				self.remove();
			}
		}

		/**
		 * Toggle visibility
		 *
		 * @memberof Popover
		 */
		toggle() {
			if(this.isOpen()) {
				return this.close();
			}
			return this.open();
		}

		/**
		 * Is Popover open/visible?
		 *
		 * @returns {Boolean} `true` if the popover is open
		 * 
		 * @memberof Popover
		 */
		isOpen() {
			let self = this,
				o = self.options,
				cls = o.classNames.active;
			if(!self.popover) {
				return false;
			}
			return Classes.hasClass(self.popover, cls);
		}

		/**
		 * Remove Popover from DOM
		 *
		 * @memberof Popover
		 */
		remove() {
			if(this.popover) {
				this.close();
				this._off();
				this.container.removeChild(this.popover);
				this.popover = null;
				this._id = false;
			}
		}
		
		/**
		 * Is Popover in viewport?
		 *
		 * @returns {Boolean}	`true` if in viewport, `false` otherwise
		 * 
		 * @memberof Popover
		 */
		isInViewport() {
			if(!this.popover) return false;
            return isVisible(this.popover, true);
        }


		/**
		 * Create popover
		 *
		 * @param {String} pos
		 * @param {function} callback
		 * 
		 * @memberof Popover
		 * @private
		 */
		_createPopover(pos, callback) {
			let self = this, o = self.options;
			if(self.creating) {
				return;
			}
			self._creating = true;
			let template = o.template || Popover.TEMPLATE,
				tdata = {
					title: self.title,
					content: self.content,
					dismiss: o.dismiss,
					closeText: A.Translator.t('Schließen'),
					customClass: o.customClass,
					customData: o.customData,
					id: self.id
				},
				cb = (content) => {
					self.popover = document.getElementById(self.id);
					self._applyStyle(pos);
					self._on();
					self._creating = false;
					if(typeof o.onCreate === 'function') {
						o.onCreate(self.popover, self);
					}
					if(typeof callback === 'function') {
						callback();
					}
				};
			TemplateEngine.compileResource(template, tdata, cb, self.container);
		}

		/**
		 * Apply Styles
		 *
		 * @param {String|null} pos Optional position
		 * 
		 * @memberof Popover
		 * @private
		 */
		_applyStyle(pos) {
			let self = this, o = self.options;
            let placement = pos || o.placement;
            Classes.addClass(self.popover, placement + ',' + o.classNames.active);
            let linkDim = {w: self.element.offsetWidth, h: self.element.offsetHeight};
            let td = self.dimension;
            let toolDim = {w: td.w, h: td.h};
            let scrollYOffset = self.scroll.y;
			let scrollXOffset = self.scroll.x;
			self.popover.style = '';
            if (/top/.test(placement)) {
                self.popover.style.top = self.rect.top + scrollYOffset - toolDim.h + 'px';
                self.popover.style.left = (self.rect.left + scrollXOffset - (toolDim.w / 2) + (linkDim.w / 2)) + 'px';

            } else if (/bottom/.test(placement)) {
                self.popover.style.top = self.rect.top + scrollYOffset + linkDim.h + 'px';
                self.popover.style.left = self.rect.left + scrollXOffset - toolDim.w / 2 + linkDim.w / 2 + 'px';

            } else if (/left/.test(placement)) {
                self.popover.style.top = self.rect.top + scrollYOffset - toolDim.h / 2 + linkDim.h / 2 + 'px';
                self.popover.style.left = self.rect.left + scrollXOffset - toolDim.w + 'px';

            } else if (/right/.test(placement)) {
                self.popover.style.top = self.rect.top + scrollYOffset - toolDim.h / 2 + linkDim.h / 2 + 'px';
                self.popover.style.left = self.rect.left + scrollXOffset + linkDim.w + 'px';
            }
            let modal = Selectors.closest(self.element, '.modal');
            if(modal) {
                self.popover.style.zIndex = modal.style.zIndex + 10;
			}
		}

		/**
		 * Fix Styles
		 *
		 * @memberof Popover
		 * @private
		 */
		_fixStyles() {
			let self = this, o = self.options, pos;
            let placement = null;
            if (!self.isInViewport()) {
				pos = self.reversePosition;
				self._applyStyle(pos);
			}
		}

		/**
		 * Turn on event listener
		 * 
		 * @memberof Popover
		 * @private
		 */
		_on() {
			let self = this,
				o = self.options;
			AcmsEvent.on(self.popover, 'click', o.selectors.dismiss, self._onTriggerClose, true);
		}

		/**
		 * Turn off event listener
		 *
		 * @memberof Popover
		 * @private
		 */
		_off() {
			let self = this,
				o = self.options;
			AcmsEvent.off(self.popover, 'click', o.selectors.dismiss, self._onTriggerClose);
		}

		/**
		 * Setup Event Listener
		 *
		 * @memberof Popover
		 * @private
		 */
		listen() {
			let self = this, o = self.options, element = self.element;
            let events = ('onmouseleave' in element) ? ['mouseenter', 'mouseleave'] : ['mouseover', 'mouseout'];
            if (/hover/.test(o.trigger)) {
                AcmsEvent.add(element, events[0], self._onTriggerOpen);
                if (!o.dismiss) {
                    AcmsEvent.add(element, events[1], self._onTriggerClose);
                }
			}
			if (/click/.test(o.trigger)) {
                AcmsEvent.add(element, 'click', self._onTriggerToggle);
                
			}
			if (/focus/.test(o.trigger)) {
                AcmsEvent.add(element, 'focus', self._onTriggerOpen);
                if (!o.dismiss) {
                    AcmsEvent.add(element, 'blur', self._onTriggerClose);
                }
            }

            if (o.dismissOutside) {
                AcmsEvent.add(document, 'click', self._onClickDocument);
            }

            AcmsEvent.add(window, 'resize', self._onResize);
		}

		/**
		 * On Click Document Event Handler
		 *
		 * @param {Event} e Event
		 * 
		 * @memberof Popover
		 * @private
		 */
		_onClickDocument(e) {
			let self = this;
			if(!self.isOpen()) {
				return;
			}
			let target = e.target;
			if(!Selectors.closest(target, '.popover')) {
				self.close();
			}
		}

		/**
		 * On Resize Callback
		 *
		 * @memberof Popover
		 * @private
		 */
		_onResize() {
			let self = this, o = self.options;
			if(!self.isOpen()) {
				return;
			}
			if(o.dismissResize) {
				return self.close();
			}
			self._applyStyle();
			self._fixStyles();
		}

		/**
		 * On Trigger open Event Handler
		 *
		 * @param {Event} e		Event
		 * 
		 * @memberof Popover
		 * @private
		 */
		_onTriggerOpen(e) {
			let self = this;
			e.preventDefault();
			e.stopPropagation();
			self.open();
		}
		
		/**
		 * On Trigger Close Event Handler
		 *
		 * @param {Event} e		Event
		 * 
		 * @memberof Popover
		 * @private
		 */
		_onTriggerClose(e) {
			let self = this;
			e.preventDefault();
			e.stopPropagation();
			self.close();
		}
				
		/**
		 * On Trigger Toggle Event Handler
		 *
		 * @param {Event} e		Event
		 * 
		 * @memberof Popover
		 * @private
		 */
		_onTriggerToggle(e) {
			let self = this;
			e.preventDefault();
			e.stopPropagation();
			self.toggle();
		}
		

		/**
		 * Prepare Module
		 *
		 * @memberof Popover
		 * @private
		 */
		_prepare() {
			let self = this,
				o = self.options,
				element = self.element;
			self.title = (o.title || element.getAttribute('title') || element.getAttribute('aria-label') || '');
			self.container = (o.container || document.body);
		}

	}

	return Popover;
});
