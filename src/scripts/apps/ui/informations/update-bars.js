/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/ui/informations/update-bars", [
	"core/base",
	"tools/string/unique-id",
	'tools/object/extend',
	"events/event",
	"templates/template-engine",
	"core/acms"
], (Base, UniqueId, Extend, AcmsEvent, TemplateEngine, A) => {

	/**
	 * Update Bars
	 *
	 * @class
	 * @extends {Base}
	 * @author qm-b <https://bitbucket.org/qm-b/>
	 */
	class UpdateBars extends Base {

		/**
		 * Module Namespace
		 *
		 * @readonly
		 * @static
		 * @memberof UpdateBars
		 */
		static get NS() {
			return "ui.informations.update-bars";
		}


		/**
		 * Module Name
		 *
		 * @readonly
		 * @static
		 * @memberof UpdateBars
		 */
		static get MODULE() {
			return "Update-Bars";
		}

		/**
		 * Custom Stylesheet
		 *
		 * @readonly
		 * @static
		 * @memberof UpdateBars
		 */
		static get css() {
			return "media/css/ui/update-bars";
		}

		/**
		 * Default Options
		 *
		 * @readonly
		 * @static
		 * @memberof UpdateBars
		 */
		static get DEFAULT_OPTIONS() {
			return {
				autoRemove: false,
				autoTimer: 5,
				css: UpdateBars.css,
				themecss: false,
				remove: "[data-trigger=remove]",
				template: "templates/html/ui/informations/update-bar.hbs",
				
				/**
				 * Before Remove Callback
				 *
				 * @param {UpdateBars} self		Instance
				 * 
				 * @returns {Boolean} `true` to process, `false` to cancel
				 * 
				 * @memberof UpdateBars
				 */
				beforeRemove: (self) => {return true;},
				
				/**
				 * On Removed Callback
				 *
				 * @param {UpdateBars} self		Instance
				 * 
				 * @memberof UpdateBars
				 */
				onRemoved: (self) => {}
			};
		}

		/**
		 * Default Config for `UpdateBars.make`
		 *
		 * @readonly
		 * @static
		 * @memberof UpdateBars
		 */
		static get DEFAULT_CONFIG() {
			return {
				message: '', // Message to display.
				type: UpdateBars.SUCCESS,
				icon: 'acms-icon acms-icon-check-circle'
			};
		}

		/**
		 * Typ "default"
		 *
		 * @readonly
		 * @static
		 * @memberof UpdateBars
		 */
		static get DEFAULT() {
			return "default";
		}

		/**
		 * Typ "info"
		 *
		 * @readonly
		 * @static
		 * @memberof UpdateBars
		 */
		static get INFO() {
			return "info";
		}

		/**
		 * Typ "success"
		 *
		 * @readonly
		 * @static
		 * @memberof UpdateBars
		 */
		static get SUCCESS() {
			return "success";
		}

		/**
		 * Typ "primary"
		 *
		 * @readonly
		 * @static
		 * @memberof UpdateBars
		 */
		static get PRIMARY() {
			return "primary";
		}
		
		/**
		 * Typ "warning"
		 *
		 * @readonly
		 * @static
		 * @memberof UpdateBars
		 */
		static get WARNING() {
			return "warning";
		}

		/**
		 * Typ "danger"
		 *
		 * @readonly
		 * @static
		 * @memberof UpdateBars
		 */
		static get DANGER() {
			return "danger";
		}

		/**
		 * Typ "royal"
		 *
		 * @readonly
		 * @static
		 * @memberof UpdateBars
		 */
		static get ROYAL() {
			return "royal";
		}

		/**
		 * Make an Update Bar
		 *
		 * @static
		 * 
		 * @param {Object} 				conf		See `UpdateBars.DEFAULT_CONFIG` for possible properties
		 * @param {Object} 				options		Custom Options. See `UpdateBars.DEFAULT_OPTIONS` for possible properties
		 * @param {HTMLElement} 		appendTo	Element to appent the Update Bar to.
		 * @param {CallableFunction}	cb 			to be called if the update bar has been finished
		 * 
		 * @memberof UpdateBars
		 */
		static make(conf, options, appendTo, cb) {
			let o = Extend.flat({}, true, UpdateBars.DEFAULT_OPTIONS, options);
			let c = Extend.flat({}, true, UpdateBars.DEFAULT_CONFIG, conf);
			c.id = UniqueId();
			let template = o.template,
				tData = {
					bar: c,
					config: {
						autoRemove: o.autoRemove
					}
				},
				callback = () => {
					let element = document.getElementById(c.id);
					if(!element) {
						throw new Error('Bar not found!');
					}
					let bar = UpdateBars.init(element, options);
					if(typeof cb === "function") cb(bar);
				};
			TemplateEngine.compileResource(template, tData, callback, appendTo);
			require(["http/css-loader"], (CssLoader) => {
				CssLoader.load(o.css);
			});
		}

		/**
		 * Module Inits
		 *
		 * @static
		 * 
		 * @param {HTMLElement} element		Update Bars Element
		 * @param {Object|null} options		Optional custom options
		 * 
		 * @returns {UpdateBars}	New Instance
		 * 
		 * @memberof UpdateBars
		 */
		static init(element, options) {
			let m = new UpdateBars(element, options);
			m.element.acmsData(UpdateBars.NS, m);
			
			return m;
		}

		/**
		 * Creates an instance of UpdateBars.
		 * 
		 * @param {HTMLElement} element		Update Bars Element
		 * @param {Object|null} options		Optional custom options
		 * 
		 * @memberof UpdateBars
		 */
		constructor(element, options) {
			super(element, options);
			this._initialize();

		}

		get timeout() {
			return this._timeout || 5000;
		}
		set timeout(sec) {
			this._timeout = parseInt(sec) * 1000;
		}

		/**
		 * Remove Update Bar
		 *
		 * @returns {Boolean}	`true` if removed, `false` if canceled
		 * 
		 * @memberof UpdateBars
		 */
		remove() {
			let self = this, o = self.options;
			if(typeof o.beforeRemove === "function") {
				let res = o.beforeRemove(self);
				if(false === res) {
					return false;
				}
			}
			self.element.parentNode.removeChild(self.element);
			if(typeof o.onRemoved === "function") {
				o.onRemoved(self);
			}

			return true;
		}

		listen() {
			let self = this, o = self.options;
			if(true === o.autoRemove)  {
				return;
			}
			AcmsEvent.on(self.element, 'click', o.remove, self._onClickRemove, true);
		}

		buildOptions(options) {
			let o = super.buildOptions(options);
			o.autoTimer = parseInt(o.autoTimer);
			o.autoRemove = (o.autoRemove === true || o.autoRemove === 'true' || parseInt(o.autoTimer) === 1);

			return o;
		}

		_makeTimer() {
			let self = this;
			self.timer = setTimeout(() => {
				self.remove();
			}, self.timeout);

		}

		/**
		 * On Click Remove Event Handler
		 *
		 * @param {Event} e	
		 * @memberof UpdateBars
		 */
		_onClickRemove(e) {
			let self = this;
			e.preventDefault();
			e.stopPropagation();
			self.remove();
			return false;
		}

		_prepare() {
			let self = this, o = self.options;
			self.timeout = o.autoTimer;
			if(true === o.autoRemove) {
				self._makeTimer();
			}
		}

	}

	return UpdateBars;
});
