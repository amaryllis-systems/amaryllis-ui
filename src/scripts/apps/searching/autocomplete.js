/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/searching/autocomplete", [
	"core/base",
    "tools/string/unique-id",
    "tools/string/highlight",
	"core/classes",
	"core/selectors",
	"events/event",
	"core/acms"
], (Base, UniqueId, highlight, Classes, Selectors, AcmsEvent, A) => {

	/**
	 * Auto Complete Search
	 * 
	 * Class provides Auto-Complete Handling for Text-/Search-Fields
	 *
	 * @class
	 * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
	 */
	class AutoComplete extends Base {

		/**
		 * Module Namespace
		 *
		 * @readonly
		 * @static
		 * @memberof AutoComplete
		 */
		static get NS() {
			return "apps.searching.autocomplete";
		}

		/**
		 * Module Name
		 *
		 * @readonly
		 * @static
		 * @memberof AutoComplete
		 */
		static get MODULE() {
			return "Auto-Complete Search";
		}

		/**
		 * Module Stylesheet
		 *
		 * @readonly
		 * @static
		 * @memberof AutoComplete
		 */
		static get css() {
			return "media/css/ui/autocomplete";
		}

		/**
		 * Default Options
		 *
		 * @readonly
		 * @static
		 * @memberof AutoComplete
		 */
		static get DEFAULT_OPTIONS() {
			return {
				remote: null, // String remote URL
				menuClass: '',
				updateValue: false,
				minChars: 3,
				offsetLeft: 0,
                offsetTop: 1,
                li: 'autocomplete-suggestion', // string - CSS Klasse für result item
				selected: 'selected', // string - CSS Klasse für gewählte Werte
                data: null, // NULL | Object - Zusätzliche Daten
                            
                /**
                 * Render the list Item
                 *
                 * @param {Object|Number|String}    item        Result Item. Shoul be an Object with properties 
                 *                                              value and label at least, but can even be the 
                 *                                              value of the search result.
                 * @param {String}                  search      Search Term
                 * @param {HTMLUListElement}        list        Result List
                 * @param {AutoComplete}            self        Instance of AutoComplete Class
                 * 
                 * @returns {HTMLLIElement}
                 * 
                 * @memberof AutoComplete
                 */
                renderItem: (item, search, list, self) => {
                    return self.renderItem(item, search, list);
				},

				/**
				 * onSelect Trigger, wenn das Element angeklickt wird
				 * 
				 * @param {Event}           e       Das Event
				 * @param {String}          value   Der Wert
				 * @param {String}          label   Das Label, wenn gesetzt, andernfalls das Value
				 * @param {HTMLLIElement}   item    Das HTML-Element, auf das geklickt wurde
                 * @param {AutoComplete}    self    Instance of AutoComplete Class
                 * 
				 * @memberof AutoComplete
				 */
				onSelect: (e, value, label, item, self) => {
				},
				
				onResponse: (xhr, response, statusText, self) => {},

				css: AutoComplete.css,
				themecss: false
			};
		}

		/**
		 * Module Init
		 *
		 * @static
		 * 
		 * @param {HTMLElement} element		HTML Element
		 * @param {Object|null} options		Optional custom Options
		 * 
		 * @returns	{AutoComplete}	New Instance
		 * 
		 * @memberof AutoComplete
		 */
		static init(element, options) {
			let m = new AutoComplete(element, options);
			m.element.acmsData(AutoComplete.NS, m);
			
			return m;
		}

		/**
		 * Creates an instance of AutoComplete.
		 * 
		 * @param {HTMLElement} element		HTML Element
		 * @param {Object|null} options		Optional custom Options
		 * 
		 * @memberof AutoComplete
		 */
		constructor(element, options) {
			super(element, options);
			this.container  =
			this.value      =
			this.lastValue  =
			this.isInModal	=
			this.cache		=
			this.term       = null;
			this._initialize();

        }
        
        /**
         * Render the list Item
         *
         * @param {Object|Number|String}    item        Result Item. Shoul be an Object with properties 
         *                                              value and label at least, but can even be the 
         *                                              value of the search result.
         * @param {String}                  search      Search Term
         * @param {HTMLUListElement}        list        Result List
         * 
         * @returns {HTMLLIElement}
         */
        renderItem(item, search, list) {
            let self = this, 
                isObj = (typeof item === "object"),
                o = self.options,
                val = isObj ? item.value : item,
                label = isObj ? item.label : item,
                li = document.createElement('li');
            Classes.addClass(li, o.li);
            li.setAttribute('data-value', val);
            li.setAttribute('data-label', label);
            li.acmsData('autocompleteitem', item);
            li.innerHTML = self.highlightMatch(search, label);
            list.appendChild(li);

            return li;
        }
        
        /**
         * Highlight match
         *
         * @param {String} search       Search Term
         * @param {String} label        Result Label
         * 
         * @returns {String}    label with highlighted search string
         * 
         * @memberof AutoComplete
         */
        highlightMatch(search, label) {
            return highlight(search, label, {tag: 'b', className: ''});
        }

		/**
		 * Setup Event Listeners
		 *
		 * @memberof AutoComplete
		 */
		listen() {
            let self = this,
				o = self.options, 
				container = self.container;
            AcmsEvent.add(container, 'resize', self._onResize);
            AcmsEvent.add(self.element, 'keydown', self._onKeyDown);
            AcmsEvent.add(self.element, 'keyup', self._onKeyUp);

            AcmsEvent.on(self.ul, 'mouseleave', o.li, self._onMouseLeave, true);
            AcmsEvent.on(self.ul, 'mouseover', o.li, self._onMouseOver, true);
            AcmsEvent.on(self.ul, 'mousedown', o.li, self._onMouseDown, true);
		}

		/**
		 * Update the container
		 *
		 * @param {boolean} 			resize	Currently resizing?
		 * @param {HTMLElement|null} 	next	Optional next element.
		 * @memberof AutoComplete
		 */
		updateContainer(resize, next) {
            let self = this,
				o = self.options,
				element = self.element, 
				container = self.container;
            let rect = element.getBoundingClientRect();
            container.style.left = Math.round(rect.left + (window.pageXOffset || document.documentElement.scrollLeft) + o.offsetLeft) + 'px';
            container.style.top = Math.round(rect.bottom + (window.pageYOffset || document.documentElement.scrollTop) + o.offsetTop) + 'px';
            container.style.width = Math.round(rect.right - rect.left) + 'px'; // outerWidth
            if (!resize) {
                container.style.display = 'block';
                if(self.isInModal) {
                    container.style.zIndex = self.isInModal.style.zIndex + 10;
                }
                self.ul.setAttribute('aria-hidden', 'false');
                if (!container.maxHeight) {
                    container.maxHeight = parseInt((window.getComputedStyle ? getComputedStyle(container, null) : container.currentStyle).maxHeight);
                }
                if (!container.suggestionHeight) {
					container.suggestionHeight = container.querySelector('.'+ o.li).offsetHeight;
				} else {
                    if (!next) {
                        container.scrollTop = 0;
					} else {
                        let scrTop = container.scrollTop, selTop = next.getBoundingClientRect().top - container.getBoundingClientRect().top;
                        if (selTop + container.suggestionHeight - container.maxHeight > 0) {
                            container.scrollTop = selTop + container.suggestionHeight + scrTop - container.maxHeight;
						} else if (selTop < 0) {
							container.scrollTop = selTop + scrTop;
						}
					}
				}
            }
		}
		
		/**
         * Build the result list container
         *
         * @memberof AutoComplete
         */
        _buildContainer() {
            let self = this, 
                o = self.options;
            self.ul = document.createElement('ul');
            self.ulID = UniqueId('autocomplete');
            self.container = document.createElement('div');
            Classes.addClass(self.container, 'autocomplete v1');
            if (o.menuClass) {
                Classes.addClass(self.container, o.menuClass);
            }
            self.container.setAttribute('aria-live', 'polite');
            self.container.setAttribute('aria-hidden', 'true');
            self.ul.setAttribute('id', self.ulID);
            self.ul.id = self.ulID;
            self.container.appendChild(self.ul);
            document.body.appendChild(self.container);
		}
		
        /**
         * On Mouse Leave Event Handler
         *
         * @param {Event} e Mouse Leave Event
         * 
         * @memberof AutoComplete
         */
        _onMouseLeave(e) {
            let self = this,
                    o = self.options,
                    selected = e.target;
            if(!Selectors.matches(selected, '.' + o.li)) {
                selected = Selectors.closest(selected, '.' + o.li);
            }
            if (selected && Classes.hasClass(selected, o.selected)) {
                setTimeout(() => {
                    Classes.removeClass(selected, o.selected);
                }, 20);
            }
		}
        
        
        /**
         * On Mouse Over Event Handler
         *
         * @param {Event} e Mouse Enter Event
         * @memberof AutoComplete
         */
        _onMouseOver(e) {
            let self = this,
                    o = self.options,
                    container = self.container,
                    target = e.target,
                    selected = container.querySelector('.' + o.li + '.' + o.selected);
            if(!Selectors.matches(target, '.' + o.li)) {
                target = Selectors.closest(target, '.' + o.li);
            }
            if (selected) {
                setTimeout(() => {
                    Classes.removeClass(selected, o.selected);
                }, 20);
            }
            Classes.addClass(target, o.selected);
		}
        
        /**
         * On Mouse Down Event Handler
         *
         * @param {Event} e Mouse Down Event
         * @memberof AutoComplete
         */
        _onMouseDown(e) {
            let target = e.target,
                    self = this,
                    container = self.container,
                    o = self.options;
            if (!Classes.hasClass(target,  o.li)) {
                let closest = Selectors.closest(target, '.' + o.li);
                if(closest) {
                    target = closest;
                }
            }
            if (Classes.hasClass(target, o.li)) {
                let v = target.getAttribute('data-value'),
                        l = target.getAttribute('data-label');

                self.element.value = v;
                o.onSelect(e, v, l, target, self);
                container.style.display = 'none';
                container.setAttribute('aria-hidden', 'true');
            }
		}
		
        /**
         * On Key Down Event Handler
         *
         * @param {Event} e Key Down Event
         * @memberof AutoComplete
         */
        _onKeyDown(e) {
            let key = e.keyCode || e.which,
                    self = this,
                    o = self.options,
                    selector = '.' + o.li + '.' + o.selected,
                    container = self.container;
            // down (40), up (38)
            if ((key === 40 || key === 38) && container.innerHTML) {
                let next, sel = container.querySelector(selector);
                if (!sel) {
                    next = (key === 40) ? 
                            container.querySelector('.' + o.li) :
                            container.childNodes[container.childNodes.length - 1]; // first : last
                    Classes.addClass(next, o.selected);
                    if(o.updateValue) {
                        self.element.value = next.getAttribute('data-value');
                    }
                } else {
                    next = (key === 40) ? sel.nextSibling : sel.previousSibling;
                    if (next) {
                        Classes.removeClass(sel, o.selected);
                        Classes.addClass(next, o.selected);
                        if(o.updateValue) {
                            self.element.value = next.getAttribute('data-value');
                        }
                    } else {
                        Classes.removeClass(sel, o.selected);
                        if(o.updateValue) {
                            self.element.value = self.lastValue;
                        }
                        next = 0;
                    }
                }
                self.updateContainer(false, next);
                return false;
            }
            // esc
            else if (key === 27) {
                if(o.updateValue) {
                    self.element.value = self.lastValue;
                }
                container.style.display = 'none';
            }
            // enter
            else if (key === 13 || key === 9) {
                let sel = container.querySelector(selector);
                if (sel && container.style.display != 'none') {
                    o.onSelect(e, sel.getAttribute('data-value'), sel.getAttribute('data-label'), sel, self);
                    setTimeout(() => {
                        container.style.display = 'none';
                    }, 20);
                }
            }
		}
		
        /**
         * On Key Up Event Handler
         *
         * @param {Event} e Key up Event
         * @memberof AutoComplete
         */
        _onKeyUp(e) {
            let key = e.keyCode || e.which;
            let self = this, 
                o = this.options,
                container = self.container;
            if (!key || (key < 35 || key > 40) && key != 13 && key != 27) {
                let val = self.element.value;
                if (val.length >= o.minChars) {
                    if (val != self.lastValue) {
                        self.lastValue = val;
                        clearTimeout(self.timer);
                        if (o.cache) {
                            if (val in self.cache) {
                                self._suggest(self.cache[val]);
                                return;
                            }
                            for (let i = 1; i < val.length - o.minChars; i++) {
                                let part = val.slice(0, val.length - i);
                                if (part in self.cache && !self.cache[part].length) {
                                    self._suggest([]);
                                    return;
                                }
                            }
                        }
                        self.timer = setTimeout(() => {
                            self._request(val);
                        }, o.delay);
                    }
                } else {
                    self.lastValue = val;
                    container.style.display = 'none';
                }
            }
        }
        
        /**
		 * Process Request
		 *
		 * @param {String} 				term		Search Term
		 * 
		 * @memberof AutoComplete
		 */
		_request(term) {
			let self = this, 
				fData = new FormData(), 
                o = self.options, k;
            fData.append('term', term);
            if(typeof o.data === "object" && null !== o.data) {
                for(k in o.data) {
                    if(o.data.hasOwnProperty(k)) {
                        fData.append(k, o.data[k]);
                    }
                }
            }
            require(['http/request'], (Request) => {
                Request.post(o.remote, fData, self._suggest, self._fail, self._always);
            });
        }
        
        /**
         * Process Remote data
         *
         * @param {Object} data
         * @memberof AutoComplete
         */
        _suggest(data) {
            let self = this,
                o = self.options,
                container = self.container,
                val = self.element.value, 
                i, len = typeof data.length !== "undefined" ? data.length : Object.keys(data).length;
            self.cache[val] = data;
            self.ul.innerHTML = '';
            if (len && val.length >= o.minChars) {
                if(typeof data.length !== "undefined") {
                    for (i = 0; i < len; i++) {
                        o.renderItem(data[i], val, self.ul, self);
                    }
                } else {
                    for(let prop in data) {
                        if(data.hasOwnProperty(prop)) {
                            o.renderItem(data[prop], val, self.ul, self);
                        }
                    }
                }
                self.updateContainer(false);
            } else {
                container.style.display = 'none';
            }
		}
		
		
        /**
         * Ausführung bei fehlgeschlagener Abfrage
         * 
		 * Wird ausgeführt, wenn keine Antwort vom Server kommt bzw die 
         * Anfrage fehlschlägt
         * 
         * @param {Object}  xhr    		Das XHR Abfrage Object
         * @param {String}	textStatus 	Text Status Message
		 * 
		 * @memberof AutoComplete
         */
        _fail (xhr, textStatus) {
            A.Logger.writeLog(textStatus);
        }
        
        /**
         * Auführung bei jeder Abfrage
         * 
         * @param {String} textStatus Text Status
         * 
		 * @memberof AutoComplete
         */
        _always (xhr, response, textStatus) {
			let self = this,
                o = self.options;
			A.Logger.writeLog(textStatus);
			if(typeof o.onResponse === 'function') {
				o.onResponse(xhr, response, textStatus, self);
			}
            self._processing = false;
        }
        
		
		/**
		 * On Resize Callback
		 *
		 * @param {Event} 	e		Resize Event
		 * 
		 * @memberof AutoComplete
		 */
		_onResize(e) {
			self.updateContainer(e);
		}

		/**
		 * Prepare
		 *
		 * @memberof AutoComplete
		 */
		_prepare() {
			let self = this, element = self.element;
			self.isInModal = Selectors.closest(element, '.modal');
            self._buildContainer();
            if (element.tagName.toUpperCase() === 'INPUT') {
                self.element.setAttribute('autocomplete', 'off');
            }
            self.cache = {};
		}

	}

	return AutoComplete;
});
