/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("apps/searching/live-search", [
	"core/base",
	"tools/string/unique-id",
	"tools/string/highlight",
	"core/classes",
	"core/selectors",
	"events/event",
	"templates/template-engine",
	"core/acms"
], (Base, UniqueId, highlight, Classes, Selectors, AcmsEvent, TemplateEngine, A) => {

	/**
	 * Live Search
	 * 
	 * Process Live Search Requests/Responses to Amaryllis-CMS and Modules.
	 *
	 * @class
	 * @extends {Base}
	 * @author qm-b <https://bitbucket.org/qm-b/>
	 */
	class LiveSearch extends Base {

		/**
		 * Module Namespace
		 *
		 * @readonly
		 * @static
		 * @memberof LiveSearch
		 */
		static get NS() {
			return "apps.searching.live-search";
		}


		/**
		 * Module Name
		 *
		 * @readonly
		 * @static
		 * @memberof LiveSearch
		 */
		static get MODULE() {
			return "Live Search";
		}

		/**
		 * Module Stylesheet
		 *
		 * @readonly
		 * @static
		 * @memberof LiveSearch
		 */
		static get css() {
			return "media/css/ui/search";
		}
		/**
		 * Default Options
		 *
		 * @readonly
		 * @static
		 * @memberof LiveSearch
		 */
		static get DEFAULT_OPTIONS() {
			return {
				// Minimum Characters before start search
				minChars: 3,
				// result type
				type: 'standard',
				// Class Name Map
				classNames: {
					wrapper: 'search v1',
					focus: 'focus',
					hidden: 'hidden',
					loading: 'loading',
					results: 'results',
					result: 'result',
					active: 'active'
				},
				// Selectors map
				selectors: {
					wrapper: '.search.v1',
					hidden: '.hidden',
					loading: '.loading',
					results: '.results',
					result: '.result',
					active: '.active'
				},
				// custom request data to process along search
				requestdata: {
					op: 'search'
				},
				// field map
				fieldMap: {
					// term field to be used with search request
					term: 'term'
				},
				templates: {
					'message': 'templates/html/ui/searching/live-search/message.hbs',
					'standard': 'templates/html/ui/searching/live-search/standard.hbs',
					'category': 'templates/html/ui/searching/live-search/category.hbs'
				},
				messages: {
					noResult: A.Translator._('Es wurden keine Ergebnisse gefunden.'),
					connectionFailed: A.Translator._('Die Verbindung zum Server ist fehlgeschlagen.')
				},
                /**
                 * Callback after rendering the list Item
                 *
                 * @param {Object|Number|String}    item        Result Item. Shoul be an Object with properties 
                 *                                              value and label at least, but can even be the 
                 *                                              value of the search result.
                 * @param {String}                  search      Search Term
                 * @param {HTMLUListElement}        list        Result List
                 * @param {LiveSearch}            	self        Instance of LiveSearch Class
                 * 
                 * @returns {HTMLElement}
				 * @memberof LiveSearch
                 */
                onRenderItem: (item, search, list, self) => {
                    return self.renderItem(item, search, list);
				},

				
				/**
				 * Before Request Callback
				 * 
				 * Callback will be triggered before sending request to Server
				 *
				 * @param {FormData} 	fData	Form Data
				 * @param {String} 		term	Search Term
				 * @param {LiveSearch} 	self	LiveSearch instance
				 * @memberof LiveSearch
				 */
				beforeRequest: (fData, term, self) => {},

				/**
				 * onSelect Trigger, wenn das Element angeklickt wird
				 * 
				 * @param {Event}           e       Das Event
				 * @param {String}          value   Der Wert
				 * @param {String}          label   Das Label, wenn gesetzt, andernfalls das Value
				 * @param {HTMLLIElement}   item    Das HTML-Element, auf das geklickt wurde
                 * @param {LiveSearch}    	self    Instance of LiveSearch Class
                 * 
				 * @memberof LiveSearch
				 */
				onSelect: (e, value, label, item, self) => {
				},
				
				onResponse: (xhr, response, statusText, self) => {},

				/**
				 * On Clear Results Callback
				 *
				 * @param {HTMLElement} 		results		Results Container
				 * @param {HTMLInputElement} 	field		Seach Field
				 * @param {LiveSearch} 			self		Instance of LiveSearch Class
				 * @memberof LiveSearch
				 */
				onClear: (results, field, self) => {},

				css: LiveSearch.css,
				themecss: false
			};
		}

		/**
		 * Module Init
		 *
		 * @static
		 * @param {HTMLElement} element		Wrapper Element
		 * @param {Object|null} options		optional custom options
		 * 
		 * @returns {LiveSearch}	New Instance
		 * 
		 * @memberof LiveSearch
		 */
		static init(element, options) {
			let m = new LiveSearch(element, options);
			m.element.acmsData(LiveSearch.NS, m);
			
			return m;
		}

		/**
		 * Creates an instance of LiveSearch.
		 * 
		 * @param {HTMLElement} element		Wrapper Element
		 * @param {Object|null} options		optional custom options
		 * 
		 * @memberof LiveSearch
		 */
		constructor(element, options) {
			super(element, options);
			this._initialize();

		}

		/**
		 * Checks if the results are hidden
		 *
		 * @returns	{Boolean}
		 * 
		 * @memberof LiveSearch
		 */
		isHidden() {
			let self = this, o = self.options;

			return Classes.hasClass(self.results, o.classNames.hidden);
		}
		
		/**
		 * Clear Results
		 *
		 * @memberof LiveSearch
		 */
		clear() {
			let self = this, o = self.options;
			self.results.innerHTML = '';
			if(typeof o.onClear === 'function') {
				o.onClear(self.results, self.field, self);
			}
		}

		/**
		 * Makes the results container visible
		 *
		 * @memberof LiveSearch
		 */
		showResults() {
			let self = this, o = self.options;
			Classes.removeClass(self.results, o.classNames.hidden);
			self.results.setAttribute('aria-hidden', 'false');
		}

		/**
		 * Makes the results container invisible
		 * 
		 * @param {Boolean}	clear 	Clear results?
		 *
		 * @memberof LiveSearch
		 */
		hideResults(clear) {
			let self = this, o = self.options;
			Classes.addClass(self.results, o.classNames.hidden);
			self.results.setAttribute('aria-hidden', 'true');
			if(clear) {
				self.clear();
			}
		}

		/**
		 * Show no results message
		 *
		 * @memberof LiveSearch
		 */
		showNoResult() {
			let self = this, o = self.options;
			self.clear();
			let template = o.templates.message,
				cb = () => {
					if(self.isHidden()) {
						self.showResults();
					}
				},
				data = {message: {
					type: 'empty',
					content: o.messages.noResult
				}};
			TemplateEngine.compileResource(template, data, cb, self.results);
		}


		listen() {
			let self = this, 
				o = self.options,
				selectors = o.selectors;
			AcmsEvent.add(self.field, 'focus', self._onFocus);
			AcmsEvent.add(self.field, 'blur', self._onBlur);

            AcmsEvent.on(self.results, 'mouseleave', selectors.result, self._onMouseLeave, true);
            AcmsEvent.on(self.results, 'mouseover', selectors.result, self._onMouseOver, true);
            AcmsEvent.on(self.results, 'mousedown', selectors.result, self._onMouseDown, true);
		}

		/**
		 * On Focus Event Handler
		 *
		 * @param {Event} e		Focus Event
		 * 
		 * @memberof LiveSearch
		 */
		_onFocus(e) {
			let self = this, o = self.options;
			Classes.addClass(self.field, o.classNames.focus);
		}

		/**
		 * On Blur Event Handler
		 *
		 * @param {Event} e		Blur Event
		 * 
		 * @memberof LiveSearch
		 */
		_onBlur(e) {
			let self = this, o = self.options;
			Classes.removeClass(self.field, o.classNames.focus);
		}

        /**
         * On Mouse Leave Event Handler
         *
         * @param {Event} e Mouse Leave Event
         * 
         * @memberof LiveSearch
         */
        _onMouseLeave(e) {
            let self = this,
				o = self.options,
				selected = e.target,
				selectors = o.selectors,
				classNames = o.classNames;
            if(!Selectors.matches(selected, selectors.result)) {
                selected = Selectors.closest(selected, selectors.result);
            }
            if (selected && Classes.hasClass(selected, classNames.active)) {
                setTimeout(() => {
                    Classes.removeClass(selected, classNames.active);
                }, 20);
            }
		}
        
        
        /**
         * On Mouse Over Event Handler
         *
         * @param {Event} e Mouse Enter Event
         * @memberof LiveSearch
         */
        _onMouseOver(e) {
            let self = this,
				o = self.options,
				container = self.results,
				target = e.target,
				selectors = o.selectors,
				classNames = o.classNames,
				selected = container.querySelector(selectors.result + selectors.active);
            if(!Selectors.matches(target, selectors.result)) {
                target = Selectors.closest(target, selectors.result);
            }
            if (selected) {
                setTimeout(() => {
                    Classes.removeClass(selected, classNames.active);
                }, 20);
            }
            Classes.addClass(target, classNames.active);
		}
        
        /**
         * On Mouse Down Event Handler
         *
         * @param {Event} e Mouse Down Event
         * @memberof LiveSearch
         */
        _onMouseDown(e) {
            let target = e.target,
                    self = this,
                    container = self.results,
					o = self.options,
					selectors = o.selectors,
					classNames = o.classNames;
            if (!Classes.hasClass(target,  selectors.result)) {
                target = Selectors.closest(target, selectors.result);
			}
			if(!target) {
				e.preventDefault();
				e.stopPropagation();
				return false;
			}
            if (Classes.hasClass(target, classNames.result)) {
                let v = target.getAttribute('data-value'),
                        l = target.getAttribute('data-label');
				if(o.updateValue && self.hidden) {
					self.field.value = l;
					self.hidden.value = v;
				}
                o.onSelect(e, v, l, target, self);
                self.hideResults();
            }
		}
		
        /**
         * On Key Down Event Handler
         *
         * @param {Event} e Key Down Event
		 * 
         * @memberof LiveSearch
         */
        _onKeyDown(e) {
            let key = e.keyCode || e.which,
                    self = this,
                    o = self.options,
					selectors = o.selectors,
					classNames = o.classNames,
					next, selected,
					container = self.results;
			let all = Selectors.qa(selectors.result, container);
			if(self.isHidden() || all.length === 0) {
				return;
			}
			switch(key) {
				case 38: // arrow up
					selected = container.querySelector(selectors.result + selectors.active);
					if(!selected) {
						next = all[all.length - 1];
						Classes.addClass(next, selectors.active);
						if(o.updateValue && self.hidden) {
							self.hidden.value = next.getAttribute('data-value');
							self.field.value = next.getAttribute('data-label');
						}
					} else {
						next = selected.previousSibling;
						if (next) {
							Classes.removeClass(selected, selectors.active);
							Classes.addClass(next, selectors.active);
							if(o.updateValue && self.hidden) {
								self.hidden.value = next.getAttribute('data-value');
								self.field.value = next.getAttribute('data-label');
							}
						} else {
							Classes.removeClass(selected, selectors.active);
							if(o.updateValue && self.hidden) {
								self.field.value = self.lastLabel;
								self.hidden.value = self.lastValue;
							}
							next = 0;
						}
					}
					break;
				case 40: // arrow down
					selected = container.querySelector(selectors.result + selectors.active);
					if (!selected) {
						next = container.querySelector(selectors.result);
						Classes.addClass(next, o.selected);
						if(o.updateValue && self.hidden) {
							self.hidden.value = next.getAttribute('data-value');
							self.field.value = next.getAttribute('data-label');
						}
					} else {
						next = selected.nextSibling;
						if (next) {
							Classes.removeClass(selected, selectors.active);
							Classes.addClass(next, selectors.active);
							if(o.updateValue && self.hidden) {
								self.hidden.value = next.getAttribute('data-value');
								self.field.value = next.getAttribute('data-label');
							}
						} else {
							Classes.removeClass(selected, selectors.active);
							if(o.updateValue && self.hidden) {
								self.field.value = self.lastLabel;
								self.hidden.value = self.lastValue;
							}
							next = 0;
						}
					}
					break;
				case 27: // ESC
					if(o.updateValue && self.hidden) {
						self.hidden.value = self.lastValue;
						self.hidden.value = self.lastLabel;
					}
					self.hideResults();
					break;
				case 9:
				case 13:
					selected = container.querySelector(selectors.active);
					if(selected) {
						if(o.updateValue && self.hidden) {
							self.hidden.value = selected.getAttribute('data-value');
							self.hidden.value = selected.getAttribute('data-label');
						}
						o.onSelect(e, selected.getAttribute('data-value'), selected.getAttribute('data-label'), selected, self);
						setTimeout(() => {
							self.hideResults();
						}, 20);
					}
					break;
			}
		}
		

		/**
		 * Highlight match in search String
		 *
		 * @param {String} term	Search Term
		 * @param {String} str	String to search in
		 * 
		 * @returns	{String}	Highlighted String
		 * 
		 * @memberof LiveSearch
		 */
		highlightMatch(term, str) {
			return highlight(term, str, {tag: 'b', className: ''});
		}

        /**
		 * Process Request
		 *
		 * @param {String} 				term		Search Term
		 * 
		 * @memberof LiveSearch
		 */
		_request(term) {
			let self = this, 
				fData = new FormData(), 
                o = self.options, k;
            fData.append(o.fieldMap.term, term);
            if(typeof o.requestdata === "object" && null !== o.requestdata) {
                for(k in o.requestdata) {
                    if(o.requestdata.hasOwnProperty(k)) {
                        fData.append(k, o.requestdata[k]);
                    }
                }
			}
			if(typeof o.beforeRequest === 'function') {
				o.beforeRequest(fData, term, self);
			}
            require(['http/request'], (Request) => {
                Request.post(o.remote, fData, self._done, self._fail, self._always);
            });
        }
		
		/**
		 * On Response Callback
		 *
		 * @param {Object} 	data	Response Data
		 * 
		 * @memberof LiveSearch
		 */
		_done(data) {
			let self = this,
                o = self.options,
                val = self.element.value, 
				prop, method,
				len = typeof data.length !== "undefined" ? data.length : Object.keys(data).length;
            self.cache[val] = data;
            self.clear();
            if (len && val.length >= o.minChars) {
				switch(o.type) {
					case 'category':
						method = 'appendCategoryItem';
						break;
					case 'standard':
						method = 'appendStandard';
						break;
				}
                if(typeof data.length !== "undefined") {
                    for (prop = 0; prop < len; prop++) {
						o.renderItem(data[prop], val, self.ul, self);
						self[method](data[prop]);
                    }
                } else {
                    for(prop in data) {
                        if(data.hasOwnProperty(prop)) {
							o.renderItem(data[prop], val, self.ul, self);
							self[method](data[prop]);
                        }
                    }
                }
                self.updateContainer(false);
            } else {
                self.showNoResult();
            }
		}

		appendStandard(item) {
			let self = this,
				o = self.options;
			let template = o.templates.standard,
				cb = () => {
					if(self.isHidden()) {
						self.showResults();
					}
					o.onRenderItem(item, results, self.ul, self);
				},
				data = {item: item};
			TemplateEngine.compileResource(template, data, cb, self.results);
		}

		appendCategoryItem(item) {
			let self = this,
				o = self.options,
				category = item.name || 'undefined',
				prop;
			let cat = Selectors.q('[data-category="'+category+'"]', self.results),
				callback = () => {
					let c = Selectors.q('[data-category="'+category+'"]', self.results),
						results = item.results;
					if(typeof results.length !== "undefined") {
						for (prop = 0; prop < results.length; prop++) {
							self._renderItem(results[prop], c);
						}
					} else {
						for (prop in results) {
							if(results.hasOwnProperty(prop)) {
								self._renderItem(results[prop], c);
							}
						}
					}
				};
			if(cat) {
				callback();
			}
			self.appendCategory(item, callback);
		}

		appendCategory(category, callback) {
			let self = this,
				o = self.options;
			if(typeof callback !== 'function') {
				callback = () => {};
			}
			let template = o.templates.category,
				cb = () => {
					if(self.isHidden()) {
						self.showResults();
					}
					callback();
				},
				data = {category: {
					name: category.name,
					id: category.id
				}};
			TemplateEngine.compileResource(template, data, cb, self.results);

		}

		/**
		 * Render a single item
		 *
		 * @param {Object} 				item		Item setup from Search result
		 * @param {HTMLElement} 		category	Category Wrapper Element
		 * @param {CallableFunction} 	callback	Optional callback
		 * 
		 * @memberof LiveSearch
		 */
		_renderItem(item, category, callback) {
			let self = this,
				o = self.options,
				results = Selectors.q(o.selectors.results, category);
			if(typeof callback !== 'function') {
				callback = () => {
					o.renderItem(item, results, self);
				};
			}
			let template = o.templates.standard,
				cb = () => {
					if(self.isHidden()) {
						self.showResults();
					}
					callback();
				},
				data = {item: item};
			TemplateEngine.compileResource(template, data, cb, results);
			
		}
		
        /**
         * Ausführung bei fehlgeschlagener Abfrage
         * 
		 * Wird ausgeführt, wenn keine Antwort vom Server kommt bzw die 
         * Anfrage fehlschlägt
         * 
         * @param {Object}  xhr    		Das XHR Abfrage Object
         * @param {String}	textStatus 	Text Status Message
		 * 
		 * @memberof LiveSearch
         */
        _fail (xhr, textStatus) {
            A.Logger.writeLog(textStatus);
        }
        
        /**
         * Auführung bei jeder Abfrage
         * 
         * @param {String} textStatus Text Status
         * 
		 * @memberof LiveSearch
         */
        _always (xhr, response, textStatus) {
			let self = this,
                o = self.options;
			A.Logger.writeLog(textStatus);
			if(typeof o.onResponse === 'function') {
				o.onResponse(xhr, response, textStatus, self);
			}
            self._processing = false;
        }
        
		
		/**
         * Build the result list container
         *
         * @memberof LiveSearch
         */
        _buildContainer() {
            let self = this, 
                o = self.options;
            self.results = document.createElement('div');
            self.containerId = UniqueId('livesearch');
            //self.container = document.createElement('div');
			Classes.addClass(self.results, o.classNames.results);
			Classes.addClass(self.results, o.classNames.hidden);
            self.results.setAttribute('aria-live', 'polite');
            self.results.setAttribute('aria-hidden', 'true');
            self.results.setAttribute('id', self.containerId);
            self.results.id = self.containerId;
            self.parent.appendChild(self.results);
            //document.body.appendChild(self.results);
		}
		
		/**
		 * Prepare Class instance
		 *
		 * @memberof LiveSearch
		 */
		_prepare() {
			let self = this, element = self.element, o = self.options;
			self.isInModal = Selectors.closest(element, '.modal');
			if(Classes.hasClass(element, o.selectors.wrapper)) {
				self.parent = element;
				self.field = Selectors.q('.field', element);
			} else {
				self.parent = Selectors.closest(element, o.selectors.wrapper);
				if (element.tagName.toUpperCase() === 'INPUT') {
					self.element.setAttribute('autocomplete', 'off');
					self.field = element;
				} else {
					self.field = Selectors.q('.field', element);
				}
			}
			self.hidden = Selectors.q('input[type="hidden"]', element);
            self._buildContainer();
            self.cache = {};
		}

	}

	return LiveSearch;
});
