/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/form/model-form", [
    "core/base",
    "tools/string/string-tool",
    "core/classes",
    "core/selectors",
    "events/event",
    "core/acms"
], (Base, StringTool, Classes, Selectors, AcmsEvent, A) => {

    /**
     * Model Form
     *
     * Class responsible for managing default ACMS Forms.
     * The form is responsible to listen on the triggers 
     * of the form
     *
     * __Deal with events of the class__:
     *
     * ``` js
     * let opts = {
     *   'onReset': (element, self) => {
     *       console.log(element);
     *   }
     * }, myform = Selectors.q('#my-form');
     * let form = ModelForm.init(myform, opts);
     * form.on('reset', (data, element, self) => {
     *     console.log(element, data);
     * });
     * AcmsEvent.add(ModelForm.EVENT_RESET, (e) => {
     *     let detail = e.detail;
     *     console.log(detail);
     *     console.log(detail.data);
     *     console.log(detail.scope);
     * });
     * ```
     *
     *
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     * 
     * @property {HTMLFormElement} element The form
     * @property {Object} options Current options build from init
     * @property {Boolean} isInModal Is the form inside a modal overlay?
     * @property {HTMLElement|null} modal the modal overlay or `null` if not inside modal.
     * 
     */
    class ModelForm extends Base {
        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof ModelForm
         */
        static get NS() {
            return "acms.apps.form.model-form";
        }

        /**
         * Module name
         *
         * @readonly
         * @static
         * @memberof ModelForm
         */
        static get MODULE() {
            return "Model Form";
        }

        /**
         * Event "before trigger save"
         *
         * Event ill be fired on Form element, before the save process
         * starts
         *
         * @readonly
         * @static
         * @memberof ModelForm
         */
        static get EVENT_BEFORE_TRIGGER_SAVE() {
            return ModelForm.NS + "before-trigger-save";
        }
        static get EVENT_AFTER_TRIGGER_SAVE() {
            return ModelForm.NS + "after-trigger-save";
        }

        static get EVENT_BEFORE_TRIGGER_SAVE_NEW() {
            return ModelForm.NS + "before-trigger-save-new";
        }

        static get EVENT_AFTER_TRIGGER_SAVE_NEW() {
            return ModelForm.NS + "after-trigger-save-new";
        }
        static get EVENT_BEFORE_TRIGGER_SAVE_CLOSE() {
            return ModelForm.NS + "before-trigger-save-close";
        }
        static get EVENT_AFTER_TRIGGER_SAVE_CLOSE() {
            return ModelForm.NS + "after-trigger-save-close";
        }
        static get EVENT_BEFORE_TRIGGER_ABORT() {
            return ModelForm.NS + "before-trigger-abort";
        }
        static get EVENT_AFTER_TRIGGER_ABORT() {
            return ModelForm.NS + "after-trigger-abort";
        }

        /**
         * Event "Acms before Save"
         * 
         * Some kind of default event, that should be triggered on saving any form.
         * This is currently primary used to save the editor instnances, if needed (e.g. tinymce)
         *
         * @readonly
         * @static
         * @memberof ModelForm
         */
        static get EVENT_ACMS_BEFORE_SAVE() {
            return "acms.before-save-form";
        }

        /**
         * Event fired on form reset
         *
         * @readonly
         * @static
         * @memberof ModelForm
         */
        static get EVENT_RESET() {
            return ModelForm.NS + ".reset";
        }

        /**
         * Event "succeed"
         *
         * Event ill be fired on Form element, if
         * the submission is successfully processed
         *
         * @readonly
         * @static
         * @memberof ModelForm
         */
        static get EVENT_SUCCEED() {
            return ModelForm.NS + ".succeed";
        }

        /**
         * Event "failed"
         *
         * Event ill be fired on Form element, if
         * the submission is not successfully processed
         * (e.g. invalid values)
         *
         * @readonly
         * @static
         * @memberof ModelForm
         */
        static get EVENT_FAILED() {
            return ModelForm.NS + ".error";
        }

        /**
         * Scope "save"
         *
         * Saving the form
         *
         * @readonly
         * @static
         * @memberof ModelForm
         */
        static get SCOPE_SAVE() {
            return "save";
        }

        /**
         * Scope "save-new"
         *
         * Saving the from and reset the form back
         *
         * @readonly
         * @static
         * @memberof ModelForm
         */
        static get SCOPE_SAVE_NEW() {
            return "save-new";
        }

        /**
         * Scope "save-close"
         *
         * Saving the form and close the modal/go back to previous page.
         *
         * @readonly
         * @static
         * @memberof ModelForm
         */
        static get SCOPE_SAVE_CLOSE() {
            return "save-close";
        }

        /**
         * Scope "abort"
         *
         * Abort the form, reset back and close modal/go back to previous page
         *
         * @readonly
         * @static
         * @memberof ModelForm
         */
        static get SCOPE_ABORT() {
            return "abort";
        }

        /**
         * Default Options
         *
         * @readonly
         * @static
         * @memberof ModelForm
         */
        static get DEFAULT_OPTIONS() {
            return {
                beforeSave: form => {
                    return true;
                },
                previousUrl: null,
                remote: null,
                modal: ".modal",
                modalDismiss: '[data-dismiss="modal"]',
                disabled: "disabled",
                saveButton: '[data-trigger="save"]',
                saveNewButton: '[data-trigger="save-new"]',
                saveCloseButton: '[data-trigger="save-close"]',
                abortButton: '[data-trigger="abort"]',
                model: 'model', // name of the model in response.
                reload: true,
                key: "id",
                onSuccess: (form, self, response) => { },
                onAbort: (form, self) => { }
            };
        }

        /**
         * Module Init
         *
         * @static
         *
         * @param {HTMLFormElement} element Form
         * @param {Object} options Optional custom options
         *
         * @returns {ModelForm} New Instance
         *
         * @memberof ModelForm
         */
        static init(element, options) {
            let m = new ModelForm(element, options);
            m.element.acmsData(ModelForm.NS, m);

            return m;
        }

        /**
         * Creates an instance of ModelForm.
         *
         * @param {HTMLFormElement} element Form
         * @param {Object} options Optional custom options
         *
         * @memberof ModelForm
         */
        constructor(element, options) {
            super(element, options);
            this._initialize();
        }

        /**
         * Is Form inside a modal overlay?
         *
         * @var {Boolean}
         *
         * @readonly
         * @memberof ModelForm
         */
        get isInModal() {
            return this._isInModal;
        }

        /**
         * modal overlay
         *
         * @var {HTMLElement|null}
         *
         * @readonly
         * @memberof ModelForm
         */
        get modal() {
            return this._modal;
        }

        /**
         * Current Scope
         *
         * @var {String|null}
         *
         * @readonly
         * @memberof ModelForm
         */
        get scope() {
            return this._scope;
        }

        /**
         * Overlay
         * 
         * @var {Overlay}
         *
         * @readonly
         * @memberof ModelForm
         */
        get overlay() {
            return this._overlay;
        }

        /**
         * Reset the form
         *
         * Resetting the form and fires the events
         *
         * @memberof ModelForm
         */
        reset() {
            this.element.reset();
            this._triggerEvent(ModelForm.EVENT_RESET, "reset");
        }

        /**
         * Setup Event Listener
         *
         * @memberof ModelForm
         * @private
         */
        listen() {
            const self = this;
            const element = self.element;
            const o = self.options;
            AcmsEvent.on(
                element,
                "click",
                o.saveButton,
                self._onClickSave,
                true
            );
            AcmsEvent.on(
                element,
                "click",
                o.saveCloseButton,
                self._onClickSaveClose,
                true
            );
            AcmsEvent.on(
                element,
                "click",
                o.saveNewButton,
                self._onClickSaveNew,
                true
            );
            AcmsEvent.on(
                element,
                "click",
                o.abortButton,
                self._triggerAbort,
                true
            );
            if (self.isInModal) {
                AcmsEvent.on(
                    self.modal,
                    "click",
                    o.modalDismiss,
                    self._onClickReset,
                    true
                );
            }
        }

        /**
         * On click "save" button
         *
         * @param {Event} e  Event
         * 
         * @memberof ModelForm
         * @private
         */
        _onClickSave(e) {
            AcmsEvent.stop(e);
            const self = this;
            const o = self.options;
            let target = e.target;
            if (self._processing) {
                return false;
            }
            self._processing = true;
            if (!Selectors.matches(target, o.saveButton)) {
                target = Selectors.closest(target, o.saveButton);
            }
            if (Classes.hasClass(target, o.disabled)) {
                return false;
            }
            const form = Selectors.closest(target, 'form[role="form"]');
            self._scope = ModelForm.SCOPE_SAVE;
            self._triggerDefaultBeforeSave();
            const evt = self._triggerEvent(ModelForm.EVENT_BEFORE_TRIGGER_SAVE, "save", { relatedEvent: e });
            if (evt.defaultPrevented) {
                self._processing = false;
                return false;
            }
            if (false === self._beforeSave(form)) {
                self._processing = false;
                return false;
            }
            self.emit('beforeSave', form, self);
            self._process(e, form);
        }

        /**
         * On click "save and close" button
         *
         * @param {Event} e  Event
         * 
         * @memberof ModelForm
         * @private
         */
        _onClickSaveClose(e) {
            AcmsEvent.stop(e);
            const self = this;
            const o = self.options;
            let target = e.target;
            if (self._processing) {
                return false;
            }
            self._processing = true;
            if (!Selectors.matches(target, o.saveCloseButton)) {
                target = Selectors.closest(target, o.saveCloseButton);
            }
            if (Classes.hasClass(target, o.disabled)) {
                return false;
            }
            const form = Selectors.closest(target, 'form[role="form"]');
            self._scope = ModelForm.SCOPE_SAVE_CLOSE;
            self._triggerDefaultBeforeSave(form);
            const evt = self._triggerEvent(
                ModelForm.EVENT_BEFORE_TRIGGER_SAVE_CLOSE,
                "saveClose"
            );
            if (evt.defaultPrevented) {
                return false;
            }
            if (false === self._beforeSave()) {
                return false;
            }
            self._process(e, form);
        }
    
        /**
         * On click "save and new" button
         *
         * @param {Event} e  Event
         * 
         * @memberof ModelForm
         * @private
         */
        _onClickSaveNew(e) {
            AcmsEvent.stop(e);
            const self = this;
            const o = self.options;
            let target = e.target;
            if (self._processing) {
                return false;
            }
            self._processing = true;
            if (!Selectors.matches(target, o.saveNewButton)) {
                target = Selectors.closest(target, o.saveNewButton);
            }
            if (Classes.hasClass(target, o.disabled)) {
                return false;
            }
            const form = Selectors.closest(target, 'form[role="form"]');
            self._scope = ModelForm.SCOPE_SAVE_NEW;
            self._triggerDefaultBeforeSave();
            const evt = self._triggerEvent(ModelForm.EVENT_BEFORE_TRIGGER_SAVE_NEW, "saveNew");
            if (evt.defaultPrevented) {
                return false;
            }
            if (false === self._beforeSave(form)) {
                return false;
            }
            self._process(e, form);
        }
            
        _onClickAbort(e) {
            AcmsEvent.stop(e);
            const self = this;
            const o = self.options;
            let target = e.target;
            if (self._processing) {
                return false;
            }
            self.scope = ModelForm.SCOPE_ABORT;
            const evt = self._triggerEvent(ModelForm.EVENT_BEFORE_TRIGGER_ABORT, "beforeAbort", self.element, self);
            if (evt.defaultPrevented) {
                return false;
            }
            self.reset();
            self._triggerEvent(ModelForm.EVENT_AFTER_TRIGGER_ABORT, "abort", self.element, self);
            if (self.isInModal) {
                var modal = Selectors.closest(element, ".modal"),
                    dismiss;
                if (modal) dismiss = modal.querySelector("[data-dismiss=modal]");
                if (dismiss && dismiss !== target) AcmsEvent.fireClick(dismiss);
                return false;
            }
            if (
                typeof o.previousUrl === "string" &&
                StringTool.isUrl(o.previousUrl)
            ) {
                window.location.href = o.previousUrl;
            } else {
                history.go(-1);
            }
        }
        
        /**
         * on Click reset Form
         *
         * @param {Event} e Event
         *
         * @memberof ModelForm
         */
        _onClickReset(e) {
            if (e) AcmsEvent.stop(e);
            this.reset();
        }

        _triggerDefaultBeforeSave() {
            this._triggerEvent(ModelForm.EVENT_ACMS_BEFORE_SAVE);
        }

        /**
         * Trigger an ModelForm Event
         *
         * The method triggers the following:
         *
         * - if the option "on" + ucfirst(shortName) is a function,
         *   the function will be called with parameters HTMLFormElement
         *   and ModelForm instance.
         * - the in-class event will be emitted (.on('shortname', HTMLFormElement, ModelForm))
         * - the global event on the Form interface will be triggered with parameter `name`
         *
         * ``` js
         * this._triggerEvent(ModelForm.EVENT_RESET, 'reset');
         * // -> dispatches the event 'acms.apps.form.model-form.reset' on form element
         * // -> triggers options['onReset'](element, self)
         * // -> triggers this.emit('reset', data, element, self);
         * ```
         *
         * Deal with the Events:
         *
         * ``` js
         * let opts = {
         *   'onReset': (element, self) => {
         *       console.log(element);
         *   }
         * }, myform = Selectors.q('#my-form');
         * let form = ModelForm.init(myform, opts);
         * form.on('reset', (data, element, self) => {
         *     console.log(element, data);
         * });
         * AcmsEvent.add(ModelForm.EVENT_RESET, (e) => {
         *     let detail = e.detail;
         *     console.log(detail);
         *     console.log(detail.data);
         *     console.log(detail.scope);
         * });
         * ```
         *
         * @param {String} name
         * @param {String} shortName
         * @param {Object} data
         *
         * @returns {Event} The event created by `AcmsEvent.createCustom`
         *
         * @memberof ModelForm
         */
        _triggerEvent(name, shortName, data) {
            const self = this,
                o = self.options,
                element = self.element,
                opts = {
                    relatedTarget: self.element,
                    scope: self.scope,
                    data: data || false
                };
            if (typeof shortName === "string") {
                var onName = "on" + StringTool.ucfirst(shortName);
                if (typeof o[onName] === "function") {
                    o[onName](element, self);
                }
                this.emit(shortName, data, element, self);
            }
            const e = AcmsEvent.createCustom(name, opts);
            AcmsEvent.dispatch(element, e);

            return e;
        }

        /**
         * Process Form
         *
         * @param {Event} e Event
         * @param {HTMLFormElement} form form element
         *
         * @memberof ModelForm
         * @private
         */
        _process(e, form) {
            const self = this;
            let fData,
                remote = self.action;
            if (!form && e) {
                form =
                    e.target.tagName.toUpperCase() === "FORM" ? 
                        e.target :
                        Selectors.closest(e.target, 'form[role="form"]');
            }
            if (!form) {
                form = self.element;
            }
            fData = new FormData(form);
            require(["http/request", "apps/ui/information/overlay", "notifier"], (Request, Overlay) => {
                self._overlay = Overlay.init(document.body, { closeOnEscape: false });
                self.overlay.append();
                setTimeout(() => {
                    Request.post(
                        remote,
                        fData,
                        self._done,
                        self._fail,
                        self._always
                    );
                }, 200);
            });
        }

        /**
         * On Done
         *
         * @param {Response} response  Remote response object
         *
         * @memberof ModelForm
         * @private
         */
        _done(response) {
            const self = this,
                scope = self.scope;
            A.Notifications.createFromResponse(response);
            if (response.status !== "success") {
                self._triggerEvent(ModelForm.EVENT_FAILED, "error", response);
                return;
            }
            self._triggerEvent(ModelForm.EVENT_SUCCEED, "success", response);
            switch (scope) {
                case ModelForm.SCOPE_SAVE:
                    self._onDoneSave(response);
                    break;
                case ModelForm.SCOPE_SAVE_CLOSE:
                    self._onDoneSaveClose(response);
                    break;
                case ModelForm.SCOPE_SAVE_NEW:
                    self._onDoneSaveNew(response);
                    break;
            }
            self._scope = null;
        }

        /**
         * On Done Save New
         *
         * @param {Response} response
         * @memberof ModelForm
         * @private
         */
        _onDoneSaveNew(response) {
            const self = this;
            const o = self.options;
            if (self.isInModal) {
                self.reset();
            } else if (o.reload == "true" || o.reload === true) {
                window.refresh(500);
            } else {
                self.reset();
            }
        }

        /**
         * On Done Save Close
         *
         * @param {Response} response
         * @memberof ModelForm
         * @private
         */
        _onDoneSaveClose(response) {
            const self = this;
            const o = self.options;
            if (self.isInModal && scope !== ModelForm.SCOPE_SAVE) {
                self.element.reset();
                if (self.modal) {
                    const dismiss = self.modal.querySelector("[data-dismiss=modal]");
                    if (dismiss) AcmsEvent.fireClick(dismiss);
                    if (o.reload === true || o.reload === "true") {
                        window.refresh(500);
                    }
                    return;
                }
            }
            const red = response.redirect;
            if (red && StringTool.isUrl(red)) {
                window.location.href = response.redirect;
            } else if (typeof o.redirect === "string" && StringTool.isUrl(o.redirect)) {
                window.location.href = o.redirect;
            } else if (typeof o.previousUrl === "string" && StringTool.isUrl(o.previousUrl)) {
                window.location.href = o.previousUrl;
            } else {
                history.go(-1);
            }
        }

        /**
         * On Done Save
         *
         * @param {Response} response
         * @memberof ModelForm
         * @private
         */
        _onDoneSave(response) {
            const self = this;
            const o = self.options;
            const model = response[o.model] ? response[o.model] : {};
            const id = (typeof response[self.keyField] !== 'undefined' ? response[self.keyField] : model[self.keyField]);

            if (id) {
                const idField = element.querySelector("input[name=" + key + "]");
                if (idField) {
                    idField.value = id;
                    idField.setAttribute("value", id);
                    AcmsEvent.fireChange(idField);
                }
            }
            const modelop = element.querySelector("input[name=modelop]");
            if (modelop) {
                modelop.value = "edit";
                modelop.setAttribute("value", modelop.value);
                AcmsEvent.fireChange(modelop);
            }
            const newmodel = element.querySelector("input[name=is_new_model]");
            if (newmodel) {
                newmodel.value = "0";
                newmodel.setAttribute("value", newmodel.value);
                AcmsEvent.fireChange(newmodel);
            }
            const files = Selectors.qa('input[type="file"]');
            if (files) {

                files.forEach((file) => {
                    if (file.files) {
                        const name = file.name;
                        file.value = "";
                        file.value = (model[name] || '');
                        file.acmsData('value', (model[name] || ''));
                        AcmsEvent.fireChange(file);
                    }
                });
            }

        }

        /**
         * On Request Failed callback
         *
         * @param {XMLHttpRequest} xhr
         * @param {String} textStatus
         *
         * @memberof ModelForm
         * @private
         */
        _fail(xhr, textStatus) {
            A.Logger.logWarning(textStatus);
            A.Notifications.create({
                content: textStatus,
                title: A.Translator._("Fehler!"),
                type: A.Notifications.TYPE_DANGER
            });
            this.emit("fail", xhr, textStatus, self, self.element);
        }

        /**
         * On Request finished callback
         *
         * @param {XMLHttpRequest} xhr
         * @param {String} textStatus
         *
         * @memberof ModelForm
         * @private
         */
        _always(xhr, textStatus) {
            this._processing = false;
            if (this.overlay) {
                this.overlay.remove();
            }
            switch (this.scope) {
                case ModelForm.SCOPE_SAVE:
                    this._triggerEvent(ModelForm.EVENT_AFTER_TRIGGER_SAVE, 'afterSave');
                    break;
                case ModelForm.SCOPE_SAVE_CLOSE:
                    this._triggerEvent(ModelForm.EVENT_AFTER_TRIGGER_SAVE_CLOSE, 'afterSaveClose');
                    break;
                case ModelForm.SCOPE_SAVE_NEW:
                    this._triggerEvent(ModelForm.EVENT_AFTER_TRIGGER_SAVE_NEW, 'afterSaveNew');
                    break;
            }

            this.emit("always", xhr, textStatus, self, self.element);
            A.Logger.writeLog("Formular erfolgreich übermittelt");
        }

        /**
         * Before Save Action
         *
         * @returns {Boolean}  `true` on success, `false` otherwise.
         * @memberof ModelForm
         */
        _beforeSave(form) {
            let o = this.options,
                result = true;
            form = form || this.element;
            if (typeof o.beforeSave === "function") {
                result = o.beforeSave(form);
            }
            return (result === true);
        }

        /**
         * Prepare Form
         *
         * @memberof ModelForm
         * @private
         */
        _prepare() {
            const self = this;
            const element = self.element;
            const o = self.options;
            const modal = Selectors.closest(element, o.modal);
            self._isInModal = null !== modal;
            self._modal = modal;
            self.action = o.remote && StringTool.isUrl(o.remote) ? 
                            o.remote : 
                            element.getAttribute("action");
            self.keyField = o.key;
            if (self.isInModal) {
                const abort = Selectors.q(o.abortButton, element);
                if (abort) {
                    abort.setAttribute("onclick", "");
                }
            }
        }
    }

    return ModelForm;
});
