/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/form/confirm-form", [
    "core/base",
    "core/classes",
    "core/selectors",
    "events/event",
    "core/acms"
], (Base, Classes, Selectors, AcmsEvent, A) => {

    /**
     * Confirm Form
     * 
     * Class responsible for handling confirm forms.
     *
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class ConfirmForm extends Base {

        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof ConfirmForm
         */
        static get NS() {
            return "apps.form.confirm-form";
        }

        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof ConfirmForm
         */
        static get MODULE() {
            return "Confirm Form";
        }

        /**
         * Default options
         * 
         * @type {ConfirmForm~ModuleOptions}
         *
         * @readonly
         * @static
         * @memberof ConfirmForm
         */
        static get DEFAULT_OPTIONS() {
            return {
                button: 'button[type=submit]'
            };
        }

        /**
         * Module init
         *
         * @static
         * 
         * @param {HTMLFormElement} element
         * @param {Object} options
         * 
         * @returns {ConfirmForm} New instance
         * 
         * @memberof ConfirmForm
         */
        static init(element, options) {
            let m = new ConfirmForm(element, options);
            m.element.acmsData(ConfirmForm.NS, m);
            
            return m;
        }

        /**
         * Creates an instance of ConfirmForm.
         * 
         * @param {HTMLFormElement} element
         * @param {Object} options
         * 
         * @memberof ConfirmForm
         */
        constructor(element, options) {
            super(element, options);
            this._initialize();

        }

        /**
         * Setup Event Listener
         *
         * @memberof ConfirmForm
         * @private
         */
        listen() {
            const self = this;
            const o = self.options;
            AcmsEvent.on(self.element, 'change', 'input[type=checkbox]', self._onCheckBoxChange);
            AcmsEvent.on(self.element, 'click', o.button, self._onClickSubmit, true);
        }

        /**
         * On Click Submit callback
         *
         * @param {Event} e Click Event
         * 
         * @memberof ConfirmForm
         * @private
         */
        _onClickSubmit(e) {
            AcmsEvent.stop(e);
            const self = this;
            const o = self.options;
            let target = e.target;
            if(!Selectors.matches(target, o.button)) {
                target = Selectors.closest(target, o.button);
            }
            if(!target) return false;
            let form = Selectors.closest(target, 'form[role="form"]');
            if(!form) form = self.element;
            if(Classes.hasClass(target, 'disabled') || Classes.hasClass(form, 'disabled')) {
                return false;
            }
            form.submit();
        }

        /**
         * On Checkbox change
         *
         * @param {Event} e
         * 
         * @memberof ConfirmForm
         * @private
         */
        _onCheckBoxChange(e) {

        }

    }

    return ConfirmForm;
});
