/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("apps/password/password-field", [
    "core/base",
    "core/classes",
    "core/selectors",
    "events/event",
    "core/acms"
], (Base, Classes, Selectors, AcmsEvent, A) => {

    /**
     * Password Field
     * 
     * Helper Class to handle password fields
     *
     * @class PasswordField
     * @extends {Base}
     */
    class PasswordField extends Base {

        /**
         * Module Namespace
         * 
         * @type {String}
         *
         * @readonly
         * @static
         * @memberof PasswordField
         */
        static get NS() {
            return "acms.apps.password.password-field";
        }

        /**
         * Module Name
         * 
         * @type {String}
         *
         * @readonly
         * @static
         * @memberof PasswordField
         */
        static get MODULE() {
            return "Password Field";
        }

        /**
         * Default options
         *
         * @readonly
         * @static
         * @memberof PasswordField
         */
        static get DEFAULT_OPTIONS() {
            return {
                hasRepeat: false,
                repeat: '[data-role="repeat-password"]'
            };
        }

        /**
         * Module Init
         *
         * @static
         * 
         * @param {HTMLInputElement} element    Password field
         * @param {Object|undefined} options    optional custom options
         * 
         * @returns {PasswordField} New instance
         * 
         * @memberof PasswordField
         */
        static init(element, options) {
            let m = new PasswordField(element, options);
            m.element.acmsData(PasswordField.NS, m);
            
            return m;
        }


        /**
         * Creates an instance of PasswordField.
         * 
         * @param {HTMLInputElement} element    Password field
         * @param {Object|undefined} options    optional custom options
         * 
         * @memberof PasswordField
         */
        constructor(element, options) {
            super(element, options);
            this._initialize();
        }


        _prepare() {
        }

    }

    return PasswordField;
});
