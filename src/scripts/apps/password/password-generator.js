/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('apps/password/password-generator', [
    'tools/object/extend',
    'core/selectors',
    'events/event'
], (Extend, Selectors, AcmsEvent) => {

    /**
     * Password Generator
     * 
     * A small tool to generate random passwords and serials.
     *
     * @class
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class PasswordGenerator {
        
        /**
         * Numbers
         * 
         * @type {String}
         *
         * @readonly
         * @static
         * @memberof PasswordGenerator
         */
        static get NUMS() {
            return '0123456789';
        }
        
        /**
         * Lower Alphabet
         * 
         * @type {String}
         *
         * @readonly
         * @static
         * @memberof PasswordGenerator
         */
        static get UPPER() {
            return 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }

        /**
         * Upper Alphabet
         * 
         * @type {String}
         *
         * @readonly
         * @static
         * @memberof PasswordGenerator
         */
        static get LOWER() {
            return 'abcdefghijklmnopqrstuvwxyz';
        }

        /**
         * Special Characters
         *
         * @readonly
         * @static
         * @memberof PasswordGenerator
         */
        static get SPECIAL() {
            return ':;?!@#$%^&*()_+-=';
        }

        /**
         * Default Options
         *
         * @readonly
         * @static
         * @memberof PasswordGenerator
         */
        static get DEFAULT_OPTIONS() {
            return {
                nums: true,
                upper: true,
                lower: true,
                special: true,
                length: 15,
                terms: 4,
                subterms: 5
            };
        }

        /**
         * Creates an instance of PasswordGenerator.
         * 
         * @param {Object|undefined}    options
         * @param {Boolean}             [options.nums=true]     Use Numbers?
         * @param {Boolean}             [options.upper=true]    Use upper Alphabet?
         * @param {Boolean}             [options.lower=true]    Use lower Alphabet?
         * @param {Boolean}             [options.special=true]  Use Special Characters? __NOTE__: This defaults to true, but is always ignored in serial generation.
         * @param {Number}              [options.length=15]     Length of the password. Is ignored for Serial
         * @param {Number}              [options.terms=4]       Number of terms for Serial Generation (ignored for password)
         * @param {Number}              [options.subterms=5]    Number of subterms for Serial Generation (ignored for password)
         * 
         * @memberof PasswordGenerator
         */
        constructor(options) {
            this.options = Extend.flat({}, true, PasswordGenerator.DEFAULT_OPTIONS, options);
        }
        
        /**
         * Generates a Password
         *
         * @returns {String}
         * 
         * @memberof PasswordGenerator
         */
        generatePassword() {
            let result = "",
                    o = this.options,
                    i, auch, chars, arr;
            chars = this._getChars();
            arr = chars.split('');
            for (i = 0; i < o.length; i++) {
                auch = (Math.floor((Math.random() * arr.length) + 1)) - 1;
                result += arr[auch];
            }

            return result;
        }

        /**
         * Generates a Serial
         *
         * @returns {String}
         * 
         * @memberof PasswordGenerator
         */
        generateSerial() {
            let result = "",
                o = this.options,
                result1 = "",
                i, j, k, auch, chars, arr;
            chars = this._getChars(true);
            arr = chars.split('');
            for (i = 1; i < o.terms; i++) {
                for (j = 0; j < o.subterms; j++) {
                    auch = (Math.floor((Math.random() * arr.length) + 1)) - 1;
                    result1 += arr[auch];
                }
                result += result1 + "-";
                result1 = "";
            }
            for (k = 0; k < o.subterms; k++) {
                auch = (Math.floor((Math.random() * arr.length) + 1)) - 1;
                result += arr[auch];
            }

            return result;
        }

        
        /**
         * Gibt die verfügbaren Zeichen basieredn auf den Optionen zurück
         *
         * @param {Boolean} forSerial Is the char list for serial? if so, exlude chars independent of options.
         * 
         * @returns {String}
         * 
         * @memberof PasswordGenerator
         * @private
         */
        _getChars(forSerial) {
            let chars = "",
                    o = this.options;
            if (o.nums) {
                chars += PasswordGenerator.NUMS;
            }
            if (o.lower) {
                chars += PasswordGenerator.LOWER;
            }
            if (o.upper) {
                chars += PasswordGenerator.UPPER;
            }
            if (true !== forSerial && o.special) {
                chars += PasswordGenerator.SPECIAL;
            }

            return chars;
        }

    }

    return PasswordGenerator;

});
