/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('device/os-detect', [], () => {

    const Win = window;

    const Navi = navigator || Win.navigator;

    class OSDetector {

        /**
         * Detects the OS
         * 
         * returns an Object with keys
         * 
         * - `os`
         * - `version`
         *
         * @static
         * @returns {Object}
         * @memberof OSDetector
         */
        static detect() {
            let unknown = '-',
                naviVersion = Navi.appVersion,
                naviAgent = Navi.userAgent,
                header = [
                    Navi.platform,
                    Navi.userAgent,
                    Navi.appVersion,
                    Navi.vendor,
                    window.opera
                ],
                os = unknown,
                version = unknown,
                isWindows = null,
                clients = [{
                    name: 'Windows Phone',
                    regex: /Windows Phone/,
                    version: 'OS'
                }, {
                    name: 'Windows 10',
                    regex: /(Windows 10.0|Windows NT 10.0)/,
                    version: 'NT'
                }, {
                    name: 'Windows 8.1',
                    regex: /(Windows 8.1|Windows NT 6.3)/,
                    version: 'NT'
                }, {
                    name: 'Windows 8',
                    regex: /(Windows 8|Windows NT 6.2)/,
                    version: 'NT'
                }, {
                    name: 'Windows 7',
                    regex: /(Windows 7|Windows NT 6.1)/,
                    version: 'NT'
                }, {
                    name: 'Windows Vista',
                    regex: /Windows NT 6.0/,
                    version: 'NT'
                }, {
                    name: 'Windows Server 2003',
                    regex: /Windows NT 5.2/,
                    version: 'NT'
                }, {
                    name: 'Windows XP',
                    regex: /(Windows NT 5.1|Windows XP)/,
                    version: 'NT'
                }, {
                    name: 'Windows 2000',
                    regex: /(Windows NT 5.0|Windows 2000)/,
                    version: 'NT'
                }, {
                    name: 'Windows ME',
                    regex: /(Win 9x 4.90|Windows ME)/,
                    version: 'NT'
                }, {
                    name: 'Windows 98',
                    regex: /(Windows 98|Win98)/,
                    version: 'NT'
                }, {
                    name: 'Windows 95',
                    regex: /(Windows 95|Win95|Windows_95)/,
                    version: 'NT'
                }, {
                    name: 'Windows NT 4.0',
                    regex: /(Windows NT 4.0|WinNT4.0|WinNT|Windows NT)/,
                    version: 'NT'
                }, {
                    name: 'Windows CE',
                    regex: /Windows CE/,
                    version: 'NT'
                }, {
                    name: 'Windows 3.11',
                    regex: /Win16/,
                    version: 'NT'
                }, {
                    name: 'Android',
                    regex: /Android/,
                    version: 'Android'
                }, {
                    name: 'Kindle',
                    regex: /Silk/,
                    version: 'Silk'
                }, {
                    name: 'PlayBook',
                    regex: /PlayBook/,
                    version: 'OS'
                }, {
                    name: 'BlackBerry',
                    regex: /BlackBerry/,
                    version: '/'
                }, {
                    name: 'Open BSD',
                    regex: /OpenBSD/
                }, {
                    name: 'Sun OS',
                    regex: /SunOS/
                }, {
                    name: 'Linux',
                    regex: /(Linux|X11)/,
                    version: 'rv'
                }, {
                    name: 'Palm',
                    regex: /Palm/,
                    version: 'PalmOS'
                }, {
                    name: 'iOS',
                    regex: /(iPhone|iPad|iPod)/
                }, {
                    name: 'Mac OS X',
                    regex: /Mac OS X/
                }, {
                    name: 'Mac OS',
                    regex: /(MacPPC|MacIntel|Mac_PowerPC|Macintosh)/
                }, {
                    name: 'QNX',
                    regex: /QNX/
                }, {
                    name: 'UNIX',
                    regex: /UNIX/
                }, {
                    name: 'BeOS',
                    regex: /BeOS/
                }, {
                    name: 'OS/2',
                    regex: /OS\/2/
                }, {
                    name: 'Search Bot',
                    regex: /(nuhk|Googlebot|Yammybot|Openbot|Slurp|MSNBot|Ask Jeeves\/Teoma|ia_archiver)/
                }],
                headerString = header.join(' '),
                regexv, cs;
            for (let i = 0; i < clients.length; i++) {
                cs = clients[i];
                if (cs.regex.test(naviAgent)) {
                    os = cs.name;
                    if (cs.version) {
                        regexv = new RegExp(cs.version + '[- /:;]([\\d._]+)', 'i');
                        let matches = headerString.match(regexv);
                        version = '';
                        if (matches) {
                            if (matches[1]) {
                                matches = matches[1];
                            }
                        }
                        if (matches) {
                            matches = matches.split(/[._]+/);
                            for (let j = 0; j < matches.length; j++) {
                                if (j === 0) {
                                    version += matches[j] + '.';
                                } else {
                                    version += matches[j];
                                }
                            }
                            version = parseFloat(version);
                        } else {
                            version = unknown;
                        }
                    }
                    break;
                }
            }

            if (version === unknown && /Windows/.test(os)) {
                isWindows = true;
                if (/Windows (.*)/.test(os)) {
                    version = /Windows (.*)/.exec(os)[1];
                }
            }

            if (isWindows === null) {
                isWindows = /Windows/.test(os);
            }

            switch (os) {
                case 'Mac OS X':
                    if (/Mac OS X (10[\.\_\d]+)/.test(naviAgent)) {
                        version = /Mac OS X (10[\.\_\d]+)/.exec(naviAgent)[1];
                    }
                    break;
                case 'Android':
                    if (/Android ([\.\_\d]+)/.test(naviAgent)) {
                        version = /Android ([\.\_\d]+)/.exec(naviAgent)[1];
                    }
                    break;
                case 'iOS':
                    if (/OS (\d+)_(\d+)_?(\d+)?/.test(naviAgent)) {
                        version = /OS (\d+)_(\d+)_?(\d+)?/.exec(naviVersion);
                        version = version[1] + '.' + version[2] + '.' + (version[3] | 0);
                    }
                    break;
            }

            return {
                os: os,
                version: version
            };
        }

    }

    return OSDetector.detect();

});
