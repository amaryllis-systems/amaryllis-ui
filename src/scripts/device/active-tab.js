/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('device/active-tab', [
    "core/base",
    "core/window",
    "events/event"
], (Base, Win, AcmsEvent) => {

       
    /**
     * Active Tab
     *
     * @class ActiveTab
     * @extends {Base}
     */
    class ActiveTab extends Base {

        /**
         * Creates an instance of ActiveTab.
         *
         * @param {HTMLElement} element
         * @param {Object|null} options
         * @memberof ActiveTab
         */
        constructor(element, options) {
            super.constructor(element, options);
        }

        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof ActiveTab
         */
        static get MODULE() {
            return "Aktiver Tab";
        }

        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof ActiveTab
         */
        static get NS() {
            return 'acms.device.active-tab';
        }

        /**
         * Event "Activate"
         * 
         * Event fired during callback Activation of this Module
         *
         * @readonly
         * @static
         * @memberof ActiveTab
         */
        static get EVENT_ACTIVATE() {
            return ActiveTab.NS + ".activate";
        }

        /**
         * Event "Deactivate"
         * 
         * Event fired during callback Activation of this Module
         *
         * @readonly
         * @static
         * @memberof ActiveTab
         */
        static get EVENT_DEACTIVATE() {
            return ActiveTab.NS + ".deactivate";
        }

        /**
         * Module Options
         *
         * @readonly
         * @static
         * @memberof ActiveTab
         */
        static get DEFAULT_OPTIONS() {
            return {
                delay: 1, // delay time in seconds for the interval to check
                onblur: function(self){},
                onfocus: function(self){}
            };
        }

        static init(element, options) {
            let m = new ActiveTab(element, options);
            m.element.acmsData(ActiveTab.NS, m);

            return m;
        }

        enable(cb) {
            if(this.interval) {

                return false;
            }
            let self = this, o = self.options, delay = (o.delay || 1) * 1000;
            let evt = AcmsEvent.createCustom(ActiveTab.EVENT_ACTIVATE, {module: self, relatedTarget: self.element});
            AcmsEvent.dispatch(document.body, evt);
            this.interval = setInterval(cb, delay);

            return true;
        }

        disable() {
            if(!this.interval) {
                return;
            }
            let self = this;
            
            clearInterval(this.interval);
        }

        listen() {
            let self = this, o = self.options;
            AcmsEvent.add(window, 'blur', self._onBlur);
            AcmsEvent.add(window, 'focus', self._onFocus);
        }
        
        off() {
            var self = this, o = self.options;
            AcmsEvent.remove(window, 'blur', self._onBlur);
            AcmsEvent.remove(window, 'focus', self._onFocus);
        }


        _onBlur() {
            let self = this, o = self.options;
            if(this.active) {
                let evt = AcmsEvent.createCustom(ActiveTab.EVENT_DEACTIVATE, {module: self, relatedTarget: self.element});
                AcmsEvent.dispatch(document.body, evt);
                this.active = false;
                    
                if(typeof o.onblur === "function") {
                    o.onblur(self);
                }
            }
        }

        _onFocus() {
            let self = this, o = self.options;
            if(!this.active) {
                let evt = AcmsEvent.createCustom(ActiveTab.EVENT_ACTIVATE, {module: self, relatedTarget: self.element});
                AcmsEvent.dispatch(document.body, evt);
                this.active = true;
                    
                if(typeof o.onblur === "function") {
                    o.onblur(self);
                }
                if(typeof o.onfocus === "function") {
                    o.onfocus(self);
                }
            }
            
        }
        
        isActive() {
            return this.active;
        }

        _prepare() {
            this.interval =
            this.active = null;
        }
    }
    
    return ActiveTab;

});
