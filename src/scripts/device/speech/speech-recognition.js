/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('device/speech/speech-recognition', [
    "core/base"
], (Base) => {

    /**
     *  Important: retrieve the voices of the browser as soon as possible. 
     *             The execution of speechSynthesis.getVoices will return 
     *             at the first time an empty array.
     */
    if (window.hasOwnProperty('speechSynthesis')) {
        speechSynthesis.getVoices();
    }

    if (window.hasOwnProperty('webkitSpeechRecognition')) {
        let recognition = new webkitSpeechRecognition();
    }

    /**
     * SpeechCmd
     *
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class SpeechCmd extends Base {

        /**
         * Creates an instance of SpeechCmd.
         * 
         * @param {HTMLElement} element 
         * @param {Object|null} options 
         * 
         * @memberof SpeechCmd
         */
        constructor(element, options) {
            super.constructor(element, options);
            this.commands = null;
            this.commands = [];
        }

        /**
         * Module Name
         * 
         * @readonly
         * @static
         * @memberof SpeechCmd
         */
        static get MODULE() {
            return "Sprach Unterstützung";
        }

        /**
         * Module Namespace
         * 
         * @readonly
         * @static
         * @memberof SpeechCmd
         */
        static get NS() {
            return 'device/speech/speech-recognition';
        }

        /**
         * Standard Optionen
         * 
         * @readonly
         * @static
         * @memberof SpeechCmd
         */
        static get DEFAULT_OPTIONS() {
            return {};
        }

        /**
         * Initialisierung
         * 
         * @static
         * 
         * @param {HTMLElement} element     Element
         * @param {Object}      options     Optionen
         * 
         * @returns {SpeechCmd} Instanz
         * @memberof SpeechCmd
         */
        static init(element, options) {
            let v = new SpeechCmd(element, options);
            v.element.acmsData(SpeechCmd.NS, v);
    
            return v;
        }

        /**
         * Aufsetzen der Event-Listener
         * 
         */
        listen() {
            let self = this,
                    o = self.options,
                    element = self.element;
        }
        
        
        /**
         * Fügt ein Command Object hinzu
         * 
         * ```
         *  let promptcmd = {
         *      description: 'Beschreibung des Commands',
         *      action(i, wildcard) {
         *          // command action
         *      },
         *      indexes: ['Suche', /test/i, ...],
         *      
         *  }
         * ```
         * 
         * @param {Object} config
         * @returns {Boolean}
         */
        addCommand(conf) {
            
            let _process = function (obj) {
                if(obj.hasOwnProperty("cmd")){
                    SpeechCmd.commands.push(obj);
                } else {
                    throw new Error("cmd index fehlt, ist aber erforderlich");
                }
            }, i;

            if (conf instanceof Array) {
                for (i = 0; i < conf.length; i++) {
                    _process(conf[i]);
                }
            } else {
                _process(conf);
            }

            return true;
        }

        /**
         * Vorbereiten des Moduls
         * 
         * 
         */
        _prepare() {
            let self = this,
                    element = self.element,
                    o = self.options;
        }

    }

    return SpeechCmd;

});

