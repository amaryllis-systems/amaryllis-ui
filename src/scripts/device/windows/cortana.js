/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('device/windows/cortana', [
    'tools/object/extend'
], (Extend) => {

    /**
     * Cortana
     * 
     * Cortana implementation for windows devices
     *
     * @class 
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class Cortana {

        static DEFAULT_OPTIONS() {
            return {
                commands: {},
                onFound: function(cmd, value, result) {
                },
                onFail: function(cmd, value, result) {
                }
            };
        }

        constructor(options) {
            this.options = Extend.deep({}, true, Cortana.DEFAULT_OPTIONS, options);
            this._prepare();
        }
        
        /**
         * Vorbereiten des Moduls
         * 
         * @private
         */
        _prepare() {
            var self = this,
                    element = self.element,
                    o = self.options;
            if (typeof Windows !== 'undefined' &&
                    typeof Windows.UI !== 'undefined' &&
                    typeof Windows.ApplicationModel !== 'undefined') {
                
                Windows.UI.WebUI.WebUIApplication.addEventListener("activated", function (args) {
                    var activation = Windows.ApplicationModel.Activation;
                    if (args.kind === activation.ActivationKind.voiceCommand) {
                        var speechRecognitionResult = args.result;
                        var textSpoken = speechRecognitionResult.text;
                        var matched = false;
                        for(var cmd in o.commands) {
                            if(o.commands.hasOwnProperty(cmd)) {
                                if (speechRecognitionResult.rulePath[0] === cmd) {
                                    matched = true;
                                    var props = o.commands[cmd], prop, matchedProp;
                                    if(null !== props && false !== props && props.length > 0) {
                                        for(prop in props) {
                                            if(props.hasOwnProperty(prop)) {
                                                if((new RegExp(prop, 'i')).test(prop)) {
                                                    props[prop](cmd);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                });
            } else {
                console.log("Windows namespace is unavaiable");
            }
        }

    }

    return Cortana;

});

