/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("device/battery/battery", [
    "tools/object/extend",
    "events/event",
    "core/navigator"
], (Extend, AcmsEvent, Navi) => {

    /**
     * Batter Manager
     * 
     * @type {Object}
     */
    const BatteryManagery = {
            charging: false,
            chargingTime: Infinity,
            dischargingTime: 8940,
            onchargingchange: null,
            onchargingtimechange: null,
            ondischargingtimechange: null,
            onlevelchange: null
    };

    /**
     * Device Battery
     * 
     * @class
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class Battery {

        /**
         * Battery
         * 
         * @param {Object} options Optionen
         * 
         * @exports device/battery/battery
         */
        constructor(options) {
            this.loading =
            this.battery = null;
            this.options = Battery.DEFAULT_OPTIONS;
            this.initialize(document.body, options);
        }

        /**
         * Module Namespace
         * 
         * @readonly
         * @static
         * @memberof Battery
         */
        static get NS() {
            return "acms.device.battery";
        }

        /**
         * Modul Name
         * 
         * @readonly
         * @static
         * @memberof Battery
         */
        static get MODULE() {
            return "Battery Manager";
        }

        
        /**
         * Browser Schnittstelle zum Batter Manager
         * 
         * @static
         * @returns {Object}
         * @memberof Battery
         */
        static get manager() {
            let b = Navi.battery || 
                        Navi.webkitBattery || 
                        Navi.mozBattery || 
                        Navi.msBattery ||
                        Navi.getBattery ||
                        null;
                        
            return b;
        }

        /**
         * Prüft den Support diese Moduls
         * 
         * @static
         * @memberof Battery
         */
        static isSupported() {
            let b = Battery.manager;

            return (null !== b);
        }
            
        /**
         * Event "loaded"
         * 
         * Das Event wird getriggert, wenn der BatteryManager geladen wurde
         * 
         * @type {String}
         */
        static get EVENT_LOADED() {
            return this.NS + '.loaded';
        }
            
        /**
         * Event "levelchange"
         * 
         * Das Event wird getriggert, wenn der BatteryManager eine Level-Änderung empfängt
         * 
         * @type {String}
         */
        static get EVENT_CHANGE() {
            return this.NS + '.change';
        }

        /**
         * Event "chargingchange"
         * 
         * Das Event wird getriggert, wenn der BatteryManager den Lade-Status ändert
         * 
         * @type {String}
         */
        static get EVENT_CHARGING_CHANGE() {
            return this.NS + '.chargingchange';
        }


        /**
         * Standard Optionen
         * 
         * @readonly
         * @static
         * @memberof Battery
         */
        static get DEFAULT_OPTIONS() {

            return {
                // Wird aufgerufen, wenn der Zugriff erlaubt ist
                onLoad() {},

                /*
                * Wird bei Level-Änderung aufgerufen.
                * 
                * @param {Float} level Das neue Level
                * 
                */
                onLevelChange(level) {},
                /**
                 * Wird aufgerufen, wenn das Gerät lädt
                 * 
                 * @param {Boolean} charging    TRUE wenn das Gerät lädt
                 * 
                 */
                onChargingChange(charging) {},
                /**
                 * Wird aufgerufen, wenn sich die Lade-Zeit ändert (status-update am Ladebalken im Gerät)
                 * 
                 * @param {Integer} charging    Die Restladezeit in Sekunden
                 * 
                 * 
                 */
                onChargingTimeChange(charging) {
                },
                /**
                 * Wird aufgerufen, wenn sich die Entlade-Zeit ändert (status-update am Ladebalken im Gerät)
                 * 
                 * @param {Integer} charging    Die Restentladezeit in Sekunden
                 * 
                 * 
                 */
                onDischargingTimeChange(charging) {
                }
            };
        }
            
        /**
         * @param {Object} options Eigene Optionen
         * 
         * @return {Battery}  Eine neue Instanz
         */
        static init(options) {
            return new Battery(options);
        }

        
        /**
         * Initiaisierung
         * 
         * @param {Element} element
         * @param {Object} options
         * 
         * 
         */
        initialize(element, options) {
            if(false === Battery.isSupported()) {
                throw new Error('Nicht unterstützt von diesem Browser');
            }
            let self = this;
            self.element = element || document.body;
            self.options = self.buildOptions(options);
            let o = self.options;
            self.loading = true;
            if (Navi.getBattery) {
                Navi.getBattery().then(function(battery) {
                    self.battery = battery;
                    o.onLoad();
                    self.loading = false;
                    self._listen();
                });
            } else {
                self.battery = Navi.battery ||
                                Navi.webkitBattery ||
                                Navi.mozBattery ||
                                Navi.msBattery;
                o.onLoad();
                self.loading = false;
                self._listen();
            }
        }
        
        /**
         * Gibt das aktuelle Batterie-Level zurück
         *
         * Gibt NULL zurück,wenn der Batterie-Manager nicht verfügbar ist oder
         * den Wert. Der Batterie-Manager geht von 1 als voll geladen aus.
         *
         * @returns {Battery.manager.level}
         */
        getLevel() {
            if(null === this.loading || true === this.loading) {
                return null;
            }
            return this.battery.level;
        }

        /**
         * Checkt, ob die Batterie gerade aufgeladen wird
         *
         * Es kan entweder boolean TRUE oder FALSE zurückgegeben werden, wenn
         * der BatteryManager geladen ist, oder NULL, wenn er nicht verfügbar
         * ist.
         *
         * @returns {Boolean|NULL}
         */
        isCharging() {
            if(null === this.loading || true === this.loading) {
                return null;
            }
            return this.battery.charging === true;
        }
        
        /**
         * Gibt die verbleibende Ladezeit bis das Gerät voll ist
         *
         * Es kan entweder die verbleibende Zeit in Sekunden zurückgegeben werden, 
         * wenn der BatteryManager geladen ist, oder NULL, wenn er nicht verfügbar
         * ist.
         *
         * @returns {Integer|NULL} 
         */
        getChargingtime() {
            if(null === this.loading || true === this.loading) {
                return null;
            }
            return this.battery.chargingTime;
        }
        
        /**
         * Gibt das Level als Prozent-Zahl zurück
         * 
         * @returns {Number}
         */
        getBatteryPercentage() {
            let self = this, percentage = Math.round(self.battery.level * 100);
            
            return percentage;
        }
        
        isBatteryFullyCharged() {
            if (this.battery.level === 1) {
                return true;
            }
            return false;
        }
        
        /**
         * Lädt die Batterie gerade?
         * 
         * @returns {Boolean}
         */
        isBatteryCharging() {
            if (this.battery.level === 1) {
                return false;
            }

            return this.battery.charging;
        }
        
        /**
         * Gibt die verbleibende Entladezeit bis das Gerät voll ist
         *
         * Es kan entweder die verbleibende Zeit in Sekunden zurückgegeben werden, 
         * wenn der BatteryManager geladen ist, oder NULL, wenn er nicht verfügbar
         * ist.
         *
         * @returns {Integer|NULL} 
         */
        getDischargingtime() {
            if(null === this.loading || true === this.loading) {
                return null;
            }
            return this.battery.dischargingTime;
        }
        
        getNiceDischargingTime() {
            let time = this.getDischargingtime().toString();
            
            return time.toHHMMSS();
        }
        
        /**
         * 
         * @returns {unresolved}
         */
        getNiceChargingTime() {
            let time = this.getChargingtime().toString();
            
            return time.toHHMMSS();
        }

        /*
         * Aufsetzen der Event Listener
         * 
         * 
         */
        _listen() {
            let self = this, o = self.options;
            self.battery.onlevelchange = function(e) {
                o.onLevelChange(self.battery.level);
                let evt = AcmsEvent.create(this.EVENT_CHANGE);
                AcmsEvent.dispatch(document.body, evt);
            };
            self.battery.onchargingchange = function(e) {
                o.onChargingChange(self.battery.charging || false);
                let evt = AcmsEvent.create(this.EVENT_CHARGING_CHANGE);
                AcmsEvent.dispatch(document.body, evt);
            };
            self.battery.onchargingtimechange = function(e) {
                o.onChargingTimeChange(self.battery.chargingTime);
                let evt = AcmsEvent.create(this.EVENT_CHARGING_CHANGE);
                AcmsEvent.dispatch(document.body, evt);
            };
            self.battery.ondischargingtimechange = function(e) {
                o.onDischargingTimeChange(self.battery.dischargingTime);
            };
        }
        
        /**
         * Gibt die Standard-Optionen zurück
         * 
         * @returns {Battery.DEFAULT_OPTIONS}
         */
        getDefaultOptions () {
            return Battery.DEFAULT_OPTIONS;
        }

        /**
         * Bilden der Optionen
         * 
         * Bildet die Optionen basierend auf den Standard-Optionen, den 
         * übergebenen Optionen des Constructors und den Element Data-Attributen. 
         * Letztere gewinnen den Kampf, wenn gesetzt.
         * 
         * @param {Object} options    Die eignenen Optionen
         * @returns {Object|Battery.DEFAULT_OPTIONS}
         */
        buildOptions (options) {
            return Extend.flat({}, true, this.getDefaultOptions(), options);
        }
    }

    return Battery;
    
});
