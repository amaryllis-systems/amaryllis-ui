/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('device/browser-detect', [], () => {

    const Win = window;

    const Navi = navigator || Win.navigator;

    let header = [
            Navi.platform,
            Navi.userAgent,
            Navi.appVersion,
            Navi.vendor,
            window.opera
        ],
        headerString = header.join(' '),
        isMobileDevice = !!(/Android|webOS|iPhone|iPad|iPod|BB10|BlackBerry|IEMobile|Opera Mini|Mobile|mobile/i.test(headerString || '')),
        isEdge = headerString.indexOf('Edge') !== -1 && (!!navigator.msSaveOrOpenBlob || !!navigator.msSaveBlob),
        isOpera = (!!window.opera || headerString.indexOf(' OPR/') !== -1),
        isFirefox = (headerString.indexOf('Firefox') !== -1),
        isMozilla = (headerString.indexOf('Mozialla') !== -1 && !isFirefox && !isEdge),
        isSafari = (/^((?!chrome|android).)*safari/i.test(headerString)),
        isChrome = (!!window.chrome && !isOpera),
        isIE = ((typeof document !== 'undefined') && !!(document.documentMode) && !(isEdge)),
        isBlackBerry = (headerString.indexOf('BlackBerry') !== -1);

    let retry = (isDone, next) => {
        let currentTrial = 0,
            maxRetry = 50,
            interval = 10,
            isTimeOut = false;
        let id = window.setInterval(
            () => {
                if (isDone()) {
                    window.clearInterval(id);
                    next(isTimeOut);
                }
                if (currentTrial++ > maxRetry) {
                    window.clearInterval(id);
                    isTimeOut = true;
                    next(isTimeOut);
                }
            },
            interval
        );
    };

    let detectPrivateMode = (callback, browser) => {
        let isPrivateMode;

        if (window.webkitRequestFileSystem) {
            window.webkitRequestFileSystem(
                window.TEMPORARY, 1,
                () => {
                    isPrivateMode = false;
                },
                (e) => {
                    //console.log(e);
                    isPrivateMode = true;
                }
            );
        } else if (window.indexedDB && (isFirefox || isMozilla)) {
            let db;
            try {
                db = window.indexedDB.open('test');
            } catch (e) {
                isPrivateMode = true;
            }

            if (typeof isPrivateMode === 'undefined') {
                retry(
                    () => {
                        return db.readyState === 'done' ? true : false;
                    },
                    (isTimeOut) => {
                        if (!isTimeOut) {
                            isPrivateMode = db.result ? false : true;
                        }
                    }
                );
            }
        } else if (isIE && browser.version >= 10) {
            isPrivateMode = false;
            try {
                if (!window.indexedDB) {
                    isPrivateMode = true;
                }
            } catch (e) {
                isPrivateMode = true;
            }
        } else if (window.localStorage && isSafari) {
            try {
                window.localStorage.setItem('test', 1);
            } catch (e) {
                isPrivateMode = true;
            }

            if (typeof isPrivateMode === 'undefined') {
                isPrivateMode = false;
                window.localStorage.removeItem('test');
            }
        }

        retry(
            function isDone() {
                return typeof isPrivateMode !== 'undefined' ? true : false;
            },
            function next(isTimeOut) {
                callback(isPrivateMode);
            }
        );
    };

    /**
     * Browser Detektor
     * 
     * Einfacher Detektor für gängige Browser Engines.
     *
     * @class BrowserDetect
     */
    class BrowserDetect {

        constructor() {
            let unknown = '-',
                browser = unknown,
                version = unknown;
            let naviAgent = navigator.userAgent;
            let fullVersion = '' + parseFloat(navigator.appVersion);
            let majorVersion = parseInt(navigator.appVersion, 10);
            let nameOffset, verOffset, ix, off, detected, regexv;
            for (off in BrowserDetect.browsers) {
                if (BrowserDetect.browsers[off].is) {
                    detected = off;
                    if (BrowserDetect.browsers[off].version) {
                        regexv = new RegExp(BrowserDetect.browsers[off].version + '[- /:;]([\\d._]+)', 'i');
                        let matches = headerString.match(regexv), j;
                        version = '';
                        if (matches) {
                            if (matches[1]) {
                                matches = matches[1];
                            }
                        }
                        if (matches) {
                            matches = matches.split(/[._]+/);
                            for (j = 0; j < matches.length; j += 1) {
                                if (j === 0) {
                                    version += matches[j] + '.';
                                } else {
                                    version += matches[j];
                                }
                            }
                            version = parseFloat(version);
                        } else {
                            version = unknown;
                        }
                    }
                    BrowserDetect.browsers[off].version = version;
                    break;
                }
            }

            if (isOpera) {
                browser = BrowserDetect.browsers.opera.name;
                try {
                    fullVersion = headerString.split('OPR/')[1].split(' ')[0];
                    majorVersion = fullVersion.split('.')[0];
                } catch (e) {
                    fullVersion = '0.0.0.0';
                    majorVersion = 0;
                }
            } else if (isIE) {
                browser = BrowserDetect.browsers.msie.name;
                verOffset = naviAgent.indexOf('rv:');
                if (verOffset > 0) { //IE 11
                    fullVersion = naviAgent.substring(verOffset + 3);
                } else { //IE 10 or earlier
                    verOffset = naviAgent.indexOf('MSIE');
                    fullVersion = naviAgent.substring(verOffset + 5);
                }
                browser = 'IE';
            } else if (isChrome) {
                browser = BrowserDetect.browsers.chrome.name;
                verOffset = naviAgent.indexOf('Chrome');
                fullVersion = naviAgent.substring(verOffset + 7);
            } else if (isSafari) {
                verOffset = naviAgent.indexOf('Safari');
                browser = BrowserDetect.browsers.safari.name;
                fullVersion = naviAgent.substring(verOffset + 7);

                if ((verOffset = naviAgent.indexOf('Version')) !== -1) {
                    fullVersion = naviAgent.substring(verOffset + 8);
                }

                if (navigator.userAgent.indexOf('Version/') !== -1) {
                    fullVersion = navigator.userAgent.split('Version/')[1].split(' ')[0];
                }
            } else if (isFirefox) {
                verOffset = naviAgent.indexOf('Firefox');
                browser = BrowserDetect.browsers.firefox.name;
                fullVersion = naviAgent.substring(verOffset + 8);
            } else if ((nameOffset = naviAgent.lastIndexOf(' ') + 1) < (verOffset = naviAgent.lastIndexOf('/'))) {
                browser = naviAgent.substring(nameOffset, verOffset);
                fullVersion = naviAgent.substring(verOffset + 1);

                if (browser.toLowerCase() === browser.toUpperCase()) {
                    browser = navigator.appName;
                }
            }

            if (isEdge) {
                browser = BrowserDetect.browsers.edge.name;
                fullVersion = navigator.userAgent.split('Edge/')[1];
            }

            // trim the fullVersion string at semicolon/space/bracket if present
            if ((ix = fullVersion.search(/[; \)]/)) !== -1) {
                fullVersion = fullVersion.substring(0, ix);
            }

            majorVersion = parseInt('' + fullVersion, 10);

            if (isNaN(majorVersion)) {
                fullVersion = '' + parseFloat(navigator.appVersion);
                majorVersion = parseInt(navigator.appVersion, 10);
            }
            BrowserDetect.stack = BrowserDetect.browsers[detected];
            BrowserDetect.stack.fullVersion = fullVersion;
            BrowserDetect.stack.majorVersion = majorVersion;
            BrowserDetect.stack.isPrivateBrowsing = false;
            BrowserDetect.stack.mobile = isMobileDevice;

            let ready = false,
                inter, cb;
            try {
                detectPrivateMode(function (mode) {
                    BrowserDetect.stack.isPrivateBrowsing = (mode === true ? true : (mode === false ? false : 'unbekannt'));
                    ready = true;
                }, BrowserDetect.stack);

                cb = function () {
                    if (ready === true) {
                        clearInterval(inter);
                        return BrowserDetect.stack;
                    }
                };
                inter = setInterval(cb, 15);
            } catch (e) {
                BrowserDetect.stack.isPrivateBrowsing = 'unbekannt';
            }
        }

        is(name) {
            if (typeof BrowserDetect.browsers[name] === 'undefined') {
                return false;
            }

            return BrowserDetect.browsers[name].is;
        }
        get() {
            return BrowserDetect.stack;
        }
    }

    BrowserDetect.stack = null;

    BrowserDetect.browsers = {
        edge: {
            name: 'Microsoft Edge',
            version: 'Edge',
            regex: 'Edge',
            is: isEdge,
            vendor: Navi.vendor || 'Microsoft'
        },
        chrome: {
            name: 'Google Chrome',
            version: 'Chrome',
            regex: 'Chrome',
            is: isChrome,
            vendor: Navi.vendor || 'Google Inc.'
        },
        firefox: {
            name: 'Firefox',
            regex: 'Firefox',
            version: 'Firefox',
            is: isFirefox,
            vendor: Navi.vendor
        },
        safari: {
            name: 'Safari',
            regex: 'Safari',
            version: 'Version',
            is: isSafari,
            vendor: Navi.vendor
        },
        msie: {
            name: 'Internet Explorer',
            regex: 'MSIE',
            version: 'MSIE',
            vendor: Navi.vendor || 'Microsoft',
            is: isIE
        },
        opera: {
            name: 'Opera',
            regex: 'Opera',
            version: 'Opera',
            is: isOpera,
            vendor: Navi.vendor
        },
        blackberry: {
            name: 'BlackBerry',
            regex: 'CLDC',
            version: 'CLDC',
            is: isBlackBerry,
            vendor: Navi.vendor || 'BlackBerry'
        },
        mozilla: {
            name: 'Mozilla',
            regex: 'Mozilla',
            version: 'Mozilla',
            is: isMozilla,
            vendor: Navi.vendor
        }
    };

    return BrowserDetect;

});
