/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("device/geo/geo", [
    "tools/object/extend",
    'device/geo/location/location'
], function(Extend, Location) {

    /**
     * Geo Location Handling
     * 
     * @class
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class Geo {
        
        /**
         * Creates an instance of Geo.
         * 
         * @param {Object} options 
         * @memberof Geo
         * @exports device/geo/geo
         */
        constructor(options) {
            this.options    =
            this.position   =
            this.error      =
            this.location   = null;
            this.initialize(options);
        }

        /**
         * Module Name
         * 
         * @readonly
         * @static
         * @memberof Geo
         */
        static get MODULE() {
            return "Geo Location";
        }

        /**
         * Module Namespace
         * 
         * @readonly
         * @static
         * @memberof Geo
         */
        static get NS() {
            return "acms.device.geo.geo";
        }

        /**
         * Standard Optionen
         * 
         * @readonly
         * @static
         * @memberof Geo
         */
        static get DEFAULT_OPTIONS() {
            return {
                location: Location.DEFAULT_OPTIONS
            };
        }

        /**
         * Interner Constructor
         *
         * @param {Object} options  Optionen
         *
         * 
         */
        initialize(options) {
            this.options = this.buildOptions(options);
            this.location = new Loc(this.options.location);
            this.$body = document.body;
        }

        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {Geo.DEFAULT_OPTIONS}
         */
        getDefaultOptions() {
            return Geo.DEFAULT_OPTIONS;
        }

        /**
         * Gibt die neuen Optionen zurück
         *
         * @param {Object} options
         * @returns {Object}
         */
        buildOptions(options) {
            return Extend.deep({}, true, this.getDefaultOptions(), options);
        }

        hasError() {
            return (null === this.error ?  false : true);
        }

        /**
         *
         * @returns {Coordinate|null}
         */
        getPosition() {
            let self = this;
            if(null === this.position) {
                self.error = null;
                try {
                    self.position = self.location.request();
                } catch(err) {
                    self.error = err;
                }
            }
            return self.position;
        }
    }
    
    return Geo;
    
});
