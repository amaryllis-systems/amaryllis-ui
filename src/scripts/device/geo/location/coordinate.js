/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("device/geo/location/coordinate", [], () => {

    /**
     * Coordinate
     * 
     * Class Represanting a single coordinate
     *
     * @class Coordinate
     */
    class Coordinate {


        /**
         * Erstellt eine neue Koordinate
         * 
         * @param {Object} coords   Die Koordinate
         * 
         * @memberof Coordinate
         * 
         * @exports device/geo/location/coordinate
         */
        constructor(coords) {
            this._latitude =
                this._longitude =
                this._altitude =
                this._accuracy =
                this._altitudeAccuracy =
                this._heading =
                this._speed = null;
            this.initialize(coords);
        }
    
        /**
         * Modul Name
         * 
         * @readonly
         * @static
         * @memberof Coordinate
         */
        static get MODULE() {
            return "Geo-Koordinate";
        }
    
        /**
         * Module Namespace
         * 
         * @readonly
         * @static
         * @memberof Coordinate
         */
        static get NS() {
            return "acms.device.geo.location.coordinate";
        }
    
        /**
         * Erstellt eine Koordinate von einer Geolocation Position
         *
         * @param {Object} position
         *
         * @throws {Error} Wenn die Position kein Valides Object ist
         *
         * @returns {Coordinate}
         */
        static createFromPosition(position) {
            if (typeof position !== "object" || (false === ("coords" in position))) {
                throw new Error("Invalide Position");
            }
            let Coord = new Coordinate(position.coords);
    
            return Coord;
        }
    
    
        /**
         * Initialisierung der Koordinate
         * 
         * @param {Object}  coords  Die Koordinaten-Punkte. Erwartet wird ein 
         *                          Objekt mit den Argumenten latitude und 
         *                          longitude, accuracy, altitude, altitudeAccuracy, 
         *                          heading und speed.
         */
        initialize(coords) {
            this._latitude = coords.latitude || false;
            this._longitude = coords.longitude || false;
            this._accuracy = coords.accuracy;
            this._altitude = coords.altitude;
            this._altitudeAccuracy = coords.altitudeAccuracy;
            this._heading = coords.heading;
            this._speed = coords.speed;
            this._coords = coords;
        }
    
        /**
         * Gibt die Latitude zurück
         * 
         * @return {Float}  Die Latitude oder FALSE wenn nicht gesetzt
         */
        latitude() {
            return this._latitude;
        }
    
        /**
         * Gibt die Longitude zurück
         * 
         * @return {Float}  Die Longitude oder FALSE wenn nicht gesetzt
         */
        longitude() {
            return this._longitude;
        }
    
        /**
         * Gibt die Geschwindigkeit zurück
         * 
         * @return {Float}    Die Geschwindigkeit
         */
        speed() {
            return this._speed;
        }
    
        /**
         * Gibt die Akkuranz zurück
         * 
         * @return {Float}    Die Akkuranz
         */
        accuracy() {
            return this._accuracy;
        }
    
    
        /**
         * Gibt die Altitude zurück
         * 
         * @return {Float}    Die Altitude
         */
        altitude() {
            return this._altitude;
        }
    
        /**
         * Gibt die Akkuranz der Altitude zurück
         * 
         * @return {Float}    Die Akkuranz der Altitude
         */
        altitudeAccuracy() {
            return this._altitudeAccuracy;
        }
    
        /**
         * 
         * @return {String} Eine überschrift/Bezeichnung
         */
        heading() {
            return this._heading;
        }
    
        /**
         * @return {Object}     Das koordinaten-Objekt aus dem Constructor
         */
        coords() {
            return this._coords;
        }
    
        /**
         * @return {Float}  Die Distanz zwischen der übergebenen Koordinate und dieser
         */
        distance(coord, dist) {
            if (!(coord instanceof Coordinate)) {
                throw new Error("Die Coordinate muss eine Instanz von Coordinate sein");
            }
            if (!(dist instanceof Distance)) {
                throw new Error("Die Distanz muss eine Instanz von Distance sein");
            }
            return Distance.calc(coord, this);
        }
    }

    return Coordinate;

});
