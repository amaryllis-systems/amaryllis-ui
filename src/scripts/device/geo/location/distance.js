/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("device/geo/location/distance", ["device/geo/location/coordinate"], (Coordinate) => {

    const R = Math.PI / 180;


    /**
     * Distanz Berechnung
     * 
     * Basis-Konzept der Distanz-Berechnung zwischen 2 Koordinaten. Berechnet immer 
     * den kürzesten Weg über Luftlinie, keine Routenberechnung.
     * 
     * 
     * @exports device/geo/location/coordinate
     * 
     * @class Distance
     */
    class Distance {

        /**
         * Enthält den Erd-Radius in verschiedenen Einheiten
         * 
         * @type {Object}
         */
        static get distances() {
            return {
                'yards': 6967410,
                'km': 6371,
                'miles': 3959,
                'metres': 6371000,
                'feet': 20902231
            };
        }

        /**
         * km Angabe
         * 
         * @type String
         */
        static get KILOMETER() {
            return "km";
        }

        /**
         * Meilen Angabe
         * 
         * @type String
         */
        static get MILES() {
            return miles;
        }

        /**
         * Distanz Angabe
         * 
         * @type {Float}
         */
        static get R() {
            return R;
        }

        /**
         * Module Namespace
         * 
         * @readonly
         * @static
         * @memberof Distance
         */
        static get NS() {
            return "acms.device.geo.location.distance";
        }

        /**
         * Kalkuliert die Distanz zwischen 2 Koordinaten
         * 
         * @static
         * 
         * @param {Object|Coordinate}   Eine Instanze der Koordinate 1
         * @param {Object|Coordinate}   Eine Instanze der Koordinate 2
         * 
         * @returns {Number}    Distanz
         * 
         * @memberof Distance
         */
        static getCoordinateDistance(coord1, coord2) {
            if (!(coord1 instanceof Coordinate) || !(coord2 instanceof Coordinate)) {
                throw new Error("Die Coordinaten sind invalide");
            }
            let fromLat = coord1.latitude(),
                fromLng = coord1.longitude(),
                toLat = coord2.latitude(),
                toLng = coord2.longitude(),
                p = Distance.R;
            let c = Math.cos;
            let a = 0.5 - c((toLat - fromLat) * p) / 2 +
                c(fromLat * p) * c(toLat * p) *
                (1 - c((toLng - fromLng) * p)) / 2;
            let R = Distance.distances.km * 2;

            return R * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
        }
    }

    return Distance;

});
