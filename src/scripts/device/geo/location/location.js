/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("device/geo/location/location", [
    "tools/object/extend",
    "core/navigator",
    "device/geo/location/coordinate"
], (Extend, Navi, Coordinate) => {

    /**
     * Geo Location
     * 
     * Ein Objekt das eine Location basierend auf einer Position repräsentiert
     * 
     * @class
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class Location {


        /**
         * Creates an instance of Location.
         * @param {Object} options Optionen
         * 
         * @memberof Location
         * @exports device/geo/location/location
         */
        constructor(options) {
            this.isAvailable =
                this.options = null;
            this.initialize(options);
        }

        static get DEFAULT_OPTIONS() {
            return {
                enableHighAccuracy: true,
                timeout: 5000,
                maximumAge: 0
            };
        }

        static get NS() {
            return "acms.device.geo.location.location";
        }


        /**
         * Modul Name
         * 
         * @type String
         */
        static get MODULE() {
            return "Location";
        }


        /**
         * Permission Denied Status
         * 
         * Der Status tritt auf, wenn der Benutzer die Berechtung für den 
         * Standort-Zugriff zugriff vewehrt hat.
         * 
         * @type {Integer}
         */
        static get PERMISSION_DENIED() {
            return 1;
        }

        /**
         * Position unavailable Status
         * 
         * Der Status tritt auf, wenn es keine valide Positions-Bestimmung gab 
         * (GPS nicht erreichbar usw).
         * 
         * @type {Integer}
         */
        static get POSITION_UNAVAILABLE() {
            return 2;
        }

        /**
         * Timeout Status
         * 
         * Der Status tritt auf, wenn der Benutzer zu lange nicht reagiert hat, die 
         * Berechtigung für die Standort Abfrage zu erteilen.
         * 
         * @type {Integer}
         */
        static get TIMEOUT() {
            return 3;
        }

        /**
         * Ist die Geolocation verfügbar?
         * 
         * @returns {Boolean}    TRUE wenn die Geolocation unterstützt wird, FALSE 
         *                       andernfalls.
         */
        static get isSupported() {
            return ("geolocation" in Navi);
        }


        initialize(options) {
            this.options = this.buildOptions(options);
            this.isAvailable = Location.isSupported();
            this.$body = document.body;
            this._prepare();
        }

        getPosition() {
            return this.position;
        }

        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {Geo.DEFAULT_OPTIONS}
         */
        getDefaultOptions() {
            return Location.DEFAULT_OPTIONS;
        }


        /**
         * Bilden der Optionen
         * 
         * Bildet die Optionen basierend auf den Standard-Optionen, den 
         * übergebenen Optionen des Constructors und den Element Data-Attributen. 
         * Letztere gewinnen den Kampf, wenn gesetzt.
         * 
         * @param {Object} options    Die eignenen Optionen
         * @returns {Object|Battery.DEFAULT_OPTIONS}
         */
        buildOptions(options) {
            return Extend.flat({}, true, this.getDefaultOptions(), options);
        }

        /**
         * Gibt zurück,ob ein Fehler vorhanden ist
         * @returns {Boolean}
         */
        hasError() {
            return (null === this.error ? false : true);
        }

        request() {
            let self = this;
            if (false === self.isAvailable) {
                throw new Error(Acms.Translator._("Die Lokalisierung steht in Ihrem Browser nicht zur Verfügung."));
            }
            Navi.geolocation.getCurrentPosition(self._success, self._error, self.options);
        }

        /**
         * Erfolfreiche Positions-Bestimmung
         * @param {Object} position
         * @returns {Coordinate}
         */
        _success(position) {
            let self = this;

            let coo = Coordinate.createFromPosition(position);
            self.coordinate = coo;
            self.position = position;

            return {
                coord: self.coordinate,
                position: position
            };
        }

        _error(error) {

            Acms.Logger.logWarning('Die Positions-Bestimmung ist Fehlgeschlagen: ' + error.message + " [" + error.code + "]");

            switch (error.code) {
                case Location.PERMISSION_DENIED:
                    throw new Error(Acms.Translator._("Ihre Position kann nicht ermittelt werden, wenn Sie die Genehmigung nicht erteilen."));
                case Location.POSITION_UNAVAILABLE:
                    throw new Error(Acms.Translator._("Ihre Position konnte nicht ermittelt werden."));
                case Location.TIMEOUT:
                    throw new Error(Acms.Translator._(
                        "Ihre Position konnte nicht ermittelt werden. Sie müssen die Bestätigung im Browser akzeptieren, wenn das Popup auftaucht."));
                default:
                    throw new Error(Acms.Translator._("Ihre Position konnte nicht ermittelt werden."));
            }

        }

        /**
         * Vorbereiten des Moduls
         * 
         * 
         */
        _prepare() {
            let self = this;

            for (let fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        }


    }

    return Location;
});
