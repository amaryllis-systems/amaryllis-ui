/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('device/camera/capture', [], () => {


    const takePicture = (success, error, opts) => {
        if (opts && opts[2] === 1) {
            capture(success, error);
        } else {
            let input = document.createElement('input');
            input.type = 'file';
            input.name = 'files[]';
    
            input.onchange = function (inputEvent) {
                let canvas = document.createElement('canvas');
    
                let reader = new FileReader();
                reader.onload = function (readerEvent) {
                    input.parentNode.removeChild(input);
    
                    let imageData = readerEvent.target.result;
    
                    return success(imageData.substr(imageData.indexOf(',') + 1));
                };
    
                reader.readAsDataURL(inputEvent.target.files[0]);
            };
    
            document.body.appendChild(input);
        }
    };
    
    const capture = (success, errorCallback) => {
        let localMediaStream;
    
        let video = document.createElement('video');
        let button = document.createElement('button');
    
        video.width = 320;
        video.height = 240;
        button.innerHTML = Acms.Translator._('Aufnehmen!');
    
        button.onclick = function () {
            let canvas = document.createElement('canvas');
            canvas.getContext('2d').drawImage(video, 0, 0, 320, 240);
            let imageData = canvas.toDataURL('img/png');
            imageData = imageData.replace('data:image/png;base64,', '');
            localMediaStream.stop();
            video.parentNode.removeChild(video);
            button.parentNode.removeChild(button);
    
            return success(imageData);
        };
    
        navigator.getUserMedia = navigator.getUserMedia ||
            navigator.webkitGetUserMedia ||
            navigator.mozGetUserMedia ||
            navigator.msGetUserMedia;
    
        let successCallback = function (stream) {
            localMediaStream = stream;
            video.src = window.URL.createObjectURL(localMediaStream);
            video.play();
    
            document.body.appendChild(video);
            document.body.appendChild(button);
        };
    
        if (navigator.getUserMedia) {
            navigator.getUserMedia({ video: true, audio: true }, successCallback, errorCallback);
        } else {
            alert(Acms.Translator._('Der Browser unterstützt keine Web-Kamera :('));
        }
    };
    
    const module = takePicture;
    
    return module;

});
