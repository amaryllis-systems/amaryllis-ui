/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('device/match-media', [
    'core/window',
    'events/event'
], (Win, AcmsEvent) => {

    let match = Win.matchMedia || (Win.matchMedia = function () {
        let styleMedia = (Win.styleMedia || Win.media);
        if (!styleMedia) {
            let style = document.createElement('style'),
                    title = document.getElementsByTagName('title')[0],
                    info = null;
            style.type = 'text/css';
            style.id = 'acms-stylemedia-test';
            title.parentNode.insertAfter(style, title);
            info = ('getComputedStyle' in window) && Win.getComputedStyle(style, null) || style.currentStyle;
            styleMedia = {
                matchMedium: function (media) {
                    let text = '@media ' + media + '{ #acms-stylemedia-test { width: 1px; } }';
                    if (style.styleSheet) {
                        style.styleSheet.cssText = text;
                    } else {
                        style.textContent = text;
                    }
                    return info.width === '1px';
                }
            };
        }

        return function (media) {
            return {
                matches: styleMedia.matchMedium(media || 'all'),
                media: media || 'all'
            };
        };
    }());
    
    let defaults = {
        'default': 'only screen',
        landscape: 'only screen and (orientation: landscape)',
        portrait: 'only screen and (orientation: portrait)',
        retina: 'only screen and (-webkit-min-device-pixel-ratio: 2),' + 'only screen and (min--moz-device-pixel-ratio: 2),' + 'only screen and (-o-min-device-pixel-ratio: 2/1),' + 'only screen and (min-device-pixel-ratio: 2),' + 'only screen and (min-resolution: 192dpi),' + 'only screen and (min-resolution: 2dppx)',
        '480-767': 'only screen and (min-width: 480px) and (max-width: 767px)',
        '768-1023': 'only screen and (min-width: 768px) and (max-width: 1023px)',
        '1024-1199': 'only screen and (min-width: 1024px) and (max-width: 1199px)',
        '1200-1599': 'only screen and (min-width: 1200px) and(max-width: 1599px)',
        '1600': 'only screen and (min-width: 1600px)',
    };
    let listen = () => {
        AcmsEvent.add(Win, 'resize', function() {Module._matchings = {}; Module._init();});
    };
    let Module = {
        MODULE: 'Media Matcher',
        _matchings: {},
        _init: function () {
            let key;
            for (key in defaults) {
                if(defaults.hasOwnProperty(key))
                this._matchings[key] = this.match(defaults[key]);
            }
        },
        /**
         * Gibt das dierekte Abfrage Ergebnis zurück
         * @param {string} media
         * @returns {Module._matchings.media.matches}
         */
        matches: function (media) {
            if (!this._matchings[media]) {
                this._matchings[media] = this.match(media);
            }
            return this._matchings[media].matches;
        },
        /**
         * Prüft ob der Media String zutrifft
         * 
         * Es wird ein Object zurückgegeben mit den Schlüsseln 
         * 
         * - `matches` (Boolean, `TRUE` wenn erfolgreich)
         * - `media` Der Media Query
         * 
         * @param {String} media    Der Media String
         * @returns {Object}
         */
        match: function (media) {
            return match(media);
        }
    };
    listen();
    Module._init();
    
    return Module;

});
