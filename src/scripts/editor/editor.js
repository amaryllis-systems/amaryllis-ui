/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (https://www.amaryllis-systems.de)
 *
 * @author qm-b <qm-b@outlook.de>
 * @copyright 2011 - 2016 QM-B <qm-b@outlook.de>
 * @copyright Amaryllis Systems http://www.amaryllis-systems.de/
 * 
 *
 * Copyright 2018 Amaryllis Systems GmbH <https://www.amaryllis-systems.de/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("editor/editor", [
    'core/base', 'core/acms'
], (Base, A) => {

    /**
     * Editor Loader
     * 
     * Utility Module to load the Editor from Options passed for an Element.
     *
     * @class Editor
     */
    class Editor extends Base {

        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof Editor
         */
        static get MODULE() {
            return "Editor Loader";
        }

        static get NS() {
            return "acms.editor.editor";
        }

        /**
         * Standard Optionen
         *
         * @var {Object}
         */
        static get DEFAULT_OPTIONS() {
            return {
                editor: null,
                editorWeight: null
            };
        }

        /**
         * Module init through Module Loader
         *
         * @param {HTMLElement} element Element to initialize the Editor on
         * @param {Object|null} options Custom Options
         * 
         * @memberof Editor
         */
        static init(element, options) {
            let ed = new Editor(element, options);
            ed.element.acmsData(this.NS, ed);
    
            return ed;
        }

        constructor(element, options) {
            super(element, options);
            this._initialize();
        }

        load() {
            let self = this,
                ele = self.element,
                dataEditor = self.option('editor'),
                name, editorfile;
            if (dataEditor) {
                name = dataEditor.toLowerCase();
                editorfile = `editor/${name}/${name}`;
                require([editorfile], function (Edi) {
                    Edi.init(ele);
                    A.Logger.writeLog(editorfile + ' angefordert');
                });
            } else {
                A.Logger.logWarning(dataEditor + ' nicht gefunden');
            }
        }

        _prepare() {
            this.load();
        }

    }

    return Editor;

});
