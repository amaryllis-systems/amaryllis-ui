/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @copyright 2013 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright 2014 - 2018 Amaryllis Systems GmbH http://www.amws.eu/
 * @copyright 2017 - 2018 Amaryllis CMS Project http://www.amaryllis-cms.de/
 * @license https://www.amaryllis-cms.de/lizenzen/
 * @license Apache Licence 2.0 https://www.apache.org/licenses/LICENSE-2.0
 *
 * Dual-Licensed with Amaryllis CMS EULA. You may obtain a copy of the
 * EULA at
 *
 *     https://www.amaryllis-cms.de/lizenzen/eula/
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

define('editor/codemirror/codemirror', [
    "libs/editors/codemirror/lib/codemirror",
    "http/css-loader",
    "tools/utils",
    "core/selectors",
    "events/event"
], function (CodeMirror, CssLoader, Utils, Selectors, AcmsEvent) {

    const instanzes = {};

    class CodeMirrorLoader {

        static get MODULE() {
            return 'CodeMirror Plugin';
        }

        static get needInit() {
            return true;
        }

        static get DEFAULT_OPTIONS() {
            return {
                value: '',
                mode: 'smartymixed',
                theme: 'blackboard',
                lineNumbers: true,
                css: "media/scripts/libs/editors/codemirror/lib/codemirror.min.css"
            };
        }

        static init(element, options) {

        }

        constructor(element, options) {
            let self = this;
            this.element = element;
            let editor = element,
                form = Selectors.closest(editor, 'form'),
                data = editor.acmsData(),
                value = editor.value,
                opts = Utils.extend({}, Module.DEFAULT_OPTIONS, options, data),
                op = Utils.extend({}, opts, {
                    value: value
                });
            CssLoader.load(opts.css);
            data.mode = data.mode || 'smartymixed';
            
            require(['libs/editors/codemirror/mode/' + data.mode + '/' + data.mode], function (mode) {
                let o = Utils.extend({}, opts, op, data);
                if (o.theme) {
                    CssLoader.load('/media/scripts/libs/editors/codemirror/theme/' + o.theme);
                }

                let editorInst = CodeMirror.fromTextArea(editor, o);
                let cb = function () {
                    editorInst.save();
                };
                let Group = Selectors.closest(editor, '.form-group'),
                    codem;
                if (Group) {
                    codem = Group.querySelector('.CodeMirror');
                }
                if (codem) {
                    let tarea = codem.querySelector('textarea');
                    AcmsEvent.add(tarea, 'blur', cb);
                    AcmsEvent.add(form, "acms.before-save-form", cb);
                }

            });
        }


    }

    let Module = {
        MODULE: 'CodeMirror Plugin',
        needInit: true,
        initialized: false,
        init: function (ele, options) {
            let self = this;
            let editor = ele,
                form = Selectors.closest(editor, 'form'),
                data = editor.acmsData(),
                value = editor.value,
                opts = Utils.extend({}, Module.DEFAULT_OPTIONS, data);
            let op = {
                value: value
            };
            CssLoader.load(opts.css);
            data.mode = data.mode || 'smartymixed';

            require(['libs/editors/codemirror/mode/' + data.mode + '/' + data.mode], function (mode) {
                let o = Utils.extend({}, opts, op, data);
                if (o.theme) {
                    CssLoader.load('/media/scripts/libs/editors/codemirror/theme/' + o.theme);
                }

                let editorInst = CodeMirror.fromTextArea(editor, o);
                let cb = function () {
                    editorInst.save();
                };
                let Group = Selectors.closest(editor, '.form-group'),
                    codem;
                if (Group) {
                    codem = Group.querySelector('.CodeMirror');
                }
                if (codem) {
                    let tarea = codem.querySelector('textarea');
                    AcmsEvent.add(tarea, 'blur', cb);
                    AcmsEvent.add(form, "acms.before-save-form", cb);
                }

            });
        }
    };
    Module.DEFAULT_OPTIONS = {
        value: '',
        mode: 'smartymixed',
        theme: 'blackboard',
        lineNumbers: true,
        css: "media/scripts/libs/editors/codemirror/lib/codemirror.min.css"
    };
    return Module;

});
