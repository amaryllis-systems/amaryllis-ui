/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @copyright 2013 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright 2014 - 2018 Amaryllis Systems GmbH http://www.amws.eu/
 * @copyright 2017 - 2018 Amaryllis CMS Project http://www.amaryllis-cms.de/
 * @license https://www.amaryllis-cms.de/lizenzen/
 * @license Apache Licence 2.0 https://www.apache.org/licenses/LICENSE-2.0
 *
 * Dual-Licensed with Amaryllis CMS EULA. You may obtain a copy of the
 * EULA at
 *
 *     https://www.amaryllis-cms.de/lizenzen/eula/
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */


/**
 * Amaryllis Editor Mode Public
 *
 * @returns {Mode}
 */
define('editor/amaryllis/modes/public', [
    'core/acms'
], function (A) {

    "use strict";

    /**
     * Constructor
     *
     * @param {Object} config
     * @returns {Mode}
     */
    var Mode = function (config) {
        config = config || {};
        this.config = this.initialize(config);
    };

    /**
     * Initialisirung
     *
     * @param {Object} config
     * @returns {Mode}
     */
    Mode.init = function (config) {
        var m = new Mode(config);

        return m;
    };

    Mode.prototype = {
        constructor: Mode,

        initialize: function (config) {
            return {
                // Mode Name
                mode: "public",
                // Action Rows 
                rows: {
                    'row1': {
                        'typography': {
                            'actions': [
                                'bold', 'italic'
                            ],
                            'label': A.Translator._('Typographie')
                        },
                        'linking': {
                            'actions': [
                                'anchor'
                            ],
                            'label': A.Translator._('Links')
                        },
                        'misc': {
                            'actions': [
                                'bullist', 'numlist', 'quote'
                            ],
                            'label': A.Translator._('Verschiedenes')
                        }
                    },
                },
                // Controls
                fullscreen: true,
                help: true
            };
        },

        getMode: function () {
            return this.config;
        }
    };

    return Mode;

});