/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @copyright 2013 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright 2014 - 2018 Amaryllis Systems GmbH http://www.amws.eu/
 * @copyright 2017 - 2018 Amaryllis CMS Project http://www.amaryllis-cms.de/
 * @license https://www.amaryllis-cms.de/lizenzen/
 * @license Apache Licence 2.0 https://www.apache.org/licenses/LICENSE-2.0
 *
 * Dual-Licensed with Amaryllis CMS EULA. You may obtain a copy of the
 * EULA at
 *
 *     https://www.amaryllis-cms.de/lizenzen/eula/
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

/**
 * 
 * @param {type} Utils
 * @param {type} Http
 * @param {type} Editor
 * @returns {AmEditor}
 * 
 * @experimentel
 * @internal
 * @deprecated To be remved
 */
define("editor/amaryllis/amaryllis", [
    "tools/utils",
    "editor/amaryllis/lib/amaryllis",
    "core/selectors",
    'events/event',
    "core/acms"
], function (Utils, Editor, Selectors, AcmsEvent, A) {

    /**
     * Constructor
     * 
     * @param {Element} element     Das Element
     * @param {Object}  options     Das Objekt
     * 
     * @returns {AmEditor}
     */
    var AmEditor = function (element, options) {
        this.options =
        this.element =
        this.editor =
        this.mode = null;
        
        this.initialize(element, options);
    };

    /**
     * Modul Name
     * 
     * @type {String}
     */
    AmEditor.MODULE = "AmEditor Konfigurator";

    /**
     * Modul Namespace
     * 
     * @type {String}
     */
    AmEditor.NS = "acms.editor.amaryllis.amaryllis";


    /**
     * Mode Public
     * 
     * @type {Number}   
     */
    AmEditor.MODE_PUBLIC = 1;

    /**
     * Mode Basic
     * 
     * @type {Number}   
     */
    AmEditor.MODE_BASIC = 2;

    /**
     * Mode Extended
     * 
     * @type {Number}   
     */
    AmEditor.MODE_EXTENDED = 3;

    /**
     * Mode Advanced
     * 
     * @type {Number}   
     */
    AmEditor.MODE_ADVANCED = 4;

    /**
     * Mode Export
     * 
     * @type {Number}   
     */
    AmEditor.MODE_EXPERT = 5;

    /**
     * Standard Optionen
     * 
     * @type {Object}
     */
    AmEditor.DEFAULT_OPTIONS = {
        mode: AmEditor.MODE_PUBLIC
    };

    /**
     * Einbinden des Modul loaders
     * 
     * @type {String}
     */
    AmEditor.needInit = true;

    /**
     * Initialisierung des Moduls
     * 
     * @param {Element} element     Das Element
     * @param {Object}  options     Das Objekt
     * 
     * @returns {AmEditor}
     */
    AmEditor.init = function (element, options) {
        var ed = new AmEditor(element, options);

        return ed;
    };

    AmEditor.prototype = {
        /**
         * @constructor
         */
        constructor: AmEditor,
        
        /**
         * Interner Constructor
         * 
         * @param {Element} element     Das Element
         * @param {Object}  options     Das Objekt
         * 
         * @returns {AmEditor}
         */
        initialize: function (element, options) {
            this.element = element;
            this.options = this.buildOptions(options);
            this._prepare();
            this.listen();
        },


        /**
         * Gibt die Standard-Optionen zurück
         *
         * @returns {AmEditor.DEFAULT_OPTIONS}
         */
        getDefaultOptions: function() {
            return AmEditor.DEFAULT_OPTIONS;
        },


        /**
         * Aufsetzen der Event Listener
         * 
         * 
         */
        listen: function() {
            var self = this, 
                element = self.element,
                form = Selectors.closest(element, 'form');
            if(!form) {
                return;
            }
            AcmsEvent.add(form, 'acms.before-save-form', function(e) {
                
            });
        },
        
        /**
         * Bildet die Optionen
         *
         * <p>
         *  Basierend auf den Standard-Optionen, den übergebenen Optionen des
         *  Constructors und den Element Data-Attributen werden die aktuellen
         *  Konfigurationen gebildet. Letztenendes gewinnen die Element
         *  Data-Attribute.
         * </p>
         *
         * @param {Object} options
         *
         * @returns {Object}
         */
        buildOptions: function(options) {
            return Utils.extend(
                    {},
                    this.getDefaultOptions(),
                    options,
                    this.element.acmsData()
                    );
        },

        /**
         * Setter/Getter für die Optionen
         *
         * @param {String|NULL} a   Der Optionen-Schlüssel oder NULL
         * @param {Mixed}       b   Undefined oder der Wert für a
         * @returns {AmEditor.options|Boolean|Mixed|AmEditor.prototype}
         */
        option: function (a, b) {
            if (!a) {
                return this.options;
            }
            if (typeof b === "undefined") {
                return this.options[a] || false;
            }
            this.options[a] = b;
            return this;
        },
        

        /**
         * Vorbereiten
         * 
         * 
         */
        _prepare: function() {
            var self = this,
                element = self.element,
                o = self.options,
                dataHeight = o.height || 300;
            if(!element.getAttribute('id')) {
                element.setAttribute('id', Utils.generateUniqeId());
            }
            var config = {
                },
                thisMode, mod;
            
            thisMode = parseInt(o.mode) || AmEditor.MODE_PUBLIC;
            A.Logger.writeLog(thisMode + ' Editor-Modus');
            switch(thisMode) {
                case AmEditor.MODE_EXPERT:
                    mod = "editor/amaryllis/modes/expert";
                    break;
                case AmEditor.MODE_ADVANCED:
                    mod = 'editor/amaryllis/modes/advanced';
                    break;
                case AmEditor.MODE_EXTENDED:
                    mod = 'editor/amaryllis/modes/extended';
                    break;
                case AmEditor.MODE_BASIC:
                    mod = 'editor/amaryllis/modes/basic';
                    break;
                case AmEditor.MODE_PUBLIC:
                    mod = "editor/amaryllis/modes/public";
                    break;
                default:
                    if(typeof o.mode === 'string' && o.mode.indexOf('/') !== -1) {
                        mod = o.mode;
                    } else {
                        mod = "editor/amaryllis/modes/public";
                    }
                    break;
            }
            require([mod], function(Mode) {
                var amMode = Mode.init(config),
                    amConfig = amMode.getMode();
                    amConfig.setup = function(ed) {
                        ed.on("blur", function(){
                            self.element.value = ed.getContent();
                            AcmsEvent.fireChange(self.element);
                        });
                   },
                   opts = {
                       mode: amConfig
                   };
                self.editor = Editor.init(element, opts);
            });
        }

    };

    return AmEditor;

});
