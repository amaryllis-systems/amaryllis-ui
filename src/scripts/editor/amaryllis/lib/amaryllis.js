/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @copyright 2013 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright 2014 - 2018 Amaryllis Systems GmbH http://www.amws.eu/
 * @copyright 2017 - 2018 Amaryllis CMS Project http://www.amaryllis-cms.de/
 * @license https://www.amaryllis-cms.de/lizenzen/
 * @license Apache Licence 2.0 https://www.apache.org/licenses/LICENSE-2.0
 *
 * Dual-Licensed with Amaryllis CMS EULA. You may obtain a copy of the
 * EULA at
 *
 *     https://www.amaryllis-cms.de/lizenzen/eula/
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

/**
 * Amaryllis Editor
 */
define("editor/amaryllis/lib/amaryllis", [
    "tools/utils",
    "core/selectors",
    "core/classes",
    "templates/template-engine",
    "editor/amaryllis/lib/generators/editor-generator",
    "editor/amaryllis/lib/controls/controls",
    "editor/amaryllis/lib/cursor",
    "apps/ui/informations/tooltip",
    "events/event",
    "core/window",
    "core/acms"
], function (Utils, Selectors, Classes, TemplateEngine, Generator, Controls, Cursor, Tooltip, AcmsEvent, Win, A) {

    "use strict";

    var editors = [],
        ctrlPressed = false,
        hk = '';

    var Editor = function (element, options) {
        this.element =
            this.buttons =
            this.options =
            this._isFullScreen =
            this._maximized =
            this._controls =
            this._oldContent =
            this.$nextTab =
            this._container = null;
        this.initialize(element, options);
    };

    Editor.MODULE = "Amaryllis Markdown-Editor";

    Editor.NS = "acms.editor.amaryllis.lib.amaryllis";

    Editor.EVENT_FULLSCREEN_ENABLED = Editor.NS + '.fullscreen.enabled';

    Editor.EVENT_FULLSCREEN_DISABLED = Editor.NS + '.fullscreen.disabled';

    Editor.EVENT_MAXIMIZE_ENABLED = Editor.NS + '.maximize.enabled';

    Editor.EVENT_MAXIMIZE_EXIT = Editor.NS + '.maximize.disabled';

    /**
     * Standard Optionen
     * 
     * @type {Object}
     * 
     * 
     * @property {String}       template        Das Template (erforderlich)
     * @property {Boolean}      disabled        Ist der Editor verboten?
     * @property {Object}       mode            Der Editor Modus (erforderlich)
     * @property {Object|null}  plugins         Plugin Konfigurationen
     * @property {Function}     onFocus         Callback für den Fokus
     * @property {Function}     onBlur          Callback für Blur
     * 
     */
    Editor.DEFAULT_OPTIONS = {
        template: 'templates/html/amaryllis-editor.hbs',
        maxChars: 0, // maximale Zeichen
        disabled: false,
        mode: false,
        plugins: null,
        /* Events hook */
        onShow: function (ed, evt) {},
        onPreview: function (ed, evt) {},
        onPreviewEnd: function (ed, evt) {},
        onSave: function (ed, evt) {},
        onBlur: function (ed, ele) {},
        onFocus: function (ed, ele) {},
        onChange: function (ed, evt) {},
        onEnable: function () {},
        onDisable: function () {},
        onFullscreen: function (ed, api, fs) {},
        onFullscreenExit: function (ed, api, fs) {},
        onMaximize: function (ed) {},
        onMaximizeExit: function (ed) {},
        onHelp: function (ed, evt) {},
        onhange: function (ed, evt) {},
        onSelect: function (ed, evt) {}
    };

    Editor.init = function (element, options) {
        return new Editor(element, options);
    };

    Editor.prototype = {

        constructor: Editor,

        /**
         *Interner Constructor
         *
         * @param {HTMLElement} element Die Textarea
         * @param {Object|null} options
         */
        initialize: function (element, options) {
            this.element = element;
            this._original = element.cloneNode(true);
            this.buttons = {};
            this.options = this.buildOptions(options);
            this.$nextTab = [];
            this._prepare();
            editors.push(this);
        },


        /**
         * Gibt die Standard-Optionen zurück
         * 
         * @returns {Editor.DEFAULT_OPTIONS|Object} Standard-Optionen
         * 
         * @property {String}   template    Das Template (erforderlich)
         * @property {Boolean}  disabled    Ist der Editor verboten?
         * @property {Object}   mode        Der Editor Modus (erforderlich)
         * @property {Function} onFocus     Callback für den Fokus
         * @property {Function} onBlur      Callback für Blur
         * 
         */
        getDefaultOptions: function () {
            return Editor.DEFAULT_OPTIONS;
        },
        buildOptions: function (options) {
            options = typeof options === "object" ? options : {};
            var data = this.element.acmsData();
            if (typeof data.mode !== "undefined")
                delete data.mode;

            return Utils.extend({}, this.getDefaultOptions(), options, data);
        },

        listen: function () {
            var self = this;
        },

        /**
         * @returns {Boolean}   TRUE wenn der Content nicht clean ist
         */
        isDirty: function () {
            return this._oldContent != this.getContent();
        },

        /**
         * Den Content erhalten
         * 
         * @returns {String}    Den Content der Text-Area
         */
        getContent: function () {
            return this.element.value;
        },

        getTextarea: function () {
            return this.element;
        },

        /**
         * Setzen von Content
         * 
         * Die Methode überschreibt/ersetzt vorigen Content mit neuem.
         * 
         * @param {String}  content Der Content
         * 
         * @return {Editor}
         */
        setContent: function (content) {
            this.element.value = content;
            AcmsEvent.fireChange(this.element);

            return this;
        },

        /**
         * Einfügen eines Content-Parts an einem spezifischen Index
         * 
         * @param {Integer} index   Index an dem Content eingefügt werden soll
         * @param {String}  content Der Content
         */
        insertContent: function (index, content) {
            var firstHalf = this.getContent().slice(0, index);
            var secondHalf = this.getContent().slice(index + 1);
            this.setContent(firstHalf.concat(content).concat(secondHalf));
        },

        /**
         * Erstellt einen neuen Cursor
         * 
         * @returns {Cursor}    Den Cursor
         */
        getCursor: function () {
            return new Cursor(this);
        },

        getNextTab: function () {
            // Shift the nextTab
            var self = this;
            if (self.$nextTab.length === 0) {
                return null;
            } else {
                var nextTab, tab = self.$nextTab.shift();

                if (typeof tab == 'function') {
                    nextTab = tab();
                } else if (typeof tab == 'object' && tab.length > 0) {
                    nextTab = tab;
                }

                return nextTab;
            }
        },

        /**
         *
         *
         * @param {String|Number} start
         * @param {Number} end
         * 
         * @returns {void}
         */
        setNextTab: function (start, end) {
            // Push new selection into nextTab collections
            var self = this,
                cursor = self.getCursor();
            if (typeof start === 'string') {
                self.$nextTab.push(function () {
                    return cursor.findSelection(start);
                });
            } else if (typeof start === 'number' && typeof end === 'number') {
                var oldSelection = cursor.getSelection();

                cursor.setSelection(start, end);
                self.$nextTab.push(cursor.getSelection());
                cursor.setSelection(oldSelection.start, oldSelection.end);
            }

            return;
        },

        /**
         * On Fullscreen Change Callback
         * 
         * Das Callback wird durch die Fullscreen Control 
         * aufgerufen, wenn sich der FullScreen Mode ändert.
         * Der Aufruf der Methode dient **nicht** zum Umschalten 
         * des FullScreen Mode.
         * 
         * @param {Boolean}                 enabled     Ist der Mode aktiviert?
         * @param {Object}                  api         FullScreen Api
         * @param {apps/ui/helpers/full-screen}  fs          Das FullScreen Modul
         */
        toggleFullScreen: function (enabled, api, fs) {
            var self = this,
                o = self.options,
                container = self.getContainer(),
                eventName;
            self._isFullScreen = enabled;
            if (enabled) {
                o.onFullscreen(self, api, fs);
                eventName = Editor.EVENT_FULLSCREEN_ENABLED;
            } else {
                o.onFullscreenExit(self, api, fs);
                eventName = Editor.EVENT_FULLSCREEN_DISABLED;
            }
            AcmsEvent.emit(container, eventName, {
                _relatedTarget: self.element,
                editor: self
            });
        },

        isFullScreen: function () {
            return this._isFullScreen;
        },

        /**
         * On Maximize Change Callback
         * 
         * Das Callback wird durch die Maximize Control 
         * aufgerufen, wenn sich der Maximize Mode ändert.
         * Der Aufruf der Methode dient **nicht** zum Umschalten 
         * des Maximize Mode.
         * 
         * @param {Boolean}                 enabled     Ist der Mode aktiviert?
         */
        toggleMaximize: function (enabled) {
            var self = this,
                o = self.options,
                container = self.getContainer(),
                eventName;
            self._maximized = enabled;
            if (enabled) {
                o.onMaximize(self);
                eventName = Editor.EVENT_MAXIMIZE_ENABLED;
            } else {
                o.onMaximizeExit(self);
                eventName = Editor.EVENT_MAXIMIZE_EXIT;
            }
            AcmsEvent.emit(container, eventName, {
                _relatedTarget: self.element,
                editor: self
            });
        },

        isMaximized: function () {
            return this._maximized;
        },

        /**
         * 
         * @returns {HTMLElement}   Den Editor Wrapper Container
         */
        getContainer: function () {
            return this._container;
        },

        disable: function () {
            var self = this,
                element = self.getTextarea(),
                container = self.getContainer(),
                o = self.options;
            element.disabled = true;
            container.disabled = true;
            container.setAttribute('aria-disabled', 'true');
            element.setAttribute('aria-disabled', 'true');
            Classes.addClass(element, 'disabled');
            Classes.addClass(container, 'disabled');
            o.onDisable(self);
        },

        isDisabled: function () {
            return this.element.disabled === true;
        },

        enable: function () {
            var self = this,
                element = self.getTextarea(),
                container = self.getContainer(),
                o = self.options;
            element.disabled = false;
            container.disabled = false;
            container.setAttribute('aria-disabled', 'false');
            element.setAttribute('aria-disabled', 'false');
            Classes.removeClass(element, 'disabled');
            Classes.removeClass(container, 'disabled');
            o.onEnable(self);
        },

        /**
         * Gibt den Modus zurück
         * 
         * @returns {Object}
         */
        getMode: function () {
            return this.options.mode;
        },

        focus: function () {
            var self = this,
                o = self.options,
                element = self.element,
                container = self._container;
            Classes.addClass(container, 'active');
            element.focus();
            o.onFocus(this, element);

            return this;
        },

        blur: function () {
            var self = this,
                o = self.options,
                element = self.element,
                container = self._container;
            Classes.removeClass(container, 'active');
            element.blur();
            o.onBlur(this, element);

            return this;
        },

        /**
         * Zerstören de Instanz
         */
        destroy: function () {
            var self = this,
                val = self.element.value;
            self.parent.innerHTML = "";
            self.element = self._original.cloneNode(true);
            self.parent.appendChild(self.element);
            self.element.value = val;
            AcmsEvent.fireChange(self.element);
        },

        exec: function (action) {
            var plugin = self.plugins[action] || false;
            if (!plugin) {
                Acms.Logger.logWarning('Plugin ' + action + ' nicht gefunden');
                return false;
            }
            element.focus();
            plugin.apply();

            return true;
        },


        ctrl: function (action) {
            var control = self.controls[action] || false;
            if (!control) {
                Acms.Logger.logWarning('Plugin ' + action + ' nicht gefunden');
                return false;
            }
            element.focus();
            control.control.apply();

            return true;
        },

        /**
         * Rendern des Editors
         */
        render: function () {
            var self = this,
                o = self.options,
                parent = self.element.parentNode,
                wrapper = document.createElement('div');
            self.parent = parent;
            var currentArea = Selectors.q('textarea', parent);
            parent.removeChild(currentArea);
            //parent.innerHTML = "";
            wrapper.innerHTML = self.tpl;
            while (wrapper.firstChild) {
                parent.appendChild(wrapper.firstChild);
            }
            self.element = parent.querySelector('textarea');
            self._container = parent.querySelector('.amaryllis-editor');
            var actions = Selectors.qa('[data-action]', self._container);
            var controls = Selectors.qa('[data-control]', self._container);
            var footerItems = Selectors.qa('.footer [aria-label]', self._container);
            actions.forEach(function (btn) {
                if (btn.getAttribute('aria-label')) {
                    Tooltip.init(btn, {
                        title: btn.getAttribute('aria-label')
                    });
                }
            });
            controls.forEach(function (btn) {
                if (btn.getAttribute('aria-label')) {
                    Tooltip.init(btn, {
                        title: btn.getAttribute('aria-label')
                    });
                }
            });
            footerItems.forEach(function (btn) {
                Tooltip.init(btn, {
                    title: btn.getAttribute('aria-label')
                });
            });
            self._listen();
            var action, plugins = self.plugins;
            for (action in plugins) {
                if (plugins.hasOwnProperty(action)) {
                    if (typeof plugins[action].onRender === 'function') {
                        plugins[action].onRender();
                    }
                }
            }
            var control;
            self._controls = new Controls(self);
            self.controls = self._controls.getControls();
            for (action in self.controls) {
                if (self.controls.hasOwnProperty(action)) {
                    control = self.controls[action];
                    self.hotkeys[control.hotkey] = control.control;
                }
            }
        },

        /**
         * Aufsetzen der Event Listener
         * 
         * @returns {void}
         */
        _listen: function () {
            var self = this,
                ele = self.element,
                heading = Selectors.q('.heading', self._container),
                btns = Selectors.qa('[data-action]', heading);
            AcmsEvent.on(heading, 'click', '[data-action]', self._onClickAction, true);
            AcmsEvent.on(heading, 'click', '[data-control]', self._onClickControl, true);
        },

        /**
         * On Click Control Event Handler
         * 
         * @param {Event} evt   Das Event
         */
        _onClickControl: function (evt) {
            var self = this,
                o = self.options,
                element = self.element,
                target = evt.target;
            if (target.tagName.toLowerCase() !== 'button') {
                target = Selectors.closest(target, 'button');
            }
            if (target.disabled || this.element.disabled || this.disabled) {

                return;
            }
            var action = target.acmsData('control');
            var control = self.controls[action] || false;
            if (!control) {
                Acms.Logger.logWarning('Control ' + action + ' nicht gefunden');
                return;
            }
            element.focus();
            control.control.apply(evt);
            evt.stopPropagation();
            evt.preventDefault();
            return false;
        },

        /**
         * On Click Plugin Button Action
         * 
         * @param {Event} evt Das Click Event
         * @returns {void|boolean}
         */
        _onClickAction: function (evt) {
            var self = this,
                o = self.options,
                element = self.element,
                target = evt.target;
            if (target.tagName.toLowerCase() !== 'button') {
                target = Selectors.closest(target, 'button');
            }
            if (target.disabled || this.element.disabled || this.disabled) {

                return false;
            }
            var action = target.acmsData('action');
            var plugin = self.plugins[action] || false;
            if (!plugin) {
                Acms.Logger.logWarning('Plugin ' + action + ' nicht gefunden');
                return;
            }
            element.focus();
            plugin.apply(evt);
            evt.preventDefault();
            return false;
        },

        _setSetup: function (setup) {
            var self = this,
                o = self.options,
                mode = o.mode;
            if (mode.fullscreen) {

                setup.heading.controls.fullscreen = true;
            }
            self.setup = setup;
            var tpl = o.template,
                data = {
                    editor: setup,
                    fullscreenCaption: A.Translator.t("Vollbild umschalten"),
                    helpCaption: A.Translator.t("Hilfe"),
                    infoCaption: A.Translator.t('Info')
                };
            if (tpl.substring(tpl.length - 4) === '.hbs') {
                TemplateEngine.compileResource(tpl, data, function (content) {
                    self.tpl = content;
                    self.render();
                });
            } else {
                self.tpl = TemplateEngine.compile(tpl, data);
                self.render();
            }
        },

        /**
         * Vorbereiten
         */
        _prepare: function () {
            var self = this,
                o = self.options,
                element = self.element,
                mode = self.getMode(),
                cb = function (setup, plugins, hotkeys) {
                    self.plugins = plugins;
                    self.hotkeys = hotkeys;
                    self._setSetup(setup);
                };
            self.generator = new Generator(self, o, element, mode);
            self.generator.generate(cb);
            self._oldContent = element.value;
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        }
    };

    return Editor;

});
