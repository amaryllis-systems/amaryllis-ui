/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @copyright 2013 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright 2014 - 2018 Amaryllis Systems GmbH http://www.amws.eu/
 * @copyright 2017 - 2018 Amaryllis CMS Project http://www.amaryllis-cms.de/
 * @license https://www.amaryllis-cms.de/lizenzen/
 * @license Apache Licence 2.0 https://www.apache.org/licenses/LICENSE-2.0
 *
 * Dual-Licensed with Amaryllis CMS EULA. You may obtain a copy of the
 * EULA at
 *
 *     https://www.amaryllis-cms.de/lizenzen/eula/
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

/**
 * Cursor
 * 
 * @param {events/event} AcmsEvent
 * @returns {Cursor}
 */
 define('editor/amaryllis/lib/cursor', [
     'events/event'
 ], function(AcmsEvent) {

    "use strict";
    
    /**
     * Cursor
     * 
     * @param {editor/amaryllis/lib/amaryllis} editor
     * 
     * @exports editor/amaryllis/lib/cursor
     */
    var Cursor = function(editor) {
        this.element =
        this.editor = null;
        this.editor = editor;
        this.element = editor.getTextarea();
    };
    
    Cursor.prototype = {
        
        constructor: Cursor,
        
        /**
         * Ist die selection im Browser unterstützt?
         * 
         * @returns {Boolean}   TRUE wenn unterstützt
         */
        isSelectionSupported: function() {
            return 'selectionStart' in this.element;
        },
        
        /**
         * Gibt das Zeichen an der angegeben position zurück
         * 
         * Ausgegangen wird hier vom aktuellen Cursor. Erwartet wird eine
         * ganze Zahl, kann aber grö0er wie kleiner 0 sein.
         * 
         * @returns {String|null}   NULL wenn nicht unterstützt, einen leeren String wenn es den Wert/das Offset nicht gibt 
         */
        peek: function(offset) {
            offset = (typeof offset === "undefined") ? 1 : offset;
            var sel = this.getSelection();
            if(null === sel)  {
                return null;
            }
            var val = this.editor.getContent();
            if((sel.start + offset +1) < val.length) {
                return "";
            }
            
            return val.substr(sel.start + offset, 1);
            
        },

        /**
         * Gibt die Position des letzten Line Break zurück.
         * 
         * Sucht im Content der Textarea nach dem letzten Zeilenumbruch 
         * basierend auf der aktuellen Cursor Position.
         * 
         * @param {Number}  index   Optionaler Index, vor dem gesucht werden 
         *                          soll. Wenn nicht angegeben wird die aktuelle 
         *                          Cursor Position genommen 
         * 
         * @returns {Number}        Position des Line Break oder -1 wenn kein 
         *                          Line Break.
         */
        getLastLineBreakPosition: function(index) {
            var chars = this.editor.getContent().split('');
            var enterIndex = index || this.getSelection().start;
            var priorNewlineIndex = -1; // initial line break at before index 0
            for (var i = enterIndex - 2; i >= 0; i--) {
                if (chars[i] === '\n') {
                    priorNewlineIndex = i;
                    break;
                }
            }

            return priorNewlineIndex;
        },

        
        /**
         * Gibt das erste Zeichen nach dem vorigen Line Break
         * 
         * @param {Number}  index   Optionaler Index, vor dem gesucht werden 
         *                          soll. Wenn nicht angegeben wird die aktuelle 
         *                          Cursor Position genommen 
         * 
         * 
         * @returns {String}    Das Zeichen
         */
        getFirstCharAfterLastLineBreak: function(index) {
            var chars = this.editor.getContent().split('');
            var priorNewlineIndex = this.getLastLineBreakPosition(index);
            
            var char = chars[priorNewlineIndex + 1];

            return char;
        },

        /**
         * Gibt die Position des nächsten Vorkommens des Zeichens zurück
         * 
         * @param {Number} enterIndex   Der Index ab dem gesucht werden soll
         * @param {String} char         Das Zeichen 
         * 
         * @returns {Number}    Die Position oder -1 wenn nicht gefunden.
         */
        getPreviousCharPosition: function(enterIndex, char) {
            var index = -1, chars = this.editor.getContent().split('');
            for (var i = enterIndex; i >= 0; i--) {
                if (chars[i] === char) {
                    index = i;
                    break;
                }
            }

            return index;
        },

        /**
         * Gibt die Zeile und Spalte der aktuellen Cursor-Position zurück
         * 
         * @returns {Array}     Indizierten Array mit der aktuellen Zeilennummer 
         *                      an Position 0 und der Spalte an Position 1
         */
        getLineNumberAndColumnIndex: function() {
            var content = this.editor.getContent(),
                selection = this.getSelection();
            if(null === selection) {
                return [0,0];
            }
            var textLines = content.substr(0, selection.start).split("\n");
            var currentLineNumber = textLines.length;
            var currentColumnIndex = textLines[textLines.length-1].length;
            
            return [currentLineNumber, currentColumnIndex];
        },

        /**
         * Gibt die Position des nächsten Vorkommens des Zeichens zurück
         * 
         * @param {Number} enterIndex   Der Index ab dem gesucht werden soll
         * @param {String} char         Das Zeichen 
         * 
         * @returns {Number}    Die Position oder -1 wenn nicht gefunden.
         */
        getNextCharPosition: function(enterIndex, char) {
            var index = -1, chars = this.editor.getContent().split('');
            for (var i = enterIndex; i <= chars.length; i++) {
                if (chars[i] === char) {
                    index = i;
                    break;
                }
            }

            return index;
        },
        
        /**
         * Gibt die aktuellen Cursor Positionen zurück
         * 
         * @property {Number}   start   Start-Punkt
         * @property {Number}   end     End-Punkt
         * @property {Number}   length  Länge der Auswahl oder 0 wenn beide Punkte an einer Stelle stehen (nichts ausgewählt)
         * @property {String}   text    Ausgewählter Text
         * 
         * @returns {Object|null}   Das Object oder NULL wenn nicht unterstützt
         */
        getSelection: function () {
            if (!this.isSelectionSupported()) {
                return null;
            }
            var element = this.element;
            return {
                start: element.selectionStart,
                end: element.selectionEnd,
                length: (element.selectionEnd - element.selectionStart),
                text: element.value.substring(element.selectionStart, element.selectionEnd)
            };
        },
        
        
        /**
         * Setzen der Auswahl im Cursor
         * 
         * Setzt man Start- und End-Punkt auf den gleichen Wert, 
         * ist kein Text ausgewählt und der Cursor steht nur an 
         * dieser Stelle.
         * 
         * @param {Number}  start   Start-Punkt der Auswahl 
         * @param {Number}  end     End-Punkt der Auswahl.
         * @returns 
         */
        setSelection: function (start, end) {
            if (!this.isSelectionSupported()) {
                return null;
            }
            var element = this.element;
            element.selectionStart = start;
            element.selectionEnd = end;
        },
        
        /**
         * 
         * @param {String} text
         * @returns {Cursor.prototype}
         */
        replaceSelection: function (text) {
            var self = this,
                element = self.element,
                value = element.value;
            if (!this.isSelectionSupported()) {
                element.value += text;
                AcmsEvent.fireChange(self.element);
                return this;
            }
            element.value = value.substring(0, element.selectionStart) + text + value.substring(element.selectionEnd, value.length);
            element.selectionStart = element.value.length;
            AcmsEvent.fireChange(self.element);

            return this;
        },

        /**
         * Finded die Start- und End-Position für einen Chunk
         * 
         * @param {String} chunk    Der zu suchende Chunk
         * 
         * @returns {Object|null}   Die Auswahl oder NULL wenn nicht unterstützt oder nicht gefunden
         */
        findSelection: function (chunk) {
            var content = this.editor.getContent(),
                startChunkPosition = content.indexOf(chunk);
            if (!this.isSelectionSupported() || startChunkPosition < 0 || chunk.length === 0) {
                return null;
            }
        
            var oldSelection = this.getSelection(),
                selection;

            this.setSelection(startChunkPosition, startChunkPosition + chunk.length);
            selection = this.getSelection();

            this.setSelection(oldSelection.start, oldSelection.end);

            return selection;
        },

        /**
         * Gibt einen Text-Ausschnitt zurück
         * 
         * @param {Number} start    Start-Position (ab 0, die Start-Position ist im neuen String 
         *                          enthalten)
         * @param {Number} end      End-Position. Die End-Position meint "bis zu". Dementsprechend 
         *                          ist das Zeichen an der End-Position nicht enhalten.
         *                          Die Methode nicmmt string.prototype.substring um den String auszuschneiden. 
         *                          Damit gelten auch die gleichen Restriktionen/Voraussetzungen.
         * 
         * @returns {String}  
         */
        getTextRange: function(start, end) {
            var self = this,
                element = self.element,
                value = element.value;
            return value.substring(start, end);
        }
        
    };

    return Cursor;
    
 });
 