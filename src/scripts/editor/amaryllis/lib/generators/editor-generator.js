/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @copyright 2013 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright 2014 - 2018 Amaryllis Systems GmbH http://www.amws.eu/
 * @copyright 2017 - 2018 Amaryllis CMS Project http://www.amaryllis-cms.de/
 * @license https://www.amaryllis-cms.de/lizenzen/
 * @license Apache Licence 2.0 https://www.apache.org/licenses/LICENSE-2.0
 *
 * Dual-Licensed with Amaryllis CMS EULA. You may obtain a copy of the
 * EULA at
 *
 *     https://www.amaryllis-cms.de/lizenzen/eula/
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */


define('editor/amaryllis/lib/generators/editor-generator', [
    
], function() {

    "use strict";

    /**
     * @param {Object} editor Die Editor Instanz
     * @param {Object} config Editor Konfiguration
     * @param {HTMLElement} tarea Die Textarea
     * @param {Object} mode Der Editor Modus
     * 
     * @exports editor/amaryllis/lib/generators/editor-generator
     */
    var Generator = function(editor, config, tarea, mode) {
        this.editor = editor;
        this.config = config;
        this.tarea = tarea;
        this.mode = mode;
        this.started = 0;
        this.processed = 0;
    };
    
    Generator.prototype = {

        constructor: Generator,

        /**
         * Generieren des Editor Setup
         *
         * @param {Callable} callback
         */
        generate: function(callback) {
            this.callback = callback;
            this.setup = {
                heading: {
                    rows: {},
                    controls: {}
                },
                tarea: this.tarea.outerHTML
            };
            this.plugins = {};
            this.hotkeys = {};
            this.running = true;
            this._processRows(this.mode);
        },

        /**
         * Durchlauf aller Action Rows aus dem Mode
         * 
         * @param {Object} mode Der Modus
         */
        _processRows: function(mode) {
            var rows = mode.rows, row, prop;
            for(prop in rows) {
                if(rows.hasOwnProperty(prop)) {
                    row = rows[prop];
                    this.setup.heading.rows[prop] = {
                        groups: {}
                    };
                    this._processRow(row, prop);
                }
            }
            this.running = false;
        },

        /**
         * Durchlauf einer einzelnen Row
         * 
         * @param {Object} row      Row
         * @param {String} name     Name der Row
         */
        _processRow: function(row, name) {
            var group, prop, btns, btn;
            this.setup.heading.rows[name].groups = {};
            for(prop in row) {
                if(row.hasOwnProperty(prop)) {
                    group = row[prop];
                    this.setup.heading.rows[name].groups[prop] = {label: group.label, actions: {}};
                    for(btns in group.actions) {
                        this.started++;
                        this._preloadPlugin(group.actions[btns], name, prop);
                    }
                }
            }
        },

        /**
         * Laden eines Plugins aus dem Mode
         * 
         * @param {String} action       Name der Action
         * @param {String} rowName      Name der Action Row
         * @param {String} groupName    Name der Plugin Gruppe
         */
        _preloadPlugin: function(action, rowName, groupName) {
            
            var self = this, mod;
            if(action.indexOf('/') === -1) {
                mod = 'editor/amaryllis/lib/plugins/' + action;
            } else {
                mod = action;
            }
            self.setup.heading.rows[rowName].groups[groupName].actions[action] = {};
            require([mod], function(obj) {
                var plugin = new obj(self.editor, self.config);
                self.setup.heading.rows[rowName].groups[groupName].actions[action] = {
                    label: plugin.getLabel(),
                    name: plugin.getName(),
                    icon: plugin.options.icon,
                    plugin: plugin,
                    action: plugin.getMethod()
                };
                self.plugins[plugin.getMethod()] = plugin;
                if('getHotkey' in plugin) {
                    self.hotkeys[plugin.getHotkey()] = plugin;
                }
                self.processed++;
                if(self.processed === self.started && !self.running) {
                    self.callback(self.setup, self.plugins, self.hotkeys);
                }
            });
        }
    };

    return Generator;

});