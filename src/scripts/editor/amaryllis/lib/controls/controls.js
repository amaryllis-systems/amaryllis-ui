/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @copyright 2013 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright 2014 - 2018 Amaryllis Systems GmbH http://www.amws.eu/
 * @copyright 2017 - 2018 Amaryllis CMS Project http://www.amaryllis-cms.de/
 * @license https://www.amaryllis-cms.de/lizenzen/
 * @license Apache Licence 2.0 https://www.apache.org/licenses/LICENSE-2.0
 *
 * Dual-Licensed with Amaryllis CMS EULA. You may obtain a copy of the
 * EULA at
 *
 *     https://www.amaryllis-cms.de/lizenzen/eula/
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */


define('editor/amaryllis/lib/controls/controls', [
    'editor/amaryllis/lib/controls/fullscreen',
    'editor/amaryllis/lib/controls/maximize',
    'editor/amaryllis/lib/controls/help',
    'editor/amaryllis/lib/controls/hotkeys'
], function(FullScreen, Maximize, Help, Hotkeys) {

    'use strict';
    
    var Controls = function(editor) {
        this.editor = 
        this.fullscreen =
        this.hotkeys =
        this.maximize =
        this.help = null;
        this.initialize(editor);
    };

    Controls.prototype = {

        constructor: Controls,

        /**
         * 
         * 
         * @param {any} editor 
         */
        initialize: function(editor) {
            this.editor = editor;
            this.fullscreen = new FullScreen(this, editor);
            this.help = new Help(this, editor);
            this.hotkeys = new Hotkeys(this, editor);
            this.maximize = new Maximize(this, editor);
            this._prepare();
        },

        getControls: function() {
            return this.controls;
        },

        _prepare: function() {
            var self = this, editor = self.editor;
            self.controls = {
                help: {
                    label: this.help.getLabel(),
                    hint: this.help.getHelp(),
                    hotkey: this.help.getHotkey(),
                    control: this.help
                },
                fullscreen: {
                    label: this.fullscreen.getLabel(),
                    hint: this.fullscreen.getHelp(),
                    hotkey: this.fullscreen.getHotkey(),
                    control: this.fullscreen
                },
                maximize: {
                    label: this.maximize.getLabel(),
                    hint: this.maximize.getHelp(),
                    hotkey: this.maximize.getHotkey(),
                    control: this.maximize
                }
            };
            if(editor.options.controls) {
                editor.options.controls.forEach(function(control) {
                    control.editor = editor;
                    control.controls = self;
                    self.controls[control.getName()] = {
                        label: control.getLabel(),
                        hint: control.getHelp(),
                        hotkey: control.getHotkey(),
                        control: control 
                    };
                });
            }
        }
    };

    return Controls;

});
