/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @copyright 2013 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright 2014 - 2018 Amaryllis Systems GmbH http://www.amws.eu/
 * @copyright 2017 - 2018 Amaryllis CMS Project http://www.amaryllis-cms.de/
 * @license https://www.amaryllis-cms.de/lizenzen/
 * @license Apache Licence 2.0 https://www.apache.org/licenses/LICENSE-2.0
 *
 * Dual-Licensed with Amaryllis CMS EULA. You may obtain a copy of the
 * EULA at
 *
 *     https://www.amaryllis-cms.de/lizenzen/eula/
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

define("editor/amaryllis/lib/controls/help", [
    'tools/utils',
    'core/selectors',
    'text!templates/html/amaryllis-editor-help.hbs!strip',
    'apps/ui/informations/modal',
    'events/event',
    'core/acms'
], function (Utils, Selectors, Template, Modal, AcmsEvent, A) {

    "use strict";

    var Control = function (controls, editor) {
        this.config =
            this.editor =
            this.plugins =
            this.controls = null;
        this.initialize(controls, editor);
    };

    Control.init = function (controls, editor) {
        return new Control(controls, editor);
    };

    /**
     * Standard Optionen
     * 
     * @type {object}
     */
    Control.DEFAULT_CONFIG = {
        label: A.Translator._('Hilfe'),
        hotkey: 'f1',
        template: Template
    };

    /**
     * Template Konfiguration
     * 
     * @type {object}
     */
    Control.TEMPLATE_CONFIG = {
        title: A.Translator._('Amaryllis Markdown Editor Hilfe'),
        closeText: A.Translator._('Schließen'),
        
    };


    Control.prototype = {
        constructor: Control,

        initialize: function (controls, editor) {
            this.editor = editor;
            this.controls = controls;
            this._prepare();
        },

        /**
         * Name der Control
         * 
         * @returns {String}
         */
        getName: function () {
            return 'help';
        },

        /**
         * Methode der Control
         * 
         * @returns {String}
         */
        getMethod: function () {
            return 'help';
        },

        /**
         * Hotkey der Control
         * 
         * @returns {String}
         */
        getHotkey: function () {
            return this.options.hotkey;
        },

        /**
         * Label der Control
         * 
         * @returns {String}
         */
        getLabel: function () {
            return this.options.label;
        },

        getHelp: function () {
            return Acms.Translator.t('Gibt diese Hilfe aus.');
        },

        /**
         * Anwenden der Control
         * 
         * @param {Event} evt Das Event
         */
        apply: function (evt) {
            var self = this,
                editor = self.editor,
                o = self.options,
                data;
            self._preparePlugins();
            self._prepareControls();
            data = this.templateConfig;
            var btn = editor.getContainer().querySelector('[data-control="help"]');
            var cb = function (modal, button, ctx, tplData) {
                modal.show();
                self.modal = modal;
                self.button = button;
                self.ctx = ctx;
                self.tplData = tplData;
                self.box = Selectors.q('#' + tplData.id);
                AcmsEvent.on(self.box, 'click', '[data-dismiss="modal"]', self._onDismiss);
            };
            Modal.createModal(o.template, data, btn, cb);
        },


        _onDismiss: function () {
            var self = this,
                id = self.tplData.id;
            self.modal.hide();
            var modal = self.box;
            AcmsEvent.off(modal, 'click', '[data-dismiss="modal"]', self._onDismiss);
            setTimeout(function () {
                document.body.removeChild(modal);
                self.modal = self.ctx = self.tplData = null;
                if (self.button) {
                    self.button.acmsData(Modal.NS, null);
                }
            }, 1000);
        },

        /**
         * Vorbereiten
         */
        _prepare: function () {
            var self = this,
                config = self.editor.options,
                options;
            if (null !== config.controls &&
                typeof config.controls === 'object' &&
                typeof config.controls.help === 'object') {
                options = Utils.extend({}, Control.DEFAULT_CONFIG, config.controls.help);
            } else {
                options = Control.DEFAULT_CONFIG;
            }
            this.options = options;
            this.templateConfig = Control.TEMPLATE_CONFIG;
            this.templateConfig.plugins = {};
            this.templateConfig.controls = {};
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        },

        _preparePlugins: function () {
            var self = this,
                editor = self.editor,
                plugins = editor.plugins,
                j, plugin;
            for (j in plugins) {
                if(!plugins.hasOwnProperty(j)) {
                    continue;
                }
                plugin = plugins[j];
                self.templateConfig.plugins[plugin.getName()] = {
                    'hotkey': self._transformHotkey((typeof plugin.getHotkey !== 'undefined') ? plugin.getHotkey() : ''),
                    'label': plugin.getLabel(),
                    'hint': (typeof plugin.getHelp === 'function') ? plugin.getHelp() : (plugin.options && typeof plugin.options === 'object' && plugin.options.hint ? plugin.options.hint : ''),
                };
            }
        },

        _prepareControls: function () {
            var self = this,
                ctrl = self.controls,
                controls = ctrl.getControls(),
                j, control;
            for (j in controls) {
                if(!controls.hasOwnProperty(j)) {
                    continue;
                }
                control = controls[j].control;
                self.templateConfig.controls[control.getName()] = {
                    'hotkey': self._transformHotkey((typeof control.getHotkey !== 'undefined') ? control.getHotkey() : ''),
                    'label': control.getLabel(),
                    'hint': (typeof control.getHelp === 'function') ? control.getHelp() : (control.options && typeof control.options === 'object' && control.options.hint ? control.options.hint : ''),
                };
            }

        },

        /**
         * Hotkey zu Array
         * 
         * @param {String} hotkey   Der Hotkey
         * 
         * @returns {Array}
         */
        _transformHotkey: function (hotkey) {
            var last, current, arr = hotkey.split('+'),
                keys = [], hasPlus = false,
                j, plus = '+';
            for (j = 0; j < arr.length; j++) {
                current = arr[j];
                if(typeof current === 'undefined' || current.trim() === '') {
                    current = plus;
                }
                if(current === plus && true === hasPlus) {
                    continue;
                } else if(current === plus) {
                    hasPlus = true;
                }
                last = current;
                keys.push(current);
            }
            return keys;
        }

    };


    return Control;

});
