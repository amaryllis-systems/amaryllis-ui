/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @copyright 2013 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright 2014 - 2018 Amaryllis Systems GmbH http://www.amws.eu/
 * @copyright 2017 - 2018 Amaryllis CMS Project http://www.amaryllis-cms.de/
 * @license https://www.amaryllis-cms.de/lizenzen/
 * @license Apache Licence 2.0 https://www.apache.org/licenses/LICENSE-2.0
 *
 * Dual-Licensed with Amaryllis CMS EULA. You may obtain a copy of the
 * EULA at
 *
 *     https://www.amaryllis-cms.de/lizenzen/eula/
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */


define('editor/amaryllis/lib/controls/hotkeys', [
    'tools/utils',
    'core/selectors',
    'events/event',
    'core/acms'
], function (Utils, Selectors, AcmsEvent, A) {

    'use strict';

    /**
     * Hotkeys
     * 
     * Listener für Keyboard Shortcuts im Editor.
     * 
     * @param {editor/amaryllis/lib/controls/controls} controls 
     * @param {editor/amaryllis/lib/amaryllis} editor 
     */
    var Hotkeys = function (controls, editor) {
        this.editor =
            this.controlls =
            this.onChange = null;
        this.initialize(controls, editor);
    };

    var ctrlPressed = false,
        altPressed = false,
        dBllocked = false,
        hk = "",
        keyMap = {
            8: "backspace",
            9: "tab",
            10: "return",
            13: "return",
            16: "shift",
            17: "ctrl",
            18: "alt",
            19: "pause",
            20: "capslock",
            27: "esc",
            32: "space",
            33: "pageup",
            34: "pagedown",
            35: "end",
            36: "home",
            37: "left",
            38: "up",
            39: "right",
            40: "down",
            45: "insert",
            46: "del",
            59: ";",
            61: "=",
            91: "cmd",
            93: "cmd",
            96: "0",
            97: "1",
            98: "2",
            99: "3",
            100: "4",
            101: "5",
            102: "6",
            103: "7",
            104: "8",
            105: "9",
            106: "*",
            107: "+",
            109: "-",
            110: ".",
            111: "/",
            112: "f1",
            113: "f2",
            114: "f3",
            115: "f4",
            116: "f5",
            117: "f6",
            118: "f7",
            119: "f8",
            120: "f9",
            121: "f10",
            122: "f11",
            123: "f12",
            144: "numlock",
            145: "scroll",
            171: "+",
            173: "-",
            186: ";",
            187: "=",
            188: ",",
            189: "-",
            190: ".",
            191: "/",
            192: "`",
            219: "[",
            220: "\\",
            221: "]",
            222: "'",
            224: "cmd"
        };
    var i;
    // numpad
    for (i = 0; i < 10; i++) {
        keyMap[i + 95] = 'num-' + i;
    }
    // top row 0-9
    for (i = 0; i < 10; i++) {
        keyMap[i + 48] = '' + i;
    }
    // f1-f24
    for (i = 1; i < 25; i++) {
        keyMap[i + 111] = 'f' + i;
    }
    // alphabet
    for (i = 65; i < 91; i++) {
        keyMap[i] = String.fromCharCode(i).toLowerCase();
    }

    Hotkeys.DEFAULT_CONFIG = {
        'label': A.Translator.t('Hotkeys'),
        'hotkeys': {}
    };


    Hotkeys.prototype = {

        constructor: Hotkeys,

        /**
         * Interner Constructor
         * 
         * @param {editor/amaryllis/lib/controls/controls} controls 
         * @param {editor/amaryllis/lib/amaryllis} editor 
         */
        initialize: function (controls, editor) {
            this.editor = editor;
            if (typeof editor.options.onChange === 'function') {
                this.onChange = this.editor.onChange;
            }
            this.controls = controls;
            this._prepare();
            this.listen();
        },

        listen: function () {
            var self = this,
                editor = self.editor,
                tarea = editor.getTextarea();
            //AcmsEvent.add(document, 'keydown', self._keyDown);
            AcmsEvent.add(tarea, 'keyup', self._keyup);
            AcmsEvent.add(tarea, 'dblclick', self._updateSelection);
            AcmsEvent.add(tarea, 'mouseup', self._updateSelection);
            AcmsEvent.add(tarea, 'mousedown', self._updateSelection);
            AcmsEvent.add(tarea, 'mousemove', self._updateSelection);
        },


        /**
         * Name der Control
         * 
         * @returns {String}
         */
        getName: function () {
            return 'hotkeys';
        },

        /**
         * Methode der Control
         * 
         * @returns {String}
         */
        getMethod: function () {
            return 'hotkeys';
        },

        /**
         * Hotkey der Control
         * 
         * @returns {String}
         */
        getHotkeys: function () {
            return this.options.hotkeys;
        },

        /**
         * Label der Control
         * 
         * @returns {String}
         */
        getLabel: function () {
            return this.options.label;
        },

        getHelp: function () {
            return A.Translator.t('Auflistung aller zusätzlich unterstützter Hotkeys. Gleichzeitig stellt das Control Plugin die Hotkey Anwendung dar.');
        },


        /**
         * Key Down Event Listener
         * 
         * @param {Event} e 
         * @returns 
         */
        _keyDown: function (e) {
            var event = e;
            var editor = this.editor;
            var tarea = editor.getTextarea();
            if (tarea !== document.activeElement) {
                return;
            }
            e.stopPropagation();
            var code = parseInt(e.which || e.keyCode);
            if (code === 93 || code === 224) code = 91;
            switch (true) {
                case (17 === code):
                    ctrlPressed = true;
                    if (!/ctrl\+/i.test(hk))
                        hk += 'ctrl+';
                    break;
                case (18 === code):
                    altPressed = true;
                    if (!/alt\+/i.test(hk))
                        hk += 'alt+';
                    break;
                case (code === 16):
                    if (!/shift\+/i.test(hk))
                        hk += 'shift+';
                    break;
                default:
                    if (typeof keyMap[code] !== "undefined") {
                        hk += keyMap[code];
                    } else {
                        var str = String.fromCharCode(code).toLowerCase();
                        hk += str;
                    }
                    if (typeof this.editor.hotkeys[hk] !== "undefined") {
                        dBllocked = true;
                        e.stopPropagation();
                        e.preventDefault();
                        this.editor.hotkeys[hk].apply(e);

                        break;
                    } else
                        hk += '+';
                    break;
            }
            //console.log(hk);
            if (dBllocked) {
                e.cancelBubble = true;
                e.returnValue = false;
                e.ctrlPressed = e.altPressed = false;
                return false;
            }
        },

        _keyup: function (e) {
            if (dBllocked === true) {
                dBllocked = false;
                e.stopPropagation();
                e.preventDefault();
                hk = "";
                return false;
            }
            dBllocked = false;
            var code = parseInt(e.which || e.keyCode);
            ctrlPressed = altPressed = false;
            hk = '';
            var blocked = false,
                self = this;
            switch (code) {
                case 40: // down arrow
                case 38: // up arrow
                case 16: // shift
                    break;
                case 17: // ctrl
                    ctrlPressed = false;
                    break;
                case 18: // alt
                    altPressed = false;
                    break;

                case 9: // tab
                    /*var nextTab = self.editor.getNextTab(),
                        cursor = self.editor.getCursor(),
                        content = self.editor.getContent();
                    if (nextTab !== null) {
                        setTimeout(function () {
                            cursor.setSelection(nextTab.start, nextTab.end);
                        }, 500);
                        blocked = true;
                    } else {
                        var sel = cursor.getSelection();
                        if (null === sel) {
                            break;
                        }
                        if (sel.start === sel.end &&
                            sel.end === content.length) {
                            blocked = false;
                        } else {
                            cursor.setSelection(content.length, content.length);
                            blocked = true;
                        }
                    }*/
                    break;
                case 10:
                case 13: // enter
                    self._updateLines();
                    break;
                case 27: // escape
                    break;
                default:
                    break;
            }

            if (blocked) {
                e.stopPropagation();
                e.preventDefault();
            }
            self._updateSelection();
            self._updateLines();
            if (self.onChange) self.onChange(self.editor);
            if (blocked) {
                return false;
            }
        },


        _updateLines: function () {
            var self = this,
                editor = self.editor,
                container = editor.getContainer(),
                content = editor.getContent(),
                lines = content.split('\n').length || 1;
            Selectors.qa('[data-role="lines"]', container).forEach(function (ele) {
                ele.textContent = lines;
            });
        },

        _updateSelection: function () {
            var self = this,
                editor = self.editor,
                container = editor.getContainer(),
                cursor = editor.getCursor(),
                selection = cursor.getSelection();
            if (null === selection) {
                return;
            }
            if (selection.start === selection.end) {
                selection.start = 0;
            }
            Selectors.qa('[data-role="selection-start"]', container).forEach(function (ele) {
                ele.textContent = selection.start;
            });
            Selectors.qa('[data-role="selection-end"]', container).forEach(function (ele) {
                ele.textContent = selection.end;
            });
            var current = cursor.getLineNumberAndColumnIndex();
            Selectors.qa('[data-role="current-line"]', container).forEach(function (ele) {
                ele.textContent = current[0];
            });
            Selectors.qa('[data-role="current-column"]', container).forEach(function (ele) {
                ele.textContent = current[1];
            });
        },


        /**
         * Vorbereiten
         */
        _prepare: function () {
            var self = this,
                config = self.editor.options,
                options;
            if (null !== config.controls &&
                typeof config.controls === 'object' &&
                typeof config.controls.hotkeys === 'object') {
                options = Utils.extend({}, Hotkeys.DEFAULT_CONFIG, config.controls.hotkeys);
            } else {
                options = Hotkeys.DEFAULT_CONFIG;
            }
            this.options = options;
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
            document.onkeydown = self._keyDown;
            document.onkeyup = self._keyup;
        },

    };

    return Hotkeys;

});
