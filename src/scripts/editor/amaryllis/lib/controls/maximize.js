/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @copyright 2013 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright 2014 - 2018 Amaryllis Systems GmbH http://www.amws.eu/
 * @copyright 2017 - 2018 Amaryllis CMS Project http://www.amaryllis-cms.de/
 * @license https://www.amaryllis-cms.de/lizenzen/
 * @license Apache Licence 2.0 https://www.apache.org/licenses/LICENSE-2.0
 *
 * Dual-Licensed with Amaryllis CMS EULA. You may obtain a copy of the
 * EULA at
 *
 *     https://www.amaryllis-cms.de/lizenzen/eula/
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

/**
 * 
 * @param {Object} Utils
 * @param {Object} Selectors
 * @param {Object} Classes
 * @param {Object} FullScreen
 * @param {Object} AcmsEvent
 * @param {Object} A
 * @returns {Control}
 * 
 * @author qm-b <qm-b@outlook.de>
 */
define("editor/amaryllis/lib/controls/maximize", [
    'tools/utils',
    'core/selectors',
    'core/classes',
    'events/event',
    'core/acms'
], function (Utils, Selectors, Classes, AcmsEvent, A) {

    "use strict";

    var Control = function (controls, editor) {
        this.options =
            this.editor =
            this.controls = null;
        this.initialize(controls, editor);
    };

    Control.init = function (controls, editor) {
        return new Control(controls, editor);
    };

    Control.DEFAULT_CONFIG = {
        label: A.Translator._('Editor maximieren'),
        hotkey: 'alt+f2',
        icon: 'acms-icon acms-icon-window-maximize acms-iconr'
    };

    Control.prototype = {
        constructor: Control,

        initialize: function (controls, editor) {
            this.editor = editor;
            this.controls = controls;
            this._prepare();
        },

        /**
         * Name der Control
         * 
         * @returns {String}
         */
        getName: function () {
            return 'maximize';
        },

        /**
         * Methode der Control
         * 
         * @returns {String}
         */
        getMethod: function () {
            return 'maximize';
        },

        /**
         * Hotkey der Control
         * 
         * @returns {String}
         */
        getHotkey: function () {
            return this.options.hotkey;
        },

        /**
         * Label der Control
         * 
         * @returns {String}
         */
        getLabel: function () {
            return this.options.label;
        },

        getHelp: function () {
            return A.Translator.t('Maximieren des Editor auf die volle Browser-Größe');
        },

        apply: function (evt) {
            var self = this, editor = self.editor;
            if(Classes.hasClass(editor.getContainer(), 'in-fullscreen')) {
                return false;
            }
            Classes.toggleClass(editor.getContainer(), 'maximized');
            editor.toggleMaximize(Classes.hasClass(editor.getContainer(), 'maximized'));
        },

        /**
         * Vorbereiten
         */
        _prepare: function () {
            var self = this,
                config = self.editor.options,

                options;
            if (null !== config.controls &&
                typeof config.controls === 'object' &&
                typeof config.controls.maximize === 'object') {
                options = Utils.extend({}, Control.DEFAULT_CONFIG, config.controls.maximize);
            } else {
                options = Control.DEFAULT_CONFIG;
            }
            this.options = options;
            self.button = Selectors.q('[data-control="maximize"]');
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        },

    };


    return Control;

});