/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @copyright 2013 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright 2014 - 2018 Amaryllis Systems GmbH http://www.amws.eu/
 * @copyright 2017 - 2018 Amaryllis CMS Project http://www.amaryllis-cms.de/
 * @license https://www.amaryllis-cms.de/lizenzen/
 * @license Apache Licence 2.0 https://www.apache.org/licenses/LICENSE-2.0
 *
 * Dual-Licensed with Amaryllis CMS EULA. You may obtain a copy of the
 * EULA at
 *
 *     https://www.amaryllis-cms.de/lizenzen/eula/
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */


define("editor/amaryllis/lib/plugins/sup", [
    'tools/utils',
    'core/selectors',
    'core/classes',
    'events/event',
    'core/acms'
], function (Utils, Selectors, Classes, AcmsEvent, A) {

    "use strict";

    var Plugin = function (editor, config) {
        this.editor =
            this.config = null;
        this.initialize(editor, config);
    };

    Plugin.init = function (editor, config) {
        return new Plugin(editor, config);
    };

    Plugin.DEFAULT_CONFIG = {
        md: '^',
        icon: 'acms-icon acms-icon-superscript',
        label: A.Translator._('hochgestellt')
    };

    Plugin.prototype = {

        constructor: Plugin,

        initialize: function (editor, config) {
            this.editor = editor;
            this.config = config;
            this._prepare();
        },

        /**
         * Name des Plugins
         * 
         * @returns {String}
         */
        getName: function () {
            return 'superscript';
        },

        /**
         * Label des Plugins
         * 
         * @returns {String}
         */
        getLabel: function () {
            return A.Translator._('hochgestellt');
        },

        /**
         * Gibt die Methode/den Command des Plugins zurück
         * 
         * @returns 
         */
        getMethod: function () {
            return 'sup';
        },

        getHotkey: function() {
            return 'ctrl+shift++';
        },

        /**
         * Event Callback
         * 
         * @param {Event} evt   Das Event
         */
        apply: function (evt) {
            var self = this,
                editor = self.editor,
                o = self.options,
                chunk,
                cursor = editor.getCursor(),
                selection = cursor.getSelection(),
                text,
                content = editor.getContent(),
                pointer, prevChar, pos, reg = new RegExp(/^([0-9]|[a-z])+([0-9a-z]+)$/gi);

            if(!reg.test(content.substr(selection.start-1, 1))) {
                //return;
            }
            text = selection.text ? selection.text + ' ' : '';
            pos = selection.start+1;
            chunk = o.md+text;
            cursor.replaceSelection(chunk);
            cursor.setSelection(pos, pos + text.length);
        },


        _prepare: function () {
            var self = this,
                config = self.config,
                options;
            if (null !== config.plugins &&
                typeof config.plugins === 'object' &&
                typeof config.plugins.sup === 'object') {
                options = Utils.extend({}, Plugin.DEFAULT_CONFIG, config.plugins.sup);
            } else {
                options = Plugin.DEFAULT_CONFIG;
            }
            this.options = options;
        }


    };

    return Plugin;

});