/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("editor/amaryllis/lib/plugins/anchor", [
    'tools/object/extend',
    'tools/string/sprintf',
    'core/selectors',
    'core/classes',
    "tools/string/string-tool",
    'events/event',
    'text!templates/html/amaryllis-editor-url.hbs!strip',
    'apps/ui/informations/modal',
    'http/module-loader',
    'core/acms'
], (Extend, sprintf, Selectors, Classes, StringTool, AcmsEvent, Template, Modal, Loader, A) => {

    "use strict";

    /**
     * anchor Plugin Constructor
     * 
     * @param {editor/amaryllis/lib/amaryllis} editor 
     * @param {Object} config 
     * 
     * @exports editor/amaryllis/lib/plugins/anchor
     */
    var Plugin = function (editor, config) {
        this.editor =
            this.config =
            this.button = null;
        this.initialize(editor, config);
    };

    /**
     * anchor Plugin Constructor
     * 
     * @param {editor/amaryllis/lib/amaryllis} editor 
     * @param {Object} config 
     * 
     * @returns {Plugin}    Neue instanz
     */
    Plugin.init = function (editor, config) {
        return new Plugin(editor, config);
    };

    /**
     * @var {Object}
     */
    Plugin.DEFAULT_CONFIG = {
        label: A.Translator._('Link einfügen'),
        md: '[%s](%s "%s")',
        icon: 'acms-icon acms-icon-link',
        template: Template,
        expert: true
    };

    Plugin.TEMPLATE_CONFIG = {
        title: A.Translator._('Link einfügen'),
        closeText: A.Translator._('Abbrechen'),
        submitText: A.Translator._('Übernehmen'),
        captionText: A.Translator._('Link-Caption'),
        titleText: A.Translator._('Link-Titel'),
        urlText: A.Translator._('Die URL'),
        targetText: A.Translator._('In neuem Tab öffnen?'),
        cssText: A.Translator._('CSS Klassen')
    };

    Plugin.prototype = {

        constructor: Plugin,

        /**
         * Interner Constructor
         * 
         * @param {editor/amaryllis/lib/amaryllis} editor 
         * @param {Object} config 
         */
        initialize: function (editor, config) {
            this.editor = editor;
            this.config = config;
            this._prepare();
        },

        /**
         * Name des Plugins
         * 
         * @returns {String}
         */
        getName: function () {
            return 'anchor';
        },

        /**
         * Label des Plugins
         * 
         * @returns {String}
         */
        getLabel: function () {
            return this.options.label;
        },

        /**
         * Gibt die Methode/den Command des Plugins zurück
         * 
         * @returns 
         */
        getMethod: function () {
            return 'anchor';
        },

        /**
         * Gibt den Hot Key zurück
         * 
         * @returns {String}
         */
        getHotkey: function () {
            return 'ctrl+l';
        },


        getHelp: function () {
            return A.Translator.t('Fügt einen Link ein. Über das Formular können Link-Caption und Link-Title eingefügt sowie Ziel und CSS Klassen festgelegt werden. Das Ergebnis in Markdown würde wie folgt ausssehen: %s', 'Core', 'de_DE', ['<code class="inline-code">[Caption](url "title")</code>']);
        },


        /**
         * Event Callback
         * 
         * @param {Event} evt   Das Event
         */
        apply: function (evt) {
            var self = this,
                editor = self.editor,
                o = self.options,
                cursor = editor.getCursor(),
                sel = cursor.getSelection() || {
                    text: '',
                    length: 0
                },
                chunk, text, pos, data = Plugin.TEMPLATE_CONFIG,
                content = editor.getContent();
            data.expertMode = o.expert;
            if (sel.length > 0) {
                data.selectedValue = sel.text;
            }
            var btn = editor.getContainer().querySelector('[data-action="anchor"]');
            var cb = function (modal, button, ctx, tplData) {
                modal.show();
                self.modal = modal;
                self.button = button;
                self.ctx = ctx;
                self.tplData = tplData;
                self.box = Selectors.q('#' + tplData.id);
                Loader.load(self.box);
                Selectors.q('input', self.box).focus();
                self.saveButton = Selectors.q('#' + tplData.id + ' button[data-action="submit"]');
                AcmsEvent.add(self.saveButton, 'click', self._onSubmitModal);
                AcmsEvent.on(self.box, 'change', 'input', self._onChange);
                AcmsEvent.on(self.box, 'blur', 'input', self._onChange);
                AcmsEvent.on(self.box, 'click', '[data-dismiss="modal"]', self._onDismiss);
            };
            Modal.createModal(o.template, data, btn, cb);
        },

        _onDismiss: function () {
            var self = this,
                id = self.tplData.id;
            self.modal.hide();
            var modal = self.box;
            AcmsEvent.remove(self.saveButton, 'click', self._onSubmitModal);
            AcmsEvent.off(modal, 'change', 'input', self._onChange);
            AcmsEvent.off(modal, 'blur', 'input', self._onChange);
            AcmsEvent.off(modal, 'click', '[data-dismiss="modal"]', self._onDismiss);
            setTimeout(function () {
                document.body.removeChild(modal);
                self.modal = self.ctx = self.tplData = null;
                if (self.button) {
                    self.button.acmsData(Modal.NS, null);
                }
            }, 1000);
        },

        _onChange: function (e) {
            var self = this,
                target = e.target,
                fBox = Selectors.closest(target, '.has-feedback'),
                form = Selectors.closest(target, 'form'),
                hint, p;
            if (target.type === 'checkbox') {
                return;
            }

            if (target.type === 'url' && false === StringTool.isUrl(target.value)) {
                self.saveButton.disabled = true;
                self.saveButton.setAttribute('aria-disabled', 'true');
                if (fBox && !Selectors.q('.label-v2', fBox)) {
                    p = document.createElement('p');
                    Classes.addClass(fBox, 'invalid');
                    Classes.removeClass(fBox, 'valid');
                    fBox.appendChild(p);
                    Classes.addClass(p, 'top danger arrow label-v2');
                    p.innerHTML = A.Translator._('Die URL ist invalide');
                }
                return;
            } else if (target.type === 'url' && true === StringTool.isUrl(target.value)) {

                if (fBox) {
                    Classes.addClass(fBox, 'valid');
                    Classes.removeClass(fBox, 'invalid');
                    hint = Selectors.q('.label-v2', fBox);
                    if (hint) {
                        fBox.removeChild(hint);
                    }
                }

            }
            if (target.type === 'text' && target.required && '' === target.value.trim()) {
                self.saveButton.disabled = true;
                self.saveButton.setAttribute('aria-disabled', 'true');
                if (fBox && !Selectors.q('.label-v2', fBox)) {
                    p = document.createElement('p');
                    Classes.addClass(fBox, 'invalid');
                    Classes.removeClass(fBox, 'valid');
                    fBox.appendChild(p);
                    Classes.addClass(p, 'top danger arrow label-v2');
                    p.innerHTML = A.Translator._('Das Feld ist erforderlich');
                }
                return;
            } else if (target.type === 'text' && target.required && '' !== target.value.trim()) {
                if (fBox) {
                    Classes.addClass(fBox, 'valid');
                    Classes.removeClass(fBox, 'invalid');
                    hint = Selectors.q('.label-v2', fBox);
                    if (hint) {
                        fBox.removeChild(hint);
                    }
                }
            }

            if (Selectors.qa('.invalid', form).length === 0) {
                self.saveButton.disabled = false;
                self.saveButton.setAttribute('aria-disabled', 'false');
            }
        },

        _onSubmitModal: function (e) {
            this.editor.getTextarea().focus();
            var self = this,
                t = e.target,
                form = Selectors.closest(t, 'form'),
                id = self.tplData.id,
                url = Selectors.q('input[name="url"]', form),
                caption = Selectors.q('input[name="caption"]', form),
                title = Selectors.q('input[name="title"]', form),
                o = self.options,
                cursor = self.editor.getCursor(),
                selection = cursor.getSelection(),
                content = self.editor.getContent(),
                last, div = document.createElement('div');
            div.textContent = url.value;
            var urlVal = div.textContent;
            div.textContent = caption.value;
            var captionVal = div.textContent;
            div.textContent = title.value;
            var titleVal = div.textContent;
            if (t.disabled) {
                e.preventDefault();
                e.stopPropagation();
                return false;
            }
            var chunk = sprintf(o.md, captionVal, urlVal, titleVal);
            if (self.tplData.expertMode) {
                var target = Selectors.q('input[name="target"]', form);
                var css = Selectors.q('input[name="css"]', form);
                var expert = '';
                if (target.checked) {
                    expert += 'target="_blank" ';
                }
                if (css.value) {
                    div.className = css.value;
                    var cssVal = div.className.split(',').join(' ');
                    expert += 'class="' + cssVal + '"';
                }
                if (expert.trim() !== '') {
                    chunk += '{' + expert.trim() + '} ';
                }
            }
            if (selection.start !== 0 && (last = content.substr(selection.start - 1, 1)) !== ' ' && last !== '\n') {
                chunk = ' ' + chunk;
            }
            cursor.replaceSelection(chunk);
            cursor.setSelection(selection.end + chunk.length, selection.end + chunk.length);
            self.modal.hide();
            var modal = self.box;
            AcmsEvent.remove(self.saveButton, 'click', self._onSubmitModal);
            AcmsEvent.off(modal, 'change', 'input', self._onChange);
            AcmsEvent.off(modal, 'blur', 'input', self._onChange);
            setTimeout(function () {
                document.body.removeChild(modal);
                if (self.button) {
                    self.button.acmsData(Modal.NS, null);
                }
                self.modal = self.ctx = self.tplData = null;
            }, 1000);
            e.preventDefault();
            e.stopPropagation();
            return false;
        },

        _prepare: function () {
            var self = this,
                config = self.config,
                options;
            if (config.plugins &&
                typeof config.plugins === 'object' &&
                typeof config.plugins.anchor === 'object') {
                options = Extend.flat({}, false, Plugin.DEFAULT_CONFIG, config.plugins.anchor);
            } else {
                options = Plugin.DEFAULT_CONFIG;
            }
            this.options = options;
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        }

    };

    return Plugin;

});
