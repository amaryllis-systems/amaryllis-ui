/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("editor/amaryllis/lib/plugins/critic-mark", [
    'tools/object/extend',
    'tools/string/sprintf',
    'core/selectors',
    'core/classes',
    'events/event',
    'core/acms'
], function (Extend, sprintf, Selectors, Classes, AcmsEvent, A) {

    "use strict";

    var Plugin = function (editor, config) {
        this.editor =
            this.config = null;
        this.initialize(editor, config);
    };

    Plugin.init = function (editor, config) {
        return new Plugin(editor, config);
    };

    Plugin.DEFAULT_CONFIG = {
        md: '{== %s ==}',
        icon: 'acms-icon acms-icon-highlighter',
        label: A.Translator._('markieren'),
        hotkey: 'ctrl+shift+h'
    };

    Plugin.prototype = {

        constructor: Plugin,

        initialize: function (editor, config) {
            this.editor = editor;
            this.config = config;
            this._prepare();
        },

        /**
         * Name des Plugins
         * 
         * @returns {String}
         */
        getName: function () {
            return 'critic-mark';
        },

        /**
         * Label des Plugins
         * 
         * @returns {String}
         */
        getLabel: function () {
            return this.options.label;
        },

        /**
         * Gibt die Methode/den Command des Plugins zurück
         * 
         * @returns 
         */
        getMethod: function () {
            return 'critic-mark';
        },

        getHotkey: function() {
            return this.options.hotkey;
        },

        /**
         * Event Callback
         * 
         * @param {Event} evt   Das Event
         */
        apply: function (evt) {
            var self = this,
                editor = self.editor,
                o = self.options,
                chunk,
                cursor = editor.getCursor(),
                selection = cursor.getSelection(),
                content = editor.getContent(),
                text,
                pos;
            text = selection.length ? selection.text : A.Translator.t('markierten Text hier');
            if(selection.length && (
                    text.substr(0, 4) === '{== ' && text.substr(text.length -4) === ' ==}'
                )) {
                chunk = text.substr(4, text.length - 4);
                pos = selection.start + 4;
            } else if (selection.length && 
                (text.substr(0, 3) === '{== ' && text.substr(text.length -3) === '==}')) {
                    chunk = text.substr(4, text.length - 3);
                    pos = selection.start + 4;
            } else if(selection.length && content.substr(selection.start-4, 4) === '{== ' && 
                        content.substr(selection.end, 4) === ' ==}') {
                cursor.setSelection(selection.start-4, selection.end + 4);
                chunk = text;
                pos = selection.start - 4;
            } else if(selection.length && content.substr(selection.start-3, 3) === '{==' && 
                content.substr(selection.end, 3) === '==}') {
                cursor.setSelection(selection.start-3, selection.end + 3);
                chunk = text;
                pos = selection.start - 3;
            } else {
                chunk = sprintf(o.md, text);
                pos = selection.start + 4;
            }
            cursor.replaceSelection(chunk);
            cursor.setSelection(pos, pos + text.length);
        },


        _prepare: function () {
            var self = this,
                config = self.config,
                options;
            if (null !== config.plugins &&
                typeof config.plugins === 'object' &&
                typeof config.plugins.critic-mark === 'object') {
                options = Extend.flat({}, false, Plugin.DEFAULT_CONFIG, config.plugins.critic-mark);
            } else {
                options = Plugin.DEFAULT_CONFIG;
            }
            this.options = options;
        }


    };

    return Plugin;

});
