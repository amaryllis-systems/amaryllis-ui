/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @copyright 2013 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright 2014 - 2018 Amaryllis Systems GmbH http://www.amws.eu/
 * @copyright 2017 - 2018 Amaryllis CMS Project http://www.amaryllis-cms.de/
 * @license https://www.amaryllis-cms.de/lizenzen/
 * @license Apache Licence 2.0 https://www.apache.org/licenses/LICENSE-2.0
 *
 * Dual-Licensed with Amaryllis CMS EULA. You may obtain a copy of the
 * EULA at
 *
 *     https://www.amaryllis-cms.de/lizenzen/eula/
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */


/**
 * @param {tools/Utils}     Utils       Das Utils Modul
 * @param {core/Selectors}  Selectors   Selectors Modul
 * @param {core/Classes}    Classes     Classes Modul
 * @param {events/Event}    AcmsEvent   Event Modul
 */
define("editor/amaryllis/lib/plugins/code", [
    'tools/utils',
    'core/selectors',
    'core/classes',
    'events/event',
    'core/acms'
], function (Utils, Selectors, Classes, AcmsEvent, A) {

    "use strict";

    /**
     * code Plugin Constructor
     * 
     * @param {editor/amaryllis/lib/amaryllis} editor 
     * @param {Object} config 
     * 
     * @exports editor/amaryllis/lib/plugins/code
     */
    var Plugin = function (editor, config) {
        this.editor =
            this.config =
            this.button = null;
        this.initialize(editor, config);
    };

    /**
     * code Plugin Constructor
     * 
     * @param {editor/amaryllis/lib/amaryllis} editor 
     * @param {Object} config 
     * 
     * @returns editor/amaryllis/lib/plugins/code
     */
    Plugin.init = function (editor, config) {
        return new Plugin(editor, config);
    };

    /**
     * @var {Object}
     */
    Plugin.DEFAULT_CONFIG = {
        label: A.Translator._('Code Beispiel'),
        md: '```\n%s\n```\n',
        icon: 'acms-icon acms-icon-code'
    };

    Plugin.prototype = {

        constructor: Plugin,

        /**
         * Interner Constructor
         * 
         * @param {editor/amaryllis/lib/amaryllis} editor 
         * @param {Object} config
         */
        initialize: function (editor, config) {
            this.editor = editor;
            this.config = config;
            this._prepare();
        },

        /**
         * Name des Plugins
         * 
         * @returns {String}
         */
        getName: function () {
            return 'code';
        },

        /**
         * Label des Plugins
         * 
         * @returns {String}
         */
        getLabel: function () {
            return A.Translator._('Code Beispiel');
        },

        /**
         * Gibt die Methode/den Command des Plugins zurück
         * 
         * @returns 
         */
        getMethod: function () {
            return 'code';
        },

        /**
         * Gibt den Hot Key zurück
         * 
         * @returns {String}
         */
        getHotkey: function() {
            return 'ctrl+k';
        },

        /**
         * Event Callback
         * 
         * @param {Event} evt   Das Event
         */
        apply: function (evt) {
            var self = this,
                editor = self.editor,
                o = self.options,
                cursor = editor.getCursor(),
                selected = cursor.getSelection() || {
                    length: 0,
                    text: ''
                },
                text, pos, chunk, last, type, content = editor.getContent();
            last = content.substr(selected.start -1, 1);
            if (selected.length === 0) {
                text = A.Translator._('Code-Schnipsel hier');
            } else {
                text = selected.text;
            }
            if (content.substr(selected.start - 4, 4) === '```\n' &&
                content.substr(selected.end, 4) === '\n```') {
                cursor.setSelection(selected.start - 4, selected.end + 4);
                chunk = text;
                pos = selected.start - 4;
                type = "removed";
            } else if (content.substr(selected.start - 1, 1) === '`' &&
                content.substr(selected.end, 1) === '`') {
                cursor.setSelection(selected.start - 1, selected.end + 1);
                pos = selected.start - 1;
                chunk = text;
                type = "removed";
            } else if (content.substr(selected.start - 1, 1) === '\n' || selected.start === 0) {
                chunk = '``` php\n' + text + '\n```';
                pos = selected.start + 8;
                type = "block";
                if(selected.start !== 0 && content.substr(selected.start -2, 2) !== '\n\n') {
                    chunk = '\n' + chunk;
                    pos++;
                }
            } else {
                chunk = '`' + text + '`';
                pos = selected.start + 1;
                type = "inline";
            }
            
            cursor.replaceSelection(chunk);
            cursor.setSelection(pos, pos + text.length);
        },

        _prepare: function () {
            var self = this,
                config = self.config,
                options;
            if (null !== config.plugins &&
                typeof config.plugins === 'object' &&
                typeof config.plugins.code === 'object') {
                options = Utils.extend({}, Plugin.DEFAULT_CONFIG, config.plugins.code)
            } else {
                options = Plugin.DEFAULT_CONFIG;
            }
            this.options = options;
        }

    };

    return Plugin;

});
