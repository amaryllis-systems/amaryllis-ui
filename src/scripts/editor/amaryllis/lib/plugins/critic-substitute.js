/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("editor/amaryllis/lib/plugins/critic-substitute", [
    'tools/object/extend',
    'tools/string/sprintf',
    'core/selectors',
    'core/classes',
    'events/event',
    'text!templates/html/amaryllis-editor-critic-substitute.hbs!strip',
    'apps/ui/informations/modal',
    'http/module-loader',
    'core/acms'
], function (Extend, sprintf, Selectors, Classes, AcmsEvent, Template, Modal, Loader, A) {

    "use strict";

    var Plugin = function (editor, config) {
        this.editor =
            this.config = null;
        this.initialize(editor, config);
    };

    Plugin.init = function (editor, config) {
        return new Plugin(editor, config);
    };

    Plugin.DEFAULT_CONFIG = {
        md: '{~~ %s ~> %s ~~}',
        icon: 'acms-icon acms-icon-sync-alt',
        label: A.Translator._('ersetzen'),
        hotkey: 'ctrl+shift+r',
        template: Template
    };

    Plugin.TEMPLATE_CONFIG = {
        title: A.Translator._('Text ersetzen'),
        closeText: A.Translator._('Abbrechen'),
        submitText: A.Translator._('Übernehmen'),
        delText: A.Translator._('Alter Text'),
        insText: A.Translator._('Neuer Text'),
        commentText: A.Translator._('Kommentar'),
        selectedValue: false
    };

    Plugin.prototype = {

        constructor: Plugin,

        initialize: function (editor, config) {
            this.editor = editor;
            this.config = config;
            this._prepare();
        },

        /**
         * Name des Plugins
         * 
         * @returns {String}
         */
        getName: function () {
            return 'critic-substitute';
        },

        /**
         * Label des Plugins
         * 
         * @returns {String}
         */
        getLabel: function () {
            return this.options.label;
        },

        /**
         * Gibt die Methode/den Command des Plugins zurück
         * 
         * @returns 
         */
        getMethod: function () {
            return 'critic-substitute';
        },

        getHotkey: function() {
            return this.options.hotkey;
        },

        /**
         * Event Callback
         * 
         * @param {Event} evt   Das Event
         */
        apply: function (evt) {
            var self = this,
                editor = self.editor,
                o = self.options,
                cursor = editor.getCursor(),
                sel = cursor.getSelection() || {
                    text: '',
                    length: 0
                },
                chunk, text, pos, data = Plugin.TEMPLATE_CONFIG,
                content = editor.getContent();
            if (sel.length > 0) {
                data.selectedValue = sel.text;
            }
            var btn = editor.getContainer().querySelector('[data-action="critic-substitute"]');
            var cb = function (modal, button, ctx, tplData) {
                modal.show();
                self.modal = modal;
                self.button = button;
                self.ctx = ctx;
                self.tplData = tplData;
                self.box = Selectors.q('#' + tplData.id);
                Loader.load(self.box);
                Selectors.q('input', self.box).focus();
                self.saveButton = Selectors.q('#' + tplData.id + ' button[data-action="submit"]');
                AcmsEvent.add(self.saveButton, 'click', self._onSubmitModal);
                AcmsEvent.on(self.box, 'change', 'input', self._onChange);
                AcmsEvent.on(self.box, 'click', '[data-dismiss="modal"]', self._onDismiss);
            };
            Modal.createModal(o.template, data, btn, cb);
        },

        _onDismiss: function () {
            var self = this,
                id = self.tplData.id;
            self.modal.hide();
            var modal = self.box;
            AcmsEvent.remove(self.saveButton, 'click', self._onSubmitModal);
            AcmsEvent.off(modal, 'change', 'input', self._onChange);
            AcmsEvent.off(modal, 'click', '[data-dismiss="modal"]', self._onDismiss);
            setTimeout(function () {
                document.body.removeChild(modal);
                self.modal = self.ctx = self.tplData = null;
                if (self.button) {
                    self.button.acmsData(Modal.NS, null);
                }
            }, 1000);
        },

        _onChange: function (e) {
            var self = this,
                target = e.target,
                fBox = Selectors.closest(target, '.has-feedback'),
                form = Selectors.closest(target, 'form'),
                hint, p;
            
            if (target.type === 'text' && target.required && '' === target.value.trim()) {
                self.saveButton.disabled = true;
                self.saveButton.setAttribute('aria-disabled', 'true');
                if (fBox && !Selectors.q('.label-v2', fBox)) {
                    p = document.createElement('p');
                    Classes.addClass(fBox, 'invalid');
                    Classes.removeClass(fBox, 'valid');
                    fBox.appendChild(p);
                    Classes.addClass(p, 'top danger arrow label-v2');
                    p.innerHTML = A.Translator._('Das Feld ist erforderlich');
                }
                return;
            } else if (target.type === 'text' && target.required && '' !== target.value.trim()) {
                if (fBox) {
                    Classes.addClass(fBox, 'valid');
                    Classes.removeClass(fBox, 'invalid');
                    hint = Selectors.q('.label-v2', fBox);
                    if (hint) {
                        fBox.removeChild(hint);
                    }
                }
            }

            if (Selectors.qa('.invalid', form).length === 0) {
                self.saveButton.disabled = false;
                self.saveButton.setAttribute('aria-disabled', 'false');
            }
        },

        _onSubmitModal: function (e) {
            this.editor.getTextarea().focus();
            var self = this,
                t = e.target,
                form = Selectors.closest(t, 'form'),
                id = self.tplData.id,
                del = Selectors.q('input[name="del"]', form),
                ins = Selectors.q('input[name="ins"]', form),
                comment = Selectors.q('input[name="comment"]', form),
                o = self.options,
                cursor = self.editor.getCursor(),
                selection = cursor.getSelection(),
                content = self.editor.getContent(),
                last, div = document.createElement('div');
            div.textContent = del.value;
            var delVal = div.textContent;
            div.textContent = ins.value;
            var insVal = div.textContent;
            div.textContent = comment.value;
            var commentVal = div.textContent;
            if (t.disabled) {
                e.preventDefault();
                e.stopPropagation();
                return false;
            }
            var chunk = sprintf(o.md, delVal, insVal);
            if(commentVal.trim() !== '') {
                chunk += '{>> ' + commentVal + ' <<}';
            }
            if (selection.start !== 0 && (last = content.substr(selection.start - 1, 1)) !== ' ' && last !== '\n') {
                chunk = ' ' + chunk;
            }
            cursor.replaceSelection(chunk);
            cursor.setSelection(selection.end + chunk.length, selection.end + chunk.length);
            self.modal.hide();
            var modal = self.box;
            AcmsEvent.remove(self.saveButton, 'click', self._onSubmitModal);
            AcmsEvent.off(modal, 'change', 'input', self._onChange);
            setTimeout(function () {
                document.body.removeChild(modal);
                if (self.button) {
                    self.button.acmsData(Modal.NS, null);
                }
                self.modal = self.ctx = self.tplData = null;
            }, 1000);
            e.preventDefault();
            e.stopPropagation();
            return false;
        },

        _prepare: function () {
            var self = this,
                config = self.config,
                options;
            if (null !== config.plugins &&
                typeof config.plugins === 'object' &&
                typeof config.plugins.critic-substitute === 'object') {
                options = Extend.flat({}, false, Plugin.DEFAULT_CONFIG, config.plugins.critic-substitute);
            } else {
                options = Plugin.DEFAULT_CONFIG;
            }
            this.options = options;
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        }


    };

    return Plugin;

});
