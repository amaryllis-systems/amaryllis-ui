/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @copyright 2013 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright 2014 - 2018 Amaryllis Systems GmbH http://www.amws.eu/
 * @copyright 2017 - 2018 Amaryllis CMS Project http://www.amaryllis-cms.de/
 * @license https://www.amaryllis-cms.de/lizenzen/
 * @license Apache Licence 2.0 https://www.apache.org/licenses/LICENSE-2.0
 *
 * Dual-Licensed with Amaryllis CMS EULA. You may obtain a copy of the
 * EULA at
 *
 *     https://www.amaryllis-cms.de/lizenzen/eula/
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */


define("editor/amaryllis/lib/plugins/outdent", [
    'tools/utils',
    'core/selectors',
    'core/classes',
    'events/event',
    'core/acms'
], function (Utils, Selectors, Classes, AcmsEvent, A) {

    "use strict";

    var Plugin = function (editor, config) {
        this.editor =
            this.config = null;
        this.initialize(editor, config);
    };

    Plugin.init = function (editor, config) {
        return new Plugin(editor, config);
    };

    Plugin.DEFAULT_CONFIG = {
        md: tab,
        icon: 'acms-icon acms-icon-outdent',
        label: A.Translator._('einrücken'),
        hotkey: 'shift+tab'
    };

    var tab = '\t';

    Plugin.prototype = {

        constructor: Plugin,

        initialize: function (editor, config) {
            this.editor = editor;
            this.config = config;
            this._prepare();
        },

        /**
         * Name des Plugins
         * 
         * @returns {String}
         */
        getName: function () {
            return 'outdent';
        },

        /**
         * Label des Plugins
         * 
         * @returns {String}
         */
        getLabel: function () {
            return this.options.label;
        },

        /**
         * Gibt die Methode/den Command des Plugins zurück
         * 
         * @returns 
         */
        getMethod: function () {
            return 'outdent';
        },

        getHotkey: function() {
            return this.options.hotkey;
        },

        /**
         * Event Callback
         * 
         * @param {Event} evt   Das Event
         */
        apply: function (evt) {
            var self = this,
                editor = self.editor,
                o = self.options,
                chunk,
                cursor = editor.getCursor(),
                selection = cursor.getSelection(),
                content = editor.getContent(),
                text,
                pos;

            text = selection.text ? selection.text + '' : '';
            if(text.length && text.substr(0, tab.length) === tab) {
                chunk = text.substr(tab.length);
                pos = selection.start + 1;
            } else if(content.substr(selection.start-tab.length, tab.length) === tab) {
                cursor.setSelection(selection.start-tab.length, tab.length);
                chunk = '';
                pos = selection.start;
            } else {
                var last = cursor.getLastLineBreakPosition();
                if(last === -1) {
                    last = 0;
                }
                if(content.substr(last, tab.length) === tab) {
                    cursor.setSelection(last, tab.length);
                    chunk = content.substr(last, tab.length);
                }
            }
            
            cursor.replaceSelection(chunk);
            cursor.setSelection(pos, pos + text.length);
        },


        _prepare: function () {
            var self = this,
                config = self.config,
                options;
            if (null !== config.plugins &&
                typeof config.plugins === 'object' &&
                typeof config.plugins.outdent === 'object') {
                options = Utils.extend({}, Plugin.DEFAULT_CONFIG, config.plugins.outdent);
            } else {
                options = Plugin.DEFAULT_CONFIG;
            }
            this.options = options;
        }


    };

    return Plugin;

});
