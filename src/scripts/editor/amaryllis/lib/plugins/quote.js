/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("editor/amaryllis/lib/plugins/quote", [
    'tools/object/extend',
    'tools/string/sprintf',
    'events/event',
    'core/acms'
], function (Extend, sprintf, AcmsEvent, A) {

    "use strict";

    /**
     * quote Plugin Constructor
     * 
     * @param {editor/amaryllis/lib/amaryllis} editor 
     * @param {Object} config 
     * 
     * @exports editor/amaryllis/lib/plugins/quote
     */
    var Plugin = function (editor, config) {
        this.editor =
            this.config =
            this.button = null;
        this.initialize(editor, config);
    };

    /**
     * quote Plugin Constructor
     * 
     * @param {editor/amaryllis/lib/amaryllis} editor 
     * @param {Object} config 
     * 
     * @returns editor/amaryllis/lib/plugins/quote
     */
    Plugin.init = function (editor, config) {
        return new Plugin(editor, config);
    };

    /**
     * @var {Object}
     */
    Plugin.DEFAULT_CONFIG = {
        label: A.Translator._('Zitat'),
        md: '> %s',
        icon: 'acms-icon acms-icon-quote-right',
        hotkey: '', // 'ctrl+alt+q'
    };

    Plugin.prototype = {

        constructor: Plugin,

        /**
         * Interner Constructor
         * 
         * @param {editor/amaryllis/lib/amaryllis} editor 
         * @param {Object} config
         */
        initialize: function (editor, config) {
            this.editor = editor;
            this.config = config;
            this._prepare();
        },

        /**
         * Name des Plugins
         * 
         * @returns {String}
         */
        getName: function () {
            return 'quote';
        },

        /**
         * Label des Plugins
         * 
         * @returns {String}
         */
        getLabel: function () {
            return A.Translator._('Zitat');
        },

        /**
         * Gibt die Methode/den Command des Plugins zurück
         * 
         * @returns 
         */
        getMethod: function () {
            return 'quote';
        },

        /**
         * Gibt den Hot Key zurück
         * 
         * @returns {String}
         */
        getHotkey: function () {
            return this.options.hotkey;
        },

        getHelp: function() {
            return A.Translator._('Fügt ein Zitat-Markup ein.');
        },

        /**
         * On Render
         * 
         * Die Methode wird aufgerufen, wenn der Editor gerendert wurde.
         */
        onRender: function() {
            var self = this, tarea = self.editor.getTextarea();
            AcmsEvent.add(tarea, 'keyup', self._onKeyup);
        },

        /**
         * Event Callback
         * 
         * @param {Event} evt   Das Event
         */
        apply: function (evt) {
            var self = this,
                editor = self.editor,
                o = self.options,
                cursor = editor.getCursor(),
                selected = cursor.getSelection() || {
                    length: 0,
                    text: ''
                },
                text, pos, chunk, content = editor.getContent();
                var last;
            if (selected.length === 0) {
                text = A.Translator._('zitiere hier');
                chunk = sprintf(o.md, text);
                pos = selected.start + 2;
            } else {
                if (selected.text.indexOf('\n') < 0) {
                    text = selected.text;
                    chunk = sprintf(o.md, text);
                    pos = selected.start + 2;
                } else {
                    var list = [];
                    list = selected.text.split('\n');
                    text = list[0];
                    list.forEach(function (v, k) {
                        list[k] = sprintf(o.md, v);
                    });
                    chunk = '\n\n' + list.join('\n');
                    pos = selected.start + 4;
                }
            }
            last = content.substr(selected.start - 1);
            if(last !== '\n') {
                chunk = '\n' + chunk;
            }
            cursor.replaceSelection(chunk);
            cursor.setSelection(pos, pos + text.length);
        },


        /**
         * On Keyup Event Handler
         * 
         * @param {Event} e Das Event
         * 
         * @returns {void}
         */
        _onKeyup: function(e) {
            switch (parseInt(e.keyCode || e.which)) {
                case 13: // enter
                    var self = this,
                        cursor = self.editor.getCursor(),
                        content = self.editor.getContent(),
                        pos = cursor.getLastLineBreakPosition(),
                        char = content.substr(pos+1, 1), 
                        char2 = content.substr(pos+2, 1); //cursor.getFirstCharAfterLastLineBreak();
                    var selection = cursor.getSelection(), enterIndex = selection.start, o = self.options;
                    if (char === '>' && char2 === ' ') {
                        if(pos + 4 == enterIndex || pos + 3 == enterIndex) {
                            cursor.setSelection(pos, enterIndex);
                            cursor.replaceSelection('\n');
                            return; 
                        }
                        self.editor.insertContent(enterIndex, sprintf(o.md, '\n'));
                        cursor.setSelection(enterIndex + 2, enterIndex + 2);
                    }
                    break;
                default:
                    break;
            }
        },

        _prepare: function () {
            var self = this,
                config = self.config,
                options;
            if (null !== config.plugins &&
                typeof config.plugins === 'object' &&
                typeof config.plugins.quote === 'object') {
                options = Extend.flat({}, false, Plugin.DEFAULT_CONFIG, config.plugins.quote)
            } else {
                options = Plugin.DEFAULT_CONFIG;
            }
            this.options = options;
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        }

    };

    return Plugin;

});
