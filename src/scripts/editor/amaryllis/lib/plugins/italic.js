/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("editor/amaryllis/lib/plugins/italic", [
    'tools/object/extend',
    'tools/string/sprintf',
    'core/acms'
], function (Extend, sprintf, A) {

    "use strict";

    /**
     * italic Plugin Constructor
     * 
     * @param {editor/amaryllis/lib/amaryllis} editor 
     * @param {Object} config 
     * 
     * @exports editor/amaryllis/lib/plugins/italic
     */
    var Plugin = function (editor, config) {
        this.editor =
            this.config =
            this.button = null;
        this.initialize(editor, config);
    };

    /**
     * italic Plugin Constructor
     * 
     * @param {editor/amaryllis/lib/amaryllis} editor 
     * @param {Object} config 
     * 
     * @returns editor/amaryllis/lib/plugins/italic
     */
    Plugin.init = function (editor, config) {
        return new Plugin(editor, config);
    };

    /**
     * @var {Object}
     */
    Plugin.DEFAULT_CONFIG = {
        label: A.Translator._('kursiv'),
        md: '*%s*',
        icon: 'acms-icon acms-icon-italic'
    };

    Plugin.prototype = {

        constructor: Plugin,

        /**
         * Interner Constructor
         * 
         * @param {editor/amaryllis/lib/amaryllis} editor 
         * @param {Object} config
         */
        initialize: function (editor, config) {
            this.editor = editor;
            this.config = config;
            this._prepare();
        },

        /**
         * Name des Plugins
         * 
         * @returns {String}
         */
        getName: function () {
            return 'italic';
        },

        /**
         * Label des Plugins
         * 
         * @returns {String}
         */
        getLabel: function () {
            return A.Translator._('Text kursiv');
        },

        /**
         * Gibt die Methode/den Command des Plugins zurück
         * 
         * @returns  {String}
         */
        getMethod: function () {
            return 'italic';
        },

        /**
         * Gibt den Hot Key zurück
         * 
         * @returns {String}
         */
        getHotkey: function() {
            return 'ctrl+i';
        },

        /**
         * Event Callback
         * 
         * @param {Event} evt   Das Event
         */
        apply: function (evt) {
            var self = this,
                editor = self.editor,
                o = self.options,
                cursor = editor.getCursor(),
                sel = cursor.getSelection() || {
                    text: '',
                    length: 0
                },
                chunk, text, pos,
                content = editor.getContent();

            if (sel.length === 0) {
                text = A.Translator._('kursiver Text');
            } else {
                text = sel.text;
            }
            if((content.substr(sel.start - 3, 3) === ' **' &&
                    content.substr(sel.end, 2) === '**') ||
                    (content.substr(sel.start - 3, 3) !== ' __' &&
                    content.substr(sel.end, 2) !== '__')) {
                chunk = sprintf(o.md, text);
                pos = sel.start + 1;
            } else if (((content.substr(sel.start - 1, 1) === '*' &&
                    content.substr(sel.end, 1) === '*') ||
                (content.substr(sel.start - 1, 1) === '_' &&
                    content.substr(sel.end, 1) === '_'))
                ) {
                cursor.setSelection(sel.start - 1, sel.end + 1);
                chunk = text;
                pos = sel.start - 1;
            } else {
                chunk = sprintf(o.md, text);
                pos = sel.start + 1;
            }
            cursor.replaceSelection(chunk);
            cursor.setSelection(pos, pos + text.length);
        },


        _prepare: function () {
            var self = this,
                config = self.config,
                options;
            if (null !== config.plugins &&
                typeof config.plugins === 'object' &&
                typeof config.plugins.italic === 'object') {
                options = Extend.flat({}, false, Plugin.DEFAULT_CONFIG, config.plugins.italic);
            } else {
                options = Plugin.DEFAULT_CONFIG;
            }
            this.options = options;
        }


    };

    return Plugin;

});
