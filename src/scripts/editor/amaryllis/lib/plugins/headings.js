/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @copyright 2013 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright 2014 - 2018 Amaryllis Systems GmbH http://www.amws.eu/
 * @copyright 2017 - 2018 Amaryllis CMS Project http://www.amaryllis-cms.de/
 * @license https://www.amaryllis-cms.de/lizenzen/
 * @license Apache Licence 2.0 https://www.apache.org/licenses/LICENSE-2.0
 *
 * Dual-Licensed with Amaryllis CMS EULA. You may obtain a copy of the
 * EULA at
 *
 *     https://www.amaryllis-cms.de/lizenzen/eula/
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */


define("editor/amaryllis/lib/plugins/headings", [
    'tools/utils',
    'core/selectors',
    'core/classes',
    'events/event',
    'core/acms'
], function (Utils, Selectors, Classes, AcmsEvent, A) {

    "use strict";

    var Plugin = function (editor, config) {
        this.editor =
            this.config = null;
        this.initialize(editor, config);
    };

    Plugin.init = function (editor, config) {
        return new Plugin(editor, config);
    };

    Plugin.DEFAULT_CONFIG = {
        md: '###',
        icon: 'acms-icon acms-icon-heading',
        hotkey: 'ctrl+h'
    };

    Plugin.prototype = {

        constructor: Plugin,

        initialize: function (editor, config) {
            this.editor = editor;
            this.config = config;
            this._prepare();
        },

        /**
         * Name des Plugins
         * 
         * @returns {String}
         */
        getName: function () {
            return 'headings';
        },

        /**
         * Label des Plugins
         * 
         * @returns {String}
         */
        getLabel: function () {
            return A.Translator._('Überschriften');
        },

        /**
         * Gibt die Methode/den Command des Plugins zurück
         * 
         * @returns 
         */
        getMethod: function () {
            return 'headings';
        },

        /**
         * Gibt den Hotkey zurück
         * 
         * @returns {String}
         */
        getHotkey: function() {
            return this.options.hotkey;
        },

        /**
         * Event Callback
         * 
         * @param {Event} evt   Das Event
         */
        apply: function (evt) {
            var self = this,
                editor = self.editor,
                o = self.options,
                chunk,
                cursor = editor.getCursor(),
                selection = cursor.getSelection(),
                text,
                content = editor.getContent(),
                pointer, prevChar, pos, headers = 0, chars;
            if(selection.start === 0) {
                return this._addHeader(selection, cursor, content, length);
            }            
            var priorNewLineIndex = cursor.getLastLineBreakPosition(selection.start+1);
            if(priorNewLineIndex < 0) {
                priorNewLineIndex = 0;
            }
            var index = -1, len = 0, line = ''; chars = content.split('');
            for (var i = priorNewLineIndex; i < chars.length; i++) {
                len++;
                if (chars[i] === '\n') {
                    index = i;
                    break;
                }
                line += chars[i];
            }
            if(index === -1) {
                index = len;
            }

            if(index === 0 && priorNewLineIndex === 0) {
                
                return this._addHeader(selection, cursor, content);
            }
            cursor.setSelection(priorNewLineIndex, index);
            chars = line.split(''), length = 0;
            for(var j = 0; j < chars.length; j++) {
                if(chars[j] === '#') {
                    length++;
                } else {
                    break;
                }
            }
            if(length === 0) {
                return this._addHeader(selection, cursor, content);
            }
            if(length === 6) {
                if(content.substr(priorNewLineIndex-1, 1) === '\n') {
                    cursor.setSelection(priorNewLineIndex-1, index);
                }
                
                chunk = line.replace(/#/gi, '').trim();
                cursor.replaceSelection(chunk);
                return cursor.setSelection(chunk.length);
            }
            length++;

            line = '#'+line;
            cursor.setSelection(priorNewLineIndex, line.length);
            cursor.replaceSelection(line);
            cursor.setSelection(line.length + length, line.length);

        },

        _addHeader: function(selection, cursor, content) {
            var text, pos, chunk;
            if (selection.length === 0) {
                // Give extra word
                text = A.Translator._('Überschriften Text');
            } else {
                text = selection.text + '\n';
            }
            chunk = '# ' + text;
            pos = selection.start + 2;
            if(selection.start !== 0 && 
                    content.substr(selection.start - 1, 1) !== '\n' && 
                    content.substr(selection.start - 2, 2) !== '\n ') {
                chunk = '\n' + chunk;
                pos++;
            }
            cursor.replaceSelection(chunk);
            cursor.setSelection(pos, pos + text.length);
        },

        _prepare: function () {
            var self = this,
                config = self.config,
                options;
            if (null !== config.plugins &&
                typeof config.plugins === 'object' &&
                typeof config.plugins.headings === 'object') {
                options = Utils.extend({}, Plugin.DEFAULT_CONFIG, config.plugins.headings);
            } else {
                options = Plugin.DEFAULT_CONFIG;
            }
            this.options = options;
        }


    };

    return Plugin;

});