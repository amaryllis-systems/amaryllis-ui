/*!
 * Diese Datei ist Bestandteil von Amaryllis-Systems Software (http://www.amaryllis-systems.de)
 *
 * @copyright 2013 - 2018 QM-B <qm-b@amaryllis-systems.de>
 * @copyright 2014 - 2018 Amaryllis Systems GmbH http://www.amws.eu/
 * @copyright 2017 - 2018 Amaryllis CMS Project http://www.amaryllis-cms.de/
 * @license https://www.amaryllis-cms.de/lizenzen/
 * @license Apache Licence 2.0 https://www.apache.org/licenses/LICENSE-2.0
 *
 * Dual-Licensed with Amaryllis CMS EULA. You may obtain a copy of the
 * EULA at
 *
 *     https://www.amaryllis-cms.de/lizenzen/eula/
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

/**
 * @param {tools/Utils}     Utils       Das Utils Modul
 * @param {events/Event}    AcmsEvent   Event Modul
 * @param {Acms|Object}     A           Acms Global
 */
define("editor/amaryllis/lib/plugins/numlist", [
    "tools/utils",
    "events/event",
    "core/acms"
], function (Utils, AcmsEvent, A) {

    "use strict";

    /**
     * Bullet List Plugin
     *
     * @param {editor/amaryllis/lib/amaryllis} editor
     * @param {Object} config
     *
     * @exports editor/amaryllis/lib/plugins/numlist
     */
    var Plugin = function (editor, config) {
        this.editor = this.config = this.button = null;
        this.initialize(editor, config);
    };

    /**
     * numlist Plugin Constructor
     *
     * @param {editor/amaryllis/lib/amaryllis} editor
     * @param {Object} config
     *
     * @returns {Plugin}
     */
    Plugin.init = function (editor, config) {
        return new Plugin(editor, config);
    };

    /**
     * @var {Object}
     */
    Plugin.DEFAULT_CONFIG = {
        label: A.Translator._("Ungeordnete Liste"),
        md: "- %s",
        icon: 'acms-icon acms-icon-list-ol',
        hotkey: 'ctrl+o'
    };

    Plugin.prototype = {
        constructor: Plugin,

        /**
         * Interner Constructor
         *
         * @param {editor/amaryllis/lib/amaryllis} editor
         * @param {Object} config
         */
        initialize: function (editor, config) {
            this.editor = editor;
            this.config = config;
            this._prepare();
        },

        /**
         * Name des Plugins
         * 
         * @returns {String}
         */
        getName: function () {
            return "numlist";
        },

        /**
         * Label des Plugins
         * 
         * @returns {String}
         */
        getLabel: function () {
            return A.Translator._("geordnete Liste");
        },

        /**
         * Gibt die Methode/den Command des Plugins zurück
         * 
         * @returns {String}
         */
        getMethod: function () {
            return "numlist";
        },

        /**
         * Gibt den Hotkey zurück
         * 
         * @returns {String}
         */
        getHotkey: function() {
            return this.options.hotkey;
        },

        /**
         * On Render
         * 
         * Die Methode wird aufgerufen, wenn der Editor gerendert wurde.
         */
        onRender: function () {
            var self = this,
                tarea = self.editor.getTextarea();
            AcmsEvent.add(tarea, 'keyup', self._onKeyup);
        },

        /**
         * Event Callback
         *
         * @param {Event} evt   Das Event
         */
        apply: function (evt) {
            var self = this,
                o = self.options,
                editor = self.editor,
                cursor = editor.getCursor(),
                sel = cursor.getSelection() || {length: 0},
                chunk,
                text,
                pos;
            
            pos = sel.start + 3;
            if (sel.length === 0) {
              text = A.Translator._('List-Text hier');
              chunk = '1. ' + text;
            } else {
              if (sel.text.indexOf('\n') < 0) {
                text = sel.text;
                chunk = '1. ' + text;
              } else {
                var i = 1;
                var list = [];
                list = sel.text.split('\n');
                text = list[0];
                list.forEach(function (v, k) {
                    list[k] = i + '. ' + v;
                    i++;
                });
                chunk = '\n\n' + list.join('\n');
                pos = sel.start + 5;
              }
            }
            cursor.replaceSelection(chunk);
            cursor.setSelection(pos, pos + text.length);
        },

        /**
         * On Keyup Event Handler
         * 
         * @param {Event} e Das Event
         * 
         * @returns {void}
         */
        _onKeyup: function (e) {
            switch (parseInt(e.keyCode)) {
                case 13: // enter
                    var cursor = this.editor.getCursor();
                    var pos = cursor.getLastLineBreakPosition();
                    var char = cursor.getFirstCharAfterLastLineBreak();
                    var selection = cursor.getSelection(),
                        enterIndex = selection.start;
                    if (Utils.isNumeric(char)) {
                        var numBullet = this._getBulletNumber(pos + 1);
                        if(!numBullet) return;
                        //console.log('nl pos', pos);
                        //console.log('nl numbullet', numBullet.toString().length);
                        //console.log('nl enter', enterIndex);
                        if ((pos + 4 + numBullet.toString().length) == enterIndex) {
                            cursor.setSelection(pos, enterIndex);
                            cursor.replaceSelection('\n');
                            return;
                        }
                        this._addNumberedBullet(enterIndex, numBullet);
                    }
                    break;
                default:
                    break;
            }
        },

        _addNumberedBullet: function (index, num) {
            var numb = num + 1;
            var numBullet = numb + '. \n',
                cursor;
            this.editor.insertContent(index, numBullet);
            cursor = this.editor.getCursor();
            var prefixLength = num.toString().length + 2,
                start = index + prefixLength;
            cursor.setSelection(start, start); // move the Cursor to the end of the line
            this._checkFollowingNumbers(numb, start);
            cursor.setSelection(start, start); // reset the cursor
        },
        
        _getBulletNumber: function (startIndex) {
            var bulletNum = this.editor.getContent().slice(startIndex).split('.')[0];

            return Utils.isNumeric(bulletNum) ? parseInt(bulletNum) : null;
        },

        _checkFollowingNumbers: function(numb, index) {
            var newLine, char, blank, end, text, cursor;
            cursor = this.editor.getCursor();
            newLine = cursor.getNextCharPosition(index, '\n');
            if(newLine < 0) {
                return;
            }
            var content = this.editor.getContent();
            var chunk = content.substr(newLine +1);
            cursor.setSelection(newLine +1, content.length);
            var list = chunk.split('\n');
            for(var k = 0; k < list.length; k++) {
                var v = list[k];
                if(v.trim() === "") {
                    break;
                }
                char = v.split('.')[0];
                var numBullet = Utils.isNumeric(char) ? parseInt(char) : null;
                if(null === numBullet) {
                    break;
                }
                numb++;
                var len = numBullet.toString().length;
                var sub = v.substr(len);
                list[k] = numb.toString() + sub;
            }
            cursor.replaceSelection(list.join('\n'));
            
        },

        /**
         * Vorbereiten
         */
        _prepare: function () {
            var self = this,
                config = self.config,
                options;
            if (null !== config.plugins &&
                typeof config.plugins === 'object' &&
                typeof config.plugins.numlist === 'object') {
                options = Utils.extend({}, Plugin.DEFAULT_CONFIG, config.plugins.numlist);
            } else {
                options = Plugin.DEFAULT_CONFIG;
            }
            this.options = options;
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        }

    };

    return Plugin;
});