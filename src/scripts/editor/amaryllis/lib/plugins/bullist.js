/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("editor/amaryllis/lib/plugins/bullist", [
    'tools/object/extend',
    'tools/string/sprintf',
    "events/event",
    "core/acms"
], function(Extend, sprintf, AcmsEvent, A) {
    
    "use strict";

    /**
     * Bullet List Plugin
     *
     * @param {editor/amaryllis/lib/amaryllis} editor
     * @param {Object} config
     *
     * @exports editor/amaryllis/lib/plugins/bullist
     */
    var Plugin = function(editor, config) {
        this.editor = this.config = this.button = null;
        this.initialize(editor, config);
    };

    /**
     * bullist Plugin Constructor
     *
     * @param {editor/amaryllis/lib/amaryllis} editor
     * @param {Object} config
     *
     * @returns {Plugin}
     */
    Plugin.init = function(editor, config) {
        return new Plugin(editor, config);
    };

    /**
     * @var {Object}
     */
    Plugin.DEFAULT_CONFIG = {
        label: A.Translator._("Ungeordnete Liste"),
        md: "- %s",
        icon: 'acms-icon acms-icon-list-ul',
        hotkey: 'ctrl+u'
    };

    Plugin.prototype = {
        constructor: Plugin,

        /**
         * Interner Constructor
         *
         * @param {editor/amaryllis/lib/amaryllis} editor
         * @param {Object} config
         */
        initialize: function(editor, config) {
            this.editor = editor;
            this.config = config;
            this._prepare();
        },

        /**
         * Name des Plugins
         * 
         * @returns {String}
         */
        getName: function() {
            return "bullist";
        },

        /**
         * Label des Plugins
         * 
         * @returns {String}
         */
        getLabel: function() {
            return A.Translator._("Ungeordnete Liste");
        },

        /**
         * Gibt die Methode/den Command des Plugins zurück
         * 
         * @returns 
         */
        getMethod: function() {
            return "bullist";
        },

        /**
         * Gibt den Hotkey zurück
         * 
         * @returns {String}
         */
        getHotkey: function() {
            return this.options.hotkey;
        },

        /**
         * On Render
         * 
         * Die Methode wird aufgerufen, wenn der Editor gerendert wurde.
         */
        onRender: function() {
            var self = this, tarea = self.editor.getTextarea();
            AcmsEvent.add(tarea, 'keyup', self._onKeyup);
        },

        /**
         * Event Callback
         *
         * @param {Event} evt   Das Event
         */
        apply: function(evt) {
            var self = this,
                o = self.options,
                editor = self.editor,
                cursor = editor.getCursor(),
                sel = cursor.getSelection() || {length: 0},
                chunk,
                text,
                pos, last, content = editor.getContent();
            pos = sel.start + 2;
            last = content.substr(sel.start -1, 1);
            if (sel.length === 0) {
                text = A.Translator._("List-Text hier");
                chunk = sprintf(o.md, text);
                if(last !== '\n') {
                    chunk = '\n\n' + chunk;
                    pos += 2;
                }
            } else {
                if (sel.text.indexOf("\n") < 0) {
                    text = sel.text;
                    chunk = sprintf(o.md, sel.text);
                    if(last !== '\n') {
                        chunk = '\n\n' + chunk;
                        pos += 2;
                    }
                } else {
                    var list = [], v, k;
                    list = sel.text.split("\n");
                    text = list[0];
                    for(k = 0; k < list.length; k++) {
                        v = list[k] || '';
                        if(v.trim() === "") {
                            continue;
                        }
                        var char = v.charAt(0);
                        if((char === '-' || char === '*') && v.charAt(1) === ' ') {
                            list[k] = v.substring(2);
                        } else {
                            list[k] = sprintf(o.md, v);
                        }
                    }
                    chunk = list.join("\n");
                    pos = sel.start + 4;
                    if(last !== '\n') {
                        chunk = '\n\n' + chunk;
                        pos += 2;
                    }
                }
            }
            
            cursor.replaceSelection(chunk);
            cursor.setSelection(pos, pos + text.length);
        },

        /**
         * On Keyup Event Handler
         * 
         * @param {Event} e Das Event
         * 
         * @returns {void}
         */
        _onKeyup: function(e) {
            switch (parseInt(e.keyCode)) {
                case 13: // enter
                    var cursor = this.editor.getCursor();
                    var content = this.editor.getContent();
                    var pos = cursor.getLastLineBreakPosition();
                    var char = content.substr(pos+1, 1), char2 = content.substr(pos+2, 1) //cursor.getFirstCharAfterLastLineBreak();
                    var selection = cursor.getSelection(), enterIndex = selection.start;
                    if ((char === '*' || char === '-') && char2 === ' ') {
                        if(pos + 4 == enterIndex || pos + 3 == enterIndex) {
                            cursor.setSelection(pos, enterIndex);
                            cursor.replaceSelection('\n');
                            return; 
                        }
                        
                        this._addBullet(enterIndex, cursor);
                    }
                    break;
                default:
                    break;
            }
        },

        /**
         * Fügt ein List-Item hinzu
         * 
         * @param {Number} index Index, an dem das Item erstellt werden soll
         * @param {editor/amaryllis/lib/cursor} cursor  Der Cursor
         */
        _addBullet: function(index, cursor) {
            var self = this, o = self.options;
            self.editor.insertContent(index, sprintf(o.md, '\n'));
            cursor.setSelection(index + 2, index + 2); // Put the cursor after the bullet
        },
        
        /**
         * Vorbereiten
         */
        _prepare: function() {
            var self = this, config = self.config, options;
            if(null !== config.plugins && 
                    typeof config.plugins === 'object' && 
                    typeof config.plugins.bullist === 'object') {
                options = Extend.flat({}, false, Plugin.DEFAULT_CONFIG, config.plugins.bullist);
            } else {
                options = Plugin.DEFAULT_CONFIG;
            }
            this.options = options;
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        }
        
    };

    return Plugin;

});
