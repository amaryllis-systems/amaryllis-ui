/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("editor/amaryllis/lib/plugins/bold", [
    'tools/object/extend',
    'tools/string/sprintf',
    'core/selectors',
    'core/classes',
    'events/event',
    'core/acms'
], function (Extend, sprintf, Selectors, Classes, AcmsEvent, A) {

    "use strict";

    /**
     * bold Plugin Constructor
     * 
     * @param {editor/amaryllis/lib/amaryllis} editor 
     * @param {Object} config 
     * 
     * @exports editor/amaryllis/lib/plugins/bold
     */
    var Plugin = function (editor, config) {
        this.editor =
            this.config =
            this.button = null;
        this.initialize(editor, config);
    };

    /**
     * bold Plugin Constructor
     * 
     * @param {editor/amaryllis/lib/amaryllis} editor 
     * @param {Object} config 
     * 
     * @returns editor/amaryllis/lib/plugins/bold
     */
    Plugin.init = function (editor, config) {
        return new Plugin(editor, config);
    };

    /**
     * @var {Object}
     */
    Plugin.DEFAULT_CONFIG = {
        label: A.Translator._('Fett'),
        md: '**%s**',
        icon: 'acms-icon acms-icon-bold'
    };

    Plugin.prototype = {

        constructor: Plugin,

        /**
         * Interner Constructor
         * 
         * @param {editor/amaryllis/lib/amaryllis} editor 
         * @param {Object} config 
         */
        initialize: function (editor, config) {
            this.editor = editor;
            this.config = config;
            this._prepare();
        },

        /**
         * Name des Plugins
         * 
         * @returns {String}
         */
        getName: function () {
            return 'bold';
        },

        /**
         * Label des Plugins
         * 
         * @returns {String}
         */
        getLabel: function () {
            return this.options.label;
        },

        /**
         * Gibt die Methode/den Command des Plugins zurück
         * 
         * @returns 
         */
        getMethod: function () {
            return 'bold';
        },

        /**
         * Gibt den Hot Key zurück
         * 
         * @returns {String}
         */
        getHotkey: function() {
            return 'ctrl+b';
        },

        /**
         * Event Callback
         * 
         * @param {Event} evt   Das Event
         */
        apply: function (evt) {
            var self = this,
                editor = self.editor,
                o = self.options,
                cursor = editor.getCursor(),
                sel = cursor.getSelection() || {
                    text: '',
                    length: 0
                },
                chunk, text, pos,
                content = editor.getContent();

            if (sel.length === 0) {

                text = A.Translator._('fetter Text');
            } else {
                text = sel.text;
            }
            if ((content.substr(sel.start - 2, 2) === '**' &&
                    content.substr(sel.end, 2) === '**') ||
                (content.substr(sel.start - 2, 2) === '__' &&
                    content.substr(sel.end, 2) === '__')) {
                cursor.setSelection(sel.start - 2, sel.end + 2);
                chunk = text;
                pos = sel.start - 2;
            } else {
                chunk = sprintf(o.md, text);
                pos = sel.start + 2;
            }
            cursor.replaceSelection(chunk);
            cursor.setSelection(pos, pos + text.length);
        },

        _prepare: function () {
            var self = this,
                config = self.config,
                options;
            if (config.plugins &&
                typeof config.plugins === 'object' &&
                typeof config.plugins.bold === 'object') {
                options = Extend.flat({}, false, Plugin.DEFAULT_CONFIG, config.plugins.bold);
            } else {
                options = Plugin.DEFAULT_CONFIG;
            }
            this.options = options;
            for (var fn in self) {
                if (fn.charAt(0) === '_' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            }
        }

    };

    return Plugin;

});
