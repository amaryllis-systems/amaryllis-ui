/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("editor/tinymce/tinymce", [
    "core/base",
    "tools/string/unique-id",
    "core/selectors",
    "events/event",
    "tinyMCE",
    "core/acms"
], (Base, UniqueId, Selectors, AcmsEvent, tinyMCE, A) => {


    /**
     * TinyMCE Implementation
     *
     * @class Tiny
     * @extends {Base}
     */
    class Tiny extends Base {

        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof Tiny
         */
        static get MODULE() {
            return "TinyMCE Plugin";
        }

        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof Tiny
         */
        static get NS() {
            return "editor.tinymce.tinymce";
        }

        /**
         * Default Options
         *
         * @readonly
         * @static
         * @memberof Tiny
         */
        static get DEFAULT_OPTIONS() {
            return {
                mode: Tiny.MODE_PUBLIC,
                theme: 'silver',
                skin: 'dark'
            };
        }
            
        /**
         * Mode Public
         * 
         * @type {Number}   
         */
        static get MODE_PUBLIC() {
            return 1;
        }

        /**
         * Mode Basic
         * 
         * @type {Number}   
         */
        static get MODE_BASIC() {
            return 2;
        }

        /**
         * Mode Extended
         * 
         * @type {Number}   
         */
        static get MODE_EXTENDED() {
            return 3;
        }

        /**
         * Mode Advanced
         * 
         * @type {Number}   
         */
        static get MODE_ADVANCED() {
            return 4;
        }

        /**
         * Mode Export
         * 
         * @type {Number}   
         */
        static get MODE_EXPERT() {
            return 5;
        }

        /**
         * Module Init
         *
         * @static
         * 
         * @param {HTMLTextAreaElement} element     Textarea Element
         * @param {Object}              options     Optional Custom Options
         * 
         * @returns {Tiny} new instance
         * 
         * @memberof Tiny
         */
        static init(element, options) {
            let inst = new Tiny(element, options);
            inst.element.acmsData(Tiny.NS, inst);

            return inst;
        }

        /**
         * Creates an instance of Tiny.
         * 
         * @param {HTMLTextAreaElement} element     Textarea Element
         * @param {Object}              options     Optional Custom Options
         * 
         * @memberof Tiny
         */
        constructor(element, options) {
            super(element, options);
            this._initialize();
        }

        /**
         * Setup Event Listener
         *
         * @memberof Tiny
         */
        listen() {
            let self = this, 
                element = self.element,
                form = Selectors.closest(element, 'form');
            AcmsEvent.add(form, 'acms.before-save-form', function(e) {
                tinyMCE.triggerSave();
                let value = tinyMCE.get(element.getAttribute('id')).save();
                self.element.value = value;
            });
        }

        /**
         * Prepare Module
         * 
         * @access private
         *
         * @memberof Tiny
         */
        _prepare() {
            let self = this, 
                element = self.element,
                o = self.options,
                dataHeight = element.acmsData('height') || 300;
            if(!element.getAttribute('id')) {
                element.setAttribute('id', UniqueId());
            }
            let config = {
                    theme: o.theme,
                    skin: o.skin,
                    height: dataHeight,
                    selector: '#' + element.getAttribute('id'),
                    id: element.getAttribute('id')
                },
                thisMode, mod
            ;
            tinyMCE.baseURL = A.Urls.baseUrl + "/media/scripts/libs/editors/tinymce";
            tinyMCE.suffix = '.min';
            thisMode = parseInt(o.mode) || Tiny.MODE_PUBLIC;
            A.Logger.writeLog(thisMode + ' Editor-Modus');
            switch(thisMode) {
                case Tiny.MODE_EXPERT:
                    mod = "editor/tinymce/modes/expert";
                    break;
                case Tiny.MODE_ADVANCED:
                    mod = 'editor/tinymce/modes/advanced';
                    break;
                case Tiny.MODE_EXTENDED:
                    mod = 'editor/tinymce/modes/extended';
                    break;
                case Tiny.MODE_BASIC:
                    mod = 'editor/tinymce/modes/basic';
                    break;
                case Tiny.MODE_PUBLIC:
                default:
                    mod = "editor/tinymce/modes/public";
                    break;
            }
            require([mod], (Mode) => {
                let tinyMode = new Mode(config),
                    tinyConfig = tinyMode.getMode();
                A.Logger.writeLog('Loaded TinyMCE Mode ' + Mode.name);
                tinyConfig.setup = function(ed) {
                    ed.on("blur", function(){
                        self.element.value = tinyMCE.activeEditor.getContent();
                        AcmsEvent.fireChange(self.element);
                    });
                };
                tinyMCE.init(tinyConfig);
                
            });
        }

    }

    return Tiny;
});
