/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * TinyMCE Mode Basic
 *
 * @returns {Mode}
 */
define('editor/tinymce/modes/basic', [], () => {

    /**
     * Basic TinyMCE Mode
     *
     * @class BasicMode
     * 
     * @property {String} name      Gets the Mode name
     * @property {Object} config    Gets the Mode Configuration 
     */
    class BasicMode {

        /**
         * Creates an instance of BasicMode.
         * 
         * @param {Object} config
         * 
         * @memberof BasicMode
         */
        constructor(config) {
            this.config = this._make(config);
        }

        get name() {
            return "Extended Mode";
        }
        
        get config() {
            return this._config;
        }

        set config(config) {
            if(!config || typeof config !== 'object') {
                throw new Error("invalid Config");
            }
            this._config = config;
        }

        /**
         * Gets the Mode Configuration
         *
         * @returns
         * @memberof BasicMode
         */
        getMode() {
            return this.config;
        }
        
        _make(config) {
            return {
                mode: config.selector ? "exact" : 'textareas',
                selector: config.selector ? "textarea" + config.selector : 'textarea[data-editor="tinymce"],textarea[data-editor="Tinymce"], textarea.editor-tinymce, textarea.editor-Tinymce',
                theme: config.theme || 'silver',
                theme_url: '/media/scripts/libs/editors/tinymce/themes/silver/theme.min.js',
                height: config.height || 300,
                plugins: [
                ],
                language_url : Acms.Urls.translationsUrl + Acms.Locale.locale + "/tinymce.js",
                toolbar1: 'bold italic bullist numlist link',
                menubar : false,
                convert_urls : 0,
                remove_script_host : 0,
                verify_html: false,
                branding: false
            };
        }

    }

    return BasicMode;

});
