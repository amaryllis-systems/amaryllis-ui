/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define('editor/tinymce/modes/expert', [
    'libs/prism/prism'
], () => {

    /**
     * Expert TinyMCE Mode
     *
     * @class ExpertMode
     * 
     * @property {String} name      Gets the Mode name
     * @property {Object} config    Gets the Mode Configuration 
     */
    class ExpertMode {

        /**
         * Creates an instance of ExpertMode.
         * 
         * @param {Object} config
         * 
         * @memberof ExpertMode
         */
        constructor(config) {
            this.config = this._make(config);
        }

        get name() {
            return "Extended Mode";
        }
        
        get config() {
            return this._config;
        }

        set config(config) {
            if(!config || typeof config !== 'object') {
                throw new Error("invalid Config");
            }
            this._config = config;
        }

        /**
         * Gets the Mode Configuration
         *
         * @returns
         * @memberof ExpertMode
         */
        getMode() {
            return this.config;
        }
        
        _make(config) {
            return {
                mode: config.selector ? "exact" : 'textareas',
                selector: config.selector || 'textarea[data-editor="tinymce"],textarea[data-editor="Tinymce"], textarea.editor-tinymce, textarea.editor-Tinymce',
                theme: config.theme || 'silver',
                theme_url: '/media/scripts/libs/editors/tinymce/themes/silver/theme.min.js',
                height: config.height || 300,
                plugins: [
                    'advlist link image lists hr anchor pagebreak spellchecker',
                    'code fullscreen insertdatetime',
                    'paste codesample'
                ],
                language_url : Acms.Urls.translationsUrl + Acms.Locale.locale + "/tinymce.js",
                toolbar1: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | link image codesample',
                toolbar2: 'pagebreak pastetext image link code | advlist bullist numlist outdent indent',
                menubar : false,
                convert_urls : 0,
                remove_script_host : 0,
                verify_html: false,
                branding: false
            };
        }

    }

    return ExpertMode;

});
