/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('tools/filter-html', [
    'tools/utils',
    'core/selectors',
    'core/acms'
], function(Utils, Selectors, A) {

    "use strict";


    var isString = function(data) {
        return typeof data === 'string';
    };

    var isArray = function(value) {
        return Object.prototype.toString.call(value) === "[object Array]";
    };

    var isObject = function(value) {
        return !isArray(value) && value instanceof Object;
    };

    var isNumber = function(value) {
        return typeof value === 'number';
    };

    var isBoolean = function(value) {
        return typeof value === 'boolean';
    };

    var charForLoopStrategy = function(unescapedString) {
        var i, character, escapedString = '';

        for (i = 0; i < unescapedString.length; i += 1) {
            character = unescapedString.charAt(i);
            switch (character) {
                case '<':
                    escapedString += '&lt;';
                    break;
                case '>':
                    escapedString += '&gt;';
                    break;
                case '&':
                    escapedString += '&amp;';
                    break;
                case '/':
                    escapedString += '&#x2F;';
                    break;
                case '"':
                    escapedString += '&quot;';
                    break;
                case "'":
                    escapedString += '&#x27;';
                    break;
                default:
                    escapedString += character;
            }
        }

        return escapedString;
    };

    var regexStrategy = function(string) {
        return string
            .replace(/&/g, '&amp;')
            .replace(/</g, '&lt;')
            .replace(/>/g, '&gt;')
            .replace(/"/g, '&quot;')
            .replace(/'/g, "&#x27;")
            .replace(/\//g, '&#x2F;');
    };

    var shiftToRegexStrategyThreshold = 32;

    var xssEscape = function(data, forceStrategy) {
        var escapedData, character, key, i, charArray = [],
            stringLength;

        if (isString(data)) {
            stringLength = data.length;
            if (forceStrategy === 'charForLoopStrategy') {
                escapedData = charForLoopStrategy(data);
            } else if (forceStrategy === 'regexStrategy') {
                escapedData = regexStrategy(data);
            } else if (stringLength > shiftToRegexStrategyThreshold) {
                escapedData = regexStrategy(data);
            } else {
                escapedData = charForLoopStrategy(data);
            }
        } else if (isNumber(data) || isBoolean(data)) {
            escapedData = data;
        } else if (isArray(data)) {
            escapedData = [];
            for (i = 0; i < data.length; i += 1) {
                escapedData.push(xssEscape(data[i]));
            }
        } else if (isObject(data)) {
            escapedData = {};
            for (key in data) {
                if (data.hasOwnProperty(key)) {
                    escapedData[key] = xssEscape(data[key]);
                }
            }
        }

        return escapedData;
    };

    /**
     * Constructor
     * 
     * @param {Element}     element     Das HTML-Element
     * @param {Object}      options     Die Optionen
     * @returns {Filter}
     * 
     * @exports tools/filter-html
     */
    var Filter = {

        ecape: function(input) {
            return xssEscape(input);
        },

        filter: function(input, options) {
            var o = Utils.extend(Filter.DEFAULT_OPTIONS, options);
            var div = document.createElement('div');
            div.innerHtml = input;
            var scripts = div.getElementsByTagName('script');
            var l = scripts.length;
            while (l--) {
                scripts[l].parentNode.removeChild(scripts[l]);
            }
            var all = Selectors.qa('*', div),
                length = all.length,
                i, j, attr, ele;
            for (i = 0; i < length; i++) {
                ele = all[i];
                attr = ele.attributes;
                for (j = 0; j < attr.length; j++) {

                    if (attr[j].name.indexOf('on') === 0) {
                        attr[j].value = null;
                    }
                    if (o.allowData !== true) {
                        if (attr[j].name.indexOf('data') === 0) {
                            attr[j].value = null;
                        }
                    }
                }
            }
            var filtered = div.innerHtml;
            filtered.replace(Filter.REGEX_SCRIPT, '');
            if (o.escape === false || o.escape === 'false') {
                return filtered;
            }

            return this.ecape(div.innerHtml, 'charForLoopStrategy');
        }
    };

    /**
     * Modul Name
     * 
     * @var {String}
     */
    Filter.MODULE = "HTML Filter";

    /**
     * Modul Version
     * 
     * @var {String}
     */
    Filter.VERSION = "1.5.0";

    /**
     * Modul Namespace
     * 
     * @var {String}
     */
    Filter.NS = "acms.tools.filter-html";

    /**
     * Standard-Optionen des Moduls
     * 
     * @var {Object}
     */
    Filter.DEFAULT_OPTIONS = {
        allowData: false,
        escape: true
    };

    Filter.REGEX_SCRIPT = /<script\b[^<]*(?:(?!<\/script>)<[^<]*)*<\/script>/gi;


    return Filter;

});
