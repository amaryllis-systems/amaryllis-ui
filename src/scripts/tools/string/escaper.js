/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 
 define("tools/string/escaper", [
 ], () => {
 
	 /**
      * Escaper
      *
      * @class Escaper
      * @author qm-b <https://bitbucket.org/qm-b/>
      */
     class Escaper {
 
        /**
         * Liste an HTML Entities für Escaping.
         * 
         * @readonly
         * @memberof Escaper
         */
        static get escapeMap() {
            return {
                '&': '&amp;',
                '<': '&lt;',
                '>': '&gt;',
                '"': '&quot;',
                "'": '&#x27;',
                '`': '&#x60;'
            };
        }

        /**
         * Invertierte Liste an HTML Entities für unescaping.
         * 
         * @readonly
         * @memberof Escaper
         */
        static get unescapeMap() {
            return {
                '&amp;': '&',
                '&lt;': '<',
                '&gt;': '>',
                '&quot;': '"',
                '&#x27;': "'",
                '&#x60;': '`'
            };
        }

        constructor() {
        }

        /**
         * Create Escaper
         *
         * @readonly
         * @memberof Escaper
         */
        get escaper() {
            return this._createEscaper(Escaper.escapeMap);
        }

        /**
         * Create unescaper
         *
         * @readonly
         * @memberof Escaper
         */
        get unescaper() {
            return this._createEscaper(Escaper.unescapeMap);
        }

        /**
         * escaping string
         * 
         * @param {String}  str to Escape
         * 
         * @returns {String}    escaped string
         * 
         * @memberof Escaper
         */
        escape(str) {
            let cb = this.escaper;

            return cb(str);
        }

        /**
         * unescaping string
         * 
         * @param {String}  str to Unescape
         * 
         * @returns {String}    unescaped string
         * 
         * @memberof Escaper
         */
        unescape(str) {
            let cb = this.unescaper;
            return cb(str);
        }

        
        /**
         * Erstellt den (Un-)Escaper
         * 
         * @param {Object} map  Die jeweilige Map zum Escapen oder unescape
         * 
         * @returns {Function}  Einen Escaper Callback
         * 
         * @memberof Escaper
         * @private
         */
        _createEscaper(map) {
            let cb = (match) => {
                return map[match];
            };
            let keys = [],
                key;
            for (key in map) {
                if (map.hasOwnProperty(key)) {
                    keys.push(key);
                }
            }
            let source = '(?:' + keys.join('|') + ')',
                testRegexp = RegExp(source),
                replaceRegexp = RegExp(source, 'g');
            return function(string) {
                string = (string === null) ?
                    '' :
                    '' + string;
                return testRegexp.test(string) ? string.replace(replaceRegexp, cb) : string;
            };
        }


	 }
 
	 return Escaper;
 });
