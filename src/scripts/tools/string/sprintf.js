/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("tools/string/sprintf", [], () => {

    /**
     * sprintf for js
     * 
     * @example
     * // prints 'Hello world'
     * sprintf('Hello %s', 'world');
     * 
     *
     * @param {String} str string to handle
     * @param {...String} args arguments to print in
     * 
     * @returns {String}
     * 
     * @exports tools/string/sprintf
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    const sprintf = (str, ...args) => {
        let strng,
            flag = true,
            i = 0;
        strng = str.replace(/%s/g, () => {
            let arg = args[i++];

            if (typeof arg === 'undefined') {
                flag = false;
                return '';
            }
            return arg;
        });

        if (flag) {
            return strng;
        }
        return '';
    };
    
    return sprintf;
});
