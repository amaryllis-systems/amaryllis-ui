/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("tools/string/unique-id", [], () => {

	/**
	 * Generates a unique id in current document
	 * 
	 * @param {String} prefix optional prefix for the string
	 *
	 * @returns {String}
	 * @author qm-b <https://bitbucket.org/qm-b/>
	 * @exports tools/string/UniqueId
	 */
	const UniqueId = (prefix) => {
		let rand = Math.round(new Date().getTime() + (Math.random() * 100)),
			pref = prefix || "un-" + rand;
		do {
			pref += ~~(rand * 10000);
		} while (document.getElementById(pref));

		return pref;
	};

	return UniqueId;

});
