/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("tools/string/chop", [], () => {

    /**
     * Remove leading and trailing whitespace and non-word
     * characters from the given string.
     *
     * @param {String} str Strign to chop
     * 
     * @return {String}
     * 
     * @exports tools/string/Chop
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    const Chop = (str) => {
        if (typeof str !== 'string') return '';
        let re = /^[-_.\W\s]+|[-_.\W\s]+$/g;

        return str.trim().replace(re, '');
    };

    return Chop;
});
