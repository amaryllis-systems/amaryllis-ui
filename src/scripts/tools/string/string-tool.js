/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("tools/string/string-tool", [
    "tools/string/camelcase",
], (camelcase) => {

    /**
     * String Tool
     * 
     * Helper Class with several small string utility functions
     *
     * @class
     * @author qm-b <https://bitbucket.org/qm-b/>
     * @since 3.4.4
     */
    class StringTool {

        /**
         * Parse HTML String
         *
         * @static
         * 
         * @param {String} str  HTML String to parse
         * 
         * @returns {HTMLElement} Parsed Element
         * 
         * @memberof StringTool
         * @since 3.5.0
         */
        static parseHTML(str) {
            let div = document.createElement('div');
            div.innerHTML = str;
            return div.firstElementChild;
        }
        
        /**
         * Camelize a word
         *
         * @static
         * @param {String} str
         * @param {String} separator    Separator used for string to be capitalized.
         * 
         * @returns {String}
         * 
         * @memberof StringTool
         */
        static camelize(str, separator) {
            return camelcase.toCamel(str, separator);
        }

        /**
         * Camelize to separated words
         *
         * @static
         * 
         * @param {String} str
         * @param {String} separator    Separator used for string to be capitalized.
         * 
         * @returns {String}
         * 
         * @memberof StringTool
         */
        static uncamelize(str, separator) {
            return camelcase.toDash(str, separator);
        }

        /**
         * transform first char to uppercase
         *
         * @static
         * 
         * @param {String} str
         * 
         * @returns {String}
         * 
         * @memberof StringTool
         */
        static ucfirst(str) {
            return str.charAt(0).toUpperCase() + str.slice(1);
        }

        /**
         * Trim String
         * 
         * @static
         * 
         * @returns {String}
         * 
         * @memberof StringTool
         */
        static trim(str) {
            return str.replace(/^\s+|\s+$/g, "");
        }
        
        /**
         * Trim String from left
         * 
         * @static
         * 
         * @returns {String}
         * 
         * @memberof StringTool
         */
        static ltrim(str) {
            return str.replace(/^\s+/,"");
        }

        /**
         * Trim String from right
         * 
         * @static
         * 
         * @returns {String}
         * 
         * @memberof StringTool
         */
        static rtrim(str) {
            return str.replace(/\s+$/,"");
        }


        /**
         * Checks if a string `str` starts with `needle`
         *
         * @static
         * 
         * @param {String} str      String to test
         * @param {String} needle   Needle to test
         * 
         * @returns {boolean} `true` if `str` starts with `needle`, `false` otherwise
         * 
         * @memberof StringTool
         */
        static startsWith(str, needle) {
            return (str.indexOf(needle) === 0);
        }

        /**
         * Print a number
         *
         * @static
         * 
         * @param {Number} num
         * @param {Number} [decs=1]
         * 
         * @returns {String} String representation of number
         * 
         * @memberof StringTool
         * @since 3.5.0
         */
        static printNum(num, decs = 1) {
            const str = (decs > 0) ? num.toFixed(decs).replace(/0+$/, '').replace(/\.$/, '')
                                   : num.toString();
            return str || '0';
        }
        
        /**
         * str_pad method to pad a string from left or right
         *
         * @static
         * 
         * @param {String} str  String to pad
         * @param {Number} len  target (min) length of `str`
         * @param {String} pad  Char(s) to pad
         * @param {String} side Can be eigther `start` or `left` to pad from start (left), at least any other is treated as `right` or `end`.
         * 
         * @returns {String}
         * 
         * @memberof StringTool
         * @since 3.5.0
         */
        static pad(str, len, pad, side) {
            side = side || 'start';
            while(str.length < len) {
                if(side === 'start' || side === 'left') {
                    str = pad + str;
                } else {
                    str = str + pad;
                }
            }
            return str;
        }

        /**
         * Is str blank?
         *
         * @static
         * 
         * @param {String} str
         * 
         * @returns {Boolean} `true` if `str` is a blank string, might be empty or with whitespace. 
         * 
         * @memberof StringTool
         */
        static isBlank(str) {
            return /^\s*$/.test(str);
        }

        /**
         * Is string empty?
         *
         * @static
         * 
         * @param {String} str
         * 
         * @returns {Boolean}   `true` if `str` is an empty string (`''`), `false` otherwise
         * 
         * @memberof StringTool
         */
        static isEmpty(str) {
            return '' === str;
        }

        /**
         * Is string an E-Mail Adress?
         *
         * @static
         * 
         * @param {String} str
         * 
         * @returns {Boolean}   `true` if `str` is an email address, `false` otherwise
         * 
         * @memberof StringTool
         */
        static isEmail(str) {
            return /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i.test(str);
        }

        /**
         * Is string a hex color?
         *
         * @static
         * 
         * @param {String} str
         * 
         * @returns {Boolean}   `true` if `str` is a color, `false` otherwise
         * 
         * @memberof StringTool
         */
        static isColor(str) {
            return /(^#[0-9A-F]{6}$)|(^#[0-9A-F]{3}$)/i.test(str);
        }

        /**
         * Is `str` an url?
         *
         * @static
         * @param {String} str
         * 
         * @returns {Boolean} `true` if `str` is a valid url, `false` otherwise.
         * 
         * @memberof StringTool
         */
        static isUrl(str) {
            let regexp = /^(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
            return regexp.test(str);
        }

    }

    return StringTool;
});
