/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("tools/string/highlight", [
	"tools/utils"
], (Utils) => {

	/**
	 * @private
	 */
	const defaults = {
		tag: 'b',
		className: ''
	};

	
	/**
	 * Highlight term in String
	 *
	 * @param {String} term		Term to search/highlight
	 * @param {String} str		String to search the term in
	 * @param {Object} options	Optional custom options
	 * 
	 * @returns {String} highlighted string
	 * 
	 * @exports tools/string/highlight
	 * @author qm-b <https://bitbucket.org/qm-b/>
	 */
	const highlight = (term, str, options) => {
		let o = Utils.extend(defaults, options),
			search = term.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'),
			re = new RegExp("(" + search.split(' ').join('|') + ")", "gi"),
			rep = "<"+o.tag+" class='"+o.className+"'>$1</"+o.tag+">";
		
		return str.replace(re, rep);
	};

	return highlight;
});
