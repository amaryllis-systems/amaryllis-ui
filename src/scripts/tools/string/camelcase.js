/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("tools/string/camelcase", [
], () => {

    /**
     * CamelCase
     *
     * @class CamelCase
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class CamelCase {

        /**
         * To Camel
         *
         * ``` js 
         * CamelCase.toCamel('foo-bar-baz', '-');
         * //=> 'fooBarBaz'
         * ```
         * 
         * @param {String}  str             String to convert
         * @param {String}  [separator='-'] separator Sepearator
         * 
         * @returns {String} camel string
         * 
         * @static
         * @memberof CamelCase
         */
        static toCamel(str, separator) {
            separator = separator || '-';
            let pattern = '(\\' + separator + '[a-z])';
            return str.replace(new RegExp(pattern, 'gi') , function($1){return $1.toUpperCase().replace(separator,'');});
        }

        /**
         * Convert camelcase to separated string
         * ``` js
         * CamelCase.toDash('fooBarBaz', ' ');
         * //=> 'foo bar baz'
         *
         * CamelCase.toDash('fooBarBaz', '-');
         * //=> 'foo-bar-baz'
         * ```
         * 
         * @param {String}  str             String to convert
         * @param {String}  [separator='-'] separator Sepearator
         * 
         * @returns {String}
         * 
         * @static
         * @memberof CamelCase
         */
        static toDash(str, separator) {
            separator = separator || '-';
            return str.replace(/([A-Z])/g, function($1){return separator + $1.toLowerCase();});
        }
    }

    return CamelCase;
});
