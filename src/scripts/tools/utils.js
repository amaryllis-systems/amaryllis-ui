/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("tools/utils", [
    'tools/object/extend',
    'tools/number/is-number',
    'tools/number/bytes',
    'tools/string/camelcase',
    'tools/string/sprintf',
    'tools/object/array-helper',
    'tools/string/unique-id',
    'tools/object/range',
    'tools/color/random-color'
], function(Extend, isNumber, Bytes, CamelCase, sprintf, ArrayHelper, UniqueId, Range, RandomColor) {

    /**
     * Utilities im System
     * 
     * @deprecated
     * @class
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class Utils {

        /**
         * 
         * @param {Integer} bytes
         * @param {Boolean} si      SI anstelle von IEC Profil verwenden?
         * 
         * @returns {String}
         * @deprecated
         */
        static convertBytes(bytes, si) {
            return Bytes(bytes, 2, si);
        }

        /**
         * Camelize
         * 
         * 
         *  Wandelt eine Bindestrich-separierte String-Kette in einen Camelized 
         *  String um.
         * 
         * 
         * @param {String} str   Der umzuwandelnde String
         * 
         * @returns {String}
         * @deprecated
         */
        static camelize(str) {
            
            return CamelCase.toCamel(str);
        }

        /**
         * Einfaches Erweitern von platten Objekten
         *
         * 
         *  Dise Methode wurde speziell für die Anforderungen der Objekt-Bildung
         *  von Optionen einer Klasse entwickelt.
         *  Das Ziel-Objekt sollte z.B. ein leeres Objekt sein. Source1 ist 
         *  meist das Objekt mit den Standard-Optionen, source2 die übergebenen
         *  Optionen und source3 die Element Data-Attribute
         * 
         *
         * @param {Object} destination  Ziel Object
         * @param {...Object} source       Quell-Object 1
         * 
         * @returns {Object}    Das neue Objekt
         * @deprecated
         */
        static extend(destination, source1, source2, source3) {

            return Extend.flat(destination, true, source1, source2, source3);
        }

        /**
         * Tiefes Erweitern von verschachtelten Objekten
         *
         * @param {Object} destination  Ziel Object
         * @param {Object} source      Quell-Objects
         * @returns {Object}
         * @deprecated
         */
        static deepExtend(destination, source1, source2, source3) {
            return Extend.deep(destination, true, source1, source2, source3);
        }

        /**
         * Prüft ob eine Variable numerisch ist
         * 
         * @returns {Boolean}   TRUE wenn es eine numerische Variable ist
         */
        static isNumber(n){
            return isNumber(n);
        }

        /**
         * Vergleichen von Arrays
         * 
         * @param {Array} a1    Array 1
         * @param {Array} a2    Array 2
         * 
         * @returns {Boolean}
         * @deprecated
         */
        static arrayCompare(a1, a2) {
            return ArrayHelper.compare(a1, a2);
        }

        /**
         * Generiert eine einheitliche Nummer basierend auf der aktuellen Zeit
         * @returns {Number}
         * @deprecated
         */
        static generateUniqeId() {
            return UniqueId();
        }

        /**
         * Prüft ob needle in haystack enthalten ist
         * 
         * @param {Mixed} nadel
         * @param {Array} haystack
         * @returns {Boolean}
         * @deprecated
         */
        static inArray(nadel, haystack) {
            return ArrayHelper.inArray(nadel, haystack);
        }

        /**
         * Erstellt eine Zufalls-Farbe (HEX)
         * 
         * @returns {String}
         * @deprecated
         */
        static getRandomColor() {
            return RandomColor.generate();
        }

        /**
         * Gibt das erste Element eines Array zurück
         * 
         * @param {Array} array
         * @returns {Mixed}
         * @deprecated
         */
        static first(array) {
            return ArrayHelper.first(array);
        }

        /**
         * Gibt das letzte Element eines Array zurück
         * 
         * @param {Array} array
         * @returns {Mixed}
         * @deprecated
         */
        static last(array) {
            return ArrayHelper.last(array);
        }

        /**
         * Splitten eines Array an einem Punkt i
         * 
         * @param {Array} array
         * @param {Integer} i
         * 
         * @returns {Array}
         * @deprecated
         */
        static eq(array, i) {
            return ArrayHelper.eq(array);
        }

        /**
         * Bilden eines Array ähnlich zur PHP range funktion
         * 
         * Beispiele:
         * 
         * ``` js
         *  console.log(range("A", "Z", 1));
         *  console.log(range("Z", "A", 1));
         *  console.log(range("A", "Z", 3));
         *  console.log(range(0, 25, 1));
         *  console.log(range(0, 25, 5));
         *  console.log(range(20, 5, 5));
         * ```
         * 
         * @param {Number|String}   start   Der Start-Punkt
         * @param {Number|String}   end     Der End-Punkt
         * @param {Number}          step    Einzelschritte
         * 
         * @returns {Array}
         * @deprecated
         */
        static range(start, end, step) {
            return Range.createRange(start, end, step);
        }
    }

    return Utils;
});
