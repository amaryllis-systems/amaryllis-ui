/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("tools/color/color", [
    "tools/color/color-converter",
    "tools/string/string-tool"
], (ColorConverter, StringTool) => {

    /**
     * Color
     * 
     * Class to deal with colors
     *
     * @class
     * @author qm-b <https://bitbucket.org/qm-b/>
     * @since 3.5.0
     */
    class Color {
        
        /**
         * @typedef {Object} Color~RGBA
         * @property {number} r - The R-Value of RGB(A)
         * @property {number} g - The G-Value of RGB(A)
         * @property {number} b - The B-Value of RGB(A)
         * @property {number} a - The A-Value of RGB(A)
         */
        
        /**
         * HSL representation of a color
         * 
         * @typedef {Object} Color~HSL
         * 
         * @property {number} h - The H-Value of HSL
         * @property {number} s - The S-Value of HSL
         * @property {number} l - The L-Value of HSL
         */

        /**
         * Creates an instance of Color.
         * 
         * @param {number|string|array|object} r
         * @param {number|undefined} g
         * @param {number|undefined} b
         * @param {number|undefined} a
         * 
         * @memberof Color
         */
        constructor(r, g, b, a) {

            const self = this;
            function parseString(input) {

                //HSL string. Examples:
                //	hsl(120, 60%,  50%) or 
                //	hsla(240, 100%, 50%, .7)
                if (StringTool.startsWith(input, 'hsl')) {
                    let [h, s, l, a] = input.match(/([\-\d\.e]+)/g).map(Number);
                    if (a === undefined) { a = 1; }

                    h /= 360;
                    s /= 100;
                    l /= 100;
                    self.hsla = {h: h, s: s, l: l, a: a};
                }

                //RGB string. Examples:
                //	rgb(51, 170, 51)
                //	rgba(51, 170, 51, .7)
                else if (StringTool.startsWith(input,'rgb')) {
                    let [r, g, b, a] = input.match(/([\-\d\.e]+)/g).map(Number);
                    if (a === undefined) { a = 1; }

                    self.rgba = {r: r, g: g, b: b, a: a};
                }

                //Hex string or color name:
                else {
                    if (StringTool.startsWith(input, '#')) {
                        self.rgba = self.converter.hexToRgb(input);
                    }
                    else {
                        self.rgba = self.converter.nameToRgb(input) || self.converter.hexToRgb(input);
                    }
                }
            }

            if (r === undefined) {
                //No color input - the color can be set later through .hsla/.rgba/.hex
            }

            //Single input - RGB(A) array
            else if (Array.isArray(r)) {
                if(typeof r[3] === 'undefined') r[3] = 1; 
                this.rgba = {r: r[0], g: r[1], b: r[2], a: r[3]};
            }
            //Single input - RGB(A) array
            else if (r && typeof r === 'object') {
                if(r.r) {
                    self.rgba = r;
                } else if(r.l) {
                    self.hsl = r;
                } else if(r.v) {
                    self.hsl = self.converter.hsvToHsl(hsv);
                } else {
                    throw new Error('Invalid object');
                }
            }

            //Single input - CSS string
            else if (b === undefined) {
                const color = r && ('' + r);
                if (color) {
                    parseString(color.toLowerCase());
                }
            }

            else {
                this.rgba = {r: r, g: g, b: b, a: (a === undefined) ? 1 : a};
            }
        }


        /**
         * Color Converter
         * 
         * @type {ColorConverter}
         * 
         * @readonly
         * @memberof Color
         */
        get converter() {
            return ColorConverter;
        }

        /* RGBA representation */

        /**
         * RGBA
         * 
         * @type {RGBA}
         *
         * @memberof Color
         */
        get rgba() {
            if (this._rgba) { 
                return this._rgba; 
            }
            if (!this._hsla) { 
                throw new Error('You must set a color first!');
            }

            return (this._rgba = this.converter.hslToRgb(this._hsla));
        }

        set rgba(rgb) {
            if (typeof rgb.a === 'undefined') { 
                rgb.a = 1; 
            }
            this._rgba = rgb;
            this._hsla = null;
        }

        /**
         * Print RGB string
         *
         * @param {Boolean} alpha   print with alpha?
         * 
         * @returns {String}
         * 
         * @memberof Color
         */
        printRGB(alpha) {
            const v = this.rgba;
            const rgb = alpha ? [v.r, v.g, v.b, v.a] : [v.r, v.g, v.b],
                vals = rgb.map((x, i) => StringTool.printNum(x, (i === 3) ? 3 : 0));

            return alpha ? `rgba(${vals})` : `rgb(${vals})`;
        }

        /**
         * Get rgb string of the color
         *
         * @type {String}
         *
         * @readonly
         * @memberof Color
         */
        get rgbString() {
            return this.printRGB(); 
        }

        /**
         * get rgba string of the color
         *
         * @type {String}
         *
         * @readonly
         * @memberof Color
         */
        get rgbaString() {
            return this.printRGB(true); 
        }


        /* HSLA representation */

        /**
         * Get hsla of the string
         * 
         * @type {HSL}
         *
         * @memberof Color
         */
        get hsla() {
            if (this._hsla) { return this._hsla; }
            if (!this._rgba) { throw new Error('No color is set'); }

            return (this._hsla = this.converter.rgbToHsl(this._rgba));
        }

        set hsla(hsl) {
            if (typeof hsl.a === 'undefined') { hsl.a = 1; }

            this._hsla = hsl;
            this._rgba = null;
        }

        /**
         * Print HSL string
         *
         * @param {Boolean} alpha   print with alpha?
         * 
         * @returns {String}
         * 
         * @memberof Color
         */
        printHSL(alpha) {
            const mults = [360, 100, 100, 1],
                suff = ['', '%', '%', ''];
            let v = self.hsla;
            const hsl = alpha ? [v.h, v.s, v.l, v.a] : [v.h, v.s, v.l],
                //in StringTool.printNum(), use enough decimals to represent all RGB colors:
                vals = hsl.map((x, i) => StringTool.printNum(x * mults[i], (i === 3) ? 3 : 1) + suff[i]);

            return alpha ? `hsla(${vals})` : `hsl(${vals})`;
        }

        /**
         * Get hsl string of the color
         *
         * @type {String}
         *
         * @readonly
         * @memberof Color
         */
        get hslString() { 
            return this.printHSL(); 
        }

        /**
         * Get hsla string of the color
         * 
         * @type {String}
         *
         * @readonly
         * @memberof Color
         */
        get hslaString() { 
            return this.printHSL(true); 
        }


        /* HEX representation */

        /**
         * hex representation of the color
         * 
         * @type {string}
         *
         * @memberof Color
         */
        get hex() {
            const rgb = this.rgba,
                hex = this.converter.rgbaToHex(rgb);

            return hex;
        }
        
        set hex(hex) {
            this.rgba = this.converter.hexToRgb(hex);
        }

        /**
         * Print hex
         *
         * @param {Boolean} alpha print with alpha? If true the hex string is 8 chars + prefix (`#`)
         * 
         * @returns {String}
         * 
         * @memberof Color
         */
        printHex(alpha) {
            const hex = this.hex;
            return alpha ? hex : hex.substring(0, 7);
        }

    }

    return Color;
});
