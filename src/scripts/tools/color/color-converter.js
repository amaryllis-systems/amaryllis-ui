/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('tools/color/color-converter', [
    "text!tools/color/color.json"
], (ColorData) => {

    /**
     * @private
     */
    const round = Math.round;

    /**
     * @private
     */
    const max = Math.max;

    /**
     * Named colors
     * 
     * @private
     */
    const named = JSON.parse(ColorData);

    /**
     * Color Converter
     * 
     * tool to convert colors from one format to another. The common 
     * color specifications such as hex, rgb(a), hsl and hsv are supported.
     * 
     * @class
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class ColorConverter {

        /**
         * RGB(A) representation of a color
         * 
         * @typedef {Object} ColorConverter~RGBA
         * 
         * @property {number} r - The R-Value of RGB(A) (0-255)
         * @property {number} g - The G-Value of RGB(A) (0-255)
         * @property {number} b - The B-Value of RGB(A) (0-255)
         * @property {number} a - The A-Value of RGB(A) (0-1.0)
         */

        /**
         * HSL representation of a color
         * 
         * @typedef {Object} ColorConverter~HSL
         * 
         * @property {number} h - The H-Value of HSL
         * @property {number} s - The S-Value of HSL
         * @property {number} l - The L-Value of HSL
         */

        /**
         * HSV representation of a color
         * 
         * @typedef {Object} ColorConverter~HSV
         * 
         * @property {(1-360)}  h   The H-Value of HSV(A) (1-360)
         * @property {(0-1.0)}  s   The S-Value of HSV(A) (0-1.0)
         * @property {(0-1.0)}  v   The V-Value of HSV(A) (0-1.0)
         */

        /**
         * Get named colors
         * 
         * Color Data is a JSON Storage of all named colors in css
         * see {@link https://bitbucket.org/amaryllissystems/amaryllis-ui/src/master/src/scripts/tools/color/color.json css-color-names}
         *
         * @readonly
         * @static
         * @memberof ColorConverter
         */
        static get namedColors() {
            return named;
        }
        

        /**
         * Gets the RGB value from a CSS color name
         * 
         * @static
         * 
         * @param {String} input    hex input to parse
         * 
         * @returns {RGBA|bool} rgba value  or `false` if not in named
         * 
         * @memberof ColorConverter
         */
        static nameToRgb(input) {
            const hex = ColorData[input];
            if (hex) {
                return Color.hexToRgb(hex);
            }
            return false;
        }


        /**
         * RGB zu Hex
         * 
         * @static
         * 
         * @param {RGBA} rgb  Ein Objekt mit den Attributen r/g/b und den 
         *                      jeweiligen numerischen Angaben als Werte
         *                      
         * @return {string}     Hex-Repräsentation des RGB Wertes
         * 
         * @memberof ColorConverter
         */
        static rgbToHex(rgb) {
            let hexR = rgb.r.toString(16);
            if (hexR <= 15) {
                hexR = "0" + hexR;
            }
            let hexG = rgb.g.toString(16);
            if (hexG <= 15) {
                hexG = "0" + hexG;
            }
            let hexB = rgb.b.toString(16);
            if (hexB <= 15) {
                hexB = "0" + hexB;
            }
            return hexR + "" + hexG + "" + hexB;
        }

        /**
         * Converts RGB(a) to hsl
         *
         * @static
         * 
         * @param {RGBA} rgba   RGBA of a color
         * 
         * @returns {Array} HSL representation
         * 
         * @memberof ColorConverter
         */
        static rgbToHsl(rgba) {
            r = rgba.r;
            g = rgba.g;
            b = rgba.b;
            a = rgba.a;

            r /= 255;
            g /= 255;
            b /= 255;

            const max = Math.max(r, g, b),
                min = Math.min(r, g, b);
            let h,
                s,
                l = (max + min) / 2;

            if (max === min) {
                h = s = 0; // achromatic
            } else {
                const d = max - min;
                s = (l > 0.5) ? d / (2 - max - min)
                    : d / (max + min);
                switch (max) {
                    case r: h = (g - b) / d + (g < b ? 6 : 0); break;
                    case g: h = (b - r) / d + 2; break;
                    case b: h = (r - g) / d + 4; break;
                }

                h /= 6;
            }

            return {
                h: h, 
                s: s,
                l: l,
                a: a
            };
        }

        /**
         * Konvertiert RGB zu HSV
         * 
         * @static
         * 
         * @param {RGBA} rgba      RGBA Color
         *                          
         * @return {HSV} HSV Color
         * 
         * @memberof ColorConverter
         */
        static rgbToHsv(rgba) {
            let r,
                g,
                b;
            if(typeof rgba.a === 'undefined') {
                rgba.a = 1;
            }
            r = rgba.r / 255;
            g = rgba.g / 255;
            b = rgba.b / 255;
            let v = Math.max(r, g, b);
            let min = Math.min(r, g, b);
            let h = 0;
            switch (v) {
                case min:
                    break;
                case r:
                    h = 60 * (g - b) / (v - min);
                    break;
                case g:
                    h = 60 * (2 + (b - r) / (v - min));
                    break;
                case b:
                    h = 60 * (4 + (r - g) / (v - min));
                    break;
                default:
                    break;
            }
            if (h < 0) {
                h += 360;
            }
            let s = v ? (1 - min / v) : 0;
            return {
                h: h,
                s: s,
                v: v,
                a: rgba.a
            };
        }

        /**
         * Konvertiert hsv zu hsl
         * 
         * @static
         * 
         * @param {HSV}  hsv             Objekt mit den Attributen h/s/v und 
         *                                  den jeweiligen numerischen Werten 
         *                                  1-360, 0-1.0, 0-1.0
         * 
         * @returns {HSL}         Ein Objekt mit den Attributen h/s/l als 
         *                          numerische Angaben 0-255/0-1/0-1
         * 
         * @memberof ColorConverter
         * 
         */
        static hsvToHsl(hsv) {
            if(typeof hsv.a === 'undefined') {
                hsv.a = 1;
            }
            let b = parseFloat(hsv.s),
                c = parseFloat(hsv.v),
                d = (2 - b) * c;
            return {
                h: hsv.h,
                s: b * c / (1 > d ? d : 2 - d) || 0,
                l: d / 2,
                a: hsv.a
            };
        }

        /**
         * Konvertiert HSV zu RGB
         * 
         * @static
         * 
         * @param {HSV}  hsv             Objekt mit den Attributen h/s/v und 
         *                                  den jeweiligen numerischen Werten 
         *                                  1-360, 0-1.0, 0-1.0
         *                                  
         * @return {RGBA} Object    RGB(A) repräsentanz
         * 
         * @memberof ColorConverter
         */
        static hsvToRgb(hsv) {
            if(typeof hsv.a === 'undefined') {
                hsv.a = 1;
            }
            let h = hsv.h;
            let s = hsv.s;
            let v = 255 * hsv.v;
            let hh = Math.floor(h / 60);
            let f = h / 60 - hh;
            let p = parseInt(v * (1 - s) + 0.5, 10);
            let q = parseInt(v * (1 - s * f) + 0.5, 10);
            let t = parseInt(v * (1 - s * (1 - f)) + 0.5, 10);
            v = parseInt(v + 0.5, 10);
            switch (hh) {
                case 1:
                    return {
                        r: q,
                        g: v,
                        b: p,
                        a: hsv.a
                    };
                case 2:
                    return {
                        r: p,
                        g: v,
                        b: t,
                        a: hsv.a
                    };
                case 3:
                    return {
                        r: p,
                        g: q,
                        b: v,
                        a: hsv.a
                    };
                case 4:
                    return {
                        r: t,
                        g: p,
                        b: v,
                        a: hsv.a
                    };
                case 5:
                    return {
                        r: v,
                        g: p,
                        b: q,
                        a: hsv.a
                    };
                default:
                    return {
                        r: v,
                        g: t,
                        b: p,
                        a: hsv.a
                    };
            }
        }

        /**
         * Konvertiert hsv zu hex
         * 
         * @static
         * 
         * @param {HSV}  hsv             Objekt mit den Attributen h/s/v und 
         *                                  den jeweiligen numerischen Werten 
         *                                  1-360, 0-1.0, 0-1.0
         * @param {Boolean} includeSign     Soll das Raute-Zeichen mit ausgegeben 
         *                                  werden?                         
         *                                  
         * @return {String}                 Hex Code als String
         * 
         * @memberof ColorConverter
         */
        static hsvToHex(hsv, includeSign) {
            if(typeof hsv.a === 'undefined') {
                hsv.a = 1;
            }
            includeSign = includeSign === false ? false : true;
            let rgb = this.hsvToRgb(hsv);
            let hexR = rgb.r.toString(16);
            if (hexR <= 15) {
                hexR = "0" + hexR;
            }
            let hexG = rgb.g.toString(16);
            if (hexG <= 15) {
                hexG = "0" + hexG;
            }
            let hexB = rgb.b.toString(16);
            if (hexB <= 15) {
                hexB = "0" + hexB;
            }

            let hex = hexR + '' + hexG + '' + hexB;
            return hex;
        }

        /**
         * Konvertiert HSL zu HSV
         * 
         * @static
         * 
         * @param {HSL} hsl    HSL Color
         *                          
         * @return {HSV}       HSV Color
         * 
         * @memberof ColorConverter
         */
        static hslToHsv(hsl) {
            if(typeof hsl.a === 'undefined') {
                hsl.a = 1;
            }
            let s = parseFloat(hsl.s);
            let l = parseFloat(hsl.l);
            l *= 2;
            if (l < 1) {
                s *= l;
            } else {
                s *= 2 - l;
            }
            return {
                h: hsl.h,
                s: 2 * s / (l + s) || 0,
                v: (l + s) / 2 || 0,
                a: hsl.a
            };
        }

        /**
         * Convert HSL to RGB
         * 
         * @static
         * 
         * @param {HSL} hsl HSL Color
         * 
         * @returns {RGBA} RGBA Color
         * 
         * @memberof ColorConverter
         */
        static hslToRgb(hsl) {
            if(typeof hsl.a === 'undefined') {
                hsl.a = 1;
            }
            let r, g, b;

            if (hsl.s === 0) {
                hsl.r = g = b = hsl.l; // achromatic
            } else {
                const q = (hsl.l < 0.5) ? 
                        hsl.l * (1 + hsl.s) : 
                        hsl.l + hsl.s - (hsl.l * hsl.s),
                    p = (2 * hsl.l) - q;

                r = this.hueToRgb(p, q, hsl.h + 1 / 3);
                g = this.hueToRgb(p, q, hsl.h);
                b = this.hueToRgb(p, q, hsl.h - 1 / 3);
            }

            const rgba = {
                r: r * 255, 
                g: g * 255,
                b: b * 255
            };
            rgba.a = hsl.a;

            return rgba;
        }

        /**
         * Konvertiert Hex zu RGB
         * 
         * @static
         * 
         * @param {string}  hex     Die HEX-Angabe der Farbe
         * 
         * @return {RGBA}   Ein Objekt mit den Attributen r/g/b/a und den 
         *                  jeweiligen zugehörigen Werten {r:r, g:g; b:b, a:a}
         * 
         * @memberof ColorConverter
         */
        static hexToRgb(hex) {
            if (hex.charAt(0) === '#') {
                hex = hex.substr(1);
            }
            //Normalize all hex codes (3/4/6/8 digits) to 8 digits RGBA
            const val = hex
                    .replace(/^(\w{3})$/,          '$1F')                   //987      -> 987F
                    .replace(/^(\w)(\w)(\w)(\w)$/, '$1$1$2$2$3$3$4$4')      //9876     -> 99887766
                    .replace(/^(\w{6})$/,          '$1FF');                 //987654   -> 987654FF

            if(!val.match(/^([0-9a-fA-F]{8})$/)) { throw new Error('Unknown hex color; ' + hex); }

            const arr = val
                .match(/^(\w\w)(\w\w)(\w\w)(\w\w)$/).slice(1)  //98765432 -> 98 76 54 32
                .map(x => parseInt(x, 16));                    //Hex to decimal

            arr[3] = arr[3]/255;

            const rgba = {
                r: arr[0],
                g: arr[1],
                b: arr[2],
                a: arr[3]
            };

            return rgba;
        }

        /**
         * hue value to rgb
         *
         * @static
         * 
         * @param {number} p
         * @param {number} q
         * @param {number} t
         * 
         * @returns {number} 
         * 
         * @private
         * @memberof ColorConverter
         */
        static hueToRgb(p, q, t) {
            if (t < 0) t += 1;
            if (t > 1) t -= 1;
            if (t < 1 / 6) return p + (q - p) * 6 * t;
            if (t < 1 / 2) return q;
            if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
            return p;
        }
    }

    return ColorConverter;

});
