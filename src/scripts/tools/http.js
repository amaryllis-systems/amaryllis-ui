/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * http Modul
 * 
 * @param {Object} doc Das document Objekt
 * 
 * @returns {Object} Das "http" Modul
 * 
 * @exports tools/http
 * 
 * @deprecated Wird in 2.0 entfernt. Siehe http/css-loader als ersatz.
 */
define("tools/http", ['core/document', 'core/acms'], function(doc, A) {


    /**
     * CSS Loader
     * 
     * Hilfs-Tool zum Nachladen von CSS Dateien aus dem System, theme oder Modul
     * 
     * @exports tools/http
     */
    var Module = {

        sheets: null,

        MODULE: 'HTTP Tools',

        /**
         * Laden von Css Dateien
         * 
         * Erwartet wir eine einzelne URL oder ein Object mit URLs oder ein Array,
         * Sowohl das Object als auch der Array sollten ein bestimmtes Markup haben:
         * 
         * @example 
         *          var urls = {
         *              1: {url: 'https://..', 'theme': true},
         *              2: {url: 'media/css/..', 'theme': true},
         *          };
         *          var urls2 = [
         *              {url: 'https://..', 'theme': true},
         *              {url: 'media/css/..', 'theme': true},
         *          };
         * 
         * @param {String|Array|Object} urls  URL der CSS Datei
         * @param {Boolean} theme    Optionaler Name des Themes
         */
        loadCSS: function(urls, theme) {
            Acms.Logger.logDeprecated('Module tools/http veraltet. Bitte http/css-loader als Ersatz verwenden.');
            if (Module.sheets === null) {
                Module._initCSS();
            }
            if(typeof urls === 'string') {
                urls = {
                    1: {
                        url: urls,
                        theme: (typeof theme === 'string' ? theme : (true === theme && typeof A.Theme !== 'undefined' ? A.Theme.foldername : false))
                    }
                };
            }
            if(urls instanceof Array) {
                urls.forEach(function(url) {
                    Module._doLoad(url);
                });
            } else if (typeof urls === "object" && null !== urls) {
                for(var prop in urls) {
                    Module._doLoad(urls[prop], theme);
                }
            }
        },
        
        _doLoad: function(urlConf, theme) {
            var url = (typeof urlConf === 'object') ? urlConf.url : urlConf;
            if(typeof theme === "undefined" && typeof urlConf !== 'string') {
                theme = urlConf.theme === true ? A.Theme.foldername : false;
            }
            
            if(typeof url !== "string") {
                console.log('Unable to load CSS Conf');
                console.log(urlConf);
                return;
            }
            if(url.substring(0, 4) !== 'http') {
                if(url.substring(0, 1) !== '/') {
                    url = '/' + url;
                }
                if(typeof theme === 'string') {
                    url = '/themes/' + theme + url;
                }
                url = A.Urls.baseUrl + url;
            }
            if(url.slice(-4) !== '.css') {
                url += (A.General.debug === true ? '.css' : '.min.css');
            }
            if (Module.sheets[url]) {
                return;
            }
            Module.sheets[url] = url;
            var link = doc.createElement("link");
            link.type = "text/css";
            link.rel = "stylesheet";
            link.href = url;
            doc.getElementsByTagName("head")[0].appendChild(link);
        },

        /**
         * Interne Modul Initialisierung
         * 
         * Initialisieren aller link-Tags mit einem href Attribut 
         * um dem doppelten Laden von Stylesheets vorzubeugen.
         * 
         * @access private
         */
        _initCSS: function() {
            Module.sheets = [];
            for (var i in doc.styleSheets) {
                if (doc.styleSheets[i].href) {
                    Module.sheets[doc.styleSheets[i].href] = doc.styleSheets[i].href;
                }
            }
        }

    };

    return Module;
});
