/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('tools/clipboard', [
    "core/base",
    'libs/clipboard/clipboard'
], function(Base, Clipboard) {

    /**
     * Clipboard Wrapper für die Clipboard Bibliothek
     *
     * @class
     * @extends {Base}
     */
    class ClipboardWrapper extends Base {

        
        /**
         * on success callback
         * 
         * @typedef {function} ClipboardWrapper~onSuccess
         * 
         * @param {String} action       Clipboard Action (copy, cut)
         * @param {String} text         Kopierter Text
         * @param {Element} trigger     HTML Element Trigger
         */

        /**
         * on error callback
         * 
         * @typedef {function} ClipboardWrapper~onError
         * 
         * @param {String} action       Clipboard Action (copy, cut)
         * @param {Element} trigger     HTML Element Trigger
         */
        
        /**
         * target callback
         * 
         * @typedef {function} ClipboardWrapper~target
         * 
         * @returns {HTMLElement} Element to copy
         */
        

        /**
         * Default Module options
         * 
         * __NOTE__: `text` __OR__ `target` is mandatory
         * 
         * @typedef {Object} ClipboardWrapper~ModuleOptions
         * 
         * @property {String}       text        Optinal text to copy
         * @property {target}       tatget      optional callable to get the target element to copy
         * @property {onSuccess}    onSuccess   on success callback
         * @property {onError}      onError     on error callback
         */

        

        /**
         * Module Namespace
         * 
         * @type {String}
         * 
         * @readonly
         * @static
         * @memberof ClipboardWrapper
         */
        static get NS() {
            return 'tools/clipboard';
        }

        /**
         * Module Name
         * 
         * @type {String}
         * 
         * @readonly
         * @static
         * @memberof ClipboardWrapper
         */
        static get MODULE() {
            return 'Clipboard wrapper';
        }

        /**
         * Default options
         * 
         * @type {ModuleOptions}
         * 
         * @readonly
         * @static
         * @memberof ClipboardWrapper
         */
        static get DEFAULT_OPTIONS() {
            return {
                onError: function(action, trigger) {},
                onSuccess: function(action, text, trigger) {},
                target: null,
                text: null
            };
        }

        /**
         * Module init
         *
         * @static
         * 
         * @param {HTMLElement}     element HTML Element
         * @param {ModuleOptions}   options Custom options
         * 
         * @returns {Object} New instance
         * 
         * @memberof ClipboardWrapper
         */
        static init(element, options) {
            let m = new ClipboardWrapper(element, options);
            m.element.acmsData(ClipboardWrapper.NS, m);

            return m;
        }

        /**
         * Creates an instance of ClipboardWrapper.
         * 
         * @param {HTMLElement}     element HTML Element
         * @param {ModuleOptions}   options Custom options
         * 
         * @memberof ClipboardWrapper
         */
        constructor(element, options) {
            super(element, options);
            this._initialize();
        }
        
        /**
         * Aufsetzen der Event-Listener
         * 
         * @memberof ClipboardWrapper
         */
        listen() {
            let self = this,
                o = self.options,
                element = self.element,
                opts = {};
            
            if (typeof o.text === 'function') {
                opts.text = o.text;
            } else if (o.target) {
                opts.target = o.target;
            }
            if(o.container) {
                opts.container = o.container;
            }
            let clipboard = new Clipboard(element, opts);
            clipboard.on('success', function(e) {
                o.onSuccess(e.action, e.text, e.trigger, clipboard);
                e.clearSelection();
            });

            clipboard.on('error', function(e) {
                o.onError(e.action, e.trigger);
            });
            self.clipboard = clipboard;
        }
    }

    return ClipboardWrapper;

});
