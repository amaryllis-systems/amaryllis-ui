/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("tools/class/class-tool", [], () => {

	/**
	 * Class Tools
	 *
	 * @class ClassTool
	 * @author qm-b <https://bitbucket.org/qm-b/>
	 */
	class ClassTool {

		/**
		 * List Methods of Class
		 *
		 * @static
		 * @param {*} clsName
		 * 
		 * @returns {Set}
		 * 
		 * @memberof ClassTool
		 */
		static listMethods(clsName) {
            let fns, fn;
            function getAllFuncs(obj) {
                let methods = new Set();
                while (obj = Reflect.getPrototypeOf(obj)) {
                    let keys = Reflect.ownKeys(obj);
                    keys.forEach((k) => methods.add(k));
                }
                return methods;
            }
            fns = getAllFuncs(clsName);
            return fns;
		}

		/**
		 * Binds Methods of `className` to `self`, if the Method starts with underscore (`_`)
		 *
		 * @static
		 * 
		 * @param {*} className
		 * @param {*} self
		 * 
		 * @memberof ClassTool
		 */
		static bindMethods(className, self) {
			let fns = this.listMethods(className);
			fns.forEach(fn => {
                if (fn.charAt(0) === '_' && fn.charAt(1) !== '' && typeof self[fn] === 'function') {
                    self[fn] = self[fn].bind(self);
                }
            });
		}
	}

	return ClassTool;
});
