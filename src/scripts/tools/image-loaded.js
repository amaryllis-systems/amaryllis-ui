/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("tools/image-loaded", [], () => {
	
	/**
	 * Is an image src loaded successful?
	 *
	 * @param {HTMLImageElement} img	Image Element
	 * 
	 * @returns {Boolean} true if the image is loaded
	 * 
	 * @exports tools/image-loaded
	 * @author qm-b <https://bitbucket.org/qm-b/>
	 */
	const isImageLoaded = (img) => {
		if(img.complete) {
			return true;
		}

		return (img.naturalHeight !== 0);
	};

	return isImageLoaded;

});
