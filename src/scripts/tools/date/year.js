/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("tools/date/year", [], () => {

    
    /**
     * Year output for the current year
     *
     * @param {String} pattern optional pattern (YYYY for full, YY for short year. pattern is case insensitive. )
     * 
     * @returns {String} formatted year
     * 
     * @exports tools/date/Year
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    const Year = (pattern) => {
        let year = new Date().getUTCFullYear().toString();
        if (typeof pattern !== 'string') {
          return year;
        }
        if (/[Yy]{4}/.test(pattern)) {
          return year;
        }
        if (/[Yy]{2}/.test(pattern)) {
          return year.substr(2, 2);
        }
    };

    return Year;
});
