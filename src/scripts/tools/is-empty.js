/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('tools/is-empty', [], () => {

    /**
     * Is value empty
     *
     * @param {*} val Value to check
     * @returns {boolean} `true` if empty, `false` otherwise
     * 
     * @exports tools/is-empty
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    const isEmpty = (val) => {
        if (val === 0 || typeof val === 'boolean') {
            
            return false;
        }
        if (val == null) {

            return true;
        }
        if (typeof val === 'object') {
            val = Object.keys(val);
        }
        if (!val.length) {

            return true;
        }

        return false;
    };

    return isEmpty;
    
});
