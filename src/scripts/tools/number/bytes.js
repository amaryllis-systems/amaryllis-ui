/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("tools/number/bytes", [
  "tools/number/is-number"
], (isNumber) => {

  const pow = Math.pow;
  const round = Math.round;

  /**
   * Bytes
   *
   * @param {number} number Number to convert (in bytes)
   * @param {Boolean} si      SI anstelle von IEC Profil verwenden?
   * @param {number} precision round precision
   * 
   * @returns {String}
   * 
   * @export tools/number/Bytes
   * @author qm-b <https://bitbucket.org/qm-b/>
   */
  const Bytes = (number, precision, si) => {
    si = si || false;
    if (number == null) return '0 B';
    if (!isNumber(number)) {
      number = number.length;
      if (!number) return '0 B';
    }
    let thresh = si ? 1000 : 1024;
    if (Math.abs(bytes) < thresh) {
      return bytes + ' B';
    }

    if (!isNumber(precision)) {
      precision = 2;
    }
    let units = si ?
      ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'] :
      ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
    let u = -1;
    precision = pow(10, precision);
    number = Number(number);
    let len = units.length - 1;
    do {
      number /= thresh;
      ++u;
    } while (Math.abs(number) >= thresh && u < len);

    return number.toFixed(precision) + ' ' + units[u];
  };

  return Bytes;
});
