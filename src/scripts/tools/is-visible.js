/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("tools/is-visible", [], () => {

	/**
	 * Is visible element?
	 *
	 * @param {HTMLElement} element Element to chekc
	 * @param {Boolean} 	[fullyVisible=true] Fill visible in document?
	 * @returns {Boolean}
	 * 
	 * @exports tools/is-visible
	 * @author qm-b <https://bitbucket.org/qm-b/>
	 */
	const isVisible = (element, fullyVisible) => {
		fullyVisible = (typeof fullyVisible === 'undefined' ? true : (true === fullyVisible));
		let	rect = element.getBoundingClientRect(),
			elemTop = rect.top,
			elemBottom = rect.bottom,
			visible;
		if(true === fullyVisible) {
			visible = (elemTop >= 0) && (elemBottom <= window.innerHeight);
		} else {
			// Partially visible elements return true
			visible = elemTop < window.innerHeight && elemBottom >= 0;
		}
		return isVisible;
	};

	return isVisible;
});
