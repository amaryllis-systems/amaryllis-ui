/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('tools/history', [], () => {

    /** 
     * Min
     * 
     * @private
     */
    const min = Math.min;

    /** 
     * truncate stack
     * 
     * @private
     */
    const truncate = (stack, limit) => {
        while (stack.length > limit) {
            stack.shift();
        }
    };


    /**
     * Simple History provider
     * 
     * Class providing a simple history for several cases.
     * 
     * @example
     * let inital = 'initial';
     * let currentItem = 'next';
     * let options = {
     *  provider: (cb) => {
     *      cb(currentItem); // pass the current item to the stack
     *  },
     *  maxLength: 200,
     *  onUpdate: () => {
     *      console.log(done);
     *  }
     * }
     * const hist = new SimpleHistory();
     * hist.setIinitial(initial);
     * hist.save();
     * alert(hist.count());
     *
     * @class
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class SimpleHistory 
    {

        /** 
         * Provider Creator
         * 
         * A function callback that will be called during `save` process. 
         * This should cover the current status of the history
         * 
         * @callback SimpleHistory~provider
         * 
         * @param {function} done Callback to be called when the history has proper saved
         */

        /** 
         * On Update Callback
         * 
         * A function callback that will be called during `save` process. 
         * This should cover the current status of the history
         * 
         * @callback SimpleHistory~onUpdate
         * 
         * @param {Object} self this instance of the module
         */

        /**
         * Custom Module options
         *  
         * @typedef {Object} SimpleHistory~ModuleOptions
         * 
         * @property {Number}   [maxLength=300] Max length of history
         * @property {provider} provider        the provider of the history and the responsible for saving 
         *                                      the history. @see {@linkcode SimpleHistory#~provider|provider}
         * @property {function} onUpdate        @see {@linkcode SimpleHistory#~onUpdate|onUpdate}
         */

        /**
         * Default options
         * 
         * @see {@linkcode SimpleHistory#ModuleOptions|ModuleOptions}
         *
         * @readonly
         * @static
         * @memberof SimpleHistory
         * 
         */
        static get DEFAULT_OPTIONS() {
            return {
                provider: () => {
                    throw new Error("No provider!");
                },
                maxLength: 300,
                onUpdate: (self) => {}
            };
        }

        /**
         * Creates an instance of SimpleHistory.
         * 
         * @param {Object} options @see {@linkcode SimpleHistory#~ModuleOptions|ModuleOptions}
         */
        constructor(options) {
            this.provider =
            this.maxLength =
            this.onUpdate =
            this.stack =
            this.position =
            this.initialItem = null;
            this.initialize(options);
        }
        
        /**
         * Get default options
         * 
         * @returns {Object} @see {@linkcode SimpleHistory#~ModuleOptions|ModuleOptions}
         * 
         * @memberof SimpleHistory
         */
        getDefaultOptions() {

            return SimpleHistory.DEFAULT_OPTIONS;
        }

        /** 
         * Build options
         * 
         * @param options  @see {@linkcode SimpleHistory#~ModuleOptions|ModuleOptions}
         * 
         * @memberof SimpleHistory
         */
        buildOptions(options) {
            let self = this,
                defaults = this.getDefaultOptions();
            self.provider = (typeof options.provider != 'undefined') ? options.provider : defaults.provider;
            self.maxLength = (typeof options.maxLength != 'undefined') ? options.maxLength : defaults.maxLength;
            self.onUpdate = (typeof options.onUpdate != 'undefined') ? options.onUpdate : defaults.onUpdate;
        }

        /** 
         * Init module
         * 
         * @param {Object} options @see {@linkcode SimpleHistory#ModuleOptions|ModuleOptions}
         * 
         * @memberof SimpleHistory
         * @private
         */
        _initialize(options) {
            if (!options) {
                options = {};
            }
            this.buildOptions(options);
            this.clear();
        }

        /**
         * Set initial item
         * 
         * @param {*} initialItem 
         */
        setInitial(initialItem) {
            this.stack[0] = initialItem;
            this.initialItem = initialItem;
        }

        /**
         * Clear history
         */
        clear() {
            this.stack = [this.initialItem];
            this.position = 0;
            this.onUpdate();
        }

        /**
         * Save history
         */
        save() {
            this.provider(function (current) {
                if (this.position >= this.maxLength) truncate(this.stack, this.maxLength);
                this.position = min(this.position, this.stack.length - 1);

                this.stack = this.stack.slice(0, this.position + 1);
                this.stack.push(current);
                this.position++;
                this.onUpdate();
            }.bind(this));
        }

        /**
         * undo last
         * 
         * @param {function} callback 
         */
        undo(callback) {
            if (this.canUndo()) {
                let item = this.stack[--this.position];
                this.onUpdate();

                if (callback) {
                    callback(item);
                }
            }
        }

        /**
         * Redo last 
         * 
         * @param {function} callback 
         */
        redo(callback) {
            if (this.canRedo()) {
                let item = this.stack[++this.position];
                this.onUpdate(this);

                if (callback) {
                    callback(item);
                }
            }
        }

        /**
         * Can undo last step?
         * 
         * @returns {Boolean}
         */
        canUndo() {
            return this.position > 0;
        }

        /**
         * Can redo last step?
         * 
         * @returns {Boolen}
         */
        canRedo() {
            return this.position < this.count();
        }

        /**
         * Count items
         */
        count() {
            return this.stack.length - 1; // -1 because of initial item
        }
    }

    return SimpleHistory;

});
