/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('tools/object/range', [], () => {

    /**
     * Range
     *
     * @class
     * @author qm-b <https://bitbucket.org/qm-b/>
     * @since 3.5.0
     */
    class Range {

        /**
         * Build a range similar as we can do in PHP range function
         * 
         * __Examples__:
         * 
         * ``` js
         *  console.log(range("A", "Z", 1));
         *  console.log(range("Z", "A", 1));
         *  console.log(range("A", "Z", 3));
         *  console.log(range(0, 25, 1));
         *  console.log(range(0, 25, 5));
         *  console.log(range(20, 5, 5));
         * ```
         * 
         * @param {Number|String}   start       Start point of the range
         * @param {Number|String}   end         End point of the range
         * @param {Number}          [step=1]    Steps
         * 
         * @returns {Array}
         * @memberof Range
         */
        static createRange(start, end, step) {

            let range = [],
                typeofStart = typeof start,
                typeofEnd = typeof end;
            if (step === 0) {
                throw TypeError("Step cannot be zero.");
            }
            if (typeofStart === "undefined" || typeofEnd === "undefined") {
                throw TypeError("Must pass start and end arguments.");
            } else if (typeofStart !== typeofEnd) {
                throw TypeError("Start and end arguments must be of same type.");
            }
            if (typeof step === "undefined") (step = 1);

            if (end < start) {
                step = -step;
            }

            if (typeofStart === "number") {

                while (step > 0 ? end >= start : end <= start) {
                    range.push(start);
                    start += step;
                }

            } else if (typeofStart === "string") {
                if (start.length !== 1 || end.length !== 1) {
                    throw TypeError("Only strings with one character are supported.");
                }
                start = start.charCodeAt(0);
                end = end.charCodeAt(0);
                while (step > 0 ? end >= start : end <= start) {
                    range.push(String.fromCharCode(start));
                    start += step;
                }
            } else {
                throw TypeError("Only string and number types are supported");
            }
            return range;
        }

    } 

    return Range;
});
