/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("tools/object/extend", [], () => {

    /**
     * Extend Util
     * 
     * Simple Utility to extend objects
     *
     * @class 
     * @author qm-b <https://bitbucket.org/qm-b/>
     * @since 3.5.0
     */
    class Extend {
        
        /**
         * Extends `destination` with all properties of `source` objects
         * 
         * Here, only each property of the source is simply copied to the 
         * destination. Nested objects are not taken into account.
         *
         * @param {Object}      destination  target Object
         * @param {Boolean}     processJSON  Try to JSON parse property values?
         * @param {...Object}   source       Source objects
         * 
         * @returns {Object}    Das neue Objekt
         */
        static flat(destination, processJSON, ...source) {
            const self = this; let i;
            for(i = 0; i < source.length; i++) {
                destination = self._doExtend(destination, processJSON, source[i]);
            }
            
            return destination;
        }

        /**
         * Deep Extend
         * 
         * extends deep nested properties from sources to destination.
         *
         * @param {Object}      destination     Destination object
         * @param {Boolean}     processJSON     Try to JSON parse property values?
         * @param {...Object}   source          Source objects
         * 
         * @returns {Object}
         */
        static deep(destination, processJSON, ...source) {
            const self = this; let i;
            for(i = 0; i < source.length; i++) {
                destination = self._doDeepExtend(destination, processJSON, source[i]);
            }
            
            return destination;
        }

        /**
         * Internal extend
         *
         * @static
         * 
         * @param {Object} destination
         * @param {Boolean}     processJSON     Try to JSON parse property values?
         * @param {Object} source
         * 
         * @returns {Object}
         * 
         * @private
         * @memberof Extend
         */
        static _doExtend(destination, processJSON, source) {
            for (let property in source) {
                if (source.hasOwnProperty(property)) {
                    let prop = source[property];
                    let isObj = (prop !== null && typeof prop === "object");

                    if(true === processJSON && false === isObj) {
                        try {
                            prop = JSON.parse(prop);
                        } catch(e) {
                            prop = prop;
                        }
                        isObj = (prop !== null && typeof prop === "object");
                    }
                    destination[property] = prop;
                }
            }
            return destination;
        }

        /**
         * Internal extend
         *
         * @static
         * 
         * @param {Object} destination
         * @param {Boolean}     processJSON     Try to JSON parse property values?
         * @param {Object} source
         * 
         * @returns {Object}
         * 
         * @private
         * @memberof Extend
         */
        static _doDeepExtend(destination, processJSON, source) {
            let self = this;
            for (let property in source) {
                if (false === source.hasOwnProperty(property)) {
                    continue;
                }
                let prop = source[property];
                let isObj = (prop !== null && typeof prop === "object");

                if(true === processJSON && false === isObj) {
                    try {
                        prop = JSON.parse(prop);
                    } catch(e) {
                        prop = prop;
                    }
                    isObj = (prop !== null && typeof prop === "object");
                }
                
                if (true === isObj) {
                    destination[property] = typeof destination[property] !== "object" ? {} : destination[property];
                    self._doDeepExtend(destination[property], processJSON, prop);
                } else {
                    destination[property] = prop;
                }
            }
            return destination;
        }

    }

    return Extend;
});
