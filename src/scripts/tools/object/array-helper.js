/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("tools/object/array-helper", [], () => {

    /**
     * Array Helper
     *
     * @class
     * @author qm-b <https://bitbucket.org/qm-b/>
     * @since 3.5.0
     */
    class ArrayHelper {

        /**
         * Vergleichen von Arrays
         * 
         * @param {Array} a1    Array 1
         * @param {Array} a2    Array 2
         * 
         * @returns {Boolean}
         * 
         * @static
         * @memberof ArrayHelper
         */
        static compare(a1, a2) {
            if (a1.length != a2.length)
                return false;
            let length = a2.length;
            for (let i = 0; i < length; i++) {
                if (a1[i] !== a2[i])
                    return false;
            }
            return true;
        }

        /**
         * Prüft ob needle in haystack enthalten ist
         * 
         * @param {Mixed} nadel
         * @param {Array} haystack
         * 
         * @returns {Boolean}
         * 
         * @static
         * @memberof ArrayHelper
         */
        static inArray(nadel, haystack) {
            let length = haystack.length,
                i;
            for (i = 0; i < length; i++) {
                if (haystack[i] instanceof Array) {
                    return this.compare(haystack[i], nadel);
                } else {
                    if (haystack[i] == nadel) {
                        return true;
                    }
                }
            }
            return false;
        }

        /**
         * Gibt das erste Element eines Array zurück
         * 
         * @param {Array} array
         * @returns {Mixed}
         * 
         * @static
         * @memberof ArrayHelper
         */
        static first(array) {
            return array.slice(0).pop();
        }

        /**
         * Gibt das letzte Element eines Array zurück
         * 
         * @param {Array} array
         * @returns {Mixed}
         * 
         * @static
         * @memberof ArrayHelper
         */
        static last(array) {
            return array.slice(-1).pop();
        }

        /**
         * Splitten eines Array an einem Punkt i
         * 
         * @param {Array} array
         * @param {Integer} i
         * 
         * @returns {Array}
         * 
         * @static
         * @memberof ArrayHelper
         */
        static eq(array, i) {
            return array.slice(i);
        }

    }

    return ArrayHelper;
});
