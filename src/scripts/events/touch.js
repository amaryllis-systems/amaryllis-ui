/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("events/touch", ["core/window", "core/document"], (w, d) => {

    let EVENT_MOUSEUP   = "mouseup",
        EVENT_MOUSEDOWN = "mousedown",
        EVENT_MOUSEMOVE = "mousemove",
        EVENT_CLICK     = "click",
        EVENT_LONGCLICK = "longclick";

    /**
     * Touch to Mouse Event
     *
     * @class
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class Touch {

        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof Touch
         */
        static get MODULE() {
            return "Touch to Mouse";
        }
        
        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof Touch
         */
        static get NS() {
            return "acms.events.touch";
        }

        /**
         * Direction "vertical"
         *
         * @readonly
         * @static
         * @memberof Touch
         */
        static get DIRECTION_VERTICAL() {
            return "vertical";
        }
        
        /**
         * Direction "Horizontal"
         *
         * @readonly
         * @static
         * @memberof Touch
         */
        static get DIRECTION_HORIZONTAL() {
            return "horizontal";
        }

        /**
         * Creates an instance of Touch.
         * 
         * @param {HTMLElement} element
         * 
         * @memberof Touch
         */
        constructor(element) {
            this.eventTimer =
            this.startX =
            this.startY =
            this.deltaX =
            this.deltaY = null;
            this.hasTouch = w.AmaryllisCMS.support.touch;
            this.mouseDown = false;
            this.initialize(element);
        }

        initialize(element) {
            var self = this;
            if (hasTouch) {
                element.addEventListener("touchstart", function (e) {
                    return self.touch2Mouse(e);
                }, true);
                element.addEventListener("touchmove", function (e) {
                    return self.touch2Mouse(e);
                }, true);
                element.addEventListener("touchend", function (e) {
                    return self.touch2Mouse(e);
                }, true);
            }
        }

        touch2Mouse(e) {
            var thisTouch = e.changedTouches[0],
                self = this, 
                mouseEv = this._getEventType(e.type);

            if(false === mouseEv) {
                return;
            }

            if (mouseEv === EVENT_MOUSEDOWN) {
                self.eventTimer = (new Date()).getTime();
                self.startX = thisTouch.clientX;
                self.startY = thisTouch.clientY;
                self.mouseDown = true;
            } else if (mouseEv === EVENT_MOUSEUP) {
                if ((new Date()).getTime() - self.eventTimer <= 500) {
                    mouseEv = EVENT_CLICK;
                } else if ((new Date()).getTime() - self.eventTimer > 1000) {
                    mouseEv = EVENT_LONGCLICK;
                }
                self.eventTimer = 0;
                self.mouseDown = false;
            } else if (mouseEv === EVENT_MOUSEMOVE) {
                if (true === self.mouseDown) {
                    self.deltaX = thisTouch.clientX - self.startX;
                    self.deltaY = thisTouch.clientY - self.startY;
                    self.moveDirection = self.deltaX > self.deltaY ?
                                            Touch.DIRECTION_HORIZONTAL :
                                            Touch.DIRECTION_VERTICAL;
                }
            }
            var ev = this._build(mouseEv, thisTouch);
            thisTouch.target.dispatchEvent(ev);

            e.preventDefault();
        }

        _build(mouseEv, thisTouch) {
            var init = {
                bubbles: true,
                cancelable: true,
                view: w,
                detail: 1,
                screenX: thisTouch.screenX,
                screenY: thisTouch.screenY,
                clientX: thisTouch.clientX,
                clientY: thisTouch.clientY,
                ctrlKey: false,
                altKey: false,
                shiftKey: false,
                metaKey: false,
                button: 0,
                relatedTarget: null
            };

            return new MouseEvent(mouseEv, init);
        }

        _getEventType(type) {
            switch (type) {
                case "touchstart":
                    return EVENT_MOUSEDOWN;
                case "touchend":
                    return EVENT_MOUSEUP;
                case "touchmove":
                    return EVENT_MOUSEMOVE;
                default:
                    return false;
            }
        }
    }

    return Touch;

});
