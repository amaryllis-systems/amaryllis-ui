/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("events/event", [
    "core/document", 
    "core/selectors"
], (d, Selectors) => {


    /**
     * Acms Event Handling
     * 
     * A helper tool for creating, emitting and listening events.
     * 
     * @class
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class AcmsEvent {

        /**
         * Creates an Event
         * 
         * ``` js
         * const e = AcmsEvent.create('evt');
         * ```
         * 
         * @param {String}  name    Der Event Name
         * @param {String}  type    Der Event Typ. Wenn nicht angegeben wird "HTMLEvents" als Typ genommen
         * @returns {Event}
         */
        static create(name, type) {
            let event;
            type = type || "HTMLEvents";
            if (d.createEvent) {
                event = d.createEvent(type);
                event.initEvent(name, true, true);
            } else {
                event = d.createEventObject();
                event.eventType = type;
            }
            event.eventName = name;
            return event;
        }

        /**
         * Creates a CustomEvent with detail Data Object
         * 
         * ``` js
         * const e = AcmsEvent.createCustom('evt', {foo: 'bar'});
         * console.log(e.detail.foo);
         * ```
         * 
         * @param {String} name Der Event name
         * @param {Object} data Die customEvent.detail Daten
         * 
         * @returns {CustomEvent}
         */
        static createCustom(name, data) {
            let ev, self = this;
            try {
                ev = new window.CustomEvent(name, {
                    detail: data,
                    bubbles: true,
                    cancelable: false
                });
            } catch (e) {
                let CustomEvent = function(event) {
                    let evt;

                    evt = self.create(event, 'CustomEvent');
                    return evt;
                };

                CustomEvent.prototype = window.Event.prototype;
                window.CustomEvent = CustomEvent;
                ev = new CustomEvent(name);
                ev.detail = data;
            }
            return ev;
        }

        /**
         * Add Event-Listener
         * 
         * @param {HTMLElement} obj
         * @param {String} type
         * @param {CallableFunction} fn
         * 
         * @returns {Boolean}
         */
        static add(obj, type, fn, useCapture) {
            let success;
            useCapture = useCapture || false;
            if ('addEventListener' in obj) {
                success = obj.addEventListener(type, fn, useCapture);
            } else if ("attachEvent" in obj) {
                success = obj.attachEvent('on' + type, fn);
            }
            return success;
        }

        /**
         * Fügt mehrere Listener hinzu
         * 
         * ``` js
         * AcmsEvent.addMultiple(
         *      document.getElementByID('elem'),
         *      ['click', 'mouseenter', 'mouseleave'],
         *      function(e) {console.log(e)}
         * );
         * ```
         * 
         * @param {HTMLElement} element
         * @param {Array} events
         * @param {CallableFunction} handler
         * 
         * 
         */
        static addMultiple(element, events, handler, useCapture) {
            if (!(events instanceof Array)) {
                throw 'addMultiple: ' +
                    'please supply an array of eventstrings ' +
                    '(like ["click","mouseover"])';
            }
            for (let i = 0; i < events.length; i++) {
                this.add(element, events[i], handler, useCapture);
            }
        }

        /**
         * Entfernen eines Event-Listeners
         * 
         * @param {HTMLElement} obj Element to remove listener from
         * @param {String} type Event type
         * @param {CallableFunction} fn Callback Listener
         * 
         * 
         */
        static remove(obj, type, fn) {
            if ('removeEventListener' in obj) {
                obj.removeEventListener(type, fn, false);
            } else if ('detachEvent' in obj) {
                obj.detachEvent('on' + type, fn);
            } else {
                obj.removeEventListener(type, fn, false);
            }
        }

        /**
         * Entfernt mehrere Listener
         * 
         * @param {HTMLElement} element
         * @param {Array} events
         * @param {CallableFunction} handler
         * 
         * 
         */
        static removeMultiple(element, events, handler) {
            if (!(events instanceof Array)) {
                throw 'addMultiple: ' +
                    'please supply an array of eventstrings ' +
                    '(like ["click","mouseover"])';
            }
            for (let i = 0; i < events.length; i++) {
                this.remove(element, events[i], handler);
            }
        }

        /**
         * Auflösen eines Events
         * 
         * ``` js
         * const e = AcmsEvent.createCustom('myevent', {});
         * AcmsEvent.dispatch(document.body, e);
         * ```
         * 
         * @param {HTMLElement} obj     Element to dispatch the event on
         * @param {Event}       event   Event to dispatch
         */
        static dispatch(obj, event) {
            if (typeof event !== 'object') {
                return false;
            }
            if (d.createEvent) {
                obj.dispatchEvent(event);
            } else {
                obj.fireEvent("on" + event.eventType, event);
            }
        }

        /**
         * Auflösen eines Events
         * 
         * ``` js
         * AcmsEvent.emit(document.body, 'myevent', {foo: document.createElement('div'), bar: 'custom'});
         * ```
         * 
         * @param {HTMLElement} obj         Das Object auf dem das Event ausgeführt werden soll
         * @param {String}      eventName   Der Event Name
         * @param {Object}      params      Die Parameter
         * 
         * @returns {Event}
         */
        static emit(obj, eventName, params) {
            let evt = this.createCustom(eventName, params);
            if (d.createEvent) {
                obj.dispatchEvent(evt);
            } else {
                obj.fireEvent("on" + evt.eventType, evt);
            }

            return evt;
        }

        /**
         * 
         * @param {HTMLElement} obj
         * @param {String} type
         * @param {String} selector
         * @param {CallableFunction} fn
         * @param {Boolean} checkParent
         * 
         * 
         */
        static on(obj, type, selector, fn, checkParent) {
            checkParent = checkParent || false;
            const cb = function(e) {
                let target = e.target,
                    noBrackets = (selector.indexOf('[') === -1),
                    isSel = (selector.indexOf('.') === 0 || selector.indexOf('#') === 0);
                if (Selectors.matches(target, selector) ||
                    (true === noBrackets && target.classList.contains(selector)) ||
                    (checkParent === true &&
                        Selectors.closest(target, selector) !== null)
                ) {
                    fn(e);
                }

            };
            AcmsEvent.add(obj, type, cb, (type === 'blur' || type === 'focus'));
        }

        /**
         * 
         * @param {HTMLElement} obj
         * @param {String} type
         * @param {String} selector
         * @param {CallableFunction} fn
         * @param {Boolean} checkParent
         * 
         * 
         */
        static off(obj, type, selector, fn, checkParent) {
            checkParent = checkParent || false;
            let cb = function(e) {
                let target = e.target,
                    noBrackets = (selector.indexOf('[') === -1),
                    isSel = (selector.indexOf('.') === 0 || selector.indexOf('#') === 0);
                if (Selectors.matches(target, selector) ||
                    (true === noBrackets && target.classList.contains(selector)) ||
                    (checkParent === true &&
                        Selectors.closest(target, selector) !== null)
                ) {
                    fn(e);
                }

            };
            AcmsEvent.remove(obj, type, cb);
        }

        /**
         * Stop Event
         *
         * @static
         * 
         * @param {Event}   e               Event to stop
         * @param {Boolean} [prevent=true]  Prevent default?
         * @param {Boolean} [stop=true]     Stop propagation?
         * 
         * @memberof AcmsEvent
         * @since 3.5.0
         */
        static stop(e, prevent, stop) {
            if(typeof prevent === "undefined") prevent = true;
            if(typeof stop === "undefined") stop = true;
            if(true === stop) {
                e.stopPropagation();
            }
            if(true === prevent) {
                e.preventDefault();
            }
        }

        /**
         * Fires click event
         *
         * @static
         * @param {HTMLElement} elem
         * @memberof AcmsEvent
         */
        static fireClick(elem) {
            if (typeof MouseEvent === 'function') {
                let mevt = new MouseEvent('click', {
                    view: window,
                    bubbles: true,
                    cancelable: true
                });
                this.dispatch(elem, mevt);
            } else if (document.createEvent) {
                let evt = document.createEvent('MouseEvents');
                evt.initEvent('click', true, true);
                this.dispatch(elem, evt);
            } else if (document.createEventObject) {
                elem.fireEvent('onclick');
            } else if (typeof elem.onclick === 'function') {
                elem.onclick();
            }
        }

        /**
         * Feuert das change-Event auf elem
         * 
         * @param {HTMLElement} elem
         * 
         * 
         */
        static fireChange(elem) {
            if ("createEvent" in document) {
                let evt = document.createEvent("HTMLEvents");
                evt.initEvent("change", true, true);
                elem.dispatchEvent(evt);
            } else {
                elem.fireEvent("onchange");
            }
        }

        /**
         * Feuert das resize-Event auf window
         * 
         * @param {Object} elem
         * 
         */
        static fireResize() {
            if ("createEvent" in document) {
                let evt = window.document.createEvent('UIEvents');
                evt.initUIEvent('resize', true, false, window, 0);
                window.dispatchEvent(evt);
            } else {
                event = d.createEventObject();
                event.eventType = 'UIEvents';
                window.dispatchEvent(event);
            }
        }

        static onetimeEvent(elem, type, callback) {
            let onetimecb = (e) => {
                AcmsEvent.remove(e.target, e.type, onetimecb);
                callback();
            };
            AcmsEvent.add(elem, type, onetimecb);
        }

    }

    return AcmsEvent;

});
