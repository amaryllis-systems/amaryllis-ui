/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("events/swipe", [
    "core/base",
    'tools/utils',
    'events/event'
], (Base, Utils, AcmsEvent) => {

    /**
     * Swipe Listener
     * 
     * @class
     * @extends {Base}
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class SwipeListener extends Base {

        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof SwipeListener
         */
        static get MODULE() {
            return "Swipe Event";
        }

        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof SwipeListener
         */
        static get NS() {
            return "acms.events.swipe";
        }

        /**
         * Event "up"
         * 
         * Event fired on swipe up
         *
         * @readonly
         * @static
         * @memberof SwipeListener
         */
        static get EVENT_UP() {
            return SwipeListener.NS + '.swipe-up';
        }

        /**
         * Event "down"
         * 
         * Event fired on swipe down
         *
         * @readonly
         * @static
         * @memberof SwipeListener
         */
        static get EVENT_DOWN() {
            return SwipeListener.NS + '.swipe-down';
        }

        /**
         * Event "left"
         * 
         * Event fired on swipe left
         *
         * @readonly
         * @static
         * @memberof SwipeListener
         */
        static get EVENT_LEFT() {
            return SwipeListener.NS + '.swipe-left';
        }

        /**
         * Event "right"
         * 
         * Event fired on swipe right
         *
         * @readonly
         * @static
         * @memberof SwipeListener
         */
        static get EVENT_RIGHT() {
            return SwipeListener.NS + '.swipe-right';
        }

        /**
         * Module Options
         *
         * @readonly
         * @static
         * @memberof SwipeListener
         */
        static get DEFAULT_OPTIONS() {
            return {
                onSwipeUp: function(opts) {
                    
                },
                onSwipeDown: function(opts) {
                    
                },
                onSwipeLeft: function(opts) {
                    
                },
                onSwipeRight: function(opts) {
                    
                },
                xTolerance: 0,
                yTolerance: 0
                
            };
        }

        /**
         *
         *
         * @static
         * 
         * @param {HTMLElement|document} element
         * @param {Object|null} options
         * 
         * @returns {SwipeListener}
         * 
         * @memberof SwipeListener
         */
        static init(element, options) {
            let sl = new SwipeListener(element, options);
            if('acmsData' in sl.element) {
                sl.element.acmsData(SwipeListener.NS, sl);
            }
    
            return sl;
        }

        /**
         * Creates an instance of SwipeListener.
         *
         * @param {HTMLElement} element
         * @param {Object|null} options
         * @memberof SwipeListener
         */
        constructor(element, options) {
            if(document === element) {
                element = document.body;
            }
            super(element, options);
            this._initialize();
        }

        
        /**
         * Bilden der eigenen Optionen
         * 
         * @param {Object} options
         * @returns {Object}
         */
        buildOptions(options) {
            let eleOpts = ('acmsData' in this.element ? this.element.acmsData() : {});
            let o = Utils.extend({}, this.getDefaultOptions(), options, eleOpts);
            o.xTolerance = parseInt(o.xTolerance);
            o.yTolerance = parseInt(o.yTolerance);
            
            return o;
        }
        
        /**
         * Aufsetzen der Event Listener
         * 
         * 
         */
        listen() {
            let self = this, element = self.element;
            AcmsEvent.add(element, 'touchstart', self._onTouchStart);
            AcmsEvent.add(element, 'touchmove', self._onTouchMove);
        }
        
        /**
         * TouchStart Event Handler
         * 
         * @param {Event} e
         * 
         * 
         */
        _onTouchStart(e) {
            this.xDown = e.touches[0].clientX;
            this.yDown = e.touches[0].clientY;
        }
        
        /**
         * Touch Move Event Listener
         * 
         * @param {type} e
         * 
         */
        _onTouchMove(e) {
            if (!this.xDown || !this.yDown) {
                return;
            }
            let self = this,
                element = self.element,
                o = self.options,
                xDown = self.xDown,
                yDown = self.yDown,
                xUp = e.touches[0].clientX,
                yUp = e.touches[0].clientY,
                xDiff = xDown - xUp,
                xAbsDiff = Math.abs(xDiff),
                yDiff = yDown - yUp, evt,
                yAbsDiff = Math.abs(yDiff),
                evo = {
                    xDown: xDown,
                    yDown: yDown,
                    xUp: xUp,
                    yUp: yUp,
                    xDiff: xDiff,
                    yDiff: yDiff,
                    element: element
                };
            if(xAbsDiff < o.xTolerance) {
                return;
            }
            if(yAbsDiff < o.yTolerance) {
                return;
            }
            if (Math.abs(xDiff) > Math.abs(yDiff)) {
                if (xDiff > 0) {
                    evt = AcmsEvent.createCustom(SwipeListener.EVENT_LEFT, evo);
                    AcmsEvent.dispatch(element, evt);
                    if(typeof o.onSwipeLeft === "function") {
                        o.onSwipeLeft(evo);
                    }
                } else {
                    evt = AcmsEvent.createCustom(SwipeListener.EVENT_RIGHT, evo);
                    AcmsEvent.dispatch(element, evt);
                    if(typeof o.onSwipeRight === "function") {
                        o.onSwipeRight(evo);
                    }
                }
            } else {
                if (yDiff > 0) {
                    evt = AcmsEvent.createCustom(SwipeListener.EVENT_UP, evo);
                    AcmsEvent.dispatch(element, evt);
                    if(typeof o.onSwipeUp === "function") {
                        o.onSwipeUp(evo);
                    }
                } else {
                    evt = AcmsEvent.createCustom(SwipeListener.EVENT_DOWN, evo);
                    AcmsEvent.dispatch(element, evt);
                    if(typeof o.onSwipeDown === "function") {
                        o.onSwipeDown(evo);
                    }
                }
            }
            self.xDown = null;
            self.yDown = null;
        }

        _prepare() {
            this.xDown =
            this.yDown = null;
        }
    }

    return SwipeListener;

});
