/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("http/request", ["http/ajax/ajax", "tools/string/string-tool"], (
    Ajax,
    StringTool
) => {

    /**
     * HTTP Request
     *
     * Simple Wrapper around the most common use cases inside our
     * framework.
     *
     * However, sending requests to other sites should be initiated manually.
     *
     * @example
     * // simple post request
     * require(['https/request', 'notifier'], AcmsHttpRequest => {
     *  let cb = (response, jsonResponse, rawResponse) {
     *      // notify user
     *      Acms.Notifications.createFromResponse(Response);
     *      // optionaly process response.
     *      // first is the instance of this class, second
     *      // a JSON Data object of the response and last
     *      // is the raw String representation of the response.
     *  };
     *  let onFailed = (xhr, textStatus) {
     *      alert('Failed!');
     *  };
     *  let onAlways = (xhr, textStatus) => {
     *      console.log('Done: ' + textStatus);
     *  }
     *  AcmsHttpRequest.post(remoteUrl, new FormData(), cb, onFailed, onAlways);
     * });
     * // manuallly
     * require(['https/request', 'notifier'], AcmsHttpRequest => {
     *  let opts = {
     *      url: url,
     *      type: 'JSON',
     *      method: 'post',
     *      data: new FormData(),
     *      headers: {},
     *      processContent: false,
     *      token: null, // CSRF Token
     *      timeout: 0, // Connect timeout
     *  	onStart: () => {},
     *      onSuccess: (response, jsonResponse, rawResponse) => {},
     *      onError: (xhr, statusText) => {},
     *      onFinish: (xhr, response, statusText) => {},
     *      onTimeout: () => {},
     *      onprogress: (loaded, total, percent, xhr, e) => {},
     *      onLoad: null // xhr onload Callback
     * };
     *  
     *  let req = AcmsHttpRequest.getRequest(opts);
     *  req.send();
     * });
     *
     * @class
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class AcmsHttpRequest {
        /**
         * Module Namespace
         *
         * @type {String}
         *
         * @readonly
         * @static
         * @memberof AcmsHttpRequest
         */
        static get NS() {
            return "http.request";
        }

        /**
         * Module Name
         *
         * @type {String}
         *
         * @readonly
         * @static
         * @memberof AcmsHttpRequest
         */
        static get MODULE() {
            return "HTTP Request";
        }

        /**
         * Gets the Ajax Request Wrapper
         *
         * @readonly
         * @static
         * @memberof AcmsHttpRequest
         * @var {Ajax}
         */
        static get ajax() {
            return Ajax;
        }

        /**
         * Gets a new instance of a Transport Class
         *
         * @readonly
         * @static
         *
         * @memberof AcmsHttpRequest
         *
         * @type {XMLHttpRequest|ActiveXObject}
         */
        static get transport() {
            return Ajax.getTransport();
        }

        /**
         * Gets a custom Request instance
         *
         * @param {Object} args Custom Options
         *
         * @returns {Request}
         *
         * @static
         * @memberof AcmsHttpRequest
         */
        static getRequest(args) {
            return Ajax.getRequest(args);
        }

        /**
         * POST Request
         *
         * Führt einen POST Request aus und gibt die Anfrage zurück. Für
         * diese Abfrage wird eine JSON Response erwartet!
         *
         * @param {String}          url         Die Remote-URL für die Abfrage
         * @param {FormData|NULL}   data        Optionale Daten, die mit der Anfrage gesendet werden sollen
         * @param {Callable|NULL}   onDone      Callback nach Beendigung der Abfrage (http/ajax/ajax().done())
         * @param {Callable|NULL}   onFail      Callback wenn die Abfrage fehlschlägt (http/ajax/ajax().fail())
         * @param {Callable|NULL}   onAlways    Callback wenn die Abfrage ausgeführt wird (http/ajax/ajax().always())
         *
         * @returns {Request}
         */
        static post(url, data, onDone, onFail, onAlways) {
            let opts = this._makeOptions(
                url,
                "JSON",
                "post",
                "json",
                data,
                onDone,
                onFail,
                onAlways
            );
            return this._doSend(opts);
        }

        /**
         * Lädt eine Einzelne Datei direct zu [url] hoch.
         *
         * @param {String}      url             Die Upload-URL
         * @param {File}        fileToUpload    Das Datei-Objekt
         * @param {String}      name            Der POST Name für die Datei
         * @param {Function}    onDone          Funktion für erfolgreichen
         *                                      Abschluss des XHR Request
         *                                      (enthält den status über
         *                                      gelungenen oder auch
         *                                      misslungenen Upload)
         * @param {Function}    onFail          Funktion für Fehler während des
         *                                      XHR Request
         * @param {Function}    onAlways        Funktion wird immer ausgeführt,
         *                                      wenn alles abgeschlossen wurde
         * @param {Function}    onProgress      On Upload Progress Callback
         *
         * @returns {Request}
         */
        static upload(
            url,
            fileToUpload,
            name,
            onDone,
            onFail,
            onAlways,
            onProgress
        ) {
            let fData = new FormData(),
                opts;
            fData.append(name, fileToUpload, fileToUpload.name);
            opts = this._makeOptions(
                url,
                "JSON",
                "post",
                "json",
                fData,
                onDone,
                onFail,
                onAlways
            );
            opts.onprogress = onProgress;
            return this._doSend(opts);
        }

        /**
         * Download
         *
         * @static
         *
         * @param {string}          url         Request URL
         * @param {('post'|'get')}  method      Request method for file
         * @param {FormData}        fData       Data send to the server
         * @param {function}        onDone
         * @param {function}        onFail
         * @param {function}        onAlways
         *
         * @returns {Request}
         *
         * @memberof AcmsHttpRequest
         */
        static download(url, method, fData, onDone, onFail, onAlways) {
            let opts, onLoad;
            onLoad = function(e) {
                if (parseInt(this.status) === 200) {
                    let ctype = this.getResponseHeader("content-type");
                    let blob = new Blob([this.response], { type: ctype });
                    let a = document.createElement("a");
                    a.style = "display: none;visibility: hidden;";
                    document.body.appendChild(a);
                    let burl = window.URL.createObjectURL(blob);
                    a.href = burl;
                    a.download = this.getResponseHeader("filename");
                    a.click();
                    window.URL.revokeObjectURL(burl);
                } else {
                    Acms.Logger.logWarning("XHR ist schief gelaufen");
                }
            };
            opts = this._makeOptions(
                url,
                "BLOB",
                method,
                "blob",
                fData,
                onDone,
                onFail,
                onAlways
            );
            opts.processContent = false;
            opts.onLoad = onLoad;

            return this._doSend(opts);
        }

        /**
         * POST Request
         *
         * Führt einen POST Request aus und gibt die Anfrage zurück. Für
         * diese Abfrage wird eine JSON Response erwartet!
         *
         * @param {String}          url         Die Remote-URL für die Abfrage
         * @param {Object|NULL}     data        Optionale Daten, die mit der Anfrage gesendet werden sollen
         * @param {Callable|NULL}   onDone      Callback nach Beendigung der Abfrage (http/ajax/ajax().done())
         * @param {Callable|NULL}   onFail      Callback wenn die Abfrage fehlschlägt (http/ajax/ajax().fail())
         * @param {Callable|NULL}   onAlways    Callback wenn die Abfrage ausgeführt wird (http/ajax/ajax().always())
         *
         * @returns {Request}
         */
        static postFile(url, data, onDone, onFail, onAlways) {
            Acms.Logger &&
                Acms.Logger.logDeprecated(
                    "postFile ist DEPRECATED and not longer supported. See Request.post or Request.upload"
                );
            return this.post(url, data, onDone, onFail, onAlways);
        }

        /**
         * GET Request
         *
         * Führt einen POST Request aus und gibt die Anfrage zurück. Für
         * diese Abfrage wird eine JSON Response erwartet!
         *
         * @param {String}          url         Die Remote-URL für die Abfrage
         * @param {Object|NULL}     data        Optionale Daten, die mit der Anfrage gesendet werden sollen als standard object (`{}`)
         * @param {Callable|NULL}   onDone      Callback nach Beendigung der Abfrage
         * @param {Callable|NULL}   onFail      Callback wenn die Abfrage fehlschlägt
         * @param {Callable|NULL}   onAlways    Callback wenn die Abfrage ausgeführt wird
         * @param {String|NULL}     type        Der Return Typ
         *
         * @returns {Request}
         */
        static get(url, data, onDone, onFail, onAlways, type) {
            let opts;
            opts = this._makeOptions(
                url,
                type || "TXT",
                "get",
                type || "TXT",
                data,
                onDone,
                onFail,
                onAlways
            );
            opts.processContent = true;
            return this._doSend(opts);
        }

        /**
         * Finaly Send Request
         *
         * @param {Object} opts
         *
         * @returns {Request} Ajax Request
         *
         * @memberof AcmsHttpRequest
         * @private
         */
        static _doSend(opts) {
            let req = Ajax.getRequest(opts);
            req.send();

            return req;
        }

        /**
         * Make Options
         *
         * @static
         *
         * @param {String} url      Remote Url
         * @param {String} type     Request Type (e.g. `JSON`)
         * @param {String} method   Request Method (e.g. `post`)
         * @param {String} dataType Data Type (e.g. `json`)
         * @param {FormData|Object} data    Request data
         * @param {Function} onDone On Done Callback
         * @param {Function} onFail On Fail callback
         * @param {Function} onAlways On Allways Callback
         *
         * @returns {Object}
         *
         * @memberof AcmsHttpRequest
         * @private
         */
        static _makeOptions(
            url,
            type,
            method,
            dataType,
            data,
            onDone,
            onFail,
            onAlways
        ) {
            this._assertUrl(url);
            onDone = onDone || function() {};
            onFail = onFail || function() {};
            onAlways = onAlways || function() {};

            return {
                url: url,
                type: type,
                method: method,
                dataType: dataType,
                data: data,
                onSuccess: onDone,
                onError: onFail,
                onFinish: onAlways
            };
        }

        /**
         * Assert Url to be valid
         *
         * @static
         * @param {String} url
         * @memberof AcmsHttpRequest
         * @private
         */
        static _assertUrl(url) {
            if (!url || typeof url !== "string" || !StringTool.isUrl(url)) {
                throw new Error("Eine Url ist erforderlich!");
            }
        }
    }

    return AcmsHttpRequest;
});
