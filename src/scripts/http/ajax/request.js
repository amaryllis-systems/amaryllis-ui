/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("http/ajax/request", [
    "http/ajax/response",
    "tools/object/extend",
    "tools/string/string-tool"
], (Response, Extend, StringTool) => {

    /** 
     * Callbacks
     * 
     * @var {Array}
     * @memberof Request
     */
    const CALLBACKS = [
        'onStart',
        'onSuccess',
        'onError',
        'onFinish',
        'onTimeout',
        'onLoad'
    ];

    /**
     * Ajax Request
     *
     * @class
     * @author qm-b <https://bitbucket.org/qm-b/>
     * 
     */
    class Request {

        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof Request
         */
        static get NS() {
            return "http.ajax.request";
        }

        /**
         * Content Types
         *
         * @readonly
         * @static
         * @memberof Request
         */
        static get CONTENT_TYPES() {
            return {
                'URLENCODED': 'application/x-www-form-urlencoded',
                'JSON': 'application/json',
                'TXT': 'text/plain',
                'HTML': 'text/html',
                'ZIP': 'application/zip'
            };
        }

        /**
         * Request Methods
         *
         * @readonly
         * @static
         * @memberof Request
         */
        static get METHODS() {
            return ['post', 'get', 'put', 'delete', 'head'];
        }

        /**
         * Default Options
         *
         * @readonly
         * @static
         * @memberof Request
         */
        static get DEFAULT_OPTIONS() {
            return {
                url: null, // @var {String} remote Url
                method: 'POST', // @var {String} Request Method
                type: 'JSON', // @var String Response Content Type
                data: {},
                token: null,
                timeout: 0,
                headers: {},
                processContent: false,
                onStart: () => {},
                onSuccess: (response, jsonResponse, rawResponse) => {},
                onError: (xhr, statusText) => {},
                onFinish: (xhr, response, statusText) => {},
                onTimeout: () => {},
                onprogress: (loaded, total, percent, xhr, e) => {},
                onLoad: null // xhr onload Callback
            };
        }

        /**
         * Default Headers
         * 
         * @var {Object}
         *
         * @readonly
         * @static
         * @memberof Request
         */
        static get DEFAULT_HEADERS() {
            return {
                'X-Requested-With': 'XMLHttpRequest',
                'X-Acms': 'https://www.Amaryllis-CMS.de',
                'X-Developed-By': 'https://www.Amaryllis-Systems.de',
                'Accept': 'text/javascript, application/json, application/txt, text/html, application/xml, text/xml, */*'
            };
        }

        /**
         * Creates an instance of Request.
         * 
         * @param {Object}                          args        Custom Options
         * @param {XMLHttpRequest|ActiveXObject}    transport   Transport Object
         * 
         * @memberof Request
         */
        constructor(args, transport) {
            this._transport = transport;
            this.options = this.buildOptions(args);
            this._prepare();
        }

        /**
         * Transport instance from Constructor
         * 
         * @var {XMLHttpRequest|ActiveXObject}
         * @readonly
         * @memberof Request
         */
        get transport() {
            return this._transport;
        }

        /**
         * Remote Url
         *
         * @readonly
         * @memberof Request
         */
        get url() {
            return this._url;
        }

        /**
         * content type
         * 
         * @type {String}
         *
         * @readonly
         * @memberof Request
         */
        get contentType() {
            return Request.CONTENT_TYPES[this.type.toUpperCase()] || 'application/octet-stream';
        }

        /**
         * Request Type
         * 
         * @var {String}
         *
         * @memberof Request
         */
        get type() {
            return this._type;
        }

        set type(type) {
            if(typeof type !== 'string') throw new TypeError('Inalid Request Type');
            this._type = type;
        }

        /**
         * Checks if the given parameter is n nstance of `FormData`
         *
         * @param {FormData|Object} data
         * 
         * @returns {Boolean} `true` if `data` is a `FormData` instance
         * 
         * @memberof Request
         */
        isFormData(data) {
            if(!data) return false;
            return data.constructor.toString().match('FormData') ? true : false;
        }

        /**
         * Send the Request
         *
         * @memberof Request
         */
        send() {
            let self = this, data = self._parseData();
            window.setTimeout(() => {
                self.transport.send(data);
            }, 50);
        }

        /**
         * Build Options
         *
         * @param {Object} options
         * 
         * @returns {Object}
         * 
         * @memberof Request
         */
        buildOptions(options) {
            return Extend.flat({}, false, Request.DEFAULT_OPTIONS, options);
        }
        
        /**
         * Add a callback to module
         * 
         * This is currently not supported/used in many modules, but we're working on.
         * See modules for possible support of `evt`
         *
         * @param {String}              evt         Name of Event
         * @param {CallableFunction}    handler     Callback
         * 
         * @returns {Base} this instance
         * 
         * @example 
         * Module.on('done', () => {alert('done!')});
         * 
         * @memberof Base
         */
        on(evt, handler) {
            if(typeof this._listeners[evt] === "undefined") {
                this._listeners[evt] = [];
            }
            this._listeners[evt].push(handler);

            return this;
        }

        /**
         * remove a callback from module
         * 
         * This is currently not supported/used in many modules, but we're working on.
         * See modules for possible support of `evt`
         *
         * @param {String}              evt         Name of Event
         * @param {CallableFunction}    handler     Callback
         * 
         * @returns {Base}  Current istance
         * 
         * @example 
         * Module.off('done', () => {alert('done!')});
         * 
         * @memberof Base
         */
        off(evt, handler) {
            if(typeof this._listeners[evt] === "undefined") {
                return;
            }
            this._listeners.filter(callback => callback !== handler);

            return this;
        }

        /**
         * emit an Event
         *
         * @param {String}  evt     Event Name
         * @param {...*}    args    Arguments
         * 
         * @example 
         * Module.emit('done', response, self);
         * 
         * @memberof Base
         */
        emit(evt, ...args) {
            if(typeof this._listeners[evt] === "undefined") {
                return;
            }
            let i, cb, len = this._listeners[evt].length;
            for(i = 0; i < len; i++) {
                cb = this._listeners[evt][i];
                cb(...args);
            }

        }


        /**
         * Prepare the Request Data
         * 
         * Converts a default opbject to FormData, if needed
         *
         * @param {FormData|Object} data
         * 
         * @returns {FormData}
         * 
         * @memberof Request
         * @private
         */
        _prepareData(data) {
            data = data || {};
            if (!this.isFormData(data)) {
                let n, fData = new FormData();
                try {
                    for (n in data) {
                        if (data.hasOwnProperty(n)) {
                            fData.append(n, data[n]);
                        }
                    }
                    data = fData;
                } catch(err) {

                }
            }
            return data;
        }

        /**
         * Validate the current setup
         *
         * @memberof Request
         * 
         * @throws {Error}
         * @private
         */
        _validate() {
            let type = this.type,
                available = Request.CONTENT_TYPES;
            if (typeof(available[type]) === "undefined") {
                throw new Error('Ajax: Invalid type');
            }
            if (!this.url || typeof this.url !== "string" || !StringTool.isUrl(this.url)) {
                throw new Error('Ajax: Eine valide URL ist erforderlich');
            }
            if (Request.METHODS.indexOf(this.method) === -1) {
                throw new Error('Ajax: Methode ist invalide');
            }
        }

        /**
         * Set Request Headers
         *
         * @memberof Request
         * @private
         */
        _setHeaders() {
            let xhttp = this.transport,
                self = this;
            let custom = this.headers,
                token = this.token;
            let defaultHeaders = Request.DEFAULT_HEADERS;
            for (let n in custom) {
                if (custom.hasOwnProperty(n)) {
                    xhttp.setRequestHeader(n, custom[n]);
                }
            }
            for (let name in defaultHeaders) {
                if (defaultHeaders.hasOwnProperty(name)) {
                    xhttp.setRequestHeader(name, defaultHeaders[name]);
                }
            }
            if (token) {
                xhttp.setRequestHeader('X-CSRF-Token', token);
            }
            if (!this.isFormData(self.data)) {
                xhttp.setRequestHeader('Content-Type', this.contentType);
            }
        }

        /**
         * Bind Callback Event Handlers
         *
         * @memberof Request
         * @private
         */
        _bindEvents() {
            let self = this,
                xhr = self.transport,
                o = self.options;

            xhr.ontimeout = self.onTimeout;

            if (typeof o.onLoad === 'function') {
                xhr.onload = self.onLoad;
            }
            if(xhr.upload && typeof o.onprogress === 'function') {
                xhr.upload.onprogress = (ev) => {
                    let percent = 0;
                    if (ev.lengthComputable) {
                        percent = 100 * ev.loaded / ev.total;
                        //$("#yourprogress").width(percent + "%");
                        //or something like progress tip
                        o.onprogress(ev.loaded, ev.total, percent, xhr, ev);
                        self.emit('onprogress', ev.loaded, ev.total, percent, xhr, ev);
                    }
                };
            }
            xhr.onreadystatechange = () => {
                let type = self.type,
                    response, jsonResponse;
                if (xhr.readyState === 4) {
                    if (xhr.status.toString().match(/2[0-9]{1,2}/)) {
                        if (type.toLowerCase() === 'json') {
                            try {
                                jsonResponse = JSON.parse(xhr.responseText);
                            } catch (E) {
                                jsonResponse = { message: E.message, title: 'Fehler', _original: xhr.responseText, _xhr: xhr };
                            }
                            response = new Response(jsonResponse, xhr);
                        } else {
                            response = xhr.responseText;
                        }
                        self.emit('success', jsonResponse, xhr.responseText);
                        o.onSuccess(response, jsonResponse, xhr.responseText);
                    } else {
                        self.emit('error', xhr, xhr.statusText, xhr.status);
                        o.onError(xhr, xhr.statusText, xhr.status);
                    }
                    self.emit('finish', response, xhr.statusText, xhr);
                    o.onFinish(xhr, response, xhr.statusText);
                }
            };

        }

        /**
         * Parse Request Data
         *
         * @returns {FormData|String}
         * @memberof Request
         * @private
         */
        _parseData() {
            let self = this,
                o = this.options;
            let data = self.data;

            if (true === self.isFormData(data)) {
                return data;
            }
            if (self.type.toUpperCase() === 'JSON' && false !== o.processContent) {
                return JSON.stringify(data);
            } else if (false !== o.processContent) {
                return self._dataToURLEncoded(data);
            } else {
                return data;
            }
        }

        /**
         * Data Object to Url Encoded
         *
         * @param {Object} data
         * 
         * @returns {String}
         * 
         * @memberof Request
         * @private
         */
        _dataToURLEncoded(data) {
            
            if(!data) return '';
            let out = Object.keys(data).map(key => {
                return key + '=' + encodeURIComponent(data[key]);
            });

            return out.join('&');
        }

        /**
         * Prepare Module
         *
         * @memberof Request
         * @private
         */
        _prepare() {
            let self = this;
            let o = this.options;
            self._url = o.url;
            self.method = o.method;
            self.type = o.type;
            self.token = o.token;
            self.timeout = parseInt(o.timeout);
            self.headers = o.headers;
            self._listeners = {};
            self.data = self._prepareData(o.data);
            CALLBACKS.forEach(callback => {
                self[callback] = o[callback];
            });
            self._validate();
            if (self.type === 'blob') {
                self.transport.responseType = 'blob';
            }

            self.transport.open(self.method, self.url, true);
            self.transport.timeout = self.timeout;
            self._setHeaders();
            self._bindEvents();
            o.onStart(self.transport);
        }
    }

    return Request;
});
