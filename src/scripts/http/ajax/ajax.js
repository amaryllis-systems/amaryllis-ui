/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("http/ajax/ajax", [
    "http/ajax/request"
], (Request) => {

    /**
     * Ajax Handling
     * 
     * Einfacher Ajax Wrapper zur Erstellung und Ausführung von HTTP Requests
     * 
     * @class
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class Ajax {

        /**
         * Das Anfrage Objekt
         *
         * @var {Request}
         * @readonly
         * @static
         * @memberof Ajax
         */
        static get request()  {
            return Request;
        }

        /**
         * Erstellt einen Transport und gibt diesen zurück
         * 
         * @returns {ActiveXObject|XMLHttpRequest}
         * 
         * @memberof Ajax
         */
        static getTransport() {
            let xhttp = false;
            if (window.XMLHttpRequest) {
                xhttp = new XMLHttpRequest();
            } else {
                xhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }

            return xhttp;
        }

        /**
         * Erstellt eine Anfrage und führt diese aus
         * 
         * @param {Object} args Die Request-Argumente/Optionen
         * 
         * @returns {Request}
         * 
         * @memberof Ajax
         */
        static getRequest(args) {
            let transport = this.getTransport();
            let req = new Request(args, transport);
            return req;
        }
        
    }

    return Ajax;
});
