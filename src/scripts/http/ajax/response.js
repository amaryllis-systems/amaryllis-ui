/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
define("http/ajax/response", [], () => {

    /**
     * Response
     * 
     * Class representing a default Remote response from 
     * an Amaryllis-CMS driven Website.
     * 
     * @example
     * require(['https/request', 'notifier'], Request => {
     *  let cb = (response, jsonResponse, rawResponse) {
     *      // notify user
     *      Acms.Notifications.createFromResponse(Response);
     *      // optionaly process response.
     *      // first is the instance of this class, second
     *      // a JSON Data object of the response and last 
     *      // is the raw String representation of the response.
     *  };
     *  Request.post(remoteUrl, new FormData(), )
     * });
     *
     * @class
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class Response {

        /**
         * Creates an instance of Response.
         * 
         * @param {Object}  data            Remote response data
         * @param {String}  data.title      subject of the response
         * @param {String}  data.message    message of the response
         * @param {Number}  data.code       response code.
         * @param {('success'|'error')}     data.status 
         *                                  Text Status of the response
         * @param {XMLHttpRequest|ActiveXObject} xhr xhr Transport
         * @memberof Response
         */
        constructor(data, xhr) {
            this._code = data.code;
            this._title = data.title;
            this._message = data.message;
            this._status = data.status;
            this._xhr = xhr;
            let self = this, prop;
            for(prop in data) {
                if(!data.hasOwnProperty(prop)) {
                    continue;
                }
                if(typeof self[prop] === "undefined") {
                    self[prop] = data[prop];
                }
            }
        }

        /**
         * XHR Transport
         * 
         * @type {XMLHttpRequest|ActiveXObject}
         *
         * @readonly
         * @memberof Response
         */
        get xhr() {
            return this._xhr;
        }

        /**
         * Status Code
         * 
         * - `200` is always success, 
         * - `404` op not found
         * - `800` Not permitted
         * - > `800` Custom Response error messages
         *
         * @type {Number}
         * @readonly
         * @memberof Response
         */
        get code() {
            return this._code;
        }

        /**
         * Response Status Text
         * 
         * Simple Text representation of the response status.
         * 
         * @type {('success'|'error')}
         *
         * @readonly
         * @memberof Response
         */
        get status() {
            return this._status;
        }

        /**
         * Subject of response message
         * 
         * @type {String}
         *
         * @readonly
         * @memberof Response
         */
        get title() {
            return this._title;
        }

        /**
         * Response Message
         *
         * @readonly
         * @memberof Response
         */
        get message() {
            return this._message;
        }
    }
    
    return Response;
});
