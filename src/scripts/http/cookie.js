/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("http/cookie", [], () => {

    /**
     * HTTP Cookie Handling
     * 
     * HttpCookie is a helper class to deal with cookies in browser.
     * 
     * @class
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class HttpCookie {
        
        /**
         * Set a cookie
         * 
         * Sets the cookie and writes it to Document.
         *
         * @param {String}  name        nome of cookie
         * @param {Mixed}   value       value of cookie
         * @param {Integer} days        Lifetime in days
         * @param {String}  [path=/]    Path of cookie
         * 
         * @static
         * @memberof HttpCookie
         */
        static setCookie(name, value, days, path) {
            let cookiepath = path || '/',
                expires, date, cookie;
            if (days) {
                let date = new Date();
                date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
                expires = "; expires=" + date.toGMTString();
            } else {
                expires = "";
            }
            cookie = name + "=" + value + expires + "; path=" + cookiepath;
            if (true === Acms.General.useSSL) {
                cookie += "; secure";
            }
            this.writeCookie(cookie);
        }

        /**
         * Write a cookie
         *
         * internally used to finaly write a cookie
         * 
         * @param {String} cookie
         * 
         * @static
         * @memberof HttpCookie
         */
        static writeCookie(cookie) {
            document.cookie = cookie;
        }

        /**
         * Liest einen Cookie
         *
         * @param string name - Name des Cookie, dessen Wert ausgelesen werden soll
         *
         * @return mixed - Den Wert des Cookie
         * 
         * @static
         * @memberof HttpCookie
         */
        static readCookie(name) {
            let my_cookie_eq = name + "=",
                ca = document.cookie.split(';'),
                i = 0;
            for (let i = 0; i < ca.length; i++) {
                let c = ca[i];
                while (c.charAt(0) === ' ') {
                    c = c.substring(1, c.length);
                }
                if (c.indexOf(my_cookie_eq) === 0) {
                    return c.substring(my_cookie_eq.length, c.length);
                }
            }
            return null;
        }

        /**
         * Expires a Cookie
         *
         * @param {String}  name        Name des Cookie, der gelöscht werden soll
         * @param {String}  [path=/]    Pfad des Cookie, der gelöscht werden soll
         * 
         * @static
         * @memberof HttpCookie
         */
        static deleteCookie(name, path) {
            this.setCookie(name, "", -1, (path || '/'));
        }

        /**
         * Gibt alle Cookies zurück, die gesetzt wurden
         *
         * @return {Object}
         * 
         * @static
         * @memberof HttpCookie
         */
        static getCookies() {
            return document.cookie;
        }

        /**
         * test if cookies are enabled in browser
         * 
         * @returns {Boolean}
         * 
         * @static
         * @memberof HttpCookie
         */
        static isSupported() {
            let cookieEnabled = (navigator.cookieEnabled) ? true : false;
            if (typeof navigator.cookieEnabled === "undefined" && !cookieEnabled) {
                let date = new Date(), cookie;
                date.setTime(date.getTime() + (30 * 60 * 1000));
                cookie = "testcookie=1; expires=" + date.toGMTString() + '; path=/';
                if (true === Acms.General.useSSL) {
                    cookie += "; secure";
                }
                try {
                    this.writeCookie(cookie);
                    cookieEnabled = (null === this.readCookie('testcookie')) ? false : true;
                    this.deleteCookie('testcookie');
                } 
                // fixes a possible Security Exception of document
                catch(e) {
                    cookieEnabled = false;
                }
            }
            return cookieEnabled;
        }
    }

    return HttpCookie;
});
