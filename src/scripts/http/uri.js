/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('http/uri', [], () => {

    /**
     * Uri
     * 
     * Helper Class around Uri
     *
     * @class
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class Uri {

        /**
         * Uri factory
         *
         * @static
         * 
         * @param {String}          url     URL
         * @param {Object}          props   Anchor Properties wie title, rel, id
         * @param {String|Array}    cls     CSS Class Name(s)
         * 
         * @returns {Uri} New instance
         * 
         * @memberof Uri
         */
        static factory(url, props, cls) {
            let U = new Uri(url);
            let a = Uri.anchor;
            for (let prop in props) {
                if (props.hasOwnProperty(prop)) {
                    a.setAttribute(prop, props[prop]);
                }
            }
            if(typeof cls === "string") {
                a.classList.add(cls);
            }
            
            return U;
        }

        /**
         * Creates an instance of Uri.
         * 
         * @param {String} url  URL
         * 
         * @memberof Uri
         */
        constructor(url) {
            this.url =
            this.hostname =
            this.host =
            this.scheme =
            this.pathname =
            this.hash =
            this.port =
            this._anchor = null;
            this.url = url;
            this._anchor = document.createElement('a');
            this._anchor.href = url;
            this._parse();
        }
        
        /**
         * Anchor Element
         *
         * @readonly
         * @memberof Uri
         * @type {HTMLAnchorElement}
         */
        get anchor() {
            return this._anchor;
        }

        /**
         * Ist die Url eine Lokale Url?
         * 
         * @returns {Boolean}   `TRUE` wenn es eine lokale Url ist
         * @memberof Uri
         */
        isLocal() {
            return this.hostname === window.location.hostname;
        }

        /**
         * Host Getter
         * 
         * @returns {String} Host
         */
        getHost() {
            return this.anchor.host;
        }

        /**
         * Hash Getter
         * 
         * @returns {String} Hash
         */
        getHash() {
            return this.hash;
        }

        /**
         * Hostname Getter
         * 
         * @returns {String} Hostname
         */
        getHostName() {
            return this.hostname;
        }

        /**
         * Scheme Getter
         * 
         * @returns {String}    Scheme
         */
        getScheme() {
            return this.scheme;
        }

        /**
         * Anchor Getter
         * 
         * @returns {HTMLAnchorElement} Den Anchor
         */
        getAnchor() {
            return this.anchor;
        }

        /**
         * Getter für alle geparsten Bestandteile.
         * 
         * @returns {Object}    Parsed Url Parts
         */
        getParsed() {
            return {
                host: this.host,
                url: this.url,
                scheme: this.scheme,
                hostname: this.hostname,
                port: this.port,
                username: this.username,
                password: this.password,
                origin: this.origin,
                hash: this.hash
            };
        }

        /**
         * Parsen der Url aus dem Constructor
         * 
         * @memberof Uri
         * 
         * @private
         */
        _parse() {
            this.hostname = this.anchor.hostname;
            this.scheme = this.anchor.protocol;
            this.pathname = this.anchor.pathname;
            this.hash = this.anchor.hash;
            this.host = this.anchor.host;
            this.port = this.anchor.port;
            this.password = this.anchor.password;
            this.origin = this.anchor.origin;
            this.username = this.anchor.username;
        }

    }

    return Uri;

});
