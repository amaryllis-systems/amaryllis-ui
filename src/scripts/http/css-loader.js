/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define('http/css-loader', ['tools/string/camelcase', 'core/acms'], (CamelCase, A) => {

    let doc = document || window.document;

    let sheets = {};
    for (let i in doc.styleSheets) {
        if (doc.styleSheets[i].href) {
            sheets[doc.styleSheets[i].href] = doc.styleSheets[i].href;
        }
    }

    let themecss = doc.body.getAttribute('data-themecss');
    if(null !== themecss) {
        if(themecss === 'true' || parseInt(themecss, 10) === 1) {
            themecss = true;
        } else if(themecss === 'false') {
            themecss = false;
        }
        themecss = (typeof themecss === 'string' ? themecss : (true === themecss && typeof A.Theme !== 'undefined' ? A.Theme.foldername : false));
    }
    let rootColors = null;
    
    /**
     * CSS loader
     * 
     * Loads Stylesheets in theme and js modules with custom styles
     *
     * @class
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class CssLoader {

        /**
         * Sheets Cache
         * 
         * Holds all Styles added manually to head and injected through CSS Loader
         *
         * @readonly
         * @static
         * @memberof CssLoader
         */
        static get sheets() {
            return sheets;
        }

        /**
         * Load CSS Files from theme?
         * 
         * Checks the global themecss setting. If `body`
         * has the attribute `data-themecss` this might 
         * contain 
         * 
         * - `true` , to turn on for the current 
         *  theme.
         * - `[String]` Foldername of the theme
         * - `false` to use default style loader (defaults to)
         * 
         * !!! note Note
         * If this setting is set to true, any style sheet provided 
         * by Core and modules is loaded from the theme folder. In 
         * JavaScript, we can't check first to see if the file exists. 
         * If you create the topic with gulp in this repository, make 
         * sure you make copies of any module and core style sheets!
         * !!!
         * 
         * @type {String|Boolean}
         *
         * @readonly
         * @static
         * @memberof CssLoader
         */
        static get themecss() {
            return themecss;
        }

        /**
         * root colors
         * 
         * This property actually reads all CSS-vars set 
         * by `--colors-*`. Color specifications are not 
         * further tested. Colors found depend on the 
         * layout.
         * 
         * ``` css
         * :root {
         *  --colors-components-primary: '#0050ef';
         *  --colors-components-secondary: '#4b626d';
         *  --colors-shades-black: '#000000'
         *  ...
         * }
         * ```
         * 
         * gets turned to:
         * 
         * ``` js
         * {
         *  colors: {
         *      components: {
         *          primary: {
         *              value: '#0050ef'
         *          },
         *          secondary: {
         *              value: '#4b626d'
         *          }
         *       },
         *       shades: {
         *          black: {
         *              value: '#000000'
         *          }
         *       }
         *  }
         * }
         * ```
         * 
         * @type {Array}
         *
         * @readonly
         * @static
         * @memberof CssLoader
         * @since 3.5.0
         */
        static get rootColors() {
            if(null === rootColors) {
                rootColors = {};
                this._loadRootColors();
            }
            return rootColors;
        }

        /**
         * loading CSS Style Sheets
         * 
         * The passed value can be an array of style 
         * sheets or a single style sheet as a string.
         * 
         * @example
         * let urls1 = 'media/css/components/panel'
         * CssLoader.load(urls1);
         * let urls2 = [
         *     'media/css/components/panel',
         *     'media/css/components/alert',
         *     'media/css/components/buttons'
         * ];
         * CssLoader.load(urls2);
         * 
         * @static
         * @param {String|Array} urls  Eine URL oder ein Array an Urls
         * 
         * 
         * 
         * @memberof CssLoader
         */
        static load(urls, theme) {
            if (doc === null) {
                throw new DOMError('document is not defined');
            }
            theme = (typeof theme === 'string' ? theme : (true === theme && typeof A.Theme !== 'undefined' ? A.Theme.foldername : this.themecss));

            if(typeof urls === 'string') {
                urls = {
                    1: {
                        url: urls,
                        theme: theme
                    }
                };
            }
            if (urls instanceof Array) {
                urls.forEach((url) => {
                    CssLoader._doLoad(url, theme);
                });
            } else if (typeof urls === "object" && null !== urls) {
                for (let prop in urls) {
                    if(urls.hasOwnProperty(prop))
                    CssLoader._doLoad(urls[prop].url, urls[prop].theme);
                }
            }
        }


        /**
         * load a single style sheet
         * 
         * @static
         * @access private
         * @param {String} url 
         * 
         * 
         * @memberof CssLoader
         * @private
         */
        static _doLoad(url, theme) {
            if(url.substring(0, 4) !== 'http') {
                if(url.substring(0, 1) !== '/') {
                    url = '/' + url;
                }
                if(typeof theme === 'string' && url.indexOf('/libs/') === -1) {
                    url = '/themes/' + theme + url;
                }
                url = A.Urls.baseUrl + url;
            }
            if (url.substr(-4) !== '.css') {
                url += (Acms.General.debug === true ? '.css' : '.min.css');
            }
            //console.log(url);
            if (typeof sheets[url] !== "undefined" && sheets.hasOwnProperty(url)) {
                return;
            }
            sheets[url] = url;
            let link = doc.createElement("link");
            link.type = "text/css";
            link.rel = "stylesheet";
            link.href = url;
            doc.getElementsByTagName("head")[0].appendChild(link);
        }

        /**
         * Load css root colors
         *
         * @static
         * @param {HTMLElement} [element=document.body]     element
         * @param {String}      [pseudo=null]               Pseudo name
         * 
         * @memberof CssLoader
         * @since 3.5.0
         * @private
         */
        static _loadRootColors(element = document.body, pseudo = null) {
           let vars = this._extractAllCSSVariableNames(), i;
           
            const deepCopy = (o, keys, val) => {
                let k;
                k = keys[0];
                keys.shift();
                if(typeof o[k] === 'undefined') {
                    o[k] = {};
                }
                if(keys.length === 0) {
                    o[k].value = val;
                } else {
                    o[k] = deepCopy(o[k], keys, val);
                }

                return o;
            };
            let elStyles = window.getComputedStyle(element, pseudo);
            for(i = 0; i < vars.length; i++){
                let key = vars[i];
                let value = elStyles.getPropertyValue(key);
                if(value){
                    key = key.replace('--', '');
                    let keys = key.split('-'), ct = keys.length, a, b;
                    // do not support deep list now... we just need the default color list!
                    if(ct > 1) {
                        deepCopy(rootColors, keys, value);
                    } else {
                        if(typeof rootColors[key] === "undefined") {
                            rootColors[key] = {};
                        }
                        rootColors[key].value = value;
                    }
                }
            }
            
        }

        /**
         * Extracts all css variable names from current stylesheets
         *
         * @static
         * @returns {Object}
         * 
         * @memberof CssLoader
         * @since 3.5.0
         * @private
         */
        static _extractAllCSSVariableNames() {
            let styleSheets = document.styleSheets;
            let cssVars = [], i, j, k, name;
            for(i = 0; i < styleSheets.length; i++) {
               try {
                  for(j = 0; j < styleSheets[i].cssRules.length; j++){
                     try{
                        for(k = 0; k < styleSheets[i].cssRules[j].style.length; k++){
                           name = styleSheets[i].cssRules[j].style[k];
                           if(true === name.startsWith('--') && cssVars.indexOf(name) === -1 && cssVars.indexOf('colors') !== -1){
                              cssVars.push(name);
                           }
                        }
                     } catch (error) {}
                  }
               } catch (error) {}
            }
            return cssVars;
        }
    }
    
    return CssLoader;
});
