/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("http/module-loader", [
    "http/css-loader",
    "core/selectors",
    "tools/utils",
    "core/window",
    "core/acms",
    "core/logger"
], (CssLoader, Selectors, Utils, Win, A, Logger) => {

    /**
     * Module Loader
     * 
     * Loading JS Modules on the fly using data-attributes
     *
     * @class
     * @author qm-b <https://bitbucket.org/qm-b/>
     */
    class ModuleLoader {

        /**
         * Loads Modules and Styles
         *
         * @param {HTMLElement} element The base Element to search in. If omitted the document will be used.
         * 
         * @static 
         * @memberof ModuleLoader
         */
        static load(element) {
            this.loadCSS(element);
            try {
            this.loadModules(element);
            } catch(E) {
                console.log(E.message)
            }
        }

        /**
         * Loads Stylesheetes 
         * 
         * Searchs in `element` for "data-cssfile" Attributes and loads style-sheets.
         * 
         * Additional parameters:
         * 
         * - "data-themecss": expected to be true or string with theme foldername
         *
         * @static
         * @param {HTMLElement} element The base Element to search in. If omitted the document will be used.
         * @memberof ModuleLoader
         */
        static loadCSS(element) {
            element = element ||Win.document;
            Selectors.qa('[data-cssfile]', element).forEach(function(file) {
                let css = file.getAttribute("data-cssfile");
                let themecss = file.getAttribute("data-themecss");
                if(typeof themecss === 'string') {
                    themecss = (themecss.trim() === 'true' ? true : (themecss.trim() === 'false' ? false : themecss));
                }
                CssLoader.load(css, themecss);
            });
        }

        /**
         * Loads Modules
         * 
         * Searchs in `element` for "data-module" Attributes and tries to load the Module.
         * Additional Parameters:
         * 
         * - "data-module-parameters":  Expected is a JSON String with custom Module Options.
         * - "data-parameters-module":  Expected is a module require like `path/to/options`. The
         *                              required Module Options **MUST** return an Object of
         *                              custom Module Options.
         * 
         * ``` html
         * <button class="small primary button" type="button" data-module="apps/ui/informations/modal" data-target="#my-modal">Launch Modal</button>
         * ```
         *
         * @param {HTMLElement} element The base Element to search in. If omitted the document will be used.
         * 
         * @static
         * @memberof ModuleLoader
         */
        static loadModules(element) {
            element = element ||Win.document;
            Selectors.qa('[data-module]', element).forEach((item) => {
                
                let fmodule = item.getAttribute("data-module"),
                    parameters = item.getAttribute("data-module-parameters", false),
                    parametersModule = item.getAttribute("data-parameters-module", false),
                    dependencies = [fmodule],
                    params;
                if (parameters) {
                    try {
                        params = JSON.parse(parameters);
                    } catch (E) {
                        let msg = "parsing of module parameters has been failed: " + E.message;
                        A.Logger.logWarning(msg);
                    } finally {
                        parameters = params || {};
                    }
                }
                if(parametersModule) {
                    dependencies.push(paprametersModule);
                } 
            
                require(dependencies, function(mod, opts) {
                    try {
                        if (typeof mod === "undefined" || !mod) {
                            A.Logger.logWarning('Modul ' + fmodule + ' could not be found!');
                            return;
                        }
                        let i, options;
                        if(typeof opts !== "undefined") {
                            options = Utils.extend({}, parameters, opts);
                        } else {
                            options = Utils.extend({}, parameters);
                        }
                        if (mod.needInit) {
                            i = mod.init(item, options);
                        }
                        if (i && "options" in i && "css" in i.options) {
                            CssLoader.load(i.options.css, i.options.themecss);
                        }
                        if (mod.MODULE) {
                            A.Logger.writeLog('Loaded Module "<b>' + mod.MODULE + '</b>" (<i>' + (mod.NS ? mod.NS : fmodule) + '</i>).');
                        } else {
                            A.Logger.writeLog('Loaded Module "<b>' + fmodule + '</b>"');
                        }
                    } catch(E) {
                        A.Logger.writeLog("lazy loading module " + fmodule + " failed! " + E.message);
                        console.log(E.stack);
                    }
                });
            
            });
            
        }
    }


    return ModuleLoader;
});
