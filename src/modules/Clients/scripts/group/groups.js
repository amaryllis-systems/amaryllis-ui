/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("clients-module/group/groups", [
	"core/base",
	"core/classes",
	"core/selectors",
	"events/event",
	"templates/template-engine",
	"core/acms"
], (Base, Classes, Selectors, AcmsEvent, TemplateEngine, A) => {

	/**
	 * Client Groups
	 * 
	 * Class responsible for handling client groups
	 *
	 * @class ClientGroups
	 * @extends {Base}
	 */
	class ClientGroups extends Base {

		/**
		 * Module Namespace
		 *
		 * @readonly
		 * @static
		 * @memberof ClientGroups
		 */
		static get NS() {
			return "clients-module.group.groups";
		}


		/**
		 * Module Name
		 *
		 * @readonly
		 * @static
		 * @memberof ClientGroups
		 */
		static get MODULE() {
			return "Client Groups";
		}

		/**
		 * Default Options
		 *
		 * @readonly
		 * @static
		 * @memberof ClientGroups
		 */
		static get DEFAULT_OPTIONS() {
			return {
				remote: null, // {String} remote url
				limit: 50, // {Integer} Limit of Groups
				offset: 0, // {Integer} Initial Offset
				order: 'title', // {String} `title`, `name` or `pdate`
				sort: 'ASC', // {String} `ASC`, `DESC` or `RAND`
				classNames: {

				},
				selectors: {
					groups: {
						list: '[data-role="group-list"]', // wrapper around single groups,
						form: '[data-role="group-form"]', // group form in hbs template
						title: '#group-form-title', // group form title input
						name: '#group-form-name', // group form title input
						desc: '#group-form-description', // group form title input
						add: '[data-role="group-add"]', // wrapper around single groups,
					},
					trigger: {
						load: '[data-role="load-groups"]',
						refresh: '[data-role="refresh-groups"]'
					}
				},
				actions: {
					groups: {
						load: 'loadgroups',
						add: 'addgroup',
						update: 'updategroup',
						remove: 'deletegroup'
					},
				},
				fieldMap: {
					op: 'op'
				},
				templates: {
					groups: {
						group: 'clients-module/templates/groups.hbs'
					}
				},

				onGroupsLoaded: (groupList, groups, self) => {}
			};
		}

		/**
		 * Module Init
		 *
		 * @static
		 * 
		 * @param {HTMLElement} element		Wrapper Element
		 * @param {Object|null} options		optional custom options
		 * 
		 * @returns {ClientGroups}	New Instance
		 * 
		 * @memberof ClientGroups
		 */
		static init(element, options) {
			let m = new ClientGroups(element, options);
			m.element.acmsData(ClientGroups.NS, m);
			
			return m;
		}

		/**
		 * Creates an instance of ClientGroups.
		 * 
		 * @param {HTMLElement} element		Wrapper Element
		 * @param {Object|null} options		optional custom options
		 * 
		 * @memberof ClientGroups
		 */
		constructor(element, options) {
			super(element, options);
			this._initialize();
			this.refresh();
		}

		/**
		 * Request Limit
		 * 
		 * @type {Number}
		 *
		 * @memberof ClientGroups
		 */
		get limit() {
			return this._limit;
		}
		set limit(limit) {
			this._limit = parseInt(limit);
		}

		/**
		 * Remote offset
		 * 
		 * @type {Number}
		 * 
		 * @memberof ClientGroups
		 */
		get offset() {
			return this._offset;
		}
		set offset(offset) {
			this._offset = parseInt(offset);
		}

		/**
		 * Get order by column (e.g. `title`, `name` or `pdate`)
		 * 
		 * @type {String}
		 *
		 * @memberof ClientGroups
		 */
		get order() {
			return this._order;
		}

		set order(order) {
			this._order = order;
		}

		/**
		 * Sort (ascending, descending, random)
		 * 
		 * Sort might be `ASC`, `DESC` or `RAND`
		 * 
		 * @type {String}
		 *
		 * @memberof ClientGroups
		 */
		get sort() {
			return this._sort;
		}
		set sort(sort) {
			let s = sort.toUpperCase().trim();
			if(s !== 'ASC' && s !== 'DESC' && s !== 'RAND') {
				throw new Error('Invalid Sort');
			}
			this._sort = sort;
		}

		/**
		 * Total Groups
		 * 
		 * @type {Number}
		 * 
		 * @memberof ClientGroups
		 */
		get total() {
			return this._total;
		}

		set total(total) {
			this._total = parseInt(total);
		}

		/**
		 * Is loading Groups?
		 * 
		 * @type {Boolean}
		 *
		 * @readonly
		 * @memberof ClientGroups
		 */
		get loading() {
			return (this._loading === true);
		}

		/**
		 * Refresh Groups
		 *
		 * @memberof ClientGroups
		 */
		refresh() {
			let self = this,
				o = self.options,
				fData = new FormData();
			if(self._loading) {
				return;
			}
			self.offset = 0;
			self.groups = [];
			self.enableLoadTrigger();
			self.load();
		}

		/**
		 * Refresh Groups
		 *
		 * @memberof ClientGroups
		 */
		load() {
			let self = this,
				o = self.options,
				fData = new FormData();
			if(self.loading) {
				return;
			}
			self._loading = true;
			fData.append(o.fieldMap.op, o.actions.groups.load);
			fData.append('limit', self.limit);
			fData.append('offset', self.offset);
			self._processed = 0;
			require(['http/request', 'notifier'], (Request) => {
				Request.post(o.remote, fData, self._onLoadedGroups, self._fail, self._always);
			});
		}

		enableLoadTrigger() {
			let self = this,
				o = self.options,
				element = self.element,
				sel = o.selectors,
				triggers = Selectors.qa(sel.trigger.load, element);
			require(['apps/ui/helpers/fade'], (Fade) => {
				triggers.forEach(btn => {
					Fade.fadeIn(btn);
					btn.removeAttribute('disabled');
				});
			});
			
		}

		disableLoadTrigger() {
			let self = this,
				o = self.options,
				element = self.element,
				sel = o.selectors,
				triggers = Selectors.qa(sel.trigger.load, element);
			require(['apps/ui/helpers/fade'], (Fade) => {
				triggers.forEach(btn => {
					Fade.fadeOut(btn);
					btn.setAttribute('disabled', 'disabled');
				});
			});
		}


		listen() {
			let self = this,
				o = self.options,
				element = self.element,
				sel = o.selectors;
			AcmsEvent.on(element, 'click', sel.trigger.load, self._onClickLoad, true);
			AcmsEvent.on(element, 'click', sel.trigger.refresh, self._onClickRefresh, true);
			AcmsEvent.on(element, 'click', sel.groups.add, self._onClickCreateGroup, true);
		}

		/**
		 * On Click Load Trigger Event Handler
		 *
		 * @param {Event} e		Click Event
		 * @memberof ClientGroups
		 */
		_onClickLoad(e) {
			e.preventDefault();
			e.stopPropagation();

			self.load();
		}

		/**
		 * On Click Refresh Trigger Event Handler
		 *
		 * @param {Event} e		Click Event
		 * @memberof ClientGroups
		 */
		_onClickRefresh(e) {
			e.preventDefault();
			e.stopPropagation();

			self.refresh();
		}

		_onClickCreateGroup(e) {
			let self = this,
				o = self.options,
				template = o.templates.groups.form,
				tdata = {
					group: {
						id: 0,
						title: '',
						name: '',
						description: ''
					},
					form: {
						op: o.actions.groups.add
					}
				};
			TemplateEngine.compileResource(template, tdata, self._onGroupFormAppended, self._groupList);
		}

		_onGroupFormAppended() {

		}

		_onClickSubmitGroupForm() {

		}

		_onClickAbortGroupForm() {

		}

		/**
		 * On Loaded Groups Callback
		 *
		 * @param {Object} response		Remote Response Object
		 * 
		 * @memberof ClientGroups
		 */
		_onLoadedGroups(response) {
			if(response.status !== 'success') {
				return A.Notifications.createFromResponse(response);
			}
			let self = this,
				o = self.options, j,
				groups = response.groups,
				total = response.total;
			self._response = response;
			self._groupList.innerHTML = '';
			self.total = total;
			if(self.total === 0 || !groups.length) {
				return;
			}
			for(j = 0; j < groups.length; j++) {
				self._makeGroup(groups[j]);
			}
			
		}

		/**
		 * Makes a Group Element from Remote Data of one Group
		 *
		 * @param {Object} data		Remote Data of Group
		 * 
		 * @memberof ClientGroups
		 */
		_makeGroup(data) {
			let self = this,
				o = self.options,
				template = o.templates.groups.group,
				tdata = {
					group: data
				};
			self.groups[data.id] = data;
			TemplateEngine.compileResource(template, tdata, self._onGroupAdded, self._groupList);
		}

		/**
		 * On Group appended/rendered Callback
		 *
		 * @memberof ClientGroups
		 */
		_onGroupAdded() {
			let self = this,
				o = self.options;
			self._processed++;
			if(self._processed !== self._response.groups.length) {
				return;
			}
			self.offset = (self.offset + self.limit + 1);
			if(self.offset >= self.total) {
				self.disableLoadTrigger();
			}
			self._loading = false;
			o.onGroupsLoaded(self._groupList, self.groups, self);

		}

		/**
		 * On XHR Fail Callback
		 *
		 * @param {Object} 		xhr				XHR Request
		 * @param {string} 		textStatus		XHR Status Message
		 * 
		 * @memberof ClientGroups
		 */
		_fail(xhr, textStatus) {
            A.Logger.writeLog(textStatus);
		}
		

        /**
		 * On Always XHR Callback
		 *
		 * @param {String} 			textStatus		XHR Status Message
		 * 
		 * @memberof ClientGroups
		 */
		_always(textStatus) {
            A.Logger.writeLog(textStatus);
		}
		
		/**
		 * Prepare Module
		 *
		 * @memberof ClientGroups
		 */
		_prepare() {
			let self = this,
				o = self.options,
				element = self.element,
				sel = o.selectors;
			self.groups = [];
			self.limit = o.limit;
			self.offset = o.offset;
			self.sort = o.sort || 'ASC';
			self.order = o.order || 'title';
			self.total = 0;
			self._groupList = Selectors.q(sel.groups.list, element);
		}

	}

	return ClientGroups;
});
