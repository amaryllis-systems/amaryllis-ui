/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("clients-module/view/clients-inputsearch", [
    "core/base",
    "core/selectors",
    "ui/components/inputsearch",
    "events/event",
    "core/acms"
], (Base, Selectors, InputSearch, AcmsEvent, A) => {

	/**
	 * Clients Live Search for Input-Search Element
	 *
	 * @class ClientsLiveInputSearch
	 * @extends {Base}
	 */
	class ClientsLiveInputSearch extends Base {

		/**
		 * Module Namespace
		 *
		 * @readonly
		 * @static
		 * @memberof ClientsLiveInputSearch
		 */
		static get NS() {
			return "clients-module.view.clients-search";
		}

		/**
		 * Module Name
		 *
		 * @readonly
		 * @static
		 * @memberof ClientsLiveInputSearch
		 */
		static get MODULE() {
			return "Clients Live Input-Search";
		}

		/**
		 * Default Options
		 *
		 * @readonly
		 * @static
		 * @memberof ClientsLiveInputSearch
		 */
		static get DEFAULT_OPTIONS() {
			return {
				remote: null,
				css: ClientsLiveInputSearch.css,
				themecss: false
			};
		}

		/**
		 * Module init
		 *
		 * @static
		 * 
		 * @param {HTMLElement} element
		 * @param {Object|null} options
		 * 
		 * @returns {ClientsLiveInputSearch}	New Instance
		 * 
		 * @memberof ClientsLiveInputSearch
		 */
		static init(element, options) {
			let m = new ClientsLiveInputSearch(element, options);
			m.element.acmsData(ClientsLiveInputSearch.NS, m);

			return m;
		}

		/**
		 * Creates an instance of ClientsLiveInputSearch.
		 * 
		 * @param {HTMLElement} element
		 * @param {Object|null} options
		 * 
		 * @memberof ClientsLiveInputSearch
		 */
		constructor(element, options) {
			super(element, options);
			this._initialize();
		}

		/**
		 * Prepare Module
		 *
		 * @memberof ClientsLiveInputSearch
		 */
		_prepare() {
			let self = this,
                    element = self.element,
                    o = self.options;
            if(element.tagName.toLowerCase() !== 'input') {
                self.field = Selectors.q('input[type=search]', element);
            } else {
                self.field = element;
            }
            let AutoCompleteOptions = {
                minChars: 3,
                remote: o.remote
            }, InputSearchOptions = {
                limit: 1,
                allowEditing: false,
                allowPasting: false,
                allowCreating: false,
                autocomplete: AutoCompleteOptions,
                autocompleteData: {}
            };
            InputSearch.init(self.field, InputSearchOptions);
		}
	}

    return ClientsLiveInputSearch;

});
