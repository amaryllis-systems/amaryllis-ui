/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("clients-module/view/clients-search", [
    "core/base",
    "core/selectors",
    "apps/searching/autocomplete",
    "events/event",
    "core/acms"
], (Base, Selectors, AutoComplete, AcmsEvent, A) => {

    /**
     * Clients Live Search
     *
     * @class ClientsLiveSearch
     * @extends {Base}
     */
    class ClientsLiveSearch extends Base {

        /**
         * Module Namespace
         *
         * @readonly
         * @static
         * @memberof ClientsLiveSearch
         */
        static get NS() {
            return "clients-module.view.clients-search";
        }

        /**
         * Module Name
         *
         * @readonly
         * @static
         * @memberof ClientsLiveSearch
         */
        static get MODULE() {
            return "Clients Live-Search";
        }

        /**
         * Custom Stylesheet from AutoComplete
         *
         * @readonly
         * @static
         * @memberof ClientsLiveSearch
         */
        static get css() {
            return AutoComplete.css;
        }

        /**
         * Default Options
         *
         * @readonly
         * @static
         * @memberof ClientsLiveSearch
         */
        static get DEFAULT_OPTIONS() {
            return {
                remote: null, // {String} remote Url
                autocomplete: {}, // {object} Autocomplete options,
                css: ClientsLiveSearch.css,
                themecss: false
            };
        }

        /**
         * Module Init
         *
         * @static
         * 
         * @param {HTMLElement} element Live Search Element
         * @param {Object|null} options Optional custom options
         * 
         * @returns {ClientsLiveSearch} New instance
         * 
         * @memberof ClientsLiveSearch
         */
        static init(element, options) {
            let m = new ClientsLiveSearch(element, options);
            m.element.acmsData(ClientsLiveSearch.NS, m);

            return m;
        }

        /**
         * Creates an instance of ClientsLiveSearch.
         * 
         * @param {HTMLElement} element Live Search Element
         * @param {Object|null} options Optional custom options
         * 
         * @memberof ClientsLiveSearch
         */
        constructor(element, options) {
            super(element, options);
            this._initialize();
        }

        /**
         * Prepare Module
         *
         * @memberof ClientsLiveSearch
         */
        _prepare() {
            let self = this,
                element = self.element,
                o = self.options;
            if (element.tagName.toLowerCase() !== 'input') {
                self.field = Selectors.q('input[type=search]', element);
            } else {
                self.field = element;
            }

            self._buildAutoComplete();
        }

        /**
         * Create AutoComplete instance
         *
         * @memberof ClientsLiveSearch
         */
        _buildAutoComplete() {
            let self = this,
                o = self.options;
            let AutoCompleteOptions = {
                minChars: 3,
                remote: o.remote,
                onSelect: function (e, value, label, item) {
                    e.preventDefault();
                    window.location.href = value;

                    return false;
                }
            };

            if(typeof o.autocomplete === 'object' && null !== o.autocomplete) {
                for(let prop in o.autocomplete) {
                    if(o.autocomplete.hasOwnProperty(prop) && typeof AutoCompleteOptions[prop] === "undefined") {
                        AutoCompleteOptions[prop] = o.autocomplete[prop];
                    }
                }
            }

            AutoComplete.init(self.field, AutoCompleteOptions);
        }

    }

    return ClientsLiveSearch;
});
