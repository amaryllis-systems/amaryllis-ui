/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("clients-module/profile/profile-field", [
    "core/base",
    "core/selectors",
    "events/event",
    "core/acms"
], (Base, Selectors, AcmsEvent, A) => {

	/**
	 * Clients Profile Field
	 *
	 * @class ClientsProfileField
	 * @extends {Base}
	 */
	class ClientsProfileField extends Base {

		
		/**
		 * Module Namespace
		 *
		 * @readonly
		 * @static
		 * @memberof ClientsProfileField
		 */
		static get NS() {
			return "clients-module.profile.profile-field";
		}

		/**
		 * Module Name
		 *
		 * @readonly
		 * @static
		 * @memberof ClientsProfileField
		 */
		static get MODULE() {
			return "Clients Profile Field";
		}

		/**
		 * Default Options
		 *
		 * @readonly
		 * @static
		 * @memberof ClientsProfileField
		 */
		static get DEFAULT_OPTIONS() {
			return {
				id: null,
        		remote: null
			};
		}

		/**
		 * Module init
		 *
		 * @static
		 * 
		 * @param {HTMLElement} element
		 * @param {Object|null} options
		 * 
		 * @returns {ClientsProfileField}	New Instance
		 * 
		 * @memberof ClientsProfileField
		 */
		static init(element, options) {
			let m = new ClientsProfileField(element, options);
			m.element.acmsData(ClientsProfileField.NS, m);

			return m;
		}

		/**
		 * Creates an instance of ClientsProfileField.
		 * 
		 * @param {HTMLElement} element
		 * @param {Object|null} options
		 * 
		 * @memberof ClientsProfileField
		 */
		constructor(element, options) {
			super(element, options);
			this._initialize();
		}

		/**
		 * Setup Event Listener
		 *
		 * @memberof ClientsProfileField
		 */
		listen() {
			let self = this,
                    o = self.options,
                    element = self.element;
            self.fields.forEach((field) => {
                AcmsEvent.add(field, "change", self._onChange);
            });
		}

		/**
		 * On Change Field Event Handler
		 *
		 * @param {Event} e		Change Event
		 * 
		 * @memberof ClientsProfileField
		 */
		_onChange(e) {
			let self = this,
                field = e.target,
                value = field.value,
                o = self.options,
                fData = new FormData();
            fData.append("field", field.name);
            fData.append("value", value);
            fData.append("id", o.id);
            fData.append("op", "update");
            self.currentField = field;
            require(["http/request", "notifier"], function(Request) {
                Request.post(o.remote, fData, self._done, self._fail, self._always);
            });
		}

		/**
		 * On Remote Done Callback
		 * 
		 * @param {Object} response		Remote Response Data
		 * 
		 * @memberof ClientsProfileField
		 */
		_done(response) {
			let self = this, field = self.currentField;
            if(response.status !== "success") {
                A.Notifications.createFromResponse(response);
                field.value = field.acmsData("value");
                
            } else {
                field.acmsData("value", field.value);
            }
		}

		/**
		 * On XHR Fail Callback
		 *
		 * @param {Object} 		xhr				XHR Request
		 * @param {string} 		textStatus		XHR Status Message
		 * 
		 * @memberof ClientsProfileField
		 */
		_fail(xhr, textStatus) {
            A.Logger.writeLog(textStatus);
		}
		

        /**
		 * On Always XHR Callback
		 *
		 * @param {String} 			textStatus		XHR Status Message
		 * 
		 * @memberof ClientsProfileField
		 */
		_always(textStatus) {
            A.Logger.writeLog(textStatus);
		}
		
		/**
		 * Prepare Module
		 *
		 * @memberof ClientsProfileField
		 */
		_prepare() {
			let self = this,
                element = self.element,
                o = self.options,
                fields = Selectors.qa("input, select, textarea", element);
            fields.forEach((field) => {
                field.acmsData("value", field.value);
            });
            self.fields = fields;
		}

	}

    return ClientsProfileField;

});
