/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("clients-module/admin/clients-initializer", [
	"core/base",
	"core/selectors",
	"events/event",
	"core/acms"
], (Base, Selectors, AcmsEvent, A) => {

	/**
	 * Clients Admin Initializer
	 * 
	 * First-Time init of the module.
	 *
	 * @class ClientsInitializer
	 * @extends {Base}
	 */
	class ClientsInitializer extends Base {

		/**
		 * Module Namespace
		 *
		 * @readonly
		 * @static
		 * @memberof ClientsInitializer
		 */
		static get NS() {
			return "clients-module.admin.clients-initializer";
		}

		/**
		 * Module Name
		 *
		 * @readonly
		 * @static
		 * @memberof ClientsInitializer
		 */
		static get MODULE() {
			return "Clients Admin Initializer";
		}

		/**
		 * Default Options
		 *
		 * @readonly
		 * @static
		 * @memberof ClientsInitializer
		 */
		static get DEFAULT_OPTIONS() {
			return {
				remote: null,
				onFinish: (response) => {},
				initnotice: '#clients-init'
			};
		}

		/**
		 * Module Init
		 *
		 * @static
		 * 
		 * @param {HTMLElement} element	Button trigger
		 * @param {Object|null} options optional custom options
		 * 
		 * @returns	{ClientsInitializer}	New instance
		 * 
		 * @memberof ClientsInitializer
		 */
		static init(element, options) {
			let m = new ClientsInitializer(element, options);
			m.element.acmsData(ClientsInitializer.NS, m);
			
			return m;
		}

		/**
		 * Creates an instance of ClientsInitializer.
		 * 
		 * @param {HTMLElement} element	Button trigger
		 * @param {Object|null} options optional custom options
		 * 
		 * @memberof ClientsInitializer
		 */
		constructor(element, options) {
			super(element, options);
			this._initialize();

		}

		/**
		 * Setup Event Listener
		 *
		 * @memberof ClientsInitializer
		 */
		listen() {
            let self = this;
            AcmsEvent.add(self.element, 'click', self._onClick);
		}
		
		_onClick(e) {
			let self = this, o = self.options, fData = new FormData;
			e.stopPropagation();
			e.preventDefault();
			fData.append('init', true);
			fData.append('op', 'preinit');
			require(["http/request", "notifier"], (Request) => {
				Request.post(o.remote, fData, self._done, self._fail, self._always);
			});
		}

		/**
		 * On Remote Done Callback
		 *
		 * @param {Object} response	Response Data
		 * 
		 * @memberof ClientsInitializer
		 */
		_done(response) {
			let self = this, o = self.options;
			if(typeof o.onFinish === 'function') {
				o.onFinish(response);
			}
			let type = (response.status === 'success') ? A.Notifications.TYPE_SUCCESS : A.Notifications.TYPE_DANDER;
			A.Logger.writeLog(response.message);
			let opts = {
				title: response.title,
				content: response.message,
				type: type
			};
			A.Notifications.create(opts);
			if(response.status === 'success') {
				let ele = Selectors.q(o.initnotice);
				if(ele && ele.parentNode) {
					ele.parentNode.removeChild(ele);
				}
				window.refresh(600);
			}
		}
		
		/**
		 * On XHR Fail Callback
		 *
		 * @param {Object} 		xhr				XHR Request
		 * @param {string} 		textStatus		XHR Status Message
		 * 
		 * @memberof ClientsInitializer
		 */
		_fail(xhr, textStatus) {
            A.Logger.writeLog(textStatus);
		}
		

        /**
		 * On Always XHR Callback
		 *
		 * @param {String} 			textStatus		XHR Status Message
		 * 
		 * @memberof ClientsInitializer
		 */
		_always(textStatus) {
            A.Logger.writeLog(textStatus);
		}
		
		_prepare() {
		}

	}

	return ClientsInitializer;
});
