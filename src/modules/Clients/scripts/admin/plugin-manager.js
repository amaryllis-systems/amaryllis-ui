/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("clients-module/admin/plugin-manager", [
    "core/base",
    "http/request",
    "core/selectors",
    "core/classes",
    "events/event",
    "core/acms",
    "notifier"
], (Base, Request, Selectors, Classes, AcmsEvent, A) => {

	/**
	 * Clients Plugin Manager
	 *
	 * @class ClientsPluginManager
	 * @extends {Base}
	 */
	class ClientsPluginManager extends Base {
		
		/**
		 * Module Namespace
		 *
		 * @readonly
		 * @static
		 * @memberof ClientsPluginManager
		 */
		static get NS() {
			return "clients-module.admin.plugin-manager";
		}

		/**
		 * Module Name
		 *
		 * @readonly
		 * @static
		 * @memberof ClientsPluginManager
		 */
		static get MODULE() {
			return "Clients Plugin Manager";
		}

		/**
		 * Default Options
		 *
		 * @readonly
		 * @static
		 * @memberof ClientsPluginManager
		 */
		static get DEFAULT_OPTIONS() {
			return {
				remote: null,
				fieldtype: null,
				module: null,
				id: null
			};
		}

		/**
		 * Module init
		 *
		 * @static
		 * 
		 * @param {HTMLElement} element
		 * @param {Object|null} options
		 * 
		 * @returns {ClientsPluginManager}	New Instance
		 * 
		 * @memberof ClientsPluginManager
		 */
		static init(element, options) {
			let m = new ClientsPluginManager(element, options);
			m.element.acmsData(ClientsPluginManager.NS, m);

			return m;
		}

		/**
		 * Creates an instance of ClientsPluginManager.
		 * 
		 * @param {HTMLElement} element
		 * @param {Object|null} options
		 * 
		 * @memberof ClientsPluginManager
		 */
		constructor(element, options) {
			super(element, options);
			this._initialize();
		}

		listen() {
			let self = this, fields, field, i;
            fields = Selectors.qa("input,select,textarea", self.element);
            for (i = 0; i < fields.length; i++) {
                field = fields[i];
                if(field.acmsData("prevent")) {
                    continue;
                }
                AcmsEvent.add(field, "change", self._onChange);
            }
		}

		/**
		 * On Change Event Handler
		 *
		 * @param {Event} e		OnChange Event
		 * 
		 * @memberof ClientsPluginManager
		 */
		_onChange(e) {
			let el = e.target || e.srcElement,
                    tagName = el.tagName.toLowerCase(),
                    self = this,
                    o = this.options,
                    val, dirname,
                    parent = Selectors.closest(el, "div");
            
            if (el.type && el.type === "checkbox" && parent && Classes.hasClass(parent, "switch")) {
                val = !el.checked ? "true" : "false";
            } else if (el.type && el.type === "checkbox") {
                let container = Selectors.closest(el, ".form-group"),
                        checkboxes = Selectors.qa("input[type=checkbox]", container);
                if(checkboxes.length === 1) {
                    val = !checkboxes[0].checked ? "true" : "false";
                } else {
                    let checkboxesChecked = [], i;
                    for (i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].checked) {
                            checkboxesChecked.push(checkboxes[i].value);
                        }
                    }
                    val = checkboxesChecked.join(",");
                }
            } else {
                val = el.value;
            }
            plugin = el.getAttribute("data-plugin") || o.plugin || "";
            let fData = new FormData(), data = {
                value: val,
                field: el.getAttribute("name").replace("[]", ""),
                plugin: plugin,
                op: "update"
            };
            for(let prop in data) {
                if(data.hasOwnProperty(prop)) {
                    fData.append(prop, data[prop]);
                }
            }
            Request.post(self.remote, fData, self._done, self._fail, self._always);
		}
		
		/**
		 * On Remote Done Callback
		 * 
		 * @param {Object} response		Remote Response Data
		 * 
		 * @memberof ClientsPluginManager
		 */
		_done(response) {
			let self = this, o = self.options;
            A.Notifications.createFromResponse(response);
            A.Logger.writeLog(response.message);
		}

		/**
		 * On XHR Fail Callback
		 *
		 * @param {Object} 		xhr				XHR Request
		 * @param {string} 		textStatus		XHR Status Message
		 * 
		 * @memberof ClientsPluginManager
		 */
		_fail(xhr, textStatus) {
            A.Logger.writeLog(textStatus);
		}
		

        /**
		 * On Always XHR Callback
		 *
		 * @param {String} 			textStatus		XHR Status Message
		 * 
		 * @memberof ClientsPluginManager
		 */
		_always(textStatus) {
            A.Logger.writeLog(textStatus);
		}
		
		/**
		 * Prepare Module
		 *
		 * @memberof ClientsPluginManager
		 */
		_prepare() {
			let self = this, 
                o = self.options,
                element = self.element;
            self.form = Selectors.closest(element, "form");
            self.remote = o.remote || self.form.acmsData("remote");
            self.remote = o.remote || element.getAttribute("action");
            self.toggles = Selectors.qa("input[type=checkbox]", element);
		}
	}

	return ClientsPluginManager;
	
});
