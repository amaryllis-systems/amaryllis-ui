/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("bulletboard-module/admin/plugin-manager", [
	"core/base",
    'http/request',
    'core/selectors',
    'core/classes',
    'events/event',
    "core/acms",
    'notifier'
], (Base, Request, Selectors, Classes, AcmsEvent, A) => {

	
	/**
	 * Bulletboard Plugin Manager
	 *
	 * @class PluginManager
	 * @extends {Base}
	 */
	class PluginManager extends Base {

		/**
		 * Module Name
		 *
		 * @readonly
		 * @static
		 * @memberof PluginManager
		 */
		static get MODULE() {
			return "Plugin Manager";
		}

		/**
		 * Module Namespace
		 *
		 * @readonly
		 * @static
		 * @memberof PluginManager
		 */
		static get NS() {
			return "bulletboard-module.admin.plugin-manager";
		}

		/**
		 * Default Options
		 *
		 * @readonly
		 * @static
		 * @memberof PluginManager
		 */
		static get DEFAULT_OPTIONS() {
			return {
				fieldtype: null,
				module: null,
				id: null
			};
		}

		/**
		 * Modul Init
		 *
		 * @static
		 * 
		 * @param {HTMLElement} element Wrapper Element
		 * @param {Object|null} options Options
		 * 
		 * @returns	{PluginManager}	New Class instance
		 * 
		 * @memberof PluginManager
		 */
		static init(element, options) {
			let ta = new PluginManager(element, options);
			ta.element.acmsData(PluginManager.NS, ta);

			return ta;
		}

		/**
		 * Creates an instance of PluginManager.
		 * 
		 * @param {HTMLElement} element Wrapper Element
		 * @param {Object|null} options Options
		 * 
		 * @memberof PluginManager
		 */
		constructor(element, options) {
			super(element, options);
			this.form =
			this.remote =
			this.toggles = null;
			this._initialize();
		}

		/**
		 * Event Listener aufsetzen
		 *
		 * @memberof PluginManager
		 */
		listen() {
            let self = this, fields, field, i;
            fields = Selectors.qa('input,select,textarea', self.element);
            for (i = 0; i < fields.length; i++) {
                field = fields[i];
                if(field.acmsData('prevent')) {
                    continue;
                }
                AcmsEvent.add(field, 'change', self._onChange);
            }
        }

		/**
		 * On Change Event Handler
		 *
		 * @param {Event} e		Change Event
		 * 
		 * @memberof PluginManager
		 */
		_onChange(e) {
            let el = e.target || e.srcElement,
                    tagName = el.tagName.toLowerCase(),
                    self = this,
                    o = this.options,
                    val, dirname,
                    parent = Selectors.closest(el, 'div');
            
            if (el.type && el.type === 'checkbox' && parent && Classes.hasClass(parent, 'switch')) {
                val = !el.checked ? 'true' : 'false';
            } else if (el.type && el.type === 'checkbox') {
                let container = Selectors.closest(el, '.form-group'),
                        checkboxes = Selectors.qa('input[type=checkbox]', container);
                if(checkboxes.length === 1) {
                    val = !checkboxes[0].checked ? 'true' : 'false';
                } else {
                    let checkboxesChecked = [], i;
                    for (i = 0; i < checkboxes.length; i++) {
                        if (checkboxes[i].checked) {
                            checkboxesChecked.push(checkboxes[i].value);
                        }
                    }
                    val = checkboxesChecked.join(',');
                }
            } else {
                val = el.value;
            }
            plugin = el.getAttribute('data-plugin') || o.plugin || '';
            let fData = new FormData(), data = {
                value: val,
                field: el.getAttribute('name').replace('[]', ''),
                plugin: plugin,
                op: 'update'
            };
            for(let prop in data) {
                if(data.hasOwnProperty(prop)) {
                    fData.append(prop, data[prop]);
                }
            }
            Request.post(self.remote, fData, self._done, self._fail, self._always);
        }

        /**
         * Ausführung bei erfolgter Abfrage
         * 
         * Wird ausgeführt, wenn eine Antwort vom Server kommt
         * 
         * @param {Object}      response    Die Antwort vom Server
         * 
		 * @memberof PluginManager
         */
        _done(response) {
            let self = this, o = self.options;
            A.Notifications.createFromResponse(response);
            A.Logger.writeLog(response.message);
		}
		
        /**
         * Ausführung bei fehlgeschlagener Abfrage
         * 
         * <p>
         *  Wird ausgeführt, wenn keine Antwort vom Server kommt bzw die 
         *  Anfrage fehlschlägt
         * </p>
         * 
         * @param {Object}      xhr    		Das XHR Abfrage Object
		 * @param {String}		textStatus	Text Status Message
         * 
		 * @memberof PluginManager
         */
        _fail(xhr, textStatus) {
            A.Logger.writeLog(textStatus);
        }
        
        /**
         * Auführung bei jeder Abfrage
         * 
         * @param {String} textStatus Text Status
         * 
		 * @memberof PluginManager
         */
        _always(xhr, textStatus) {
            A.Logger.writeLog(textStatus);
        }
        
        /**
		 * Vorbereiten des Moduls
		 *
		 * @memberof PluginManager
		 */
		_prepare() {
            let self = this, 
                o = self.options,
                element = self.element;
            self.form = Selectors.closest(element, 'form');
            self.remote = o.remote || self.form.acmsData('remote');
            self.remote = o.remote || element.getAttribute('action');
            self.toggles = Selectors.qa('input[type=checkbox]', element);
		}
		
	}

	return PluginManager;

});
