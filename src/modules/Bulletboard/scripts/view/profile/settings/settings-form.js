/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("bulletboard-module/view/profile/settings/settings-form", [
	"core/base",
	"core/classes",
	"core/selectors",
	"events/event",
	"core/acms"
], (Base, Classes, Selectors, AcmsEvent, A) => {

	/**
	 * Profile Settings Form
	 *
	 * @class ProfileSettingsForm
	 * @extends {Base}
	 */
	class ProfileSettingsForm extends Base {

		/**
		 * Module Namespace
		 *
		 * @readonly
		 * @static
		 * @memberof ProfileSettingsForm
		 */
		static get NS() {
			return "bulletboard-module.view.profile.settings.settings-form";
		}

		/**
		 * Module Name
		 *
		 * @readonly
		 * @static
		 * @memberof ProfileSettingsForm
		 */
		static get MODULE() {
			return "Bulletboard Profile Settings";
		}


		/**
		 * Event Name "Value Upated"
		 *
		 * @readonly
		 * @static
		 * @memberof ProfileSettingsForm
		 */
		static get EVENT_UPDATED() {
			return ProfileSettingsForm.NS + '.updated';
		}

		/**
		 * Default Options
		 *
		 * @readonly
		 * @static
		 * @memberof ProfileSettingsForm
		 */
		static get DEFAULT_OPTIONS() {
			return {
				remote: null
			};
		}

		/**
		 * Module Init
		 *
		 * @static
		 * 
		 * @param {HTMLElement} element Form
		 * @param {Object|null} options Custom options
		 * 
		 * @returns
		 * @memberof ProfileSettingsForm
		 */
		static init(element, options) {
			let m = new ProfileSettingsForm(element, options);
			m.element.acmsData(ProfileSettingsForm.NS, m);
			
			return m;
		}

		/**
		 * Creates an instance of ProfileSettingsForm.
		 * 
		 * @param {HTMLElement} element Form
		 * @param {Object|null} options Custom options
		 * 
		 * @memberof ProfileSettingsForm
		 */
		constructor(element, options) {
			super(element, options);
			this._initialize();
		}

		/**
		 * Vorbereiten des Moduls
		 *
		 * @memberof ProfileSettingsForm
		 */
		_prepare() {
		}

		/**
		 * Aufsetzen der Event Listener
		 *
		 * @memberof ProfileSettingsForm
		 */
		listen() {
            let self = this;
            let fields = Selectors.qa('input,select,textarea', self.element), i;
            for(i = 0; i < fields.length; i++) {
				let field = fields[i];
                AcmsEvent.add(field, 'change', self._onChange);
            }
        }
        
        /**
		 * On Change Event Handler
		 *
		 * @param {Event} e		Change Event
		 * 
		 * @memberof ProfileSettingsForm
		 */
		_onChange (e) {
            let el = e.target || e.srcElement,
                self = this,
                o = this.options,
                val;
            val = self._valueFromElement(el);
            self.data = {
                value: val,
                field: el.getAttribute('name').replace('[]', ''),
                op: 'update'
			};
			require(["http/request", "notifier"], (Request) => {
				Request.post(o.remote, self.data, self._done, self._fail, self._always);
			});
		}
		
		/**
		 * Get Value from changed Element
		 *
		 * @param {HTMLElement} el	Form Element
		 * 
		 * @returns {String}	Element Value
		 * 
		 * @memberof ProfileSettingsForm
		 */
		_valueFromElement(el) {
			let val,
				parent = Selectors.closest(el, 'div');

			if (el.type && el.type === 'checkbox' && parent && Classes.hasClass(parent, 'switch')) {
				val = !el.checked ? 'true' : 'false';
			} else if (el.type && el.type === 'checkbox') {
				let container = Selectors.closest(el, '.form-group'),
					checkboxes = Selectors.qa('input[type=checkbox]', container);
				if(checkboxes.length === 1) {
					val = !checkboxes[0].checked ? 'true' : 'false';
				} else {
					let checkboxesChecked = [], i;
					for (i = 0; i < checkboxes.length; i++) {
						if (checkboxes[i].checked) {
							checkboxesChecked.push(checkboxes[i].value);
						}
					}
					val = checkboxesChecked.join(',');
				}
			} else {
				val = el.value;
			}

			return val;
		}

        /**
		 * On Done Callback
		 *
		 * @param {Object} response
		 * 
		 * @memberof ProfileSettingsForm
		 */
		_done(response) {
            if(response.status === "error") {
                A.Notifications.createFromResponse(response);
			}
			let self = this,
				data = self.data;
            let ev = AcmsEvent.createCustom(ProfileSettingsForm.EVENT_UPDATED, {
                status: response.status,
                value: data.value,
				field: data.field,
				reponse: response
            });
            AcmsEvent.dispatch(self.element, ev);
        }
		
		/**
		 * On XHR Fail Callback
		 *
		 * @param {Object} 		xhr				XHR Request
		 * @param {string} 		textStatus		XHR Status Message
		 * 
		 * @memberof ProfileSettingsForm
		 */
		_fail(xhr, textStatus) {
            A.Logger.writeLog(textStatus);
		}
		

        /**
		 * On Always XHR Callback
		 *
		 * @param {String} 			textStatus		XHR Status Message
		 * 
		 * @memberof ProfileSettingsForm
		 */
		_always(textStatus) {
            A.Logger.writeLog(textStatus);
		}
		
	}

	return ProfileSettingsForm;

});
