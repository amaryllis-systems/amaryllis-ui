/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("bulletboard-module/view/topic", [
	 "core/base",
	 "core/classes",
	 "core/selectors",
	 "events/event",
	 "core/window",
	 "core/acms"
], (Base, Classes, Selectors, AcmsEvent, Win, A) => {
 
	/**
	 * Topic Admin
	 *
	 * @class TopicAdmin
	 * @extends {Base}
	 */
	class TopicAdmin extends Base {

		/**
		 * Module Name
		 *
		 * @readonly
		 * @static
		 * @memberof TopicAdmin
		 */
		static get MODULE() {
			return "Topic Admin";
		}

		/**
		 * Module Namespace
		 *
		 * @readonly
		 * @static
		 * @memberof TopicAdmin
		 */
		static get NS() {
			return "bulletboard-module.view.topic";
		}

		/**
		 * Default Options
		 *
		 * @readonly
		 * @static
		 * @memberof TopicAdmin
		 */
		static get DEFAULT_OPTIONS() {
			return {
				remote: null,
				topic: 0,
				forum: 0,
				moveField: '#move-topic-to',
				closeField: '#close-topic-field'
			};
		}

		/**
		 * Modul Init
		 *
		 * @static
		 * 
		 * @param {HTMLElement} element Wrapper Element
		 * @param {Object|null} options Options
		 * 
		 * @returns	{TopicAdmin}	New Class instance
		 * 
		 * @memberof TopicAdmin
		 */
		static init(element, options) {
			let ta = new TopicAdmin(element, options);
			ta.element.acmsData(TopicAdmin.NS, ta);

			return ta;
		}

		/**
		 * Creates an instance of TopicAdmin.
		 * 
		 * @param {HTMLElement} element Wrapper Element
		 * @param {Object|null} options Options
		 * 
		 * @memberof TopicAdmin
		 */
		constructor(element, options) {
			super(element, options);
			this.duplicateField =
			this.moveField =
			this.closeField = null;
			this._processing = false;
			this._initialize();
		}

		listen() {
			let self = this;
			if(self.moveField) {
				AcmsEvent.add(self.moveField, 'change', self._onChangeMoveField);
			}
			if(self.closeField) {
				AcmsEvent.add(self.closeField, 'change', self._onChangeCloseField);
			}
		}


		/**
		 * On Change Close Select
		 *
		 * @param {Event} e		Change Event
		 * 
		 * @memberof TopicAdmin
		 */
		_onChangeCloseField(e) {
			e.preventDefault();
			e.stopPropagation();
			if(this._processing) {
				return this._resetMoveField();
			}
			let self = this, 
				o = self.options, 
				value = parseInt(self.moveField.value), 
				topic = parseInt(o.topic),
				closed = parseInt(o.closed),
				fData = new FormData();

			if(value == closed) {
				return false;
			}
			fData.append('topic_id', topic);
			fData.append('closed', value);
			fData.append('op', 'close');
			
			require(["http/request", "notifier"], (Request) => {
				Request.post(o.remote, fData, self._onMoved, self._fail, self._always);
			});

			return self._resetCloseField();
		}

		_enableDuplicateField() {

		}

		_disableDuplicateField() {
			
		}


		/**
		 * On Change Move Field Callback
		 *
		 * @param {Event} e		Change Event
		 * 
		 * @returns	{Boolean}	Immer `false`
		 * 
		 * @memberof TopicAdmin
		 */
		_onChangeMoveField(e) {
			e.preventDefault();
			e.stopPropagation();
			if(this._processing) {
				return this._resetMoveField();
			}
			let self = this, 
				o = self.options, 
				value = parseInt(self.moveField.value), 
				topic = parseInt(o.topic),
				fData = new FormData();

			if(value <= 0) {
				return false;
			}
			if(value === parseInt(o.forum)) {
				return self._resetMoveField();
			}
			fData.append('topic_id', topic);
			fData.append('forum_id', value);
			fData.append('op', 'move');

			require(["http/request", "notifier"], (Request) => {
				Request.post(o.remote, fData, self._onMoved, self._fail, self._always);
			});

			return self._resetMoveField();
		}

		/**
		 * process move Topic Response
		 *
		 * @param {Object} response xhr Response object
		 * 
		 * @memberof TopicAdmin
		 */
		_onMoved(response) {
			A.Notifications.createFromResponse(response);
			Win.refresh(500);
		}

		/**
		 * Reset Close Field
		 *
		 * @returns {Boolean}	immer `false`
		 * 
		 * @memberof TopicAdmin
		 */
		_resetCloseField() {
			let self = this;
			self.closeField.value = 0;
			AcmsEvent.fireChange(self.moveField);

			return false;
		}

		/**
		 * Reset Move Field
		 *
		 * @returns {Boolean}	immer `false`
		 * 
		 * @memberof TopicAdmin
		 */
		_resetMoveField() {
			let self = this;
			self.moveField.value = 0;
			AcmsEvent.fireChange(self.moveField);

			return false;
		}
		
        /**
         * Ausführung bei fehlgeschlagener Abfrage
         * 
		 * Wird ausgeführt, wenn keine Antwort vom Server kommt bzw die 
         * Anfrage fehlschlägt
         * 
         * @param {Object}  xhr    		Das XHR Abfrage Object
         * @param {String}	textStatus 	Text Status Message
		 * 
		 * @memberof TopicAdmin
         */
        _fail (xhr, textStatus) {
            A.Logger.writeLog(textStatus);
        }
        
        /**
         * Auführung bei jeder Abfrage
         * 
         * @param {String} textStatus Text Status
         * 
		 * @memberof TopicAdmin
         */
        _always (textStatus) {
            A.Logger.writeLog(textStatus);
            this._processing = false;
        }
        

		_prepare() {
			let self = this, o = self.options;
			self.moveField = Selectors.q(o.moveField);
			self.closeField = Selectors.q(o.closeField);
		}
	}
 
	return TopicAdmin;
 });
