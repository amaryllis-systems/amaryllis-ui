/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 
 define("bulletboard-module/view/topic/topic-search", [
	 "core/base",
	 "core/classes",
	 "core/selectors",
	 "events/event",
	 "core/acms"
 ], (Base, Classes, Selectors, AcmsEvent, A) => {
 
	 /**
	  * Topic Search
	  * 
	  * Quick Search field to find Topics
	  *
	  * @class TopicSearch
	  * @extends {Base}
	  */
	 class TopicSearch extends Base {
 
		 /**
		  * Module Namespace
		  *
		  * @readonly
		  * @static
		  * @memberof TopicSearch
		  */
		 static get NS() {
			 return "bulletboard-module.view.topic.topic-search";
		 }
 
 
		 /**
		  * Module Name
		  *
		  * @readonly
		  * @static
		  * @memberof TopicSearch
		  */
		 static get MODULE() {
			 return "Topic Search";
		 }
 
		 /**
		  * Default Options
		  *
		  * @readonly
		  * @static
		  * @memberof TopicSearch
		  */
		 static get DEFAULT_OPTIONS() {
			 return {
				 remote: null,
				 current: null,
				 onSelect: () => {},
			 };
		 }
 
		 /**
		  * Module Loader Init
		  *
		  * @static
		  * 
		  * @param {HTMLElement} element	HTML Element
		  * @param {Object|null} options	optional Custom Options
		  * 
		  * @returns {TopicSearch}	New instance
		  * 
		  * @memberof TopicSearch
		  */
		 static init(element, options) {
			 let m = new TopicSearch(element, options);
			 m.element.acmsData(TopicSearch.NS, m);
			 
			 return m;
		 }
 
 
		 /**
		  * Creates an instance of TopicSearch.
		  * 
		  * @param {HTMLElement} element	HTML Element
		  * @param {Object|null} options	optional Custom Options
		  * @memberof TopicSearch
		  */
		 constructor(element, options) {
			 super(element, options);
			 this._initialize();
 
		 }
 
 
		 _prepare() {
		 }
 
	 }
 
	 return TopicSearch;
 });
