/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("bulletboard-module/view/topic/topic-vote", [
	"core/base",
	"tools/utils",
	"core/classes",
	"core/selectors",
	"events/event",
	"core/acms"
], (Base, Utils, Classes, Selectors, AcmsEvent, A) => {

	class TopicVote extends Base {

		static get NS() {
			return "bulletboard-module/view/topic/topic-vote";
		}


		static get MODULE() {
			return "Topic Voter";
		}

		static get DEFAULT_OPTIONS() {
			return {
				/**
				 * @var {String}    Remote Url
				 */
				remote: null,
				/**
				 * @var {String|Integer}    Topic ID
				 */
				id: null,
				/**
				 * @var {String|Integer|null}    Post ID
				 */
				post: null,
				/**
				 * On Voted
				 * 
				 * @param {Object} response Ajax Response Daten
				 * 
				 * @returns {undefined}
				 */
				onvoted(response) {},
				/**
				 * On Downvoted
				 * 
				 * @param {Object} response Ajax Response Daten
				 * 
				 * @returns {undefined}
				 */
				downvote(response) {},
				/**
				 * On Upvoted
				 * 
				 * @param {Object} response Ajax Response Daten
				 * 
				 * @returns {undefined}
				 */
				upvote(response) {},
				/**
				 * On Star
				 * 
				 * @param {Object} response Ajax Response Daten
				 * 
				 * @returns {undefined}
				 */
				star(response) {},
				/**
				 * On Unstar
				 * 
				 * @param {Object} response Ajax Response Daten
				 * 
				 * @returns {undefined}
				 */
				unstar(response) {},
				/**
				 * On Star-Toggled
				 * 
				 * @param {Object} response Ajax Response Daten
				 * 
				 * @returns {undefined}
				 */
				onstarred(response) {},
				
				/**
				 * On Solved
				 * 
				 * @param {Object} response Ajax Response Daten
				 * 
				 * @returns {undefined}
				 */
				onsolved(response) {}
			};
		}

			
		/**
		 * Event "Star-Toggle-Aktion"
		 * 
		 * Wird bei jeder erfolgreichen Star-Toggle-Aktion (star oder unstar) ausgeführt
		 * 
		 * @type {String}
		 */
		static get EVENT_STARRED() {
			return TopicVote.NS + ".topic-starred";
		}
		
		/**
		 * Event "Star-Aktion"
		 * 
		 * Wird bei jeder erfolgreichen Star-Aktion ausgeführt
		 * 
		 * @type {String}
		 */
		static get EVENT_STAR() {
			return TopicVote.NS + ".topic-star";
		}
			
		/**
		 * Event "Unstar-Aktion"
		 * 
		 * Wird bei jeder erfolgreichen Unstar-Aktion ausgeführt
		 * 
		 * @type {String}
		 */
		static get EVENT_UNSTAR() {
			return TopicVote.NS + ".topic-unstar";
		}
			
		/**
		 * Event "Voted-Aktion"
		 * 
		 * Wird bei jeder erfolgreichen Vote-Aktion ausgeführt (up- und downvote)
		 * 
		 * @type {String}
		 */
		static get EVENT_VOTED() {
			return TopicVote.NS + ".topic-voted";
		}
			
		/**
		 * Event "Voted-Up-Aktion"
		 * 
		 * Wird bei jeder erfolgreichen Upvote-Aktion ausgeführt
		 * 
		 * @type {String}
		 */
		static get EVENT_VOTEDUP() {
			return TopicVote.NS + ".topic-voted-up";
		}
			
		/**
		 * Event "Voted-Down-Aktion"
		 * 
		 * Wird bei jeder erfolgreichen Downvote-Aktion ausgeführt
		 * 
		 * @type {String}
		 */
		static get EVENT_VOTEDDOWN() {
			return TopicVote.NS + ".topic-voted-down";
		}
		
		/**
		 * Event "Solve-Aktion"
		 * 
		 * Wird bei jeder erfolgreichen Solve-Aktion ausgeführt
		 * 
		 * @type {String}
		 */
		static get EVENT_SOLVE() {
			return TopicVote.NS + ".topic-solve";
		}
		
		/**
		 * Module Init
		 *
		 * @static
		 * 
		 * @param {HTMLElement} element
		 * @param {Object|null} options
		 * 
		 * @returns {TopicVote}
		 * 
		 * @memberof TopicVote
		 */
		static init(element, options) {
			let m = new TopicVote(element, options);
			m.element.acmsData(TopicVote.NS, m);
			
			return m;
		}

		/**
		 * Creates an instance of TopicVote.
		 * 
		 * @param {HTMLElement} element
		 * @param {Object|null} options
		 * 
		 * @memberof TopicVote
		 */
		constructor(element, options) {
			super(element, options);
			this._initialize();

		}


		/**
		 * Bilden der Optionen
		 *
		 * @param {Object} options
		 * @returns {Object}
		 * @memberof TopicVote
		 */
		buildOptions(options) {
			let o = super.buildOptions(options);
			if(o.post === null || o.post === 'null' ) {
                o.post = null;
            } else {
                o.post = parseInt(o.post, 10);
            }
            return o;
		}

		
        /**
		 * Aufsetzen der Event Listener
		 *
		 * @memberof TopicVote
		 */
		listen() {
            let self = this, o = self.options,
                up = self._upvoteBtn, 
                down = self._downvoteBtn,
                star = self._starBtn;
            console.log(up, down, o.post);
            if(up) {
                AcmsEvent.add(up, 'click', self._onClick);
            }
            if(down) {
                AcmsEvent.add(down, 'click', self._onClick);
            }
            if(!o.post && star && Classes.hasClass(star, 'can-star')) {
                AcmsEvent.add(star, 'click', self._onClickStar);
            }
            if(o.post && self._solveBtn) {
                AcmsEvent.add(self._solveBtn, 'click', self._onClickSolve);
            }
        }
        
        
        _onClickStar(e) {
            let self = this, target = e.target, o = self.options;
            if(self.starring === true) {
                return;
            }
            self.starring = true;
            if(!Classes.hasClass(target, 'star')) {
                target = Selectors.closest(target, '.star');
            }
            if(!target) throw new Error('Missing target');
            let fData = new FormData(), 
                op = (Classes.hasClass(target, 'star-off') ? 'star' : 'unstar');
            fData.append('op', op);
            fData.append('topic_id', o.id);
            
            require(['http/request', 'notifier'], function(Request) {
                Request.post(self.remote, fData, self._doneStars, self._fail, self._always);
            });
        }
        
        
        _onClickSolve(e) {
            let self = this, target = e.target, o = self.options, trigger = target.acmsData('trigger');
            if(self.starring === true) {
                return;
            }
            self.starring = true;
            if(!trigger || trigger != "solve") {
                target = Selectors.closest(target, '[data-trigger="solve"]');
            }
            if(!target) throw new Error('Missing target');
            let fData = new FormData(), 
                op = 'solve';
            fData.append('op', op);
            fData.append('topic_id', o.id);
            fData.append('post_id', o.post);
            
            require(['http/request', 'notifier'], function(Request) {
                Request.post(self.remote, fData, self._doneSolve, self._fail, self._always);
            });
        }
        
        
        _onClick(e) {
            let self = this, target = e.target, o = self.options;
            console.log(e);
            if(self._voted === true) {
                return;
            }
            console.log(e);
            self._voted = true;
            if(!target.acmsData('trigger')) {
                target = Selectors.closest(target, '[data-trigger]');
            }
            if(!target) throw new Error('Missing target');
            let fData = new FormData();
            fData.append('op', target.acmsData('trigger'));
            fData.append('topic_id', o.id);
            if(o.post) {
                fData.append('post_id', o.post);
            }
            
            require(['http/request', 'notifier'], function(Request) {
                Request.post(self.remote, fData, self._done, self._fail, self._always);
            });
        }
        
        
        _doneStars(response, data) {
            let self = this, o = self.options, element = self.element;
            if(response.status === "error") {
                A.Notifications.createFromResponse(response);
                return;
            }
            Classes.toggleClass(self._starBtn, 'star-off');
            Classes.toggleClass(self._starBtn, 'star-on');
            
            let ev = AcmsEvent.createCustom(TopicVote.EVENT_STARRED, {
                status: response.status,
                topic: response.topic,
                startype: response.startype,
                element: element
            });
            let name = (Classes.hasClass(self._starBtn, 'star-off') ? TopicVote.EVENT_UNSTAR : TopicVote.EVENT_STAR);
            let ev2 = AcmsEvent.createCustom(name, {
                status: response.status,
                topic: response.topic,
                startype: response.startype,
                element: element
            });
            AcmsEvent.dispatch(this.element, ev);
            AcmsEvent.dispatch(this.element, ev2);
            if(typeof o[response.startype] === 'function') {
                o[response.startype](response);
            }
            if(typeof o.onstarred === 'function') {
                o.onstarred(response);
            }
            A.Logger.writeLog(response.message);
        }
        
        _doneSolve(response, data) {
            let self = this, o = self.options, element = self.element;
            if(response.status === "error") {
                A.Notifications.createFromResponse(response);
                return;
            }
            Classes.toggleClass(self._solveBtn, 'is-solution');
            Classes.toggleClass(self._solveBtn, 'flag-solution');
            
            let ev = AcmsEvent.createCustom(TopicVote.EVENT_SOLVE, {
                status: response.status,
                topic: response.topic,
                startype: response.startype,
                element: element
            });
            AcmsEvent.dispatch(this.element, ev);
            if(typeof o.onsolve === 'function') {
                o.onsolve(response);
            }
            A.Logger.writeLog(response.message);
        }
        
        
        _done(response) {
            let self = this, element = self.element, o = self.options;
            if(response.status === "error") {
                A.Notifications.createFromResponse(response);
                return;
            }
            let counter = Selectors.q('.vote-count-post', element);
            let ev = AcmsEvent.createCustom(TopicVote.EVENT_VOTED, {
                status: response.status,
                votetype: response.votetype,
                topic: response.topic
            });
            let name = response.votetype === 'upvote' ? TopicVote.EVENT_VOTEDUP : TopicVote.EVENT_VOTEDDOWN;
            let ev2 = AcmsEvent.createCustom(name, {
                status: response.status,
                votetype: response.votetype,
                topic: response.topic
            });
            AcmsEvent.dispatch(this.element, ev);
            AcmsEvent.dispatch(this.element, ev2);
            if(typeof o[response.votetype] === 'function') {
                o[response.votetype](response);
            }
            if(typeof o.onvoted === 'function') {
                o.onvoted(response);
            }
            let avg = Selectors.q('.vote-count-post', element),
                val = parseInt(avg.textContent, 10);
            if(response.votetype === 'upvote') {
                avg.textContent = val + 1;
            } else {
                avg.textContent = val - 1;
            }
            A.Logger.writeLog(response.message);
        }
        
        /**
         * Ausführung bei fehlgeschlagener Abfrage
         * 
		 * Wird ausgeführt, wenn keine Antwort vom Server kommt bzw die 
         * Anfrage fehlschlägt
         * 
         * @param {Object}      xhr    Das XHR Abfrage Object
         * 
         * @returns {undefined}
         */
        _fail (xhr, textStatus) {
            A.Logger.writeLog(textStatus);
        }
        
        /**
         * Auführung bei jeder Abfrage
         * 
         * @param {String} textStatus Text Status
         * 
         * @returns {undefined}
         */
        _always (xhr, textStatus) {
            A.Logger.writeLog(textStatus);
            this.starring = false;
        }
        
        _prepare() {
            let self = this, 
                o = self.options,
                element = self.element;
            self.remote = o.remote;
            self._starBtn = Selectors.q('.star', element);
            self._solveBtn = Selectors.q('[data-trigger="solve"]', element);
            let up = o.post ? '[data-trigger="postvoteup"]' : '[data-trigger="voteup"]';
            let down = o.post ? '[data-trigger="postvotedown"]' : '[data-trigger="votedown"]';
            self._upvoteBtn = Selectors.q(up, element);
            self._downvoteBtn = Selectors.q(down, element);
            self._voted = false;
            self.starring = false;
		}
		
	}

	return TopicVote;
});
