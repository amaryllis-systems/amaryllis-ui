/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("bulletboard-module/view/form/forumperm-form", [
	"core/base",
	"core/selectors",
	"events/event",
	"core/acms"
], (Base, Selectors, AcmsEvent, A) => {

	/**
	 * Forum Permission Form Handling
	 *
	 * @class ForumpermForm
	 * 
	 * @extends {Base}
	 */
	class ForumpermForm extends Base {

		/**
		 * Module Namespace
		 *
		 * @readonly
		 * @static
		 * @memberof ForumpermForm
		 */
		static get NS() {
			return "bulletboard-module.view.form.forumperm-form";
		}


		/**
		 * Module Name
		 *
		 * @readonly
		 * @static
		 * @memberof ForumpermForm
		 */
		static get MODULE() {
			return "Forum Permission Form";
		}

		/**
		 * Default Options
		 *
		 * @readonly
		 * @static
		 * @memberof ForumpermForm
		 */
		static get DEFAULT_OPTIONS() {
			return {};
		}

		/**
		 * Module Init
		 *
		 * @static
		 * @param {HTMLElement} element
		 * @param {Object|null} options
		 * @returns
		 * @memberof ForumpermForm
		 */
		static init(element, options) {
			let m = new ForumpermForm(element, options);
			m.element.acmsData(ForumpermForm.NS, m);
			
			return m;
		}

		/**
		 * Creates an instance of ForumpermForm.
		 * 
		 * @param {HTMLElement} element
		 * @param {Object|null} options
		 * 
		 * @memberof ForumpermForm
		 */
		constructor(element, options) {
			super(element, options);
			this.toggles =
			this.button = null;
			this.isInModal = Selectors.closest(element, '.modal');
			this._initialize();
		}

		/**
		 * Event Listener aufsetzen
		 *
		 * @memberof ForumpermForm
		 */
		listen() {
            let self = this, i;
            for(i = 0; i < self.toggles.length; i++) {
                AcmsEvent.add(self.toggles[i], 'change', self._onChange);
            }
		}
		
		
        /**
         * On-Change-Event Handler
         * 
         * @param {Event} e  Das Event
		 * 
         * @returns {Boolean}
		 * 
		 * @memberof ForumpermForm
         */
        _onChange(e) {
            let self = this,
                target = e.target,
                checked = !target.checked;
                fData = new FormData();
            
            fData.append('group', target.acmsData('group'));
            fData.append('perm', target.acmsData('perm'));
            fData.append('forum_id', target.acmsData('forum'));
            fData.append('checked', checked);
            fData.append('op', 'updatepermission');
            e.preventDefault();
            require(['http/request', 'notifier'], function(Request) {
                Request.post(self.remote, fData, self._done, self._fail, self._always);
            });
            return false;
        }
        
        /**
         * Ausführung bei erfolgter Abfrage
         * 
         * Wird ausgeführt, wenn eine Antwort vom Server kommt
         * 
         * @param {Object}      response    Die Antwort vom Server
         * 
		 * @memberof ForumpermForm
         */
        _done(response) {
            let self = this, o = self.options;
            if(response.status !== 'success')
            A.Notifications.createFromResponse(response);
            A.Logger.writeLog(response.message);
		}
		
        /**
         * Ausführung bei fehlgeschlagener Abfrage
         * 
         * Wird ausgeführt, wenn keine Antwort vom Server kommt bzw die 
         * Anfrage fehlschlägt
         * 
         * @param {Object}      xhr    Das XHR Abfrage Object
		 * @param {String} textStatus Text Status
         * 
		 * @memberof ForumpermForm
         */
        _fail(xhr, textStatus) {
            A.Logger.writeLog(textStatus);
        }
        
        /**
         * Auführung bei jeder Abfrage
         * 
         * @param {String} textStatus Text Status
         * 
		 * @memberof ForumpermForm
         */
        _always(xhr, textStatus) {
            A.Logger.writeLog(textStatus);
        }
        
		/**
		 * Vorbereiten des Moduls
		 *
		 * @memberof ForumpermForm
		 */
        _prepare() {
            let self = this, 
                o = self.options,
                element = self.element;
            self.remote = o.remote || element.getAttribute('action');
            self.toggles = Selectors.qa('input[type=checkbox]', element);
        }

	}

	return ForumpermForm;

});
