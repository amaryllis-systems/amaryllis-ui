/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("notes-module/app/helper/note", [
	"core/base",
	"core/classes",
	"core/selectors",
	"events/event",
	"core/acms",
	"templates/template-engine",
	"notes-module/app/notes",
	"notes-module/app/helper/notebook"
], (Base, Classes, Selectors, AcmsEvent, A, TemplateEngine, Notes, Notebook) => {

	/**
	 * Note
	 * 
	 * Class representing a single note.
	 *
	 * @class Note
	 * @extends {Base}
	 */
	class Note extends Base {

		/**
		 * Module Namespace
		 *
		 * @readonly
		 * @static
		 * @memberof Note
		 */
		static get NS() {
			return "apps.profile.notes.helpers.note";
		}

		/**
		 * Module Name
		 *
		 * @readonly
		 * @static
		 * @memberof Note
		 */
		static get MODULE() {
			return "Profile Note";
		}

		/**
		 * Default Options
		 *
		 * @readonly
		 * @static
		 * @memberof Note
		 */
		static get DEFAULT_OPTIONS() {
			return {
				hasColor: true
			};
		}
		
		/**
		 * Default Colors
		 *
		 * @readonly
		 * @static
		 * @memberof Note
		 */
		static get DEFAULT_COLORS() {
			return {
				0: {
					'name': A.Translator._('Keine Farbe', 'Notes'),
					'color': '#FFFFFF',
					'inverse': ''
				},
				1: {
					'name': A.Translator._('Farbe Rot', 'Notes'),
					'color': '#D50000',
					'inverse': '#FFFFFF'
				},
				2: {
					'name': A.Translator._('Farbe Blau', 'Notes'),
					'color': '#2962FF',
					'inverse': '#FFFFFF'
				},
				3: {
					'name': A.Translator._('Farbe Grün', 'Notes'),
					'color': '#00C853',
					'inverse': '#FFFFFF'
				},
				4: {
					'name': A.Translator._('Farbe Gelb', 'Notes'),
					'color': '#ffd600',
					'inverse': '#FFFFFF'
				},
				5: {
					'name': A.Translator._('Farbe Lila', 'Notes'),
					'color': '#aa00ff',
					'inverse': '#FFFFFF'
				},
				6: {
					'name': A.Translator._('Farbe Orange', 'Notes'),
					'color': '#ff6d00',
					'inverse': '#FFFFFF'
				}
			};
		}
	
		/**
		 * Default Labels
		 *
		 * @readonly
		 * @static
		 * @memberof Note
		 */
		static get DEFAULT_LABELS() {
			return {
				edit: A.Translator._('bearbeiten'),
				delete: A.Translator._('entfernen'),
				selectColor: A.Translator._('Farbe wählen', 'Notes'),
				new: A.Translator._('Neue Notiz', 'Notes')
			};
		}
		
		/**
		 * Erstellt einen Note aus einem Response Objekt
		 * 
		 * Die Methode kann genutzt werden um aus einem Note-Model Objekt aus der 
		 * JSON Response beim Laden oder erstellen eines Notes den Note zu einer 
		 * Liste hinzuzufügen.
		 * 
		 * @param {Object}  	data    Daten aus der Datenbank
		 * @param {HTMLElement} list    Die Liste
		 * @param {Object}  	options Die Optionen  
		 * @param {Notebook}	nbook	Notebook instance
		 * @param {Notes}		pnotes	Profile Notes App instance
		 * 
		 * @returns {Note}
		 */
		static createFromResponse(data, list, options, nbook, pnotes, callback) {
			let labels = options.note.labels || Note.DEFAULT_LABELS, 
				colors = options.note.colors || Note.DEFAULT_COLORS,
				element, i, tpl, tData, n, cb;
			for (i in colors) {
				if(colors.hasOwnProperty(i)) {
					colors[i].active = (i == parseInt(data.colorscheme));
				}
			}
			tpl = options.templates.note.note;
			options.isOwner = (parseInt(options.owner, 10) == parseInt(data.owner, 10));
			tData = {
				note: data,
				labels: labels,
				colors: colors,
				options: options
			};
			cb = () => {
				element = list.querySelector('#profile-note-'+data.id);
				n = Note.init(element, options, nbook, pnotes);
				n.data = data;
				n.element.acmsData('original', data);
				
				callback(n);
			};
			TemplateEngine.compileResource(tpl, tData, cb, list);
			
		}

		static init(element, options, pnotes, nbook) {
			let m = new Note(element, options, pnotes, nbook);
			m.element.acmsData(Note.NS, m);
			
			return m;
		}


		constructor(element, options, nbook, pnotes) {
			super(element, options);
			this.profilenotes = pnotes;
			this.notebook = nbook;
			this._initialize();
		}

		set data(data) {
			this._data = data;
		}

		get data() {
			return this._data;
		}

		setData(data) {
			this.data = data;
		}

		getData() {
			return this.data;
		}

		/**
		 * Is the note active?
		 *
		 * @returns {Boolean}	`true` if the not has been activated, `false` otherwise
		 * 
		 * @memberof Note
		 */
		isActive() {
			let self = this;
			return Classes.hasClass(self.element, 'active');
		}

		/**
		 * Activates the note
		 *
		 * @memberof Note
		 */
		activate() {
			let self = this, element = self.element, dsc;
			if(self._isToggling) {
				return;
			}
			self._isToggling = true;
			dsc = document.createElement('div');
			Classes.addClass(dsc, 'note-description fade');
			element.appendChild(dsc);
			dsc.innerHTML = self.data.body;
			Classes.addClass(dsc, 'in');
			Classes.addClass(self.element, 'active');
			self._isToggling = false;
		}

		/**
		 * Deactivate the note
		 *
		 * @memberof Note
		 */
		deactivate() {
			let self = this, element = self.element, dsc = Selectors.q('.note-description', element);
			if(self._isToggling || !dsc) {
				return;
			}
			self._isToggling = true;
			Classes.removeClass(dsc, 'in');
			setTimeout(() => {
				element.removeChild(dsc);
				self._isToggling = false;
			}, 200);
		
			Classes.removeClass(self.element, 'active');
		}

		/**
		 * Toggle Active State
		 *
		 * @memberof Note
		 */
		toggleActive() {
			if(this.isActive()) {
				this.deactivate();
			} else {
				this.activate();
			}
		}

		listen() {
			let self = this, o = self.options;
            AcmsEvent.add(self.element, 'click', self._onClickNote);
            if(self.editButton && o.isOwner) {
                AcmsEvent.add(self.editButton, 'click', self._onClickEdit);
            }
            if(self.delButton && o.isOwner) {
                AcmsEvent.add(self.delButton, 'click', self._onClickDelete);
            }
            if(self.checkbox && o.isOwner) {
                AcmsEvent.add(self.checkbox, 'change', self._onCheckboxChange);
            }
            if(self.colorselect && o.isOwner || true === o.hasColor) {
                AcmsEvent.on(self.colorselect, 'click', '.notes-color', self._onSelectColor);
            }
		}

		_onClickNote(e) {
			let self = this, o = self.options, target = e.target;
			if(Classes.hasClass(target, 'note-action ') || Selectors.closest(target, '.note-action ')) {
				return;
			}
			this.toggleActive();
		}
		
        /**
         * On-Click-Event Handler
         * 
         * @param {Event} e  Das Event
         * @returns {Boolean}
		 * 
		 * @memberof Note
         */
        _onClickEdit(e) {
            let self = this,
				target = e.target,
				pnotes = self.profilenotes,
				o = pnotes.options,
				template = o.templates.note.form;
            
            return false;
        }
		
        /**
         * On-Click-Delete Event Handler
         * 
         * @param {Event} e  Das Event
         * 
		 * @returns {Boolean}
		 * 
		 * @memberof Note
         */
        _onClickDelete(e) {
			let self = this, 
				o = self.options,
				target = e.target, 
				data = self.getData(),
				fData = new FormData();
            fData.append('op', 'deletenote');
            fData.append('id', data.id);
            fData.append('notebook_id', data.notebook_id);
            require(['http/request'], (Request) => {
                Request.post(o.remote, fData, self._done);
            });
            return false;
        }
		
        /**
         * On-Ccheckbox Change Event Handler
         * 
         * @param {Event} e  Das Event
		 * 
         * @returns {Boolean}
		 * 
		 * @memberof Note
         */
        _onCheckboxChange(e) {
            let self = this,
                    target = e.target,
                    form = Selectors.closest(target, 'form'),
                    fData = new FormData(form);
            if(target.disabled) {
                return;
            }
            /*
            require(['http/request', 'notifier'], function (Request) {
                Request.post(self.remote, fData, self._done, self._fail, self._always);
            });
            */
            return false;
        }

		/**
		 * On Select Color Event Handler
		 *
		 * @param {Event} e		Click Event
		 * 
		 * @memberof Note
		 */
		_onSelectColor(e) {
            let self = this, o = self.options, span = e.target, element = self.element;
            let value = parseInt(span.acmsData('color'), 10);
            self.colors.forEach((sp) => {
                Classes.removeClass(sp, 'active');
            });
            Classes.removeClass(self.element, self.colorclasses);
            Classes.addClass(self.element, 'notes-color-'+value);
            Classes.addClass(span, 'active');
            let fData = new FormData(), data = self.getData();
            fData.append('notebook_id', data.notebook_id);
            fData.append('note_id', data.id);
            fData.append('field', 'colorscheme');
            fData.append('value', value);
            fData.append('op', 'updatenote');
            require(['http/request'], (Request) => {
                Request.post(o.remote, fData);
            });
		}
		
		/**
		 * On Deleted xhr Callback
		 *
		 * @param {Object} response Response data from server
		 * 
		 * @memberof Note
		 */
		_onDeleted(response) {
			let self = this, o = self.options;
            if(response.status === 'success') {
				if(self.element.parentNode) 
					self.element.parentNode.removeChild(self.element);
                self.element = null;
            }
		}

		_prepare() {
			let self = this,
                    o = self.options,
                    element = self.element, colors = o.note.colors || Note.DEFAULT_COLORS;
            self.remote = o.remote;
            self.editButton = Selectors.q('.note-edit', element);
            self.checkbox = Selectors.q('input[type="checkbox"]', element);
            self.delButton = Selectors.q('.note-delete', element);
            if(!o.isOwner) {
                Classes.addClass(self.editButton, 'hidden invisible');
                Classes.addClass(self.delButton, 'hidden invisible');
                Classes.addClass(Selectors.closest(self.checkbox, 'div'), 'hidden invisible');
            }
            self.colorselect = Selectors.q('.note-colors', element);
            if(o.hasColor && !self.colorselect) {
                self._appendColorSelect();
            } else {
                self.colors = Selectors.qa('.notes-color', element);
                self.colorclasses = '';
                for(let i in colors) {
                    if(colors.hasOwnProperty(i)) {
                        self.colorclasses += 'notes-color-'+i+',';
                    }
                }
                self.colorclasses = self.colorclasses.substring(0, self.colorclasses.length - 1);
            }
		}
		
        /**
         * Ausführung bei fehlgeschlagener Abfrage
         * 
		 * Wird ausgeführt, wenn keine Antwort vom Server kommt bzw die 
         * Anfrage fehlschlägt
         * 
         * @param {Object}  xhr    		Das XHR Abfrage Object
         * @param {String}	textStatus 	Text Status Message
		 * 
		 * @memberof Note
         */
        _fail (xhr, textStatus) {
            A.Logger.writeLog(textStatus);
        }
        
        /**
         * Auführung bei jeder Abfrage
         * 
         * @param {String} textStatus Text Status
         * 
		 * @memberof Note
         */
        _always (textStatus) {
            A.Logger.writeLog(textStatus);
            this._processing = false;
        }
		
		_appendColorSelect() {
            let self = this, o = self.options, colors = o.colors || Note.DEFAULT_COLORS,
                select = self.colorselect, scheme;
            scheme = parseInt(o.original.colorscheme, 10);
            self.colorclasses = '';
            self.colors = [];
            for(let i in colors) {
                if(colors.hasOwnProperty(i)) {
                    let span = document.createElement('span');
                    let val = parseInt(i);
                    self.colorclasses += 'notes-color-'+val+',';
                    select.appendChild(span);
                    span.acmsData('color', val);
                    span.setAttribute('aria-label', colors[i].name);
                    Classes.addClass(span, 'notes-color,notes-color-'+i);
                    if(scheme === val) {
                        Classes.addClass(span, 'active');
                    }
                    self.colors.push(span);
                }
            }
            self.colorclasses = self.colorclasses.substring(0, self.colorclasses.length - 1);
		}
		
	}

	return Note;
});
