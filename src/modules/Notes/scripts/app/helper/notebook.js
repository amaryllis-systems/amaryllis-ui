/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("notes-module/app/helper/notebook", [
	"core/base",
	"tools/utils",
	"core/classes",
	"core/selectors",
	"events/event",
	"core/acms",
	"notes-module/app/notes",
	"notes-module/app/helper/note"
], (Base, Utils, Classes, Selectors, AcmsEvent, A, Notes, Note) => {

	/**
	 * Notebook
	 * 
	 * Class representing a single notebook.
	 *
	 * @class Notebook
	 * @extends {Base}
	 */
	class Notebook extends Base {

		/**
		 * Module Namespace
		 *
		 * @readonly
		 * @static
		 * @memberof Notebook
		 */
		static get NS() {
			return "apps.profile.notes.helper.notebook";
		}

		/**
		 * Module Name
		 *
		 * @readonly
		 * @static
		 * @memberof Notebook
		 */
		static get MODULE() {
			return "Profile Notebook";
		}

		/**
		 * Default Options
		 *
		 * @readonly
		 * @static
		 * @memberof Notebook
		 */
		static get DEFAULT_OPTIONS() {
			return {};
		}

		/**
		 * Module init
		 *
		 * @static
		 * 
		 * @param {HTMLElement} 				element		Element representing the notebook
		 * @param {Object} 						options
		 * @param {notes-module/app/notes} 	pnotes		profile Notes instance
		 * 
		 * @returns {Notebook}
		 * 
		 * @memberof Notebook
		 */
		static init(element, options, pnotes) {
			let m = new Notebook(element, options, pnotes);
			m.element.acmsData(Notebook.NS, m);
			
			return m;
		}


		/**
		 * Creates an instance of Notebook.
		 * 
		 * @param {HTMLElement} 				element		Element representing the notebook
		 * @param {Object} 						options
		 * @param {notes-module/app/notes} 	pnotes		profile Notes instance
		 * 
		 * @memberof Notebook
		 */
		constructor(element, options, pnotes) {
			super(element, options);
			this.profilenotes = pnotes;
			this._loaded =
			this._loading =
			this._notes =
			this.button = null;
			this._initialize();
		}

		/**
		 * Get all Notes of the Notebook
		 * 
		 * The getter returns the notes of the notebook. Please Note, 
		 * that the notes have to be loaded first during activation 
		 * of the notebook. otherwise an empty array or null might 
		 * be returned.
		 * 
		 * @returns {Array}	Array of Note Objects
		 *
		 * @memberof Notebook
		 */
		get notes() {
			return this._notes;
		}

		/**
		 * Reset the notes to an empty array
		 *
		 * @memberof Notebook
		 */
		set notes(notes) {
			this._notes = [];
		}

		/**
		 * Is this Notebook currently active in notes window?
		 *
		 * @returns {Boolean}	`true` if the notebook is active
		 * @memberof Notebook
		 */
		isActive() {
			let self = this, 
				pnotes = self.profilenotes,
				po = pnotes.options.classNames;
            return Classes.hasClass(self.element, po.active);
		}

		/**
		 * Deactivate Notebook
		 *
		 * @memberof Notebook
		 */
		deactivate() {
			let self = this, 
				pnotes = self.profilenotes,
				po = pnotes.options.classNames,
				anchor = Selectors.q('.title', self.element);
			Classes.removeClass(self.element, po.active);
			Classes.removeClass(anchor, po.active);
		}

		/**
		 * Activate Notebook
		 *
		 * @param {CallableFunction} cb	Optional Callback to be triggered if loading has been finished
		 * @memberof Notebook
		 */
		activate(cb) {
			let self = this, 
				pnotes = self.profilenotes,
				po = pnotes.options.classNames,
				anchor = Selectors.q('.title', self.element);
			Classes.addClass(self.element, po.active);
			Classes.addClass(anchor, po.active);
			self.profilenotes.currentNotebook = self.getId();
			self.refresh(cb);
		}

		/**
		 * Gets the Notebook Title
		 *
		 * @returns {String}
		 * @memberof Notebook
		 */
		getTitle() {
			let self = this, element = self.element, anchor;
			anchor = Selectors.q('.title', element);

			return anchor.innerHTML;
		}
		
		/**
		 * Gets the Notebook Title
		 *
		 * @returns {String}
		 * @memberof Notebook
		 */
		getId() {
			let self = this, element = self.element, anchor;
			
			return parseInt(element.getAttribute('data-book-id'));
		}
		
        /**
		 * Append Note to Notebook
		 *
		 * @param {Object} data	Response Object of a Note
		 * 
		 * @memberof Notebook
		 */
		appendNote(data) {
            this._makeNote(parseInt(data.id, 10), data);
        }
        
        /**
		 * Remove Note from Notebook
		 *
		 * @param {Number} idx	ID of the Note
		 * 	
		 * @memberof Notebook
		 */
		removeNote(idx) {
            let self = this;
            if(typeof self._notes[idx] !== 'undefined') {
                delete self._notes[idx];
            }
        }
        
        /**
		 * Gets a Note from the Notebook
		 *
		 * @param {Number} idx	ID of the Note
		 * @returns {notes-module/app/helper/note|Boolean}	Note or `false` if not found
		 * @memberof Notebook
		 */
		getNote(idx) {
            let self = this;
            if(typeof self._notes[idx] !== 'undefined') {
                return self._notes[idx];
            }
            
            return false;
		}
		
		/**
		 * Loads all Notes of the Notebook from remote
		 *
		 * @param {CallableFunction} cb	Callback to be triggered if all notes have been loaded
		 * 
		 * @memberof Notebook
		 */
		load(cb) {
			if(true === this._loaded || true === this._loading) {
                return;
			}
			this._loading = true;
            let self = this, o = self.options, fData = new FormData();
            fData.append('op', 'loadnotes');
			fData.append('notebook_id', self.element.getAttribute('data-book-id'));
			if(self.currentOrder) {
				fData.append('order', self.currentOrder);
				fData.append('sort', self.currentSort);
			} else {
				fData.append('order', 'pdate');
				fData.append('sort', 'DESC');
			}
            let onDone = (response) => {
                return self._onLoaded(response, cb);
            };
            require(['http/request'], (Request) => {
                Request.post(o.remote, fData, onDone, self._fail, self._always);
                
            });
		}

		/**
		 * Setup Event Listener
		 *
		 * @memberof Notebook
		 */
		listen() {
			let self = this;
            AcmsEvent.add(self.anchor, 'click', self._onClick);
            if(self.sorter) {
                AcmsEvent.on(self.sorter, 'change', 'input[type="radio"]', self._onChangeSorter);
            }
		}

		/**
		 * On Click Event Handler
		 *
		 * @param {Event} e	On Click Event
		 * 
		 * @returns {Boolean}	always `false`
		 * 
		 * @memberof Notebook
		 */
		_onClick(e) {
			let self = this;
            if(e) {
				e.preventDefault();
				e.stopPropagation();
			}
            if(self._loading === true) {
                return false;
            }
			this.activate();
			return false;
		}

		/**
		 * On Change Sort/Order
		 *
		 * @param {Event} e	Change Event of the sorter
		 * 
		 * @returns {Boolean|undefined}
		 * 
		 * @memberof Notebook
		 */
		_onChangeSorter(e) {
			let self = this, radio = e.target, current;
            if(!self.isActive()) {
                return;
            }
            if(self.updateSort === true) {
                return;
            }
            self.updateSort = true;
            console.log(radio);
            e.stopPropagation();
            e.preventDefault();
            current = radio.value.split('-');
            self.currentSort = current[1].toUpperCase();
            self.currentOrder = current[0];
            self.refresh(() => {
                self.updateSort = false;
            });
            
            return false;
		}

		/**
		 * On Loaded Callback
		 *
		 * @param {Object} 				data	Remote Response Data
		 * @param {CallableFunction} 	cb		optional Callback to be called on finished process
		 * 
		 * @memberof Notebook
		 */
		_onLoaded(data, cb) {
			let self = this, j, t, total = parseInt(data.total), o = self.options;
            self._loaded = true;
            self._loading = false;
            self._notes = [];
            self.notebook.innerHTML = '';
            if(data.status === 'success') {
                for(j = 0; j < total; j++) {
					t = data.notes[j];
					self._makeNote(t.id, t);
                }
            }
			let interval = setInterval(() => {
				if(total === self._notes.length) {
					if(cb)
						cb(self, self.notebook, self._notes, data);
					o.onLoadedNotebook(self, self._notes, self.profilenotes);
					clearInterval(interval);
				}
			}, 10);
		
		}

		/**
		 * Make a Note
		 * 
		 * Creates a Note Obj from Remote Data and appends the Note to the Notebook
		 * 
		 * @param {Number} id	ID of the Note
		 * @param {Object} data	Remote Response Object of a single Note
		 * @memberof Notebook
		 */
		_makeNote(id, data) {
			let self = this, 
				cb = (obj) => {
					self._notes[id] = obj;
				}, 
				options = self.profilenotes.options,
				list = self.notebook
			;
			Note.createFromResponse(data, list, options, self, self.profilenotes, cb);
		}

		/**
		 * Prepare Module 
		 *
		 * @memberof Notebook
		 */
		_prepare() {
			let self = this,
				o = self.options,
				element = self.element, win = Selectors.closest(element, '.window');
            self.window = win;
            self.notebook = Selectors.q('.notebook.open-notes', win);
            self.sorter = Selectors.q('.window-sort-menu', win);
            self.completedlist = Selectors.q('.notebook.completed-notes', win);
            self.anchor = Selectors.q('a', element);
            if(self.sorter) {
                let checked = Selectors.q('input[type="radio"]:checked', self.sorter),
                    current = (checked ? checked.value : 'title-asc').split('-');
                self.currentSort = current[1].toUpperCase();
                self.currentOrder = current[0];
            }
		}

		
        /**
         * Ausführung bei fehlgeschlagener Abfrage
         * 
		 * Wird ausgeführt, wenn keine Antwort vom Server kommt bzw die 
         * Anfrage fehlschlägt
         * 
         * @param {Object}  xhr    		Das XHR Abfrage Object
         * @param {String}	textStatus 	Text Status Message
		 * 
		 * @memberof Notebook
         */
        _fail (xhr, textStatus) {
            A.Logger.writeLog(textStatus);
        }
        
        /**
         * Auführung bei jeder Abfrage
         * 
         * @param {String} textStatus Text Status
         * 
		 * @memberof Notebook
         */
        _always (textStatus) {
            A.Logger.writeLog(textStatus);
			this._processing = false;
			this._loading = false;
        }
        
	}

	return Notebook;
});
