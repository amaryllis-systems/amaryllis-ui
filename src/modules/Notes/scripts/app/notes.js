/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("notes-module/app/notes", [
	"core/base",
	"tools/utils",
	"core/classes",
	"core/selectors",
	"apps/ui/informations/window",
	"http/module-loader",
	"notes-module/app/helper/notebook",
	"notes-module/app/helper/note",
	"templates/template-engine",
	"events/event",
	"core/acms"
], (Base, Utils, Classes, Selectors, WindowBox, ModuleLoader, Notebook, Note, TemplateEngine, AcmsEvent, A) => {

	/**
	 * Profile Notes
	 * 
	 * Class responsible for profile notes
	 * 
	 * @class Notes
	 * @extends {Base}
	 */
	class Notes extends Base {

		/**
		 * Module Namespace
		 *
		 * @readonly
		 * @static
		 * @memberof Notes
		 */
		static get NS() {
			return "apps.profile.notes.notes";
		}

		/**
		 * Module Name
		 *
		 * @readonly
		 * @static
		 * @memberof Notes
		 */
		static get MODULE() {
			return "Profile Notes";
		}

		/**
		 * Custom Stylesheet required for the app
		 *
		 * @readonly
		 * @static
		 * 
		 * @memberof Notes
		 */
		static get css() {
			return "media/css/profile/profile-notes";
		}

		/**
		 * Default Options
		 *
		 * @readonly
		 * @static
		 * @memberof Notes
		 */
		static get DEFAULT_OPTIONS() {
			return {
				remote: null,
				owner: null,
				editorType: null,
				editorName: null,
				selectors: {
					notebook: {
						add: '[data-role="add-notebook"]',
						remove: '[data-role="delete-notebook"]',
						actions: '[data-action="notebook"]',
					},
					note: {
						add: '[data-role="add-note"]'
					},
					window: {
						// dismiss button of the window overlay
						dismissOverlay: '[data-dismiss="window-overlay"]',
						// notebook list of the current profile user
						myNotebooks: '.my-notebooks'
					}
				},
				classNames: {
					active: 'active'
				},
				actions: {
					notebooks: {
						load: 'loadnotebooks',
						add: 'addnotebook',
						remove: 'deletenotebook',
						update: 'updatenotebook'
					},
					note: {
						save: 'savenote',
						remove: 'deletenote'
					}
				},
				fieldMap: {
					op: 'op'
				},
				templates: {
					notebook: {
						// single notebook in sidebar list
						list: 'notes-module/templates/notebook/list-item.hbs',
						// Notebook Form
						form: 'notes-module/templates/notebook/form.hbs',
						remove: 'notes-module/templates/notebook/delete.hbs'
					},
					note: {
						// single note in a notebook
						note: 'notes-module/templates/note/note.hbs',
						// note add/edit form
						form: 'notes-module/templates/note/form.hbs'
					}
				},
				note: {}, // optional overwrite of `labels` and `colors`. Both properties must provide an Object.
				css: Notes.css,
				themecss: false,

				// callbacks

				/**
				 * on loaded/refreshed notebooks callback
				 *
				 * @param {Object} 	notebooks 	All Notebooks in an Object
				 * @param {Notes} 	self		Current Class instance
				 */
				onLoadedNotebooks: (notebooks, self) => {},
				
				/**
				 * on loaded/refreshed notebook callback
				 *
				 * @param {Notebook} 	book 	The Notebook instance
				 * @param {Object} 		notes 	All Notes in an Object
				 * @param {Notes} 		self	Current Class instance
				 */
				onLoadedNotebook: (book, notes, self) => {},

				onChangeNotebook: (oldNotebook, newNotebook) => {}
			};
		}

		/**
		 * Module init
		 *
		 * @static
		 * 
		 * @param {HTMLElement} element		HMTL Element
		 * @param {Object|null} options		Optional custom options
		 * 
		 * @returns	{Notes}	New Insance of Notes App
		 * 
		 * @memberof Notes
		 */
		static init(element, options) {
			let m = new Notes(element, options);
			m.element.acmsData(Notes.NS, m);
			
			return m;
		}


		/**
		 * Creates an instance of Notes.
		 * 
		 * @param {HTMLElement} element		HMTL Element
		 * @param {Object|null} options		Optional custom options
		 * 
		 * @memberof Notes
		 */
		constructor(element, options) {
			super(element, options);
			this._initialize();
			this.refreshMyNotebooks();
		}

		/**
		 * Gets the current active Notebook ID
		 *
		 * @returns {Number} 	ID of the current active Notebook
		 * 
		 * @memberof Notes
		 */
		get currentNotebook() {
			return this._currentNotebook;
		}

		/**
		 * Sets the current active Notebook
		 *
		 * @param {Number}	nbid	ID of the active Notebook
		 * 
		 * @memberof Notes
		 */
		set currentNotebook(nbid) {
			let self = this, o = self.options, current = self.currentNotebook;
			if(current && typeof self.notebooks[current] !== "undefined") {
				self.notebooks[current].deactivate();
			}
			this._toggleActions((nbid > 0));
			this._currentNotebook = parseInt(nbid);
			o.onChangeNotebook(current, this._currentNotebook);
		}

		_toggleActions(activate) {
			let self = this, o = self.options, nbactions;
			nbactions = Selectors.qa(o.selectors.notebook.actions, self.element);
			nbactions.forEach((action) => {
				if(activate === true) {
					Classes.removeClass(action, 'disabled');
					action.removeAttribute('disabled');
				} else {
					Classes.addClass(action, 'disabled');
					action.setAttribute('disabled', 'disabled');
				}
			});
		}

		/**
		 * Loads/refreshs all Notebooks from remote
		 *
		 * @memberof Notes
		 */
		refreshMyNotebooks() {
			let self = this,
				o = self.options,
				fData = new FormData();
            fData.append(o.fieldMap.op, o.actions.notebooks.load);
            require(['http/request'], function(Request) {
                Request.post(o.remote, fData, self._onLoadedMyNotebooks, self._fail, self._always);
            });
		}

		/**
		 * Showing the window overlay
		 *
		 * @memberof Notes
		 */
		showWindowOverlay() {
			let self = this, 
                o = self.options;
            if(!self.overlay) {
                return;
            }
			Classes.toggleClass(self.overlay, 'active');
			self.overlay.setAttribute('aria-hidden', 'false');
		}

		/**
		 * Hiding the window overlay
		 *
		 * @memberof Notes
		 */
		hideWindowOverlay() {
			let self = this, 
                o = self.options;
            if(!self.overlay) {
                return;
            }
			Classes.toggleClass(self.overlay, 'active');
			self.overlay.setAttribute('aria-hidden', 'true');
		}

		/**
		 * Setup Event Listener
		 *
		 * @memberof Notes
		 */
		listen() {
			let self = this, 
				o = self.options, 
				selectors = o.selectors,
				element = self.element;
			
			AcmsEvent.on(element, 'click', selectors.note.add, self._onClickAddNote, true);
			AcmsEvent.on(element, 'click', selectors.notebook.add, self._onClickAddNotebook, true);
			AcmsEvent.on(element, 'click', selectors.notebook.remove, self._onClickDeleteNotebook, true);
			AcmsEvent.on(element, 'click', selectors.window.dismissOverlay, self._onClickDismissOverlay, true);
            
		}

		/**
		 * On click dismiss overlay event handler
		 *
		 * @param {Event} e Click Event
		 * 
		 * @returns
		 * @memberof Notes
		 */
		_onClickDismissOverlay(e) {
			let self = this, 
                o = self.options;
            if(!self.overlay) {
                return;
            }
            if(e)  {
				e.preventDefault();
				e.stopPropagation();
			}
            self.showWindowOverlay();
		}

		/**
		 * on-Click-Delete-Notebook Event Handler
		 *
		 * @param {Event} 	e		Click Event
		 * @memberof Notes
		 */
		_onClickDeleteNotebook(e) {
			let self = this, o = self.options, target = e.target, menu, lists;
            if(!Selectors.matches(target, o.selectors.notebook.remove)) {
                target = Selectors.closest(target, o.selectors.notebook.remove);
			}
			if(!target || ! self.currentNotebook) {
				e.preventDefault();
				e.stopPropagation();
				return false;
			}
			
		}

		/**
		 * On Click Add Notebook Event Handler
		 *
		 * @param {Event} e Click Event
		 * 
		 * @returns {Boolean}	Always false
		 * 
		 * @memberof Notes
		 */
		_onClickAddNotebook(e) {
			e.preventDefault();
            let self = this, o = self.options, target = e.target, menu, lists;
            if(!Selectors.matches(target, o.selectors.notebook.add)) {
                target = Selectors.closest(target, o.selectors.notebook.add);
            }
            if(!target) {
                return false;
            }
			lists = self.windowSideList;
			menu = Selectors.q(o.selectors.window.myNotebooks, lists);
			let item = document.createElement('li'),
				formGroup = document.createElement('div'),
                label = document.createElement('label'),
                field = document.createElement('input');
            field.type = 'text';
            field.name = 'title';
			field.id = 'inline-add-notebook-title';
			field.setAttribute('placeholder', A.Translator._('Name des Notizbuches'));
			field.setAttribute('data-book-id', 0);
			label.innerHTML = A.Translator._('Fügen Sie ein neues Notizbuch hinzu');
			formGroup.appendChild(label);
			formGroup.appendChild(field);
			item.appendChild(formGroup);
			menu.appendChild(item);
			Classes.addClass(item, 'fields wide wide-12');
			Classes.addClass(field, 'field');
			Classes.addClass(label, 'field-label');
			Classes.addClass(formGroup, 'form-group');
			//AcmsEvent.add(field, 'change', self._onChangeNotebookField);
			AcmsEvent.add(field, 'keyup', self._onKeyUpNotebookField);
			self.windowBox.closeMenu();
			field.focus();
            
            return false;
		}

		/**
		 * 
		 *
		 * @param {Event} e
		 * @memberof Notes
		 */
		_onKeyUpNotebookField(e) {
			let self = this, key = e.which || e;
			if(key === 9 || key === 13) {
				return self._onChangeNotebookField(e);
			}
		}

		/**
		 *
		 *
		 * @param {Event} e
		 * @returns
		 * @memberof Notes
		 */
		_onChangeNotebookField(e) {
			e.preventDefault();
			if(this._processing) {
				return;
			}
			this._processing = true;
			let self = this, 
				o = self.options, 
				field = e.target, 
				li = Selectors.closest(field, 'li'),
				nbid = parseInt(field.getAttribute('data-book-id')) || 0,
				op = nbid === 0 ? 'add' : 'update',
				action = o.actions.notebooks[op],
				lists = self.windowSideList,
				menu = Selectors.q(o.selectors.window.myNotebooks, lists),
				fData = new FormData(),
				cb = (data) => {
					if(data.status === 'success') {
						menu.removeChild(li);
						self.refreshMyNotebooks();
					} else {
						let parent = Selectors.closest(field, '.form-group');
						let error = document.createElement('div');
						let p = document.createElement('p');
						error.appendChild(p);
						p.innerHTML = data.message;
						parent.appendChild(error);
						Classes.addClass(error, 'danger bordered alert v3');
						setTimeout(() => {
							parent.removeChild(error);
						}, 3000);
					}
					setTimeout(() => {
						self._processing = false;
					}, 100);
				};
			if(field.value.trim() === '') {
				console.log('empty');
				cb({status: 'success'});
				return;
			}
			fData.append('op', action);
			fData.append(field.name, field.value);
			fData.append('notebook_id', nbid);
			require(['http/request'], (Request) => {
				Request.post(o.remote, fData, cb, self._fail, self._always);
			});

		}
		
		/**
		 * On Loaded Notebooks Callback
		 *
		 * @param {Object} result	Response Object from remote
		 * @returns
		 * @memberof Notes
		 */
		_onLoadedMyNotebooks(result) {
			let self = this, 
                o = self.options, 
                notebooks, l, i, book, first,
                lists = self.windowSideList, my = Selectors.q(o.selectors.window.myNotebooks, lists),
                badge = Selectors.q('.profile-notebooks > a > .badge', lists);
            if(result.status !== 'success') {
                // @todo - proper error handling
                return;
            }
            self.notebooks = {};
            notebooks = result.notebooks;
			l = notebooks.length;
			self.totalNotebooks = l;
            if(badge) badge.textContent = parseInt(result.total) || 0;
			first = true;
			my.innerHTML = '';
            for(i = 0; i < l; i++) {
				book = notebooks[i];
				let active = (first === true && ! self.currentNotebook) || (self.currentNotebook == book.id);
                
                if(active) {
                    self.currentNotebook = book.id;
				}
				self._appendNotebook(book, active, my);
                
                first = false;
            }
		}

		
		/**
		 *
		 *
		 * @param {HTMLElement} overlay
		 * @param {HTMLElement} trigger
		 * 
		 * @memberof Notes
		 */
		_onOverlayOpen(overlay, trigger, wbox, woverlay) {
        }
		
        /**
		 * Window Callback Event "Before Add overlay open"
		 *
		 * @param {HTMLElement} overlay		Window overlay
		 * @param {HTMLElement} trigger		Trigger that has been used to open the overlay
		 * 
		 * @memberof Notes
		 */
		_beforeAddOverlayOpen(overlay, trigger, wbox, woverlay) {
            let self = this, o = self.options, ele, inner, overlayContent;
            overlayContent = Selectors.q('.overlay-content', overlay);
            let cb = function(result) {
                return self._onCreateNote(result, overlayContent);
            };
            overlay.acmsData('notebook', self.currentNotebook);
            require(['http/request', 'notifier'], function(Request) {
				let fData = new FormData();
				fData.append(o.fieldMap.op, o.actions.note.save);
				fData.append('notebook_id', self.currentNotebook);
                Request.post(o.remote, fData, cb, self._fail, self._always);
            });
		}

        /**
		 * Window Callback Event "Before Edit overlay open"
		 *
		 * @param {HTMLElement} overlay		Window overlay
		 * @param {HTMLElement} trigger		Trigger that has been used to open the overlay
		 * 
		 * @memberof Notes
		 */
        _beforeEditOverlayOpen(overlay, trigger, wbox, woverlay) {
            let self = this, o = self.options, ele, inner, overlayContent;
            overlayContent = Selectors.q('.overlay-content', overlay);
            let note = Selectors.closest(trigger, '.profile-note'),
				noteObj = note.acmsData(Note.NS),
				model = noteObj.getData();
            
			let template = o.templates.note.form, 
				data = {
					note: model,
					editor: {
						type: o.editorType.toLowerCase(),
						name: o.editorName.toLowerCase()
					}
				},
				callback = () => {
					let title = Selectors.q('input[name="title"]', overlayContent),
						body = Selectors.q('textarea[name="body"]', overlayContent),
						id = Selectors.q('input[name="id"]', overlayContent),
						nb = Selectors.q('input[name="notebook_id"]', overlayContent);
					ModuleLoader.load(overlayContent);
					AcmsEvent.add(body, 'change', self._onChangeNoteForm);
					AcmsEvent.add(title, 'change', self._onChangeNoteForm);
					AcmsEvent.add(body, 'blur', self._onChangeNoteForm);
					AcmsEvent.add(title, 'blur', self._onChangeNoteForm);
				};
			TemplateEngine.compileResource(template, data, callback, overlayContent);
            overlay.acmsData('notebook', model.notebook_id);
		}

		_beforeDeleteNotebookOverlayOpen(overlay, trigger, wbox, woverlay) {
			let self = this, o = self.options, overlayContent;
            overlayContent = Selectors.q('.overlay-content', overlay);
            let nbid = self.currentNotebook,
				notebookObj = self.notebooks[nbid];
            
			let template = o.templates.notebook.remove, 
				data = {
					notebook: {
						title: notebookObj.getTitle(),
						id: notebookObj.getId()
					},
					form: {
						op: o.actions.notebooks.remove,
						field: o.fieldMap.op
					}
				},
				callback = () => {
					let form = Selectors.q('form', overlayContent);
					let cb = (e) => {
						e.preventDefault();
						e.stopPropagation();
						let fData = new FormData(form);
						require(["http/request", "notifier"], (Request) => {
							Request.post(o.remote, fData, self._onDeletedNotebook, self._fail, self._always);
						});
					}, dismiss;
					AcmsEvent.on(form, 'click', 'button[type="submit"]', cb, true);
					ModuleLoader.load(overlayContent);
					
				};
			TemplateEngine.compileResource(template, data, callback, overlayContent);
		}

		_onDeletedNotebook(response) {
			if(response.status === 'success') {
				let self = this, 
					o = self.options, 
					overlay = Selectors.q('.window-overlay', self.windowOverlay),
					overlayContent = Selectors.q('.overlay-content', overlay);
				if(overlayContent) overlayContent.innerHTML = '';
				self.currentNotebook = 0;
				self.hideWindowOverlay();
				self.refreshMyNotebooks();
			} else {
				A.Notifications.createFromResponse(response);
			}
		}
		
		/**
		 * On Change Note Form Field Event Handler
		 *
		 * @param {Event} e		Change/Blur Event
		 * @memberof Notes
		 */
		_onChangeNoteForm(e) {
			let form = Selectors.closest(e.target, 'form'), self = this, o = self.options, fData = new FormData(form);
			require(['http/request'], (Request) => {
				Request.post(o.remote, fData, function(response){}, self._fail, self._always);
			});
		}

        /**
		 * Window Callback Event "Before overlay open"
		 *
		 * @param {HTMLElement} overlay		Window overlay
		 * @param {HTMLElement} trigger		Trigger that has been used to open the overlay
		 * 
		 * @memberof Notes
		 */
        _beforeOverlayOpen(overlay, trigger, wbox, woverlay) {
            let self = this, o = self.options, matches, t1, t2, t3;
            t1 = Selectors.closest(trigger, '[data-role="note-add"]');
			
            matches = (Selectors.matches(trigger, '[data-role="note-add"]') || (null !== t1));
            if(matches) {
				self._beforeAddOverlayOpen(overlay, trigger, wbox, woverlay);
				return true;
			}
			t2 = Selectors.closest(trigger, '[data-role="note-edit"]');
            matches = (Selectors.matches(trigger, '[data-role="note-edit"]') || (null !== t2));
            if(matches) {
				self._beforeEditOverlayOpen(overlay, trigger, wbox, woverlay);
				return true;
			}
			t2 = Selectors.closest(trigger, o.selectors.notebook.remove);
			matches = (Selectors.matches(trigger, o.selectors.notebook.remove) || (null !== t2));
			if(matches) {
				self._beforeDeleteNotebookOverlayOpen(overlay, trigger, wbox, woverlay);
			}
            return true;
        }
        
        /**
		 * Window Callback Event "Before overlay dismiss"
		 *
		 * @param {HTMLElement} overlay		Window overlay
		 * @param {HTMLElement} trigger		Trigger that has been used to close the overlay
		 * 
		 * @memberof Notes
		 */
        _beforeOverlayDismiss(overlay, trigger, wbox, woverlay) {
            let self = this, nbid = parseInt(overlay.acmsData('notebook'), 10);
            if(nbid && typeof self.notebooks[nbid] !== 'undefined') {
                self.notebooks[nbid].refresh();
            }
            return true;
        }
        
        /**
		 * Window Callback Event "On overlay close"
		 *
		 * @param {HTMLElement} overlay		Window overlay
		 * @param {HTMLElement} trigger		Trigger that has been used to close the overlay
		 * 
		 * @memberof Notes
		 */
        _onOverlayClose(overlay, trigger, wbox, woverlay) {
            let overlayContent;
			if(window.tinyMCE) {
				let ed = window.tinyMCE.get('profile-note-body');
				if(ed) ed.remove();
			}
            overlayContent = Selectors.q('.overlay-content', overlay);
            overlayContent.innerHTML = '';
        }
        
        /**
		 * On Create Note Callback
		 *
		 * @param {Object} 			result			Result Response Object from Remote
		 * @param {HTMLElement} 	overlayContent	Overlay Content Element
		 * 
		 * @memberof Notes
		 */
		_onCreateNote(result, overlayContent) {
			let model = result.model, 
				self = this, 
				o = self.options;
			let nbid = parseInt(model.notebook_id, 10);
            if(typeof self.notebooks[nbid] !== 'undefined') {
                self.notebooks[nbid].appendNote(model);
            }
			let template = o.templates.note.form, 
				data = {
					note: model,
					editor: {
						type: o.editorType.toLowerCase(),
						name: o.editorName.toLowerCase()
					}
				},
				callback = () => {
					let title = Selectors.q('input[name="title"]', overlayContent),
						body = Selectors.q('textarea[name="body"]', overlayContent),
						id = Selectors.q('input[name="id"]', overlayContent),
						nb = Selectors.q('input[name="notebook_id"]', overlayContent);
					ModuleLoader.load(overlayContent);
					AcmsEvent.add(body, 'change', self._onChangeNoteForm);
					AcmsEvent.add(title, 'change', self._onChangeNoteForm);
					AcmsEvent.add(body, 'blur', self._onChangeNoteForm);
					AcmsEvent.add(title, 'blur', self._onChangeNoteForm);
				};
			TemplateEngine.compileResource(template, data, callback, overlayContent);
            self.windowBox.overlay.acmsData('notebook', nbid);
        }

		/**
		 * Appen a notebook to the list
		 *
		 * @param {Object} 		book	Data of the book from remote server
		 * @param {Boolean} 	active	Currently active?
		 * @param {HTMLElement} list	List Element or Wrapper for the Notebooks
		 * @param {Boolean} 	shared	`true` if the notebook is a shared notebook
		 * 
		 * @memberof Notes
		 */
		_appendNotebook(book, active, list, shared) {
			let self = this, 
				o = self.options,
				template = o.templates.notebook.list,
				data = {
					book: book,
					active: active,
					shared: shared
				},
				callback = () => {
					let li = Selectors.q('.notebook-item[data-book-id="'+book.id+'"]', list);
					let nb = Notebook.init(li, o, self);
					if(true === active) {
						nb.activate();
					}
					self.notebooks[parseInt(book.id)] = nb;
					if(Object.keys(self.notebooks).length === self.totalNotebooks) {
						o.onLoadedNotebooks(self);
					}
				};
			TemplateEngine.compileResource(template, data, callback, list);
		}

		/**
		 * prepare Class instance
		 *
		 * @memberof Notes
		 */
		_prepare() {
			let self = this, 
                o = self.options,
                element = self.element;
            self.remote = o.remote;
            self.windowOverlay = Classes.hasClass(element, 'window') ? element : Selectors.q('.window', element);
            self._prepareWindowOverlay();
            self.overlay = Selectors.q('.window-overlay', self.windowOverlay);
            self.dismissOverlay = Selectors.qa('[data-dismiss="window-overlay"]', self.windowOverlay);
		}

		/**
		 * Prepare Window Overlay Module
		 *
		 * @memberof Notes
		 */
		_prepareWindowOverlay() {
			let self = this, 
                o = self.options,
				element = self.element;
			let windowOptions = {
				owner: o.owner,
				overlayOptions: {
					beforeDismiss: function(overlay, trigger, wbox, woverlay) {
						return self._beforeOverlayDismiss(overlay, trigger, wbox, woverlay);
					},
					onDismiss: function(overlay, trigger, wbox, woverlay) {
						self._onOverlayClose(overlay, trigger, wbox, woverlay);
					},
					beforeOpen: function(overlay, trigger, wbox, woverlay) {
						return self._beforeOverlayOpen(overlay, trigger, wbox, woverlay);
					},
					onOpen: function(overlay, trigger, wbox, woverlay) {
						self._onOverlayOpen(overlay, trigger, wbox, woverlay);
					}
				}
			};
			self.windowBox = WindowBox.init(self.windowOverlay, windowOptions);
			self.windowMenu = Selectors.q('.window-menu', self.windowOverlay);
			self.windowSideList = Selectors.q('.side-list', self.windowOverlay);
		}

		
        /**
         * Ausführung bei fehlgeschlagener Abfrage
         * 
		 * Wird ausgeführt, wenn keine Antwort vom Server kommt bzw die 
         * Anfrage fehlschlägt
         * 
         * @param {Object}  xhr    		Das XHR Abfrage Object
         * @param {String}	textStatus 	Text Status Message
		 * 
		 * @memberof Notes
         */
        _fail (xhr, textStatus) {
            A.Logger.writeLog(textStatus);
        }
        
        /**
         * Auführung bei jeder Abfrage
         * 
         * @param {String} textStatus Text Status
         * 
		 * @memberof Notes
         */
        _always (textStatus) {
            A.Logger.writeLog(textStatus);
            this._processing = false;
        }
        

	}

	return Notes;
});
