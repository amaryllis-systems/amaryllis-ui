/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("steuer-module/app/steuer-table", [
	"core/base",
	"tools/utils",
	"core/classes",
	"core/selectors",
	"events/event",
	"core/acms"
], (Base, Utils, Classes, Selectors, AcmsEvent, A) => {

	/**
	 * Steuer Table to manage tax entries
	 *
	 * @class SteuerTable
	 * 
	 * @extends {Base}
	 */
	class SteuerTable extends Base {

		/**
		 * Module Namespace
		 *
		 * @readonly
		 * @static
		 * @memberof SteuerTable
		 */
		static get NS() {
			return "steuer-module.app.steuer-table";
		}

		/**
		 * Module Name
		 *
		 * @readonly
		 * @static
		 * @memberof SteuerTable
		 */
		static get MODULE() {
			return "Steuer Table";
		}

		/**
		 * Default Options
		 *
		 * @readonly
		 * @static
		 * @memberof SteuerTable
		 */
		static get DEFAULT_OPTIONS() {
			return {
				remote: null,
				classNames: {

				},
			};
		}

		/**
		 * Module Init
		 *
		 * @static
		 * 
		 * @param {HTMLElement} 	element		Wrapper Element
		 * @param {Object} 			options		Custom Options
		 * 
		 * @returns {SteuerTable}	New instance
		 * 
		 * @memberof SteuerTable
		 */
		static init(element, options) {
			let m = new SteuerTable(element, options);
			m.element.acmsData(SteuerTable.NS, m);

			return m;
		}

		/**
		 * Creates an instance of SteuerTable.
		 * 
		 * @param {HTMLElement} 	element		Wrapper Element
		 * @param {Object} 			options		Custom Options
		 * 
		 * @memberof SteuerTable
		 */
		constructor(element, options) {
			super(element, options);
			this._initialize();

		}


		_prepare() {}

	}

	return SteuerTable;
});
