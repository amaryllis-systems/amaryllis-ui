/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


define("glossary-module/app/glossary", [
	"core/base",
	"core/classes",
	"core/selectors",
	"events/event",
	"core/acms"
], (Base, Classes, Selectors, AcmsEvent, A) => {

	/**
	 * Glossary
	 *
	 * @class Glossary
	 * @extends {Base}
	 */
	class Glossary extends Base {

		/**
		 * Module Namespace
		 *
		 * @readonly
		 * @static
		 * @memberof Glossary
		 */
		static get NS() {
			return "glossary-module.app.glossary";
		}

		/**
		 * Module Name
		 *
		 * @readonly
		 * @static
		 * @memberof Glossary
		 */
		static get MODULE() {
			return "Glossary";
		}

		/**
		 * Default Options
		 *
		 * @readonly
		 * @static
		 * @memberof Glossary
		 */
		static get DEFAULT_OPTIONS() {
			return {
				remote: null,
				selectors: {
					active: '.active',
					disabled: 'disabled',
					search: '[data-role="glossary-search"]',
					searchresult: '[data-role="glossary-searchresult"]',
					nav: '[data-role="glossary-nav"]',
					letter: '[data-role="letter-collection"]',
					
					terms: {
						term: '[data-role="glossary-term"]',
						remove: '[data-role="delete-term"]'
					},
					altterms: {
						term: '[data-role="glossary-altterm"]',
						remove: '[data-role="delete-altterm"]'
					}
				},
				classNames: {
					active: 'active',
					disabled: 'disabled'
				},
				actions: {
					terms: {
						remove: 'dodelete'
					},
					altterms: {
						remove: 'removeterm'
					}
				}
			};
		}

		/**
		 * Module Init
		 *
		 * @static
		 * 
		 * @param {HTMLElement} element		Wrapper Element
		 * @param {Object} 		options		Custom Options
		 * 
		 * @returns {Glossary}	New Instance
		 * 
		 * @memberof Glossary
		 */
		static init(element, options) {
			let m;
			try {
				m = new Glossary(element, options);
				m.element.acmsData(Glossary.NS, m);
			} catch(E) {
				console.log(E.message);
				console.log(E.stack);
				console.log(E.lineNumber+'::'+E.columnNumber);

				throw E;
			} finally {
				return m;
			}
		}

		/**
		 * Creates an instance of Glossary.
		 * 
		 * @param {HTMLElement} element		Wrapper Element
		 * @param {Object} 		options		Custom Options
		 * 
		 * @memberof Glossary
		 */
		constructor(element, options) {
			super(element, options);
			this._initialize();
		}

		get search() {
			return this._search;
		}
		
		get searchResult() {
			return this._searchResults;
		}

		get currentLetter() {
			return this._currentLetter;
		}

		set currentLetter(letter) {
			this._currentLetter = letter;
		}

		get processing() {
			return this._processing;
		}

		set processing(flag) {
			this._processing = (true === flag);
		}

		listen() {
			let self = this,
				o = self.options,
				element = self.element,
				sel = o.selectors;
			AcmsEvent.on(element, 'click', sel.nav, self._onClickNavItem, true);
			AcmsEvent.on(element, 'click', sel.terms.remove, self._onClickDeleteTerm, true);
			AcmsEvent.on(element, 'click', sel.altterms.remove, self._onClickDeleteAltTerm, true);
			if(self.search) {
				AcmsEvent.add(self.search, 'keydown', self._onKeyDown);
				AcmsEvent.add(self.search, 'focus', self._onFocus);
				AcmsEvent.add(self.search, 'blur', self._onBlur);
			}
		}

		_onClickDeleteTerm(e) {
			e.preventDefault(e);
			let self = this,
				o = self.options,
				action = o.actions.terms.remove,
				fData = new FormData(),
				term;
			if(self.processing) {
				return;
			}
			self.processing = true;
			term = Selectors.closest(e.target, o.selectors.terms.term);
			if(!term) {
				self.processing = false;
				return;
			}
			fData.append('op', action);
			fData.append('token', o.token);
			self._currentItem = term;
			fData.append('entry_id', term.acmsData('id'));
			require(['http/request', 'notifier'], (Request) => {
				Request.post(o.remote, fData, self._onDoneDeleteTerm, self._fail, self._always);
			});
		}
		
		_onClickDeleteAltTerm(e) {
			e.preventDefault(e);
			let self = this,
				o = self.options,
				action = o.actions.altterms.remove,
				fData = new FormData(),
				term;
			if(self.processing) {
				return;
			}
			self.processing = true;
			term = Selectors.closest(e.target, o.selectors.altterms.term);
			if(!term) {
				self.processing = false;
				return;
			}
			fData.append('op', action);
			self._currentItem = term;
			fData.append('entry_id', term.acmsData('entry'));
			fData.append('altterm_id', term.acmsData('id'));
			fData.append('token', o.token);
			require(['http/request', 'notifier'], (Request) => {
				Request.post(o.remote, fData, self._onDoneDeleteTerm, self._fail, self._always);
			});
		}

		_onDoneDeleteTerm(response) {
			this.processing = false;
			if(response.status !== 'success') {
				return A.Notifications.createFromResponse(response);
			}
			let self = this;
			Classes.addClass(self._currentItem, 'fade');
			setTimeout(() => {
				self._currentItem.parentNode.removeChild(self._currentItem);
			}, 400);
		}

		_onBlur() {
			let self = this,
			o = self.options,
			c = o.classNames;
			Classes.removeClass(self.search, 'focus');
		}

		_onFocus() {
			let self = this,
			o = self.options,
			c = o.classNames;
			Classes.addClass(self.search, 'focus');
		}

		/**
		 * On KeyPress Search Event Handler
		 *
		 * @param {Event} e
		 * @memberof Glossary
		 */
		_onKeyDown(e) {
			let self = this,
				o = self.options,
				element = self.element,
				sel = o.selectors,
				result = Selectors.q('.letter-entries', self.searchResult),
				key = e.which || e.keyCode,
				val = e.target.value,
				lower = val.toLowerCase();
			if(key === 13 || key === 9) {
				if(val.length === 0) {
					self._hideSearchResults(true);
				}
				e.preventDefault();
				return;
			}
			self._showSearchResults();
			result.innerHTML = "";
			if(val.length) {
				let entries = Selectors.qa(sel.terms.term, element);
				let len = entries.length, j, entry;
				for(j = 0; j < len; j++) {
					entry = entries[j];
					let term = entry.acmsData('term').replace(/\s+/g, ' ').toLowerCase();
					if(!~term.indexOf(lower)) {
						let altterms = Selectors.q(sel.altterms.term, entry),
							l = altterms.length, k, altterm;
						if(!altterms.length) {
							continue;
						}
						for(k = 0; k < l; k++) {
							altterm = altterms[k];
							let aterm = altterm.acmsData('term').replace(/\s+/g, ' ').toLowerCase();
							if(!~aterm.indexOf(lower)) {
								continue;
							} else {
								self._cloneEntry(entry, result);
								break;
							}
						}
					} else {
						self._cloneEntry(entry, result);
					}
				}
			}
		}

		/**
		 * Clone Entry while search
		 *
		 * @param {HTMLElement} entry
		 * @param {HTMLElement} result
		 * 
		 * @memberof Glossary
		 */
		_cloneEntry(entry, result) {
			let klon = entry.cloneNode(false);
			klon.innerHTML = entry.innerHTML;
			Classes.addClass(klon, 'fade');
			result.appendChild(klon);
			setTimeout(() => {
				Classes.addClass(klon, 'in');
			}, 200);
		}

		/**
		 * On Click Nav-Item Event Handler
		 *
		 * @param {Event} e
		 * @memberof Glossary
		 */
		_onClickNavItem(e) {
			e.preventDefault();
			e.stopPropagation();
			let self = this,
				o = self.options,
				element = self.element,
				sel = o.selectors,
				c = o.classNames,
				target = e.target;
			if(!Selectors.matches(target, sel.nav)) {
				target = Selectors.closest(target, sel.nav);
			}
			if(!target) {
				return false;
			}
			if(Classes.hasClass(target, c.active)) {
				return false;
			}
			if(Classes.hasClass(target, c.disabled)) {
				return false;
			}
			self._hideSearchResults();
			let current = Selectors.q(sel.nav+sel.active, element);
			if(current) {
				Classes.removeClass(current, c.active);
			}
			let currentLetter = Selectors.q(sel.letter+sel.active, element);
			if(currentLetter) {
				Classes.removeClass(currentLetter, c.active);
			}
			Classes.addClass(target, c.active);
			let letter = target.acmsData('letter');
			self.currentLetter = letter;
			let s = sel.letter+'[data-letter='+letter+']';
			let next = Selectors.q(s, element);
			if(next) {
				Classes.addClass(next, c.active);
			}
		}

		_showSearchResults() {
			let self = this,
				o = self.options,
				element = self.element,
				sel = o.selectors,
				c = o.classNames;
			if(!Classes.hasClass(self.searchResult, c.active)) {
				let actives = Selectors.qa(sel.active, element);
				actives.forEach((item) => {
					Classes.removeClass(item, c.active);
				});
				Classes.addClass(self.searchResult, c.active);
			}
		}

		/**
		 * Hide Search Result
		 *
		 * @param {Boolean} restoreLetter	Restore Previous letter?
		 * @memberof Glossary
		 */
		_hideSearchResults(restoreLetter) {
			let self = this,
				o = self.options,
				element = self.element,
				sel = o.selectors,
				result = Selectors.q('.letter-entries', self.searchResult),
				c = o.classNames;
			self.search.value = "";
			AcmsEvent.fireChange(self.search);
			if(Classes.hasClass(self.searchResult, c.active)) {
				Classes.removeClass(self.searchResult, c.active);
				setTimeout(() => {
					result.innerHTML = "";
				}, 400);
				if(!self.currentLetter || !restoreLetter) return;
				let s = sel.letter+'[data-letter='+self.currentLetter+']';
				let next = Selectors.q(s, element);
				if(next) Classes.addClass(next, c.active);
				let nextNav = Selectors.q(sel.nav+'[data-letter='+self.currentLetter+']', element);
				if(nextNav) Classes.addClass(nextNav, c.active);
			}
		}

		/**
		 * Prepare Module
		 *
		 * @memberof Glossary
		 */
		_prepare() {
			let self = this,
				o = self.options,
				element = self.element,
				sel = o.selectors;
			self._search = Selectors.q(sel.search, element);
			self._searchResults = Selectors.q(sel.searchresult, element);
			let current = Selectors.q(sel.nav+sel.active, element);
			if(current) {
				self.currentLetter = current.acmsData('letter');
			}
		}

		/**
		 * On XHR Fail Callback
		 *
		 * @param {Object} 		xhr				XHR Request
		 * @param {string} 		textStatus		XHR Status Message
		 * 
		 * @memberof Glossary
		 */
		_fail(xhr, textStatus) {
			A.Logger.writeLog(textStatus);
			this.processing = false;
		}
		

        /**
		 * On Always XHR Callback
		 *
		 * @param {Object} 		xhr				XHR Request
		 * @param {string} 		textStatus		XHR Status Message
		 * 
		 * @memberof Glossary
		 */
		_always(xhr, textStatus) {
            A.Logger.writeLog(textStatus);
		}
		
	}

	return Glossary;
});
