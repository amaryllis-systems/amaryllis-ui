/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("tasks-module/app/tasks", [
	"core/base",
	"core/classes",
	"core/selectors",
	"apps/ui/informations/window",
	"http/module-loader",
	"tasks-module/app/helper/tasklist",
	"tasks-module/app/helper/task",
	"templates/template-engine",
	"events/event",
	"core/acms"
 ], (Base, Classes, Selectors, WindowBox, ModuleLoader, Tasklist, Task, TemplateEngine, AcmsEvent, A) => {
 
	/**
	 * Profile Tasks
	 *
	 * @class Tasks
	 * @extends {Base}
	 */
	class Tasks extends Base {

		/**
		 * Module Name
		 *
		 * @readonly
		 * @static
		 * @memberof Tasks
		 */
		static get MODULE() {
			return "Profile Tasks";
		}

		/**
		 * Module Namespace
		 *
		 * @readonly
		 * @static
		 * @memberof Tasks
		 */
		static get NS() {
			return "tasks-module.app.tasks";
		}

		/**
		 * Custom Stylesheet required for the app
		 *
		 * @readonly
		 * @static
		 * 
		 * @memberof Tasks
		 */
		static get css() {
			return "modules/Tasks/media/css/profile/profile-tasks";
		}

		/**
		 * Default Options
		 *
		 * @readonly
		 * @static
		 * @memberof Tasks
		 */
		static get DEFAULT_OPTIONS() {
			return {
				remote: null,
				owner: null,
				editorType: null,
				editorName: null,
				selectors: {
					tasklist: {
						add: '[data-role="add-tasklist"]',
						remove: '[data-role="delete-tasklist"]',
						actions: '[data-action="tasklist"]',
					},
					task: {
						add: '[data-role="add-task"]'
					},
					window: {
						// dismiss button of the window overlay
						dismissOverlay: '[data-dismiss="window-overlay"]',
						// tasklist list of the current profile user
						myTasklists: '.my-tasklists'
					}
				},
				classNames: {
					active: 'active'
				},
				actions: {
					tasklists: {
						load: 'loadtasklists',
						add: 'addlist',
						remove: 'deletelist',
						update: 'updatelist'
					},
					task: {
						save: 'savetask',
						add: 'addtask',
						update: 'updatetask',
						remove: 'deletetask',
						move: 'movetask'
					}
				},
				fieldMap: {
					op: 'op'
				},
				templates: {
					tasklist: {
						// single tasklist in sidebar list
						list: 'tasks-module/templates/tasklist/list-item.hbs',
						// Tasklist Form
						form: 'tasks-module/templates/tasklist/form.hbs',
						remove: 'tasks-module/templates/tasklist/delete.hbs'
					},
					task: {
						// single task in a tasklist
						task: 'tasks-module/templates/task/task.hbs',
						// task add/edit form
						form: 'tasks-module/templates/task/form.hbs'
					}
				},
				task: {}, // optional overwrite of `labels` and `colors`. Both properties must provide an Object.
				css: Tasks.css,
				themecss: false,

				// callbacks

				/**
				 * on loaded/refreshed tasklists callback
				 *
				 * @param {Object} 	tasklists 	All Tasklists in an Object
				 * @param {Tasks} 	self		Current Class instance
				 */
				onLoadedTasklists: (tasklists, self) => {},
				
				/**
				 * on loaded/refreshed tasklist callback
				 *
				 * @param {Tasklist} 	tlist 	The Tasklist instance
				 * @param {Object} 		tasks 	All Tasks in an Object
				 * @param {Tasks} 		self	Current Class instance
				 */
				onLoadedTasklist: (tlist, tasks, self) => {},

				onChangeTasklist: (oldTasklist, newTasklist) => {}
			};
		}

		/**
		 * Module init
		 *
		 * @static
		 * 
		 * @param {HTMLElement} element		HMTL Element
		 * @param {Object|null} options		Optional custom options
		 * 
		 * @returns	{Tasks}	New Insance of Tasks App
		 * 
		 * @memberof Tasks
		 */
		static init(element, options) {
			let m = new Tasks(element, options);
			m.element.acmsData(Tasks.NS, m);
			
			return m;
		}


		/**
		 * Creates an instance of Tasks.
		 * 
		 * @param {HTMLElement} element		HMTL Element
		 * @param {Object|null} options		Optional custom options
		 * 
		 * @memberof Tasks
		 */
		constructor(element, options) {
			super(element, options);
			this._initialize();
			this.refreshMyTasklists();
		}

		/**
		 * Gets the current active Tasklist ID
		 *
		 * @returns {Number} 	ID of the current active Tasklist
		 * 
		 * @memberof Tasks
		 */
		get currentTasklist() {
			return this._currentTasklist;
		}

		/**
		 * Sets the current active Tasklist
		 *
		 * @param {Number}	nbid	ID of the active Tasklist
		 * 
		 * @memberof Tasks
		 */
		set currentTasklist(nbid) {
			let self = this, o = self.options, current = self.currentTasklist;
			if(current && typeof self.tasklists[current] !== "undefined") {
				self.tasklists[current].deactivate();
			}
			this._toggleActions((nbid > 0));
			this._currentTasklist = parseInt(nbid);
			o.onChangeTasklist(current, this._currentTasklist);
		}

		_toggleActions(activate) {
			let self = this, o = self.options, nbactions;
			nbactions = Selectors.qa(o.selectors.tasklist.actions, self.element);
			nbactions.forEach((action) => {
				if(activate === true) {
					Classes.removeClass(action, 'disabled');
					action.removeAttribute('disabled');
				} else {
					Classes.addClass(action, 'disabled');
					action.setAttribute('disabled', 'disabled');
				}
			});
		}

		/**
		 * Loads/refreshs all Tasklists from remote
		 *
		 * @memberof Tasks
		 */
		refreshMyTasklists() {
			let self = this,
				o = self.options,
				fData = new FormData();
            fData.append(o.fieldMap.op, o.actions.tasklists.load);
            require(['http/request'], function(Request) {
                Request.post(o.remote, fData, self._onLoadedMyTasklists, self._fail, self._always);
            });
		}

		/**
		 * Showing the window overlay
		 *
		 * @memberof Tasks
		 */
		showWindowOverlay() {
			let self = this, 
                o = self.options;
            if(!self.overlay) {
                return;
            }
			Classes.toggleClass(self.overlay, 'active');
			self.overlay.setAttribute('aria-hidden', 'false');
		}

		/**
		 * Hiding the window overlay
		 *
		 * @memberof Tasks
		 */
		hideWindowOverlay() {
			let self = this, 
                o = self.options;
            if(!self.overlay) {
                return;
            }
			Classes.toggleClass(self.overlay, 'active');
			self.overlay.setAttribute('aria-hidden', 'true');
		}

		/**
		 * Setup Event Listener
		 *
		 * @memberof Tasks
		 */
		listen() {
			let self = this, 
				o = self.options, 
				selectors = o.selectors,
				element = self.element;
			
			AcmsEvent.on(element, 'click', selectors.task.add, self._onClickAddTask, true);
			AcmsEvent.on(element, 'click', selectors.tasklist.add, self._onClickAddTasklist, true);
			AcmsEvent.on(element, 'click', selectors.tasklist.remove, self._onClickDeleteTasklist, true);
			AcmsEvent.on(element, 'click', selectors.window.dismissOverlay, self._onClickDismissOverlay, true);
            
		}

		/**
		 * On click dismiss overlay event handler
		 *
		 * @param {Event} e Click Event
		 * 
		 * @returns
		 * @memberof Tasks
		 */
		_onClickDismissOverlay(e) {
			let self = this, 
                o = self.options;
            if(!self.overlay) {
                return;
            }
            if(e)  {
				e.preventDefault();
				e.stopPropagation();
			}
            self.showWindowOverlay();
		}

		/**
		 * on-Click-Delete-Tasklist Event Handler
		 *
		 * @param {Event} 	e		Click Event
		 * @memberof Tasks
		 */
		_onClickDeleteTasklist(e) {
			let self = this, o = self.options, target = e.target, menu, lists;
            if(!Selectors.matches(target, o.selectors.tasklist.remove)) {
                target = Selectors.closest(target, o.selectors.tasklist.remove);
			}
			if(!target || ! self.currentTasklist) {
				e.preventDefault();
				e.stopPropagation();
				return false;
			}
			
		}

		/**
		 * On Click Add Tasklist Event Handler
		 *
		 * @param {Event} e Click Event
		 * 
		 * @returns {Boolean}	Always false
		 * 
		 * @memberof Tasks
		 */
		_onClickAddTasklist(e) {
			e.preventDefault();
            let self = this, o = self.options, target = e.target, menu, lists;
            if(!Selectors.matches(target, o.selectors.tasklist.add)) {
                target = Selectors.closest(target, o.selectors.tasklist.add);
            }
            if(!target) {
                return false;
            }
			lists = self.windowSideList;
			menu = Selectors.q(o.selectors.window.myTasklists, lists);
			let item = document.createElement('li'),
				formGroup = document.createElement('div'),
                label = document.createElement('label'),
                field = document.createElement('input');
            field.type = 'text';
            field.name = 'title';
			field.id = 'inline-add-tasklist-title';
			field.setAttribute('placeholder', A.Translator._('Name der Taskliste'));
			field.setAttribute('data-tlist-id', 0);
			label.innerHTML = A.Translator._('Fügen Sie eine neue Taskliste hinzu');
			formGroup.appendChild(label);
			formGroup.appendChild(field);
			item.appendChild(formGroup);
			menu.appendChild(item);
			Classes.addClass(item, 'fields wide wide-12');
			Classes.addClass(field, 'field');
			Classes.addClass(label, 'field-label');
			Classes.addClass(formGroup, 'form-group');
			//AcmsEvent.add(field, 'change', self._onChangeTasklistField);
			AcmsEvent.add(field, 'keyup', self._onKeyUpTasklistField);
			self.windowBox.closeMenu();
			field.focus();
            
            return false;
		}

		/**
		 * 
		 *
		 * @param {Event} e
		 * @memberof Tasks
		 */
		_onKeyUpTasklistField(e) {
			let self = this, key = e.which || e;
			if(key === 9 || key === 13) {
				return self._onChangeTasklistField(e);
			}
		}

		/**
		 *
		 *
		 * @param {Event} e
		 * @returns
		 * @memberof Tasks
		 */
		_onChangeTasklistField(e) {
			e.preventDefault();
			if(this._processing) {
				return;
			}
			this._processing = true;
			let self = this, 
				o = self.options, 
				field = e.target, 
				li = Selectors.closest(field, 'li'),
				nbid = parseInt(field.getAttribute('data-tlist-id')) || 0,
				op = nbid === 0 ? 'add' : 'update',
				action = o.actions.tasklists[op],
				lists = self.windowSideList,
				menu = Selectors.q(o.selectors.window.myTasklists, lists),
				fData = new FormData(),
				cb = (data) => {
					if(data.status === 'success') {
						menu.removeChild(li);
						self.refreshMyTasklists();
					} else {
						let parent = Selectors.closest(field, '.form-group');
						let error = document.createElement('div');
						let p = document.createElement('p');
						error.appendChild(p);
						p.innerHTML = data.message;
						parent.appendChild(error);
						Classes.addClass(error, 'danger bordered alert v3');
						setTimeout(() => {
							parent.removeChild(error);
						}, 3000);
					}
					setTimeout(() => {
						self._processing = false;
					}, 100);
				};
			if(field.value.trim() === '') {
				console.log('empty');
				cb({status: 'success'});
				return;
			}
			fData.append('op', action);
			fData.append(field.name, field.value);
			fData.append(field.tasklist_id, nbid);
			require(['http/request'], (Request) => {
				Request.post(o.remote, fData, cb, self._fail, self._always);
			});

		}
		
		/**
		 * On Loaded Tasklists Callback
		 *
		 * @param {Object} result	Response Object from remote
		 * @returns
		 * @memberof Tasks
		 */
		_onLoadedMyTasklists(result) {
			let self = this, 
                o = self.options, 
                tasklists, l, i, tlist, first,
                lists = self.windowSideList, my = Selectors.q(o.selectors.window.myTasklists, lists),
                badge = Selectors.q('.profile-tasklists > a > .badge', lists);
            if(result.status !== 'success') {
                // @todo - proper error handling
                return;
            }
            self.tasklists = {};
            tasklists = result.tasklists;
			l = tasklists.length;
			self.totalTasklists = l;
            if(badge) badge.textContent = parseInt(result.total) || 0;
			first = true;
			my.innerHTML = '';
            for(i = 0; i < l; i++) {
				tlist = tasklists[i];
				let active = (first === true && ! self.currentTasklist) || (self.currentTasklist == tlist.id);
                
                if(active) {
                    self.currentTasklist = tlist.id;
				}
				self._appendTasklist(tlist, active, my);
                
                first = false;
            }
		}

		
		/**
		 *
		 *
		 * @param {HTMLElement} overlay
		 * @param {HTMLElement} trigger
		 * 
		 * @memberof Tasks
		 */
		_onOverlayOpen(overlay, trigger, wbox, woverlay) {
        }
		
        /**
		 * Window Callback Event "Before Add overlay open"
		 *
		 * @param {HTMLElement} overlay		Window overlay
		 * @param {HTMLElement} trigger		Trigger that has been used to open the overlay
		 * 
		 * @memberof Tasks
		 */
		_beforeAddOverlayOpen(overlay, trigger, wbox, woverlay) {
            let self = this, o = self.options, ele, inner, overlayContent;
            overlayContent = Selectors.q('.overlay-content', overlay);
            let cb = function(result) {
                return self._onCreateTask(result, overlayContent);
            };
            overlay.acmsData('tasklist', self.currentTasklist);
            require(['http/request', 'notifier'], function(Request) {
				let fData = new FormData();
				fData.append(o.fieldMap.op, o.actions.task.save);
				fData.append('tasklist_id', self.currentTasklist);
                Request.post(o.remote, fData, cb, self._fail, self._always);
            });
		}

        /**
		 * Window Callback Event "Before Edit overlay open"
		 *
		 * @param {HTMLElement} overlay		Window overlay
		 * @param {HTMLElement} trigger		Trigger that has been used to open the overlay
		 * 
		 * @memberof Tasks
		 */
        _beforeEditOverlayOpen(overlay, trigger, wbox, woverlay) {
            let self = this, o = self.options, ele, inner, overlayContent;
            overlayContent = Selectors.q('.overlay-content', overlay);
            let task = Selectors.closest(trigger, '.profile-task'),
				taskObj = task.acmsData(Task.NS),
				model = taskObj.getData();
            
			let template = o.templates.task.form, 
				data = {
					task: model,
					editor: {
						type: o.editorType.toLowerCase(),
						name: o.editorName.toLowerCase()
					}
				},
				callback = () => {
					let title = Selectors.q('input[name="title"]', overlayContent),
						body = Selectors.q('textarea[name="body"]', overlayContent),
						id = Selectors.q('input[name="id"]', overlayContent),
						nb = Selectors.q('input[name="tasklist_id"]', overlayContent);
					ModuleLoader.load(overlayContent);
					AcmsEvent.add(body, 'change', self._onChangeTaskForm);
					AcmsEvent.add(title, 'change', self._onChangeTaskForm);
					AcmsEvent.add(body, 'blur', self._onChangeTaskForm);
					AcmsEvent.add(title, 'blur', self._onChangeTaskForm);
				};
			TemplateEngine.compileResource(template, data, callback, overlayContent);
            overlay.acmsData('tasklist', model.tasklist_id);
		}

		_beforeDeleteTasklistOverlayOpen(overlay, trigger, wbox, woverlay) {
			let self = this, o = self.options, overlayContent;
            overlayContent = Selectors.q('.overlay-content', overlay);
            let nbid = self.currentTasklist,
				tasklistObj = self.tasklists[nbid];
            
			let template = o.templates.tasklist.remove, 
				data = {
					tasklist: {
						title: tasklistObj.getTitle(),
						id: tasklistObj.getId()
					},
					form: {
						op: o.actions.tasklists.remove,
						field: o.fieldMap.op
					}
				},
				callback = () => {
					let form = Selectors.q('form', overlayContent);
					let cb = (e) => {
						e.preventDefault();
						e.stopPropagation();
						let fData = new FormData(form);
						require(["http/request", "notifier"], (Request) => {
							Request.post(o.remote, fData, self._onDeletedTasklist, self._fail, self._always);
						});
					}, dismiss;
					AcmsEvent.on(form, 'click', 'button[type="submit"]', cb, true);
					ModuleLoader.load(overlayContent);
					
				};
			TemplateEngine.compileResource(template, data, callback, overlayContent);
		}

		_onDeletedTasklist(response) {
			if(response.status === 'success') {
				let self = this, 
					o = self.options, 
					overlay = Selectors.q('.window-overlay', self.windowOverlay),
					overlayContent = Selectors.q('.overlay-content', overlay);
				if(overlayContent) overlayContent.innerHTML = '';
				self.currentTasklist = 0;
				self.hideWindowOverlay();
				self.refreshMyTasklists();
			} else {
				A.Notifications.createFromResponse(response);
			}
		}
		
		/**
		 * On Change Task Form Field Event Handler
		 *
		 * @param {Event} e		Change/Blur Event
		 * @memberof Tasks
		 */
		_onChangeTaskForm(e) {
			let form = Selectors.closest(e.target, 'form'), self = this, o = self.options, fData = new FormData(form);
			require(['http/request'], (Request) => {
				Request.post(o.remote, fData, function(response){}, self._fail, self._always);
			});
		}

        /**
		 * Window Callback Event "Before overlay open"
		 *
		 * @param {HTMLElement} overlay		Window overlay
		 * @param {HTMLElement} trigger		Trigger that has been used to open the overlay
		 * 
		 * @memberof Tasks
		 */
        _beforeOverlayOpen(overlay, trigger, wbox, woverlay) {
            let self = this, o = self.options, matches, t1, t2, t3;
            t1 = Selectors.closest(trigger, '[data-role="task-add"]');
			
            matches = (Selectors.matches(trigger, '[data-role="task-add"]') || (null !== t1));
            if(matches) {
				self._beforeAddOverlayOpen(overlay, trigger, wbox, woverlay);
				return true;
			}
			t2 = Selectors.closest(trigger, '[data-role="task-edit"]');
            matches = (Selectors.matches(trigger, '[data-role="task-edit"]') || (null !== t2));
            if(matches) {
				self._beforeEditOverlayOpen(overlay, trigger, wbox, woverlay);
				return true;
			}
			t2 = Selectors.closest(trigger, o.selectors.tasklist.remove);
			matches = (Selectors.matches(trigger, o.selectors.tasklist.remove) || (null !== t2));
			if(matches) {
				self._beforeDeleteTasklistOverlayOpen(overlay, trigger, wbox, woverlay);
			}
            return true;
        }
        
        /**
		 * Window Callback Event "Before overlay dismiss"
		 *
		 * @param {HTMLElement} overlay		Window overlay
		 * @param {HTMLElement} trigger		Trigger that has been used to close the overlay
		 * 
		 * @memberof Tasks
		 */
        _beforeOverlayDismiss(overlay, trigger, wbox, woverlay) {
            let self = this, nbid = parseInt(overlay.acmsData('tasklist'), 10);
            if(nbid && typeof self.tasklists[nbid] !== 'undefined') {
                self.tasklists[nbid].refresh();
            }
            return true;
        }
        
        /**
		 * Window Callback Event "On overlay close"
		 *
		 * @param {HTMLElement} overlay		Window overlay
		 * @param {HTMLElement} trigger		Trigger that has been used to close the overlay
		 * 
		 * @memberof Tasks
		 */
        _onOverlayClose(overlay, trigger, wbox, woverlay) {
            let overlayContent;
			if(window.tinyMCE) {
				let ed = window.tinyMCE.get('profile-task-body');
				if(ed) ed.remove();
			}
            overlayContent = Selectors.q('.overlay-content', overlay);
            overlayContent.innerHTML = '';
        }
		
        /**
		 * On Create Task Callback
		 *
		 * @param {Object} 			result			Result Response Object from Remote
		 * @param {HTMLElement} 	overlayContent	Overlay Content Element
		 * 
		 * @memberof Tasks
		 */
		_onCreateTask(result, overlayContent) {
			let model = result.model, 
				self = this, 
				o = self.options;
			let tid = parseInt(model.tasklist_id, 10);
            if(typeof self.tasklists[tid] !== 'undefined') {
                self.tasklists[tid].appendTask(model);
            }
			let template = o.templates.task.form, 
				data = {
					task: model,
					editor: {
						type: o.editorType.toLowerCase(),
						name: o.editorName.toLowerCase()
					}
				},
				callback = () => {
					let title = Selectors.q('input[name="title"]', overlayContent),
						body = Selectors.q('textarea[name="body"]', overlayContent),
						id = Selectors.q('input[name="id"]', overlayContent),
						nb = Selectors.q('input[name="tasklist_id"]', overlayContent);
					ModuleLoader.load(overlayContent);
					AcmsEvent.add(body, 'change', self._onChangeTaskForm);
					AcmsEvent.add(title, 'change', self._onChangeTaskForm);
					AcmsEvent.add(body, 'blur', self._onChangeTaskForm);
					AcmsEvent.add(title, 'blur', self._onChangeTaskForm);
				};
			TemplateEngine.compileResource(template, data, callback, overlayContent);
            self.windowBox.overlay.acmsData('tasklist', tid);
        }

		
		/**
		 * Appen a tasklist to the list
		 *
		 * @param {Object} 		tasklist	Data of the tasklist from remote server
		 * @param {Boolean} 	active		Currently active?
		 * @param {HTMLElement} list		List Element or Wrapper for the Tasklists
		 * @param {Boolean} 	shared		`true` if the tasklist is a shared list
		 * 
		 * @memberof Tasks
		 */
		_appendTasklist(tasklist, active, list, shared) {
			let self = this, 
				o = self.options,
				template = o.templates.tasklist.list,
				data = {
					tasklist: tasklist,
					active: active,
					shared: shared
				},
				callback = () => {
					let li = Selectors.q('.tasklist-item[data-list-id="'+tasklist.id+'"]', list);
					let tl = Tasklist.init(li, o, self);
					if(true === active) {
						tl.activate();
					}
					self.tasklists[parseInt(tasklist.id)] = tl;
					if(Object.keys(self.tasklists).length === self.totalTasklists) {
						o.onLoadedTasklists(self);
					}
				};
			TemplateEngine.compileResource(template, data, callback, list);
		}


		/**
		 * Prepare Class instance
		 *
		 * @memberof Tasks
		 */
		_prepare() {
			let self = this, 
                o = self.options,
                element = self.element;
			self.remote = o.remote;
			self.windowOverlay = Classes.hasClass(element, 'window') ? element : Selectors.q('.window', element);
			self._prepareWindowOverlay();
			self.overlay = Selectors.q('.window-overlay', self.windowOverlay);
			self.dismissOverlay = Selectors.qa('[data-dismiss="window-overlay"]', self.windowOverlay);
		}

		
		/**
		 * Prepare Window Overlay Module
		 *
		 * @memberof Tasks
		 */
		_prepareWindowOverlay() {
			let self = this, 
                o = self.options,
				element = self.element;
			let windowOptions = {
				owner: o.owner,
				overlayOptions: {
					beforeDismiss: function(overlay, trigger, wbox, woverlay) {
						return self._beforeOverlayDismiss(overlay, trigger, wbox, woverlay);
					},
					onDismiss: function(overlay, trigger, wbox, woverlay) {
						self._onOverlayClose(overlay, trigger, wbox, woverlay);
					},
					beforeOpen: function(overlay, trigger, wbox, woverlay) {
						return self._beforeOverlayOpen(overlay, trigger, wbox, woverlay);
					},
					onOpen: function(overlay, trigger, wbox, woverlay) {
						self._onOverlayOpen(overlay, trigger, wbox, woverlay);
					}
				}
			};
			self.windowBox = WindowBox.init(self.windowOverlay, windowOptions);
			self.windowMenu = Selectors.q('.window-menu', self.windowOverlay);
			self.windowSideList = Selectors.q('.side-list', self.windowOverlay);
		}

		
        /**
         * Ausführung bei fehlgeschlagener Abfrage
         * 
		 * Wird ausgeführt, wenn keine Antwort vom Server kommt bzw die 
         * Anfrage fehlschlägt
         * 
         * @param {Object}  xhr    		Das XHR Abfrage Object
         * @param {String}	textStatus 	Text Status Message
		 * 
		 * @memberof Tasks
         */
        _fail (xhr, textStatus) {
            A.Logger.logWarning(textStatus);
        }
        
        /**
         * Auführung bei jeder Abfrage
         * 
         * @param {String} textStatus Text Status
         * 
		 * @memberof Tasks
         */
        _always (textStatus) {
            A.Logger.logInfo(textStatus);
            this._processing = false;
        }
        
	}
 
	return Tasks;
 });
