/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("tasks-module/app/helper/task", [
	"core/base",
	"tools/utils",
	"core/classes",
	"core/selectors",
	"events/event",
	"core/acms",
	"templates/template-engine",
	"tasks-module/app/tasks",
	"tasks-module/app/helper/tasklist"
], (Base, Utils, Classes, Selectors, AcmsEvent, A, TemplateEngine, Tasks, Tasklist) => {

	/**
	 * Task
	 *
	 * @class Task
	 * @extends {Base}
	 */
	class Task extends Base {

		/**
		 * Module Namespace
		 *
		 * @readonly
		 * @static
		 * @memberof Task
		 */
		static get NS() {
			return "tasks-module.app.helper.task";
		}

		/**
		 * Module Name
		 *
		 * @readonly
		 * @static
		 * @memberof Task
		 */
		static get MODULE() {
			return "Profile Task";
		}

		/**
		 * Default Options
		 *
		 * @readonly
		 * @static
		 * @memberof Task
		 */
		static get DEFAULT_OPTIONS() {
			return {
				hasColor: true
			};
		}
		
		/**
		 * Default Colors
		 *
		 * @readonly
		 * @static
		 * @memberof Task
		 */
		static get DEFAULT_COLORS() {
			return {
				0: {
					'name': A.Translator._('Keine Farbe'),
					'color': '#FFFFFF',
					'inverse': ''
				},
				1: {
					'name': A.Translator._('Farbe Rot'),
					'color': '#D50000',
					'inverse': '#FFFFFF'
				},
				2: {
					'name': A.Translator._('Farbe Blau'),
					'color': '#2962FF',
					'inverse': '#FFFFFF'
				},
				3: {
					'name': A.Translator._('Farbe Grün'),
					'color': '#00C853',
					'inverse': '#FFFFFF'
				},
				4: {
					'name': A.Translator._('Farbe Gelb'),
					'color': '#ffd600',
					'inverse': '#FFFFFF'
				},
				5: {
					'name': A.Translator._('Farbe Lila'),
					'color': '#aa00ff',
					'inverse': '#FFFFFF'
				},
				6: {
					'name': A.Translator._('Farbe Orange'),
					'color': '#ff6d00',
					'inverse': '#FFFFFF'
				}
			};
		}
	
		/**
		 * Default Labels
		 *
		 * @readonly
		 * @static
		 * @memberof Task
		 */
		static get DEFAULT_LABELS() {
			return {
				edit: A.Translator._('bearbeiten'),
				delete: A.Translator._('entfernen'),
				selectColor: A.Translator._('Farbe wählen'),
				new: A.Translator._('Neue Aufgabe')
			};
		}
		
		/**
		 * Erstellt einen Task aus einem Response Objekt
		 * 
		 * Die Methode kann genutzt werden um aus einem Task-Model Objekt aus der 
		 * JSON Response beim Laden oder erstellen eines Tasks den Task zu einer 
		 * Liste hinzuzufügen.
		 * 
		 * @param {Object}  	data    Daten aus der Datenbank
		 * @param {HTMLElement} list    Die Liste
		 * @param {Object}  	options Die Optionen  
		 * @param {Tasklist}	tlist	Tasklist instance
		 * @param {Tasks}		ptasks	Profile Tasks App instance
		 * 
		 * @returns {Task}
		 */
		static createFromResponse(data, list, options, tlist, ptasks, callback) {
			let labels = options.task.labels || Task.DEFAULT_LABELS, 
				colors = options.task.colors || Task.DEFAULT_COLORS,
				element, i, tpl, tData, n, cb;
			for (i in colors) {
				if(colors.hasOwnProperty(i)) {
					colors[i].active = (i == parseInt(data.colorscheme));
				}
			}
			tpl = options.templates.task.task;
			options.isOwner = (parseInt(options.owner, 10) == parseInt(data.owner, 10));
			tData = {
				task: data,
				labels: labels,
				colors: colors,
				options: options
			};
			cb = () => {
				element = list.querySelector('#profile-task-'+data.id);
				n = Task.init(element, options, tlist, ptasks);
				n.data = data;
				n.element.acmsData('original', data);
				
				callback(n);
			};
			TemplateEngine.compileResource(tpl, tData, cb, list);
			
		}

		static init(element, options, ptasks, tlist) {
			let m = new Task(element, options, ptasks, tlist);
			m.element.acmsData(Task.NS, m);
			
			return m;
		}


		constructor(element, options, tlist, ptasks) {
			super(element, options);
			this.profiletasks = ptasks;
			this.tasklist = tlist;
			this._initialize();
		}

		set data(data) {
			this._data = data;
		}

		get data() {
			return this._data;
		}

		setData(data) {
			this.data = data;
		}

		getData() {
			return this.data;
		}

		/**
		 * Is the task active?
		 *
		 * @returns {Boolean}	`true` if the not has been activated, `false` otherwise
		 * 
		 * @memberof Task
		 */
		isActive() {
			let self = this;
			return Classes.hasClass(self.element, 'active');
		}

		/**
		 * Activates the task
		 *
		 * @memberof Task
		 */
		activate() {
			let self = this, element = self.element, dsc;
			if(self._isToggling) {
				return;
			}
			self._isToggling = true;
			dsc = document.createElement('div');
			Classes.addClass(dsc, 'task-description fade');
			element.appendChild(dsc);
			dsc.innerHTML = self.data.body;
			Classes.addClass(dsc, 'in');
			Classes.addClass(self.element, 'active');
			self._isToggling = false;
		}

		/**
		 * Deactivate the task
		 *
		 * @memberof Task
		 */
		deactivate() {
			let self = this, element = self.element, dsc = Selectors.q('.task-description', element);
			if(self._isToggling || !dsc) {
				return;
			}
			self._isToggling = true;
			Classes.removeClass(dsc, 'in');
			setTimeout(() => {
				element.removeChild(dsc);
				self._isToggling = false;
			}, 200);
		
			Classes.removeClass(self.element, 'active');
		}

		/**
		 * Toggle Active State
		 *
		 * @memberof Task
		 */
		toggleActive() {
			if(this.isActive()) {
				this.deactivate();
			} else {
				this.activate();
			}
		}

		listen() {
			let self = this, o = self.options;
            AcmsEvent.add(self.element, 'click', self._onClickTask);
            if(self.editButton && o.isOwner) {
                AcmsEvent.add(self.editButton, 'click', self._onClickEdit);
            }
            if(self.delButton && o.isOwner) {
                AcmsEvent.add(self.delButton, 'click', self._onClickDelete);
            }
            if(self.checkbox && o.isOwner) {
                AcmsEvent.add(self.checkbox, 'change', self._onCheckboxChange);
            }
            if(self.colorselect && o.isOwner || true === o.hasColor) {
                AcmsEvent.on(self.colorselect, 'click', '.tasks-color', self._onSelectColor);
            }
		}

		_onClickTask(e) {
			let self = this, o = self.options, target = e.target;
			if(Classes.hasClass(target, 'task-action ') || Selectors.closest(target, '.task-action ')) {
				return;
			}
			this.toggleActive();
		}
		
        /**
         * On-Click-Event Handler
         * 
         * @param {Event} e  Das Event
         * @returns {Boolean}
		 * 
		 * @memberof Task
         */
        _onClickEdit(e) {
            let self = this,
				target = e.target,
				ptasks = self.profiletasks,
				o = ptasks.options,
				template = o.templates.task.form;
            
            return false;
        }
		
        /**
         * On-Click-Delete Event Handler
         * 
         * @param {Event} e  Das Event
         * 
		 * @returns {Boolean}
		 * 
		 * @memberof Task
         */
        _onClickDelete(e) {
			let self = this, 
				o = self.options,
				target = e.target, 
				data = self.getData(),
				fData = new FormData();
            fData.append('op', 'deletetask');
            fData.append('id', data.id);
            fData.append('tasklist_id', data.tasklist_id);
            require(['http/request'], (Request) => {
                Request.post(o.remote, fData, self._done);
            });
            return false;
        }
		
        /**
         * On-Ccheckbox Change Event Handler
         * 
         * @param {Event} e  Das Event
		 * 
         * @returns {Boolean}
		 * 
		 * @memberof Task
         */
        _onCheckboxChange(e) {
            let self = this,
                    target = e.target,
                    form = Selectors.closest(target, 'form'),
                    fData = new FormData(form);
            if(target.disabled) {
                return;
            }
            /*
            require(['http/request', 'notifier'], function (Request) {
                Request.post(self.remote, fData, self._done, self._fail, self._always);
            });
            */
            return false;
        }

		/**
		 * On Select Color Event Handler
		 *
		 * @param {Event} e		Click Event
		 * 
		 * @memberof Task
		 */
		_onSelectColor(e) {
            let self = this, o = self.options, span = e.target, element = self.element;
            let value = parseInt(span.acmsData('color'), 10);
            self.colors.forEach((sp) => {
                Classes.removeClass(sp, 'active');
            });
            Classes.removeClass(self.element, self.colorclasses);
            Classes.addClass(self.element, 'tasks-color-'+value);
            Classes.addClass(span, 'active');
            let fData = new FormData(), data = self.getData();
            fData.append('tasklist_id', data.tasklist_id);
            fData.append('task_id', data.id);
            fData.append('field', 'colorscheme');
            fData.append('value', value);
            fData.append('op', 'updatetask');
            require(['http/request'], (Request) => {
                Request.post(o.remote, fData);
            });
		}
		
		/**
		 * On Deleted xhr Callback
		 *
		 * @param {Object} response Response data from server
		 * 
		 * @memberof Task
		 */
		_onDeleted(response) {
			let self = this, o = self.options;
            if(response.status === 'success') {
				if(self.element.parentNode) 
					self.element.parentNode.removeChild(self.element);
                self.element = null;
            }
		}

		_prepare() {
			let self = this,
                    o = self.options,
                    element = self.element, colors = o.task.colors || Task.DEFAULT_COLORS;
            self.remote = o.remote;
            self.editButton = Selectors.q('.task-edit', element);
            self.checkbox = Selectors.q('input[type="checkbox"]', element);
            self.delButton = Selectors.q('.task-delete', element);
            if(!o.isOwner) {
                Classes.addClass(self.editButton, 'hidden invisible');
                Classes.addClass(self.delButton, 'hidden invisible');
                Classes.addClass(Selectors.closest(self.checkbox, 'div'), 'hidden invisible');
            }
            self.colorselect = Selectors.q('.task-colors', element);
            if(o.hasColor && !self.colorselect) {
                self._appendColorSelect();
            } else {
                self.colors = Selectors.qa('.tasks-color', element);
                self.colorclasses = '';
                for(let i in colors) {
                    if(colors.hasOwnProperty(i)) {
                        self.colorclasses += 'tasks-color-'+i+',';
                    }
                }
                self.colorclasses = self.colorclasses.substring(0, self.colorclasses.length - 1);
            }
		}
		
        /**
         * Ausführung bei fehlgeschlagener Abfrage
         * 
		 * Wird ausgeführt, wenn keine Antwort vom Server kommt bzw die 
         * Anfrage fehlschlägt
         * 
         * @param {Object}  xhr    		Das XHR Abfrage Object
         * @param {String}	textStatus 	Text Status Message
		 * 
		 * @memberof Task
         */
        _fail (xhr, textStatus) {
            A.Logger.writeLog(textStatus);
        }
        
        /**
         * Auführung bei jeder Abfrage
         * 
         * @param {String} textStatus Text Status
         * 
		 * @memberof Task
         */
        _always (textStatus) {
            A.Logger.writeLog(textStatus);
            this._processing = false;
        }
		
		_appendColorSelect() {
            let self = this, o = self.options, colors = o.colors || Task.DEFAULT_COLORS,
                select = self.colorselect, scheme;
            scheme = parseInt(o.original.colorscheme, 10);
            self.colorclasses = '';
            self.colors = [];
            for(let i in colors) {
                if(colors.hasOwnProperty(i)) {
                    let span = document.createElement('span');
                    let val = parseInt(i);
                    self.colorclasses += 'tasks-color-'+val+',';
                    select.appendChild(span);
                    span.acmsData('color', val);
                    span.setAttribute('aria-label', colors[i].name);
                    Classes.addClass(span, 'tasks-color,tasks-color-'+i);
                    if(scheme === val) {
                        Classes.addClass(span, 'active');
                    }
                    self.colors.push(span);
                }
            }
            self.colorclasses = self.colorclasses.substring(0, self.colorclasses.length - 1);
		}
		
	}

	return Task;
});
