/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

define("tasks-module/app/helpers/tasklist", [
	"core/base",
	"core/classes",
	"core/selectors",
	"events/event",
	"core/acms",
	"tasks-module/app/tasks",
	"tasks-module/app/helper/task"
], (Base, Classes, Selectors, AcmsEvent, A, Tasks, Task) => {

	/**
	 * Tasklist
	 *
	 * @class Tasklist
	 * @extends {Base}
	 */
	class Tasklist extends Base {

		/**
		 * Module Namespace
		 *
		 * @readonly
		 * @static
		 * @memberof Tasklist
		 */
		static get NS() {
			return "tasks-module.app.helper.tasklist";
		}

		/**
		 * Module Name
		 *
		 * @readonly
		 * @static
		 * @memberof Tasklist
		 */
		static get MODULE() {
			return "Profile Tasklist";
		}

		/**
		 * Default Options
		 *
		 * @readonly
		 * @static
		 * @memberof Tasklist
		 */
		static get DEFAULT_OPTIONS() {
			return {};
		}

		/**
		 * Module init
		 *
		 * @static
		 * 
		 * @param {HTMLElement} 				element		Element representing the tasklist
		 * @param {Object} 						options
		 * @param {tasks-module/app/tasks} 	ptasks		profile Tasks instance
		 * 
		 * @returns {Tasklist}
		 * 
		 * @memberof Tasklist
		 */
		static init(element, options, ptasks) {
			let m = new Tasklist(element, options, ptasks);
			m.element.acmsData(Tasklist.NS, m);
			
			return m;
		}


		/**
		 * Creates an instance of Tasklist.
		 * 
		 * @param {HTMLElement} 				element		Element representing the tasklist
		 * @param {Object} 						options
		 * @param {tasks-module/app/tasks} 	ptasks		profile Tasks instance
		 * 
		 * @memberof Tasklist
		 */
		constructor(element, options, ptasks) {
			super(element, options);
			this.profiletasks = ptasks;
			this._loaded =
			this._loading =
			this._tasks =
			this.button = null;
			this._initialize();
		}

		/**
		 * Get all Tasks of the Tasklist
		 * 
		 * The getter returns the tasks of the tasklist. Please Task, 
		 * that the tasks have to be loaded first during activation 
		 * of the tasklist. otherwise an empty array or null might 
		 * be returned.
		 * 
		 * @returns {Array}	Array of Task Objects
		 *
		 * @memberof Tasklist
		 */
		get tasks() {
			return this._tasks;
		}

		/**
		 * Reset the tasks to an empty array
		 *
		 * @memberof Tasklist
		 */
		set tasks(tasks) {
			this._tasks = [];
		}

		/**
		 * Is this Tasklist currently active in tasks window?
		 *
		 * @returns {Boolean}	`true` if the tasklist is active
		 * @memberof Tasklist
		 */
		isActive() {
			let self = this, 
				ptasks = self.profiletasks,
				po = ptasks.options.classNames;
            return Classes.hasClass(self.element, po.active);
		}

		/**
		 * Deactivate Tasklist
		 *
		 * @memberof Tasklist
		 */
		deactivate() {
			let self = this, 
				ptasks = self.profiletasks,
				po = ptasks.options.classNames,
				anchor = Selectors.q('.title', self.element);
			Classes.removeClass(self.element, po.active);
			Classes.removeClass(anchor, po.active);
		}

		/**
		 * Activate Tasklist
		 *
		 * @param {CallableFunction} cb	Optional Callback to be triggered if loading has been finished
		 * @memberof Tasklist
		 */
		activate(cb) {
			let self = this, 
				ptasks = self.profiletasks,
				po = ptasks.options.classNames,
				anchor = Selectors.q('.title', self.element);
			Classes.addClass(self.element, po.active);
			Classes.addClass(anchor, po.active);
			self.profiletasks.currentTasklist = self.getId();
			self.refresh(cb);
		}

		/**
		 * Gets the Tasklist Title
		 *
		 * @returns {String}
		 * @memberof Tasklist
		 */
		getTitle() {
			let self = this, element = self.element, anchor;
			anchor = Selectors.q('.title', element);

			return anchor.innerHTML;
		}
		
		/**
		 * Gets the Tasklist Title
		 *
		 * @returns {String}
		 * @memberof Tasklist
		 */
		getId() {
			let self = this, element = self.element, anchor;
			
			return parseInt(element.getAttribute('data-tlist-id'));
		}
		
        /**
		 * Append Task to Tasklist
		 *
		 * @param {Object} data	Response Object of a Task
		 * 
		 * @memberof Tasklist
		 */
		appendTask(data) {
            this._makeTask(parseInt(data.id, 10), data);
        }
        
        /**
		 * Remove Task from Tasklist
		 *
		 * @param {Number} idx	ID of the Task
		 * 	
		 * @memberof Tasklist
		 */
		removeTask(idx) {
            let self = this;
            if(typeof self._tasks[idx] !== 'undefined') {
                delete self._tasks[idx];
            }
        }
        
        /**
		 * Gets a Task from the Tasklist
		 *
		 * @param {Number} idx	ID of the Task
		 * @returns {tasks-module/app/helper/task|Boolean}	Task or `false` if not found
		 * @memberof Tasklist
		 */
		getTask(idx) {
            let self = this;
            if(typeof self._tasks[idx] !== 'undefined') {
                return self._tasks[idx];
            }
            
            return false;
		}
		
		/**
		 * Force refresh tasks from remote host
		 *
		 * @param {CallableFunction} cb	Optional Callback to be called on finished
		 * @returns
		 * @memberof Tasklist
		 */
		refresh(cb) {
			this._loaded = this._loading = false;
            return this.load(cb);
		}

		/**
		 * Loads all Tasks of the Tasklist from remote
		 *
		 * @param {CallableFunction} cb	Callback to be triggered if all tasks have been loaded
		 * 
		 * @returns {undefined}	If the tasklists are currently loading
		 * 
		 * @memberof Tasklist
		 */
		load(cb) {
			if(true === this._loaded || true === this._loading) {
                return;
			}
			this._loading = true;
            let self = this, o = self.options, fData = new FormData();
            fData.append('op', 'loadtasks');
			fData.append('tasklist_id', self.element.getAttribute('data-tlist-id'));
			if(self.currentOrder) {
				fData.append('order', self.currentOrder);
				fData.append('sort', self.currentSort);
			} else {
				fData.append('order', 'pdate');
				fData.append('sort', 'DESC');
			}
            let onDone = (response) => {
                return self._onLoaded(response, cb);
            };
            require(['http/request'], (Request) => {
                Request.post(o.remote, fData, onDone, self._fail, self._always);
                
            });
		}

		/**
		 * Setup Event Listener
		 *
		 * @memberof Tasklist
		 */
		listen() {
			let self = this;
            AcmsEvent.add(self.anchor, 'click', self._onClick);
            if(self.sorter) {
                AcmsEvent.on(self.sorter, 'change', 'input[type="radio"]', self._onChangeSorter);
            }
		}

		/**
		 * On Click Event Handler
		 *
		 * @param {Event} e	On Click Event
		 * 
		 * @returns {Boolean}	always `false`
		 * 
		 * @memberof Tasklist
		 */
		_onClick(e) {
			let self = this;
            if(e) {
				e.preventDefault();
				e.stopPropagation();
			}
            if(self._loading === true) {
                return false;
            }
			this.activate();
			return false;
		}

		/**
		 * On Change Sort/Order
		 *
		 * @param {Event} e	Change Event of the sorter
		 * 
		 * @returns {Boolean|undefined}
		 * 
		 * @memberof Tasklist
		 */
		_onChangeSorter(e) {
			let self = this, radio = e.target, current;
            if(!self.isActive()) {
                return;
            }
            if(self.updateSort === true) {
                return;
            }
            self.updateSort = true;
            console.log(radio);
            e.stopPropagation();
            e.preventDefault();
            current = radio.value.split('-');
            self.currentSort = current[1].toUpperCase();
            self.currentOrder = current[0];
            self.refresh(() => {
                self.updateSort = false;
            });
            
            return false;
		}

		/**
		 * On Loaded Callback
		 *
		 * @param {Object} 				data	Remote Response Data
		 * @param {CallableFunction} 	cb		optional Callback to be called on finished process
		 * 
		 * @memberof Tasklist
		 */
		_onLoaded(data, cb) {
			let self = this, j, t, total = parseInt(data.total), o = self.options;
            self._loaded = true;
            self._loading = false;
            self._tasks = [];
            self.tasklist.innerHTML = '';
            if(data.status === 'success') {
                for(j = 0; j < total; j++) {
					t = data.tasks[j];
					self._makeTask(t.id, t);
                }
            }
			let interval = setInterval(() => {
				if(total === self._tasks.length) {
					if(cb)
						cb(self, self.tasklist, self._tasks, data);
					o.onLoadedTasklist(self, self._tasks, self.profiletasks);
					clearInterval(interval);
				}
			}, 10);
		
		}

		/**
		 * Make a Task
		 * 
		 * Creates a Task Obj from Remote Data and appends the Task to the Tasklist
		 * 
		 * @param {Number} id	ID of the Task
		 * @param {Object} data	Remote Response Object of a single Task
		 * @memberof Tasklist
		 */
		_makeTask(id, data) {
			let self = this, 
				cb = (obj) => {
					self._tasks[id] = obj;
				}, 
				options = self.profiletasks.options,
				list = self.tasklist
			;
			Task.createFromResponse(data, list, options, self, self.profiletasks, cb);
		}

		/**
		 * Prepare Module 
		 *
		 * @memberof Tasklist
		 */
		_prepare() {
			let self = this,
				o = self.options,
				element = self.element, win = Selectors.closest(element, '.window');
            self.window = win;
            self.tasklist = Selectors.q('.tasklist.open-tasks', win);
            self.sorter = Selectors.q('.window-sort-menu', win);
            self.completedlist = Selectors.q('.tasklist.completed-tasks', win);
            self.anchor = Selectors.q('a', element);
            if(self.sorter) {
                let checked = Selectors.q('input[type="radio"]:checked', self.sorter),
                    current = (checked ? checked.value : 'title-asc').split('-');
                self.currentSort = current[1].toUpperCase();
                self.currentOrder = current[0];
            }
		}

		
        /**
         * Ausführung bei fehlgeschlagener Abfrage
         * 
		 * Wird ausgeführt, wenn keine Antwort vom Server kommt bzw die 
         * Anfrage fehlschlägt
         * 
         * @param {Object}  xhr    		Das XHR Abfrage Object
         * @param {String}	textStatus 	Text Status Message
		 * 
		 * @memberof Tasklist
         */
        _fail (xhr, textStatus) {
            A.Logger.writeLog(textStatus);
        }
        
        /**
         * Auführung bei jeder Abfrage
         * 
         * @param {String} textStatus Text Status
         * 
		 * @memberof Tasklist
         */
        _always (textStatus) {
            A.Logger.writeLog(textStatus);
			this._processing = false;
			this._loading = false;
        }
        
	}

	return Tasklist;
});
