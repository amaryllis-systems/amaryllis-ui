/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

let gulp = require('gulp'),
	composer = require('gulp-uglify/composer'),
	uglifyes = require('uglify-es'),
	rename = require('gulp-rename'),
	track = require('../tasks-helpers/track'),
	buildOptions = require('../tasks-helpers/build-options'),
	paths = require('../tasks-helpers/build-paths'),
	files = require('../tasks-helpers/files'),
	sprintf = require('../tasks-helpers/sprintf'),
	argv = require('yargs').argv;

let minifyOptions = buildOptions.minify;

let minify = composer(uglifyes, console);

module.exports = (callback) => {

	let folders = files.getFolders(paths.themes.input.base);
	let pipes = [];

	folders.forEach((folder) => {
		let input = sprintf(paths.themes.input.scripts, folder);
		let input2 = sprintf(paths.themes.input.hbs, folder);
		let output = sprintf(paths.themes.output.scripts, folder);
		track(`minify scripts of module "${folder}" (${input} => ${output}) ...`);
		let pipe = new Promise((resolve, reject) => {
			gulp.src(input, {allowEmpty: true})
				
				.pipe(minify(minifyOptions)).on('error', (err) => {
					track('Error during uglify:');
					track(err.message);
					reject();
				})
				.pipe(rename({suffix: '.min', extname: ".js"}))
				.pipe(gulp.dest(output))
				.on('end', resolve);
		});
		
		pipes.push(pipe);
		
		pipes.push(pipe);
		let p2 = new Promise((resolve, reject) => {
            gulp.src(input2, {allowEmpty: true})
            .pipe(gulp.dest(output))
            .on('end', resolve);
		});
		pipes.push(p2);
	});
	if(pipes.length === 0) {
		return callback();
	}

	return Promise.all(pipes);
};
