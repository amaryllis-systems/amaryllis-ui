/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

let gulp = require('gulp'),
	sass = require('gulp-sass'),
	prefix = require('gulp-autoprefixer'),
	track = require('../tasks-helpers/track'),
	paths = require('../tasks-helpers/build-paths'),
	files = require('../tasks-helpers/files'),
	buildOptions = require('../tasks-helpers/build-options'),
	sprintf = require('../tasks-helpers/sprintf');;

module.exports = (callback) => {
    
    let folders = files.getFolders(paths.modules.input.base);
    let pipes = [];

    folders.forEach((folder) => {
        let input = sprintf(paths.modules.input.sass, folder);
        let output = sprintf(paths.modules.output.sass, folder);
        track(`compiling styles of module "${folder}" (${input} => ${output}) ...`);
        let pipe = new Promise((resolve, reject) => {
            gulp.src(input, {allowEmpty: true})
                    .pipe(sass(buildOptions.sass).on('error', sass.logError))
                    .on('error', reject)
                    .pipe(prefix(buildOptions.prefix))
                    .pipe(gulp.dest(output))
                    .on('end', resolve);
        });
        pipes.push(pipe);
    });
    if(pipes.length === 0) {
        return callback();
    }

    return Promise.all(pipes).then(() => {callback();});
};
