/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

let gulp = require('gulp'),
	zip = require('gulp-zip'),
	fs = require('fs');

const jsconfig = JSON.parse(fs.readFileSync('./version.json'));
const dirname = "amaryllis-ui-v"+jsconfig.version;
const dirname2 = "amaryllis-ui";

const PackCore = (callback) => {
	let pipes = [];

	let p1 = new Promise((resolve, reject) => {
		gulp.src('dist/media/**')
        .pipe(zip(dirname +'.zip'))
		.pipe(gulp.dest('releases'))
		.on('end', () => {
			resolve();
		})
		.on('error', () => {
			reject();
		});
	});

	let p2 = new Promise((resolve, reject) => {
		gulp.src('dist/media/**')
        .pipe(zip(dirname2 +'.zip'))
		.pipe(gulp.dest('releases'))
		.on('end', () => {
			resolve();
		})
		.on('error', () => {
			reject();
		});
	});
	pipes.push(p1);
	pipes.push(p2);

	return Promise.all(pipes).then(() => {
		callback();
	});
    
};

module.exports = PackCore;
