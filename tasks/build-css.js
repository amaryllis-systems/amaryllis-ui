/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const gulp = require('gulp'),
      track = require('../tasks-helpers/track');

const taskCopyCSS = () => {
    track('Copy core styles...');
    return new Promise((resolve, reject) => {
        gulp.src(['ui/sass/dist/media/css/**/*.*'])
        .pipe(gulp.dest('dist/media/css'))
        .on('end', resolve);
    });
};

module.exports = (done) => {

    let t1 = taskCopyCSS();
    
    return Promise.all([
        t1
    ]).then(() => {
        done();
    });
};
