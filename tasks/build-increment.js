/*!
* Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
* 
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
* 
*    http://www.apache.org/licenses/LICENSE-2.0
* 
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

let fs = require('fs');
let track = require('../tasks-helpers/track');

module.exports = (done) => {
    const jconfig = JSON.parse(fs.readFileSync('./version.json')),
          build = parseInt(jconfig.build) + 1;
    jconfig.build = build;
    fs.writeFileSync('./version.json', JSON.stringify(jconfig, null, 4));
    track('Incremented Build Number to ' + build);
    if(done) done();
};
