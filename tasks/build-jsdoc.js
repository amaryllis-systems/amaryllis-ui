/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

const gulp = require('gulp');
const track = require('../tasks-helpers/track');

const jsdocs = (done) => {
    
    let t1 = copyTask();
    t1.then(() => {
        let t2 = buildTask();
        t2.then(() => {
            done();
        });
    });
    
};

const copyTask = () => {
    track('Copy Core Fonts...');
    return new Promise((resolve, reject) => {
        gulp.src('./src/fonts/*.*')
            .pipe(gulp.dest('./docs/jsdoc/httpd/fonts'))
            .on('end', resolve);
    });
};

const buildTask = () => {
    track('Buid docs...');
    const jsdoc = require('gulp-jsdoc3');
    const config = require('../jsdoc.json');
    return new Promise((resolve, reject) => {
        gulp.src(['README.md'], {read: false})
            .pipe(jsdoc(config, resolve))
            .on('error', reject);
    });
};


module.exports = jsdocs;
