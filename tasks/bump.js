/*!
 * Copyright 2018 qm-b <https://bitbucket.org/qm-b/>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *    http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
let gulp = require('gulp'),
    bump = require('gulp-bump'),
	argv = require('yargs').argv;

const packa = 'package.json';
const uiVersion = "version.json";
const uiVersionDest = "src/";

// Update Major Version:
//
// gulp bump --next major
const bumpDefault = () => {
    let v = argv.next, args;
    if(!v) {
        v = argv.v;
        if(!v) {
            v = argv.next = 'patch';
            args = {type: v};
        } else {
            args = {version: v};
        }
    } else {
        args = {type: v};
    }
    let pipes = [
        gulp.src([packa])
            .pipe(bump(args))
            .pipe(gulp.dest('./')),
        gulp.src(uiVersion)
            .pipe(bump(args))
            .pipe(gulp.dest('./'))
            .pipe(gulp.dest(uiVersionDest))
    ];

    return Promise.all(pipes);
};

const bumpMajor = () => {
    let v = 'minor';
    let pipes = [
        gulp.src([packa])
            .pipe(bump({type: v}))
            .pipe(gulp.dest('./')),
        gulp.src(uiVersion)
            .pipe(bump({type: v}))
            .pipe(gulp.dest('./'))
            .pipe(gulp.dest(uiVersionDest))
    ];

    return Promise.all(pipes);
};

const bumpMinor = () => {
    let v = 'minor';
    let pipes = [
        gulp.src([packa])
            .pipe(bump({type: v}))
            .pipe(gulp.dest('./')),
        gulp.src(uiVersion)
            .pipe(bump({type: v}))
            .pipe(gulp.dest('./'))
            .pipe(gulp.dest(uiVersionDest))
    ];

    return Promise.all(pipes);
};

const bumpPatch = () => {
    let v = 'patch';
    let pipes = [
        gulp.src([packa])
            .pipe(bump({type: v}))
            .pipe(gulp.dest('./')),
        gulp.src(uiVersion)
            .pipe(bump({type: v}))
            .pipe(gulp.dest(uiVersionDest))
            .pipe(gulp.dest('./'))
    ];

    return Promise.all(pipes);
};

const bumpVersion = () => {
    let v = argv.next;
    if(!v) {
        throw new Error('Missing version argument which is required!');
    }
    let pipes = [
        gulp.src([packa])
            .pipe(bump({version: v}))
            .pipe(gulp.dest('./')),
        gulp.src(uiVersion)
            .pipe(bump({version: v}))
            .pipe(gulp.dest('./'))
            .pipe(gulp.dest(uiVersionDest))
    ];

    return Promise.all(pipes);
};

module.exports = {
    bump: bumpDefault,
    version: bumpVersion,
	major: bumpMajor,
	minor: bumpMinor,
	patch: bumpPatch
};
